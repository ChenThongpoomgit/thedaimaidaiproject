﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color32)
extern void ParticleSystem_Emit_m8C3FCE4F94165CDF0B86326DDB5DB886C1D7B0CF ();
// 0x00000002 System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem_Particle)
extern void ParticleSystem_Emit_m26C1CE51747F6F96A02AF1E56DDF3C3539FC926D ();
// 0x00000003 System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem_Particle[],System.Int32,System.Int32)
extern void ParticleSystem_SetParticles_mBD5C10AC2CCDECED4FEFE83235CF2CA1257A68AC ();
// 0x00000004 System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem_Particle[],System.Int32)
extern void ParticleSystem_SetParticles_mCBB22C645CD23845D88FDF981FC2F32E31AB4FB5 ();
// 0x00000005 System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern void ParticleSystem_Play_mBB238026E6389F44C76498D31038FE7A8C47E3AA ();
// 0x00000006 System.Void UnityEngine.ParticleSystem::Play()
extern void ParticleSystem_Play_m5BC5E6B56FCF639CAD5DF41B51DC05A0B444212F ();
// 0x00000007 System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern void ParticleSystem_Stop_m0EEE504756C56D38D3F8AB0FE73042A4612352F2 ();
// 0x00000008 System.Void UnityEngine.ParticleSystem::Stop(System.Boolean)
extern void ParticleSystem_Stop_m6A4F8F9868A2A39AE5C1A0A780716E0DCBFF6F92 ();
// 0x00000009 System.Void UnityEngine.ParticleSystem::Stop()
extern void ParticleSystem_Stop_m02FB082790DB4DEC7A8D0A56A1AC34D1E446099E ();
// 0x0000000A System.Void UnityEngine.ParticleSystem::Emit(System.Int32)
extern void ParticleSystem_Emit_m4C0873B2917D6C3E000609EA35B3C3F648B0BBC2 ();
// 0x0000000B System.Void UnityEngine.ParticleSystem::Emit_Internal(System.Int32)
extern void ParticleSystem_Emit_Internal_m1857956B7219B8232C1777E515706F8075C8B925 ();
// 0x0000000C System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem_EmitParams,System.Int32)
extern void ParticleSystem_Emit_mC0F1810F887D9EDE111F2307F2280CD0E4BA6AA2 ();
// 0x0000000D System.Void UnityEngine.ParticleSystem::EmitOld_Internal(UnityEngine.ParticleSystem_Particle&)
extern void ParticleSystem_EmitOld_Internal_m4313E5BD80E21011786EA12F2D2D9EFE9186320E ();
// 0x0000000E UnityEngine.ParticleSystem_MainModule UnityEngine.ParticleSystem::get_main()
extern void ParticleSystem_get_main_m360B0AA57C71DE0358B6B07133C68B5FD88C742F ();
// 0x0000000F UnityEngine.ParticleSystem_EmissionModule UnityEngine.ParticleSystem::get_emission()
extern void ParticleSystem_get_emission_mA1204EAF07A6C6B3F65B45295797A1FFF64D343C ();
// 0x00000010 System.Void UnityEngine.ParticleSystem::Emit_Injected(UnityEngine.ParticleSystem_EmitParams&,System.Int32)
extern void ParticleSystem_Emit_Injected_mB34A23399928EDC3111C060A2346A1EF63E1B9CC ();
// 0x00000011 System.Void UnityEngine.ParticleSystem_MainModule::.ctor(UnityEngine.ParticleSystem)
extern void MainModule__ctor_m10DC65291ACEC243EC5302404E059717B552BA7A_AdjustorThunk ();
// 0x00000012 UnityEngine.ParticleSystem_MinMaxCurve UnityEngine.ParticleSystem_MainModule::get_startSize()
extern void MainModule_get_startSize_m6ADAAC3A34D29AFBC962615553178D866E03613D_AdjustorThunk ();
// 0x00000013 UnityEngine.ParticleSystem_MinMaxGradient UnityEngine.ParticleSystem_MainModule::get_startColor()
extern void MainModule_get_startColor_m7837CBA981528393CE3EC758CD62DA7D59BD4E40_AdjustorThunk ();
// 0x00000014 System.Void UnityEngine.ParticleSystem_MainModule::get_startSize_Injected(UnityEngine.ParticleSystem_MainModule&,UnityEngine.ParticleSystem_MinMaxCurve&)
extern void MainModule_get_startSize_Injected_mDB29D09C47565D337A7D22DF51836F8D9350DA03 ();
// 0x00000015 System.Void UnityEngine.ParticleSystem_MainModule::get_startColor_Injected(UnityEngine.ParticleSystem_MainModule&,UnityEngine.ParticleSystem_MinMaxGradient&)
extern void MainModule_get_startColor_Injected_mF7E92B901DB87BBFB5EFBE303B785643501BC7EF ();
// 0x00000016 System.Void UnityEngine.ParticleSystem_EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern void EmissionModule__ctor_mD6B4029B58ECFECE0567E7FD67962FEF52B15843_AdjustorThunk ();
// 0x00000017 System.Void UnityEngine.ParticleSystem_EmissionModule::SetBurst(System.Int32,UnityEngine.ParticleSystem_Burst)
extern void EmissionModule_SetBurst_m855EB96A7CAF97B16CE081AFD633DECA75A7E0D3_AdjustorThunk ();
// 0x00000018 System.Void UnityEngine.ParticleSystem_EmissionModule::SetBurst_Injected(UnityEngine.ParticleSystem_EmissionModule&,System.Int32,UnityEngine.ParticleSystem_Burst&)
extern void EmissionModule_SetBurst_Injected_m033097551A2C5DD87BFFCF9A92BE571765F7DB27 ();
// 0x00000019 System.Void UnityEngine.ParticleSystem_Particle::set_lifetime(System.Single)
extern void Particle_set_lifetime_m0DB60575386F2D365BCCCAB07538FC2BFF81EC17_AdjustorThunk ();
// 0x0000001A System.Void UnityEngine.ParticleSystem_Particle::set_position(UnityEngine.Vector3)
extern void Particle_set_position_m3E99F891841E8B03490433FAFF5B601A6D12BDEF_AdjustorThunk ();
// 0x0000001B System.Void UnityEngine.ParticleSystem_Particle::set_velocity(UnityEngine.Vector3)
extern void Particle_set_velocity_mD0476C793611AD570296960FB0CB8FECD387E99C_AdjustorThunk ();
// 0x0000001C System.Void UnityEngine.ParticleSystem_Particle::set_remainingLifetime(System.Single)
extern void Particle_set_remainingLifetime_mD6ABB0C19127BD86DE3723B443331E5968EE0E87_AdjustorThunk ();
// 0x0000001D System.Void UnityEngine.ParticleSystem_Particle::set_startLifetime(System.Single)
extern void Particle_set_startLifetime_mEEB2B63599B1E4D1B8B2CEE25F13A50F1BCE7BBE_AdjustorThunk ();
// 0x0000001E System.Void UnityEngine.ParticleSystem_Particle::set_startColor(UnityEngine.Color32)
extern void Particle_set_startColor_m67807C44D14862EBD8C030C1FE094E8438384AA6_AdjustorThunk ();
// 0x0000001F System.Void UnityEngine.ParticleSystem_Particle::set_randomSeed(System.UInt32)
extern void Particle_set_randomSeed_m1311237E65918DDD765FC4D6BAE85047D8B8CBCE_AdjustorThunk ();
// 0x00000020 System.Void UnityEngine.ParticleSystem_Particle::set_startSize(System.Single)
extern void Particle_set_startSize_m45B6CD1480219E30A96317D654B9439C8DB2DF87_AdjustorThunk ();
// 0x00000021 System.Void UnityEngine.ParticleSystem_Particle::set_rotation3D(UnityEngine.Vector3)
extern void Particle_set_rotation3D_m46DB39BFDEEF27C6119F5EEE2C0B1CA9093FC834_AdjustorThunk ();
// 0x00000022 System.Void UnityEngine.ParticleSystem_Particle::set_angularVelocity3D(UnityEngine.Vector3)
extern void Particle_set_angularVelocity3D_m0F282D7EE110DF290E04B2B99FEC697ED89BF4EF_AdjustorThunk ();
// 0x00000023 System.Void UnityEngine.ParticleSystem_Burst::set_count(UnityEngine.ParticleSystem_MinMaxCurve)
extern void Burst_set_count_mCBDB92052C854272D90E471DB17C2F3F9C388D5C_AdjustorThunk ();
// 0x00000024 System.Void UnityEngine.ParticleSystem_MinMaxCurve::.ctor(System.Single)
extern void MinMaxCurve__ctor_mE5FDFD4ADB7EA897D19133CF82DC290577B4DD29_AdjustorThunk ();
// 0x00000025 System.Single UnityEngine.ParticleSystem_MinMaxCurve::get_constant()
extern void MinMaxCurve_get_constant_m5674EE1F5E0DAFAC2273DAE998D8FDC1370B6905_AdjustorThunk ();
// 0x00000026 UnityEngine.ParticleSystem_MinMaxCurve UnityEngine.ParticleSystem_MinMaxCurve::op_Implicit(System.Single)
extern void MinMaxCurve_op_Implicit_m998EE9F8D83B9545F63E2DFA304E99620F0F707F ();
// 0x00000027 UnityEngine.Color UnityEngine.ParticleSystem_MinMaxGradient::get_color()
extern void MinMaxGradient_get_color_m3216DBAFE795468D49B97BA2E0B1B096B49870F7_AdjustorThunk ();
// 0x00000028 System.Int32 UnityEngine.ParticleSystemRenderer::GetMeshes(UnityEngine.Mesh[])
extern void ParticleSystemRenderer_GetMeshes_m4DE519F198B6A36169F307F1FA5D76FA28316AD2 ();
static Il2CppMethodPointer s_methodPointers[40] = 
{
	ParticleSystem_Emit_m8C3FCE4F94165CDF0B86326DDB5DB886C1D7B0CF,
	ParticleSystem_Emit_m26C1CE51747F6F96A02AF1E56DDF3C3539FC926D,
	ParticleSystem_SetParticles_mBD5C10AC2CCDECED4FEFE83235CF2CA1257A68AC,
	ParticleSystem_SetParticles_mCBB22C645CD23845D88FDF981FC2F32E31AB4FB5,
	ParticleSystem_Play_mBB238026E6389F44C76498D31038FE7A8C47E3AA,
	ParticleSystem_Play_m5BC5E6B56FCF639CAD5DF41B51DC05A0B444212F,
	ParticleSystem_Stop_m0EEE504756C56D38D3F8AB0FE73042A4612352F2,
	ParticleSystem_Stop_m6A4F8F9868A2A39AE5C1A0A780716E0DCBFF6F92,
	ParticleSystem_Stop_m02FB082790DB4DEC7A8D0A56A1AC34D1E446099E,
	ParticleSystem_Emit_m4C0873B2917D6C3E000609EA35B3C3F648B0BBC2,
	ParticleSystem_Emit_Internal_m1857956B7219B8232C1777E515706F8075C8B925,
	ParticleSystem_Emit_mC0F1810F887D9EDE111F2307F2280CD0E4BA6AA2,
	ParticleSystem_EmitOld_Internal_m4313E5BD80E21011786EA12F2D2D9EFE9186320E,
	ParticleSystem_get_main_m360B0AA57C71DE0358B6B07133C68B5FD88C742F,
	ParticleSystem_get_emission_mA1204EAF07A6C6B3F65B45295797A1FFF64D343C,
	ParticleSystem_Emit_Injected_mB34A23399928EDC3111C060A2346A1EF63E1B9CC,
	MainModule__ctor_m10DC65291ACEC243EC5302404E059717B552BA7A_AdjustorThunk,
	MainModule_get_startSize_m6ADAAC3A34D29AFBC962615553178D866E03613D_AdjustorThunk,
	MainModule_get_startColor_m7837CBA981528393CE3EC758CD62DA7D59BD4E40_AdjustorThunk,
	MainModule_get_startSize_Injected_mDB29D09C47565D337A7D22DF51836F8D9350DA03,
	MainModule_get_startColor_Injected_mF7E92B901DB87BBFB5EFBE303B785643501BC7EF,
	EmissionModule__ctor_mD6B4029B58ECFECE0567E7FD67962FEF52B15843_AdjustorThunk,
	EmissionModule_SetBurst_m855EB96A7CAF97B16CE081AFD633DECA75A7E0D3_AdjustorThunk,
	EmissionModule_SetBurst_Injected_m033097551A2C5DD87BFFCF9A92BE571765F7DB27,
	Particle_set_lifetime_m0DB60575386F2D365BCCCAB07538FC2BFF81EC17_AdjustorThunk,
	Particle_set_position_m3E99F891841E8B03490433FAFF5B601A6D12BDEF_AdjustorThunk,
	Particle_set_velocity_mD0476C793611AD570296960FB0CB8FECD387E99C_AdjustorThunk,
	Particle_set_remainingLifetime_mD6ABB0C19127BD86DE3723B443331E5968EE0E87_AdjustorThunk,
	Particle_set_startLifetime_mEEB2B63599B1E4D1B8B2CEE25F13A50F1BCE7BBE_AdjustorThunk,
	Particle_set_startColor_m67807C44D14862EBD8C030C1FE094E8438384AA6_AdjustorThunk,
	Particle_set_randomSeed_m1311237E65918DDD765FC4D6BAE85047D8B8CBCE_AdjustorThunk,
	Particle_set_startSize_m45B6CD1480219E30A96317D654B9439C8DB2DF87_AdjustorThunk,
	Particle_set_rotation3D_m46DB39BFDEEF27C6119F5EEE2C0B1CA9093FC834_AdjustorThunk,
	Particle_set_angularVelocity3D_m0F282D7EE110DF290E04B2B99FEC697ED89BF4EF_AdjustorThunk,
	Burst_set_count_mCBDB92052C854272D90E471DB17C2F3F9C388D5C_AdjustorThunk,
	MinMaxCurve__ctor_mE5FDFD4ADB7EA897D19133CF82DC290577B4DD29_AdjustorThunk,
	MinMaxCurve_get_constant_m5674EE1F5E0DAFAC2273DAE998D8FDC1370B6905_AdjustorThunk,
	MinMaxCurve_op_Implicit_m998EE9F8D83B9545F63E2DFA304E99620F0F707F,
	MinMaxGradient_get_color_m3216DBAFE795468D49B97BA2E0B1B096B49870F7_AdjustorThunk,
	ParticleSystemRenderer_GetMeshes_m4DE519F198B6A36169F307F1FA5D76FA28316AD2,
};
static const int32_t s_InvokerIndices[40] = 
{
	1953,
	1954,
	35,
	137,
	31,
	23,
	833,
	31,
	23,
	32,
	32,
	1955,
	6,
	1956,
	1957,
	64,
	26,
	1958,
	1959,
	397,
	397,
	26,
	1960,
	944,
	346,
	1443,
	1443,
	346,
	346,
	1961,
	32,
	346,
	1443,
	1443,
	1962,
	346,
	733,
	1963,
	1470,
	112,
};
extern const Il2CppCodeGenModule g_UnityEngine_ParticleSystemModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_ParticleSystemModuleCodeGenModule = 
{
	"UnityEngine.ParticleSystemModule.dll",
	40,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
