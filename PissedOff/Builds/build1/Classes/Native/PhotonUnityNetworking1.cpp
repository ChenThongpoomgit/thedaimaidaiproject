﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// ExitGames.Client.Photon.Hashtable
struct Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD;
// ExitGames.Client.Photon.OperationResponse
struct OperationResponse_tC67CCFA9FAB3EDF55AA824724BA47D45AB3585FE;
// Photon.Pun.IPunPrefabPool
struct IPunPrefabPool_tB2C028CC90B18EE2C8E70A6502A14FC36319E974;
// Photon.Pun.PhotonStream
struct PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E;
// Photon.Pun.PhotonStreamQueue
struct PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1;
// Photon.Pun.PhotonTransformView
struct PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F;
// Photon.Pun.PhotonTransformViewClassic
struct PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545;
// Photon.Pun.PhotonTransformViewPositionControl
struct PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737;
// Photon.Pun.PhotonTransformViewPositionModel
struct PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969;
// Photon.Pun.PhotonTransformViewRotationControl
struct PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE;
// Photon.Pun.PhotonTransformViewRotationModel
struct PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D;
// Photon.Pun.PhotonTransformViewScaleControl
struct PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E;
// Photon.Pun.PhotonTransformViewScaleModel
struct PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59;
// Photon.Pun.PhotonView
struct PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B;
// Photon.Pun.PhotonView[]
struct PhotonViewU5BU5D_t79E798C13CF3CB9516F65B6A4C1E65491FF5CA27;
// Photon.Pun.PunEvent
struct PunEvent_t7D66CEAFA04E9A4EEEA16858A28136B5B4C4CE39;
// Photon.Pun.PunRPC
struct PunRPC_t49B358D1BE3E291A6F2FE4674A99AF7C8D2295DE;
// Photon.Pun.SceneManagerHelper
struct SceneManagerHelper_t364642E9E40D66062319539AF06FC0BE69FE795A;
// Photon.Pun.ServerSettings
struct ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9;
// Photon.Realtime.AppSettings
struct AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8;
// Photon.Realtime.AuthenticationValues
struct AuthenticationValues_t3D3544E09B8F3860CBF8B0998760A0CAE94F2D13;
// Photon.Realtime.ConnectionCallbacksContainer
struct ConnectionCallbacksContainer_tDCE59A1A1B21F1438DACBE458620B2942BB2A110;
// Photon.Realtime.EnterRoomParams
struct EnterRoomParams_t35117DAFF025133E50BB2CFA7864669F6CED1942;
// Photon.Realtime.InRoomCallbacksContainer
struct InRoomCallbacksContainer_tD26EF1A78F7419C0FF1B25FBD0CCEA40D3EB810D;
// Photon.Realtime.LoadBalancingClient
struct LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A;
// Photon.Realtime.LoadBalancingPeer
struct LoadBalancingPeer_tCD3148D53F3EF643FCBB86A139EF0B84FD3F3F14;
// Photon.Realtime.LobbyCallbacksContainer
struct LobbyCallbacksContainer_t42830CD7E8C5C38D10AB8157E9EDECF9B26C2571;
// Photon.Realtime.MatchMakingCallbacksContainer
struct MatchMakingCallbacksContainer_tE6BA3F44DC157B50650F599CD8C26D23E7153CC3;
// Photon.Realtime.Player
struct Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202;
// Photon.Realtime.RaiseEventOptions
struct RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED;
// Photon.Realtime.RegionHandler
struct RegionHandler_tE1C559FF2DCCA020EC31ACCF3993A434617A15AF;
// Photon.Realtime.Room
struct Room_t5DFC39DD6736A2641374564EC6C31352BE33000D;
// Photon.Realtime.TypedLobby
struct TypedLobby_tD5DAFEDF05C473EE3FB9618C0641D1CBB4CA4BD5;
// Photon.Realtime.WebRpcCallbacksContainer
struct WebRpcCallbacksContainer_t13E35FB3A5C223F5EC56992E6CF51A0BC4D10895;
// System.Action`1<ExitGames.Client.Photon.EventData>
struct Action_1_t989F0A0D66ADCCABF53890A353BD2AE5709333D4;
// System.Action`1<ExitGames.Client.Photon.OperationResponse>
struct Action_1_t654A246F32A45B98C5C30E5DCC92C7FDDDA24C2B;
// System.Action`2<Photon.Pun.PhotonView,Photon.Realtime.Player>
struct Action_2_t0A813634D13A25893BE13AF0CCCD116E584CC029;
// System.Action`2<Photon.Realtime.ClientState,Photon.Realtime.ClientState>
struct Action_2_tD4F07FC3F0B08A4BE3D400AB1E3553E633312299;
// System.Attribute
struct Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.Reflection.MethodInfo,System.Reflection.ParameterInfo[]>[]
struct EntryU5BU5D_tABD16668BF5C8B530E4B5798AD5A0E2FF4952344;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Reflection.MethodInfo,System.Reflection.ParameterInfo[]>
struct KeyCollection_t8EAE43025C480B5764B3067A593EDE842E953A42;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Reflection.MethodInfo,System.Reflection.ParameterInfo[]>
struct ValueCollection_tF900C762BB887C6C65419A8B1998E671E7A42C44;
// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>
struct Dictionary_2_tBB3FFEFC7BCAAA362AC87C23C01A4A4C98E66541;
// System.Collections.Generic.Dictionary`2<Photon.Pun.PhotonNetwork/RaiseEventBatch,Photon.Pun.PhotonNetwork/SerializeViewBatch>
struct Dictionary_2_t814656FD5269C0EAD336588E42742ED4E8A7AA30;
// System.Collections.Generic.Dictionary`2<System.Int32,Photon.Pun.PhotonView>
struct Dictionary_2_t24B47C5EBEBE3CBC1A517656D2D74B343EF88745;
// System.Collections.Generic.Dictionary`2<System.Int32,Photon.Realtime.Player>
struct Dictionary_2_tA8C1FE9FA1FB583BFC17676FCF9DFF7D606CB346;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA;
// System.Collections.Generic.Dictionary`2<System.Reflection.MethodInfo,System.Reflection.ParameterInfo[]>
struct Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>>
struct Dictionary_2_tAF6372C97005A35CF864BC1B4782B28D1D7F0974;
// System.Collections.Generic.HashSet`1<System.Byte>
struct HashSet_1_tE84C3FCD7322E7212DC4A4B67223007DA245B0F3;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t20C647F44B285CB7C061931B00ED43A230F311E5;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61;
// System.Collections.Generic.IEqualityComparer`1<System.Reflection.MethodInfo>
struct IEqualityComparer_1_t20CD27F1C2B16FDAA60041A3EFD5F912B5B763E9;
// System.Collections.Generic.List`1<Photon.Realtime.TypedLobbyInfo>
struct List_1_t6C9FCEA5B81789209C0373EEE6952A0DC27D1459;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300;
// System.Collections.Generic.Queue`1<Photon.Realtime.LoadBalancingClient/CallbackTargetChange>
struct Queue_1_t2DF167EE7F886C6E1CE292988624D5634AF12DE2;
// System.Collections.Generic.Queue`1<UnityEngine.Vector3>
struct Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE;
// System.Diagnostics.Stopwatch
struct Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ParameterInfo
struct ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AsyncOperation
struct AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Component[]
struct ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;

IL2CPP_EXTERN_C RuntimeClass* ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IPunObservable_t885A5BFE290BD3AC3942882E7533681C488F048C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PunExtensions_tE401775F4D752FBE7F8267AE3290ACE975B542DC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral00DE275EEC87CFE3541333E3B3214CBEA0B6C47F;
IL2CPP_EXTERN_C String_t* _stringLiteral110BE4F4AA851DA91271D7D722097AD2AEEAD525;
IL2CPP_EXTERN_C String_t* _stringLiteral379B5CC68F0A8B7B36ADBB4201083ED4AE209AD4;
IL2CPP_EXTERN_C String_t* _stringLiteral453CDC1FEBD60FCF5E5819B6B0ED9F59FB60C270;
IL2CPP_EXTERN_C String_t* _stringLiteral59E599355D585684E85CC8FF2CFC77E2DFE102D7;
IL2CPP_EXTERN_C String_t* _stringLiteral641A80CA205D00BD50B62CE6056B444CA346C0C0;
IL2CPP_EXTERN_C String_t* _stringLiteral7E97DB408E60C67BAFDFF096CFE901D68FF1F5A1;
IL2CPP_EXTERN_C String_t* _stringLiteral823C156003F996BBF5384D39F2EE6DA95992F844;
IL2CPP_EXTERN_C String_t* _stringLiteral845F4E00AAB16D72B67F9F500C408C22F5A451A9;
IL2CPP_EXTERN_C String_t* _stringLiteral91B876DF2F22F0B715909B534ED040817CB27572;
IL2CPP_EXTERN_C String_t* _stringLiteralA74E0FB14579A337D921569501855EE2A21B81D8;
IL2CPP_EXTERN_C String_t* _stringLiteralBE3BE2E556B811A1B556DADDABF39548BAD394CB;
IL2CPP_EXTERN_C String_t* _stringLiteralD75636B15D4B8F123C4162CB1DF0929699312854;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralDB6C224CF890308B59EE451807A94B4A842EBB02;
IL2CPP_EXTERN_C String_t* _stringLiteralFDF81A60C6A64CE5D52ACBCF43295B8B789D2AF4;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_m347A176252862D6CD26E7E6B5E9EAF8F4159273B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponents_TisMonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429_mFF32883CC15055E3DA209D02FFE7DFEB78C12741_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m44391EE1219E87B732BAD0E46BC433083B9BABB0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mDE47E426BCB97B4E95703C8B6360F6D16DD9C4F7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m82085B12B5862B10FC15F89AF6C6C5F7F4548040_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_mC78C8E2525ECF7D840C9BB1A311E001856163E30_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponentsInChildren_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_m318AB55027D265F28D3BC79BEA2BE0434A568E62_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m9EFB9DB6274EEB0D4B5050480FE1E8CA56B85534_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mA6C019255203C3A859BB0856B40200EA1E5F8EDA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Queue_1_Dequeue_mF86F3E89DD24E5CC47AF827C6DA43A4062F9A344_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Queue_1_Enqueue_m1D4D0569B64A37FF041C5EF8CE19CB5210E2ED2F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Queue_1_Peek_mC6F614DA3810E0A0FB0F1397A82039A7939A2442_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Queue_1__ctor_mE0E9CF1FAAEBD411236EF5238DF26D79E380C394_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t PhotonStreamQueue_BeginWritePackage_m1422FC9FA4E756A2F8A611B14B971A60B0C90D13_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonStreamQueue_Deserialize_m3F3F09D6964955C32E72554D893FACEDD3315196_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonStreamQueue_ReceiveNext_m40ACF14FF55E9C9D3EDCDC9EFAABE7994E2D9919_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonStreamQueue_Reset_mB399A65DD771F8779251FB5C671916E3E7D4A282_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonStreamQueue_SendNext_mE2B9D3157A8AFCE44572AA33B31A9BA6B8344F17_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonStreamQueue_Serialize_m404C367BBE48A61A242517D2353389562EAAC3B3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonStreamQueue__ctor_m701EEA27997AEC65B1C9C4684B1F1C8717FDB4A2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewClassic_Awake_mD37DE3E2DA509586BB93C634941775BB66326295_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewClassic_Update_m13B896CDDFAEFD5E41EABBA12C30E5281E7AAA91_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewClassic__ctor_mB0D548ABFFE4FECC5B4E541C50CD00398CBF24CF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewPositionControl_DeserializeData_m9D749828D57C78D6AD4443D66E8DCFBC6D0423A3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewPositionControl_GetExtrapolatedPositionOffset_mFE9B782C8EDB94A36173C1559B72EF14790690C9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m4982592135EA10FACC90E0A78B3423BCB1C84405_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewPositionControl_OnPhotonSerializeView_mEADE2EB3B08D87FE97F122AAEC97FF83F63A5B51_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewPositionControl_SerializeData_m46A84222F254F10D91A0762D0A70DAC9F4BF0CC8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewPositionControl_UpdatePosition_m9D1775F2DE7FFE494E96F280CF4DFAAB65FE8429_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewPositionControl__ctor_mCEAF26452CE4B62915C473A91E0ACB05230312D7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewRotationControl_GetRotation_m8573B3D9871305C20D1023A135BA04AEA0D3EFF0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewRotationControl_OnPhotonSerializeView_m817CD4BE62ABA98E2C071C8AFF0121BB93AFDEED_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewScaleControl_GetScale_m0826273DFB50810E7B8E8CE8D05DFA2E67D8D251_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewScaleControl_OnPhotonSerializeView_mB03F0D37841831CA9A6664102D8A16F4F8517F7C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformViewScaleControl__ctor_mEFFA198BF98C3BFE37573865DD22C312FAEC20A1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformView_Awake_m8B92F4E07ECCD196DBB64FCA42D08D717AADAF25_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformView_OnPhotonSerializeView_mB933AB086DEDE024EFEBF6E9445DB2C601FB1CEB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonTransformView_Update_mF602711F83B7F676311525103EC74CE42C46E791_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_Awake_mA6E574DBACD2811EDD303783CB7C256F6ACAEA4F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_DeserializeComponent_m556FF3D2A81CA3B9A6371944CDC9F728820F7788_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_DeserializeView_m2E67D40D896990811E722E1FB99BC5BFEC46EFB2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_Find_m74440585ADB7D8B89E260397726D052BAA9F9B3E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_Get_mF77DAA2CC8B2E80DBBD4588D6B5264AF853CE343_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_Get_mFABADCC5E785D810809CC040BE8A1AA299B8D857_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_OnDestroy_mFE646382844BCDEF6001AAE47167BD704975D8DE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_RPC_m3F59D00D60E09EEFCC5B5000BB7195F25689029C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_RPC_mF2E2691617AC053125729CA8D9D1AEF14A78A301_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_RefreshRpcMonoBehaviourCache_m218A196211F926F6B1FD74910E3E87F2D5F05DD1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_RequestOwnership_mFD54041B0C70B516EC2DB1C12E606825551AB9F0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_RpcSecure_m256A5ABAB0FF68642767ECE1D1CD1C9E391C4B16_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_RpcSecure_m769EF34360B1DE9B0F7FD51F77E39AF231E36B1F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_SerializeComponent_m745D96F1F125E057101C1C52E4063E1FD74424FF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_SerializeView_m4FA3E888E8C7C3A959ABEAD9194CFEBFFA709156_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_ToString_m9C6ED6975CD25D9820A71581FCC5C1BBBF1F58B4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_TransferOwnership_mE63F4A1B73D7E89DEC4261C163880A3BEB5F5B76_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_get_ControllerActorNr_m8D9D0453C6E350464D670B38210911BCEAE61C97_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_get_Controller_mABA45440F92EDB48CB1F32D6D2B3553C61E11DC6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_get_CreatorActorNr_m182100823912B7A313C17D0844D599BFAAA03629_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_get_InstantiationData_mB0731A9C32456F8169DC4C5FEA21A7587FAD8B31_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_get_IsMine_mFE1D0AB5684F18073518777C1D8FF764CB335793_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_get_OwnerActorNr_m297F5F405EF3ACE0A262F942FF01357EE4BDE886_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_get_Owner_mD339E981ADADF5A7F88F6D4F530F1EB8F1C34DEE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_get_Prefix_m4EBD59509A85CF1D43C09F70EA41AA2CE20F3602_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PhotonView_set_ViewID_m489879E6DAD4700D0A0DCA77849EAA92FAFE7CDB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PunExtensions_AlmostEquals_m1A0F117F7E0EAB9BADEA0CF8C1B2F702530694B8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PunExtensions_AlmostEquals_m576E61725EF1B1E75659AB3616246EBE180BDFB1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PunExtensions_AlmostEquals_m945D736C9D101B6DACE0B2001733CB97C24E22F3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PunExtensions_AlmostEquals_mE1D4C852AC99DC32AEA769349F6DD6BC8D437057_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PunExtensions_GetCachedParemeters_mBD43BDB3CBD4167A1E49E72949C633762DC8105D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PunExtensions_GetPhotonView_mDA3596A23EAA5C6C2B0DBAC323D16A7C8D628412_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PunExtensions_GetPhotonViewsInChildren_m6AE772AD8960DAC9540669A39A2ED5A7984D45DB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PunExtensions__cctor_m7F398F5699A60C0DD89C3EFB7D8CF796CE5F12B9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ServerSettings_IsAppId_m728E76CE95E25CDA610ECDCC949A547E7A55E6CF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ServerSettings_ResetBestRegionCodeInPreferences_m59742F06C329B5EA26D7B070795CAB5775D951AA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ServerSettings_ToString_mFCA84BBA9102D219FA2C384B5A9F46392D34A736_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ServerSettings__ctor_mC3ABDBA3917DC9D5F3065BCA69ED7B457BB68004_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ServerSettings_get_BestRegionSummaryInPreferences_mFCEF00C287482CD83E379A87574B085F0D150AF8_MetadataUsageId;

struct PhotonViewU5BU5D_t79E798C13CF3CB9516F65B6A4C1E65491FF5CA27;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694;
struct MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// Photon.Pun.PhotonStream
struct  PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Object> Photon.Pun.PhotonStream::writeData
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___writeData_0;
	// System.Object[] Photon.Pun.PhotonStream::readData
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___readData_1;
	// System.Int32 Photon.Pun.PhotonStream::currentItem
	int32_t ___currentItem_2;
	// System.Boolean Photon.Pun.PhotonStream::<IsWriting>k__BackingField
	bool ___U3CIsWritingU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_writeData_0() { return static_cast<int32_t>(offsetof(PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E, ___writeData_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_writeData_0() const { return ___writeData_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_writeData_0() { return &___writeData_0; }
	inline void set_writeData_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___writeData_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___writeData_0), (void*)value);
	}

	inline static int32_t get_offset_of_readData_1() { return static_cast<int32_t>(offsetof(PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E, ___readData_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_readData_1() const { return ___readData_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_readData_1() { return &___readData_1; }
	inline void set_readData_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___readData_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___readData_1), (void*)value);
	}

	inline static int32_t get_offset_of_currentItem_2() { return static_cast<int32_t>(offsetof(PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E, ___currentItem_2)); }
	inline int32_t get_currentItem_2() const { return ___currentItem_2; }
	inline int32_t* get_address_of_currentItem_2() { return &___currentItem_2; }
	inline void set_currentItem_2(int32_t value)
	{
		___currentItem_2 = value;
	}

	inline static int32_t get_offset_of_U3CIsWritingU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E, ___U3CIsWritingU3Ek__BackingField_3)); }
	inline bool get_U3CIsWritingU3Ek__BackingField_3() const { return ___U3CIsWritingU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsWritingU3Ek__BackingField_3() { return &___U3CIsWritingU3Ek__BackingField_3; }
	inline void set_U3CIsWritingU3Ek__BackingField_3(bool value)
	{
		___U3CIsWritingU3Ek__BackingField_3 = value;
	}
};


// Photon.Pun.PhotonStreamQueue
struct  PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1  : public RuntimeObject
{
public:
	// System.Int32 Photon.Pun.PhotonStreamQueue::m_SampleRate
	int32_t ___m_SampleRate_0;
	// System.Int32 Photon.Pun.PhotonStreamQueue::m_SampleCount
	int32_t ___m_SampleCount_1;
	// System.Int32 Photon.Pun.PhotonStreamQueue::m_ObjectsPerSample
	int32_t ___m_ObjectsPerSample_2;
	// System.Single Photon.Pun.PhotonStreamQueue::m_LastSampleTime
	float ___m_LastSampleTime_3;
	// System.Int32 Photon.Pun.PhotonStreamQueue::m_LastFrameCount
	int32_t ___m_LastFrameCount_4;
	// System.Int32 Photon.Pun.PhotonStreamQueue::m_NextObjectIndex
	int32_t ___m_NextObjectIndex_5;
	// System.Collections.Generic.List`1<System.Object> Photon.Pun.PhotonStreamQueue::m_Objects
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___m_Objects_6;
	// System.Boolean Photon.Pun.PhotonStreamQueue::m_IsWriting
	bool ___m_IsWriting_7;

public:
	inline static int32_t get_offset_of_m_SampleRate_0() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1, ___m_SampleRate_0)); }
	inline int32_t get_m_SampleRate_0() const { return ___m_SampleRate_0; }
	inline int32_t* get_address_of_m_SampleRate_0() { return &___m_SampleRate_0; }
	inline void set_m_SampleRate_0(int32_t value)
	{
		___m_SampleRate_0 = value;
	}

	inline static int32_t get_offset_of_m_SampleCount_1() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1, ___m_SampleCount_1)); }
	inline int32_t get_m_SampleCount_1() const { return ___m_SampleCount_1; }
	inline int32_t* get_address_of_m_SampleCount_1() { return &___m_SampleCount_1; }
	inline void set_m_SampleCount_1(int32_t value)
	{
		___m_SampleCount_1 = value;
	}

	inline static int32_t get_offset_of_m_ObjectsPerSample_2() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1, ___m_ObjectsPerSample_2)); }
	inline int32_t get_m_ObjectsPerSample_2() const { return ___m_ObjectsPerSample_2; }
	inline int32_t* get_address_of_m_ObjectsPerSample_2() { return &___m_ObjectsPerSample_2; }
	inline void set_m_ObjectsPerSample_2(int32_t value)
	{
		___m_ObjectsPerSample_2 = value;
	}

	inline static int32_t get_offset_of_m_LastSampleTime_3() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1, ___m_LastSampleTime_3)); }
	inline float get_m_LastSampleTime_3() const { return ___m_LastSampleTime_3; }
	inline float* get_address_of_m_LastSampleTime_3() { return &___m_LastSampleTime_3; }
	inline void set_m_LastSampleTime_3(float value)
	{
		___m_LastSampleTime_3 = value;
	}

	inline static int32_t get_offset_of_m_LastFrameCount_4() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1, ___m_LastFrameCount_4)); }
	inline int32_t get_m_LastFrameCount_4() const { return ___m_LastFrameCount_4; }
	inline int32_t* get_address_of_m_LastFrameCount_4() { return &___m_LastFrameCount_4; }
	inline void set_m_LastFrameCount_4(int32_t value)
	{
		___m_LastFrameCount_4 = value;
	}

	inline static int32_t get_offset_of_m_NextObjectIndex_5() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1, ___m_NextObjectIndex_5)); }
	inline int32_t get_m_NextObjectIndex_5() const { return ___m_NextObjectIndex_5; }
	inline int32_t* get_address_of_m_NextObjectIndex_5() { return &___m_NextObjectIndex_5; }
	inline void set_m_NextObjectIndex_5(int32_t value)
	{
		___m_NextObjectIndex_5 = value;
	}

	inline static int32_t get_offset_of_m_Objects_6() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1, ___m_Objects_6)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_m_Objects_6() const { return ___m_Objects_6; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_m_Objects_6() { return &___m_Objects_6; }
	inline void set_m_Objects_6(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___m_Objects_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Objects_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_IsWriting_7() { return static_cast<int32_t>(offsetof(PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1, ___m_IsWriting_7)); }
	inline bool get_m_IsWriting_7() const { return ___m_IsWriting_7; }
	inline bool* get_address_of_m_IsWriting_7() { return &___m_IsWriting_7; }
	inline void set_m_IsWriting_7(bool value)
	{
		___m_IsWriting_7 = value;
	}
};


// Photon.Pun.PunEvent
struct  PunEvent_t7D66CEAFA04E9A4EEEA16858A28136B5B4C4CE39  : public RuntimeObject
{
public:

public:
};


// Photon.Pun.PunExtensions
struct  PunExtensions_tE401775F4D752FBE7F8267AE3290ACE975B542DC  : public RuntimeObject
{
public:

public:
};

struct PunExtensions_tE401775F4D752FBE7F8267AE3290ACE975B542DC_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Reflection.MethodInfo,System.Reflection.ParameterInfo[]> Photon.Pun.PunExtensions::ParametersOfMethods
	Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF * ___ParametersOfMethods_0;

public:
	inline static int32_t get_offset_of_ParametersOfMethods_0() { return static_cast<int32_t>(offsetof(PunExtensions_tE401775F4D752FBE7F8267AE3290ACE975B542DC_StaticFields, ___ParametersOfMethods_0)); }
	inline Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF * get_ParametersOfMethods_0() const { return ___ParametersOfMethods_0; }
	inline Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF ** get_address_of_ParametersOfMethods_0() { return &___ParametersOfMethods_0; }
	inline void set_ParametersOfMethods_0(Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF * value)
	{
		___ParametersOfMethods_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ParametersOfMethods_0), (void*)value);
	}
};


// Photon.Pun.SceneManagerHelper
struct  SceneManagerHelper_t364642E9E40D66062319539AF06FC0BE69FE795A  : public RuntimeObject
{
public:

public:
};


// Photon.Realtime.Player
struct  Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202  : public RuntimeObject
{
public:
	// Photon.Realtime.Room Photon.Realtime.Player::<RoomReference>k__BackingField
	Room_t5DFC39DD6736A2641374564EC6C31352BE33000D * ___U3CRoomReferenceU3Ek__BackingField_0;
	// System.Int32 Photon.Realtime.Player::actorNumber
	int32_t ___actorNumber_1;
	// System.Boolean Photon.Realtime.Player::IsLocal
	bool ___IsLocal_2;
	// System.String Photon.Realtime.Player::nickName
	String_t* ___nickName_3;
	// System.String Photon.Realtime.Player::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_4;
	// System.Boolean Photon.Realtime.Player::<IsInactive>k__BackingField
	bool ___U3CIsInactiveU3Ek__BackingField_5;
	// ExitGames.Client.Photon.Hashtable Photon.Realtime.Player::<CustomProperties>k__BackingField
	Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * ___U3CCustomPropertiesU3Ek__BackingField_6;
	// System.Object Photon.Realtime.Player::TagObject
	RuntimeObject * ___TagObject_7;

public:
	inline static int32_t get_offset_of_U3CRoomReferenceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202, ___U3CRoomReferenceU3Ek__BackingField_0)); }
	inline Room_t5DFC39DD6736A2641374564EC6C31352BE33000D * get_U3CRoomReferenceU3Ek__BackingField_0() const { return ___U3CRoomReferenceU3Ek__BackingField_0; }
	inline Room_t5DFC39DD6736A2641374564EC6C31352BE33000D ** get_address_of_U3CRoomReferenceU3Ek__BackingField_0() { return &___U3CRoomReferenceU3Ek__BackingField_0; }
	inline void set_U3CRoomReferenceU3Ek__BackingField_0(Room_t5DFC39DD6736A2641374564EC6C31352BE33000D * value)
	{
		___U3CRoomReferenceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRoomReferenceU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_actorNumber_1() { return static_cast<int32_t>(offsetof(Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202, ___actorNumber_1)); }
	inline int32_t get_actorNumber_1() const { return ___actorNumber_1; }
	inline int32_t* get_address_of_actorNumber_1() { return &___actorNumber_1; }
	inline void set_actorNumber_1(int32_t value)
	{
		___actorNumber_1 = value;
	}

	inline static int32_t get_offset_of_IsLocal_2() { return static_cast<int32_t>(offsetof(Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202, ___IsLocal_2)); }
	inline bool get_IsLocal_2() const { return ___IsLocal_2; }
	inline bool* get_address_of_IsLocal_2() { return &___IsLocal_2; }
	inline void set_IsLocal_2(bool value)
	{
		___IsLocal_2 = value;
	}

	inline static int32_t get_offset_of_nickName_3() { return static_cast<int32_t>(offsetof(Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202, ___nickName_3)); }
	inline String_t* get_nickName_3() const { return ___nickName_3; }
	inline String_t** get_address_of_nickName_3() { return &___nickName_3; }
	inline void set_nickName_3(String_t* value)
	{
		___nickName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nickName_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202, ___U3CUserIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_4() const { return ___U3CUserIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_4() { return &___U3CUserIdU3Ek__BackingField_4; }
	inline void set_U3CUserIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CUserIdU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsInactiveU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202, ___U3CIsInactiveU3Ek__BackingField_5)); }
	inline bool get_U3CIsInactiveU3Ek__BackingField_5() const { return ___U3CIsInactiveU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsInactiveU3Ek__BackingField_5() { return &___U3CIsInactiveU3Ek__BackingField_5; }
	inline void set_U3CIsInactiveU3Ek__BackingField_5(bool value)
	{
		___U3CIsInactiveU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CCustomPropertiesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202, ___U3CCustomPropertiesU3Ek__BackingField_6)); }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * get_U3CCustomPropertiesU3Ek__BackingField_6() const { return ___U3CCustomPropertiesU3Ek__BackingField_6; }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD ** get_address_of_U3CCustomPropertiesU3Ek__BackingField_6() { return &___U3CCustomPropertiesU3Ek__BackingField_6; }
	inline void set_U3CCustomPropertiesU3Ek__BackingField_6(Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * value)
	{
		___U3CCustomPropertiesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCustomPropertiesU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_TagObject_7() { return static_cast<int32_t>(offsetof(Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202, ___TagObject_7)); }
	inline RuntimeObject * get_TagObject_7() const { return ___TagObject_7; }
	inline RuntimeObject ** get_address_of_TagObject_7() { return &___TagObject_7; }
	inline void set_TagObject_7(RuntimeObject * value)
	{
		___TagObject_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TagObject_7), (void*)value);
	}
};


// Photon.Realtime.RoomInfo
struct  RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2  : public RuntimeObject
{
public:
	// System.Boolean Photon.Realtime.RoomInfo::RemovedFromList
	bool ___RemovedFromList_0;
	// ExitGames.Client.Photon.Hashtable Photon.Realtime.RoomInfo::customProperties
	Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * ___customProperties_1;
	// System.Byte Photon.Realtime.RoomInfo::maxPlayers
	uint8_t ___maxPlayers_2;
	// System.Int32 Photon.Realtime.RoomInfo::emptyRoomTtl
	int32_t ___emptyRoomTtl_3;
	// System.Int32 Photon.Realtime.RoomInfo::playerTtl
	int32_t ___playerTtl_4;
	// System.String[] Photon.Realtime.RoomInfo::expectedUsers
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___expectedUsers_5;
	// System.Boolean Photon.Realtime.RoomInfo::isOpen
	bool ___isOpen_6;
	// System.Boolean Photon.Realtime.RoomInfo::isVisible
	bool ___isVisible_7;
	// System.Boolean Photon.Realtime.RoomInfo::autoCleanUp
	bool ___autoCleanUp_8;
	// System.String Photon.Realtime.RoomInfo::name
	String_t* ___name_9;
	// System.Int32 Photon.Realtime.RoomInfo::masterClientId
	int32_t ___masterClientId_10;
	// System.String[] Photon.Realtime.RoomInfo::propertiesListedInLobby
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___propertiesListedInLobby_11;
	// System.Int32 Photon.Realtime.RoomInfo::<PlayerCount>k__BackingField
	int32_t ___U3CPlayerCountU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_RemovedFromList_0() { return static_cast<int32_t>(offsetof(RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2, ___RemovedFromList_0)); }
	inline bool get_RemovedFromList_0() const { return ___RemovedFromList_0; }
	inline bool* get_address_of_RemovedFromList_0() { return &___RemovedFromList_0; }
	inline void set_RemovedFromList_0(bool value)
	{
		___RemovedFromList_0 = value;
	}

	inline static int32_t get_offset_of_customProperties_1() { return static_cast<int32_t>(offsetof(RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2, ___customProperties_1)); }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * get_customProperties_1() const { return ___customProperties_1; }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD ** get_address_of_customProperties_1() { return &___customProperties_1; }
	inline void set_customProperties_1(Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * value)
	{
		___customProperties_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customProperties_1), (void*)value);
	}

	inline static int32_t get_offset_of_maxPlayers_2() { return static_cast<int32_t>(offsetof(RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2, ___maxPlayers_2)); }
	inline uint8_t get_maxPlayers_2() const { return ___maxPlayers_2; }
	inline uint8_t* get_address_of_maxPlayers_2() { return &___maxPlayers_2; }
	inline void set_maxPlayers_2(uint8_t value)
	{
		___maxPlayers_2 = value;
	}

	inline static int32_t get_offset_of_emptyRoomTtl_3() { return static_cast<int32_t>(offsetof(RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2, ___emptyRoomTtl_3)); }
	inline int32_t get_emptyRoomTtl_3() const { return ___emptyRoomTtl_3; }
	inline int32_t* get_address_of_emptyRoomTtl_3() { return &___emptyRoomTtl_3; }
	inline void set_emptyRoomTtl_3(int32_t value)
	{
		___emptyRoomTtl_3 = value;
	}

	inline static int32_t get_offset_of_playerTtl_4() { return static_cast<int32_t>(offsetof(RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2, ___playerTtl_4)); }
	inline int32_t get_playerTtl_4() const { return ___playerTtl_4; }
	inline int32_t* get_address_of_playerTtl_4() { return &___playerTtl_4; }
	inline void set_playerTtl_4(int32_t value)
	{
		___playerTtl_4 = value;
	}

	inline static int32_t get_offset_of_expectedUsers_5() { return static_cast<int32_t>(offsetof(RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2, ___expectedUsers_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_expectedUsers_5() const { return ___expectedUsers_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_expectedUsers_5() { return &___expectedUsers_5; }
	inline void set_expectedUsers_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___expectedUsers_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___expectedUsers_5), (void*)value);
	}

	inline static int32_t get_offset_of_isOpen_6() { return static_cast<int32_t>(offsetof(RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2, ___isOpen_6)); }
	inline bool get_isOpen_6() const { return ___isOpen_6; }
	inline bool* get_address_of_isOpen_6() { return &___isOpen_6; }
	inline void set_isOpen_6(bool value)
	{
		___isOpen_6 = value;
	}

	inline static int32_t get_offset_of_isVisible_7() { return static_cast<int32_t>(offsetof(RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2, ___isVisible_7)); }
	inline bool get_isVisible_7() const { return ___isVisible_7; }
	inline bool* get_address_of_isVisible_7() { return &___isVisible_7; }
	inline void set_isVisible_7(bool value)
	{
		___isVisible_7 = value;
	}

	inline static int32_t get_offset_of_autoCleanUp_8() { return static_cast<int32_t>(offsetof(RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2, ___autoCleanUp_8)); }
	inline bool get_autoCleanUp_8() const { return ___autoCleanUp_8; }
	inline bool* get_address_of_autoCleanUp_8() { return &___autoCleanUp_8; }
	inline void set_autoCleanUp_8(bool value)
	{
		___autoCleanUp_8 = value;
	}

	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_9), (void*)value);
	}

	inline static int32_t get_offset_of_masterClientId_10() { return static_cast<int32_t>(offsetof(RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2, ___masterClientId_10)); }
	inline int32_t get_masterClientId_10() const { return ___masterClientId_10; }
	inline int32_t* get_address_of_masterClientId_10() { return &___masterClientId_10; }
	inline void set_masterClientId_10(int32_t value)
	{
		___masterClientId_10 = value;
	}

	inline static int32_t get_offset_of_propertiesListedInLobby_11() { return static_cast<int32_t>(offsetof(RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2, ___propertiesListedInLobby_11)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_propertiesListedInLobby_11() const { return ___propertiesListedInLobby_11; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_propertiesListedInLobby_11() { return &___propertiesListedInLobby_11; }
	inline void set_propertiesListedInLobby_11(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___propertiesListedInLobby_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___propertiesListedInLobby_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPlayerCountU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2, ___U3CPlayerCountU3Ek__BackingField_12)); }
	inline int32_t get_U3CPlayerCountU3Ek__BackingField_12() const { return ___U3CPlayerCountU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CPlayerCountU3Ek__BackingField_12() { return &___U3CPlayerCountU3Ek__BackingField_12; }
	inline void set_U3CPlayerCountU3Ek__BackingField_12(int32_t value)
	{
		___U3CPlayerCountU3Ek__BackingField_12 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};


// System.Collections.Generic.Dictionary`2<System.Reflection.MethodInfo,System.Reflection.ParameterInfo[]>
struct  Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tABD16668BF5C8B530E4B5798AD5A0E2FF4952344* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t8EAE43025C480B5764B3067A593EDE842E953A42 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tF900C762BB887C6C65419A8B1998E671E7A42C44 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF, ___entries_1)); }
	inline EntryU5BU5D_tABD16668BF5C8B530E4B5798AD5A0E2FF4952344* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tABD16668BF5C8B530E4B5798AD5A0E2FF4952344** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tABD16668BF5C8B530E4B5798AD5A0E2FF4952344* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF, ___keys_7)); }
	inline KeyCollection_t8EAE43025C480B5764B3067A593EDE842E953A42 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t8EAE43025C480B5764B3067A593EDE842E953A42 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t8EAE43025C480B5764B3067A593EDE842E953A42 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF, ___values_8)); }
	inline ValueCollection_tF900C762BB887C6C65419A8B1998E671E7A42C44 * get_values_8() const { return ___values_8; }
	inline ValueCollection_tF900C762BB887C6C65419A8B1998E671E7A42C44 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tF900C762BB887C6C65419A8B1998E671E7A42C44 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.String>
struct  List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____items_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Component>
struct  List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300, ____items_1)); }
	inline ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* get__items_1() const { return ____items_1; }
	inline ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300_StaticFields, ____emptyArray_5)); }
	inline ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.Queue`1<UnityEngine.Vector3>
struct  Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_4;
	// System.Object System.Collections.Generic.Queue`1::_syncRoot
	RuntimeObject * ____syncRoot_5;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE, ____array_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__array_0() const { return ____array_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__tail_2() { return static_cast<int32_t>(offsetof(Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE, ____tail_2)); }
	inline int32_t get__tail_2() const { return ____tail_2; }
	inline int32_t* get_address_of__tail_2() { return &____tail_2; }
	inline void set__tail_2(int32_t value)
	{
		____tail_2 = value;
	}

	inline static int32_t get_offset_of__size_3() { return static_cast<int32_t>(offsetof(Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE, ____size_3)); }
	inline int32_t get__size_3() const { return ____size_3; }
	inline int32_t* get_address_of__size_3() { return &____size_3; }
	inline void set__size_3(int32_t value)
	{
		____size_3 = value;
	}

	inline static int32_t get_offset_of__version_4() { return static_cast<int32_t>(offsetof(Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE, ____version_4)); }
	inline int32_t get__version_4() const { return ____version_4; }
	inline int32_t* get_address_of__version_4() { return &____version_4; }
	inline void set__version_4(int32_t value)
	{
		____version_4 = value;
	}

	inline static int32_t get_offset_of__syncRoot_5() { return static_cast<int32_t>(offsetof(Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE, ____syncRoot_5)); }
	inline RuntimeObject * get__syncRoot_5() const { return ____syncRoot_5; }
	inline RuntimeObject ** get_address_of__syncRoot_5() { return &____syncRoot_5; }
	inline void set__syncRoot_5(RuntimeObject * value)
	{
		____syncRoot_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_5), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// Photon.Pun.PhotonMessageInfo
struct  PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0 
{
public:
	// System.Int32 Photon.Pun.PhotonMessageInfo::timeInt
	int32_t ___timeInt_0;
	// Photon.Realtime.Player Photon.Pun.PhotonMessageInfo::Sender
	Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * ___Sender_1;
	// Photon.Pun.PhotonView Photon.Pun.PhotonMessageInfo::photonView
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___photonView_2;

public:
	inline static int32_t get_offset_of_timeInt_0() { return static_cast<int32_t>(offsetof(PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0, ___timeInt_0)); }
	inline int32_t get_timeInt_0() const { return ___timeInt_0; }
	inline int32_t* get_address_of_timeInt_0() { return &___timeInt_0; }
	inline void set_timeInt_0(int32_t value)
	{
		___timeInt_0 = value;
	}

	inline static int32_t get_offset_of_Sender_1() { return static_cast<int32_t>(offsetof(PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0, ___Sender_1)); }
	inline Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * get_Sender_1() const { return ___Sender_1; }
	inline Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 ** get_address_of_Sender_1() { return &___Sender_1; }
	inline void set_Sender_1(Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * value)
	{
		___Sender_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Sender_1), (void*)value);
	}

	inline static int32_t get_offset_of_photonView_2() { return static_cast<int32_t>(offsetof(PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0, ___photonView_2)); }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * get_photonView_2() const { return ___photonView_2; }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** get_address_of_photonView_2() { return &___photonView_2; }
	inline void set_photonView_2(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		___photonView_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___photonView_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Photon.Pun.PhotonMessageInfo
struct PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0_marshaled_pinvoke
{
	int32_t ___timeInt_0;
	Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * ___Sender_1;
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___photonView_2;
};
// Native definition for COM marshalling of Photon.Pun.PhotonMessageInfo
struct PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0_marshaled_com
{
	int32_t ___timeInt_0;
	Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * ___Sender_1;
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___photonView_2;
};

// Photon.Pun.PunRPC
struct  PunRPC_t49B358D1BE3E291A6F2FE4674A99AF7C8D2295DE  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// Photon.Realtime.Room
struct  Room_t5DFC39DD6736A2641374564EC6C31352BE33000D  : public RoomInfo_t2FA7C04D0FB706F0313E7C91AEA598D7B1BDEBE2
{
public:
	// Photon.Realtime.LoadBalancingClient Photon.Realtime.Room::<LoadBalancingClient>k__BackingField
	LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A * ___U3CLoadBalancingClientU3Ek__BackingField_13;
	// System.Boolean Photon.Realtime.Room::isOffline
	bool ___isOffline_14;
	// System.Collections.Generic.Dictionary`2<System.Int32,Photon.Realtime.Player> Photon.Realtime.Room::players
	Dictionary_2_tA8C1FE9FA1FB583BFC17676FCF9DFF7D606CB346 * ___players_15;

public:
	inline static int32_t get_offset_of_U3CLoadBalancingClientU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Room_t5DFC39DD6736A2641374564EC6C31352BE33000D, ___U3CLoadBalancingClientU3Ek__BackingField_13)); }
	inline LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A * get_U3CLoadBalancingClientU3Ek__BackingField_13() const { return ___U3CLoadBalancingClientU3Ek__BackingField_13; }
	inline LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A ** get_address_of_U3CLoadBalancingClientU3Ek__BackingField_13() { return &___U3CLoadBalancingClientU3Ek__BackingField_13; }
	inline void set_U3CLoadBalancingClientU3Ek__BackingField_13(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A * value)
	{
		___U3CLoadBalancingClientU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLoadBalancingClientU3Ek__BackingField_13), (void*)value);
	}

	inline static int32_t get_offset_of_isOffline_14() { return static_cast<int32_t>(offsetof(Room_t5DFC39DD6736A2641374564EC6C31352BE33000D, ___isOffline_14)); }
	inline bool get_isOffline_14() const { return ___isOffline_14; }
	inline bool* get_address_of_isOffline_14() { return &___isOffline_14; }
	inline void set_isOffline_14(bool value)
	{
		___isOffline_14 = value;
	}

	inline static int32_t get_offset_of_players_15() { return static_cast<int32_t>(offsetof(Room_t5DFC39DD6736A2641374564EC6C31352BE33000D, ___players_15)); }
	inline Dictionary_2_tA8C1FE9FA1FB583BFC17676FCF9DFF7D606CB346 * get_players_15() const { return ___players_15; }
	inline Dictionary_2_tA8C1FE9FA1FB583BFC17676FCF9DFF7D606CB346 ** get_address_of_players_15() { return &___players_15; }
	inline void set_players_15(Dictionary_2_tA8C1FE9FA1FB583BFC17676FCF9DFF7D606CB346 * value)
	{
		___players_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___players_15), (void*)value);
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rngAccess_12), (void*)value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rng_13), (void*)value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fastRng_14), (void*)value);
	}
};


// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Reflection.MethodBase
struct  MethodBase_t  : public MemberInfo_t
{
public:

public:
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.SceneManagement.Scene
struct  Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// ExitGames.Client.Photon.ConnectionProtocol
struct  ConnectionProtocol_tD5EF0C0704A1C2D3018B03EB4E1CF14B50BE822A 
{
public:
	// System.Byte ExitGames.Client.Photon.ConnectionProtocol::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectionProtocol_tD5EF0C0704A1C2D3018B03EB4E1CF14B50BE822A, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// ExitGames.Client.Photon.DebugLevel
struct  DebugLevel_t4B9C89FBF4A24849E166B74719D3674014C0B189 
{
public:
	// System.Byte ExitGames.Client.Photon.DebugLevel::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugLevel_t4B9C89FBF4A24849E166B74719D3674014C0B189, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// Photon.Pun.ConnectMethod
struct  ConnectMethod_tF523329BB0368506F7C6271AC6184A9F27E41308 
{
public:
	// System.Int32 Photon.Pun.ConnectMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectMethod_tF523329BB0368506F7C6271AC6184A9F27E41308, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Pun.OwnershipOption
struct  OwnershipOption_t45BCF7BD7E980B42D2C78A671E82AB78E3D73097 
{
public:
	// System.Int32 Photon.Pun.OwnershipOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OwnershipOption_t45BCF7BD7E980B42D2C78A671E82AB78E3D73097, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Pun.PhotonTransformViewPositionControl
struct  PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737  : public RuntimeObject
{
public:
	// Photon.Pun.PhotonTransformViewPositionModel Photon.Pun.PhotonTransformViewPositionControl::m_Model
	PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * ___m_Model_0;
	// System.Single Photon.Pun.PhotonTransformViewPositionControl::m_CurrentSpeed
	float ___m_CurrentSpeed_1;
	// System.Double Photon.Pun.PhotonTransformViewPositionControl::m_LastSerializeTime
	double ___m_LastSerializeTime_2;
	// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewPositionControl::m_SynchronizedSpeed
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_SynchronizedSpeed_3;
	// System.Single Photon.Pun.PhotonTransformViewPositionControl::m_SynchronizedTurnSpeed
	float ___m_SynchronizedTurnSpeed_4;
	// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewPositionControl::m_NetworkPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_NetworkPosition_5;
	// System.Collections.Generic.Queue`1<UnityEngine.Vector3> Photon.Pun.PhotonTransformViewPositionControl::m_OldNetworkPositions
	Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * ___m_OldNetworkPositions_6;
	// System.Boolean Photon.Pun.PhotonTransformViewPositionControl::m_UpdatedPositionAfterOnSerialize
	bool ___m_UpdatedPositionAfterOnSerialize_7;

public:
	inline static int32_t get_offset_of_m_Model_0() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737, ___m_Model_0)); }
	inline PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * get_m_Model_0() const { return ___m_Model_0; }
	inline PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 ** get_address_of_m_Model_0() { return &___m_Model_0; }
	inline void set_m_Model_0(PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * value)
	{
		___m_Model_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Model_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentSpeed_1() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737, ___m_CurrentSpeed_1)); }
	inline float get_m_CurrentSpeed_1() const { return ___m_CurrentSpeed_1; }
	inline float* get_address_of_m_CurrentSpeed_1() { return &___m_CurrentSpeed_1; }
	inline void set_m_CurrentSpeed_1(float value)
	{
		___m_CurrentSpeed_1 = value;
	}

	inline static int32_t get_offset_of_m_LastSerializeTime_2() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737, ___m_LastSerializeTime_2)); }
	inline double get_m_LastSerializeTime_2() const { return ___m_LastSerializeTime_2; }
	inline double* get_address_of_m_LastSerializeTime_2() { return &___m_LastSerializeTime_2; }
	inline void set_m_LastSerializeTime_2(double value)
	{
		___m_LastSerializeTime_2 = value;
	}

	inline static int32_t get_offset_of_m_SynchronizedSpeed_3() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737, ___m_SynchronizedSpeed_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_SynchronizedSpeed_3() const { return ___m_SynchronizedSpeed_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_SynchronizedSpeed_3() { return &___m_SynchronizedSpeed_3; }
	inline void set_m_SynchronizedSpeed_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_SynchronizedSpeed_3 = value;
	}

	inline static int32_t get_offset_of_m_SynchronizedTurnSpeed_4() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737, ___m_SynchronizedTurnSpeed_4)); }
	inline float get_m_SynchronizedTurnSpeed_4() const { return ___m_SynchronizedTurnSpeed_4; }
	inline float* get_address_of_m_SynchronizedTurnSpeed_4() { return &___m_SynchronizedTurnSpeed_4; }
	inline void set_m_SynchronizedTurnSpeed_4(float value)
	{
		___m_SynchronizedTurnSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_NetworkPosition_5() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737, ___m_NetworkPosition_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_NetworkPosition_5() const { return ___m_NetworkPosition_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_NetworkPosition_5() { return &___m_NetworkPosition_5; }
	inline void set_m_NetworkPosition_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_NetworkPosition_5 = value;
	}

	inline static int32_t get_offset_of_m_OldNetworkPositions_6() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737, ___m_OldNetworkPositions_6)); }
	inline Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * get_m_OldNetworkPositions_6() const { return ___m_OldNetworkPositions_6; }
	inline Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE ** get_address_of_m_OldNetworkPositions_6() { return &___m_OldNetworkPositions_6; }
	inline void set_m_OldNetworkPositions_6(Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * value)
	{
		___m_OldNetworkPositions_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OldNetworkPositions_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_UpdatedPositionAfterOnSerialize_7() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737, ___m_UpdatedPositionAfterOnSerialize_7)); }
	inline bool get_m_UpdatedPositionAfterOnSerialize_7() const { return ___m_UpdatedPositionAfterOnSerialize_7; }
	inline bool* get_address_of_m_UpdatedPositionAfterOnSerialize_7() { return &___m_UpdatedPositionAfterOnSerialize_7; }
	inline void set_m_UpdatedPositionAfterOnSerialize_7(bool value)
	{
		___m_UpdatedPositionAfterOnSerialize_7 = value;
	}
};


// Photon.Pun.PhotonTransformViewPositionModel_ExtrapolateOptions
struct  ExtrapolateOptions_t714A4CC1137D3C95AD12CB3DA7336FBA5A2C227E 
{
public:
	// System.Int32 Photon.Pun.PhotonTransformViewPositionModel_ExtrapolateOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExtrapolateOptions_t714A4CC1137D3C95AD12CB3DA7336FBA5A2C227E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Pun.PhotonTransformViewPositionModel_InterpolateOptions
struct  InterpolateOptions_tCA3E9AD4922712B6F182925A8908606FBD09460F 
{
public:
	// System.Int32 Photon.Pun.PhotonTransformViewPositionModel_InterpolateOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterpolateOptions_tCA3E9AD4922712B6F182925A8908606FBD09460F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Pun.PhotonTransformViewRotationControl
struct  PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE  : public RuntimeObject
{
public:
	// Photon.Pun.PhotonTransformViewRotationModel Photon.Pun.PhotonTransformViewRotationControl::m_Model
	PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * ___m_Model_0;
	// UnityEngine.Quaternion Photon.Pun.PhotonTransformViewRotationControl::m_NetworkRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_NetworkRotation_1;

public:
	inline static int32_t get_offset_of_m_Model_0() { return static_cast<int32_t>(offsetof(PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE, ___m_Model_0)); }
	inline PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * get_m_Model_0() const { return ___m_Model_0; }
	inline PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D ** get_address_of_m_Model_0() { return &___m_Model_0; }
	inline void set_m_Model_0(PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * value)
	{
		___m_Model_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Model_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_NetworkRotation_1() { return static_cast<int32_t>(offsetof(PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE, ___m_NetworkRotation_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_NetworkRotation_1() const { return ___m_NetworkRotation_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_NetworkRotation_1() { return &___m_NetworkRotation_1; }
	inline void set_m_NetworkRotation_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_NetworkRotation_1 = value;
	}
};


// Photon.Pun.PhotonTransformViewRotationModel_InterpolateOptions
struct  InterpolateOptions_t582D3B2023DDEB2ADE071AD48F9DBC8AC9E059EF 
{
public:
	// System.Int32 Photon.Pun.PhotonTransformViewRotationModel_InterpolateOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterpolateOptions_t582D3B2023DDEB2ADE071AD48F9DBC8AC9E059EF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Pun.PhotonTransformViewScaleControl
struct  PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E  : public RuntimeObject
{
public:
	// Photon.Pun.PhotonTransformViewScaleModel Photon.Pun.PhotonTransformViewScaleControl::m_Model
	PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * ___m_Model_0;
	// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewScaleControl::m_NetworkScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_NetworkScale_1;

public:
	inline static int32_t get_offset_of_m_Model_0() { return static_cast<int32_t>(offsetof(PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E, ___m_Model_0)); }
	inline PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * get_m_Model_0() const { return ___m_Model_0; }
	inline PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 ** get_address_of_m_Model_0() { return &___m_Model_0; }
	inline void set_m_Model_0(PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * value)
	{
		___m_Model_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Model_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_NetworkScale_1() { return static_cast<int32_t>(offsetof(PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E, ___m_NetworkScale_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_NetworkScale_1() const { return ___m_NetworkScale_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_NetworkScale_1() { return &___m_NetworkScale_1; }
	inline void set_m_NetworkScale_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_NetworkScale_1 = value;
	}
};


// Photon.Pun.PhotonTransformViewScaleModel_InterpolateOptions
struct  InterpolateOptions_t541EDCC0FB94D3BA7C92FEAF570CCD9A8359FAE3 
{
public:
	// System.Int32 Photon.Pun.PhotonTransformViewScaleModel_InterpolateOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterpolateOptions_t541EDCC0FB94D3BA7C92FEAF570CCD9A8359FAE3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Pun.PunLogLevel
struct  PunLogLevel_t4A6EF1499A00201A7593ACEF6CC7D36C865D0583 
{
public:
	// System.Int32 Photon.Pun.PunLogLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PunLogLevel_t4A6EF1499A00201A7593ACEF6CC7D36C865D0583, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Pun.RpcTarget
struct  RpcTarget_tE30C1D8C1A41907B5F74531A47D12542724FD023 
{
public:
	// System.Int32 Photon.Pun.RpcTarget::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RpcTarget_tE30C1D8C1A41907B5F74531A47D12542724FD023, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Pun.ViewSynchronization
struct  ViewSynchronization_tD026A6A289A4BF5263BF25B0EF79AB614E93D08B 
{
public:
	// System.Int32 Photon.Pun.ViewSynchronization::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ViewSynchronization_tD026A6A289A4BF5263BF25B0EF79AB614E93D08B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Realtime.AuthModeOption
struct  AuthModeOption_tD09E4DA95C4EC0B4CC7EFC735D512F177EC4F829 
{
public:
	// System.Int32 Photon.Realtime.AuthModeOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AuthModeOption_tD09E4DA95C4EC0B4CC7EFC735D512F177EC4F829, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Realtime.ClientState
struct  ClientState_tB545F72ECD3CB392A4C335FACA31D158A7EE4282 
{
public:
	// System.Int32 Photon.Realtime.ClientState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClientState_tB545F72ECD3CB392A4C335FACA31D158A7EE4282, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Realtime.DisconnectCause
struct  DisconnectCause_t8A969EFEB0FE2E4ABD5824250D4C9F7941F2650F 
{
public:
	// System.Int32 Photon.Realtime.DisconnectCause::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DisconnectCause_t8A969EFEB0FE2E4ABD5824250D4C9F7941F2650F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Realtime.EncryptionMode
struct  EncryptionMode_t5BA959815A4DD1ABC4458E9951E81CB988CBE1F3 
{
public:
	// System.Int32 Photon.Realtime.EncryptionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EncryptionMode_t5BA959815A4DD1ABC4458E9951E81CB988CBE1F3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Realtime.JoinType
struct  JoinType_t7BE6E5A9E95DEC68F63C8255504A5B53342028E0 
{
public:
	// System.Int32 Photon.Realtime.JoinType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JoinType_t7BE6E5A9E95DEC68F63C8255504A5B53342028E0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Realtime.ServerConnection
struct  ServerConnection_t5E49560AE3E1B85523C39B5EAB7EA09FE5363F68 
{
public:
	// System.Int32 Photon.Realtime.ServerConnection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ServerConnection_t5E49560AE3E1B85523C39B5EAB7EA09FE5363F68, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t
{
public:

public:
};


// System.Reflection.ParameterAttributes
struct  ParameterAttributes_tF9962395513C2A48CF5AF2F371C66DD52789F110 
{
public:
	// System.Int32 System.Reflection.ParameterAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParameterAttributes_tF9962395513C2A48CF5AF2F371C66DD52789F110, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Photon.Pun.PhotonNetwork
struct  PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A  : public RuntimeObject
{
public:

public:
};

struct PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields
{
public:
	// System.String Photon.Pun.PhotonNetwork::gameVersion
	String_t* ___gameVersion_1;
	// Photon.Realtime.LoadBalancingClient Photon.Pun.PhotonNetwork::NetworkingClient
	LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A * ___NetworkingClient_2;
	// System.Int32 Photon.Pun.PhotonNetwork::MAX_VIEW_IDS
	int32_t ___MAX_VIEW_IDS_3;
	// Photon.Pun.ServerSettings Photon.Pun.PhotonNetwork::photonServerSettings
	ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9 * ___photonServerSettings_5;
	// Photon.Pun.ConnectMethod Photon.Pun.PhotonNetwork::ConnectMethod
	int32_t ___ConnectMethod_7;
	// Photon.Pun.PunLogLevel Photon.Pun.PhotonNetwork::LogLevel
	int32_t ___LogLevel_8;
	// System.Single Photon.Pun.PhotonNetwork::PrecisionForVectorSynchronization
	float ___PrecisionForVectorSynchronization_9;
	// System.Single Photon.Pun.PhotonNetwork::PrecisionForQuaternionSynchronization
	float ___PrecisionForQuaternionSynchronization_10;
	// System.Single Photon.Pun.PhotonNetwork::PrecisionForFloatSynchronization
	float ___PrecisionForFloatSynchronization_11;
	// System.Boolean Photon.Pun.PhotonNetwork::offlineMode
	bool ___offlineMode_12;
	// Photon.Realtime.Room Photon.Pun.PhotonNetwork::offlineModeRoom
	Room_t5DFC39DD6736A2641374564EC6C31352BE33000D * ___offlineModeRoom_13;
	// System.Boolean Photon.Pun.PhotonNetwork::automaticallySyncScene
	bool ___automaticallySyncScene_14;
	// System.Int32 Photon.Pun.PhotonNetwork::sendFrequency
	int32_t ___sendFrequency_15;
	// System.Int32 Photon.Pun.PhotonNetwork::serializationFrequency
	int32_t ___serializationFrequency_16;
	// System.Boolean Photon.Pun.PhotonNetwork::isMessageQueueRunning
	bool ___isMessageQueueRunning_17;
	// System.Double Photon.Pun.PhotonNetwork::frametime
	double ___frametime_18;
	// System.Int32 Photon.Pun.PhotonNetwork::frame
	int32_t ___frame_19;
	// System.Diagnostics.Stopwatch Photon.Pun.PhotonNetwork::StartupStopwatch
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * ___StartupStopwatch_20;
	// System.Single Photon.Pun.PhotonNetwork::MinimalTimeScaleToDispatchInFixedUpdate
	float ___MinimalTimeScaleToDispatchInFixedUpdate_21;
	// System.Int32 Photon.Pun.PhotonNetwork::lastUsedViewSubId
	int32_t ___lastUsedViewSubId_22;
	// System.Int32 Photon.Pun.PhotonNetwork::lastUsedViewSubIdStatic
	int32_t ___lastUsedViewSubIdStatic_23;
	// System.Collections.Generic.HashSet`1<System.String> Photon.Pun.PhotonNetwork::PrefabsWithoutMagicCallback
	HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * ___PrefabsWithoutMagicCallback_24;
	// ExitGames.Client.Photon.Hashtable Photon.Pun.PhotonNetwork::SendInstantiateEvHashtable
	Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * ___SendInstantiateEvHashtable_25;
	// Photon.Realtime.RaiseEventOptions Photon.Pun.PhotonNetwork::SendInstantiateRaiseEventOptions
	RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * ___SendInstantiateRaiseEventOptions_26;
	// System.Collections.Generic.HashSet`1<System.Byte> Photon.Pun.PhotonNetwork::allowedReceivingGroups
	HashSet_1_tE84C3FCD7322E7212DC4A4B67223007DA245B0F3 * ___allowedReceivingGroups_27;
	// System.Collections.Generic.HashSet`1<System.Byte> Photon.Pun.PhotonNetwork::blockedSendingGroups
	HashSet_1_tE84C3FCD7322E7212DC4A4B67223007DA245B0F3 * ___blockedSendingGroups_28;
	// System.Collections.Generic.Dictionary`2<System.Int32,Photon.Pun.PhotonView> Photon.Pun.PhotonNetwork::photonViewList
	Dictionary_2_t24B47C5EBEBE3CBC1A517656D2D74B343EF88745 * ___photonViewList_29;
	// System.Action`2<Photon.Pun.PhotonView,Photon.Realtime.Player> Photon.Pun.PhotonNetwork::OnOwnershipRequestEv
	Action_2_t0A813634D13A25893BE13AF0CCCD116E584CC029 * ___OnOwnershipRequestEv_30;
	// System.Action`2<Photon.Pun.PhotonView,Photon.Realtime.Player> Photon.Pun.PhotonNetwork::OnOwnershipTransferedEv
	Action_2_t0A813634D13A25893BE13AF0CCCD116E584CC029 * ___OnOwnershipTransferedEv_31;
	// System.Byte Photon.Pun.PhotonNetwork::currentLevelPrefix
	uint8_t ___currentLevelPrefix_32;
	// System.Boolean Photon.Pun.PhotonNetwork::loadingLevelAndPausedNetwork
	bool ___loadingLevelAndPausedNetwork_33;
	// Photon.Pun.IPunPrefabPool Photon.Pun.PhotonNetwork::prefabPool
	RuntimeObject* ___prefabPool_36;
	// System.Boolean Photon.Pun.PhotonNetwork::UseRpcMonoBehaviourCache
	bool ___UseRpcMonoBehaviourCache_37;
	// System.Boolean Photon.Pun.PhotonNetwork::RunRpcCoroutines
	bool ___RunRpcCoroutines_38;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.List`1<System.Reflection.MethodInfo>> Photon.Pun.PhotonNetwork::monoRPCMethodsCache
	Dictionary_2_tAF6372C97005A35CF864BC1B4782B28D1D7F0974 * ___monoRPCMethodsCache_39;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Photon.Pun.PhotonNetwork::rpcShortcuts
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___rpcShortcuts_40;
	// UnityEngine.AsyncOperation Photon.Pun.PhotonNetwork::_AsyncLevelLoadingOperation
	AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * ____AsyncLevelLoadingOperation_41;
	// System.Single Photon.Pun.PhotonNetwork::_levelLoadingProgress
	float ____levelLoadingProgress_42;
	// System.Type Photon.Pun.PhotonNetwork::typePunRPC
	Type_t * ___typePunRPC_43;
	// System.Type Photon.Pun.PhotonNetwork::typePhotonMessageInfo
	Type_t * ___typePhotonMessageInfo_44;
	// System.Object Photon.Pun.PhotonNetwork::keyByteZero
	RuntimeObject * ___keyByteZero_45;
	// System.Object Photon.Pun.PhotonNetwork::keyByteOne
	RuntimeObject * ___keyByteOne_46;
	// System.Object Photon.Pun.PhotonNetwork::keyByteTwo
	RuntimeObject * ___keyByteTwo_47;
	// System.Object Photon.Pun.PhotonNetwork::keyByteThree
	RuntimeObject * ___keyByteThree_48;
	// System.Object Photon.Pun.PhotonNetwork::keyByteFour
	RuntimeObject * ___keyByteFour_49;
	// System.Object Photon.Pun.PhotonNetwork::keyByteFive
	RuntimeObject * ___keyByteFive_50;
	// System.Object Photon.Pun.PhotonNetwork::keyByteSix
	RuntimeObject * ___keyByteSix_51;
	// System.Object Photon.Pun.PhotonNetwork::keyByteSeven
	RuntimeObject * ___keyByteSeven_52;
	// System.Object Photon.Pun.PhotonNetwork::keyByteEight
	RuntimeObject * ___keyByteEight_53;
	// System.Object[] Photon.Pun.PhotonNetwork::emptyObjectArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___emptyObjectArray_54;
	// System.Type[] Photon.Pun.PhotonNetwork::emptyTypeArray
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___emptyTypeArray_55;
	// ExitGames.Client.Photon.Hashtable Photon.Pun.PhotonNetwork::removeFilter
	Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * ___removeFilter_56;
	// ExitGames.Client.Photon.Hashtable Photon.Pun.PhotonNetwork::ServerCleanDestroyEvent
	Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * ___ServerCleanDestroyEvent_57;
	// Photon.Realtime.RaiseEventOptions Photon.Pun.PhotonNetwork::ServerCleanOptions
	RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * ___ServerCleanOptions_58;
	// ExitGames.Client.Photon.Hashtable Photon.Pun.PhotonNetwork::rpcFilterByViewId
	Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * ___rpcFilterByViewId_59;
	// Photon.Realtime.RaiseEventOptions Photon.Pun.PhotonNetwork::OpCleanRpcBufferOptions
	RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * ___OpCleanRpcBufferOptions_60;
	// ExitGames.Client.Photon.Hashtable Photon.Pun.PhotonNetwork::rpcEvent
	Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * ___rpcEvent_61;
	// Photon.Realtime.RaiseEventOptions Photon.Pun.PhotonNetwork::RpcOptionsToAll
	RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * ___RpcOptionsToAll_62;
	// System.Int32 Photon.Pun.PhotonNetwork::ObjectsInOneUpdate
	int32_t ___ObjectsInOneUpdate_63;
	// Photon.Pun.PhotonStream Photon.Pun.PhotonNetwork::serializeStreamOut
	PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___serializeStreamOut_64;
	// Photon.Pun.PhotonStream Photon.Pun.PhotonNetwork::serializeStreamIn
	PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___serializeStreamIn_65;
	// Photon.Realtime.RaiseEventOptions Photon.Pun.PhotonNetwork::serializeRaiseEvOptions
	RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * ___serializeRaiseEvOptions_66;
	// System.Collections.Generic.Dictionary`2<Photon.Pun.PhotonNetwork_RaiseEventBatch,Photon.Pun.PhotonNetwork_SerializeViewBatch> Photon.Pun.PhotonNetwork::serializeViewBatches
	Dictionary_2_t814656FD5269C0EAD336588E42742ED4E8A7AA30 * ___serializeViewBatches_67;
	// Photon.Realtime.RegionHandler Photon.Pun.PhotonNetwork::_cachedRegionHandler
	RegionHandler_tE1C559FF2DCCA020EC31ACCF3993A434617A15AF * ____cachedRegionHandler_72;

public:
	inline static int32_t get_offset_of_gameVersion_1() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___gameVersion_1)); }
	inline String_t* get_gameVersion_1() const { return ___gameVersion_1; }
	inline String_t** get_address_of_gameVersion_1() { return &___gameVersion_1; }
	inline void set_gameVersion_1(String_t* value)
	{
		___gameVersion_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameVersion_1), (void*)value);
	}

	inline static int32_t get_offset_of_NetworkingClient_2() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___NetworkingClient_2)); }
	inline LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A * get_NetworkingClient_2() const { return ___NetworkingClient_2; }
	inline LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A ** get_address_of_NetworkingClient_2() { return &___NetworkingClient_2; }
	inline void set_NetworkingClient_2(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A * value)
	{
		___NetworkingClient_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NetworkingClient_2), (void*)value);
	}

	inline static int32_t get_offset_of_MAX_VIEW_IDS_3() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___MAX_VIEW_IDS_3)); }
	inline int32_t get_MAX_VIEW_IDS_3() const { return ___MAX_VIEW_IDS_3; }
	inline int32_t* get_address_of_MAX_VIEW_IDS_3() { return &___MAX_VIEW_IDS_3; }
	inline void set_MAX_VIEW_IDS_3(int32_t value)
	{
		___MAX_VIEW_IDS_3 = value;
	}

	inline static int32_t get_offset_of_photonServerSettings_5() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___photonServerSettings_5)); }
	inline ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9 * get_photonServerSettings_5() const { return ___photonServerSettings_5; }
	inline ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9 ** get_address_of_photonServerSettings_5() { return &___photonServerSettings_5; }
	inline void set_photonServerSettings_5(ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9 * value)
	{
		___photonServerSettings_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___photonServerSettings_5), (void*)value);
	}

	inline static int32_t get_offset_of_ConnectMethod_7() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___ConnectMethod_7)); }
	inline int32_t get_ConnectMethod_7() const { return ___ConnectMethod_7; }
	inline int32_t* get_address_of_ConnectMethod_7() { return &___ConnectMethod_7; }
	inline void set_ConnectMethod_7(int32_t value)
	{
		___ConnectMethod_7 = value;
	}

	inline static int32_t get_offset_of_LogLevel_8() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___LogLevel_8)); }
	inline int32_t get_LogLevel_8() const { return ___LogLevel_8; }
	inline int32_t* get_address_of_LogLevel_8() { return &___LogLevel_8; }
	inline void set_LogLevel_8(int32_t value)
	{
		___LogLevel_8 = value;
	}

	inline static int32_t get_offset_of_PrecisionForVectorSynchronization_9() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___PrecisionForVectorSynchronization_9)); }
	inline float get_PrecisionForVectorSynchronization_9() const { return ___PrecisionForVectorSynchronization_9; }
	inline float* get_address_of_PrecisionForVectorSynchronization_9() { return &___PrecisionForVectorSynchronization_9; }
	inline void set_PrecisionForVectorSynchronization_9(float value)
	{
		___PrecisionForVectorSynchronization_9 = value;
	}

	inline static int32_t get_offset_of_PrecisionForQuaternionSynchronization_10() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___PrecisionForQuaternionSynchronization_10)); }
	inline float get_PrecisionForQuaternionSynchronization_10() const { return ___PrecisionForQuaternionSynchronization_10; }
	inline float* get_address_of_PrecisionForQuaternionSynchronization_10() { return &___PrecisionForQuaternionSynchronization_10; }
	inline void set_PrecisionForQuaternionSynchronization_10(float value)
	{
		___PrecisionForQuaternionSynchronization_10 = value;
	}

	inline static int32_t get_offset_of_PrecisionForFloatSynchronization_11() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___PrecisionForFloatSynchronization_11)); }
	inline float get_PrecisionForFloatSynchronization_11() const { return ___PrecisionForFloatSynchronization_11; }
	inline float* get_address_of_PrecisionForFloatSynchronization_11() { return &___PrecisionForFloatSynchronization_11; }
	inline void set_PrecisionForFloatSynchronization_11(float value)
	{
		___PrecisionForFloatSynchronization_11 = value;
	}

	inline static int32_t get_offset_of_offlineMode_12() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___offlineMode_12)); }
	inline bool get_offlineMode_12() const { return ___offlineMode_12; }
	inline bool* get_address_of_offlineMode_12() { return &___offlineMode_12; }
	inline void set_offlineMode_12(bool value)
	{
		___offlineMode_12 = value;
	}

	inline static int32_t get_offset_of_offlineModeRoom_13() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___offlineModeRoom_13)); }
	inline Room_t5DFC39DD6736A2641374564EC6C31352BE33000D * get_offlineModeRoom_13() const { return ___offlineModeRoom_13; }
	inline Room_t5DFC39DD6736A2641374564EC6C31352BE33000D ** get_address_of_offlineModeRoom_13() { return &___offlineModeRoom_13; }
	inline void set_offlineModeRoom_13(Room_t5DFC39DD6736A2641374564EC6C31352BE33000D * value)
	{
		___offlineModeRoom_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___offlineModeRoom_13), (void*)value);
	}

	inline static int32_t get_offset_of_automaticallySyncScene_14() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___automaticallySyncScene_14)); }
	inline bool get_automaticallySyncScene_14() const { return ___automaticallySyncScene_14; }
	inline bool* get_address_of_automaticallySyncScene_14() { return &___automaticallySyncScene_14; }
	inline void set_automaticallySyncScene_14(bool value)
	{
		___automaticallySyncScene_14 = value;
	}

	inline static int32_t get_offset_of_sendFrequency_15() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___sendFrequency_15)); }
	inline int32_t get_sendFrequency_15() const { return ___sendFrequency_15; }
	inline int32_t* get_address_of_sendFrequency_15() { return &___sendFrequency_15; }
	inline void set_sendFrequency_15(int32_t value)
	{
		___sendFrequency_15 = value;
	}

	inline static int32_t get_offset_of_serializationFrequency_16() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___serializationFrequency_16)); }
	inline int32_t get_serializationFrequency_16() const { return ___serializationFrequency_16; }
	inline int32_t* get_address_of_serializationFrequency_16() { return &___serializationFrequency_16; }
	inline void set_serializationFrequency_16(int32_t value)
	{
		___serializationFrequency_16 = value;
	}

	inline static int32_t get_offset_of_isMessageQueueRunning_17() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___isMessageQueueRunning_17)); }
	inline bool get_isMessageQueueRunning_17() const { return ___isMessageQueueRunning_17; }
	inline bool* get_address_of_isMessageQueueRunning_17() { return &___isMessageQueueRunning_17; }
	inline void set_isMessageQueueRunning_17(bool value)
	{
		___isMessageQueueRunning_17 = value;
	}

	inline static int32_t get_offset_of_frametime_18() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___frametime_18)); }
	inline double get_frametime_18() const { return ___frametime_18; }
	inline double* get_address_of_frametime_18() { return &___frametime_18; }
	inline void set_frametime_18(double value)
	{
		___frametime_18 = value;
	}

	inline static int32_t get_offset_of_frame_19() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___frame_19)); }
	inline int32_t get_frame_19() const { return ___frame_19; }
	inline int32_t* get_address_of_frame_19() { return &___frame_19; }
	inline void set_frame_19(int32_t value)
	{
		___frame_19 = value;
	}

	inline static int32_t get_offset_of_StartupStopwatch_20() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___StartupStopwatch_20)); }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * get_StartupStopwatch_20() const { return ___StartupStopwatch_20; }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 ** get_address_of_StartupStopwatch_20() { return &___StartupStopwatch_20; }
	inline void set_StartupStopwatch_20(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * value)
	{
		___StartupStopwatch_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StartupStopwatch_20), (void*)value);
	}

	inline static int32_t get_offset_of_MinimalTimeScaleToDispatchInFixedUpdate_21() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___MinimalTimeScaleToDispatchInFixedUpdate_21)); }
	inline float get_MinimalTimeScaleToDispatchInFixedUpdate_21() const { return ___MinimalTimeScaleToDispatchInFixedUpdate_21; }
	inline float* get_address_of_MinimalTimeScaleToDispatchInFixedUpdate_21() { return &___MinimalTimeScaleToDispatchInFixedUpdate_21; }
	inline void set_MinimalTimeScaleToDispatchInFixedUpdate_21(float value)
	{
		___MinimalTimeScaleToDispatchInFixedUpdate_21 = value;
	}

	inline static int32_t get_offset_of_lastUsedViewSubId_22() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___lastUsedViewSubId_22)); }
	inline int32_t get_lastUsedViewSubId_22() const { return ___lastUsedViewSubId_22; }
	inline int32_t* get_address_of_lastUsedViewSubId_22() { return &___lastUsedViewSubId_22; }
	inline void set_lastUsedViewSubId_22(int32_t value)
	{
		___lastUsedViewSubId_22 = value;
	}

	inline static int32_t get_offset_of_lastUsedViewSubIdStatic_23() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___lastUsedViewSubIdStatic_23)); }
	inline int32_t get_lastUsedViewSubIdStatic_23() const { return ___lastUsedViewSubIdStatic_23; }
	inline int32_t* get_address_of_lastUsedViewSubIdStatic_23() { return &___lastUsedViewSubIdStatic_23; }
	inline void set_lastUsedViewSubIdStatic_23(int32_t value)
	{
		___lastUsedViewSubIdStatic_23 = value;
	}

	inline static int32_t get_offset_of_PrefabsWithoutMagicCallback_24() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___PrefabsWithoutMagicCallback_24)); }
	inline HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * get_PrefabsWithoutMagicCallback_24() const { return ___PrefabsWithoutMagicCallback_24; }
	inline HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 ** get_address_of_PrefabsWithoutMagicCallback_24() { return &___PrefabsWithoutMagicCallback_24; }
	inline void set_PrefabsWithoutMagicCallback_24(HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * value)
	{
		___PrefabsWithoutMagicCallback_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PrefabsWithoutMagicCallback_24), (void*)value);
	}

	inline static int32_t get_offset_of_SendInstantiateEvHashtable_25() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___SendInstantiateEvHashtable_25)); }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * get_SendInstantiateEvHashtable_25() const { return ___SendInstantiateEvHashtable_25; }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD ** get_address_of_SendInstantiateEvHashtable_25() { return &___SendInstantiateEvHashtable_25; }
	inline void set_SendInstantiateEvHashtable_25(Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * value)
	{
		___SendInstantiateEvHashtable_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SendInstantiateEvHashtable_25), (void*)value);
	}

	inline static int32_t get_offset_of_SendInstantiateRaiseEventOptions_26() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___SendInstantiateRaiseEventOptions_26)); }
	inline RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * get_SendInstantiateRaiseEventOptions_26() const { return ___SendInstantiateRaiseEventOptions_26; }
	inline RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED ** get_address_of_SendInstantiateRaiseEventOptions_26() { return &___SendInstantiateRaiseEventOptions_26; }
	inline void set_SendInstantiateRaiseEventOptions_26(RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * value)
	{
		___SendInstantiateRaiseEventOptions_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SendInstantiateRaiseEventOptions_26), (void*)value);
	}

	inline static int32_t get_offset_of_allowedReceivingGroups_27() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___allowedReceivingGroups_27)); }
	inline HashSet_1_tE84C3FCD7322E7212DC4A4B67223007DA245B0F3 * get_allowedReceivingGroups_27() const { return ___allowedReceivingGroups_27; }
	inline HashSet_1_tE84C3FCD7322E7212DC4A4B67223007DA245B0F3 ** get_address_of_allowedReceivingGroups_27() { return &___allowedReceivingGroups_27; }
	inline void set_allowedReceivingGroups_27(HashSet_1_tE84C3FCD7322E7212DC4A4B67223007DA245B0F3 * value)
	{
		___allowedReceivingGroups_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allowedReceivingGroups_27), (void*)value);
	}

	inline static int32_t get_offset_of_blockedSendingGroups_28() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___blockedSendingGroups_28)); }
	inline HashSet_1_tE84C3FCD7322E7212DC4A4B67223007DA245B0F3 * get_blockedSendingGroups_28() const { return ___blockedSendingGroups_28; }
	inline HashSet_1_tE84C3FCD7322E7212DC4A4B67223007DA245B0F3 ** get_address_of_blockedSendingGroups_28() { return &___blockedSendingGroups_28; }
	inline void set_blockedSendingGroups_28(HashSet_1_tE84C3FCD7322E7212DC4A4B67223007DA245B0F3 * value)
	{
		___blockedSendingGroups_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blockedSendingGroups_28), (void*)value);
	}

	inline static int32_t get_offset_of_photonViewList_29() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___photonViewList_29)); }
	inline Dictionary_2_t24B47C5EBEBE3CBC1A517656D2D74B343EF88745 * get_photonViewList_29() const { return ___photonViewList_29; }
	inline Dictionary_2_t24B47C5EBEBE3CBC1A517656D2D74B343EF88745 ** get_address_of_photonViewList_29() { return &___photonViewList_29; }
	inline void set_photonViewList_29(Dictionary_2_t24B47C5EBEBE3CBC1A517656D2D74B343EF88745 * value)
	{
		___photonViewList_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___photonViewList_29), (void*)value);
	}

	inline static int32_t get_offset_of_OnOwnershipRequestEv_30() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___OnOwnershipRequestEv_30)); }
	inline Action_2_t0A813634D13A25893BE13AF0CCCD116E584CC029 * get_OnOwnershipRequestEv_30() const { return ___OnOwnershipRequestEv_30; }
	inline Action_2_t0A813634D13A25893BE13AF0CCCD116E584CC029 ** get_address_of_OnOwnershipRequestEv_30() { return &___OnOwnershipRequestEv_30; }
	inline void set_OnOwnershipRequestEv_30(Action_2_t0A813634D13A25893BE13AF0CCCD116E584CC029 * value)
	{
		___OnOwnershipRequestEv_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnOwnershipRequestEv_30), (void*)value);
	}

	inline static int32_t get_offset_of_OnOwnershipTransferedEv_31() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___OnOwnershipTransferedEv_31)); }
	inline Action_2_t0A813634D13A25893BE13AF0CCCD116E584CC029 * get_OnOwnershipTransferedEv_31() const { return ___OnOwnershipTransferedEv_31; }
	inline Action_2_t0A813634D13A25893BE13AF0CCCD116E584CC029 ** get_address_of_OnOwnershipTransferedEv_31() { return &___OnOwnershipTransferedEv_31; }
	inline void set_OnOwnershipTransferedEv_31(Action_2_t0A813634D13A25893BE13AF0CCCD116E584CC029 * value)
	{
		___OnOwnershipTransferedEv_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnOwnershipTransferedEv_31), (void*)value);
	}

	inline static int32_t get_offset_of_currentLevelPrefix_32() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___currentLevelPrefix_32)); }
	inline uint8_t get_currentLevelPrefix_32() const { return ___currentLevelPrefix_32; }
	inline uint8_t* get_address_of_currentLevelPrefix_32() { return &___currentLevelPrefix_32; }
	inline void set_currentLevelPrefix_32(uint8_t value)
	{
		___currentLevelPrefix_32 = value;
	}

	inline static int32_t get_offset_of_loadingLevelAndPausedNetwork_33() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___loadingLevelAndPausedNetwork_33)); }
	inline bool get_loadingLevelAndPausedNetwork_33() const { return ___loadingLevelAndPausedNetwork_33; }
	inline bool* get_address_of_loadingLevelAndPausedNetwork_33() { return &___loadingLevelAndPausedNetwork_33; }
	inline void set_loadingLevelAndPausedNetwork_33(bool value)
	{
		___loadingLevelAndPausedNetwork_33 = value;
	}

	inline static int32_t get_offset_of_prefabPool_36() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___prefabPool_36)); }
	inline RuntimeObject* get_prefabPool_36() const { return ___prefabPool_36; }
	inline RuntimeObject** get_address_of_prefabPool_36() { return &___prefabPool_36; }
	inline void set_prefabPool_36(RuntimeObject* value)
	{
		___prefabPool_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefabPool_36), (void*)value);
	}

	inline static int32_t get_offset_of_UseRpcMonoBehaviourCache_37() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___UseRpcMonoBehaviourCache_37)); }
	inline bool get_UseRpcMonoBehaviourCache_37() const { return ___UseRpcMonoBehaviourCache_37; }
	inline bool* get_address_of_UseRpcMonoBehaviourCache_37() { return &___UseRpcMonoBehaviourCache_37; }
	inline void set_UseRpcMonoBehaviourCache_37(bool value)
	{
		___UseRpcMonoBehaviourCache_37 = value;
	}

	inline static int32_t get_offset_of_RunRpcCoroutines_38() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___RunRpcCoroutines_38)); }
	inline bool get_RunRpcCoroutines_38() const { return ___RunRpcCoroutines_38; }
	inline bool* get_address_of_RunRpcCoroutines_38() { return &___RunRpcCoroutines_38; }
	inline void set_RunRpcCoroutines_38(bool value)
	{
		___RunRpcCoroutines_38 = value;
	}

	inline static int32_t get_offset_of_monoRPCMethodsCache_39() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___monoRPCMethodsCache_39)); }
	inline Dictionary_2_tAF6372C97005A35CF864BC1B4782B28D1D7F0974 * get_monoRPCMethodsCache_39() const { return ___monoRPCMethodsCache_39; }
	inline Dictionary_2_tAF6372C97005A35CF864BC1B4782B28D1D7F0974 ** get_address_of_monoRPCMethodsCache_39() { return &___monoRPCMethodsCache_39; }
	inline void set_monoRPCMethodsCache_39(Dictionary_2_tAF6372C97005A35CF864BC1B4782B28D1D7F0974 * value)
	{
		___monoRPCMethodsCache_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___monoRPCMethodsCache_39), (void*)value);
	}

	inline static int32_t get_offset_of_rpcShortcuts_40() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___rpcShortcuts_40)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_rpcShortcuts_40() const { return ___rpcShortcuts_40; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_rpcShortcuts_40() { return &___rpcShortcuts_40; }
	inline void set_rpcShortcuts_40(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___rpcShortcuts_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rpcShortcuts_40), (void*)value);
	}

	inline static int32_t get_offset_of__AsyncLevelLoadingOperation_41() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ____AsyncLevelLoadingOperation_41)); }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * get__AsyncLevelLoadingOperation_41() const { return ____AsyncLevelLoadingOperation_41; }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D ** get_address_of__AsyncLevelLoadingOperation_41() { return &____AsyncLevelLoadingOperation_41; }
	inline void set__AsyncLevelLoadingOperation_41(AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * value)
	{
		____AsyncLevelLoadingOperation_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____AsyncLevelLoadingOperation_41), (void*)value);
	}

	inline static int32_t get_offset_of__levelLoadingProgress_42() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ____levelLoadingProgress_42)); }
	inline float get__levelLoadingProgress_42() const { return ____levelLoadingProgress_42; }
	inline float* get_address_of__levelLoadingProgress_42() { return &____levelLoadingProgress_42; }
	inline void set__levelLoadingProgress_42(float value)
	{
		____levelLoadingProgress_42 = value;
	}

	inline static int32_t get_offset_of_typePunRPC_43() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___typePunRPC_43)); }
	inline Type_t * get_typePunRPC_43() const { return ___typePunRPC_43; }
	inline Type_t ** get_address_of_typePunRPC_43() { return &___typePunRPC_43; }
	inline void set_typePunRPC_43(Type_t * value)
	{
		___typePunRPC_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typePunRPC_43), (void*)value);
	}

	inline static int32_t get_offset_of_typePhotonMessageInfo_44() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___typePhotonMessageInfo_44)); }
	inline Type_t * get_typePhotonMessageInfo_44() const { return ___typePhotonMessageInfo_44; }
	inline Type_t ** get_address_of_typePhotonMessageInfo_44() { return &___typePhotonMessageInfo_44; }
	inline void set_typePhotonMessageInfo_44(Type_t * value)
	{
		___typePhotonMessageInfo_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typePhotonMessageInfo_44), (void*)value);
	}

	inline static int32_t get_offset_of_keyByteZero_45() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___keyByteZero_45)); }
	inline RuntimeObject * get_keyByteZero_45() const { return ___keyByteZero_45; }
	inline RuntimeObject ** get_address_of_keyByteZero_45() { return &___keyByteZero_45; }
	inline void set_keyByteZero_45(RuntimeObject * value)
	{
		___keyByteZero_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyByteZero_45), (void*)value);
	}

	inline static int32_t get_offset_of_keyByteOne_46() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___keyByteOne_46)); }
	inline RuntimeObject * get_keyByteOne_46() const { return ___keyByteOne_46; }
	inline RuntimeObject ** get_address_of_keyByteOne_46() { return &___keyByteOne_46; }
	inline void set_keyByteOne_46(RuntimeObject * value)
	{
		___keyByteOne_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyByteOne_46), (void*)value);
	}

	inline static int32_t get_offset_of_keyByteTwo_47() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___keyByteTwo_47)); }
	inline RuntimeObject * get_keyByteTwo_47() const { return ___keyByteTwo_47; }
	inline RuntimeObject ** get_address_of_keyByteTwo_47() { return &___keyByteTwo_47; }
	inline void set_keyByteTwo_47(RuntimeObject * value)
	{
		___keyByteTwo_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyByteTwo_47), (void*)value);
	}

	inline static int32_t get_offset_of_keyByteThree_48() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___keyByteThree_48)); }
	inline RuntimeObject * get_keyByteThree_48() const { return ___keyByteThree_48; }
	inline RuntimeObject ** get_address_of_keyByteThree_48() { return &___keyByteThree_48; }
	inline void set_keyByteThree_48(RuntimeObject * value)
	{
		___keyByteThree_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyByteThree_48), (void*)value);
	}

	inline static int32_t get_offset_of_keyByteFour_49() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___keyByteFour_49)); }
	inline RuntimeObject * get_keyByteFour_49() const { return ___keyByteFour_49; }
	inline RuntimeObject ** get_address_of_keyByteFour_49() { return &___keyByteFour_49; }
	inline void set_keyByteFour_49(RuntimeObject * value)
	{
		___keyByteFour_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyByteFour_49), (void*)value);
	}

	inline static int32_t get_offset_of_keyByteFive_50() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___keyByteFive_50)); }
	inline RuntimeObject * get_keyByteFive_50() const { return ___keyByteFive_50; }
	inline RuntimeObject ** get_address_of_keyByteFive_50() { return &___keyByteFive_50; }
	inline void set_keyByteFive_50(RuntimeObject * value)
	{
		___keyByteFive_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyByteFive_50), (void*)value);
	}

	inline static int32_t get_offset_of_keyByteSix_51() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___keyByteSix_51)); }
	inline RuntimeObject * get_keyByteSix_51() const { return ___keyByteSix_51; }
	inline RuntimeObject ** get_address_of_keyByteSix_51() { return &___keyByteSix_51; }
	inline void set_keyByteSix_51(RuntimeObject * value)
	{
		___keyByteSix_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyByteSix_51), (void*)value);
	}

	inline static int32_t get_offset_of_keyByteSeven_52() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___keyByteSeven_52)); }
	inline RuntimeObject * get_keyByteSeven_52() const { return ___keyByteSeven_52; }
	inline RuntimeObject ** get_address_of_keyByteSeven_52() { return &___keyByteSeven_52; }
	inline void set_keyByteSeven_52(RuntimeObject * value)
	{
		___keyByteSeven_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyByteSeven_52), (void*)value);
	}

	inline static int32_t get_offset_of_keyByteEight_53() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___keyByteEight_53)); }
	inline RuntimeObject * get_keyByteEight_53() const { return ___keyByteEight_53; }
	inline RuntimeObject ** get_address_of_keyByteEight_53() { return &___keyByteEight_53; }
	inline void set_keyByteEight_53(RuntimeObject * value)
	{
		___keyByteEight_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyByteEight_53), (void*)value);
	}

	inline static int32_t get_offset_of_emptyObjectArray_54() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___emptyObjectArray_54)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_emptyObjectArray_54() const { return ___emptyObjectArray_54; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_emptyObjectArray_54() { return &___emptyObjectArray_54; }
	inline void set_emptyObjectArray_54(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___emptyObjectArray_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___emptyObjectArray_54), (void*)value);
	}

	inline static int32_t get_offset_of_emptyTypeArray_55() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___emptyTypeArray_55)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_emptyTypeArray_55() const { return ___emptyTypeArray_55; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_emptyTypeArray_55() { return &___emptyTypeArray_55; }
	inline void set_emptyTypeArray_55(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___emptyTypeArray_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___emptyTypeArray_55), (void*)value);
	}

	inline static int32_t get_offset_of_removeFilter_56() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___removeFilter_56)); }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * get_removeFilter_56() const { return ___removeFilter_56; }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD ** get_address_of_removeFilter_56() { return &___removeFilter_56; }
	inline void set_removeFilter_56(Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * value)
	{
		___removeFilter_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___removeFilter_56), (void*)value);
	}

	inline static int32_t get_offset_of_ServerCleanDestroyEvent_57() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___ServerCleanDestroyEvent_57)); }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * get_ServerCleanDestroyEvent_57() const { return ___ServerCleanDestroyEvent_57; }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD ** get_address_of_ServerCleanDestroyEvent_57() { return &___ServerCleanDestroyEvent_57; }
	inline void set_ServerCleanDestroyEvent_57(Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * value)
	{
		___ServerCleanDestroyEvent_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ServerCleanDestroyEvent_57), (void*)value);
	}

	inline static int32_t get_offset_of_ServerCleanOptions_58() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___ServerCleanOptions_58)); }
	inline RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * get_ServerCleanOptions_58() const { return ___ServerCleanOptions_58; }
	inline RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED ** get_address_of_ServerCleanOptions_58() { return &___ServerCleanOptions_58; }
	inline void set_ServerCleanOptions_58(RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * value)
	{
		___ServerCleanOptions_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ServerCleanOptions_58), (void*)value);
	}

	inline static int32_t get_offset_of_rpcFilterByViewId_59() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___rpcFilterByViewId_59)); }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * get_rpcFilterByViewId_59() const { return ___rpcFilterByViewId_59; }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD ** get_address_of_rpcFilterByViewId_59() { return &___rpcFilterByViewId_59; }
	inline void set_rpcFilterByViewId_59(Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * value)
	{
		___rpcFilterByViewId_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rpcFilterByViewId_59), (void*)value);
	}

	inline static int32_t get_offset_of_OpCleanRpcBufferOptions_60() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___OpCleanRpcBufferOptions_60)); }
	inline RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * get_OpCleanRpcBufferOptions_60() const { return ___OpCleanRpcBufferOptions_60; }
	inline RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED ** get_address_of_OpCleanRpcBufferOptions_60() { return &___OpCleanRpcBufferOptions_60; }
	inline void set_OpCleanRpcBufferOptions_60(RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * value)
	{
		___OpCleanRpcBufferOptions_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OpCleanRpcBufferOptions_60), (void*)value);
	}

	inline static int32_t get_offset_of_rpcEvent_61() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___rpcEvent_61)); }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * get_rpcEvent_61() const { return ___rpcEvent_61; }
	inline Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD ** get_address_of_rpcEvent_61() { return &___rpcEvent_61; }
	inline void set_rpcEvent_61(Hashtable_t57A0F7332399E94183F577908C44E2B9FA4CDFBD * value)
	{
		___rpcEvent_61 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rpcEvent_61), (void*)value);
	}

	inline static int32_t get_offset_of_RpcOptionsToAll_62() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___RpcOptionsToAll_62)); }
	inline RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * get_RpcOptionsToAll_62() const { return ___RpcOptionsToAll_62; }
	inline RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED ** get_address_of_RpcOptionsToAll_62() { return &___RpcOptionsToAll_62; }
	inline void set_RpcOptionsToAll_62(RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * value)
	{
		___RpcOptionsToAll_62 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RpcOptionsToAll_62), (void*)value);
	}

	inline static int32_t get_offset_of_ObjectsInOneUpdate_63() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___ObjectsInOneUpdate_63)); }
	inline int32_t get_ObjectsInOneUpdate_63() const { return ___ObjectsInOneUpdate_63; }
	inline int32_t* get_address_of_ObjectsInOneUpdate_63() { return &___ObjectsInOneUpdate_63; }
	inline void set_ObjectsInOneUpdate_63(int32_t value)
	{
		___ObjectsInOneUpdate_63 = value;
	}

	inline static int32_t get_offset_of_serializeStreamOut_64() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___serializeStreamOut_64)); }
	inline PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * get_serializeStreamOut_64() const { return ___serializeStreamOut_64; }
	inline PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E ** get_address_of_serializeStreamOut_64() { return &___serializeStreamOut_64; }
	inline void set_serializeStreamOut_64(PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * value)
	{
		___serializeStreamOut_64 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializeStreamOut_64), (void*)value);
	}

	inline static int32_t get_offset_of_serializeStreamIn_65() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___serializeStreamIn_65)); }
	inline PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * get_serializeStreamIn_65() const { return ___serializeStreamIn_65; }
	inline PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E ** get_address_of_serializeStreamIn_65() { return &___serializeStreamIn_65; }
	inline void set_serializeStreamIn_65(PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * value)
	{
		___serializeStreamIn_65 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializeStreamIn_65), (void*)value);
	}

	inline static int32_t get_offset_of_serializeRaiseEvOptions_66() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___serializeRaiseEvOptions_66)); }
	inline RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * get_serializeRaiseEvOptions_66() const { return ___serializeRaiseEvOptions_66; }
	inline RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED ** get_address_of_serializeRaiseEvOptions_66() { return &___serializeRaiseEvOptions_66; }
	inline void set_serializeRaiseEvOptions_66(RaiseEventOptions_tA6691DAD7E0A61E11C468287C200539FBB641AED * value)
	{
		___serializeRaiseEvOptions_66 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializeRaiseEvOptions_66), (void*)value);
	}

	inline static int32_t get_offset_of_serializeViewBatches_67() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ___serializeViewBatches_67)); }
	inline Dictionary_2_t814656FD5269C0EAD336588E42742ED4E8A7AA30 * get_serializeViewBatches_67() const { return ___serializeViewBatches_67; }
	inline Dictionary_2_t814656FD5269C0EAD336588E42742ED4E8A7AA30 ** get_address_of_serializeViewBatches_67() { return &___serializeViewBatches_67; }
	inline void set_serializeViewBatches_67(Dictionary_2_t814656FD5269C0EAD336588E42742ED4E8A7AA30 * value)
	{
		___serializeViewBatches_67 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializeViewBatches_67), (void*)value);
	}

	inline static int32_t get_offset_of__cachedRegionHandler_72() { return static_cast<int32_t>(offsetof(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields, ____cachedRegionHandler_72)); }
	inline RegionHandler_tE1C559FF2DCCA020EC31ACCF3993A434617A15AF * get__cachedRegionHandler_72() const { return ____cachedRegionHandler_72; }
	inline RegionHandler_tE1C559FF2DCCA020EC31ACCF3993A434617A15AF ** get_address_of__cachedRegionHandler_72() { return &____cachedRegionHandler_72; }
	inline void set__cachedRegionHandler_72(RegionHandler_tE1C559FF2DCCA020EC31ACCF3993A434617A15AF * value)
	{
		____cachedRegionHandler_72 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cachedRegionHandler_72), (void*)value);
	}
};


// Photon.Pun.PhotonTransformViewPositionModel
struct  PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969  : public RuntimeObject
{
public:
	// System.Boolean Photon.Pun.PhotonTransformViewPositionModel::SynchronizeEnabled
	bool ___SynchronizeEnabled_0;
	// System.Boolean Photon.Pun.PhotonTransformViewPositionModel::TeleportEnabled
	bool ___TeleportEnabled_1;
	// System.Single Photon.Pun.PhotonTransformViewPositionModel::TeleportIfDistanceGreaterThan
	float ___TeleportIfDistanceGreaterThan_2;
	// Photon.Pun.PhotonTransformViewPositionModel_InterpolateOptions Photon.Pun.PhotonTransformViewPositionModel::InterpolateOption
	int32_t ___InterpolateOption_3;
	// System.Single Photon.Pun.PhotonTransformViewPositionModel::InterpolateMoveTowardsSpeed
	float ___InterpolateMoveTowardsSpeed_4;
	// System.Single Photon.Pun.PhotonTransformViewPositionModel::InterpolateLerpSpeed
	float ___InterpolateLerpSpeed_5;
	// Photon.Pun.PhotonTransformViewPositionModel_ExtrapolateOptions Photon.Pun.PhotonTransformViewPositionModel::ExtrapolateOption
	int32_t ___ExtrapolateOption_6;
	// System.Single Photon.Pun.PhotonTransformViewPositionModel::ExtrapolateSpeed
	float ___ExtrapolateSpeed_7;
	// System.Boolean Photon.Pun.PhotonTransformViewPositionModel::ExtrapolateIncludingRoundTripTime
	bool ___ExtrapolateIncludingRoundTripTime_8;
	// System.Int32 Photon.Pun.PhotonTransformViewPositionModel::ExtrapolateNumberOfStoredPositions
	int32_t ___ExtrapolateNumberOfStoredPositions_9;

public:
	inline static int32_t get_offset_of_SynchronizeEnabled_0() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969, ___SynchronizeEnabled_0)); }
	inline bool get_SynchronizeEnabled_0() const { return ___SynchronizeEnabled_0; }
	inline bool* get_address_of_SynchronizeEnabled_0() { return &___SynchronizeEnabled_0; }
	inline void set_SynchronizeEnabled_0(bool value)
	{
		___SynchronizeEnabled_0 = value;
	}

	inline static int32_t get_offset_of_TeleportEnabled_1() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969, ___TeleportEnabled_1)); }
	inline bool get_TeleportEnabled_1() const { return ___TeleportEnabled_1; }
	inline bool* get_address_of_TeleportEnabled_1() { return &___TeleportEnabled_1; }
	inline void set_TeleportEnabled_1(bool value)
	{
		___TeleportEnabled_1 = value;
	}

	inline static int32_t get_offset_of_TeleportIfDistanceGreaterThan_2() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969, ___TeleportIfDistanceGreaterThan_2)); }
	inline float get_TeleportIfDistanceGreaterThan_2() const { return ___TeleportIfDistanceGreaterThan_2; }
	inline float* get_address_of_TeleportIfDistanceGreaterThan_2() { return &___TeleportIfDistanceGreaterThan_2; }
	inline void set_TeleportIfDistanceGreaterThan_2(float value)
	{
		___TeleportIfDistanceGreaterThan_2 = value;
	}

	inline static int32_t get_offset_of_InterpolateOption_3() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969, ___InterpolateOption_3)); }
	inline int32_t get_InterpolateOption_3() const { return ___InterpolateOption_3; }
	inline int32_t* get_address_of_InterpolateOption_3() { return &___InterpolateOption_3; }
	inline void set_InterpolateOption_3(int32_t value)
	{
		___InterpolateOption_3 = value;
	}

	inline static int32_t get_offset_of_InterpolateMoveTowardsSpeed_4() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969, ___InterpolateMoveTowardsSpeed_4)); }
	inline float get_InterpolateMoveTowardsSpeed_4() const { return ___InterpolateMoveTowardsSpeed_4; }
	inline float* get_address_of_InterpolateMoveTowardsSpeed_4() { return &___InterpolateMoveTowardsSpeed_4; }
	inline void set_InterpolateMoveTowardsSpeed_4(float value)
	{
		___InterpolateMoveTowardsSpeed_4 = value;
	}

	inline static int32_t get_offset_of_InterpolateLerpSpeed_5() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969, ___InterpolateLerpSpeed_5)); }
	inline float get_InterpolateLerpSpeed_5() const { return ___InterpolateLerpSpeed_5; }
	inline float* get_address_of_InterpolateLerpSpeed_5() { return &___InterpolateLerpSpeed_5; }
	inline void set_InterpolateLerpSpeed_5(float value)
	{
		___InterpolateLerpSpeed_5 = value;
	}

	inline static int32_t get_offset_of_ExtrapolateOption_6() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969, ___ExtrapolateOption_6)); }
	inline int32_t get_ExtrapolateOption_6() const { return ___ExtrapolateOption_6; }
	inline int32_t* get_address_of_ExtrapolateOption_6() { return &___ExtrapolateOption_6; }
	inline void set_ExtrapolateOption_6(int32_t value)
	{
		___ExtrapolateOption_6 = value;
	}

	inline static int32_t get_offset_of_ExtrapolateSpeed_7() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969, ___ExtrapolateSpeed_7)); }
	inline float get_ExtrapolateSpeed_7() const { return ___ExtrapolateSpeed_7; }
	inline float* get_address_of_ExtrapolateSpeed_7() { return &___ExtrapolateSpeed_7; }
	inline void set_ExtrapolateSpeed_7(float value)
	{
		___ExtrapolateSpeed_7 = value;
	}

	inline static int32_t get_offset_of_ExtrapolateIncludingRoundTripTime_8() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969, ___ExtrapolateIncludingRoundTripTime_8)); }
	inline bool get_ExtrapolateIncludingRoundTripTime_8() const { return ___ExtrapolateIncludingRoundTripTime_8; }
	inline bool* get_address_of_ExtrapolateIncludingRoundTripTime_8() { return &___ExtrapolateIncludingRoundTripTime_8; }
	inline void set_ExtrapolateIncludingRoundTripTime_8(bool value)
	{
		___ExtrapolateIncludingRoundTripTime_8 = value;
	}

	inline static int32_t get_offset_of_ExtrapolateNumberOfStoredPositions_9() { return static_cast<int32_t>(offsetof(PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969, ___ExtrapolateNumberOfStoredPositions_9)); }
	inline int32_t get_ExtrapolateNumberOfStoredPositions_9() const { return ___ExtrapolateNumberOfStoredPositions_9; }
	inline int32_t* get_address_of_ExtrapolateNumberOfStoredPositions_9() { return &___ExtrapolateNumberOfStoredPositions_9; }
	inline void set_ExtrapolateNumberOfStoredPositions_9(int32_t value)
	{
		___ExtrapolateNumberOfStoredPositions_9 = value;
	}
};


// Photon.Pun.PhotonTransformViewRotationModel
struct  PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D  : public RuntimeObject
{
public:
	// System.Boolean Photon.Pun.PhotonTransformViewRotationModel::SynchronizeEnabled
	bool ___SynchronizeEnabled_0;
	// Photon.Pun.PhotonTransformViewRotationModel_InterpolateOptions Photon.Pun.PhotonTransformViewRotationModel::InterpolateOption
	int32_t ___InterpolateOption_1;
	// System.Single Photon.Pun.PhotonTransformViewRotationModel::InterpolateRotateTowardsSpeed
	float ___InterpolateRotateTowardsSpeed_2;
	// System.Single Photon.Pun.PhotonTransformViewRotationModel::InterpolateLerpSpeed
	float ___InterpolateLerpSpeed_3;

public:
	inline static int32_t get_offset_of_SynchronizeEnabled_0() { return static_cast<int32_t>(offsetof(PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D, ___SynchronizeEnabled_0)); }
	inline bool get_SynchronizeEnabled_0() const { return ___SynchronizeEnabled_0; }
	inline bool* get_address_of_SynchronizeEnabled_0() { return &___SynchronizeEnabled_0; }
	inline void set_SynchronizeEnabled_0(bool value)
	{
		___SynchronizeEnabled_0 = value;
	}

	inline static int32_t get_offset_of_InterpolateOption_1() { return static_cast<int32_t>(offsetof(PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D, ___InterpolateOption_1)); }
	inline int32_t get_InterpolateOption_1() const { return ___InterpolateOption_1; }
	inline int32_t* get_address_of_InterpolateOption_1() { return &___InterpolateOption_1; }
	inline void set_InterpolateOption_1(int32_t value)
	{
		___InterpolateOption_1 = value;
	}

	inline static int32_t get_offset_of_InterpolateRotateTowardsSpeed_2() { return static_cast<int32_t>(offsetof(PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D, ___InterpolateRotateTowardsSpeed_2)); }
	inline float get_InterpolateRotateTowardsSpeed_2() const { return ___InterpolateRotateTowardsSpeed_2; }
	inline float* get_address_of_InterpolateRotateTowardsSpeed_2() { return &___InterpolateRotateTowardsSpeed_2; }
	inline void set_InterpolateRotateTowardsSpeed_2(float value)
	{
		___InterpolateRotateTowardsSpeed_2 = value;
	}

	inline static int32_t get_offset_of_InterpolateLerpSpeed_3() { return static_cast<int32_t>(offsetof(PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D, ___InterpolateLerpSpeed_3)); }
	inline float get_InterpolateLerpSpeed_3() const { return ___InterpolateLerpSpeed_3; }
	inline float* get_address_of_InterpolateLerpSpeed_3() { return &___InterpolateLerpSpeed_3; }
	inline void set_InterpolateLerpSpeed_3(float value)
	{
		___InterpolateLerpSpeed_3 = value;
	}
};


// Photon.Pun.PhotonTransformViewScaleModel
struct  PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59  : public RuntimeObject
{
public:
	// System.Boolean Photon.Pun.PhotonTransformViewScaleModel::SynchronizeEnabled
	bool ___SynchronizeEnabled_0;
	// Photon.Pun.PhotonTransformViewScaleModel_InterpolateOptions Photon.Pun.PhotonTransformViewScaleModel::InterpolateOption
	int32_t ___InterpolateOption_1;
	// System.Single Photon.Pun.PhotonTransformViewScaleModel::InterpolateMoveTowardsSpeed
	float ___InterpolateMoveTowardsSpeed_2;
	// System.Single Photon.Pun.PhotonTransformViewScaleModel::InterpolateLerpSpeed
	float ___InterpolateLerpSpeed_3;

public:
	inline static int32_t get_offset_of_SynchronizeEnabled_0() { return static_cast<int32_t>(offsetof(PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59, ___SynchronizeEnabled_0)); }
	inline bool get_SynchronizeEnabled_0() const { return ___SynchronizeEnabled_0; }
	inline bool* get_address_of_SynchronizeEnabled_0() { return &___SynchronizeEnabled_0; }
	inline void set_SynchronizeEnabled_0(bool value)
	{
		___SynchronizeEnabled_0 = value;
	}

	inline static int32_t get_offset_of_InterpolateOption_1() { return static_cast<int32_t>(offsetof(PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59, ___InterpolateOption_1)); }
	inline int32_t get_InterpolateOption_1() const { return ___InterpolateOption_1; }
	inline int32_t* get_address_of_InterpolateOption_1() { return &___InterpolateOption_1; }
	inline void set_InterpolateOption_1(int32_t value)
	{
		___InterpolateOption_1 = value;
	}

	inline static int32_t get_offset_of_InterpolateMoveTowardsSpeed_2() { return static_cast<int32_t>(offsetof(PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59, ___InterpolateMoveTowardsSpeed_2)); }
	inline float get_InterpolateMoveTowardsSpeed_2() const { return ___InterpolateMoveTowardsSpeed_2; }
	inline float* get_address_of_InterpolateMoveTowardsSpeed_2() { return &___InterpolateMoveTowardsSpeed_2; }
	inline void set_InterpolateMoveTowardsSpeed_2(float value)
	{
		___InterpolateMoveTowardsSpeed_2 = value;
	}

	inline static int32_t get_offset_of_InterpolateLerpSpeed_3() { return static_cast<int32_t>(offsetof(PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59, ___InterpolateLerpSpeed_3)); }
	inline float get_InterpolateLerpSpeed_3() const { return ___InterpolateLerpSpeed_3; }
	inline float* get_address_of_InterpolateLerpSpeed_3() { return &___InterpolateLerpSpeed_3; }
	inline void set_InterpolateLerpSpeed_3(float value)
	{
		___InterpolateLerpSpeed_3 = value;
	}
};


// Photon.Realtime.AppSettings
struct  AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8  : public RuntimeObject
{
public:
	// System.String Photon.Realtime.AppSettings::AppIdRealtime
	String_t* ___AppIdRealtime_0;
	// System.String Photon.Realtime.AppSettings::AppIdChat
	String_t* ___AppIdChat_1;
	// System.String Photon.Realtime.AppSettings::AppIdVoice
	String_t* ___AppIdVoice_2;
	// System.String Photon.Realtime.AppSettings::AppVersion
	String_t* ___AppVersion_3;
	// System.Boolean Photon.Realtime.AppSettings::UseNameServer
	bool ___UseNameServer_4;
	// System.String Photon.Realtime.AppSettings::FixedRegion
	String_t* ___FixedRegion_5;
	// System.String Photon.Realtime.AppSettings::Server
	String_t* ___Server_6;
	// System.Int32 Photon.Realtime.AppSettings::Port
	int32_t ___Port_7;
	// ExitGames.Client.Photon.ConnectionProtocol Photon.Realtime.AppSettings::Protocol
	uint8_t ___Protocol_8;
	// System.Boolean Photon.Realtime.AppSettings::EnableLobbyStatistics
	bool ___EnableLobbyStatistics_9;
	// ExitGames.Client.Photon.DebugLevel Photon.Realtime.AppSettings::NetworkLogging
	uint8_t ___NetworkLogging_10;

public:
	inline static int32_t get_offset_of_AppIdRealtime_0() { return static_cast<int32_t>(offsetof(AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8, ___AppIdRealtime_0)); }
	inline String_t* get_AppIdRealtime_0() const { return ___AppIdRealtime_0; }
	inline String_t** get_address_of_AppIdRealtime_0() { return &___AppIdRealtime_0; }
	inline void set_AppIdRealtime_0(String_t* value)
	{
		___AppIdRealtime_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AppIdRealtime_0), (void*)value);
	}

	inline static int32_t get_offset_of_AppIdChat_1() { return static_cast<int32_t>(offsetof(AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8, ___AppIdChat_1)); }
	inline String_t* get_AppIdChat_1() const { return ___AppIdChat_1; }
	inline String_t** get_address_of_AppIdChat_1() { return &___AppIdChat_1; }
	inline void set_AppIdChat_1(String_t* value)
	{
		___AppIdChat_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AppIdChat_1), (void*)value);
	}

	inline static int32_t get_offset_of_AppIdVoice_2() { return static_cast<int32_t>(offsetof(AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8, ___AppIdVoice_2)); }
	inline String_t* get_AppIdVoice_2() const { return ___AppIdVoice_2; }
	inline String_t** get_address_of_AppIdVoice_2() { return &___AppIdVoice_2; }
	inline void set_AppIdVoice_2(String_t* value)
	{
		___AppIdVoice_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AppIdVoice_2), (void*)value);
	}

	inline static int32_t get_offset_of_AppVersion_3() { return static_cast<int32_t>(offsetof(AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8, ___AppVersion_3)); }
	inline String_t* get_AppVersion_3() const { return ___AppVersion_3; }
	inline String_t** get_address_of_AppVersion_3() { return &___AppVersion_3; }
	inline void set_AppVersion_3(String_t* value)
	{
		___AppVersion_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AppVersion_3), (void*)value);
	}

	inline static int32_t get_offset_of_UseNameServer_4() { return static_cast<int32_t>(offsetof(AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8, ___UseNameServer_4)); }
	inline bool get_UseNameServer_4() const { return ___UseNameServer_4; }
	inline bool* get_address_of_UseNameServer_4() { return &___UseNameServer_4; }
	inline void set_UseNameServer_4(bool value)
	{
		___UseNameServer_4 = value;
	}

	inline static int32_t get_offset_of_FixedRegion_5() { return static_cast<int32_t>(offsetof(AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8, ___FixedRegion_5)); }
	inline String_t* get_FixedRegion_5() const { return ___FixedRegion_5; }
	inline String_t** get_address_of_FixedRegion_5() { return &___FixedRegion_5; }
	inline void set_FixedRegion_5(String_t* value)
	{
		___FixedRegion_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FixedRegion_5), (void*)value);
	}

	inline static int32_t get_offset_of_Server_6() { return static_cast<int32_t>(offsetof(AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8, ___Server_6)); }
	inline String_t* get_Server_6() const { return ___Server_6; }
	inline String_t** get_address_of_Server_6() { return &___Server_6; }
	inline void set_Server_6(String_t* value)
	{
		___Server_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Server_6), (void*)value);
	}

	inline static int32_t get_offset_of_Port_7() { return static_cast<int32_t>(offsetof(AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8, ___Port_7)); }
	inline int32_t get_Port_7() const { return ___Port_7; }
	inline int32_t* get_address_of_Port_7() { return &___Port_7; }
	inline void set_Port_7(int32_t value)
	{
		___Port_7 = value;
	}

	inline static int32_t get_offset_of_Protocol_8() { return static_cast<int32_t>(offsetof(AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8, ___Protocol_8)); }
	inline uint8_t get_Protocol_8() const { return ___Protocol_8; }
	inline uint8_t* get_address_of_Protocol_8() { return &___Protocol_8; }
	inline void set_Protocol_8(uint8_t value)
	{
		___Protocol_8 = value;
	}

	inline static int32_t get_offset_of_EnableLobbyStatistics_9() { return static_cast<int32_t>(offsetof(AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8, ___EnableLobbyStatistics_9)); }
	inline bool get_EnableLobbyStatistics_9() const { return ___EnableLobbyStatistics_9; }
	inline bool* get_address_of_EnableLobbyStatistics_9() { return &___EnableLobbyStatistics_9; }
	inline void set_EnableLobbyStatistics_9(bool value)
	{
		___EnableLobbyStatistics_9 = value;
	}

	inline static int32_t get_offset_of_NetworkLogging_10() { return static_cast<int32_t>(offsetof(AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8, ___NetworkLogging_10)); }
	inline uint8_t get_NetworkLogging_10() const { return ___NetworkLogging_10; }
	inline uint8_t* get_address_of_NetworkLogging_10() { return &___NetworkLogging_10; }
	inline void set_NetworkLogging_10(uint8_t value)
	{
		___NetworkLogging_10 = value;
	}
};


// Photon.Realtime.LoadBalancingClient
struct  LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A  : public RuntimeObject
{
public:
	// Photon.Realtime.LoadBalancingPeer Photon.Realtime.LoadBalancingClient::<LoadBalancingPeer>k__BackingField
	LoadBalancingPeer_tCD3148D53F3EF643FCBB86A139EF0B84FD3F3F14 * ___U3CLoadBalancingPeerU3Ek__BackingField_0;
	// System.String Photon.Realtime.LoadBalancingClient::<AppVersion>k__BackingField
	String_t* ___U3CAppVersionU3Ek__BackingField_1;
	// System.String Photon.Realtime.LoadBalancingClient::<AppId>k__BackingField
	String_t* ___U3CAppIdU3Ek__BackingField_2;
	// Photon.Realtime.AuthenticationValues Photon.Realtime.LoadBalancingClient::<AuthValues>k__BackingField
	AuthenticationValues_t3D3544E09B8F3860CBF8B0998760A0CAE94F2D13 * ___U3CAuthValuesU3Ek__BackingField_3;
	// Photon.Realtime.AuthModeOption Photon.Realtime.LoadBalancingClient::AuthMode
	int32_t ___AuthMode_4;
	// Photon.Realtime.EncryptionMode Photon.Realtime.LoadBalancingClient::EncryptionMode
	int32_t ___EncryptionMode_5;
	// ExitGames.Client.Photon.ConnectionProtocol Photon.Realtime.LoadBalancingClient::ExpectedProtocol
	uint8_t ___ExpectedProtocol_6;
	// System.String Photon.Realtime.LoadBalancingClient::tokenCache
	String_t* ___tokenCache_7;
	// System.Boolean Photon.Realtime.LoadBalancingClient::<IsUsingNameServer>k__BackingField
	bool ___U3CIsUsingNameServerU3Ek__BackingField_8;
	// System.String Photon.Realtime.LoadBalancingClient::NameServerHost
	String_t* ___NameServerHost_9;
	// System.String Photon.Realtime.LoadBalancingClient::NameServerHttp
	String_t* ___NameServerHttp_10;
	// System.Boolean Photon.Realtime.LoadBalancingClient::<UseAlternativeUdpPorts>k__BackingField
	bool ___U3CUseAlternativeUdpPortsU3Ek__BackingField_12;
	// System.String Photon.Realtime.LoadBalancingClient::<MasterServerAddress>k__BackingField
	String_t* ___U3CMasterServerAddressU3Ek__BackingField_13;
	// System.String Photon.Realtime.LoadBalancingClient::<GameServerAddress>k__BackingField
	String_t* ___U3CGameServerAddressU3Ek__BackingField_14;
	// Photon.Realtime.ServerConnection Photon.Realtime.LoadBalancingClient::<Server>k__BackingField
	int32_t ___U3CServerU3Ek__BackingField_15;
	// Photon.Realtime.ClientState Photon.Realtime.LoadBalancingClient::state
	int32_t ___state_16;
	// System.Action`2<Photon.Realtime.ClientState,Photon.Realtime.ClientState> Photon.Realtime.LoadBalancingClient::StateChanged
	Action_2_tD4F07FC3F0B08A4BE3D400AB1E3553E633312299 * ___StateChanged_17;
	// System.Action`1<ExitGames.Client.Photon.EventData> Photon.Realtime.LoadBalancingClient::EventReceived
	Action_1_t989F0A0D66ADCCABF53890A353BD2AE5709333D4 * ___EventReceived_18;
	// System.Action`1<ExitGames.Client.Photon.OperationResponse> Photon.Realtime.LoadBalancingClient::OpResponseReceived
	Action_1_t654A246F32A45B98C5C30E5DCC92C7FDDDA24C2B * ___OpResponseReceived_19;
	// Photon.Realtime.ConnectionCallbacksContainer Photon.Realtime.LoadBalancingClient::ConnectionCallbackTargets
	ConnectionCallbacksContainer_tDCE59A1A1B21F1438DACBE458620B2942BB2A110 * ___ConnectionCallbackTargets_20;
	// Photon.Realtime.MatchMakingCallbacksContainer Photon.Realtime.LoadBalancingClient::MatchMakingCallbackTargets
	MatchMakingCallbacksContainer_tE6BA3F44DC157B50650F599CD8C26D23E7153CC3 * ___MatchMakingCallbackTargets_21;
	// Photon.Realtime.InRoomCallbacksContainer Photon.Realtime.LoadBalancingClient::InRoomCallbackTargets
	InRoomCallbacksContainer_tD26EF1A78F7419C0FF1B25FBD0CCEA40D3EB810D * ___InRoomCallbackTargets_22;
	// Photon.Realtime.LobbyCallbacksContainer Photon.Realtime.LoadBalancingClient::LobbyCallbackTargets
	LobbyCallbacksContainer_t42830CD7E8C5C38D10AB8157E9EDECF9B26C2571 * ___LobbyCallbackTargets_23;
	// Photon.Realtime.WebRpcCallbacksContainer Photon.Realtime.LoadBalancingClient::WebRpcCallbackTargets
	WebRpcCallbacksContainer_t13E35FB3A5C223F5EC56992E6CF51A0BC4D10895 * ___WebRpcCallbackTargets_24;
	// Photon.Realtime.DisconnectCause Photon.Realtime.LoadBalancingClient::<DisconnectedCause>k__BackingField
	int32_t ___U3CDisconnectedCauseU3Ek__BackingField_25;
	// Photon.Realtime.TypedLobby Photon.Realtime.LoadBalancingClient::<CurrentLobby>k__BackingField
	TypedLobby_tD5DAFEDF05C473EE3FB9618C0641D1CBB4CA4BD5 * ___U3CCurrentLobbyU3Ek__BackingField_26;
	// System.Boolean Photon.Realtime.LoadBalancingClient::EnableLobbyStatistics
	bool ___EnableLobbyStatistics_27;
	// System.Collections.Generic.List`1<Photon.Realtime.TypedLobbyInfo> Photon.Realtime.LoadBalancingClient::lobbyStatistics
	List_1_t6C9FCEA5B81789209C0373EEE6952A0DC27D1459 * ___lobbyStatistics_28;
	// Photon.Realtime.Player Photon.Realtime.LoadBalancingClient::<LocalPlayer>k__BackingField
	Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * ___U3CLocalPlayerU3Ek__BackingField_29;
	// Photon.Realtime.Room Photon.Realtime.LoadBalancingClient::<CurrentRoom>k__BackingField
	Room_t5DFC39DD6736A2641374564EC6C31352BE33000D * ___U3CCurrentRoomU3Ek__BackingField_30;
	// System.Int32 Photon.Realtime.LoadBalancingClient::<PlayersOnMasterCount>k__BackingField
	int32_t ___U3CPlayersOnMasterCountU3Ek__BackingField_31;
	// System.Int32 Photon.Realtime.LoadBalancingClient::<PlayersInRoomsCount>k__BackingField
	int32_t ___U3CPlayersInRoomsCountU3Ek__BackingField_32;
	// System.Int32 Photon.Realtime.LoadBalancingClient::<RoomsCount>k__BackingField
	int32_t ___U3CRoomsCountU3Ek__BackingField_33;
	// Photon.Realtime.JoinType Photon.Realtime.LoadBalancingClient::lastJoinType
	int32_t ___lastJoinType_34;
	// Photon.Realtime.EnterRoomParams Photon.Realtime.LoadBalancingClient::enterRoomParamsCache
	EnterRoomParams_t35117DAFF025133E50BB2CFA7864669F6CED1942 * ___enterRoomParamsCache_35;
	// ExitGames.Client.Photon.OperationResponse Photon.Realtime.LoadBalancingClient::failedRoomEntryOperation
	OperationResponse_tC67CCFA9FAB3EDF55AA824724BA47D45AB3585FE * ___failedRoomEntryOperation_36;
	// System.String[] Photon.Realtime.LoadBalancingClient::friendListRequested
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___friendListRequested_38;
	// System.String Photon.Realtime.LoadBalancingClient::<CloudRegion>k__BackingField
	String_t* ___U3CCloudRegionU3Ek__BackingField_39;
	// System.String Photon.Realtime.LoadBalancingClient::<CurrentCluster>k__BackingField
	String_t* ___U3CCurrentClusterU3Ek__BackingField_40;
	// Photon.Realtime.RegionHandler Photon.Realtime.LoadBalancingClient::RegionHandler
	RegionHandler_tE1C559FF2DCCA020EC31ACCF3993A434617A15AF * ___RegionHandler_41;
	// System.Collections.Generic.Queue`1<Photon.Realtime.LoadBalancingClient_CallbackTargetChange> Photon.Realtime.LoadBalancingClient::callbackTargetChanges
	Queue_1_t2DF167EE7F886C6E1CE292988624D5634AF12DE2 * ___callbackTargetChanges_42;
	// System.Collections.Generic.HashSet`1<System.Object> Photon.Realtime.LoadBalancingClient::callbackTargets
	HashSet_1_t20C647F44B285CB7C061931B00ED43A230F311E5 * ___callbackTargets_43;

public:
	inline static int32_t get_offset_of_U3CLoadBalancingPeerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CLoadBalancingPeerU3Ek__BackingField_0)); }
	inline LoadBalancingPeer_tCD3148D53F3EF643FCBB86A139EF0B84FD3F3F14 * get_U3CLoadBalancingPeerU3Ek__BackingField_0() const { return ___U3CLoadBalancingPeerU3Ek__BackingField_0; }
	inline LoadBalancingPeer_tCD3148D53F3EF643FCBB86A139EF0B84FD3F3F14 ** get_address_of_U3CLoadBalancingPeerU3Ek__BackingField_0() { return &___U3CLoadBalancingPeerU3Ek__BackingField_0; }
	inline void set_U3CLoadBalancingPeerU3Ek__BackingField_0(LoadBalancingPeer_tCD3148D53F3EF643FCBB86A139EF0B84FD3F3F14 * value)
	{
		___U3CLoadBalancingPeerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLoadBalancingPeerU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAppVersionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CAppVersionU3Ek__BackingField_1)); }
	inline String_t* get_U3CAppVersionU3Ek__BackingField_1() const { return ___U3CAppVersionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAppVersionU3Ek__BackingField_1() { return &___U3CAppVersionU3Ek__BackingField_1; }
	inline void set_U3CAppVersionU3Ek__BackingField_1(String_t* value)
	{
		___U3CAppVersionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAppVersionU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAppIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CAppIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CAppIdU3Ek__BackingField_2() const { return ___U3CAppIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CAppIdU3Ek__BackingField_2() { return &___U3CAppIdU3Ek__BackingField_2; }
	inline void set_U3CAppIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CAppIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAppIdU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAuthValuesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CAuthValuesU3Ek__BackingField_3)); }
	inline AuthenticationValues_t3D3544E09B8F3860CBF8B0998760A0CAE94F2D13 * get_U3CAuthValuesU3Ek__BackingField_3() const { return ___U3CAuthValuesU3Ek__BackingField_3; }
	inline AuthenticationValues_t3D3544E09B8F3860CBF8B0998760A0CAE94F2D13 ** get_address_of_U3CAuthValuesU3Ek__BackingField_3() { return &___U3CAuthValuesU3Ek__BackingField_3; }
	inline void set_U3CAuthValuesU3Ek__BackingField_3(AuthenticationValues_t3D3544E09B8F3860CBF8B0998760A0CAE94F2D13 * value)
	{
		___U3CAuthValuesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAuthValuesU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_AuthMode_4() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___AuthMode_4)); }
	inline int32_t get_AuthMode_4() const { return ___AuthMode_4; }
	inline int32_t* get_address_of_AuthMode_4() { return &___AuthMode_4; }
	inline void set_AuthMode_4(int32_t value)
	{
		___AuthMode_4 = value;
	}

	inline static int32_t get_offset_of_EncryptionMode_5() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___EncryptionMode_5)); }
	inline int32_t get_EncryptionMode_5() const { return ___EncryptionMode_5; }
	inline int32_t* get_address_of_EncryptionMode_5() { return &___EncryptionMode_5; }
	inline void set_EncryptionMode_5(int32_t value)
	{
		___EncryptionMode_5 = value;
	}

	inline static int32_t get_offset_of_ExpectedProtocol_6() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___ExpectedProtocol_6)); }
	inline uint8_t get_ExpectedProtocol_6() const { return ___ExpectedProtocol_6; }
	inline uint8_t* get_address_of_ExpectedProtocol_6() { return &___ExpectedProtocol_6; }
	inline void set_ExpectedProtocol_6(uint8_t value)
	{
		___ExpectedProtocol_6 = value;
	}

	inline static int32_t get_offset_of_tokenCache_7() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___tokenCache_7)); }
	inline String_t* get_tokenCache_7() const { return ___tokenCache_7; }
	inline String_t** get_address_of_tokenCache_7() { return &___tokenCache_7; }
	inline void set_tokenCache_7(String_t* value)
	{
		___tokenCache_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tokenCache_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsUsingNameServerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CIsUsingNameServerU3Ek__BackingField_8)); }
	inline bool get_U3CIsUsingNameServerU3Ek__BackingField_8() const { return ___U3CIsUsingNameServerU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsUsingNameServerU3Ek__BackingField_8() { return &___U3CIsUsingNameServerU3Ek__BackingField_8; }
	inline void set_U3CIsUsingNameServerU3Ek__BackingField_8(bool value)
	{
		___U3CIsUsingNameServerU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_NameServerHost_9() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___NameServerHost_9)); }
	inline String_t* get_NameServerHost_9() const { return ___NameServerHost_9; }
	inline String_t** get_address_of_NameServerHost_9() { return &___NameServerHost_9; }
	inline void set_NameServerHost_9(String_t* value)
	{
		___NameServerHost_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NameServerHost_9), (void*)value);
	}

	inline static int32_t get_offset_of_NameServerHttp_10() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___NameServerHttp_10)); }
	inline String_t* get_NameServerHttp_10() const { return ___NameServerHttp_10; }
	inline String_t** get_address_of_NameServerHttp_10() { return &___NameServerHttp_10; }
	inline void set_NameServerHttp_10(String_t* value)
	{
		___NameServerHttp_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NameServerHttp_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CUseAlternativeUdpPortsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CUseAlternativeUdpPortsU3Ek__BackingField_12)); }
	inline bool get_U3CUseAlternativeUdpPortsU3Ek__BackingField_12() const { return ___U3CUseAlternativeUdpPortsU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CUseAlternativeUdpPortsU3Ek__BackingField_12() { return &___U3CUseAlternativeUdpPortsU3Ek__BackingField_12; }
	inline void set_U3CUseAlternativeUdpPortsU3Ek__BackingField_12(bool value)
	{
		___U3CUseAlternativeUdpPortsU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CMasterServerAddressU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CMasterServerAddressU3Ek__BackingField_13)); }
	inline String_t* get_U3CMasterServerAddressU3Ek__BackingField_13() const { return ___U3CMasterServerAddressU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CMasterServerAddressU3Ek__BackingField_13() { return &___U3CMasterServerAddressU3Ek__BackingField_13; }
	inline void set_U3CMasterServerAddressU3Ek__BackingField_13(String_t* value)
	{
		___U3CMasterServerAddressU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CMasterServerAddressU3Ek__BackingField_13), (void*)value);
	}

	inline static int32_t get_offset_of_U3CGameServerAddressU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CGameServerAddressU3Ek__BackingField_14)); }
	inline String_t* get_U3CGameServerAddressU3Ek__BackingField_14() const { return ___U3CGameServerAddressU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CGameServerAddressU3Ek__BackingField_14() { return &___U3CGameServerAddressU3Ek__BackingField_14; }
	inline void set_U3CGameServerAddressU3Ek__BackingField_14(String_t* value)
	{
		___U3CGameServerAddressU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CGameServerAddressU3Ek__BackingField_14), (void*)value);
	}

	inline static int32_t get_offset_of_U3CServerU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CServerU3Ek__BackingField_15)); }
	inline int32_t get_U3CServerU3Ek__BackingField_15() const { return ___U3CServerU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CServerU3Ek__BackingField_15() { return &___U3CServerU3Ek__BackingField_15; }
	inline void set_U3CServerU3Ek__BackingField_15(int32_t value)
	{
		___U3CServerU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_state_16() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___state_16)); }
	inline int32_t get_state_16() const { return ___state_16; }
	inline int32_t* get_address_of_state_16() { return &___state_16; }
	inline void set_state_16(int32_t value)
	{
		___state_16 = value;
	}

	inline static int32_t get_offset_of_StateChanged_17() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___StateChanged_17)); }
	inline Action_2_tD4F07FC3F0B08A4BE3D400AB1E3553E633312299 * get_StateChanged_17() const { return ___StateChanged_17; }
	inline Action_2_tD4F07FC3F0B08A4BE3D400AB1E3553E633312299 ** get_address_of_StateChanged_17() { return &___StateChanged_17; }
	inline void set_StateChanged_17(Action_2_tD4F07FC3F0B08A4BE3D400AB1E3553E633312299 * value)
	{
		___StateChanged_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StateChanged_17), (void*)value);
	}

	inline static int32_t get_offset_of_EventReceived_18() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___EventReceived_18)); }
	inline Action_1_t989F0A0D66ADCCABF53890A353BD2AE5709333D4 * get_EventReceived_18() const { return ___EventReceived_18; }
	inline Action_1_t989F0A0D66ADCCABF53890A353BD2AE5709333D4 ** get_address_of_EventReceived_18() { return &___EventReceived_18; }
	inline void set_EventReceived_18(Action_1_t989F0A0D66ADCCABF53890A353BD2AE5709333D4 * value)
	{
		___EventReceived_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EventReceived_18), (void*)value);
	}

	inline static int32_t get_offset_of_OpResponseReceived_19() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___OpResponseReceived_19)); }
	inline Action_1_t654A246F32A45B98C5C30E5DCC92C7FDDDA24C2B * get_OpResponseReceived_19() const { return ___OpResponseReceived_19; }
	inline Action_1_t654A246F32A45B98C5C30E5DCC92C7FDDDA24C2B ** get_address_of_OpResponseReceived_19() { return &___OpResponseReceived_19; }
	inline void set_OpResponseReceived_19(Action_1_t654A246F32A45B98C5C30E5DCC92C7FDDDA24C2B * value)
	{
		___OpResponseReceived_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OpResponseReceived_19), (void*)value);
	}

	inline static int32_t get_offset_of_ConnectionCallbackTargets_20() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___ConnectionCallbackTargets_20)); }
	inline ConnectionCallbacksContainer_tDCE59A1A1B21F1438DACBE458620B2942BB2A110 * get_ConnectionCallbackTargets_20() const { return ___ConnectionCallbackTargets_20; }
	inline ConnectionCallbacksContainer_tDCE59A1A1B21F1438DACBE458620B2942BB2A110 ** get_address_of_ConnectionCallbackTargets_20() { return &___ConnectionCallbackTargets_20; }
	inline void set_ConnectionCallbackTargets_20(ConnectionCallbacksContainer_tDCE59A1A1B21F1438DACBE458620B2942BB2A110 * value)
	{
		___ConnectionCallbackTargets_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConnectionCallbackTargets_20), (void*)value);
	}

	inline static int32_t get_offset_of_MatchMakingCallbackTargets_21() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___MatchMakingCallbackTargets_21)); }
	inline MatchMakingCallbacksContainer_tE6BA3F44DC157B50650F599CD8C26D23E7153CC3 * get_MatchMakingCallbackTargets_21() const { return ___MatchMakingCallbackTargets_21; }
	inline MatchMakingCallbacksContainer_tE6BA3F44DC157B50650F599CD8C26D23E7153CC3 ** get_address_of_MatchMakingCallbackTargets_21() { return &___MatchMakingCallbackTargets_21; }
	inline void set_MatchMakingCallbackTargets_21(MatchMakingCallbacksContainer_tE6BA3F44DC157B50650F599CD8C26D23E7153CC3 * value)
	{
		___MatchMakingCallbackTargets_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MatchMakingCallbackTargets_21), (void*)value);
	}

	inline static int32_t get_offset_of_InRoomCallbackTargets_22() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___InRoomCallbackTargets_22)); }
	inline InRoomCallbacksContainer_tD26EF1A78F7419C0FF1B25FBD0CCEA40D3EB810D * get_InRoomCallbackTargets_22() const { return ___InRoomCallbackTargets_22; }
	inline InRoomCallbacksContainer_tD26EF1A78F7419C0FF1B25FBD0CCEA40D3EB810D ** get_address_of_InRoomCallbackTargets_22() { return &___InRoomCallbackTargets_22; }
	inline void set_InRoomCallbackTargets_22(InRoomCallbacksContainer_tD26EF1A78F7419C0FF1B25FBD0CCEA40D3EB810D * value)
	{
		___InRoomCallbackTargets_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InRoomCallbackTargets_22), (void*)value);
	}

	inline static int32_t get_offset_of_LobbyCallbackTargets_23() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___LobbyCallbackTargets_23)); }
	inline LobbyCallbacksContainer_t42830CD7E8C5C38D10AB8157E9EDECF9B26C2571 * get_LobbyCallbackTargets_23() const { return ___LobbyCallbackTargets_23; }
	inline LobbyCallbacksContainer_t42830CD7E8C5C38D10AB8157E9EDECF9B26C2571 ** get_address_of_LobbyCallbackTargets_23() { return &___LobbyCallbackTargets_23; }
	inline void set_LobbyCallbackTargets_23(LobbyCallbacksContainer_t42830CD7E8C5C38D10AB8157E9EDECF9B26C2571 * value)
	{
		___LobbyCallbackTargets_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LobbyCallbackTargets_23), (void*)value);
	}

	inline static int32_t get_offset_of_WebRpcCallbackTargets_24() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___WebRpcCallbackTargets_24)); }
	inline WebRpcCallbacksContainer_t13E35FB3A5C223F5EC56992E6CF51A0BC4D10895 * get_WebRpcCallbackTargets_24() const { return ___WebRpcCallbackTargets_24; }
	inline WebRpcCallbacksContainer_t13E35FB3A5C223F5EC56992E6CF51A0BC4D10895 ** get_address_of_WebRpcCallbackTargets_24() { return &___WebRpcCallbackTargets_24; }
	inline void set_WebRpcCallbackTargets_24(WebRpcCallbacksContainer_t13E35FB3A5C223F5EC56992E6CF51A0BC4D10895 * value)
	{
		___WebRpcCallbackTargets_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WebRpcCallbackTargets_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDisconnectedCauseU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CDisconnectedCauseU3Ek__BackingField_25)); }
	inline int32_t get_U3CDisconnectedCauseU3Ek__BackingField_25() const { return ___U3CDisconnectedCauseU3Ek__BackingField_25; }
	inline int32_t* get_address_of_U3CDisconnectedCauseU3Ek__BackingField_25() { return &___U3CDisconnectedCauseU3Ek__BackingField_25; }
	inline void set_U3CDisconnectedCauseU3Ek__BackingField_25(int32_t value)
	{
		___U3CDisconnectedCauseU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentLobbyU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CCurrentLobbyU3Ek__BackingField_26)); }
	inline TypedLobby_tD5DAFEDF05C473EE3FB9618C0641D1CBB4CA4BD5 * get_U3CCurrentLobbyU3Ek__BackingField_26() const { return ___U3CCurrentLobbyU3Ek__BackingField_26; }
	inline TypedLobby_tD5DAFEDF05C473EE3FB9618C0641D1CBB4CA4BD5 ** get_address_of_U3CCurrentLobbyU3Ek__BackingField_26() { return &___U3CCurrentLobbyU3Ek__BackingField_26; }
	inline void set_U3CCurrentLobbyU3Ek__BackingField_26(TypedLobby_tD5DAFEDF05C473EE3FB9618C0641D1CBB4CA4BD5 * value)
	{
		___U3CCurrentLobbyU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCurrentLobbyU3Ek__BackingField_26), (void*)value);
	}

	inline static int32_t get_offset_of_EnableLobbyStatistics_27() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___EnableLobbyStatistics_27)); }
	inline bool get_EnableLobbyStatistics_27() const { return ___EnableLobbyStatistics_27; }
	inline bool* get_address_of_EnableLobbyStatistics_27() { return &___EnableLobbyStatistics_27; }
	inline void set_EnableLobbyStatistics_27(bool value)
	{
		___EnableLobbyStatistics_27 = value;
	}

	inline static int32_t get_offset_of_lobbyStatistics_28() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___lobbyStatistics_28)); }
	inline List_1_t6C9FCEA5B81789209C0373EEE6952A0DC27D1459 * get_lobbyStatistics_28() const { return ___lobbyStatistics_28; }
	inline List_1_t6C9FCEA5B81789209C0373EEE6952A0DC27D1459 ** get_address_of_lobbyStatistics_28() { return &___lobbyStatistics_28; }
	inline void set_lobbyStatistics_28(List_1_t6C9FCEA5B81789209C0373EEE6952A0DC27D1459 * value)
	{
		___lobbyStatistics_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lobbyStatistics_28), (void*)value);
	}

	inline static int32_t get_offset_of_U3CLocalPlayerU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CLocalPlayerU3Ek__BackingField_29)); }
	inline Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * get_U3CLocalPlayerU3Ek__BackingField_29() const { return ___U3CLocalPlayerU3Ek__BackingField_29; }
	inline Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 ** get_address_of_U3CLocalPlayerU3Ek__BackingField_29() { return &___U3CLocalPlayerU3Ek__BackingField_29; }
	inline void set_U3CLocalPlayerU3Ek__BackingField_29(Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * value)
	{
		___U3CLocalPlayerU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLocalPlayerU3Ek__BackingField_29), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentRoomU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CCurrentRoomU3Ek__BackingField_30)); }
	inline Room_t5DFC39DD6736A2641374564EC6C31352BE33000D * get_U3CCurrentRoomU3Ek__BackingField_30() const { return ___U3CCurrentRoomU3Ek__BackingField_30; }
	inline Room_t5DFC39DD6736A2641374564EC6C31352BE33000D ** get_address_of_U3CCurrentRoomU3Ek__BackingField_30() { return &___U3CCurrentRoomU3Ek__BackingField_30; }
	inline void set_U3CCurrentRoomU3Ek__BackingField_30(Room_t5DFC39DD6736A2641374564EC6C31352BE33000D * value)
	{
		___U3CCurrentRoomU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCurrentRoomU3Ek__BackingField_30), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPlayersOnMasterCountU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CPlayersOnMasterCountU3Ek__BackingField_31)); }
	inline int32_t get_U3CPlayersOnMasterCountU3Ek__BackingField_31() const { return ___U3CPlayersOnMasterCountU3Ek__BackingField_31; }
	inline int32_t* get_address_of_U3CPlayersOnMasterCountU3Ek__BackingField_31() { return &___U3CPlayersOnMasterCountU3Ek__BackingField_31; }
	inline void set_U3CPlayersOnMasterCountU3Ek__BackingField_31(int32_t value)
	{
		___U3CPlayersOnMasterCountU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CPlayersInRoomsCountU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CPlayersInRoomsCountU3Ek__BackingField_32)); }
	inline int32_t get_U3CPlayersInRoomsCountU3Ek__BackingField_32() const { return ___U3CPlayersInRoomsCountU3Ek__BackingField_32; }
	inline int32_t* get_address_of_U3CPlayersInRoomsCountU3Ek__BackingField_32() { return &___U3CPlayersInRoomsCountU3Ek__BackingField_32; }
	inline void set_U3CPlayersInRoomsCountU3Ek__BackingField_32(int32_t value)
	{
		___U3CPlayersInRoomsCountU3Ek__BackingField_32 = value;
	}

	inline static int32_t get_offset_of_U3CRoomsCountU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CRoomsCountU3Ek__BackingField_33)); }
	inline int32_t get_U3CRoomsCountU3Ek__BackingField_33() const { return ___U3CRoomsCountU3Ek__BackingField_33; }
	inline int32_t* get_address_of_U3CRoomsCountU3Ek__BackingField_33() { return &___U3CRoomsCountU3Ek__BackingField_33; }
	inline void set_U3CRoomsCountU3Ek__BackingField_33(int32_t value)
	{
		___U3CRoomsCountU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_lastJoinType_34() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___lastJoinType_34)); }
	inline int32_t get_lastJoinType_34() const { return ___lastJoinType_34; }
	inline int32_t* get_address_of_lastJoinType_34() { return &___lastJoinType_34; }
	inline void set_lastJoinType_34(int32_t value)
	{
		___lastJoinType_34 = value;
	}

	inline static int32_t get_offset_of_enterRoomParamsCache_35() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___enterRoomParamsCache_35)); }
	inline EnterRoomParams_t35117DAFF025133E50BB2CFA7864669F6CED1942 * get_enterRoomParamsCache_35() const { return ___enterRoomParamsCache_35; }
	inline EnterRoomParams_t35117DAFF025133E50BB2CFA7864669F6CED1942 ** get_address_of_enterRoomParamsCache_35() { return &___enterRoomParamsCache_35; }
	inline void set_enterRoomParamsCache_35(EnterRoomParams_t35117DAFF025133E50BB2CFA7864669F6CED1942 * value)
	{
		___enterRoomParamsCache_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enterRoomParamsCache_35), (void*)value);
	}

	inline static int32_t get_offset_of_failedRoomEntryOperation_36() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___failedRoomEntryOperation_36)); }
	inline OperationResponse_tC67CCFA9FAB3EDF55AA824724BA47D45AB3585FE * get_failedRoomEntryOperation_36() const { return ___failedRoomEntryOperation_36; }
	inline OperationResponse_tC67CCFA9FAB3EDF55AA824724BA47D45AB3585FE ** get_address_of_failedRoomEntryOperation_36() { return &___failedRoomEntryOperation_36; }
	inline void set_failedRoomEntryOperation_36(OperationResponse_tC67CCFA9FAB3EDF55AA824724BA47D45AB3585FE * value)
	{
		___failedRoomEntryOperation_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___failedRoomEntryOperation_36), (void*)value);
	}

	inline static int32_t get_offset_of_friendListRequested_38() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___friendListRequested_38)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_friendListRequested_38() const { return ___friendListRequested_38; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_friendListRequested_38() { return &___friendListRequested_38; }
	inline void set_friendListRequested_38(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___friendListRequested_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___friendListRequested_38), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCloudRegionU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CCloudRegionU3Ek__BackingField_39)); }
	inline String_t* get_U3CCloudRegionU3Ek__BackingField_39() const { return ___U3CCloudRegionU3Ek__BackingField_39; }
	inline String_t** get_address_of_U3CCloudRegionU3Ek__BackingField_39() { return &___U3CCloudRegionU3Ek__BackingField_39; }
	inline void set_U3CCloudRegionU3Ek__BackingField_39(String_t* value)
	{
		___U3CCloudRegionU3Ek__BackingField_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCloudRegionU3Ek__BackingField_39), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentClusterU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___U3CCurrentClusterU3Ek__BackingField_40)); }
	inline String_t* get_U3CCurrentClusterU3Ek__BackingField_40() const { return ___U3CCurrentClusterU3Ek__BackingField_40; }
	inline String_t** get_address_of_U3CCurrentClusterU3Ek__BackingField_40() { return &___U3CCurrentClusterU3Ek__BackingField_40; }
	inline void set_U3CCurrentClusterU3Ek__BackingField_40(String_t* value)
	{
		___U3CCurrentClusterU3Ek__BackingField_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCurrentClusterU3Ek__BackingField_40), (void*)value);
	}

	inline static int32_t get_offset_of_RegionHandler_41() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___RegionHandler_41)); }
	inline RegionHandler_tE1C559FF2DCCA020EC31ACCF3993A434617A15AF * get_RegionHandler_41() const { return ___RegionHandler_41; }
	inline RegionHandler_tE1C559FF2DCCA020EC31ACCF3993A434617A15AF ** get_address_of_RegionHandler_41() { return &___RegionHandler_41; }
	inline void set_RegionHandler_41(RegionHandler_tE1C559FF2DCCA020EC31ACCF3993A434617A15AF * value)
	{
		___RegionHandler_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RegionHandler_41), (void*)value);
	}

	inline static int32_t get_offset_of_callbackTargetChanges_42() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___callbackTargetChanges_42)); }
	inline Queue_1_t2DF167EE7F886C6E1CE292988624D5634AF12DE2 * get_callbackTargetChanges_42() const { return ___callbackTargetChanges_42; }
	inline Queue_1_t2DF167EE7F886C6E1CE292988624D5634AF12DE2 ** get_address_of_callbackTargetChanges_42() { return &___callbackTargetChanges_42; }
	inline void set_callbackTargetChanges_42(Queue_1_t2DF167EE7F886C6E1CE292988624D5634AF12DE2 * value)
	{
		___callbackTargetChanges_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callbackTargetChanges_42), (void*)value);
	}

	inline static int32_t get_offset_of_callbackTargets_43() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A, ___callbackTargets_43)); }
	inline HashSet_1_t20C647F44B285CB7C061931B00ED43A230F311E5 * get_callbackTargets_43() const { return ___callbackTargets_43; }
	inline HashSet_1_t20C647F44B285CB7C061931B00ED43A230F311E5 ** get_address_of_callbackTargets_43() { return &___callbackTargets_43; }
	inline void set_callbackTargets_43(HashSet_1_t20C647F44B285CB7C061931B00ED43A230F311E5 * value)
	{
		___callbackTargets_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callbackTargets_43), (void*)value);
	}
};

struct LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32> Photon.Realtime.LoadBalancingClient::ProtocolToNameServerPort
	Dictionary_2_tBB3FFEFC7BCAAA362AC87C23C01A4A4C98E66541 * ___ProtocolToNameServerPort_11;

public:
	inline static int32_t get_offset_of_ProtocolToNameServerPort_11() { return static_cast<int32_t>(offsetof(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A_StaticFields, ___ProtocolToNameServerPort_11)); }
	inline Dictionary_2_tBB3FFEFC7BCAAA362AC87C23C01A4A4C98E66541 * get_ProtocolToNameServerPort_11() const { return ___ProtocolToNameServerPort_11; }
	inline Dictionary_2_tBB3FFEFC7BCAAA362AC87C23C01A4A4C98E66541 ** get_address_of_ProtocolToNameServerPort_11() { return &___ProtocolToNameServerPort_11; }
	inline void set_ProtocolToNameServerPort_11(Dictionary_2_tBB3FFEFC7BCAAA362AC87C23C01A4A4C98E66541 * value)
	{
		___ProtocolToNameServerPort_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ProtocolToNameServerPort_11), (void*)value);
	}
};


// System.Reflection.ParameterInfo
struct  ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB  : public RuntimeObject
{
public:
	// System.Type System.Reflection.ParameterInfo::ClassImpl
	Type_t * ___ClassImpl_0;
	// System.Object System.Reflection.ParameterInfo::DefaultValueImpl
	RuntimeObject * ___DefaultValueImpl_1;
	// System.Reflection.MemberInfo System.Reflection.ParameterInfo::MemberImpl
	MemberInfo_t * ___MemberImpl_2;
	// System.String System.Reflection.ParameterInfo::NameImpl
	String_t* ___NameImpl_3;
	// System.Int32 System.Reflection.ParameterInfo::PositionImpl
	int32_t ___PositionImpl_4;
	// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::AttrsImpl
	int32_t ___AttrsImpl_5;
	// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.ParameterInfo::marshalAs
	MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020 * ___marshalAs_6;

public:
	inline static int32_t get_offset_of_ClassImpl_0() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___ClassImpl_0)); }
	inline Type_t * get_ClassImpl_0() const { return ___ClassImpl_0; }
	inline Type_t ** get_address_of_ClassImpl_0() { return &___ClassImpl_0; }
	inline void set_ClassImpl_0(Type_t * value)
	{
		___ClassImpl_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ClassImpl_0), (void*)value);
	}

	inline static int32_t get_offset_of_DefaultValueImpl_1() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___DefaultValueImpl_1)); }
	inline RuntimeObject * get_DefaultValueImpl_1() const { return ___DefaultValueImpl_1; }
	inline RuntimeObject ** get_address_of_DefaultValueImpl_1() { return &___DefaultValueImpl_1; }
	inline void set_DefaultValueImpl_1(RuntimeObject * value)
	{
		___DefaultValueImpl_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DefaultValueImpl_1), (void*)value);
	}

	inline static int32_t get_offset_of_MemberImpl_2() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___MemberImpl_2)); }
	inline MemberInfo_t * get_MemberImpl_2() const { return ___MemberImpl_2; }
	inline MemberInfo_t ** get_address_of_MemberImpl_2() { return &___MemberImpl_2; }
	inline void set_MemberImpl_2(MemberInfo_t * value)
	{
		___MemberImpl_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MemberImpl_2), (void*)value);
	}

	inline static int32_t get_offset_of_NameImpl_3() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___NameImpl_3)); }
	inline String_t* get_NameImpl_3() const { return ___NameImpl_3; }
	inline String_t** get_address_of_NameImpl_3() { return &___NameImpl_3; }
	inline void set_NameImpl_3(String_t* value)
	{
		___NameImpl_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NameImpl_3), (void*)value);
	}

	inline static int32_t get_offset_of_PositionImpl_4() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___PositionImpl_4)); }
	inline int32_t get_PositionImpl_4() const { return ___PositionImpl_4; }
	inline int32_t* get_address_of_PositionImpl_4() { return &___PositionImpl_4; }
	inline void set_PositionImpl_4(int32_t value)
	{
		___PositionImpl_4 = value;
	}

	inline static int32_t get_offset_of_AttrsImpl_5() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___AttrsImpl_5)); }
	inline int32_t get_AttrsImpl_5() const { return ___AttrsImpl_5; }
	inline int32_t* get_address_of_AttrsImpl_5() { return &___AttrsImpl_5; }
	inline void set_AttrsImpl_5(int32_t value)
	{
		___AttrsImpl_5 = value;
	}

	inline static int32_t get_offset_of_marshalAs_6() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___marshalAs_6)); }
	inline MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020 * get_marshalAs_6() const { return ___marshalAs_6; }
	inline MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020 ** get_address_of_marshalAs_6() { return &___marshalAs_6; }
	inline void set_marshalAs_6(MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020 * value)
	{
		___marshalAs_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___marshalAs_6), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Reflection.ParameterInfo
struct ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB_marshaled_pinvoke
{
	Type_t * ___ClassImpl_0;
	Il2CppIUnknown* ___DefaultValueImpl_1;
	MemberInfo_t * ___MemberImpl_2;
	char* ___NameImpl_3;
	int32_t ___PositionImpl_4;
	int32_t ___AttrsImpl_5;
	MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020 * ___marshalAs_6;
};
// Native definition for COM marshalling of System.Reflection.ParameterInfo
struct ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB_marshaled_com
{
	Type_t * ___ClassImpl_0;
	Il2CppIUnknown* ___DefaultValueImpl_1;
	MemberInfo_t * ___MemberImpl_2;
	Il2CppChar* ___NameImpl_3;
	int32_t ___PositionImpl_4;
	int32_t ___AttrsImpl_5;
	MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020 * ___marshalAs_6;
};

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};

// Photon.Pun.ServerSettings
struct  ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Photon.Realtime.AppSettings Photon.Pun.ServerSettings::AppSettings
	AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * ___AppSettings_4;
	// Photon.Pun.PunLogLevel Photon.Pun.ServerSettings::PunLogging
	int32_t ___PunLogging_5;
	// System.Boolean Photon.Pun.ServerSettings::EnableSupportLogger
	bool ___EnableSupportLogger_6;
	// System.Boolean Photon.Pun.ServerSettings::RunInBackground
	bool ___RunInBackground_7;
	// System.Boolean Photon.Pun.ServerSettings::StartInOfflineMode
	bool ___StartInOfflineMode_8;
	// System.Collections.Generic.List`1<System.String> Photon.Pun.ServerSettings::RpcList
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___RpcList_9;

public:
	inline static int32_t get_offset_of_AppSettings_4() { return static_cast<int32_t>(offsetof(ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9, ___AppSettings_4)); }
	inline AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * get_AppSettings_4() const { return ___AppSettings_4; }
	inline AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 ** get_address_of_AppSettings_4() { return &___AppSettings_4; }
	inline void set_AppSettings_4(AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * value)
	{
		___AppSettings_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AppSettings_4), (void*)value);
	}

	inline static int32_t get_offset_of_PunLogging_5() { return static_cast<int32_t>(offsetof(ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9, ___PunLogging_5)); }
	inline int32_t get_PunLogging_5() const { return ___PunLogging_5; }
	inline int32_t* get_address_of_PunLogging_5() { return &___PunLogging_5; }
	inline void set_PunLogging_5(int32_t value)
	{
		___PunLogging_5 = value;
	}

	inline static int32_t get_offset_of_EnableSupportLogger_6() { return static_cast<int32_t>(offsetof(ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9, ___EnableSupportLogger_6)); }
	inline bool get_EnableSupportLogger_6() const { return ___EnableSupportLogger_6; }
	inline bool* get_address_of_EnableSupportLogger_6() { return &___EnableSupportLogger_6; }
	inline void set_EnableSupportLogger_6(bool value)
	{
		___EnableSupportLogger_6 = value;
	}

	inline static int32_t get_offset_of_RunInBackground_7() { return static_cast<int32_t>(offsetof(ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9, ___RunInBackground_7)); }
	inline bool get_RunInBackground_7() const { return ___RunInBackground_7; }
	inline bool* get_address_of_RunInBackground_7() { return &___RunInBackground_7; }
	inline void set_RunInBackground_7(bool value)
	{
		___RunInBackground_7 = value;
	}

	inline static int32_t get_offset_of_StartInOfflineMode_8() { return static_cast<int32_t>(offsetof(ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9, ___StartInOfflineMode_8)); }
	inline bool get_StartInOfflineMode_8() const { return ___StartInOfflineMode_8; }
	inline bool* get_address_of_StartInOfflineMode_8() { return &___StartInOfflineMode_8; }
	inline void set_StartInOfflineMode_8(bool value)
	{
		___StartInOfflineMode_8 = value;
	}

	inline static int32_t get_offset_of_RpcList_9() { return static_cast<int32_t>(offsetof(ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9, ___RpcList_9)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_RpcList_9() const { return ___RpcList_9; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_RpcList_9() { return &___RpcList_9; }
	inline void set_RpcList_9(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___RpcList_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RpcList_9), (void*)value);
	}
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// Photon.Pun.PhotonTransformView
struct  PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Photon.Pun.PhotonTransformView::m_Distance
	float ___m_Distance_4;
	// System.Single Photon.Pun.PhotonTransformView::m_Angle
	float ___m_Angle_5;
	// Photon.Pun.PhotonView Photon.Pun.PhotonTransformView::m_PhotonView
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___m_PhotonView_6;
	// UnityEngine.Vector3 Photon.Pun.PhotonTransformView::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_7;
	// UnityEngine.Vector3 Photon.Pun.PhotonTransformView::m_NetworkPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_NetworkPosition_8;
	// UnityEngine.Vector3 Photon.Pun.PhotonTransformView::m_StoredPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_StoredPosition_9;
	// UnityEngine.Quaternion Photon.Pun.PhotonTransformView::m_NetworkRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_NetworkRotation_10;
	// System.Boolean Photon.Pun.PhotonTransformView::m_SynchronizePosition
	bool ___m_SynchronizePosition_11;
	// System.Boolean Photon.Pun.PhotonTransformView::m_SynchronizeRotation
	bool ___m_SynchronizeRotation_12;
	// System.Boolean Photon.Pun.PhotonTransformView::m_SynchronizeScale
	bool ___m_SynchronizeScale_13;
	// System.Boolean Photon.Pun.PhotonTransformView::m_firstTake
	bool ___m_firstTake_14;

public:
	inline static int32_t get_offset_of_m_Distance_4() { return static_cast<int32_t>(offsetof(PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F, ___m_Distance_4)); }
	inline float get_m_Distance_4() const { return ___m_Distance_4; }
	inline float* get_address_of_m_Distance_4() { return &___m_Distance_4; }
	inline void set_m_Distance_4(float value)
	{
		___m_Distance_4 = value;
	}

	inline static int32_t get_offset_of_m_Angle_5() { return static_cast<int32_t>(offsetof(PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F, ___m_Angle_5)); }
	inline float get_m_Angle_5() const { return ___m_Angle_5; }
	inline float* get_address_of_m_Angle_5() { return &___m_Angle_5; }
	inline void set_m_Angle_5(float value)
	{
		___m_Angle_5 = value;
	}

	inline static int32_t get_offset_of_m_PhotonView_6() { return static_cast<int32_t>(offsetof(PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F, ___m_PhotonView_6)); }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * get_m_PhotonView_6() const { return ___m_PhotonView_6; }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** get_address_of_m_PhotonView_6() { return &___m_PhotonView_6; }
	inline void set_m_PhotonView_6(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		___m_PhotonView_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PhotonView_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Direction_7() { return static_cast<int32_t>(offsetof(PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F, ___m_Direction_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_7() const { return ___m_Direction_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_7() { return &___m_Direction_7; }
	inline void set_m_Direction_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_7 = value;
	}

	inline static int32_t get_offset_of_m_NetworkPosition_8() { return static_cast<int32_t>(offsetof(PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F, ___m_NetworkPosition_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_NetworkPosition_8() const { return ___m_NetworkPosition_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_NetworkPosition_8() { return &___m_NetworkPosition_8; }
	inline void set_m_NetworkPosition_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_NetworkPosition_8 = value;
	}

	inline static int32_t get_offset_of_m_StoredPosition_9() { return static_cast<int32_t>(offsetof(PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F, ___m_StoredPosition_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_StoredPosition_9() const { return ___m_StoredPosition_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_StoredPosition_9() { return &___m_StoredPosition_9; }
	inline void set_m_StoredPosition_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_StoredPosition_9 = value;
	}

	inline static int32_t get_offset_of_m_NetworkRotation_10() { return static_cast<int32_t>(offsetof(PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F, ___m_NetworkRotation_10)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_NetworkRotation_10() const { return ___m_NetworkRotation_10; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_NetworkRotation_10() { return &___m_NetworkRotation_10; }
	inline void set_m_NetworkRotation_10(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_NetworkRotation_10 = value;
	}

	inline static int32_t get_offset_of_m_SynchronizePosition_11() { return static_cast<int32_t>(offsetof(PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F, ___m_SynchronizePosition_11)); }
	inline bool get_m_SynchronizePosition_11() const { return ___m_SynchronizePosition_11; }
	inline bool* get_address_of_m_SynchronizePosition_11() { return &___m_SynchronizePosition_11; }
	inline void set_m_SynchronizePosition_11(bool value)
	{
		___m_SynchronizePosition_11 = value;
	}

	inline static int32_t get_offset_of_m_SynchronizeRotation_12() { return static_cast<int32_t>(offsetof(PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F, ___m_SynchronizeRotation_12)); }
	inline bool get_m_SynchronizeRotation_12() const { return ___m_SynchronizeRotation_12; }
	inline bool* get_address_of_m_SynchronizeRotation_12() { return &___m_SynchronizeRotation_12; }
	inline void set_m_SynchronizeRotation_12(bool value)
	{
		___m_SynchronizeRotation_12 = value;
	}

	inline static int32_t get_offset_of_m_SynchronizeScale_13() { return static_cast<int32_t>(offsetof(PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F, ___m_SynchronizeScale_13)); }
	inline bool get_m_SynchronizeScale_13() const { return ___m_SynchronizeScale_13; }
	inline bool* get_address_of_m_SynchronizeScale_13() { return &___m_SynchronizeScale_13; }
	inline void set_m_SynchronizeScale_13(bool value)
	{
		___m_SynchronizeScale_13 = value;
	}

	inline static int32_t get_offset_of_m_firstTake_14() { return static_cast<int32_t>(offsetof(PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F, ___m_firstTake_14)); }
	inline bool get_m_firstTake_14() const { return ___m_firstTake_14; }
	inline bool* get_address_of_m_firstTake_14() { return &___m_firstTake_14; }
	inline void set_m_firstTake_14(bool value)
	{
		___m_firstTake_14 = value;
	}
};


// Photon.Pun.PhotonTransformViewClassic
struct  PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Photon.Pun.PhotonTransformViewPositionModel Photon.Pun.PhotonTransformViewClassic::m_PositionModel
	PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * ___m_PositionModel_4;
	// Photon.Pun.PhotonTransformViewRotationModel Photon.Pun.PhotonTransformViewClassic::m_RotationModel
	PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * ___m_RotationModel_5;
	// Photon.Pun.PhotonTransformViewScaleModel Photon.Pun.PhotonTransformViewClassic::m_ScaleModel
	PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * ___m_ScaleModel_6;
	// Photon.Pun.PhotonTransformViewPositionControl Photon.Pun.PhotonTransformViewClassic::m_PositionControl
	PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * ___m_PositionControl_7;
	// Photon.Pun.PhotonTransformViewRotationControl Photon.Pun.PhotonTransformViewClassic::m_RotationControl
	PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * ___m_RotationControl_8;
	// Photon.Pun.PhotonTransformViewScaleControl Photon.Pun.PhotonTransformViewClassic::m_ScaleControl
	PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * ___m_ScaleControl_9;
	// Photon.Pun.PhotonView Photon.Pun.PhotonTransformViewClassic::m_PhotonView
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___m_PhotonView_10;
	// System.Boolean Photon.Pun.PhotonTransformViewClassic::m_ReceivedNetworkUpdate
	bool ___m_ReceivedNetworkUpdate_11;
	// System.Boolean Photon.Pun.PhotonTransformViewClassic::m_firstTake
	bool ___m_firstTake_12;

public:
	inline static int32_t get_offset_of_m_PositionModel_4() { return static_cast<int32_t>(offsetof(PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545, ___m_PositionModel_4)); }
	inline PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * get_m_PositionModel_4() const { return ___m_PositionModel_4; }
	inline PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 ** get_address_of_m_PositionModel_4() { return &___m_PositionModel_4; }
	inline void set_m_PositionModel_4(PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * value)
	{
		___m_PositionModel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PositionModel_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_RotationModel_5() { return static_cast<int32_t>(offsetof(PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545, ___m_RotationModel_5)); }
	inline PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * get_m_RotationModel_5() const { return ___m_RotationModel_5; }
	inline PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D ** get_address_of_m_RotationModel_5() { return &___m_RotationModel_5; }
	inline void set_m_RotationModel_5(PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * value)
	{
		___m_RotationModel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RotationModel_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_ScaleModel_6() { return static_cast<int32_t>(offsetof(PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545, ___m_ScaleModel_6)); }
	inline PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * get_m_ScaleModel_6() const { return ___m_ScaleModel_6; }
	inline PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 ** get_address_of_m_ScaleModel_6() { return &___m_ScaleModel_6; }
	inline void set_m_ScaleModel_6(PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * value)
	{
		___m_ScaleModel_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ScaleModel_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_PositionControl_7() { return static_cast<int32_t>(offsetof(PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545, ___m_PositionControl_7)); }
	inline PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * get_m_PositionControl_7() const { return ___m_PositionControl_7; }
	inline PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 ** get_address_of_m_PositionControl_7() { return &___m_PositionControl_7; }
	inline void set_m_PositionControl_7(PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * value)
	{
		___m_PositionControl_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PositionControl_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_RotationControl_8() { return static_cast<int32_t>(offsetof(PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545, ___m_RotationControl_8)); }
	inline PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * get_m_RotationControl_8() const { return ___m_RotationControl_8; }
	inline PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE ** get_address_of_m_RotationControl_8() { return &___m_RotationControl_8; }
	inline void set_m_RotationControl_8(PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * value)
	{
		___m_RotationControl_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RotationControl_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_ScaleControl_9() { return static_cast<int32_t>(offsetof(PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545, ___m_ScaleControl_9)); }
	inline PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * get_m_ScaleControl_9() const { return ___m_ScaleControl_9; }
	inline PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E ** get_address_of_m_ScaleControl_9() { return &___m_ScaleControl_9; }
	inline void set_m_ScaleControl_9(PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * value)
	{
		___m_ScaleControl_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ScaleControl_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_PhotonView_10() { return static_cast<int32_t>(offsetof(PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545, ___m_PhotonView_10)); }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * get_m_PhotonView_10() const { return ___m_PhotonView_10; }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** get_address_of_m_PhotonView_10() { return &___m_PhotonView_10; }
	inline void set_m_PhotonView_10(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		___m_PhotonView_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PhotonView_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_ReceivedNetworkUpdate_11() { return static_cast<int32_t>(offsetof(PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545, ___m_ReceivedNetworkUpdate_11)); }
	inline bool get_m_ReceivedNetworkUpdate_11() const { return ___m_ReceivedNetworkUpdate_11; }
	inline bool* get_address_of_m_ReceivedNetworkUpdate_11() { return &___m_ReceivedNetworkUpdate_11; }
	inline void set_m_ReceivedNetworkUpdate_11(bool value)
	{
		___m_ReceivedNetworkUpdate_11 = value;
	}

	inline static int32_t get_offset_of_m_firstTake_12() { return static_cast<int32_t>(offsetof(PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545, ___m_firstTake_12)); }
	inline bool get_m_firstTake_12() const { return ___m_firstTake_12; }
	inline bool* get_address_of_m_firstTake_12() { return &___m_firstTake_12; }
	inline void set_m_firstTake_12(bool value)
	{
		___m_firstTake_12 = value;
	}
};


// Photon.Pun.PhotonView
struct  PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Photon.Pun.PhotonView::ownerId
	int32_t ___ownerId_4;
	// System.Byte Photon.Pun.PhotonView::Group
	uint8_t ___Group_5;
	// System.Boolean Photon.Pun.PhotonView::mixedModeIsReliable
	bool ___mixedModeIsReliable_6;
	// System.Boolean Photon.Pun.PhotonView::OwnershipWasTransfered
	bool ___OwnershipWasTransfered_7;
	// System.Int32 Photon.Pun.PhotonView::prefixField
	int32_t ___prefixField_8;
	// System.Object[] Photon.Pun.PhotonView::instantiationDataField
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___instantiationDataField_9;
	// System.Collections.Generic.List`1<System.Object> Photon.Pun.PhotonView::lastOnSerializeDataSent
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___lastOnSerializeDataSent_10;
	// System.Collections.Generic.List`1<System.Object> Photon.Pun.PhotonView::syncValues
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___syncValues_11;
	// System.Object[] Photon.Pun.PhotonView::lastOnSerializeDataReceived
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___lastOnSerializeDataReceived_12;
	// Photon.Pun.ViewSynchronization Photon.Pun.PhotonView::Synchronization
	int32_t ___Synchronization_13;
	// Photon.Pun.OwnershipOption Photon.Pun.PhotonView::OwnershipTransfer
	int32_t ___OwnershipTransfer_14;
	// System.Collections.Generic.List`1<UnityEngine.Component> Photon.Pun.PhotonView::ObservedComponents
	List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * ___ObservedComponents_15;
	// System.Int32 Photon.Pun.PhotonView::viewIdField
	int32_t ___viewIdField_16;
	// System.Int32 Photon.Pun.PhotonView::InstantiationId
	int32_t ___InstantiationId_17;
	// System.Boolean Photon.Pun.PhotonView::didAwake
	bool ___didAwake_18;
	// System.Boolean Photon.Pun.PhotonView::isRuntimeInstantiated
	bool ___isRuntimeInstantiated_19;
	// System.Boolean Photon.Pun.PhotonView::removedFromLocalViewList
	bool ___removedFromLocalViewList_20;
	// UnityEngine.MonoBehaviour[] Photon.Pun.PhotonView::RpcMonoBehaviours
	MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* ___RpcMonoBehaviours_21;

public:
	inline static int32_t get_offset_of_ownerId_4() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___ownerId_4)); }
	inline int32_t get_ownerId_4() const { return ___ownerId_4; }
	inline int32_t* get_address_of_ownerId_4() { return &___ownerId_4; }
	inline void set_ownerId_4(int32_t value)
	{
		___ownerId_4 = value;
	}

	inline static int32_t get_offset_of_Group_5() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___Group_5)); }
	inline uint8_t get_Group_5() const { return ___Group_5; }
	inline uint8_t* get_address_of_Group_5() { return &___Group_5; }
	inline void set_Group_5(uint8_t value)
	{
		___Group_5 = value;
	}

	inline static int32_t get_offset_of_mixedModeIsReliable_6() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___mixedModeIsReliable_6)); }
	inline bool get_mixedModeIsReliable_6() const { return ___mixedModeIsReliable_6; }
	inline bool* get_address_of_mixedModeIsReliable_6() { return &___mixedModeIsReliable_6; }
	inline void set_mixedModeIsReliable_6(bool value)
	{
		___mixedModeIsReliable_6 = value;
	}

	inline static int32_t get_offset_of_OwnershipWasTransfered_7() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___OwnershipWasTransfered_7)); }
	inline bool get_OwnershipWasTransfered_7() const { return ___OwnershipWasTransfered_7; }
	inline bool* get_address_of_OwnershipWasTransfered_7() { return &___OwnershipWasTransfered_7; }
	inline void set_OwnershipWasTransfered_7(bool value)
	{
		___OwnershipWasTransfered_7 = value;
	}

	inline static int32_t get_offset_of_prefixField_8() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___prefixField_8)); }
	inline int32_t get_prefixField_8() const { return ___prefixField_8; }
	inline int32_t* get_address_of_prefixField_8() { return &___prefixField_8; }
	inline void set_prefixField_8(int32_t value)
	{
		___prefixField_8 = value;
	}

	inline static int32_t get_offset_of_instantiationDataField_9() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___instantiationDataField_9)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_instantiationDataField_9() const { return ___instantiationDataField_9; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_instantiationDataField_9() { return &___instantiationDataField_9; }
	inline void set_instantiationDataField_9(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___instantiationDataField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instantiationDataField_9), (void*)value);
	}

	inline static int32_t get_offset_of_lastOnSerializeDataSent_10() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___lastOnSerializeDataSent_10)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_lastOnSerializeDataSent_10() const { return ___lastOnSerializeDataSent_10; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_lastOnSerializeDataSent_10() { return &___lastOnSerializeDataSent_10; }
	inline void set_lastOnSerializeDataSent_10(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___lastOnSerializeDataSent_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastOnSerializeDataSent_10), (void*)value);
	}

	inline static int32_t get_offset_of_syncValues_11() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___syncValues_11)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_syncValues_11() const { return ___syncValues_11; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_syncValues_11() { return &___syncValues_11; }
	inline void set_syncValues_11(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___syncValues_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___syncValues_11), (void*)value);
	}

	inline static int32_t get_offset_of_lastOnSerializeDataReceived_12() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___lastOnSerializeDataReceived_12)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_lastOnSerializeDataReceived_12() const { return ___lastOnSerializeDataReceived_12; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_lastOnSerializeDataReceived_12() { return &___lastOnSerializeDataReceived_12; }
	inline void set_lastOnSerializeDataReceived_12(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___lastOnSerializeDataReceived_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastOnSerializeDataReceived_12), (void*)value);
	}

	inline static int32_t get_offset_of_Synchronization_13() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___Synchronization_13)); }
	inline int32_t get_Synchronization_13() const { return ___Synchronization_13; }
	inline int32_t* get_address_of_Synchronization_13() { return &___Synchronization_13; }
	inline void set_Synchronization_13(int32_t value)
	{
		___Synchronization_13 = value;
	}

	inline static int32_t get_offset_of_OwnershipTransfer_14() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___OwnershipTransfer_14)); }
	inline int32_t get_OwnershipTransfer_14() const { return ___OwnershipTransfer_14; }
	inline int32_t* get_address_of_OwnershipTransfer_14() { return &___OwnershipTransfer_14; }
	inline void set_OwnershipTransfer_14(int32_t value)
	{
		___OwnershipTransfer_14 = value;
	}

	inline static int32_t get_offset_of_ObservedComponents_15() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___ObservedComponents_15)); }
	inline List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * get_ObservedComponents_15() const { return ___ObservedComponents_15; }
	inline List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 ** get_address_of_ObservedComponents_15() { return &___ObservedComponents_15; }
	inline void set_ObservedComponents_15(List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * value)
	{
		___ObservedComponents_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ObservedComponents_15), (void*)value);
	}

	inline static int32_t get_offset_of_viewIdField_16() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___viewIdField_16)); }
	inline int32_t get_viewIdField_16() const { return ___viewIdField_16; }
	inline int32_t* get_address_of_viewIdField_16() { return &___viewIdField_16; }
	inline void set_viewIdField_16(int32_t value)
	{
		___viewIdField_16 = value;
	}

	inline static int32_t get_offset_of_InstantiationId_17() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___InstantiationId_17)); }
	inline int32_t get_InstantiationId_17() const { return ___InstantiationId_17; }
	inline int32_t* get_address_of_InstantiationId_17() { return &___InstantiationId_17; }
	inline void set_InstantiationId_17(int32_t value)
	{
		___InstantiationId_17 = value;
	}

	inline static int32_t get_offset_of_didAwake_18() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___didAwake_18)); }
	inline bool get_didAwake_18() const { return ___didAwake_18; }
	inline bool* get_address_of_didAwake_18() { return &___didAwake_18; }
	inline void set_didAwake_18(bool value)
	{
		___didAwake_18 = value;
	}

	inline static int32_t get_offset_of_isRuntimeInstantiated_19() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___isRuntimeInstantiated_19)); }
	inline bool get_isRuntimeInstantiated_19() const { return ___isRuntimeInstantiated_19; }
	inline bool* get_address_of_isRuntimeInstantiated_19() { return &___isRuntimeInstantiated_19; }
	inline void set_isRuntimeInstantiated_19(bool value)
	{
		___isRuntimeInstantiated_19 = value;
	}

	inline static int32_t get_offset_of_removedFromLocalViewList_20() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___removedFromLocalViewList_20)); }
	inline bool get_removedFromLocalViewList_20() const { return ___removedFromLocalViewList_20; }
	inline bool* get_address_of_removedFromLocalViewList_20() { return &___removedFromLocalViewList_20; }
	inline void set_removedFromLocalViewList_20(bool value)
	{
		___removedFromLocalViewList_20 = value;
	}

	inline static int32_t get_offset_of_RpcMonoBehaviours_21() { return static_cast<int32_t>(offsetof(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B, ___RpcMonoBehaviours_21)); }
	inline MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* get_RpcMonoBehaviours_21() const { return ___RpcMonoBehaviours_21; }
	inline MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A** get_address_of_RpcMonoBehaviours_21() { return &___RpcMonoBehaviours_21; }
	inline void set_RpcMonoBehaviours_21(MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* value)
	{
		___RpcMonoBehaviours_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RpcMonoBehaviours_21), (void*)value);
	}
};


// Photon.Realtime.ConnectionHandler
struct  ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Photon.Realtime.LoadBalancingClient Photon.Realtime.ConnectionHandler::<Client>k__BackingField
	LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A * ___U3CClientU3Ek__BackingField_4;
	// System.Byte Photon.Realtime.ConnectionHandler::fallbackThreadId
	uint8_t ___fallbackThreadId_5;
	// System.Boolean Photon.Realtime.ConnectionHandler::didSendAcks
	bool ___didSendAcks_6;
	// System.Int32 Photon.Realtime.ConnectionHandler::startedAckingTimestamp
	int32_t ___startedAckingTimestamp_7;
	// System.Int32 Photon.Realtime.ConnectionHandler::deltaSinceStartedToAck
	int32_t ___deltaSinceStartedToAck_8;
	// System.Int32 Photon.Realtime.ConnectionHandler::KeepAliveInBackground
	int32_t ___KeepAliveInBackground_9;
	// System.Int32 Photon.Realtime.ConnectionHandler::<CountSendAcksOnly>k__BackingField
	int32_t ___U3CCountSendAcksOnlyU3Ek__BackingField_10;
	// System.Boolean Photon.Realtime.ConnectionHandler::ApplyDontDestroyOnLoad
	bool ___ApplyDontDestroyOnLoad_11;

public:
	inline static int32_t get_offset_of_U3CClientU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0, ___U3CClientU3Ek__BackingField_4)); }
	inline LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A * get_U3CClientU3Ek__BackingField_4() const { return ___U3CClientU3Ek__BackingField_4; }
	inline LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A ** get_address_of_U3CClientU3Ek__BackingField_4() { return &___U3CClientU3Ek__BackingField_4; }
	inline void set_U3CClientU3Ek__BackingField_4(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A * value)
	{
		___U3CClientU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CClientU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_fallbackThreadId_5() { return static_cast<int32_t>(offsetof(ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0, ___fallbackThreadId_5)); }
	inline uint8_t get_fallbackThreadId_5() const { return ___fallbackThreadId_5; }
	inline uint8_t* get_address_of_fallbackThreadId_5() { return &___fallbackThreadId_5; }
	inline void set_fallbackThreadId_5(uint8_t value)
	{
		___fallbackThreadId_5 = value;
	}

	inline static int32_t get_offset_of_didSendAcks_6() { return static_cast<int32_t>(offsetof(ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0, ___didSendAcks_6)); }
	inline bool get_didSendAcks_6() const { return ___didSendAcks_6; }
	inline bool* get_address_of_didSendAcks_6() { return &___didSendAcks_6; }
	inline void set_didSendAcks_6(bool value)
	{
		___didSendAcks_6 = value;
	}

	inline static int32_t get_offset_of_startedAckingTimestamp_7() { return static_cast<int32_t>(offsetof(ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0, ___startedAckingTimestamp_7)); }
	inline int32_t get_startedAckingTimestamp_7() const { return ___startedAckingTimestamp_7; }
	inline int32_t* get_address_of_startedAckingTimestamp_7() { return &___startedAckingTimestamp_7; }
	inline void set_startedAckingTimestamp_7(int32_t value)
	{
		___startedAckingTimestamp_7 = value;
	}

	inline static int32_t get_offset_of_deltaSinceStartedToAck_8() { return static_cast<int32_t>(offsetof(ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0, ___deltaSinceStartedToAck_8)); }
	inline int32_t get_deltaSinceStartedToAck_8() const { return ___deltaSinceStartedToAck_8; }
	inline int32_t* get_address_of_deltaSinceStartedToAck_8() { return &___deltaSinceStartedToAck_8; }
	inline void set_deltaSinceStartedToAck_8(int32_t value)
	{
		___deltaSinceStartedToAck_8 = value;
	}

	inline static int32_t get_offset_of_KeepAliveInBackground_9() { return static_cast<int32_t>(offsetof(ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0, ___KeepAliveInBackground_9)); }
	inline int32_t get_KeepAliveInBackground_9() const { return ___KeepAliveInBackground_9; }
	inline int32_t* get_address_of_KeepAliveInBackground_9() { return &___KeepAliveInBackground_9; }
	inline void set_KeepAliveInBackground_9(int32_t value)
	{
		___KeepAliveInBackground_9 = value;
	}

	inline static int32_t get_offset_of_U3CCountSendAcksOnlyU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0, ___U3CCountSendAcksOnlyU3Ek__BackingField_10)); }
	inline int32_t get_U3CCountSendAcksOnlyU3Ek__BackingField_10() const { return ___U3CCountSendAcksOnlyU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CCountSendAcksOnlyU3Ek__BackingField_10() { return &___U3CCountSendAcksOnlyU3Ek__BackingField_10; }
	inline void set_U3CCountSendAcksOnlyU3Ek__BackingField_10(int32_t value)
	{
		___U3CCountSendAcksOnlyU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_ApplyDontDestroyOnLoad_11() { return static_cast<int32_t>(offsetof(ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0, ___ApplyDontDestroyOnLoad_11)); }
	inline bool get_ApplyDontDestroyOnLoad_11() const { return ___ApplyDontDestroyOnLoad_11; }
	inline bool* get_address_of_ApplyDontDestroyOnLoad_11() { return &___ApplyDontDestroyOnLoad_11; }
	inline void set_ApplyDontDestroyOnLoad_11(bool value)
	{
		___ApplyDontDestroyOnLoad_11 = value;
	}
};

struct ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0_StaticFields
{
public:
	// System.Boolean Photon.Realtime.ConnectionHandler::AppQuits
	bool ___AppQuits_12;

public:
	inline static int32_t get_offset_of_AppQuits_12() { return static_cast<int32_t>(offsetof(ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0_StaticFields, ___AppQuits_12)); }
	inline bool get_AppQuits_12() const { return ___AppQuits_12; }
	inline bool* get_address_of_AppQuits_12() { return &___AppQuits_12; }
	inline void set_AppQuits_12(bool value)
	{
		___AppQuits_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * m_Items[1];

public:
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB * m_Items[1];

public:
	inline ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// Photon.Pun.PhotonView[]
struct PhotonViewU5BU5D_t79E798C13CF3CB9516F65B6A4C1E65491FF5CA27  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * m_Items[1];

public:
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Queue_1__ctor_mE0E9CF1FAAEBD411236EF5238DF26D79E380C394_gshared (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_gshared_inline (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Peek()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Queue_1_Peek_mC6F614DA3810E0A0FB0F1397A82039A7939A2442_gshared (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Enqueue(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Queue_1_Enqueue_m1D4D0569B64A37FF041C5EF8CE19CB5210E2ED2F_gshared (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___item0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Dequeue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Queue_1_Dequeue_mF86F3E89DD24E5CC47AF827C6DA43A4062F9A344_gshared (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponents<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Component_GetComponents_TisRuntimeObject_mDC1D023FE07F73E1E8433D1E896D5F1250180C10_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m3455807C552312C60038DF52EF328C3687442DE3_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, RuntimeObject * ___key0, RuntimeObject ** ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m466D001F105E25DEB5C9BCB17837EE92A27FDE93_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* GameObject_GetComponentsInChildren_TisRuntimeObject_mFF238916BEA468E58A72E7C1EE6F5D9729CA4D43_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___includeInactive0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2C7E51568033239B506E15E7804A0B8658246498_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, const RuntimeMethod* method);

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
inline void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3 (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_realtimeSinceStartup_mCA1086EC9DFCF135F77BC46D3B7127711EA3DE03 (const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
inline int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07 (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
inline void List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared)(__this, method);
}
// System.Int32 UnityEngine.Time::get_frameCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Time_get_frameCount_m97573E267B487B8FD4BF37615AFC19BED7B4E436 (const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonStreamQueue::BeginWritePackage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonStreamQueue_BeginWritePackage_m1422FC9FA4E756A2F8A611B14B971A60B0C90D13 (PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
inline void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138 (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, RuntimeObject *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
inline RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// System.Void Photon.Pun.PhotonStream::SendNext(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonStream_SendNext_mCDF0BEFCFF338F4A19ECFB8D25C67DAD2AA07712 (PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Object Photon.Pun.PhotonStream::ReceiveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * PhotonStream_ReceiveNext_m8D9908B4B3BFEA0C30F32C2F8CBB89FCA63357A9 (PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Photon.Pun.PhotonView>()
inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * Component_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_m347A176252862D6CD26E7E6B5E9EAF8F4159273B (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2 (const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (const RuntimeMethod* method);
// System.Boolean Photon.Pun.PhotonView::get_IsMine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhotonView_get_IsMine_mFE1D0AB5684F18073518777C1D8FF764CB335793 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method);
// System.Int32 Photon.Pun.PhotonNetwork::get_SerializationRate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhotonNetwork_get_SerializationRate_m1121F935DAB7130246C63A55847F75EB6465B294 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_MoveTowards_mA288BB5AA73DDA9CA76EDC11F339BAFDA1E4FF45 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___current0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___target1, float ___maxDistanceDelta2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::RotateTowards(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_RotateTowards_m3AFE60D0BA85FC9B7056433278144249942DCE91 (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___from0, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___to1, float ___maxDegreesDelta2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value0, const RuntimeMethod* method);
// System.Boolean Photon.Pun.PhotonStream::get_IsWriting()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool PhotonStream_get_IsWriting_mB16D73A32F0C3C1CE9272057F667CE15C1DBC458_inline (PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Double Photon.Pun.PhotonNetwork::get_Time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double PhotonNetwork_get_Time_mDFD12A6E273C6B14118DD8E6DA3EE24C489487F7 (const RuntimeMethod* method);
// System.Double Photon.Pun.PhotonMessageInfo::get_SentServerTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double PhotonMessageInfo_get_SentServerTime_mF1D7A58C63929E6E466987F5922AA6FBC298B45D (PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Distance_mE316E10B9B319A5C2A29F86E028740FD528149E7 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Quaternion_Angle_m09599D660B724D330E5C7FE2FB1C8716161B3DD1 (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___a0, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewPositionControl::.ctor(Photon.Pun.PhotonTransformViewPositionModel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewPositionControl__ctor_mCEAF26452CE4B62915C473A91E0ACB05230312D7 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * ___model0, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewRotationControl::.ctor(Photon.Pun.PhotonTransformViewRotationModel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewRotationControl__ctor_m5350996A5B1D11CE80C1FF8275ACE0845D0E8AEF (PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * __this, PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * ___model0, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewScaleControl::.ctor(Photon.Pun.PhotonTransformViewScaleModel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewScaleControl__ctor_mEFFA198BF98C3BFE37573865DD22C312FAEC20A1 (PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * __this, PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * ___model0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Boolean Photon.Pun.PhotonNetwork::get_IsConnectedAndReady()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhotonNetwork_get_IsConnectedAndReady_mAB2CC497386B5EDADDDA63962C8973AAD392EDE6 (const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewClassic::UpdatePosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewClassic_UpdatePosition_mC5C28116F79ED3B7BA31C6E85CF4E976DD172BB8 (PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545 * __this, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewClassic::UpdateRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewClassic_UpdateRotation_m25AE2AD6E2E2492F7B7A2E50EA1B078FA2E55261 (PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545 * __this, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewClassic::UpdateScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewClassic_UpdateScale_m7C8DAE1D9A77FED238DAAAB56FA149ADEE02F501 (PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewPositionControl::UpdatePosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewPositionControl_UpdatePosition_m9D1775F2DE7FFE494E96F280CF4DFAAB65FE8429 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentPosition0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion Photon.Pun.PhotonTransformViewRotationControl::GetRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  PhotonTransformViewRotationControl_GetRotation_m8573B3D9871305C20D1023A135BA04AEA0D3EFF0 (PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___currentRotation0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewScaleControl::GetScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewScaleControl_GetScale_m0826273DFB50810E7B8E8CE8D05DFA2E67D8D251 (PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentScale0, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewPositionControl::SetSynchronizedValues(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewPositionControl_SetSynchronizedValues_m00E204C877A5290D6B9FF704B25ABB1188ECD69C (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___speed0, float ___turnSpeed1, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewPositionControl::OnPhotonSerializeView(UnityEngine.Vector3,Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewPositionControl_OnPhotonSerializeView_mEADE2EB3B08D87FE97F122AAEC97FF83F63A5B51 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentPosition0, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream1, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info2, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewRotationControl::OnPhotonSerializeView(UnityEngine.Quaternion,Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewRotationControl_OnPhotonSerializeView_m817CD4BE62ABA98E2C071C8AFF0121BB93AFDEED (PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___currentRotation0, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream1, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info2, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewScaleControl::OnPhotonSerializeView(UnityEngine.Vector3,Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewScaleControl_OnPhotonSerializeView_mB03F0D37841831CA9A6664102D8A16F4F8517F7C (PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentScale0, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream1, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info2, const RuntimeMethod* method);
// System.Boolean Photon.Pun.PhotonStream::get_IsReading()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhotonStream_get_IsReading_mB1BE45F09FE0C49E9DCEF6D70E8A0C436949B0D5 (PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewPositionControl::GetNetworkPosition()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewPositionControl_GetNetworkPosition_m047475370A825DB4FCAB3BAB86E6029CC98C89A1_inline (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion Photon.Pun.PhotonTransformViewRotationControl::GetNetworkRotation()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  PhotonTransformViewRotationControl_GetNetworkRotation_m9C232BEB12E8EBF1A79A4331A24363F5B30CCE14_inline (PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewScaleControl::GetNetworkScale()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewScaleControl_GetNetworkScale_mA0936FD6D72F6BF7EDE3966230D0AC367B81EB36_inline (PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * __this, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewPositionModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewPositionModel__ctor_m91498B472599A9EDB5163567647A4D8C9F78A301 (PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * __this, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewRotationModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewRotationModel__ctor_mF3032CE5619AA6AE9A758EB850BDD50AB8E8FC4C (PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * __this, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewScaleModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewScaleModel__ctor_m66436933C2943911D2430F3F8DEADDFF696BEB6E (PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::.ctor()
inline void Queue_1__ctor_mE0E9CF1FAAEBD411236EF5238DF26D79E380C394 (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * __this, const RuntimeMethod* method)
{
	((  void (*) (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE *, const RuntimeMethod*))Queue_1__ctor_mE0E9CF1FAAEBD411236EF5238DF26D79E380C394_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::get_Count()
inline int32_t Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_inline (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE *, const RuntimeMethod*))Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Peek()
inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Queue_1_Peek_mC6F614DA3810E0A0FB0F1397A82039A7939A2442 (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * __this, const RuntimeMethod* method)
{
	return ((  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  (*) (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE *, const RuntimeMethod*))Queue_1_Peek_mC6F614DA3810E0A0FB0F1397A82039A7939A2442_gshared)(__this, method);
}
// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewPositionControl::GetExtrapolatedPositionOffset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewPositionControl_GetExtrapolatedPositionOffset_mFE9B782C8EDB94A36173C1559B72EF14790690C9 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewPositionControl::GetOldestStoredNetworkPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m4982592135EA10FACC90E0A78B3423BCB1C84405 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, float ___t2, const RuntimeMethod* method);
// System.Int32 Photon.Pun.PhotonNetwork::GetPing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhotonNetwork_GetPing_m19C66FE34028DB920307BB89AFF5E954C57FA165 (const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewPositionControl::SerializeData(UnityEngine.Vector3,Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewPositionControl_SerializeData_m46A84222F254F10D91A0762D0A70DAC9F4BF0CC8 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentPosition0, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream1, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info2, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonTransformViewPositionControl::DeserializeData(Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewPositionControl_DeserializeData_m9D749828D57C78D6AD4443D66E8DCFBC6D0423A3 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream0, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Enqueue(!0)
inline void Queue_1_Enqueue_m1D4D0569B64A37FF041C5EF8CE19CB5210E2ED2F (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___item0, const RuntimeMethod* method)
{
	((  void (*) (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 , const RuntimeMethod*))Queue_1_Enqueue_m1D4D0569B64A37FF041C5EF8CE19CB5210E2ED2F_gshared)(__this, ___item0, method);
}
// !0 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Dequeue()
inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Queue_1_Dequeue_mF86F3E89DD24E5CC47AF827C6DA43A4062F9A344 (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * __this, const RuntimeMethod* method)
{
	return ((  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  (*) (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE *, const RuntimeMethod*))Queue_1_Dequeue_mF86F3E89DD24E5CC47AF827C6DA43A4062F9A344_gshared)(__this, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Lerp_m749B3988EE2EF387CC9BFB76C81B7465A7534E27 (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___a0, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___b1, float ___t2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB (const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonNetwork::RegisterPhotonView(Photon.Pun.PhotonView)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonNetwork_RegisterPhotonView_m4E523EEA4FE46B919ACBC4EEB2FF76AC2ED5A25F (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___netView0, const RuntimeMethod* method);
// System.Int32 Photon.Pun.PhotonView::get_CreatorActorNr()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhotonView_get_CreatorActorNr_m182100823912B7A313C17D0844D599BFAAA03629 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method);
// Photon.Realtime.Room Photon.Pun.PhotonNetwork::get_CurrentRoom()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Room_t5DFC39DD6736A2641374564EC6C31352BE33000D * PhotonNetwork_get_CurrentRoom_m493DA29D80F75DC0BEFF3125207AE9101C0100A1 (const RuntimeMethod* method);
// System.Int32 Photon.Pun.PhotonView::get_OwnerActorNr()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhotonView_get_OwnerActorNr_m297F5F405EF3ACE0A262F942FF01357EE4BDE886 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method);
// System.Int32 Photon.Pun.PhotonView::get_ViewID()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PhotonView_get_ViewID_mC15D092CC4BFE8638C31F9622B7DF6894E30D3D9_inline (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method);
// Photon.Realtime.Player Photon.Pun.PhotonNetwork::get_LocalPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * PhotonNetwork_get_LocalPlayer_m06E2D9682B713C828A00C0BD4F03A0CBC1E2D1B6 (const RuntimeMethod* method);
// System.Boolean Photon.Pun.PhotonView::get_IsOwnerActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhotonView_get_IsOwnerActive_mA17BFB41B289C7CD1A75B2AA27DAB90A69FEC87F (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method);
// Photon.Realtime.Player Photon.Pun.PhotonNetwork::get_MasterClient()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * PhotonNetwork_get_MasterClient_mE95EB702C5632D7DDADF199B4B9ECD4B45556B65 (const RuntimeMethod* method);
// Photon.Realtime.Player Photon.Pun.PhotonView::get_Owner()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * PhotonView_get_Owner_mD339E981ADADF5A7F88F6D4F530F1EB8F1C34DEE (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method);
// System.Int32 Photon.Realtime.Player::get_ActorNumber()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t Player_get_ActorNumber_m02157560C558C6007C8146F1FCDA7E759AD0CF1D_inline (Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * __this, const RuntimeMethod* method);
// System.Boolean Photon.Realtime.Player::get_IsInactive()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool Player_get_IsInactive_m565C1FB590912C70E7720C6E1EFFFD5EE755C403_inline (Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * __this, const RuntimeMethod* method);
// System.Boolean Photon.Pun.PhotonNetwork::get_IsMasterClient()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhotonNetwork_get_IsMasterClient_m7EE228BA31428CCA42C74D8805DD9B3A951E7E57 (const RuntimeMethod* method);
// System.Boolean Photon.Pun.PhotonNetwork::LocalCleanPhotonView(Photon.Pun.PhotonView)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhotonNetwork_LocalCleanPhotonView_m843AF475A4D66F8224E414855427B78D2F14D9B2 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___view0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonNetwork::RequestOwnership(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonNetwork_RequestOwnership_m878535F79D0B82193E6FDAE66B6C29467A8AC4C5 (int32_t ___viewID0, int32_t ___fromOwner1, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonView::TransferOwnership(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_TransferOwnership_mE63F4A1B73D7E89DEC4261C163880A3BEB5F5B76 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, int32_t ___newOwnerId0, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonNetwork::TransferOwnership(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonNetwork_TransferOwnership_m4A9E407E53070DEF7479C557BCDCD18E1E085486 (int32_t ___viewID0, int32_t ___playerID1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count()
inline int32_t List_1_get_Count_m9EFB9DB6274EEB0D4B5050480FE1E8CA56B85534_inline (List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32)
inline Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * List_1_get_Item_mA6C019255203C3A859BB0856B40200EA1E5F8EDA_inline (List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * (*) (List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// System.Void Photon.Pun.PhotonView::SerializeComponent(UnityEngine.Component,Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_SerializeComponent_m745D96F1F125E057101C1C52E4063E1FD74424FF (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___component0, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream1, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info2, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonView::DeserializeComponent(UnityEngine.Component,Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_DeserializeComponent_m556FF3D2A81CA3B9A6371944CDC9F728820F7788 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___component0, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream1, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info2, const RuntimeMethod* method);
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m97139CB2EE76D5CD8308C1AD0499A5F163FC7F51 (RuntimeObject * ___message0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___context1, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponents<UnityEngine.MonoBehaviour>()
inline MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* Component_GetComponents_TisMonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429_mFF32883CC15055E3DA209D02FFE7DFEB78C12741 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponents_TisRuntimeObject_mDC1D023FE07F73E1E8433D1E896D5F1250180C10_gshared)(__this, method);
}
// System.Void Photon.Pun.PhotonNetwork::RPC(Photon.Pun.PhotonView,System.String,Photon.Pun.RpcTarget,System.Boolean,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonNetwork_RPC_m2A2AEB859A31D6676325C7DEFC0765454A74899B (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___view0, String_t* ___methodName1, int32_t ___target2, bool ___encrypt3, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___parameters4, const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonNetwork::RPC(Photon.Pun.PhotonView,System.String,Photon.Realtime.Player,System.Boolean,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonNetwork_RPC_m3A324E2F867852F769217DD61E03E902E4A642B7 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___view0, String_t* ___methodName1, Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * ___targetPlayer2, bool ___encrpyt3, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___parameters4, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<Photon.Pun.PhotonView>()
inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * GameObject_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_mC78C8E2525ECF7D840C9BB1A311E001856163E30 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mD4382B2843BA9A61A01A8F9D7B9813D060F9C9CA_gshared)(__this, method);
}
// Photon.Pun.PhotonView Photon.Pun.PhotonNetwork::GetPhotonView(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * PhotonNetwork_GetPhotonView_mC8007120235F150ECB55FD563D1CE42E8D5AF153 (int32_t ___viewID0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Boolean Photon.Pun.PhotonView::get_IsSceneView()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhotonView_get_IsSceneView_m6E6B2B67B70DEE273B1BABBF739BBAA775CB5ACA (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method);
// System.Int32 Photon.Pun.PhotonView::get_Prefix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhotonView_get_Prefix_m4EBD59509A85CF1D43C09F70EA41AA2CE20F3602 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865 (String_t* ___format0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Reflection.MethodInfo,System.Reflection.ParameterInfo[]>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m44391EE1219E87B732BAD0E46BC433083B9BABB0 (Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF * __this, MethodInfo_t * ___key0, ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694** ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF *, MethodInfo_t *, ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694**, const RuntimeMethod*))Dictionary_2_TryGetValue_m3455807C552312C60038DF52EF328C3687442DE3_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Reflection.MethodInfo,System.Reflection.ParameterInfo[]>::set_Item(!0,!1)
inline void Dictionary_2_set_Item_m82085B12B5862B10FC15F89AF6C6C5F7F4548040 (Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF * __this, MethodInfo_t * ___key0, ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF *, MethodInfo_t *, ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694*, const RuntimeMethod*))Dictionary_2_set_Item_m466D001F105E25DEB5C9BCB17837EE92A27FDE93_gshared)(__this, ___key0, ___value1, method);
}
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<Photon.Pun.PhotonView>(System.Boolean)
inline PhotonViewU5BU5D_t79E798C13CF3CB9516F65B6A4C1E65491FF5CA27* GameObject_GetComponentsInChildren_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_m318AB55027D265F28D3BC79BEA2BE0434A568E62 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___includeInactive0, const RuntimeMethod* method)
{
	return ((  PhotonViewU5BU5D_t79E798C13CF3CB9516F65B6A4C1E65491FF5CA27* (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, bool, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_mFF238916BEA468E58A72E7C1EE6F5D9729CA4D43_gshared)(__this, ___includeInactive0, method);
}
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_sqrMagnitude_m1C6E190B4A933A183B308736DEC0DD64B0588968 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___a0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_get_sqrMagnitude_mAEE10A8ECE7D5754E10727BA8C9068A759AD7002 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Reflection.MethodInfo,System.Reflection.ParameterInfo[]>::.ctor()
inline void Dictionary_2__ctor_mDE47E426BCB97B4E95703C8B6360F6D16DD9C4F7 (Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF *, const RuntimeMethod*))Dictionary_2__ctor_m2C7E51568033239B506E15E7804A0B8658246498_gshared)(__this, method);
}
// System.Void System.Attribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0 (Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * __this, const RuntimeMethod* method);
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  SceneManager_GetActiveScene_mD583193D329EBF540D8AB8A062681B1C2AB5EA51 (const RuntimeMethod* method);
// System.String UnityEngine.SceneManagement.Scene::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Scene_get_name_m0E63ED0F050FCC35A4216220C584BE3D3F77B0E1 (Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Scene_get_buildIndex_m764659943B7357F5D6C9165F68EDCFBBDDD6C3C2 (Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2 * __this, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229 (String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Guid::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Guid__ctor_mC668142577A40A77D13B78AADDEFFFC2E2705079 (Guid_t * __this, String_t* ___g0, const RuntimeMethod* method);
// System.String Photon.Pun.PhotonNetwork::get_BestRegionSummaryInPreferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PhotonNetwork_get_BestRegionSummaryInPreferences_m3A4C27D397D7C07BF59B7675A3CBC1A3B232AF7D (const RuntimeMethod* method);
// System.Void Photon.Pun.PhotonNetwork::set_BestRegionSummaryInPreferences(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonNetwork_set_BestRegionSummaryInPreferences_mFEA6A32B0FADC858803016461E656BA4BD884961 (String_t* ___value0, const RuntimeMethod* method);
// System.String Photon.Realtime.AppSettings::ToStringFull()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AppSettings_ToStringFull_m1C7E9BA1BBCB3C2AA844F4928FE9676AD5359750 (AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void UnityEngine.ScriptableObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B (ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734 * __this, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Photon.Pun.PhotonStreamQueue::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonStreamQueue__ctor_m701EEA27997AEC65B1C9C4684B1F1C8717FDB4A2 (PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1 * __this, int32_t ___sampleRate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonStreamQueue__ctor_m701EEA27997AEC65B1C9C4684B1F1C8717FDB4A2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_ObjectsPerSample_2((-1));
		__this->set_m_LastSampleTime_3((-std::numeric_limits<float>::infinity()));
		__this->set_m_LastFrameCount_4((-1));
		__this->set_m_NextObjectIndex_5((-1));
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)il2cpp_codegen_object_new(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_il2cpp_TypeInfo_var);
		List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3(L_0, /*hidden argument*/List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_RuntimeMethod_var);
		__this->set_m_Objects_6(L_0);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___sampleRate0;
		__this->set_m_SampleRate_0(L_1);
		return;
	}
}
// System.Void Photon.Pun.PhotonStreamQueue::BeginWritePackage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonStreamQueue_BeginWritePackage_m1422FC9FA4E756A2F8A611B14B971A60B0C90D13 (PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonStreamQueue_BeginWritePackage_m1422FC9FA4E756A2F8A611B14B971A60B0C90D13_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_realtimeSinceStartup_mCA1086EC9DFCF135F77BC46D3B7127711EA3DE03(/*hidden argument*/NULL);
		float L_1 = __this->get_m_LastSampleTime_3();
		int32_t L_2 = __this->get_m_SampleRate_0();
		if ((!(((float)L_0) < ((float)((float)il2cpp_codegen_add((float)L_1, (float)((float)((float)(1.0f)/(float)(((float)((float)L_2)))))))))))
		{
			goto IL_0023;
		}
	}
	{
		__this->set_m_IsWriting_7((bool)0);
		return;
	}

IL_0023:
	{
		int32_t L_3 = __this->get_m_SampleCount_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0042;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_4 = __this->get_m_Objects_6();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_inline(L_4, /*hidden argument*/List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_RuntimeMethod_var);
		__this->set_m_ObjectsPerSample_2(L_5);
		goto IL_00ee;
	}

IL_0042:
	{
		int32_t L_6 = __this->get_m_SampleCount_1();
		if ((((int32_t)L_6) <= ((int32_t)1)))
		{
			goto IL_00ee;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_7 = __this->get_m_Objects_6();
		NullCheck(L_7);
		int32_t L_8 = List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_inline(L_7, /*hidden argument*/List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_RuntimeMethod_var);
		int32_t L_9 = __this->get_m_SampleCount_1();
		int32_t L_10 = __this->get_m_ObjectsPerSample_2();
		if ((((int32_t)((int32_t)((int32_t)L_8/(int32_t)L_9))) == ((int32_t)L_10)))
		{
			goto IL_00ee;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(_stringLiteralFDF81A60C6A64CE5D52ACBCF43295B8B789D2AF4, /*hidden argument*/NULL);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral845F4E00AAB16D72B67F9F500C408C22F5A451A9);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral845F4E00AAB16D72B67F9F500C408C22F5A451A9);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = L_12;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_14 = __this->get_m_Objects_6();
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_inline(L_14, /*hidden argument*/List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_RuntimeMethod_var);
		int32_t L_16 = L_15;
		RuntimeObject * L_17 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_17);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_17);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_18 = L_13;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral59E599355D585684E85CC8FF2CFC77E2DFE102D7);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral59E599355D585684E85CC8FF2CFC77E2DFE102D7);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_19 = L_18;
		int32_t L_20 = __this->get_m_SampleCount_1();
		int32_t L_21 = L_20;
		RuntimeObject * L_22 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_22);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_22);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_23 = L_19;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral110BE4F4AA851DA91271D7D722097AD2AEEAD525);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral110BE4F4AA851DA91271D7D722097AD2AEEAD525);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_24 = L_23;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_25 = __this->get_m_Objects_6();
		NullCheck(L_25);
		int32_t L_26 = List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_inline(L_25, /*hidden argument*/List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_RuntimeMethod_var);
		int32_t L_27 = __this->get_m_SampleCount_1();
		int32_t L_28 = ((int32_t)((int32_t)L_26/(int32_t)L_27));
		RuntimeObject * L_29 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_29);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_29);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_30 = L_24;
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, _stringLiteralBE3BE2E556B811A1B556DADDABF39548BAD394CB);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)_stringLiteralBE3BE2E556B811A1B556DADDABF39548BAD394CB);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_31 = L_30;
		int32_t L_32 = __this->get_m_ObjectsPerSample_2();
		int32_t L_33 = L_32;
		RuntimeObject * L_34 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_34);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_34);
		String_t* L_35 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_31, /*hidden argument*/NULL);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(L_35, /*hidden argument*/NULL);
	}

IL_00ee:
	{
		__this->set_m_IsWriting_7((bool)1);
		int32_t L_36 = __this->get_m_SampleCount_1();
		__this->set_m_SampleCount_1(((int32_t)il2cpp_codegen_add((int32_t)L_36, (int32_t)1)));
		float L_37 = Time_get_realtimeSinceStartup_mCA1086EC9DFCF135F77BC46D3B7127711EA3DE03(/*hidden argument*/NULL);
		__this->set_m_LastSampleTime_3(L_37);
		return;
	}
}
// System.Void Photon.Pun.PhotonStreamQueue::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonStreamQueue_Reset_mB399A65DD771F8779251FB5C671916E3E7D4A282 (PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonStreamQueue_Reset_mB399A65DD771F8779251FB5C671916E3E7D4A282_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_SampleCount_1(0);
		__this->set_m_ObjectsPerSample_2((-1));
		__this->set_m_LastSampleTime_3((-std::numeric_limits<float>::infinity()));
		__this->set_m_LastFrameCount_4((-1));
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = __this->get_m_Objects_6();
		NullCheck(L_0);
		List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC(L_0, /*hidden argument*/List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_RuntimeMethod_var);
		return;
	}
}
// System.Void Photon.Pun.PhotonStreamQueue::SendNext(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonStreamQueue_SendNext_mE2B9D3157A8AFCE44572AA33B31A9BA6B8344F17 (PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonStreamQueue_SendNext_mE2B9D3157A8AFCE44572AA33B31A9BA6B8344F17_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Time_get_frameCount_m97573E267B487B8FD4BF37615AFC19BED7B4E436(/*hidden argument*/NULL);
		int32_t L_1 = __this->get_m_LastFrameCount_4();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0013;
		}
	}
	{
		PhotonStreamQueue_BeginWritePackage_m1422FC9FA4E756A2F8A611B14B971A60B0C90D13(__this, /*hidden argument*/NULL);
	}

IL_0013:
	{
		int32_t L_2 = Time_get_frameCount_m97573E267B487B8FD4BF37615AFC19BED7B4E436(/*hidden argument*/NULL);
		__this->set_m_LastFrameCount_4(L_2);
		bool L_3 = __this->get_m_IsWriting_7();
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		return;
	}

IL_0027:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_4 = __this->get_m_Objects_6();
		RuntimeObject * L_5 = ___obj0;
		NullCheck(L_4);
		List_1_Add_m6930161974C7504C80F52EC379EF012387D43138(L_4, L_5, /*hidden argument*/List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_RuntimeMethod_var);
		return;
	}
}
// System.Boolean Photon.Pun.PhotonStreamQueue::HasQueuedObjects()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhotonStreamQueue_HasQueuedObjects_m8DE3DFDF2C74D19C951236E85353D4E193C9C0C7 (PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_NextObjectIndex_5();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Object Photon.Pun.PhotonStreamQueue::ReceiveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * PhotonStreamQueue_ReceiveNext_m40ACF14FF55E9C9D3EDCDC9EFAABE7994E2D9919 (PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonStreamQueue_ReceiveNext_m40ACF14FF55E9C9D3EDCDC9EFAABE7994E2D9919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_NextObjectIndex_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_000b;
		}
	}
	{
		return NULL;
	}

IL_000b:
	{
		int32_t L_1 = __this->get_m_NextObjectIndex_5();
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_2 = __this->get_m_Objects_6();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_inline(L_2, /*hidden argument*/List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_RuntimeMethod_var);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_4 = __this->get_m_NextObjectIndex_5();
		int32_t L_5 = __this->get_m_ObjectsPerSample_2();
		__this->set_m_NextObjectIndex_5(((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)L_5)));
	}

IL_0031:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_6 = __this->get_m_Objects_6();
		int32_t L_7 = __this->get_m_NextObjectIndex_5();
		V_0 = L_7;
		int32_t L_8 = V_0;
		__this->set_m_NextObjectIndex_5(((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1)));
		int32_t L_9 = V_0;
		NullCheck(L_6);
		RuntimeObject * L_10 = List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_inline(L_6, L_9, /*hidden argument*/List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_RuntimeMethod_var);
		return L_10;
	}
}
// System.Void Photon.Pun.PhotonStreamQueue::Serialize(Photon.Pun.PhotonStream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonStreamQueue_Serialize_m404C367BBE48A61A242517D2353389562EAAC3B3 (PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1 * __this, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonStreamQueue_Serialize_m404C367BBE48A61A242517D2353389562EAAC3B3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = __this->get_m_Objects_6();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_inline(L_0, /*hidden argument*/List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_RuntimeMethod_var);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = __this->get_m_ObjectsPerSample_2();
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_3 = __this->get_m_Objects_6();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_inline(L_3, /*hidden argument*/List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_RuntimeMethod_var);
		__this->set_m_ObjectsPerSample_2(L_4);
	}

IL_0028:
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_5 = ___stream0;
		int32_t L_6 = __this->get_m_SampleCount_1();
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		PhotonStream_SendNext_mCDF0BEFCFF338F4A19ECFB8D25C67DAD2AA07712(L_5, L_8, /*hidden argument*/NULL);
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_9 = ___stream0;
		int32_t L_10 = __this->get_m_ObjectsPerSample_2();
		int32_t L_11 = L_10;
		RuntimeObject * L_12 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		PhotonStream_SendNext_mCDF0BEFCFF338F4A19ECFB8D25C67DAD2AA07712(L_9, L_12, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0064;
	}

IL_004e:
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_13 = ___stream0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_14 = __this->get_m_Objects_6();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		RuntimeObject * L_16 = List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_inline(L_14, L_15, /*hidden argument*/List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_RuntimeMethod_var);
		NullCheck(L_13);
		PhotonStream_SendNext_mCDF0BEFCFF338F4A19ECFB8D25C67DAD2AA07712(L_13, L_16, /*hidden argument*/NULL);
		int32_t L_17 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1));
	}

IL_0064:
	{
		int32_t L_18 = V_0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_19 = __this->get_m_Objects_6();
		NullCheck(L_19);
		int32_t L_20 = List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_inline(L_19, /*hidden argument*/List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_RuntimeMethod_var);
		if ((((int32_t)L_18) < ((int32_t)L_20)))
		{
			goto IL_004e;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_21 = __this->get_m_Objects_6();
		NullCheck(L_21);
		List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC(L_21, /*hidden argument*/List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_RuntimeMethod_var);
		__this->set_m_SampleCount_1(0);
		return;
	}
}
// System.Void Photon.Pun.PhotonStreamQueue::Deserialize(Photon.Pun.PhotonStream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonStreamQueue_Deserialize_m3F3F09D6964955C32E72554D893FACEDD3315196 (PhotonStreamQueue_tF5A5C21DA9F0394B7A2A15871AC56A50CA9462E1 * __this, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonStreamQueue_Deserialize_m3F3F09D6964955C32E72554D893FACEDD3315196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = __this->get_m_Objects_6();
		NullCheck(L_0);
		List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC(L_0, /*hidden argument*/List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_RuntimeMethod_var);
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_1 = ___stream0;
		NullCheck(L_1);
		RuntimeObject * L_2 = PhotonStream_ReceiveNext_m8D9908B4B3BFEA0C30F32C2F8CBB89FCA63357A9(L_1, /*hidden argument*/NULL);
		__this->set_m_SampleCount_1(((*(int32_t*)((int32_t*)UnBox(L_2, Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var)))));
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_3 = ___stream0;
		NullCheck(L_3);
		RuntimeObject * L_4 = PhotonStream_ReceiveNext_m8D9908B4B3BFEA0C30F32C2F8CBB89FCA63357A9(L_3, /*hidden argument*/NULL);
		__this->set_m_ObjectsPerSample_2(((*(int32_t*)((int32_t*)UnBox(L_4, Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var)))));
		V_0 = 0;
		goto IL_0046;
	}

IL_0031:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_5 = __this->get_m_Objects_6();
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_6 = ___stream0;
		NullCheck(L_6);
		RuntimeObject * L_7 = PhotonStream_ReceiveNext_m8D9908B4B3BFEA0C30F32C2F8CBB89FCA63357A9(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		List_1_Add_m6930161974C7504C80F52EC379EF012387D43138(L_5, L_7, /*hidden argument*/List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_RuntimeMethod_var);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0046:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = __this->get_m_SampleCount_1();
		int32_t L_11 = __this->get_m_ObjectsPerSample_2();
		if ((((int32_t)L_9) < ((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_10, (int32_t)L_11)))))
		{
			goto IL_0031;
		}
	}
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_12 = __this->get_m_Objects_6();
		NullCheck(L_12);
		int32_t L_13 = List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_inline(L_12, /*hidden argument*/List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_RuntimeMethod_var);
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_006c;
		}
	}
	{
		__this->set_m_NextObjectIndex_5(0);
		return;
	}

IL_006c:
	{
		__this->set_m_NextObjectIndex_5((-1));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Photon.Pun.PhotonTransformView::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformView_Awake_m8B92F4E07ECCD196DBB64FCA42D08D717AADAF25 (PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformView_Awake_m8B92F4E07ECCD196DBB64FCA42D08D717AADAF25_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * L_0 = Component_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_m347A176252862D6CD26E7E6B5E9EAF8F4159273B(__this, /*hidden argument*/Component_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_m347A176252862D6CD26E7E6B5E9EAF8F4159273B_RuntimeMethod_var);
		__this->set_m_PhotonView_6(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_1, /*hidden argument*/NULL);
		__this->set_m_StoredPosition_9(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		__this->set_m_NetworkPosition_8(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_4 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		__this->set_m_NetworkRotation_10(L_4);
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformView::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformView_OnEnable_m47C62F61DD9BA39133FEAC3A1D26D49A33B457CD (PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_firstTake_14((bool)1);
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformView::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformView_Update_mF602711F83B7F676311525103EC74CE42C46E791 (PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformView_Update_mF602711F83B7F676311525103EC74CE42C46E791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * L_0 = __this->get_m_PhotonView_6();
		NullCheck(L_0);
		bool L_1 = PhotonView_get_IsMine_mFE1D0AB5684F18073518777C1D8FF764CB335793(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0075;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_3, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = __this->get_m_NetworkPosition_8();
		float L_6 = __this->get_m_Distance_4();
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		int32_t L_7 = PhotonNetwork_get_SerializationRate_m1121F935DAB7130246C63A55847F75EB6465B294(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Vector3_MoveTowards_mA288BB5AA73DDA9CA76EDC11F339BAFDA1E4FF45(L_4, L_5, ((float)il2cpp_codegen_multiply((float)L_6, (float)((float)((float)(1.0f)/(float)(((float)((float)L_7))))))), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_2, L_8, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_11 = Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9(L_10, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_12 = __this->get_m_NetworkRotation_10();
		float L_13 = __this->get_m_Angle_5();
		int32_t L_14 = PhotonNetwork_get_SerializationRate_m1121F935DAB7130246C63A55847F75EB6465B294(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_15 = Quaternion_RotateTowards_m3AFE60D0BA85FC9B7056433278144249942DCE91(L_11, L_12, ((float)il2cpp_codegen_multiply((float)L_13, (float)((float)((float)(1.0f)/(float)(((float)((float)L_14))))))), /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_9, L_15, /*hidden argument*/NULL);
	}

IL_0075:
	{
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformView::OnPhotonSerializeView(Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformView_OnPhotonSerializeView_mB933AB086DEDE024EFEBF6E9445DB2C601FB1CEB (PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F * __this, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream0, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformView_OnPhotonSerializeView_mB933AB086DEDE024EFEBF6E9445DB2C601FB1CEB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_0 = ___stream0;
		NullCheck(L_0);
		bool L_1 = PhotonStream_get_IsWriting_mB16D73A32F0C3C1CE9272057F667CE15C1DBC458_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a7;
		}
	}
	{
		bool L_2 = __this->get_m_SynchronizePosition_11();
		if (!L_2)
		{
			goto IL_0067;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_3, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = __this->get_m_StoredPosition_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_4, L_5, /*hidden argument*/NULL);
		__this->set_m_Direction_7(L_6);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_7, /*hidden argument*/NULL);
		__this->set_m_StoredPosition_9(L_8);
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_9 = ___stream0;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_10, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = L_11;
		RuntimeObject * L_13 = Box(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_9);
		PhotonStream_SendNext_mCDF0BEFCFF338F4A19ECFB8D25C67DAD2AA07712(L_9, L_13, /*hidden argument*/NULL);
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_14 = ___stream0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = __this->get_m_Direction_7();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = L_15;
		RuntimeObject * L_17 = Box(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_14);
		PhotonStream_SendNext_mCDF0BEFCFF338F4A19ECFB8D25C67DAD2AA07712(L_14, L_17, /*hidden argument*/NULL);
	}

IL_0067:
	{
		bool L_18 = __this->get_m_SynchronizeRotation_12();
		if (!L_18)
		{
			goto IL_0085;
		}
	}
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_19 = ___stream0;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_20 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_21 = Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9(L_20, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_22 = L_21;
		RuntimeObject * L_23 = Box(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_19);
		PhotonStream_SendNext_mCDF0BEFCFF338F4A19ECFB8D25C67DAD2AA07712(L_19, L_23, /*hidden argument*/NULL);
	}

IL_0085:
	{
		bool L_24 = __this->get_m_SynchronizeScale_13();
		if (!L_24)
		{
			goto IL_01cf;
		}
	}
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_25 = ___stream0;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_26 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_26, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = L_27;
		RuntimeObject * L_29 = Box(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_25);
		PhotonStream_SendNext_mCDF0BEFCFF338F4A19ECFB8D25C67DAD2AA07712(L_25, L_29, /*hidden argument*/NULL);
		return;
	}

IL_00a7:
	{
		bool L_30 = __this->get_m_SynchronizePosition_11();
		if (!L_30)
		{
			goto IL_0147;
		}
	}
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_31 = ___stream0;
		NullCheck(L_31);
		RuntimeObject * L_32 = PhotonStream_ReceiveNext_m8D9908B4B3BFEA0C30F32C2F8CBB89FCA63357A9(L_31, /*hidden argument*/NULL);
		__this->set_m_NetworkPosition_8(((*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)UnBox(L_32, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var)))));
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_33 = ___stream0;
		NullCheck(L_33);
		RuntimeObject * L_34 = PhotonStream_ReceiveNext_m8D9908B4B3BFEA0C30F32C2F8CBB89FCA63357A9(L_33, /*hidden argument*/NULL);
		__this->set_m_Direction_7(((*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)UnBox(L_34, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var)))));
		bool L_35 = __this->get_m_firstTake_14();
		if (!L_35)
		{
			goto IL_00fa;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_36 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_37 = __this->get_m_NetworkPosition_8();
		NullCheck(L_36);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_36, L_37, /*hidden argument*/NULL);
		__this->set_m_Distance_4((0.0f));
		goto IL_0147;
	}

IL_00fa:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		double L_38 = PhotonNetwork_get_Time_mDFD12A6E273C6B14118DD8E6DA3EE24C489487F7(/*hidden argument*/NULL);
		double L_39 = PhotonMessageInfo_get_SentServerTime_mF1D7A58C63929E6E466987F5922AA6FBC298B45D((PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0 *)(&___info1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_40 = fabsf((((float)((float)((double)il2cpp_codegen_subtract((double)L_38, (double)L_39))))));
		V_0 = L_40;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_41 = __this->get_m_NetworkPosition_8();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_42 = __this->get_m_Direction_7();
		float L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_44 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_42, L_43, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_45 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_41, L_44, /*hidden argument*/NULL);
		__this->set_m_NetworkPosition_8(L_45);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_46 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_47 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_46, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_48 = __this->get_m_NetworkPosition_8();
		float L_49 = Vector3_Distance_mE316E10B9B319A5C2A29F86E028740FD528149E7(L_47, L_48, /*hidden argument*/NULL);
		__this->set_m_Distance_4(L_49);
	}

IL_0147:
	{
		bool L_50 = __this->get_m_SynchronizeRotation_12();
		if (!L_50)
		{
			goto IL_01a2;
		}
	}
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_51 = ___stream0;
		NullCheck(L_51);
		RuntimeObject * L_52 = PhotonStream_ReceiveNext_m8D9908B4B3BFEA0C30F32C2F8CBB89FCA63357A9(L_51, /*hidden argument*/NULL);
		__this->set_m_NetworkRotation_10(((*(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *)((Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *)UnBox(L_52, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var)))));
		bool L_53 = __this->get_m_firstTake_14();
		if (!L_53)
		{
			goto IL_0186;
		}
	}
	{
		__this->set_m_Angle_5((0.0f));
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_54 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_55 = __this->get_m_NetworkRotation_10();
		NullCheck(L_54);
		Transform_set_rotation_m429694E264117C6DC682EC6AF45C7864E5155935(L_54, L_55, /*hidden argument*/NULL);
		goto IL_01a2;
	}

IL_0186:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_56 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_57 = Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9(L_56, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_58 = __this->get_m_NetworkRotation_10();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		float L_59 = Quaternion_Angle_m09599D660B724D330E5C7FE2FB1C8716161B3DD1(L_57, L_58, /*hidden argument*/NULL);
		__this->set_m_Angle_5(L_59);
	}

IL_01a2:
	{
		bool L_60 = __this->get_m_SynchronizeScale_13();
		if (!L_60)
		{
			goto IL_01c0;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_61 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_62 = ___stream0;
		NullCheck(L_62);
		RuntimeObject * L_63 = PhotonStream_ReceiveNext_m8D9908B4B3BFEA0C30F32C2F8CBB89FCA63357A9(L_62, /*hidden argument*/NULL);
		NullCheck(L_61);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_61, ((*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)UnBox(L_63, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
	}

IL_01c0:
	{
		bool L_64 = __this->get_m_firstTake_14();
		if (!L_64)
		{
			goto IL_01cf;
		}
	}
	{
		__this->set_m_firstTake_14((bool)0);
	}

IL_01cf:
	{
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformView::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformView__ctor_m33662D8E7FC7E7324E41E35903738673120C8B13 (PhotonTransformView_t93BB886AD9BA445C0A76FB45E10984749D090B9F * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_SynchronizePosition_11((bool)1);
		__this->set_m_SynchronizeRotation_12((bool)1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Photon.Pun.PhotonTransformViewClassic::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewClassic_Awake_mD37DE3E2DA509586BB93C634941775BB66326295 (PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewClassic_Awake_mD37DE3E2DA509586BB93C634941775BB66326295_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * L_0 = Component_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_m347A176252862D6CD26E7E6B5E9EAF8F4159273B(__this, /*hidden argument*/Component_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_m347A176252862D6CD26E7E6B5E9EAF8F4159273B_RuntimeMethod_var);
		__this->set_m_PhotonView_10(L_0);
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_1 = __this->get_m_PositionModel_4();
		PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * L_2 = (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 *)il2cpp_codegen_object_new(PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737_il2cpp_TypeInfo_var);
		PhotonTransformViewPositionControl__ctor_mCEAF26452CE4B62915C473A91E0ACB05230312D7(L_2, L_1, /*hidden argument*/NULL);
		__this->set_m_PositionControl_7(L_2);
		PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * L_3 = __this->get_m_RotationModel_5();
		PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * L_4 = (PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE *)il2cpp_codegen_object_new(PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE_il2cpp_TypeInfo_var);
		PhotonTransformViewRotationControl__ctor_m5350996A5B1D11CE80C1FF8275ACE0845D0E8AEF(L_4, L_3, /*hidden argument*/NULL);
		__this->set_m_RotationControl_8(L_4);
		PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * L_5 = __this->get_m_ScaleModel_6();
		PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * L_6 = (PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E *)il2cpp_codegen_object_new(PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E_il2cpp_TypeInfo_var);
		PhotonTransformViewScaleControl__ctor_mEFFA198BF98C3BFE37573865DD22C312FAEC20A1(L_6, L_5, /*hidden argument*/NULL);
		__this->set_m_ScaleControl_9(L_6);
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformViewClassic::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewClassic_OnEnable_m557F8205C4A9DE3059A41E8A2E5B7D6DFB987F53 (PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_firstTake_12((bool)1);
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformViewClassic::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewClassic_Update_m13B896CDDFAEFD5E41EABBA12C30E5281E7AAA91 (PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewClassic_Update_m13B896CDDFAEFD5E41EABBA12C30E5281E7AAA91_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * L_0 = __this->get_m_PhotonView_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * L_2 = __this->get_m_PhotonView_10();
		NullCheck(L_2);
		bool L_3 = PhotonView_get_IsMine_mFE1D0AB5684F18073518777C1D8FF764CB335793(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		bool L_4 = PhotonNetwork_get_IsConnectedAndReady_mAB2CC497386B5EDADDDA63962C8973AAD392EDE6(/*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0023;
		}
	}

IL_0022:
	{
		return;
	}

IL_0023:
	{
		PhotonTransformViewClassic_UpdatePosition_mC5C28116F79ED3B7BA31C6E85CF4E976DD172BB8(__this, /*hidden argument*/NULL);
		PhotonTransformViewClassic_UpdateRotation_m25AE2AD6E2E2492F7B7A2E50EA1B078FA2E55261(__this, /*hidden argument*/NULL);
		PhotonTransformViewClassic_UpdateScale_m7C8DAE1D9A77FED238DAAAB56FA149ADEE02F501(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformViewClassic::UpdatePosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewClassic_UpdatePosition_mC5C28116F79ED3B7BA31C6E85CF4E976DD172BB8 (PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545 * __this, const RuntimeMethod* method)
{
	{
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_0 = __this->get_m_PositionModel_4();
		NullCheck(L_0);
		bool L_1 = L_0->get_SynchronizeEnabled_0();
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		bool L_2 = __this->get_m_ReceivedNetworkUpdate_11();
		if (L_2)
		{
			goto IL_0016;
		}
	}

IL_0015:
	{
		return;
	}

IL_0016:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * L_4 = __this->get_m_PositionControl_7();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = PhotonTransformViewPositionControl_UpdatePosition_m9D1775F2DE7FFE494E96F280CF4DFAAB65FE8429(L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_3, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformViewClassic::UpdateRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewClassic_UpdateRotation_m25AE2AD6E2E2492F7B7A2E50EA1B078FA2E55261 (PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545 * __this, const RuntimeMethod* method)
{
	{
		PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * L_0 = __this->get_m_RotationModel_5();
		NullCheck(L_0);
		bool L_1 = L_0->get_SynchronizeEnabled_0();
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		bool L_2 = __this->get_m_ReceivedNetworkUpdate_11();
		if (L_2)
		{
			goto IL_0016;
		}
	}

IL_0015:
	{
		return;
	}

IL_0016:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * L_4 = __this->get_m_RotationControl_8();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_6 = Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_7 = PhotonTransformViewRotationControl_GetRotation_m8573B3D9871305C20D1023A135BA04AEA0D3EFF0(L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_3, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformViewClassic::UpdateScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewClassic_UpdateScale_m7C8DAE1D9A77FED238DAAAB56FA149ADEE02F501 (PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545 * __this, const RuntimeMethod* method)
{
	{
		PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * L_0 = __this->get_m_ScaleModel_6();
		NullCheck(L_0);
		bool L_1 = L_0->get_SynchronizeEnabled_0();
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		bool L_2 = __this->get_m_ReceivedNetworkUpdate_11();
		if (L_2)
		{
			goto IL_0016;
		}
	}

IL_0015:
	{
		return;
	}

IL_0016:
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * L_4 = __this->get_m_ScaleControl_9();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = PhotonTransformViewScaleControl_GetScale_m0826273DFB50810E7B8E8CE8D05DFA2E67D8D251(L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_3, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformViewClassic::SetSynchronizedValues(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewClassic_SetSynchronizedValues_mDF6E4BA0AC5676E8B666A9182042107B99E55F18 (PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___speed0, float ___turnSpeed1, const RuntimeMethod* method)
{
	{
		PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * L_0 = __this->get_m_PositionControl_7();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___speed0;
		float L_2 = ___turnSpeed1;
		NullCheck(L_0);
		PhotonTransformViewPositionControl_SetSynchronizedValues_m00E204C877A5290D6B9FF704B25ABB1188ECD69C(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformViewClassic::OnPhotonSerializeView(Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewClassic_OnPhotonSerializeView_mEBEC699B9BF1AA64B79D6DA9A29802EA28B9334F (PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545 * __this, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream0, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info1, const RuntimeMethod* method)
{
	{
		PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * L_0 = __this->get_m_PositionControl_7();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8(L_1, /*hidden argument*/NULL);
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_3 = ___stream0;
		PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  L_4 = ___info1;
		NullCheck(L_0);
		PhotonTransformViewPositionControl_OnPhotonSerializeView_mEADE2EB3B08D87FE97F122AAEC97FF83F63A5B51(L_0, L_2, L_3, L_4, /*hidden argument*/NULL);
		PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * L_5 = __this->get_m_RotationControl_8();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_7 = Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE(L_6, /*hidden argument*/NULL);
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_8 = ___stream0;
		PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  L_9 = ___info1;
		NullCheck(L_5);
		PhotonTransformViewRotationControl_OnPhotonSerializeView_m817CD4BE62ABA98E2C071C8AFF0121BB93AFDEED(L_5, L_7, L_8, L_9, /*hidden argument*/NULL);
		PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * L_10 = __this->get_m_ScaleControl_9();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Transform_get_localScale_mD8F631021C2D62B7C341B1A17FA75491F64E13DA(L_11, /*hidden argument*/NULL);
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_13 = ___stream0;
		PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  L_14 = ___info1;
		NullCheck(L_10);
		PhotonTransformViewScaleControl_OnPhotonSerializeView_mB03F0D37841831CA9A6664102D8A16F4F8517F7C(L_10, L_12, L_13, L_14, /*hidden argument*/NULL);
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_15 = ___stream0;
		NullCheck(L_15);
		bool L_16 = PhotonStream_get_IsReading_mB1BE45F09FE0C49E9DCEF6D70E8A0C436949B0D5(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00cf;
		}
	}
	{
		__this->set_m_ReceivedNetworkUpdate_11((bool)1);
		bool L_17 = __this->get_m_firstTake_12();
		if (!L_17)
		{
			goto IL_00cf;
		}
	}
	{
		__this->set_m_firstTake_12((bool)0);
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_18 = __this->get_m_PositionModel_4();
		NullCheck(L_18);
		bool L_19 = L_18->get_SynchronizeEnabled_0();
		if (!L_19)
		{
			goto IL_0089;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_20 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * L_21 = __this->get_m_PositionControl_7();
		NullCheck(L_21);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = PhotonTransformViewPositionControl_GetNetworkPosition_m047475370A825DB4FCAB3BAB86E6029CC98C89A1_inline(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_20, L_22, /*hidden argument*/NULL);
	}

IL_0089:
	{
		PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * L_23 = __this->get_m_RotationModel_5();
		NullCheck(L_23);
		bool L_24 = L_23->get_SynchronizeEnabled_0();
		if (!L_24)
		{
			goto IL_00ac;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_25 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * L_26 = __this->get_m_RotationControl_8();
		NullCheck(L_26);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_27 = PhotonTransformViewRotationControl_GetNetworkRotation_m9C232BEB12E8EBF1A79A4331A24363F5B30CCE14_inline(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_25, L_27, /*hidden argument*/NULL);
	}

IL_00ac:
	{
		PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * L_28 = __this->get_m_ScaleModel_6();
		NullCheck(L_28);
		bool L_29 = L_28->get_SynchronizeEnabled_0();
		if (!L_29)
		{
			goto IL_00cf;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_30 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * L_31 = __this->get_m_ScaleControl_9();
		NullCheck(L_31);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_32 = PhotonTransformViewScaleControl_GetNetworkScale_mA0936FD6D72F6BF7EDE3966230D0AC367B81EB36_inline(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_30, L_32, /*hidden argument*/NULL);
	}

IL_00cf:
	{
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformViewClassic::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewClassic__ctor_mB0D548ABFFE4FECC5B4E541C50CD00398CBF24CF (PhotonTransformViewClassic_t46127EADB747EBD310B5DF71FB7B6F029552B545 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewClassic__ctor_mB0D548ABFFE4FECC5B4E541C50CD00398CBF24CF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_0 = (PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 *)il2cpp_codegen_object_new(PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969_il2cpp_TypeInfo_var);
		PhotonTransformViewPositionModel__ctor_m91498B472599A9EDB5163567647A4D8C9F78A301(L_0, /*hidden argument*/NULL);
		__this->set_m_PositionModel_4(L_0);
		PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * L_1 = (PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D *)il2cpp_codegen_object_new(PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D_il2cpp_TypeInfo_var);
		PhotonTransformViewRotationModel__ctor_mF3032CE5619AA6AE9A758EB850BDD50AB8E8FC4C(L_1, /*hidden argument*/NULL);
		__this->set_m_RotationModel_5(L_1);
		PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * L_2 = (PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 *)il2cpp_codegen_object_new(PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59_il2cpp_TypeInfo_var);
		PhotonTransformViewScaleModel__ctor_m66436933C2943911D2430F3F8DEADDFF696BEB6E(L_2, /*hidden argument*/NULL);
		__this->set_m_ScaleModel_6(L_2);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Photon.Pun.PhotonTransformViewPositionControl::.ctor(Photon.Pun.PhotonTransformViewPositionModel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewPositionControl__ctor_mCEAF26452CE4B62915C473A91E0ACB05230312D7 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * ___model0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewPositionControl__ctor_mCEAF26452CE4B62915C473A91E0ACB05230312D7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		__this->set_m_SynchronizedSpeed_3(L_0);
		Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * L_1 = (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE *)il2cpp_codegen_object_new(Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE_il2cpp_TypeInfo_var);
		Queue_1__ctor_mE0E9CF1FAAEBD411236EF5238DF26D79E380C394(L_1, /*hidden argument*/Queue_1__ctor_mE0E9CF1FAAEBD411236EF5238DF26D79E380C394_RuntimeMethod_var);
		__this->set_m_OldNetworkPositions_6(L_1);
		__this->set_m_UpdatedPositionAfterOnSerialize_7((bool)1);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_2 = ___model0;
		__this->set_m_Model_0(L_2);
		return;
	}
}
// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewPositionControl::GetOldestStoredNetworkPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m4982592135EA10FACC90E0A78B3423BCB1C84405 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m4982592135EA10FACC90E0A78B3423BCB1C84405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = __this->get_m_NetworkPosition_5();
		V_0 = L_0;
		Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * L_1 = __this->get_m_OldNetworkPositions_6();
		NullCheck(L_1);
		int32_t L_2 = Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_inline(L_1, /*hidden argument*/Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_RuntimeMethod_var);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * L_3 = __this->get_m_OldNetworkPositions_6();
		NullCheck(L_3);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Queue_1_Peek_mC6F614DA3810E0A0FB0F1397A82039A7939A2442(L_3, /*hidden argument*/Queue_1_Peek_mC6F614DA3810E0A0FB0F1397A82039A7939A2442_RuntimeMethod_var);
		V_0 = L_4;
	}

IL_0021:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = V_0;
		return L_5;
	}
}
// System.Void Photon.Pun.PhotonTransformViewPositionControl::SetSynchronizedValues(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewPositionControl_SetSynchronizedValues_m00E204C877A5290D6B9FF704B25ABB1188ECD69C (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___speed0, float ___turnSpeed1, const RuntimeMethod* method)
{
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___speed0;
		__this->set_m_SynchronizedSpeed_3(L_0);
		float L_1 = ___turnSpeed1;
		__this->set_m_SynchronizedTurnSpeed_4(L_1);
		return;
	}
}
// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewPositionControl::UpdatePosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewPositionControl_UpdatePosition_m9D1775F2DE7FFE494E96F280CF4DFAAB65FE8429 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentPosition0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewPositionControl_UpdatePosition_m9D1775F2DE7FFE494E96F280CF4DFAAB65FE8429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = PhotonTransformViewPositionControl_GetNetworkPosition_m047475370A825DB4FCAB3BAB86E6029CC98C89A1_inline(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = PhotonTransformViewPositionControl_GetExtrapolatedPositionOffset_mFE9B782C8EDB94A36173C1559B72EF14790690C9(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_3 = __this->get_m_Model_0();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_InterpolateOption_3();
		V_1 = L_4;
		int32_t L_5 = V_1;
		switch (L_5)
		{
			case 0:
			{
				goto IL_003d;
			}
			case 1:
			{
				goto IL_0057;
			}
			case 2:
			{
				goto IL_0076;
			}
			case 3:
			{
				goto IL_00be;
			}
			case 4:
			{
				goto IL_00f1;
			}
		}
	}
	{
		goto IL_010b;
	}

IL_003d:
	{
		bool L_6 = __this->get_m_UpdatedPositionAfterOnSerialize_7();
		if (L_6)
		{
			goto IL_010b;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = V_0;
		___currentPosition0 = L_7;
		__this->set_m_UpdatedPositionAfterOnSerialize_7((bool)1);
		goto IL_010b;
	}

IL_0057:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = ___currentPosition0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = V_0;
		float L_10 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_11 = __this->get_m_Model_0();
		NullCheck(L_11);
		float L_12 = L_11->get_InterpolateMoveTowardsSpeed_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Vector3_MoveTowards_mA288BB5AA73DDA9CA76EDC11F339BAFDA1E4FF45(L_8, L_9, ((float)il2cpp_codegen_multiply((float)L_10, (float)L_12)), /*hidden argument*/NULL);
		___currentPosition0 = L_13;
		goto IL_010b;
	}

IL_0076:
	{
		Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * L_14 = __this->get_m_OldNetworkPositions_6();
		NullCheck(L_14);
		int32_t L_15 = Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_inline(L_14, /*hidden argument*/Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_RuntimeMethod_var);
		if (!L_15)
		{
			goto IL_010b;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = __this->get_m_NetworkPosition_5();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m4982592135EA10FACC90E0A78B3423BCB1C84405(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		float L_18 = Vector3_Distance_mE316E10B9B319A5C2A29F86E028740FD528149E7(L_16, L_17, /*hidden argument*/NULL);
		Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * L_19 = __this->get_m_OldNetworkPositions_6();
		NullCheck(L_19);
		int32_t L_20 = Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_inline(L_19, /*hidden argument*/Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		int32_t L_21 = PhotonNetwork_get_SerializationRate_m1121F935DAB7130246C63A55847F75EB6465B294(/*hidden argument*/NULL);
		V_2 = ((float)il2cpp_codegen_multiply((float)((float)((float)L_18/(float)(((float)((float)L_20))))), (float)(((float)((float)L_21)))));
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_22 = ___currentPosition0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = V_0;
		float L_24 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		float L_25 = V_2;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = Vector3_MoveTowards_mA288BB5AA73DDA9CA76EDC11F339BAFDA1E4FF45(L_22, L_23, ((float)il2cpp_codegen_multiply((float)L_24, (float)L_25)), /*hidden argument*/NULL);
		___currentPosition0 = L_26;
		goto IL_010b;
	}

IL_00be:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_27 = __this->get_address_of_m_SynchronizedSpeed_3();
		float L_28 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)L_27, /*hidden argument*/NULL);
		if ((!(((float)L_28) == ((float)(0.0f)))))
		{
			goto IL_00d5;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_29 = V_0;
		___currentPosition0 = L_29;
		goto IL_010b;
	}

IL_00d5:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_30 = ___currentPosition0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_31 = V_0;
		float L_32 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * L_33 = __this->get_address_of_m_SynchronizedSpeed_3();
		float L_34 = Vector3_get_magnitude_m9A750659B60C5FE0C30438A7F9681775D5DB1274((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_35 = Vector3_MoveTowards_mA288BB5AA73DDA9CA76EDC11F339BAFDA1E4FF45(L_30, L_31, ((float)il2cpp_codegen_multiply((float)L_32, (float)L_34)), /*hidden argument*/NULL);
		___currentPosition0 = L_35;
		goto IL_010b;
	}

IL_00f1:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_36 = ___currentPosition0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_37 = V_0;
		float L_38 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_39 = __this->get_m_Model_0();
		NullCheck(L_39);
		float L_40 = L_39->get_InterpolateLerpSpeed_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_41 = Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1(L_36, L_37, ((float)il2cpp_codegen_multiply((float)L_38, (float)L_40)), /*hidden argument*/NULL);
		___currentPosition0 = L_41;
	}

IL_010b:
	{
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_42 = __this->get_m_Model_0();
		NullCheck(L_42);
		bool L_43 = L_42->get_TeleportEnabled_1();
		if (!L_43)
		{
			goto IL_0139;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_44 = ___currentPosition0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_45 = PhotonTransformViewPositionControl_GetNetworkPosition_m047475370A825DB4FCAB3BAB86E6029CC98C89A1_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		float L_46 = Vector3_Distance_mE316E10B9B319A5C2A29F86E028740FD528149E7(L_44, L_45, /*hidden argument*/NULL);
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_47 = __this->get_m_Model_0();
		NullCheck(L_47);
		float L_48 = L_47->get_TeleportIfDistanceGreaterThan_2();
		if ((!(((float)L_46) > ((float)L_48))))
		{
			goto IL_0139;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_49 = PhotonTransformViewPositionControl_GetNetworkPosition_m047475370A825DB4FCAB3BAB86E6029CC98C89A1_inline(__this, /*hidden argument*/NULL);
		___currentPosition0 = L_49;
	}

IL_0139:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_50 = ___currentPosition0;
		return L_50;
	}
}
// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewPositionControl::GetNetworkPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewPositionControl_GetNetworkPosition_m047475370A825DB4FCAB3BAB86E6029CC98C89A1 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, const RuntimeMethod* method)
{
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = __this->get_m_NetworkPosition_5();
		return L_0;
	}
}
// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewPositionControl::GetExtrapolatedPositionOffset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewPositionControl_GetExtrapolatedPositionOffset_mFE9B782C8EDB94A36173C1559B72EF14790690C9 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewPositionControl_GetExtrapolatedPositionOffset_mFE9B782C8EDB94A36173C1559B72EF14790690C9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t V_2 = 0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		double L_0 = PhotonNetwork_get_Time_mDFD12A6E273C6B14118DD8E6DA3EE24C489487F7(/*hidden argument*/NULL);
		double L_1 = __this->get_m_LastSerializeTime_2();
		V_0 = (((float)((float)((double)il2cpp_codegen_subtract((double)L_0, (double)L_1)))));
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_2 = __this->get_m_Model_0();
		NullCheck(L_2);
		bool L_3 = L_2->get_ExtrapolateIncludingRoundTripTime_8();
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		int32_t L_5 = PhotonNetwork_GetPing_m19C66FE34028DB920307BB89AFF5E954C57FA165(/*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_add((float)L_4, (float)((float)((float)(((float)((float)L_5)))/(float)(1000.0f)))));
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		V_1 = L_6;
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_7 = __this->get_m_Model_0();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_ExtrapolateOption_6();
		V_2 = L_8;
		int32_t L_9 = V_2;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)1)))
		{
			case 0:
			{
				goto IL_0055;
			}
			case 1:
			{
				goto IL_00b2;
			}
			case 2:
			{
				goto IL_0080;
			}
		}
	}
	{
		goto IL_00d5;
	}

IL_0055:
	{
		float L_10 = __this->get_m_SynchronizedTurnSpeed_4();
		float L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_12 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((0.0f), ((float)il2cpp_codegen_multiply((float)L_10, (float)L_11)), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = __this->get_m_SynchronizedSpeed_3();
		float L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_13, L_14, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_16 = Quaternion_op_Multiply_mD5999DE317D808808B72E58E7A978C4C0995879C(L_12, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		goto IL_00d5;
	}

IL_0080:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = __this->get_m_NetworkPosition_5();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_18 = PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m4982592135EA10FACC90E0A78B3423BCB1C84405(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_19 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_17, L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_20 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_3), /*hidden argument*/NULL);
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_21 = __this->get_m_Model_0();
		NullCheck(L_21);
		float L_22 = L_21->get_ExtrapolateSpeed_7();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_20, L_22, /*hidden argument*/NULL);
		float L_24 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_25 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_23, L_24, /*hidden argument*/NULL);
		V_1 = L_25;
		goto IL_00d5;
	}

IL_00b2:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_26 = __this->get_m_NetworkPosition_5();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = PhotonTransformViewPositionControl_GetOldestStoredNetworkPosition_m4982592135EA10FACC90E0A78B3423BCB1C84405(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_28 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_26, L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		int32_t L_29 = PhotonNetwork_get_SerializationRate_m1121F935DAB7130246C63A55847F75EB6465B294(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_30 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_28, (((float)((float)L_29))), /*hidden argument*/NULL);
		float L_31 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_32 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_30, L_31, /*hidden argument*/NULL);
		V_1 = L_32;
	}

IL_00d5:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_33 = V_1;
		return L_33;
	}
}
// System.Void Photon.Pun.PhotonTransformViewPositionControl::OnPhotonSerializeView(UnityEngine.Vector3,Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewPositionControl_OnPhotonSerializeView_mEADE2EB3B08D87FE97F122AAEC97FF83F63A5B51 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentPosition0, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream1, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewPositionControl_OnPhotonSerializeView_mEADE2EB3B08D87FE97F122AAEC97FF83F63A5B51_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_0 = __this->get_m_Model_0();
		NullCheck(L_0);
		bool L_1 = L_0->get_SynchronizeEnabled_0();
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_2 = ___stream1;
		NullCheck(L_2);
		bool L_3 = PhotonStream_get_IsWriting_mB16D73A32F0C3C1CE9272057F667CE15C1DBC458_inline(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___currentPosition0;
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_5 = ___stream1;
		PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  L_6 = ___info2;
		PhotonTransformViewPositionControl_SerializeData_m46A84222F254F10D91A0762D0A70DAC9F4BF0CC8(__this, L_4, L_5, L_6, /*hidden argument*/NULL);
		goto IL_0029;
	}

IL_0021:
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_7 = ___stream1;
		PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  L_8 = ___info2;
		PhotonTransformViewPositionControl_DeserializeData_m9D749828D57C78D6AD4443D66E8DCFBC6D0423A3(__this, L_7, L_8, /*hidden argument*/NULL);
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		double L_9 = PhotonNetwork_get_Time_mDFD12A6E273C6B14118DD8E6DA3EE24C489487F7(/*hidden argument*/NULL);
		__this->set_m_LastSerializeTime_2(L_9);
		__this->set_m_UpdatedPositionAfterOnSerialize_7((bool)0);
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformViewPositionControl::SerializeData(UnityEngine.Vector3,Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewPositionControl_SerializeData_m46A84222F254F10D91A0762D0A70DAC9F4BF0CC8 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentPosition0, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream1, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewPositionControl_SerializeData_m46A84222F254F10D91A0762D0A70DAC9F4BF0CC8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_0 = ___stream1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___currentPosition0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = L_1;
		RuntimeObject * L_3 = Box(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		PhotonStream_SendNext_mCDF0BEFCFF338F4A19ECFB8D25C67DAD2AA07712(L_0, L_3, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___currentPosition0;
		__this->set_m_NetworkPosition_5(L_4);
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_5 = __this->get_m_Model_0();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_ExtrapolateOption_6();
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_002f;
		}
	}
	{
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_7 = __this->get_m_Model_0();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_InterpolateOption_3();
		if ((!(((uint32_t)L_8) == ((uint32_t)3))))
		{
			goto IL_0051;
		}
	}

IL_002f:
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_9 = ___stream1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = __this->get_m_SynchronizedSpeed_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = L_10;
		RuntimeObject * L_12 = Box(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		PhotonStream_SendNext_mCDF0BEFCFF338F4A19ECFB8D25C67DAD2AA07712(L_9, L_12, /*hidden argument*/NULL);
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_13 = ___stream1;
		float L_14 = __this->get_m_SynchronizedTurnSpeed_4();
		float L_15 = L_14;
		RuntimeObject * L_16 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		PhotonStream_SendNext_mCDF0BEFCFF338F4A19ECFB8D25C67DAD2AA07712(L_13, L_16, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
// System.Void Photon.Pun.PhotonTransformViewPositionControl::DeserializeData(Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewPositionControl_DeserializeData_m9D749828D57C78D6AD4443D66E8DCFBC6D0423A3 (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream0, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewPositionControl_DeserializeData_m9D749828D57C78D6AD4443D66E8DCFBC6D0423A3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_0 = ___stream0;
		NullCheck(L_0);
		RuntimeObject * L_1 = PhotonStream_ReceiveNext_m8D9908B4B3BFEA0C30F32C2F8CBB89FCA63357A9(L_0, /*hidden argument*/NULL);
		V_0 = ((*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)UnBox(L_1, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var))));
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_2 = __this->get_m_Model_0();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_ExtrapolateOption_6();
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0028;
		}
	}
	{
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_4 = __this->get_m_Model_0();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_InterpolateOption_3();
		if ((!(((uint32_t)L_5) == ((uint32_t)3))))
		{
			goto IL_004a;
		}
	}

IL_0028:
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_6 = ___stream0;
		NullCheck(L_6);
		RuntimeObject * L_7 = PhotonStream_ReceiveNext_m8D9908B4B3BFEA0C30F32C2F8CBB89FCA63357A9(L_6, /*hidden argument*/NULL);
		__this->set_m_SynchronizedSpeed_3(((*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)UnBox(L_7, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var)))));
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_8 = ___stream0;
		NullCheck(L_8);
		RuntimeObject * L_9 = PhotonStream_ReceiveNext_m8D9908B4B3BFEA0C30F32C2F8CBB89FCA63357A9(L_8, /*hidden argument*/NULL);
		__this->set_m_SynchronizedTurnSpeed_4(((*(float*)((float*)UnBox(L_9, Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var)))));
	}

IL_004a:
	{
		Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * L_10 = __this->get_m_OldNetworkPositions_6();
		NullCheck(L_10);
		int32_t L_11 = Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_inline(L_10, /*hidden argument*/Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_RuntimeMethod_var);
		if (L_11)
		{
			goto IL_005e;
		}
	}
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = V_0;
		__this->set_m_NetworkPosition_5(L_12);
	}

IL_005e:
	{
		Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * L_13 = __this->get_m_OldNetworkPositions_6();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = __this->get_m_NetworkPosition_5();
		NullCheck(L_13);
		Queue_1_Enqueue_m1D4D0569B64A37FF041C5EF8CE19CB5210E2ED2F(L_13, L_14, /*hidden argument*/Queue_1_Enqueue_m1D4D0569B64A37FF041C5EF8CE19CB5210E2ED2F_RuntimeMethod_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = V_0;
		__this->set_m_NetworkPosition_5(L_15);
		goto IL_0084;
	}

IL_0078:
	{
		Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * L_16 = __this->get_m_OldNetworkPositions_6();
		NullCheck(L_16);
		Queue_1_Dequeue_mF86F3E89DD24E5CC47AF827C6DA43A4062F9A344(L_16, /*hidden argument*/Queue_1_Dequeue_mF86F3E89DD24E5CC47AF827C6DA43A4062F9A344_RuntimeMethod_var);
	}

IL_0084:
	{
		Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * L_17 = __this->get_m_OldNetworkPositions_6();
		NullCheck(L_17);
		int32_t L_18 = Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_inline(L_17, /*hidden argument*/Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_RuntimeMethod_var);
		PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * L_19 = __this->get_m_Model_0();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_ExtrapolateNumberOfStoredPositions_9();
		if ((((int32_t)L_18) > ((int32_t)L_20)))
		{
			goto IL_0078;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Photon.Pun.PhotonTransformViewPositionModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewPositionModel__ctor_m91498B472599A9EDB5163567647A4D8C9F78A301 (PhotonTransformViewPositionModel_t99B971E45614F93E6B2A50A646F41FAA2A2BF969 * __this, const RuntimeMethod* method)
{
	{
		__this->set_TeleportEnabled_1((bool)1);
		__this->set_TeleportIfDistanceGreaterThan_2((3.0f));
		__this->set_InterpolateOption_3(2);
		__this->set_InterpolateMoveTowardsSpeed_4((1.0f));
		__this->set_InterpolateLerpSpeed_5((1.0f));
		__this->set_ExtrapolateSpeed_7((1.0f));
		__this->set_ExtrapolateIncludingRoundTripTime_8((bool)1);
		__this->set_ExtrapolateNumberOfStoredPositions_9(1);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Photon.Pun.PhotonTransformViewRotationControl::.ctor(Photon.Pun.PhotonTransformViewRotationModel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewRotationControl__ctor_m5350996A5B1D11CE80C1FF8275ACE0845D0E8AEF (PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * __this, PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * ___model0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * L_0 = ___model0;
		__this->set_m_Model_0(L_0);
		return;
	}
}
// UnityEngine.Quaternion Photon.Pun.PhotonTransformViewRotationControl::GetNetworkRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  PhotonTransformViewRotationControl_GetNetworkRotation_m9C232BEB12E8EBF1A79A4331A24363F5B30CCE14 (PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * __this, const RuntimeMethod* method)
{
	{
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_0 = __this->get_m_NetworkRotation_1();
		return L_0;
	}
}
// UnityEngine.Quaternion Photon.Pun.PhotonTransformViewRotationControl::GetRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  PhotonTransformViewRotationControl_GetRotation_m8573B3D9871305C20D1023A135BA04AEA0D3EFF0 (PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___currentRotation0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewRotationControl_GetRotation_m8573B3D9871305C20D1023A135BA04AEA0D3EFF0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * L_0 = __this->get_m_Model_0();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_InterpolateOption_1();
		V_0 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_001e;
			}
			case 1:
			{
				goto IL_0025;
			}
			case 2:
			{
				goto IL_0043;
			}
		}
	}

IL_001e:
	{
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_3 = __this->get_m_NetworkRotation_1();
		return L_3;
	}

IL_0025:
	{
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_4 = ___currentRotation0;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_5 = __this->get_m_NetworkRotation_1();
		PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * L_6 = __this->get_m_Model_0();
		NullCheck(L_6);
		float L_7 = L_6->get_InterpolateRotateTowardsSpeed_2();
		float L_8 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_9 = Quaternion_RotateTowards_m3AFE60D0BA85FC9B7056433278144249942DCE91(L_4, L_5, ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}

IL_0043:
	{
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_10 = ___currentRotation0;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_11 = __this->get_m_NetworkRotation_1();
		PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * L_12 = __this->get_m_Model_0();
		NullCheck(L_12);
		float L_13 = L_12->get_InterpolateLerpSpeed_3();
		float L_14 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_15 = Quaternion_Lerp_m749B3988EE2EF387CC9BFB76C81B7465A7534E27(L_10, L_11, ((float)il2cpp_codegen_multiply((float)L_13, (float)L_14)), /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void Photon.Pun.PhotonTransformViewRotationControl::OnPhotonSerializeView(UnityEngine.Quaternion,Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewRotationControl_OnPhotonSerializeView_m817CD4BE62ABA98E2C071C8AFF0121BB93AFDEED (PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___currentRotation0, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream1, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewRotationControl_OnPhotonSerializeView_m817CD4BE62ABA98E2C071C8AFF0121BB93AFDEED_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * L_0 = __this->get_m_Model_0();
		NullCheck(L_0);
		bool L_1 = L_0->get_SynchronizeEnabled_0();
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_2 = ___stream1;
		NullCheck(L_2);
		bool L_3 = PhotonStream_get_IsWriting_mB16D73A32F0C3C1CE9272057F667CE15C1DBC458_inline(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_4 = ___stream1;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_5 = ___currentRotation0;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_6 = L_5;
		RuntimeObject * L_7 = Box(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		PhotonStream_SendNext_mCDF0BEFCFF338F4A19ECFB8D25C67DAD2AA07712(L_4, L_7, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_8 = ___currentRotation0;
		__this->set_m_NetworkRotation_1(L_8);
		return;
	}

IL_002a:
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_9 = ___stream1;
		NullCheck(L_9);
		RuntimeObject * L_10 = PhotonStream_ReceiveNext_m8D9908B4B3BFEA0C30F32C2F8CBB89FCA63357A9(L_9, /*hidden argument*/NULL);
		__this->set_m_NetworkRotation_1(((*(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *)((Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 *)UnBox(L_10, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var)))));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Photon.Pun.PhotonTransformViewRotationModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewRotationModel__ctor_mF3032CE5619AA6AE9A758EB850BDD50AB8E8FC4C (PhotonTransformViewRotationModel_t566539178196E4B4CE6A82B5DA9C249C99F97A4D * __this, const RuntimeMethod* method)
{
	{
		__this->set_InterpolateOption_1(1);
		__this->set_InterpolateRotateTowardsSpeed_2((180.0f));
		__this->set_InterpolateLerpSpeed_3((5.0f));
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Photon.Pun.PhotonTransformViewScaleControl::.ctor(Photon.Pun.PhotonTransformViewScaleModel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewScaleControl__ctor_mEFFA198BF98C3BFE37573865DD22C312FAEC20A1 (PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * __this, PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * ___model0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewScaleControl__ctor_mEFFA198BF98C3BFE37573865DD22C312FAEC20A1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB(/*hidden argument*/NULL);
		__this->set_m_NetworkScale_1(L_0);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * L_1 = ___model0;
		__this->set_m_Model_0(L_1);
		return;
	}
}
// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewScaleControl::GetNetworkScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewScaleControl_GetNetworkScale_mA0936FD6D72F6BF7EDE3966230D0AC367B81EB36 (PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * __this, const RuntimeMethod* method)
{
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = __this->get_m_NetworkScale_1();
		return L_0;
	}
}
// UnityEngine.Vector3 Photon.Pun.PhotonTransformViewScaleControl::GetScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewScaleControl_GetScale_m0826273DFB50810E7B8E8CE8D05DFA2E67D8D251 (PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentScale0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewScaleControl_GetScale_m0826273DFB50810E7B8E8CE8D05DFA2E67D8D251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * L_0 = __this->get_m_Model_0();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_InterpolateOption_1();
		V_0 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_001e;
			}
			case 1:
			{
				goto IL_0025;
			}
			case 2:
			{
				goto IL_0043;
			}
		}
	}

IL_001e:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = __this->get_m_NetworkScale_1();
		return L_3;
	}

IL_0025:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___currentScale0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = __this->get_m_NetworkScale_1();
		PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * L_6 = __this->get_m_Model_0();
		NullCheck(L_6);
		float L_7 = L_6->get_InterpolateMoveTowardsSpeed_2();
		float L_8 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Vector3_MoveTowards_mA288BB5AA73DDA9CA76EDC11F339BAFDA1E4FF45(L_4, L_5, ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}

IL_0043:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = ___currentScale0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = __this->get_m_NetworkScale_1();
		PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * L_12 = __this->get_m_Model_0();
		NullCheck(L_12);
		float L_13 = L_12->get_InterpolateLerpSpeed_3();
		float L_14 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_15 = Vector3_Lerp_m5BA75496B803820CC64079383956D73C6FD4A8A1(L_10, L_11, ((float)il2cpp_codegen_multiply((float)L_13, (float)L_14)), /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void Photon.Pun.PhotonTransformViewScaleControl::OnPhotonSerializeView(UnityEngine.Vector3,Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewScaleControl_OnPhotonSerializeView_mB03F0D37841831CA9A6664102D8A16F4F8517F7C (PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentScale0, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream1, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonTransformViewScaleControl_OnPhotonSerializeView_mB03F0D37841831CA9A6664102D8A16F4F8517F7C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * L_0 = __this->get_m_Model_0();
		NullCheck(L_0);
		bool L_1 = L_0->get_SynchronizeEnabled_0();
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_2 = ___stream1;
		NullCheck(L_2);
		bool L_3 = PhotonStream_get_IsWriting_mB16D73A32F0C3C1CE9272057F667CE15C1DBC458_inline(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_4 = ___stream1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = ___currentScale0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = L_5;
		RuntimeObject * L_7 = Box(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		PhotonStream_SendNext_mCDF0BEFCFF338F4A19ECFB8D25C67DAD2AA07712(L_4, L_7, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = ___currentScale0;
		__this->set_m_NetworkScale_1(L_8);
		return;
	}

IL_002a:
	{
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_9 = ___stream1;
		NullCheck(L_9);
		RuntimeObject * L_10 = PhotonStream_ReceiveNext_m8D9908B4B3BFEA0C30F32C2F8CBB89FCA63357A9(L_9, /*hidden argument*/NULL);
		__this->set_m_NetworkScale_1(((*(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)UnBox(L_10, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var)))));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Photon.Pun.PhotonTransformViewScaleModel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonTransformViewScaleModel__ctor_m66436933C2943911D2430F3F8DEADDFF696BEB6E (PhotonTransformViewScaleModel_t36CDC7389CB92586219AC309C13BC629416B3D59 * __this, const RuntimeMethod* method)
{
	{
		__this->set_InterpolateMoveTowardsSpeed_2((1.0f));
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 Photon.Pun.PhotonView::get_Prefix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhotonView_get_Prefix_m4EBD59509A85CF1D43C09F70EA41AA2CE20F3602 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_get_Prefix_m4EBD59509A85CF1D43C09F70EA41AA2CE20F3602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_prefixField_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A * L_1 = ((PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields*)il2cpp_codegen_static_fields_for(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var))->get_NetworkingClient_2();
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		uint8_t L_2 = ((PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields*)il2cpp_codegen_static_fields_for(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var))->get_currentLevelPrefix_32();
		__this->set_prefixField_8(L_2);
	}

IL_001b:
	{
		int32_t L_3 = __this->get_prefixField_8();
		return L_3;
	}
}
// System.Void Photon.Pun.PhotonView::set_Prefix(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_set_Prefix_mA57D725B1E5D55956DF449DDFD7536BA7632B96C (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_prefixField_8(L_0);
		return;
	}
}
// System.Object[] Photon.Pun.PhotonView::get_InstantiationData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* PhotonView_get_InstantiationData_mB0731A9C32456F8169DC4C5FEA21A7587FAD8B31 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_get_InstantiationData_mB0731A9C32456F8169DC4C5FEA21A7587FAD8B31_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_didAwake_18();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral7E97DB408E60C67BAFDFF096CFE901D68FF1F5A1, /*hidden argument*/NULL);
	}

IL_0012:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = __this->get_instantiationDataField_9();
		return L_1;
	}
}
// System.Void Photon.Pun.PhotonView::set_InstantiationData(System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_set_InstantiationData_m1237915AD941BEFEB91462744150318B569BB932 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___value0, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = ___value0;
		__this->set_instantiationDataField_9(L_0);
		return;
	}
}
// System.Int32 Photon.Pun.PhotonView::get_ViewID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhotonView_get_ViewID_mC15D092CC4BFE8638C31F9622B7DF6894E30D3D9 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_viewIdField_16();
		return L_0;
	}
}
// System.Void Photon.Pun.PhotonView::set_ViewID(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_set_ViewID_m489879E6DAD4700D0A0DCA77849EAA92FAFE7CDB (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_set_ViewID_m489879E6DAD4700D0A0DCA77849EAA92FAFE7CDB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		bool L_0 = __this->get_didAwake_18();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = __this->get_viewIdField_16();
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_2 = ___value0;
		G_B4_0 = ((!(((uint32_t)L_2) <= ((uint32_t)0)))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B4_0 = 0;
	}

IL_0017:
	{
		int32_t L_3 = ___value0;
		__this->set_viewIdField_16(L_3);
		int32_t L_4 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		int32_t L_5 = ((PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields*)il2cpp_codegen_static_fields_for(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var))->get_MAX_VIEW_IDS_3();
		__this->set_ownerId_4(((int32_t)((int32_t)L_4/(int32_t)L_5)));
		if (!G_B4_0)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		PhotonNetwork_RegisterPhotonView_m4E523EEA4FE46B919ACBC4EEB2FF76AC2ED5A25F(__this, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Boolean Photon.Pun.PhotonView::get_IsSceneView()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhotonView_get_IsSceneView_m6E6B2B67B70DEE273B1BABBF739BBAA775CB5ACA (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = PhotonView_get_CreatorActorNr_m182100823912B7A313C17D0844D599BFAAA03629(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// Photon.Realtime.Player Photon.Pun.PhotonView::get_Owner()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * PhotonView_get_Owner_mD339E981ADADF5A7F88F6D4F530F1EB8F1C34DEE (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_get_Owner_mD339E981ADADF5A7F88F6D4F530F1EB8F1C34DEE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		Room_t5DFC39DD6736A2641374564EC6C31352BE33000D * L_0 = PhotonNetwork_get_CurrentRoom_m493DA29D80F75DC0BEFF3125207AE9101C0100A1(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		Room_t5DFC39DD6736A2641374564EC6C31352BE33000D * L_1 = PhotonNetwork_get_CurrentRoom_m493DA29D80F75DC0BEFF3125207AE9101C0100A1(/*hidden argument*/NULL);
		int32_t L_2 = PhotonView_get_OwnerActorNr_m297F5F405EF3ACE0A262F942FF01357EE4BDE886(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * L_3 = VirtFuncInvoker1< Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 *, int32_t >::Invoke(10 /* Photon.Realtime.Player Photon.Realtime.Room::GetPlayer(System.Int32) */, L_1, L_2);
		return L_3;
	}

IL_0018:
	{
		return (Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 *)NULL;
	}
}
// System.Int32 Photon.Pun.PhotonView::get_OwnerActorNr()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhotonView_get_OwnerActorNr_m297F5F405EF3ACE0A262F942FF01357EE4BDE886 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_get_OwnerActorNr_m297F5F405EF3ACE0A262F942FF01357EE4BDE886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_didAwake_18();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = PhotonView_get_ViewID_mC15D092CC4BFE8638C31F9622B7DF6894E30D3D9_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		int32_t L_2 = ((PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields*)il2cpp_codegen_static_fields_for(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var))->get_MAX_VIEW_IDS_3();
		return ((int32_t)((int32_t)L_1/(int32_t)L_2));
	}

IL_0015:
	{
		int32_t L_3 = __this->get_ownerId_4();
		return L_3;
	}
}
// System.Void Photon.Pun.PhotonView::set_OwnerActorNr(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_set_OwnerActorNr_m27673CAA4DA1E87537286453E1EA52DBDE0301EA (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_ownerId_4(L_0);
		return;
	}
}
// Photon.Realtime.Player Photon.Pun.PhotonView::get_Controller()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * PhotonView_get_Controller_mABA45440F92EDB48CB1F32D6D2B3553C61E11DC6 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_get_Controller_mABA45440F92EDB48CB1F32D6D2B3553C61E11DC6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		Room_t5DFC39DD6736A2641374564EC6C31352BE33000D * L_0 = PhotonNetwork_get_CurrentRoom_m493DA29D80F75DC0BEFF3125207AE9101C0100A1(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * L_1 = PhotonNetwork_get_LocalPlayer_m06E2D9682B713C828A00C0BD4F03A0CBC1E2D1B6(/*hidden argument*/NULL);
		return L_1;
	}

IL_000d:
	{
		bool L_2 = PhotonView_get_IsOwnerActive_mA17BFB41B289C7CD1A75B2AA27DAB90A69FEC87F(__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * L_3 = PhotonNetwork_get_MasterClient_mE95EB702C5632D7DDADF199B4B9ECD4B45556B65(/*hidden argument*/NULL);
		return L_3;
	}

IL_001b:
	{
		Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * L_4 = PhotonView_get_Owner_mD339E981ADADF5A7F88F6D4F530F1EB8F1C34DEE(__this, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 Photon.Pun.PhotonView::get_ControllerActorNr()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhotonView_get_ControllerActorNr_m8D9D0453C6E350464D670B38210911BCEAE61C97 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_get_ControllerActorNr_m8D9D0453C6E350464D670B38210911BCEAE61C97_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PhotonView_get_IsOwnerActive_mA17BFB41B289C7CD1A75B2AA27DAB90A69FEC87F(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * L_1 = PhotonNetwork_get_MasterClient_mE95EB702C5632D7DDADF199B4B9ECD4B45556B65(/*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return (-1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * L_2 = PhotonNetwork_get_MasterClient_mE95EB702C5632D7DDADF199B4B9ECD4B45556B65(/*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Player_get_ActorNumber_m02157560C558C6007C8146F1FCDA7E759AD0CF1D_inline(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_001c:
	{
		int32_t L_4 = PhotonView_get_OwnerActorNr_m297F5F405EF3ACE0A262F942FF01357EE4BDE886(__this, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean Photon.Pun.PhotonView::get_IsOwnerActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhotonView_get_IsOwnerActive_mA17BFB41B289C7CD1A75B2AA27DAB90A69FEC87F (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	{
		Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * L_0 = PhotonView_get_Owner_mD339E981ADADF5A7F88F6D4F530F1EB8F1C34DEE(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * L_1 = PhotonView_get_Owner_mD339E981ADADF5A7F88F6D4F530F1EB8F1C34DEE(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Player_get_IsInactive_m565C1FB590912C70E7720C6E1EFFFD5EE755C403_inline(L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}

IL_0017:
	{
		return (bool)0;
	}
}
// System.Int32 Photon.Pun.PhotonView::get_CreatorActorNr()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PhotonView_get_CreatorActorNr_m182100823912B7A313C17D0844D599BFAAA03629 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_get_CreatorActorNr_m182100823912B7A313C17D0844D599BFAAA03629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_viewIdField_16();
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		int32_t L_1 = ((PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields*)il2cpp_codegen_static_fields_for(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var))->get_MAX_VIEW_IDS_3();
		return ((int32_t)((int32_t)L_0/(int32_t)L_1));
	}
}
// System.Boolean Photon.Pun.PhotonView::get_IsMine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PhotonView_get_IsMine_mFE1D0AB5684F18073518777C1D8FF764CB335793 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_get_IsMine_mFE1D0AB5684F18073518777C1D8FF764CB335793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PhotonView_get_OwnerActorNr_m297F5F405EF3ACE0A262F942FF01357EE4BDE886(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * L_1 = PhotonNetwork_get_LocalPlayer_m06E2D9682B713C828A00C0BD4F03A0CBC1E2D1B6(/*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Player_get_ActorNumber_m02157560C558C6007C8146F1FCDA7E759AD0CF1D_inline(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		bool L_3 = PhotonNetwork_get_IsMasterClient_m7EE228BA31428CCA42C74D8805DD9B3A951E7E57(/*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		bool L_4 = PhotonView_get_IsOwnerActive_mA17BFB41B289C7CD1A75B2AA27DAB90A69FEC87F(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		return (bool)1;
	}
}
// System.Void Photon.Pun.PhotonView::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_Awake_mA6E574DBACD2811EDD303783CB7C256F6ACAEA4F (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_Awake_mA6E574DBACD2811EDD303783CB7C256F6ACAEA4F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PhotonView_get_ViewID_mC15D092CC4BFE8638C31F9622B7DF6894E30D3D9_inline(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_1 = PhotonView_get_ViewID_mC15D092CC4BFE8638C31F9622B7DF6894E30D3D9_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		int32_t L_2 = ((PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields*)il2cpp_codegen_static_fields_for(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var))->get_MAX_VIEW_IDS_3();
		__this->set_ownerId_4(((int32_t)((int32_t)L_1/(int32_t)L_2)));
		PhotonNetwork_RegisterPhotonView_m4E523EEA4FE46B919ACBC4EEB2FF76AC2ED5A25F(__this, /*hidden argument*/NULL);
	}

IL_0020:
	{
		__this->set_didAwake_18((bool)1);
		return;
	}
}
// System.Void Photon.Pun.PhotonView::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_OnDestroy_mFE646382844BCDEF6001AAE47167BD704975D8DE (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_OnDestroy_mFE646382844BCDEF6001AAE47167BD704975D8DE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_removedFromLocalViewList_20();
		if (L_0)
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		bool L_1 = PhotonNetwork_LocalCleanPhotonView_m843AF475A4D66F8224E414855427B78D2F14D9B2(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_2 = __this->get_InstantiationId_17();
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0047;
		}
	}
	{
		bool L_3 = ((ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0_StaticFields*)il2cpp_codegen_static_fields_for(ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0_il2cpp_TypeInfo_var))->get_AppQuits_12();
		if (L_3)
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		int32_t L_4 = ((PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_StaticFields*)il2cpp_codegen_static_fields_for(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var))->get_LogLevel_8();
		if ((((int32_t)L_4) < ((int32_t)1)))
		{
			goto IL_0047;
		}
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_5, /*hidden argument*/NULL);
		String_t* L_7 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteral823C156003F996BBF5384D39F2EE6DA95992F844, L_6, _stringLiteral379B5CC68F0A8B7B36ADBB4201083ED4AE209AD4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_7, /*hidden argument*/NULL);
	}

IL_0047:
	{
		return;
	}
}
// System.Void Photon.Pun.PhotonView::RequestOwnership()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_RequestOwnership_mFD54041B0C70B516EC2DB1C12E606825551AB9F0 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_RequestOwnership_mFD54041B0C70B516EC2DB1C12E606825551AB9F0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PhotonView_get_ViewID_mC15D092CC4BFE8638C31F9622B7DF6894E30D3D9_inline(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_ownerId_4();
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		PhotonNetwork_RequestOwnership_m878535F79D0B82193E6FDAE66B6C29467A8AC4C5(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Photon.Pun.PhotonView::TransferOwnership(Photon.Realtime.Player)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_TransferOwnership_m9FD7BE2FEC4A04A972F9386717BDF904F265F071 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * ___newOwner0, const RuntimeMethod* method)
{
	{
		Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * L_0 = ___newOwner0;
		NullCheck(L_0);
		int32_t L_1 = Player_get_ActorNumber_m02157560C558C6007C8146F1FCDA7E759AD0CF1D_inline(L_0, /*hidden argument*/NULL);
		PhotonView_TransferOwnership_mE63F4A1B73D7E89DEC4261C163880A3BEB5F5B76(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Photon.Pun.PhotonView::TransferOwnership(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_TransferOwnership_mE63F4A1B73D7E89DEC4261C163880A3BEB5F5B76 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, int32_t ___newOwnerId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_TransferOwnership_mE63F4A1B73D7E89DEC4261C163880A3BEB5F5B76_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PhotonView_get_ViewID_mC15D092CC4BFE8638C31F9622B7DF6894E30D3D9_inline(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___newOwnerId0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		PhotonNetwork_TransferOwnership_m4A9E407E53070DEF7479C557BCDCD18E1E085486(L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___newOwnerId0;
		__this->set_ownerId_4(L_2);
		return;
	}
}
// System.Void Photon.Pun.PhotonView::SerializeView(Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_SerializeView_m4FA3E888E8C7C3A959ABEAD9194CFEBFFA709156 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream0, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_SerializeView_m4FA3E888E8C7C3A959ABEAD9194CFEBFFA709156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * L_0 = __this->get_ObservedComponents_15();
		if (!L_0)
		{
			goto IL_0040;
		}
	}
	{
		List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * L_1 = __this->get_ObservedComponents_15();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m9EFB9DB6274EEB0D4B5050480FE1E8CA56B85534_inline(L_1, /*hidden argument*/List_1_get_Count_m9EFB9DB6274EEB0D4B5050480FE1E8CA56B85534_RuntimeMethod_var);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0040;
		}
	}
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_001a:
	{
		List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * L_3 = __this->get_ObservedComponents_15();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_5 = List_1_get_Item_mA6C019255203C3A859BB0856B40200EA1E5F8EDA_inline(L_3, L_4, /*hidden argument*/List_1_get_Item_mA6C019255203C3A859BB0856B40200EA1E5F8EDA_RuntimeMethod_var);
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_6 = ___stream0;
		PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  L_7 = ___info1;
		PhotonView_SerializeComponent_m745D96F1F125E057101C1C52E4063E1FD74424FF(__this, L_5, L_6, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0032:
	{
		int32_t L_9 = V_0;
		List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * L_10 = __this->get_ObservedComponents_15();
		NullCheck(L_10);
		int32_t L_11 = List_1_get_Count_m9EFB9DB6274EEB0D4B5050480FE1E8CA56B85534_inline(L_10, /*hidden argument*/List_1_get_Count_m9EFB9DB6274EEB0D4B5050480FE1E8CA56B85534_RuntimeMethod_var);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_001a;
		}
	}

IL_0040:
	{
		return;
	}
}
// System.Void Photon.Pun.PhotonView::DeserializeView(Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_DeserializeView_m2E67D40D896990811E722E1FB99BC5BFEC46EFB2 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream0, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_DeserializeView_m2E67D40D896990811E722E1FB99BC5BFEC46EFB2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * L_0 = __this->get_ObservedComponents_15();
		if (!L_0)
		{
			goto IL_0040;
		}
	}
	{
		List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * L_1 = __this->get_ObservedComponents_15();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m9EFB9DB6274EEB0D4B5050480FE1E8CA56B85534_inline(L_1, /*hidden argument*/List_1_get_Count_m9EFB9DB6274EEB0D4B5050480FE1E8CA56B85534_RuntimeMethod_var);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0040;
		}
	}
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_001a:
	{
		List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * L_3 = __this->get_ObservedComponents_15();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_5 = List_1_get_Item_mA6C019255203C3A859BB0856B40200EA1E5F8EDA_inline(L_3, L_4, /*hidden argument*/List_1_get_Item_mA6C019255203C3A859BB0856B40200EA1E5F8EDA_RuntimeMethod_var);
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_6 = ___stream0;
		PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  L_7 = ___info1;
		PhotonView_DeserializeComponent_m556FF3D2A81CA3B9A6371944CDC9F728820F7788(__this, L_5, L_6, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0032:
	{
		int32_t L_9 = V_0;
		List_1_tAAE8BF32F260E5939A1EAF05F4C38C7841B64300 * L_10 = __this->get_ObservedComponents_15();
		NullCheck(L_10);
		int32_t L_11 = List_1_get_Count_m9EFB9DB6274EEB0D4B5050480FE1E8CA56B85534_inline(L_10, /*hidden argument*/List_1_get_Count_m9EFB9DB6274EEB0D4B5050480FE1E8CA56B85534_RuntimeMethod_var);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_001a;
		}
	}

IL_0040:
	{
		return;
	}
}
// System.Void Photon.Pun.PhotonView::DeserializeComponent(UnityEngine.Component,Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_DeserializeComponent_m556FF3D2A81CA3B9A6371944CDC9F728820F7788 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___component0, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream1, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_DeserializeComponent_m556FF3D2A81CA3B9A6371944CDC9F728820F7788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_0 = ___component0;
		V_0 = ((RuntimeObject*)IsInst((RuntimeObject*)L_0, IPunObservable_t885A5BFE290BD3AC3942882E7533681C488F048C_il2cpp_TypeInfo_var));
		RuntimeObject* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_2 = V_0;
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_3 = ___stream1;
		PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  L_4 = ___info2;
		NullCheck(L_2);
		InterfaceActionInvoker2< PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E *, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  >::Invoke(0 /* System.Void Photon.Pun.IPunObservable::OnPhotonSerializeView(Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo) */, IPunObservable_t885A5BFE290BD3AC3942882E7533681C488F048C_il2cpp_TypeInfo_var, L_2, L_3, L_4);
		return;
	}

IL_0013:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral641A80CA205D00BD50B62CE6056B444CA346C0C0);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral641A80CA205D00BD50B62CE6056B444CA346C0C0);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_7 = L_6;
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_8 = ___component0;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_7;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral91B876DF2F22F0B715909B534ED040817CB27572);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral91B876DF2F22F0B715909B534ED040817CB27572);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_10 = L_9;
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_11 = ___component0;
		NullCheck(L_11);
		Type_t * L_12 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_12);
		String_t* L_13 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_10, /*hidden argument*/NULL);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_14 = ___component0;
		NullCheck(L_14);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_15 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m97139CB2EE76D5CD8308C1AD0499A5F163FC7F51(L_13, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Photon.Pun.PhotonView::SerializeComponent(UnityEngine.Component,Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_SerializeComponent_m745D96F1F125E057101C1C52E4063E1FD74424FF (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___component0, PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * ___stream1, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  ___info2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_SerializeComponent_m745D96F1F125E057101C1C52E4063E1FD74424FF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_0 = ___component0;
		V_0 = ((RuntimeObject*)IsInst((RuntimeObject*)L_0, IPunObservable_t885A5BFE290BD3AC3942882E7533681C488F048C_il2cpp_TypeInfo_var));
		RuntimeObject* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_2 = V_0;
		PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * L_3 = ___stream1;
		PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  L_4 = ___info2;
		NullCheck(L_2);
		InterfaceActionInvoker2< PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E *, PhotonMessageInfo_t0C7A1F60E266F5D63765B22A1116E0DA0BB6CAE0  >::Invoke(0 /* System.Void Photon.Pun.IPunObservable::OnPhotonSerializeView(Photon.Pun.PhotonStream,Photon.Pun.PhotonMessageInfo) */, IPunObservable_t885A5BFE290BD3AC3942882E7533681C488F048C_il2cpp_TypeInfo_var, L_2, L_3, L_4);
		return;
	}

IL_0013:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral641A80CA205D00BD50B62CE6056B444CA346C0C0);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral641A80CA205D00BD50B62CE6056B444CA346C0C0);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_7 = L_6;
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_8 = ___component0;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_7;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral91B876DF2F22F0B715909B534ED040817CB27572);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral91B876DF2F22F0B715909B534ED040817CB27572);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_10 = L_9;
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_11 = ___component0;
		NullCheck(L_11);
		Type_t * L_12 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_12);
		String_t* L_13 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_10, /*hidden argument*/NULL);
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_14 = ___component0;
		NullCheck(L_14);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_15 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m97139CB2EE76D5CD8308C1AD0499A5F163FC7F51(L_13, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Photon.Pun.PhotonView::RefreshRpcMonoBehaviourCache()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_RefreshRpcMonoBehaviourCache_m218A196211F926F6B1FD74910E3E87F2D5F05DD1 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_RefreshRpcMonoBehaviourCache_m218A196211F926F6B1FD74910E3E87F2D5F05DD1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* L_0 = Component_GetComponents_TisMonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429_mFF32883CC15055E3DA209D02FFE7DFEB78C12741(__this, /*hidden argument*/Component_GetComponents_TisMonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429_mFF32883CC15055E3DA209D02FFE7DFEB78C12741_RuntimeMethod_var);
		__this->set_RpcMonoBehaviours_21(L_0);
		return;
	}
}
// System.Void Photon.Pun.PhotonView::RPC(System.String,Photon.Pun.RpcTarget,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_RPC_mF2E2691617AC053125729CA8D9D1AEF14A78A301 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, String_t* ___methodName0, int32_t ___target1, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___parameters2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_RPC_mF2E2691617AC053125729CA8D9D1AEF14A78A301_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___target1;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = ___parameters2;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		PhotonNetwork_RPC_m2A2AEB859A31D6676325C7DEFC0765454A74899B(__this, L_0, L_1, (bool)0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Photon.Pun.PhotonView::RpcSecure(System.String,Photon.Pun.RpcTarget,System.Boolean,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_RpcSecure_m256A5ABAB0FF68642767ECE1D1CD1C9E391C4B16 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, String_t* ___methodName0, int32_t ___target1, bool ___encrypt2, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___parameters3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_RpcSecure_m256A5ABAB0FF68642767ECE1D1CD1C9E391C4B16_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___target1;
		bool L_2 = ___encrypt2;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = ___parameters3;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		PhotonNetwork_RPC_m2A2AEB859A31D6676325C7DEFC0765454A74899B(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Photon.Pun.PhotonView::RPC(System.String,Photon.Realtime.Player,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_RPC_m3F59D00D60E09EEFCC5B5000BB7195F25689029C (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, String_t* ___methodName0, Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * ___targetPlayer1, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___parameters2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_RPC_m3F59D00D60E09EEFCC5B5000BB7195F25689029C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___methodName0;
		Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * L_1 = ___targetPlayer1;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = ___parameters2;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		PhotonNetwork_RPC_m3A324E2F867852F769217DD61E03E902E4A642B7(__this, L_0, L_1, (bool)0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Photon.Pun.PhotonView::RpcSecure(System.String,Photon.Realtime.Player,System.Boolean,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView_RpcSecure_m769EF34360B1DE9B0F7FD51F77E39AF231E36B1F (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, String_t* ___methodName0, Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * ___targetPlayer1, bool ___encrypt2, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___parameters3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_RpcSecure_m769EF34360B1DE9B0F7FD51F77E39AF231E36B1F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___methodName0;
		Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * L_1 = ___targetPlayer1;
		bool L_2 = ___encrypt2;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = ___parameters3;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		PhotonNetwork_RPC_m3A324E2F867852F769217DD61E03E902E4A642B7(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// Photon.Pun.PhotonView Photon.Pun.PhotonView::Get(UnityEngine.Component)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * PhotonView_Get_mFABADCC5E785D810809CC040BE8A1AA299B8D857 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___component0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_Get_mFABADCC5E785D810809CC040BE8A1AA299B8D857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_0 = ___component0;
		NullCheck(L_0);
		PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * L_1 = Component_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_m347A176252862D6CD26E7E6B5E9EAF8F4159273B(L_0, /*hidden argument*/Component_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_m347A176252862D6CD26E7E6B5E9EAF8F4159273B_RuntimeMethod_var);
		return L_1;
	}
}
// Photon.Pun.PhotonView Photon.Pun.PhotonView::Get(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * PhotonView_Get_mF77DAA2CC8B2E80DBBD4588D6B5264AF853CE343 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_Get_mF77DAA2CC8B2E80DBBD4588D6B5264AF853CE343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = ___gameObj0;
		NullCheck(L_0);
		PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * L_1 = GameObject_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_mC78C8E2525ECF7D840C9BB1A311E001856163E30(L_0, /*hidden argument*/GameObject_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_mC78C8E2525ECF7D840C9BB1A311E001856163E30_RuntimeMethod_var);
		return L_1;
	}
}
// Photon.Pun.PhotonView Photon.Pun.PhotonView::Find(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * PhotonView_Find_m74440585ADB7D8B89E260397726D052BAA9F9B3E (int32_t ___viewID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_Find_m74440585ADB7D8B89E260397726D052BAA9F9B3E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___viewID0;
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * L_1 = PhotonNetwork_GetPhotonView_mC8007120235F150ECB55FD563D1CE42E8D5AF153(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String Photon.Pun.PhotonView::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PhotonView_ToString_m9C6ED6975CD25D9820A71581FCC5C1BBBF1F58B4 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhotonView_ToString_m9C6ED6975CD25D9820A71581FCC5C1BBBF1F58B4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B2_1 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B1_1 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B3_2 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B3_3 = NULL;
	String_t* G_B3_4 = NULL;
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B5_1 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B5_2 = NULL;
	String_t* G_B5_3 = NULL;
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B4_1 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B4_2 = NULL;
	String_t* G_B4_3 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B6_2 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B6_3 = NULL;
	String_t* G_B6_4 = NULL;
	int32_t G_B8_0 = 0;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B8_1 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B8_2 = NULL;
	String_t* G_B8_3 = NULL;
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B7_1 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B7_2 = NULL;
	String_t* G_B7_3 = NULL;
	String_t* G_B9_0 = NULL;
	int32_t G_B9_1 = 0;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B9_2 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* G_B9_3 = NULL;
	String_t* G_B9_4 = NULL;
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		int32_t L_2 = PhotonView_get_ViewID_mC15D092CC4BFE8638C31F9622B7DF6894E30D3D9_inline(__this, /*hidden argument*/NULL);
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_1;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_6, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		G_B1_0 = 1;
		G_B1_1 = L_5;
		G_B1_2 = L_5;
		G_B1_3 = _stringLiteralDB6C224CF890308B59EE451807A94B4A842EBB02;
		if (L_7)
		{
			G_B2_0 = 1;
			G_B2_1 = L_5;
			G_B2_2 = L_5;
			G_B2_3 = _stringLiteralDB6C224CF890308B59EE451807A94B4A842EBB02;
			goto IL_0030;
		}
	}
	{
		G_B3_0 = _stringLiteralD75636B15D4B8F123C4162CB1DF0929699312854;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_003b;
	}

IL_0030:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_8 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_8, /*hidden argument*/NULL);
		G_B3_0 = L_9;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_003b:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (RuntimeObject *)G_B3_0);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_10 = G_B3_3;
		bool L_11 = PhotonView_get_IsSceneView_m6E6B2B67B70DEE273B1BABBF739BBAA775CB5ACA(__this, /*hidden argument*/NULL);
		G_B4_0 = 2;
		G_B4_1 = L_10;
		G_B4_2 = L_10;
		G_B4_3 = G_B3_4;
		if (L_11)
		{
			G_B5_0 = 2;
			G_B5_1 = L_10;
			G_B5_2 = L_10;
			G_B5_3 = G_B3_4;
			goto IL_004d;
		}
	}
	{
		String_t* L_12 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		goto IL_0052;
	}

IL_004d:
	{
		G_B6_0 = _stringLiteral453CDC1FEBD60FCF5E5819B6B0ED9F59FB60C270;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
	}

IL_0052:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (RuntimeObject *)G_B6_0);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = G_B6_3;
		int32_t L_14 = PhotonView_get_Prefix_m4EBD59509A85CF1D43C09F70EA41AA2CE20F3602(__this, /*hidden argument*/NULL);
		G_B7_0 = 3;
		G_B7_1 = L_13;
		G_B7_2 = L_13;
		G_B7_3 = G_B6_4;
		if ((((int32_t)L_14) > ((int32_t)0)))
		{
			G_B8_0 = 3;
			G_B8_1 = L_13;
			G_B8_2 = L_13;
			G_B8_3 = G_B6_4;
			goto IL_0065;
		}
	}
	{
		G_B9_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		G_B9_4 = G_B7_3;
		goto IL_007a;
	}

IL_0065:
	{
		int32_t L_15 = PhotonView_get_Prefix_m4EBD59509A85CF1D43C09F70EA41AA2CE20F3602(__this, /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		RuntimeObject * L_17 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_16);
		String_t* L_18 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral00DE275EEC87CFE3541333E3B3214CBEA0B6C47F, L_17, /*hidden argument*/NULL);
		G_B9_0 = L_18;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
		G_B9_4 = G_B8_3;
	}

IL_007a:
	{
		NullCheck(G_B9_2);
		ArrayElementTypeCheck (G_B9_2, G_B9_0);
		(G_B9_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_1), (RuntimeObject *)G_B9_0);
		String_t* L_19 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(G_B9_4, G_B9_3, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Void Photon.Pun.PhotonView::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhotonView__ctor_mC9178F9A310A82BC5C8DEF9811E254D01823ECA8 (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	{
		__this->set_prefixField_8((-1));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Photon.Pun.PunEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PunEvent__ctor_m723CC3E4424B2BD0A3C68C437CD8AB135FF6BC4C (PunEvent_t7D66CEAFA04E9A4EEEA16858A28136B5B4C4CE39 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Reflection.ParameterInfo[] Photon.Pun.PunExtensions::GetCachedParemeters(System.Reflection.MethodInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694* PunExtensions_GetCachedParemeters_mBD43BDB3CBD4167A1E49E72949C633762DC8105D (MethodInfo_t * ___mo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PunExtensions_GetCachedParemeters_mBD43BDB3CBD4167A1E49E72949C633762DC8105D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PunExtensions_tE401775F4D752FBE7F8267AE3290ACE975B542DC_il2cpp_TypeInfo_var);
		Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF * L_0 = ((PunExtensions_tE401775F4D752FBE7F8267AE3290ACE975B542DC_StaticFields*)il2cpp_codegen_static_fields_for(PunExtensions_tE401775F4D752FBE7F8267AE3290ACE975B542DC_il2cpp_TypeInfo_var))->get_ParametersOfMethods_0();
		MethodInfo_t * L_1 = ___mo0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m44391EE1219E87B732BAD0E46BC433083B9BABB0(L_0, L_1, (ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694**)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m44391EE1219E87B732BAD0E46BC433083B9BABB0_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		MethodInfo_t * L_3 = ___mo0;
		NullCheck(L_3);
		ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694* L_4 = VirtFuncInvoker0< ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694* >::Invoke(17 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_3);
		V_0 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(PunExtensions_tE401775F4D752FBE7F8267AE3290ACE975B542DC_il2cpp_TypeInfo_var);
		Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF * L_5 = ((PunExtensions_tE401775F4D752FBE7F8267AE3290ACE975B542DC_StaticFields*)il2cpp_codegen_static_fields_for(PunExtensions_tE401775F4D752FBE7F8267AE3290ACE975B542DC_il2cpp_TypeInfo_var))->get_ParametersOfMethods_0();
		MethodInfo_t * L_6 = ___mo0;
		ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694* L_7 = V_0;
		NullCheck(L_5);
		Dictionary_2_set_Item_m82085B12B5862B10FC15F89AF6C6C5F7F4548040(L_5, L_6, L_7, /*hidden argument*/Dictionary_2_set_Item_m82085B12B5862B10FC15F89AF6C6C5F7F4548040_RuntimeMethod_var);
	}

IL_0022:
	{
		ParameterInfoU5BU5D_t9F6F38E4A0B0A78E2F463D1B2C0031716CA7A694* L_8 = V_0;
		return L_8;
	}
}
// Photon.Pun.PhotonView[] Photon.Pun.PunExtensions::GetPhotonViewsInChildren(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PhotonViewU5BU5D_t79E798C13CF3CB9516F65B6A4C1E65491FF5CA27* PunExtensions_GetPhotonViewsInChildren_m6AE772AD8960DAC9540669A39A2ED5A7984D45DB (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PunExtensions_GetPhotonViewsInChildren_m6AE772AD8960DAC9540669A39A2ED5A7984D45DB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = ___go0;
		NullCheck(L_0);
		PhotonViewU5BU5D_t79E798C13CF3CB9516F65B6A4C1E65491FF5CA27* L_1 = GameObject_GetComponentsInChildren_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_m318AB55027D265F28D3BC79BEA2BE0434A568E62(L_0, (bool)1, /*hidden argument*/GameObject_GetComponentsInChildren_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_m318AB55027D265F28D3BC79BEA2BE0434A568E62_RuntimeMethod_var);
		return L_1;
	}
}
// Photon.Pun.PhotonView Photon.Pun.PunExtensions::GetPhotonView(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * PunExtensions_GetPhotonView_mDA3596A23EAA5C6C2B0DBAC323D16A7C8D628412 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PunExtensions_GetPhotonView_mDA3596A23EAA5C6C2B0DBAC323D16A7C8D628412_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = ___go0;
		NullCheck(L_0);
		PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * L_1 = GameObject_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_mC78C8E2525ECF7D840C9BB1A311E001856163E30(L_0, /*hidden argument*/GameObject_GetComponent_TisPhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B_mC78C8E2525ECF7D840C9BB1A311E001856163E30_RuntimeMethod_var);
		return L_1;
	}
}
// System.Boolean Photon.Pun.PunExtensions::AlmostEquals(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PunExtensions_AlmostEquals_m576E61725EF1B1E75659AB3616246EBE180BDFB1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___target0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___second1, float ___sqrMagnitudePrecision2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PunExtensions_AlmostEquals_m576E61725EF1B1E75659AB3616246EBE180BDFB1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___target0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___second1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector3_get_sqrMagnitude_m1C6E190B4A933A183B308736DEC0DD64B0588968((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), /*hidden argument*/NULL);
		float L_4 = ___sqrMagnitudePrecision2;
		return (bool)((((float)L_3) < ((float)L_4))? 1 : 0);
	}
}
// System.Boolean Photon.Pun.PunExtensions::AlmostEquals(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PunExtensions_AlmostEquals_m1A0F117F7E0EAB9BADEA0CF8C1B2F702530694B8 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___target0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___second1, float ___sqrMagnitudePrecision2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PunExtensions_AlmostEquals_m1A0F117F7E0EAB9BADEA0CF8C1B2F702530694B8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_0 = ___target0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_1 = ___second1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector2_get_sqrMagnitude_mAEE10A8ECE7D5754E10727BA8C9068A759AD7002((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_0), /*hidden argument*/NULL);
		float L_4 = ___sqrMagnitudePrecision2;
		return (bool)((((float)L_3) < ((float)L_4))? 1 : 0);
	}
}
// System.Boolean Photon.Pun.PunExtensions::AlmostEquals(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PunExtensions_AlmostEquals_m945D736C9D101B6DACE0B2001733CB97C24E22F3 (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___target0, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___second1, float ___maxAngle2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PunExtensions_AlmostEquals_m945D736C9D101B6DACE0B2001733CB97C24E22F3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_0 = ___target0;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_1 = ___second1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		float L_2 = Quaternion_Angle_m09599D660B724D330E5C7FE2FB1C8716161B3DD1(L_0, L_1, /*hidden argument*/NULL);
		float L_3 = ___maxAngle2;
		return (bool)((((float)L_2) < ((float)L_3))? 1 : 0);
	}
}
// System.Boolean Photon.Pun.PunExtensions::AlmostEquals(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PunExtensions_AlmostEquals_mE1D4C852AC99DC32AEA769349F6DD6BC8D437057 (float ___target0, float ___second1, float ___floatDiff2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PunExtensions_AlmostEquals_mE1D4C852AC99DC32AEA769349F6DD6BC8D437057_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___target0;
		float L_1 = ___second1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)));
		float L_3 = ___floatDiff2;
		return (bool)((((float)L_2) < ((float)L_3))? 1 : 0);
	}
}
// System.Void Photon.Pun.PunExtensions::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PunExtensions__cctor_m7F398F5699A60C0DD89C3EFB7D8CF796CE5F12B9 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PunExtensions__cctor_m7F398F5699A60C0DD89C3EFB7D8CF796CE5F12B9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF * L_0 = (Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF *)il2cpp_codegen_object_new(Dictionary_2_t175D9FB8ACCADE63EB30815EFF5775018F07E5FF_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mDE47E426BCB97B4E95703C8B6360F6D16DD9C4F7(L_0, /*hidden argument*/Dictionary_2__ctor_mDE47E426BCB97B4E95703C8B6360F6D16DD9C4F7_RuntimeMethod_var);
		((PunExtensions_tE401775F4D752FBE7F8267AE3290ACE975B542DC_StaticFields*)il2cpp_codegen_static_fields_for(PunExtensions_tE401775F4D752FBE7F8267AE3290ACE975B542DC_il2cpp_TypeInfo_var))->set_ParametersOfMethods_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Photon.Pun.PunRPC::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PunRPC__ctor_mC20AE4678DE711A2A0DCAAA8B4B2C19D101CC110 (PunRPC_t49B358D1BE3E291A6F2FE4674A99AF7C8D2295DE * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Photon.Pun.SceneManagerHelper::get_ActiveSceneName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SceneManagerHelper_get_ActiveSceneName_mBDF8127A210FDA44A6942B8FABE3574318A2FAC1 (const RuntimeMethod* method)
{
	Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  L_0 = SceneManager_GetActiveScene_mD583193D329EBF540D8AB8A062681B1C2AB5EA51(/*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Scene_get_name_m0E63ED0F050FCC35A4216220C584BE3D3F77B0E1((Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2 *)(&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Photon.Pun.SceneManagerHelper::get_ActiveSceneBuildIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SceneManagerHelper_get_ActiveSceneBuildIndex_m37B9CF41489DC53231E224CEBB3C4C81955CD0E6 (const RuntimeMethod* method)
{
	Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2  L_0 = SceneManager_GetActiveScene_mD583193D329EBF540D8AB8A062681B1C2AB5EA51(/*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m764659943B7357F5D6C9165F68EDCFBBDDD6C3C2((Scene_t942E023788C2BC9FBB7EC8356B4FB0088B2CFED2 *)(&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Photon.Pun.SceneManagerHelper::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManagerHelper__ctor_m7577BA80EEC41712CA53E2259410D1C3FCF96A79 (SceneManagerHelper_t364642E9E40D66062319539AF06FC0BE69FE795A * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Photon.Pun.ServerSettings::UseCloud(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ServerSettings_UseCloud_m944D5F42BD2330D1EC6CBC75F0FB36B9B7AF9704 (ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9 * __this, String_t* ___cloudAppid0, String_t* ___code1, const RuntimeMethod* method)
{
	AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * G_B2_0 = NULL;
	AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * G_B3_1 = NULL;
	{
		AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * L_0 = __this->get_AppSettings_4();
		String_t* L_1 = ___cloudAppid0;
		NullCheck(L_0);
		L_0->set_AppIdRealtime_0(L_1);
		AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * L_2 = __this->get_AppSettings_4();
		NullCheck(L_2);
		L_2->set_Server_6((String_t*)NULL);
		AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * L_3 = __this->get_AppSettings_4();
		String_t* L_4 = ___code1;
		bool L_5 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229(L_4, /*hidden argument*/NULL);
		G_B1_0 = L_3;
		if (L_5)
		{
			G_B2_0 = L_3;
			goto IL_0029;
		}
	}
	{
		String_t* L_6 = ___code1;
		G_B3_0 = L_6;
		G_B3_1 = G_B1_0;
		goto IL_002a;
	}

IL_0029:
	{
		G_B3_0 = ((String_t*)(NULL));
		G_B3_1 = G_B2_0;
	}

IL_002a:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_FixedRegion_5(G_B3_0);
		return;
	}
}
// System.Boolean Photon.Pun.ServerSettings::IsAppId(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ServerSettings_IsAppId_m728E76CE95E25CDA610ECDCC949A547E7A55E6CF (String_t* ___val0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServerSettings_IsAppId_m728E76CE95E25CDA610ECDCC949A547E7A55E6CF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		String_t* L_0 = ___val0;
		Guid_t  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Guid__ctor_mC668142577A40A77D13B78AADDEFFFC2E2705079((&L_1), L_0, /*hidden argument*/NULL);
		goto IL_000e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0009;
		throw e;
	}

CATCH_0009:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0010;
	} // end catch (depth: 1)

IL_000e:
	{
		return (bool)1;
	}

IL_0010:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.String Photon.Pun.ServerSettings::get_BestRegionSummaryInPreferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ServerSettings_get_BestRegionSummaryInPreferences_mFCEF00C287482CD83E379A87574B085F0D150AF8 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServerSettings_get_BestRegionSummaryInPreferences_mFCEF00C287482CD83E379A87574B085F0D150AF8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		String_t* L_0 = PhotonNetwork_get_BestRegionSummaryInPreferences_m3A4C27D397D7C07BF59B7675A3CBC1A3B232AF7D(/*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Photon.Pun.ServerSettings::ResetBestRegionCodeInPreferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ServerSettings_ResetBestRegionCodeInPreferences_m59742F06C329B5EA26D7B070795CAB5775D951AA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServerSettings_ResetBestRegionCodeInPreferences_m59742F06C329B5EA26D7B070795CAB5775D951AA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PhotonNetwork_tDB10770C2795BD94850C3BE2AA04AE3ED559EB5A_il2cpp_TypeInfo_var);
		PhotonNetwork_set_BestRegionSummaryInPreferences_mFEA6A32B0FADC858803016461E656BA4BD884961((String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.String Photon.Pun.ServerSettings::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ServerSettings_ToString_mFCA84BBA9102D219FA2C384B5A9F46392D34A736 (ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServerSettings_ToString_mFCA84BBA9102D219FA2C384B5A9F46392D34A736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * L_0 = __this->get_AppSettings_4();
		NullCheck(L_0);
		String_t* L_1 = AppSettings_ToStringFull_m1C7E9BA1BBCB3C2AA844F4928FE9676AD5359750(L_0, /*hidden argument*/NULL);
		String_t* L_2 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralA74E0FB14579A337D921569501855EE2A21B81D8, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Photon.Pun.ServerSettings::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ServerSettings__ctor_mC3ABDBA3917DC9D5F3065BCA69ED7B457BB68004 (ServerSettings_t41FA10CB281A2D884146801E3E74770B97862AA9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServerSettings__ctor_mC3ABDBA3917DC9D5F3065BCA69ED7B457BB68004_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_RunInBackground_7((bool)1);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06(L_0, /*hidden argument*/List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var);
		__this->set_RpcList_9(L_0);
		ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool PhotonStream_get_IsWriting_mB16D73A32F0C3C1CE9272057F667CE15C1DBC458_inline (PhotonStream_t3E8F04756FBB266F05C8D10E0801F8CA9099B98E * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CIsWritingU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewPositionControl_GetNetworkPosition_m047475370A825DB4FCAB3BAB86E6029CC98C89A1_inline (PhotonTransformViewPositionControl_t8C6D484DDC2D49B5A28664A01575F42788B64737 * __this, const RuntimeMethod* method)
{
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = __this->get_m_NetworkPosition_5();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  PhotonTransformViewRotationControl_GetNetworkRotation_m9C232BEB12E8EBF1A79A4331A24363F5B30CCE14_inline (PhotonTransformViewRotationControl_tFFE6B30E13CE4639869B3FBA84D8E8B304F284DE * __this, const RuntimeMethod* method)
{
	{
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_0 = __this->get_m_NetworkRotation_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  PhotonTransformViewScaleControl_GetNetworkScale_mA0936FD6D72F6BF7EDE3966230D0AC367B81EB36_inline (PhotonTransformViewScaleControl_t37826D3F1E95E8193871F84B3E3803642428A75E * __this, const RuntimeMethod* method)
{
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = __this->get_m_NetworkScale_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PhotonView_get_ViewID_mC15D092CC4BFE8638C31F9622B7DF6894E30D3D9_inline (PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_viewIdField_16();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t Player_get_ActorNumber_m02157560C558C6007C8146F1FCDA7E759AD0CF1D_inline (Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_actorNumber_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool Player_get_IsInactive_m565C1FB590912C70E7720C6E1EFFFD5EE755C403_inline (Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CIsInactiveU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_2, (int32_t)L_3);
		return L_4;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t Queue_1_get_Count_m27CC19FF14FCE31924EFEF64A7F080720D8DA4E0_gshared_inline (Queue_1_t8C1AFA4617CF7DCC333DAA39272734AD3DBE4BAE * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_3();
		return L_0;
	}
}
