﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Boolean Photon.Realtime.AppSettings::get_IsMasterServerAddress()
extern void AppSettings_get_IsMasterServerAddress_m2F43A395E93E666FA86C8D6994E00260BAD434A5 ();
// 0x00000002 System.Boolean Photon.Realtime.AppSettings::get_IsBestRegion()
extern void AppSettings_get_IsBestRegion_m534861CD875A9D5E8801052024D12A5F31EA1FE7 ();
// 0x00000003 System.Boolean Photon.Realtime.AppSettings::get_IsDefaultNameServer()
extern void AppSettings_get_IsDefaultNameServer_m70F8F26188A7EB0B8D55A9904A2F51498B36C941 ();
// 0x00000004 System.Boolean Photon.Realtime.AppSettings::get_IsDefaultPort()
extern void AppSettings_get_IsDefaultPort_mFD502FC4090BAE524899B8FE8D73F783A7BEF851 ();
// 0x00000005 System.String Photon.Realtime.AppSettings::ToStringFull()
extern void AppSettings_ToStringFull_m1C7E9BA1BBCB3C2AA844F4928FE9676AD5359750 ();
// 0x00000006 System.Void Photon.Realtime.AppSettings::.ctor()
extern void AppSettings__ctor_mEE7F23500B21898BF51BD410D1CB360DEBF66931 ();
// 0x00000007 Photon.Realtime.LoadBalancingClient Photon.Realtime.ConnectionHandler::get_Client()
extern void ConnectionHandler_get_Client_mD602A5800223D074F0CF6B96FFE8529F545A4E80 ();
// 0x00000008 System.Void Photon.Realtime.ConnectionHandler::set_Client(Photon.Realtime.LoadBalancingClient)
extern void ConnectionHandler_set_Client_m41E8AA7C73CEC09E0C73FE3532C233F109DCBC45 ();
// 0x00000009 System.Int32 Photon.Realtime.ConnectionHandler::get_CountSendAcksOnly()
extern void ConnectionHandler_get_CountSendAcksOnly_m91028B52B53946478ACB442ED9DD1C16E7115C57 ();
// 0x0000000A System.Void Photon.Realtime.ConnectionHandler::set_CountSendAcksOnly(System.Int32)
extern void ConnectionHandler_set_CountSendAcksOnly_mD2D80CCEEBAACC896F8B35C6938F734E01C4104E ();
// 0x0000000B System.Boolean Photon.Realtime.ConnectionHandler::get_FallbackThreadRunning()
extern void ConnectionHandler_get_FallbackThreadRunning_m1BE4BE852820C4869BBBC66C21F57B9E573620B7 ();
// 0x0000000C System.Void Photon.Realtime.ConnectionHandler::OnApplicationQuit()
extern void ConnectionHandler_OnApplicationQuit_m3417E7F60D002C50325C2586F18DD3AF7E9550E6 ();
// 0x0000000D System.Void Photon.Realtime.ConnectionHandler::Awake()
extern void ConnectionHandler_Awake_m83D273997434940D3BB1A5431B7FFB4C8C1C5DAB ();
// 0x0000000E System.Void Photon.Realtime.ConnectionHandler::OnDisable()
extern void ConnectionHandler_OnDisable_mDFD9EF4418130730F105A69F1554FD1DA0842F31 ();
// 0x0000000F System.Void Photon.Realtime.ConnectionHandler::StartFallbackSendAckThread()
extern void ConnectionHandler_StartFallbackSendAckThread_mC683B581861BC397A4F8B6D5DD497C3BBA20B7A7 ();
// 0x00000010 System.Void Photon.Realtime.ConnectionHandler::StopFallbackSendAckThread()
extern void ConnectionHandler_StopFallbackSendAckThread_m735B91669EDB32A4F14196C741629CAF97E8D13D ();
// 0x00000011 System.Boolean Photon.Realtime.ConnectionHandler::RealtimeFallbackThread()
extern void ConnectionHandler_RealtimeFallbackThread_m3D6526BB05E1494789907593A678F679B159C273 ();
// 0x00000012 System.Void Photon.Realtime.ConnectionHandler::.ctor()
extern void ConnectionHandler__ctor_mB639F2C7F1DCE9C49A17C1CBEBABAC03ABB218E2 ();
// 0x00000013 System.Void Photon.Realtime.Extensions::Merge(System.Collections.IDictionary,System.Collections.IDictionary)
extern void Extensions_Merge_mE5A42AF13B84826877703729DB671E74D15D5AC9 ();
// 0x00000014 System.Void Photon.Realtime.Extensions::MergeStringKeys(System.Collections.IDictionary,System.Collections.IDictionary)
extern void Extensions_MergeStringKeys_mDCDF6621E96473D5021BFCEC97B22804F387951A ();
// 0x00000015 System.String Photon.Realtime.Extensions::ToStringFull(System.Collections.IDictionary)
extern void Extensions_ToStringFull_m7D5DC29B4341359CC4354664509068EF620E9DCC ();
// 0x00000016 System.String Photon.Realtime.Extensions::ToStringFull(System.Collections.Generic.List`1<T>)
// 0x00000017 System.String Photon.Realtime.Extensions::ToStringFull(System.Object[])
extern void Extensions_ToStringFull_m2121FA96DD4725901A192FA61DC03AAF6766D239 ();
// 0x00000018 ExitGames.Client.Photon.Hashtable Photon.Realtime.Extensions::StripToStringKeys(System.Collections.IDictionary)
extern void Extensions_StripToStringKeys_m3E7F93E0E13C29A850947BBA3D195FC43FF3C562 ();
// 0x00000019 System.Void Photon.Realtime.Extensions::StripKeysWithNullValues(System.Collections.IDictionary)
extern void Extensions_StripKeysWithNullValues_m7708059EAF935DD384169C58E072392DB34E0055 ();
// 0x0000001A System.Boolean Photon.Realtime.Extensions::Contains(System.Int32[],System.Int32)
extern void Extensions_Contains_m93F4A6BA815601CE40B03D37FCA0F51C9BA1EC71 ();
// 0x0000001B System.String Photon.Realtime.FriendInfo::get_Name()
extern void FriendInfo_get_Name_m7E6D9124C3CDE8D6045D3FA8B4AB2B4BE03D08AE ();
// 0x0000001C System.String Photon.Realtime.FriendInfo::get_UserId()
extern void FriendInfo_get_UserId_m475DCCA64B31738682706B4B7D02A520394E951C ();
// 0x0000001D System.Void Photon.Realtime.FriendInfo::set_UserId(System.String)
extern void FriendInfo_set_UserId_m635303D0D3B6F265DC5EA4BC48DC6F7F7CAEAC4B ();
// 0x0000001E System.Boolean Photon.Realtime.FriendInfo::get_IsOnline()
extern void FriendInfo_get_IsOnline_mB5135E0CCCE94C97B89DC682844A93E04E7AC461 ();
// 0x0000001F System.Void Photon.Realtime.FriendInfo::set_IsOnline(System.Boolean)
extern void FriendInfo_set_IsOnline_mF82EFFC6D63C22396272B734F74176E4B46D3ABE ();
// 0x00000020 System.String Photon.Realtime.FriendInfo::get_Room()
extern void FriendInfo_get_Room_m67D7169E01E1F1C9C31EBCCB8BC706090BAB3119 ();
// 0x00000021 System.Void Photon.Realtime.FriendInfo::set_Room(System.String)
extern void FriendInfo_set_Room_m77970C7788BA7A903FA0E4C83CD2C53CC7CBF251 ();
// 0x00000022 System.Boolean Photon.Realtime.FriendInfo::get_IsInRoom()
extern void FriendInfo_get_IsInRoom_mF994126A3C7188F446816E4D1079A1327DE0F9D8 ();
// 0x00000023 System.String Photon.Realtime.FriendInfo::ToString()
extern void FriendInfo_ToString_mEADB7533405EBFBF96B8FD2BD79A9E421170A61C ();
// 0x00000024 System.Void Photon.Realtime.FriendInfo::.ctor()
extern void FriendInfo__ctor_m3E7BB94A24F402D1A41247C30F470CE6F6AEB4B0 ();
// 0x00000025 Photon.Realtime.LoadBalancingPeer Photon.Realtime.LoadBalancingClient::get_LoadBalancingPeer()
extern void LoadBalancingClient_get_LoadBalancingPeer_m0997FEE69F076CDD9740F8A3B34038733175A243 ();
// 0x00000026 System.Void Photon.Realtime.LoadBalancingClient::set_LoadBalancingPeer(Photon.Realtime.LoadBalancingPeer)
extern void LoadBalancingClient_set_LoadBalancingPeer_m8B21BEA9184F84DCDE7BBB9FBB60134B29B29C3D ();
// 0x00000027 System.String Photon.Realtime.LoadBalancingClient::get_AppVersion()
extern void LoadBalancingClient_get_AppVersion_m2D816CED5CD4B71B47C03B33D2C5DFAE08997B9D ();
// 0x00000028 System.Void Photon.Realtime.LoadBalancingClient::set_AppVersion(System.String)
extern void LoadBalancingClient_set_AppVersion_mB366065ACFDA6142E92B1585194423E398A9260E ();
// 0x00000029 System.String Photon.Realtime.LoadBalancingClient::get_AppId()
extern void LoadBalancingClient_get_AppId_m49A1261CC79356CABF9A709AAC4674B406ECA1DD ();
// 0x0000002A System.Void Photon.Realtime.LoadBalancingClient::set_AppId(System.String)
extern void LoadBalancingClient_set_AppId_m526B1D687E3EE08B889F2CBD2F84853C6DA86F27 ();
// 0x0000002B Photon.Realtime.AuthenticationValues Photon.Realtime.LoadBalancingClient::get_AuthValues()
extern void LoadBalancingClient_get_AuthValues_mE56B5ADE8CC0EBB1CB9C5DFA64913AC5B36B7F02 ();
// 0x0000002C System.Void Photon.Realtime.LoadBalancingClient::set_AuthValues(Photon.Realtime.AuthenticationValues)
extern void LoadBalancingClient_set_AuthValues_mB58414B316951242AD2045E85DF048B5357A9C28 ();
// 0x0000002D System.String Photon.Realtime.LoadBalancingClient::get_TokenForInit()
extern void LoadBalancingClient_get_TokenForInit_m069DD8AE48586F2D321FC639DFF6C9D9182ADC13 ();
// 0x0000002E System.Boolean Photon.Realtime.LoadBalancingClient::get_IsUsingNameServer()
extern void LoadBalancingClient_get_IsUsingNameServer_m018A230E641F2CEC1B60DE60EB6BE472834F5092 ();
// 0x0000002F System.Void Photon.Realtime.LoadBalancingClient::set_IsUsingNameServer(System.Boolean)
extern void LoadBalancingClient_set_IsUsingNameServer_mDF99F6C889F0139711EB50A21D0055A7E4A0F8B2 ();
// 0x00000030 System.String Photon.Realtime.LoadBalancingClient::get_NameServerAddress()
extern void LoadBalancingClient_get_NameServerAddress_m3427DF83D68B873E55A3496C4B5C77E1324C7D06 ();
// 0x00000031 System.Boolean Photon.Realtime.LoadBalancingClient::get_UseAlternativeUdpPorts()
extern void LoadBalancingClient_get_UseAlternativeUdpPorts_m99F6E198B4B848CDF32991FBED08120DCB87915B ();
// 0x00000032 System.Void Photon.Realtime.LoadBalancingClient::set_UseAlternativeUdpPorts(System.Boolean)
extern void LoadBalancingClient_set_UseAlternativeUdpPorts_mE307DBDAB3A4BC8E2D81F5BF899830132F53C1A3 ();
// 0x00000033 System.String Photon.Realtime.LoadBalancingClient::get_CurrentServerAddress()
extern void LoadBalancingClient_get_CurrentServerAddress_m617D5473B405AA52AA10EBC3FB8CEC4E65252A02 ();
// 0x00000034 System.String Photon.Realtime.LoadBalancingClient::get_MasterServerAddress()
extern void LoadBalancingClient_get_MasterServerAddress_mE550265A7F1B30E6D1464D0139E859483D507F6B ();
// 0x00000035 System.Void Photon.Realtime.LoadBalancingClient::set_MasterServerAddress(System.String)
extern void LoadBalancingClient_set_MasterServerAddress_m988C29C7041A715EDA996AD6B5A6370AD6842424 ();
// 0x00000036 System.String Photon.Realtime.LoadBalancingClient::get_GameServerAddress()
extern void LoadBalancingClient_get_GameServerAddress_m572210F250D42394A3E6E0FAB7E15DBD6EE78E05 ();
// 0x00000037 System.Void Photon.Realtime.LoadBalancingClient::set_GameServerAddress(System.String)
extern void LoadBalancingClient_set_GameServerAddress_mD863BE3068C8A77BD9E85C5F090EB88A73EF9B65 ();
// 0x00000038 Photon.Realtime.ServerConnection Photon.Realtime.LoadBalancingClient::get_Server()
extern void LoadBalancingClient_get_Server_mF0DA3FA871E123896E53C39758BAF853F1A80EC3 ();
// 0x00000039 System.Void Photon.Realtime.LoadBalancingClient::set_Server(Photon.Realtime.ServerConnection)
extern void LoadBalancingClient_set_Server_m6F4DE1ABC82A6FF949AA370EA18D18920597527E ();
// 0x0000003A Photon.Realtime.ClientState Photon.Realtime.LoadBalancingClient::get_State()
extern void LoadBalancingClient_get_State_mDCC22D02A85973C68623C764C96BE9A862C8BD11 ();
// 0x0000003B System.Void Photon.Realtime.LoadBalancingClient::set_State(Photon.Realtime.ClientState)
extern void LoadBalancingClient_set_State_m1D4AA099CA318F90B257BC03CF9F54979E677BCA ();
// 0x0000003C System.Boolean Photon.Realtime.LoadBalancingClient::get_IsConnected()
extern void LoadBalancingClient_get_IsConnected_m90DAE8B064E99BA87DC5CDAC811D38647D468C2C ();
// 0x0000003D System.Boolean Photon.Realtime.LoadBalancingClient::get_IsConnectedAndReady()
extern void LoadBalancingClient_get_IsConnectedAndReady_m14A6F4A0A3D74E499F65B0360C76A33E94AFAC9C ();
// 0x0000003E System.Void Photon.Realtime.LoadBalancingClient::add_StateChanged(System.Action`2<Photon.Realtime.ClientState,Photon.Realtime.ClientState>)
extern void LoadBalancingClient_add_StateChanged_m630663477C5815D2183AC49D0F5B9C1A5860B50B ();
// 0x0000003F System.Void Photon.Realtime.LoadBalancingClient::remove_StateChanged(System.Action`2<Photon.Realtime.ClientState,Photon.Realtime.ClientState>)
extern void LoadBalancingClient_remove_StateChanged_mC9DBCEE79FE46D63C9BDB1FDDD3509040BB0F622 ();
// 0x00000040 System.Void Photon.Realtime.LoadBalancingClient::add_EventReceived(System.Action`1<ExitGames.Client.Photon.EventData>)
extern void LoadBalancingClient_add_EventReceived_m30CC69C5EEA1781F3DD52E0C1F907AF4DD16D338 ();
// 0x00000041 System.Void Photon.Realtime.LoadBalancingClient::remove_EventReceived(System.Action`1<ExitGames.Client.Photon.EventData>)
extern void LoadBalancingClient_remove_EventReceived_mB11D1D0A6B98AF8E47C0B12C2256F35455E9AD84 ();
// 0x00000042 System.Void Photon.Realtime.LoadBalancingClient::add_OpResponseReceived(System.Action`1<ExitGames.Client.Photon.OperationResponse>)
extern void LoadBalancingClient_add_OpResponseReceived_mB846EA81D44DBB08FBA43398AAECCC32A40540C5 ();
// 0x00000043 System.Void Photon.Realtime.LoadBalancingClient::remove_OpResponseReceived(System.Action`1<ExitGames.Client.Photon.OperationResponse>)
extern void LoadBalancingClient_remove_OpResponseReceived_m9DC9E23D3DC953AEDDFD9EEE78DF2ED102E96E35 ();
// 0x00000044 Photon.Realtime.DisconnectCause Photon.Realtime.LoadBalancingClient::get_DisconnectedCause()
extern void LoadBalancingClient_get_DisconnectedCause_mEC001DE2A4D9E6BC4A364D7F3039EDF72DF395B2 ();
// 0x00000045 System.Void Photon.Realtime.LoadBalancingClient::set_DisconnectedCause(Photon.Realtime.DisconnectCause)
extern void LoadBalancingClient_set_DisconnectedCause_mA384559B6EA4E119910272DF3BBBC1326C47C3E6 ();
// 0x00000046 System.Boolean Photon.Realtime.LoadBalancingClient::get_InLobby()
extern void LoadBalancingClient_get_InLobby_mEF3C513D70D233C19ED61DAF2D4450DA5184A7B9 ();
// 0x00000047 Photon.Realtime.TypedLobby Photon.Realtime.LoadBalancingClient::get_CurrentLobby()
extern void LoadBalancingClient_get_CurrentLobby_mA012C41E1DF7DF53F0CF4A024C9E0FABDDDC2456 ();
// 0x00000048 System.Void Photon.Realtime.LoadBalancingClient::set_CurrentLobby(Photon.Realtime.TypedLobby)
extern void LoadBalancingClient_set_CurrentLobby_m77CC4772915A138EE66B874EBB0A9F707C4A8FF8 ();
// 0x00000049 Photon.Realtime.Player Photon.Realtime.LoadBalancingClient::get_LocalPlayer()
extern void LoadBalancingClient_get_LocalPlayer_mAB1446D19EBDAE3D7D47E92F560AE939B3F21CD8 ();
// 0x0000004A System.Void Photon.Realtime.LoadBalancingClient::set_LocalPlayer(Photon.Realtime.Player)
extern void LoadBalancingClient_set_LocalPlayer_m3AA86EC255BF530EC13FA13CB3BE4D8475D60137 ();
// 0x0000004B System.String Photon.Realtime.LoadBalancingClient::get_NickName()
extern void LoadBalancingClient_get_NickName_m8075389432D8241A5681B82A8C63222B894E060F ();
// 0x0000004C System.Void Photon.Realtime.LoadBalancingClient::set_NickName(System.String)
extern void LoadBalancingClient_set_NickName_m5DBB4C8C18626B5CC593EC56A564C55B51A3306E ();
// 0x0000004D System.String Photon.Realtime.LoadBalancingClient::get_UserId()
extern void LoadBalancingClient_get_UserId_mE44D3E9907F0B72F57E8D6CBF3A0E80B07064AA8 ();
// 0x0000004E System.Void Photon.Realtime.LoadBalancingClient::set_UserId(System.String)
extern void LoadBalancingClient_set_UserId_m8CE468179FE3969883147E165AD466021B0BABB6 ();
// 0x0000004F Photon.Realtime.Room Photon.Realtime.LoadBalancingClient::get_CurrentRoom()
extern void LoadBalancingClient_get_CurrentRoom_mA294529AE11CDDF17597FADE478A392B8B534002 ();
// 0x00000050 System.Void Photon.Realtime.LoadBalancingClient::set_CurrentRoom(Photon.Realtime.Room)
extern void LoadBalancingClient_set_CurrentRoom_mE849A43CF3269E76CE909EE4DE23265175EEB596 ();
// 0x00000051 System.Boolean Photon.Realtime.LoadBalancingClient::get_InRoom()
extern void LoadBalancingClient_get_InRoom_m7DAE0F52D1CF9FF0B2373BE9559788C1752E03B3 ();
// 0x00000052 System.Int32 Photon.Realtime.LoadBalancingClient::get_PlayersOnMasterCount()
extern void LoadBalancingClient_get_PlayersOnMasterCount_m8DBA036419F15FE61A7FCDFE8DE1906FE95DB67F ();
// 0x00000053 System.Void Photon.Realtime.LoadBalancingClient::set_PlayersOnMasterCount(System.Int32)
extern void LoadBalancingClient_set_PlayersOnMasterCount_mBC03E29C60BE48118AD42E71EF2B123029B53E32 ();
// 0x00000054 System.Int32 Photon.Realtime.LoadBalancingClient::get_PlayersInRoomsCount()
extern void LoadBalancingClient_get_PlayersInRoomsCount_m064FF861BE6A381EC4D87C4F342F62AA6ACA3C21 ();
// 0x00000055 System.Void Photon.Realtime.LoadBalancingClient::set_PlayersInRoomsCount(System.Int32)
extern void LoadBalancingClient_set_PlayersInRoomsCount_m99D3F0A6CDA8A3475CBFE8EB3549E2192E970447 ();
// 0x00000056 System.Int32 Photon.Realtime.LoadBalancingClient::get_RoomsCount()
extern void LoadBalancingClient_get_RoomsCount_mDE93A42C477C145F9643BF64FA678B7546819063 ();
// 0x00000057 System.Void Photon.Realtime.LoadBalancingClient::set_RoomsCount(System.Int32)
extern void LoadBalancingClient_set_RoomsCount_m3F6B5B1193EB8419109C85CDCF5220B377932AEA ();
// 0x00000058 System.Boolean Photon.Realtime.LoadBalancingClient::get_IsFetchingFriendList()
extern void LoadBalancingClient_get_IsFetchingFriendList_m173C51FE398A7F356F7875DCA876B059960BFCD8 ();
// 0x00000059 System.String Photon.Realtime.LoadBalancingClient::get_CloudRegion()
extern void LoadBalancingClient_get_CloudRegion_m0AAC37B5520321CC88C90C625E56EBEC822C3DEB ();
// 0x0000005A System.Void Photon.Realtime.LoadBalancingClient::set_CloudRegion(System.String)
extern void LoadBalancingClient_set_CloudRegion_m6DBD960027DE67168D93A00A75861C9552ED9393 ();
// 0x0000005B System.String Photon.Realtime.LoadBalancingClient::get_CurrentCluster()
extern void LoadBalancingClient_get_CurrentCluster_mEDA76121FC84D677906B31A2EF74EB72292C3B82 ();
// 0x0000005C System.Void Photon.Realtime.LoadBalancingClient::set_CurrentCluster(System.String)
extern void LoadBalancingClient_set_CurrentCluster_m8AA87CB42014FB4F1305ECF87124B4E2023F6154 ();
// 0x0000005D System.Void Photon.Realtime.LoadBalancingClient::.ctor(ExitGames.Client.Photon.ConnectionProtocol)
extern void LoadBalancingClient__ctor_m3FE1B0111907E5D6B0863EE8DDD2BAE687A2F12A ();
// 0x0000005E System.Void Photon.Realtime.LoadBalancingClient::.ctor(System.String,System.String,System.String,ExitGames.Client.Photon.ConnectionProtocol)
extern void LoadBalancingClient__ctor_m055218307EA53ED610F32877AE7CD413352E6072 ();
// 0x0000005F System.String Photon.Realtime.LoadBalancingClient::GetNameServerAddress()
extern void LoadBalancingClient_GetNameServerAddress_m11E7EB63DB1C7E0DDBE48A49D7DE82140ED634BC ();
// 0x00000060 System.Boolean Photon.Realtime.LoadBalancingClient::Connect()
extern void LoadBalancingClient_Connect_mCF5DFFA5B00A2F5EF372AEC99932E51AE7E6E81C ();
// 0x00000061 System.Boolean Photon.Realtime.LoadBalancingClient::ConnectToNameServer()
extern void LoadBalancingClient_ConnectToNameServer_m2DFF6510956EA30345D6A055272C86D7390BC9FC ();
// 0x00000062 System.Boolean Photon.Realtime.LoadBalancingClient::ConnectToRegionMaster(System.String)
extern void LoadBalancingClient_ConnectToRegionMaster_m10E860D595ED32FBAF8F7809C20CEA18C477DD76 ();
// 0x00000063 System.Boolean Photon.Realtime.LoadBalancingClient::Connect(System.String,Photon.Realtime.ServerConnection)
extern void LoadBalancingClient_Connect_mD355C7839D9FFB63E33F1AC75E22355186E7E8F0 ();
// 0x00000064 System.Boolean Photon.Realtime.LoadBalancingClient::ConnectToGameServer()
extern void LoadBalancingClient_ConnectToGameServer_m38C66511C6AC3F6DDF2548832405ABD9415F651F ();
// 0x00000065 System.Boolean Photon.Realtime.LoadBalancingClient::ReconnectToMaster()
extern void LoadBalancingClient_ReconnectToMaster_m4761338630C7B0FF8917913FC1FDB6AA6E78FCC1 ();
// 0x00000066 System.Boolean Photon.Realtime.LoadBalancingClient::ReconnectAndRejoin()
extern void LoadBalancingClient_ReconnectAndRejoin_m033BA70D4D0605C4CC679DCFEC7CDDDFA18AA563 ();
// 0x00000067 System.Void Photon.Realtime.LoadBalancingClient::Disconnect()
extern void LoadBalancingClient_Disconnect_mC22029D6906E00B19FE967EC5695177F9B262C51 ();
// 0x00000068 System.Void Photon.Realtime.LoadBalancingClient::DisconnectToReconnect()
extern void LoadBalancingClient_DisconnectToReconnect_mD4506B2DB0BAC6E370909D28CA8C5B4630D631FE ();
// 0x00000069 System.Void Photon.Realtime.LoadBalancingClient::SimulateConnectionLoss(System.Boolean)
extern void LoadBalancingClient_SimulateConnectionLoss_m4C381965C26752DD6E9D6EACDBCEFB25D72CD6A1 ();
// 0x0000006A System.Boolean Photon.Realtime.LoadBalancingClient::CallAuthenticate()
extern void LoadBalancingClient_CallAuthenticate_mCA1632629441AA360D28CAF559445F4DC3D0C9E3 ();
// 0x0000006B System.Void Photon.Realtime.LoadBalancingClient::Service()
extern void LoadBalancingClient_Service_m443AD58B507AD1E7601CB7785BD4804ACFD1868A ();
// 0x0000006C System.Boolean Photon.Realtime.LoadBalancingClient::OpGetRegions()
extern void LoadBalancingClient_OpGetRegions_m46A3AFA0AD29E32E2424BC20F78D438333D421FE ();
// 0x0000006D System.Boolean Photon.Realtime.LoadBalancingClient::OpFindFriends(System.String[],Photon.Realtime.FindFriendsOptions)
extern void LoadBalancingClient_OpFindFriends_m7ECF4CAB63B89AA5D1AC0D6A2E6222BD16AB9985 ();
// 0x0000006E System.Boolean Photon.Realtime.LoadBalancingClient::OpJoinLobby(Photon.Realtime.TypedLobby)
extern void LoadBalancingClient_OpJoinLobby_m6FEB6C0808D3963ADC90E55D9ED9B7357AD61DB7 ();
// 0x0000006F System.Boolean Photon.Realtime.LoadBalancingClient::OpLeaveLobby()
extern void LoadBalancingClient_OpLeaveLobby_m079C4D33791C5B254E2EC06A95D29CC6D618F9EA ();
// 0x00000070 System.Boolean Photon.Realtime.LoadBalancingClient::OpJoinRandomRoom(Photon.Realtime.OpJoinRandomRoomParams)
extern void LoadBalancingClient_OpJoinRandomRoom_m538FE487F277FF1F054E8A0309F751DA76704376 ();
// 0x00000071 System.Boolean Photon.Realtime.LoadBalancingClient::OpJoinRandomOrCreateRoom(Photon.Realtime.OpJoinRandomRoomParams,Photon.Realtime.EnterRoomParams)
extern void LoadBalancingClient_OpJoinRandomOrCreateRoom_m0594DC879B570D9C75EBA2B87181D50D804D2057 ();
// 0x00000072 System.Boolean Photon.Realtime.LoadBalancingClient::OpCreateRoom(Photon.Realtime.EnterRoomParams)
extern void LoadBalancingClient_OpCreateRoom_m5B1E30682BEFB186628ABACBBDA004259AA37CD8 ();
// 0x00000073 System.Boolean Photon.Realtime.LoadBalancingClient::OpJoinOrCreateRoom(Photon.Realtime.EnterRoomParams)
extern void LoadBalancingClient_OpJoinOrCreateRoom_m7619F10D86CF4AC1F62C2B23CD5CFF56876753B6 ();
// 0x00000074 System.Boolean Photon.Realtime.LoadBalancingClient::OpJoinRoom(Photon.Realtime.EnterRoomParams)
extern void LoadBalancingClient_OpJoinRoom_m3BD6B9BB0D6A67A9A0FA7D3F0B56D9A2580B26A8 ();
// 0x00000075 System.Boolean Photon.Realtime.LoadBalancingClient::OpRejoinRoom(System.String)
extern void LoadBalancingClient_OpRejoinRoom_m918AFF2E3CEDDFFBC6DE787F799C7A88D797CBC8 ();
// 0x00000076 System.Boolean Photon.Realtime.LoadBalancingClient::OpLeaveRoom(System.Boolean,System.Boolean)
extern void LoadBalancingClient_OpLeaveRoom_m210103CE0E2DA347DF6C962173DE4D59FE2C9C8B ();
// 0x00000077 System.Boolean Photon.Realtime.LoadBalancingClient::OpGetGameList(Photon.Realtime.TypedLobby,System.String)
extern void LoadBalancingClient_OpGetGameList_mB230101A905B4322C01D32D1B148787EF6A6F48A ();
// 0x00000078 System.Boolean Photon.Realtime.LoadBalancingClient::OpSetCustomPropertiesOfActor(System.Int32,ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,Photon.Realtime.WebFlags)
extern void LoadBalancingClient_OpSetCustomPropertiesOfActor_mD0652B67E064E24A89BC240C8C274EA72714110B ();
// 0x00000079 System.Boolean Photon.Realtime.LoadBalancingClient::OpSetPropertiesOfActor(System.Int32,ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,Photon.Realtime.WebFlags)
extern void LoadBalancingClient_OpSetPropertiesOfActor_mB1D67FAA256836C1F5ED1286B0A74565BADD324E ();
// 0x0000007A System.Boolean Photon.Realtime.LoadBalancingClient::OpSetCustomPropertiesOfRoom(ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,Photon.Realtime.WebFlags)
extern void LoadBalancingClient_OpSetCustomPropertiesOfRoom_m868620D8B9154EFCE198CF5A670EAB7567598E0C ();
// 0x0000007B System.Void Photon.Realtime.LoadBalancingClient::OpSetPropertyOfRoom(System.Byte,System.Object)
extern void LoadBalancingClient_OpSetPropertyOfRoom_mA606EAD4CDB750B342CF07F0664F06506AF1E069 ();
// 0x0000007C System.Boolean Photon.Realtime.LoadBalancingClient::OpSetPropertiesOfRoom(ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,Photon.Realtime.WebFlags)
extern void LoadBalancingClient_OpSetPropertiesOfRoom_mE86C47F8A6D529E5B50FC9FA78232ACAE25EFEF5 ();
// 0x0000007D System.Boolean Photon.Realtime.LoadBalancingClient::OpRaiseEvent(System.Byte,System.Object,Photon.Realtime.RaiseEventOptions,ExitGames.Client.Photon.SendOptions)
extern void LoadBalancingClient_OpRaiseEvent_m720894D48B5C0E9DAE00770B19521A9F1586C106 ();
// 0x0000007E System.Boolean Photon.Realtime.LoadBalancingClient::OpChangeGroups(System.Byte[],System.Byte[])
extern void LoadBalancingClient_OpChangeGroups_mBC257BAFF6ADFA776D8B357477332E22AD0D65CB ();
// 0x0000007F System.Void Photon.Realtime.LoadBalancingClient::ReadoutProperties(ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,System.Int32)
extern void LoadBalancingClient_ReadoutProperties_mD222637E6484F8FE82195F4DC66D07981C033C33 ();
// 0x00000080 ExitGames.Client.Photon.Hashtable Photon.Realtime.LoadBalancingClient::ReadoutPropertiesForActorNr(ExitGames.Client.Photon.Hashtable,System.Int32)
extern void LoadBalancingClient_ReadoutPropertiesForActorNr_mAC576B0BBA3348B7B7675C7CFA6E96EDF2A70006 ();
// 0x00000081 System.Void Photon.Realtime.LoadBalancingClient::ChangeLocalID(System.Int32)
extern void LoadBalancingClient_ChangeLocalID_mAC51D66CAD91AD689FC634E450ED0CE8E3429C41 ();
// 0x00000082 System.Void Photon.Realtime.LoadBalancingClient::GameEnteredOnGameServer(ExitGames.Client.Photon.OperationResponse)
extern void LoadBalancingClient_GameEnteredOnGameServer_m1908BF738A090B0DD252326998F5A0E7E86300C2 ();
// 0x00000083 System.Void Photon.Realtime.LoadBalancingClient::UpdatedActorList(System.Int32[])
extern void LoadBalancingClient_UpdatedActorList_mA355582E01CBAE38175910FC738C4B9EA5C80D5D ();
// 0x00000084 Photon.Realtime.Player Photon.Realtime.LoadBalancingClient::CreatePlayer(System.String,System.Int32,System.Boolean,ExitGames.Client.Photon.Hashtable)
extern void LoadBalancingClient_CreatePlayer_m4C371647216BB95B08545C0B940913B9C2D6A3BB ();
// 0x00000085 Photon.Realtime.Room Photon.Realtime.LoadBalancingClient::CreateRoom(System.String,Photon.Realtime.RoomOptions)
extern void LoadBalancingClient_CreateRoom_m2A44592E172B369A6540C607BE5594E042366393 ();
// 0x00000086 System.Boolean Photon.Realtime.LoadBalancingClient::CheckIfOpAllowedOnServer(System.Byte,Photon.Realtime.ServerConnection)
extern void LoadBalancingClient_CheckIfOpAllowedOnServer_mD2284A0C2FFEA7824E4E08FE62A3C33A1620DBC2 ();
// 0x00000087 System.Boolean Photon.Realtime.LoadBalancingClient::CheckIfOpCanBeSent(System.Byte,Photon.Realtime.ServerConnection,System.String)
extern void LoadBalancingClient_CheckIfOpCanBeSent_m1E0ACAE5106D3E8D10B03F96E857E3E98244D64D ();
// 0x00000088 System.Boolean Photon.Realtime.LoadBalancingClient::CheckIfClientIsReadyToCallOperation(System.Byte)
extern void LoadBalancingClient_CheckIfClientIsReadyToCallOperation_m618BC7E29479E88C6ED9F6809D770E4A941D7099 ();
// 0x00000089 System.Void Photon.Realtime.LoadBalancingClient::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern void LoadBalancingClient_DebugReturn_m0E1FF29F13D3853B8A74D0427B660019FD10AB06 ();
// 0x0000008A System.Void Photon.Realtime.LoadBalancingClient::CallbackRoomEnterFailed(ExitGames.Client.Photon.OperationResponse)
extern void LoadBalancingClient_CallbackRoomEnterFailed_mDF2C50FFDD9B65C9C2C3E77C88099D606093A8BD ();
// 0x0000008B System.Void Photon.Realtime.LoadBalancingClient::OnOperationResponse(ExitGames.Client.Photon.OperationResponse)
extern void LoadBalancingClient_OnOperationResponse_mC849C966853D4423FAC2DE520EED481F2B35D7B0 ();
// 0x0000008C System.Void Photon.Realtime.LoadBalancingClient::OnStatusChanged(ExitGames.Client.Photon.StatusCode)
extern void LoadBalancingClient_OnStatusChanged_m1698C1AB10A7A509331280885909345CDA36D91E ();
// 0x0000008D System.Void Photon.Realtime.LoadBalancingClient::OnEvent(ExitGames.Client.Photon.EventData)
extern void LoadBalancingClient_OnEvent_mE53CF2903579547528ED4FD8311A50D6D47AE02A ();
// 0x0000008E System.Void Photon.Realtime.LoadBalancingClient::OnMessage(System.Object)
extern void LoadBalancingClient_OnMessage_m6D60BAA1FE0540E3BF0D7D0C73790B8637AF89B1 ();
// 0x0000008F System.Void Photon.Realtime.LoadBalancingClient::SetupEncryption(System.Collections.Generic.Dictionary`2<System.Byte,System.Object>)
extern void LoadBalancingClient_SetupEncryption_m9BBD4278BAC5CC5FEC3679A49439405600DE3E76 ();
// 0x00000090 System.Boolean Photon.Realtime.LoadBalancingClient::OpWebRpc(System.String,System.Object,System.Boolean)
extern void LoadBalancingClient_OpWebRpc_m767BB659C353F0C1FC540301D1F3EBB300236542 ();
// 0x00000091 System.Void Photon.Realtime.LoadBalancingClient::AddCallbackTarget(System.Object)
extern void LoadBalancingClient_AddCallbackTarget_m17553A4C53D4BC96DF74BD3DFE8D8487E3B52565 ();
// 0x00000092 System.Void Photon.Realtime.LoadBalancingClient::RemoveCallbackTarget(System.Object)
extern void LoadBalancingClient_RemoveCallbackTarget_mA166E163D02518ED621B80E8F07B31FFE79507A0 ();
// 0x00000093 System.Void Photon.Realtime.LoadBalancingClient::UpdateCallbackTargets()
extern void LoadBalancingClient_UpdateCallbackTargets_m4F0C56B00E0DC74B26294FA61A83186A710DBFFA ();
// 0x00000094 System.Void Photon.Realtime.LoadBalancingClient::UpdateCallbackTarget(Photon.Realtime.LoadBalancingClient_CallbackTargetChange,System.Collections.Generic.List`1<T>)
// 0x00000095 System.Void Photon.Realtime.LoadBalancingClient::.cctor()
extern void LoadBalancingClient__cctor_m852A3AC2587002191CFA49935C4C2769DCEC7DCD ();
// 0x00000096 System.Void Photon.Realtime.IConnectionCallbacks::OnConnected()
// 0x00000097 System.Void Photon.Realtime.IConnectionCallbacks::OnConnectedToMaster()
// 0x00000098 System.Void Photon.Realtime.IConnectionCallbacks::OnDisconnected(Photon.Realtime.DisconnectCause)
// 0x00000099 System.Void Photon.Realtime.IConnectionCallbacks::OnRegionListReceived(Photon.Realtime.RegionHandler)
// 0x0000009A System.Void Photon.Realtime.IConnectionCallbacks::OnCustomAuthenticationResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x0000009B System.Void Photon.Realtime.IConnectionCallbacks::OnCustomAuthenticationFailed(System.String)
// 0x0000009C System.Void Photon.Realtime.ILobbyCallbacks::OnJoinedLobby()
// 0x0000009D System.Void Photon.Realtime.ILobbyCallbacks::OnLeftLobby()
// 0x0000009E System.Void Photon.Realtime.ILobbyCallbacks::OnRoomListUpdate(System.Collections.Generic.List`1<Photon.Realtime.RoomInfo>)
// 0x0000009F System.Void Photon.Realtime.ILobbyCallbacks::OnLobbyStatisticsUpdate(System.Collections.Generic.List`1<Photon.Realtime.TypedLobbyInfo>)
// 0x000000A0 System.Void Photon.Realtime.IMatchmakingCallbacks::OnFriendListUpdate(System.Collections.Generic.List`1<Photon.Realtime.FriendInfo>)
// 0x000000A1 System.Void Photon.Realtime.IMatchmakingCallbacks::OnCreatedRoom()
// 0x000000A2 System.Void Photon.Realtime.IMatchmakingCallbacks::OnCreateRoomFailed(System.Int16,System.String)
// 0x000000A3 System.Void Photon.Realtime.IMatchmakingCallbacks::OnJoinedRoom()
// 0x000000A4 System.Void Photon.Realtime.IMatchmakingCallbacks::OnJoinRoomFailed(System.Int16,System.String)
// 0x000000A5 System.Void Photon.Realtime.IMatchmakingCallbacks::OnJoinRandomFailed(System.Int16,System.String)
// 0x000000A6 System.Void Photon.Realtime.IMatchmakingCallbacks::OnLeftRoom()
// 0x000000A7 System.Void Photon.Realtime.IInRoomCallbacks::OnPlayerEnteredRoom(Photon.Realtime.Player)
// 0x000000A8 System.Void Photon.Realtime.IInRoomCallbacks::OnPlayerLeftRoom(Photon.Realtime.Player)
// 0x000000A9 System.Void Photon.Realtime.IInRoomCallbacks::OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable)
// 0x000000AA System.Void Photon.Realtime.IInRoomCallbacks::OnPlayerPropertiesUpdate(Photon.Realtime.Player,ExitGames.Client.Photon.Hashtable)
// 0x000000AB System.Void Photon.Realtime.IInRoomCallbacks::OnMasterClientSwitched(Photon.Realtime.Player)
// 0x000000AC System.Void Photon.Realtime.IOnEventCallback::OnEvent(ExitGames.Client.Photon.EventData)
// 0x000000AD System.Void Photon.Realtime.IWebRpcCallback::OnWebRpcResponse(ExitGames.Client.Photon.OperationResponse)
// 0x000000AE System.Void Photon.Realtime.ConnectionCallbacksContainer::.ctor(Photon.Realtime.LoadBalancingClient)
extern void ConnectionCallbacksContainer__ctor_m86519082BDB51074C0A08444C9D058F9DFECC174 ();
// 0x000000AF System.Void Photon.Realtime.ConnectionCallbacksContainer::OnConnected()
extern void ConnectionCallbacksContainer_OnConnected_m86DCC1AFF556E53DAC6104AAF966FD2283AF8B54 ();
// 0x000000B0 System.Void Photon.Realtime.ConnectionCallbacksContainer::OnConnectedToMaster()
extern void ConnectionCallbacksContainer_OnConnectedToMaster_mCA9F1A159894B869AD631CE53B2EFB2F1082C513 ();
// 0x000000B1 System.Void Photon.Realtime.ConnectionCallbacksContainer::OnRegionListReceived(Photon.Realtime.RegionHandler)
extern void ConnectionCallbacksContainer_OnRegionListReceived_mA1598011E293574C634F3DD5B8F6D9E9036E8FC9 ();
// 0x000000B2 System.Void Photon.Realtime.ConnectionCallbacksContainer::OnDisconnected(Photon.Realtime.DisconnectCause)
extern void ConnectionCallbacksContainer_OnDisconnected_m4A6BE9270567815E0A07624DD92D8C56C3AF74CC ();
// 0x000000B3 System.Void Photon.Realtime.ConnectionCallbacksContainer::OnCustomAuthenticationResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void ConnectionCallbacksContainer_OnCustomAuthenticationResponse_mFB070A71F70D10E9F785B97C1109517499342FEA ();
// 0x000000B4 System.Void Photon.Realtime.ConnectionCallbacksContainer::OnCustomAuthenticationFailed(System.String)
extern void ConnectionCallbacksContainer_OnCustomAuthenticationFailed_m6F71D63E9DF50E376B12BE5DDE4856BB80D27028 ();
// 0x000000B5 System.Void Photon.Realtime.MatchMakingCallbacksContainer::.ctor(Photon.Realtime.LoadBalancingClient)
extern void MatchMakingCallbacksContainer__ctor_m2DC6BFC48067B04691D90703F1B598AAB1C39D0D ();
// 0x000000B6 System.Void Photon.Realtime.MatchMakingCallbacksContainer::OnCreatedRoom()
extern void MatchMakingCallbacksContainer_OnCreatedRoom_mBFE7022F4C9E6E03FD47DA8272D33A819CC8F6D5 ();
// 0x000000B7 System.Void Photon.Realtime.MatchMakingCallbacksContainer::OnJoinedRoom()
extern void MatchMakingCallbacksContainer_OnJoinedRoom_m5779B6F7EBBF9C65B8E59BC8C1799B30EAF9D348 ();
// 0x000000B8 System.Void Photon.Realtime.MatchMakingCallbacksContainer::OnCreateRoomFailed(System.Int16,System.String)
extern void MatchMakingCallbacksContainer_OnCreateRoomFailed_mA64F4F5D2E1EE8AF1C2AE80ED1D81121CBF8092C ();
// 0x000000B9 System.Void Photon.Realtime.MatchMakingCallbacksContainer::OnJoinRandomFailed(System.Int16,System.String)
extern void MatchMakingCallbacksContainer_OnJoinRandomFailed_m1D7DC1D9BDE49CDB34B20CEDFF105A932030C87A ();
// 0x000000BA System.Void Photon.Realtime.MatchMakingCallbacksContainer::OnJoinRoomFailed(System.Int16,System.String)
extern void MatchMakingCallbacksContainer_OnJoinRoomFailed_mEAC9F4C37B2F04F228DC585FE90DF4C44464A9CD ();
// 0x000000BB System.Void Photon.Realtime.MatchMakingCallbacksContainer::OnLeftRoom()
extern void MatchMakingCallbacksContainer_OnLeftRoom_m463DC02884ABEA72E6329D0FCF8F9F29DF8138C2 ();
// 0x000000BC System.Void Photon.Realtime.MatchMakingCallbacksContainer::OnFriendListUpdate(System.Collections.Generic.List`1<Photon.Realtime.FriendInfo>)
extern void MatchMakingCallbacksContainer_OnFriendListUpdate_m0B78E17F0EB2E1038F8C6351D6D895DF510E413F ();
// 0x000000BD System.Void Photon.Realtime.InRoomCallbacksContainer::.ctor(Photon.Realtime.LoadBalancingClient)
extern void InRoomCallbacksContainer__ctor_mAB5D415C40517C66AFA4EE51FDE27BABB34CB1BC ();
// 0x000000BE System.Void Photon.Realtime.InRoomCallbacksContainer::OnPlayerEnteredRoom(Photon.Realtime.Player)
extern void InRoomCallbacksContainer_OnPlayerEnteredRoom_m390990124F7343BFEE2082780A788AD7F2F6FA3C ();
// 0x000000BF System.Void Photon.Realtime.InRoomCallbacksContainer::OnPlayerLeftRoom(Photon.Realtime.Player)
extern void InRoomCallbacksContainer_OnPlayerLeftRoom_m118CCAC5C3B5BB1271F2ACAFBC61461F49772123 ();
// 0x000000C0 System.Void Photon.Realtime.InRoomCallbacksContainer::OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable)
extern void InRoomCallbacksContainer_OnRoomPropertiesUpdate_m2EC10EA23B6766DAE53B58C18D22E28281A22725 ();
// 0x000000C1 System.Void Photon.Realtime.InRoomCallbacksContainer::OnPlayerPropertiesUpdate(Photon.Realtime.Player,ExitGames.Client.Photon.Hashtable)
extern void InRoomCallbacksContainer_OnPlayerPropertiesUpdate_mBF368118DFF6C61AC66CDE9AA250C4390922C2B8 ();
// 0x000000C2 System.Void Photon.Realtime.InRoomCallbacksContainer::OnMasterClientSwitched(Photon.Realtime.Player)
extern void InRoomCallbacksContainer_OnMasterClientSwitched_mDE73A982F91D03B67103CD7083734A37ED3CB07D ();
// 0x000000C3 System.Void Photon.Realtime.LobbyCallbacksContainer::.ctor(Photon.Realtime.LoadBalancingClient)
extern void LobbyCallbacksContainer__ctor_m5EA40FFCF7EE793CD70AFCDF27EC96F8B3A546E3 ();
// 0x000000C4 System.Void Photon.Realtime.LobbyCallbacksContainer::OnJoinedLobby()
extern void LobbyCallbacksContainer_OnJoinedLobby_mE527C59A2BD5CBD6761A07F1B70BECC1EEA7EBF3 ();
// 0x000000C5 System.Void Photon.Realtime.LobbyCallbacksContainer::OnLeftLobby()
extern void LobbyCallbacksContainer_OnLeftLobby_m89700E7E4FE1528CCA913969F5D122EB1481DD6B ();
// 0x000000C6 System.Void Photon.Realtime.LobbyCallbacksContainer::OnRoomListUpdate(System.Collections.Generic.List`1<Photon.Realtime.RoomInfo>)
extern void LobbyCallbacksContainer_OnRoomListUpdate_m4CB513C7248881B9FC9A7D020608B2FE1B9736F3 ();
// 0x000000C7 System.Void Photon.Realtime.LobbyCallbacksContainer::OnLobbyStatisticsUpdate(System.Collections.Generic.List`1<Photon.Realtime.TypedLobbyInfo>)
extern void LobbyCallbacksContainer_OnLobbyStatisticsUpdate_m3DB8D26C9441DBD8D9DA605F568E6821D0B3A053 ();
// 0x000000C8 System.Void Photon.Realtime.WebRpcCallbacksContainer::.ctor(Photon.Realtime.LoadBalancingClient)
extern void WebRpcCallbacksContainer__ctor_mC0E65E12EA20C9F14D401D8F08FDB85E806176AF ();
// 0x000000C9 System.Void Photon.Realtime.WebRpcCallbacksContainer::OnWebRpcResponse(ExitGames.Client.Photon.OperationResponse)
extern void WebRpcCallbacksContainer_OnWebRpcResponse_m358DF5A7DECB9AAF706A79C4B01ED99E9266EF6B ();
// 0x000000CA System.Void Photon.Realtime.LoadBalancingPeer::.ctor(ExitGames.Client.Photon.ConnectionProtocol)
extern void LoadBalancingPeer__ctor_mEBA1C7F13CEB9C2666A58E9324B104F3A079C421 ();
// 0x000000CB System.Void Photon.Realtime.LoadBalancingPeer::.ctor(ExitGames.Client.Photon.IPhotonPeerListener,ExitGames.Client.Photon.ConnectionProtocol)
extern void LoadBalancingPeer__ctor_m6223CA6A845A305BE735E4D61716A06E33355C6F ();
// 0x000000CC System.Void Photon.Realtime.LoadBalancingPeer::ConfigUnitySockets()
extern void LoadBalancingPeer_ConfigUnitySockets_m408D89E41EE7698B701CB80AB3FB5CA9CEC8BDD2 ();
// 0x000000CD System.Boolean Photon.Realtime.LoadBalancingPeer::OpGetRegions(System.String)
extern void LoadBalancingPeer_OpGetRegions_m9F4665F4D56ED67248CBA86B21DE935C9B389CAD ();
// 0x000000CE System.Boolean Photon.Realtime.LoadBalancingPeer::OpJoinLobby(Photon.Realtime.TypedLobby)
extern void LoadBalancingPeer_OpJoinLobby_m7793464CC70817DCF1C649D900206941E23EA1D6 ();
// 0x000000CF System.Boolean Photon.Realtime.LoadBalancingPeer::OpLeaveLobby()
extern void LoadBalancingPeer_OpLeaveLobby_mA6F5BC8D94B95A77E9C4FE307C6A8A995D59CCA3 ();
// 0x000000D0 System.Void Photon.Realtime.LoadBalancingPeer::RoomOptionsToOpParameters(System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,Photon.Realtime.RoomOptions,System.Boolean)
extern void LoadBalancingPeer_RoomOptionsToOpParameters_m493A0A04572A343401C95AFDB825979A1E493022 ();
// 0x000000D1 System.Boolean Photon.Realtime.LoadBalancingPeer::OpCreateRoom(Photon.Realtime.EnterRoomParams)
extern void LoadBalancingPeer_OpCreateRoom_m2F616AFF2B1236D7CE8D50692972483DD39533D6 ();
// 0x000000D2 System.Boolean Photon.Realtime.LoadBalancingPeer::OpJoinRoom(Photon.Realtime.EnterRoomParams)
extern void LoadBalancingPeer_OpJoinRoom_m824B43FB7C5B63B573C4FD19731373D240A20A2D ();
// 0x000000D3 System.Boolean Photon.Realtime.LoadBalancingPeer::OpJoinRandomRoom(Photon.Realtime.OpJoinRandomRoomParams)
extern void LoadBalancingPeer_OpJoinRandomRoom_mA20F3B8E4FD9AC8336B87DFC59CC4D1444C426E0 ();
// 0x000000D4 System.Boolean Photon.Realtime.LoadBalancingPeer::OpJoinRandomOrCreateRoom(Photon.Realtime.OpJoinRandomRoomParams,Photon.Realtime.EnterRoomParams)
extern void LoadBalancingPeer_OpJoinRandomOrCreateRoom_m8BA07BB581495C5D23E021A7C41559018E6EFD83 ();
// 0x000000D5 System.Boolean Photon.Realtime.LoadBalancingPeer::OpLeaveRoom(System.Boolean,System.Boolean)
extern void LoadBalancingPeer_OpLeaveRoom_m75F3F8B5E8DF51903186AAE2CD98558E10512D90 ();
// 0x000000D6 System.Boolean Photon.Realtime.LoadBalancingPeer::OpGetGameList(Photon.Realtime.TypedLobby,System.String)
extern void LoadBalancingPeer_OpGetGameList_m3F3C7405CA812472E08707F930958C66F2440E3F ();
// 0x000000D7 System.Boolean Photon.Realtime.LoadBalancingPeer::OpFindFriends(System.String[],Photon.Realtime.FindFriendsOptions)
extern void LoadBalancingPeer_OpFindFriends_m27C2BBE02BFDEA67B761195F3739EEAE29252B21 ();
// 0x000000D8 System.Boolean Photon.Realtime.LoadBalancingPeer::OpSetCustomPropertiesOfActor(System.Int32,ExitGames.Client.Photon.Hashtable)
extern void LoadBalancingPeer_OpSetCustomPropertiesOfActor_mDA09CE598C32EEAB561956DDBD6E76D1708C09E2 ();
// 0x000000D9 System.Boolean Photon.Realtime.LoadBalancingPeer::OpSetPropertiesOfActor(System.Int32,ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,Photon.Realtime.WebFlags)
extern void LoadBalancingPeer_OpSetPropertiesOfActor_mBD84D0B5F7D7523D0316E5BF517C9AC5D9EE12AE ();
// 0x000000DA System.Void Photon.Realtime.LoadBalancingPeer::OpSetPropertyOfRoom(System.Byte,System.Object)
extern void LoadBalancingPeer_OpSetPropertyOfRoom_m9ABE7E024BDF6703B4233533866F2EF205E505D2 ();
// 0x000000DB System.Boolean Photon.Realtime.LoadBalancingPeer::OpSetCustomPropertiesOfRoom(ExitGames.Client.Photon.Hashtable)
extern void LoadBalancingPeer_OpSetCustomPropertiesOfRoom_m722E56A045F768E9BAD102B2D3BBB66100CA949B ();
// 0x000000DC System.Boolean Photon.Realtime.LoadBalancingPeer::OpSetPropertiesOfRoom(ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,Photon.Realtime.WebFlags)
extern void LoadBalancingPeer_OpSetPropertiesOfRoom_m184256399D43209989DF89AD5A95DCBA46C99F79 ();
// 0x000000DD System.Boolean Photon.Realtime.LoadBalancingPeer::OpAuthenticate(System.String,System.String,Photon.Realtime.AuthenticationValues,System.String,System.Boolean)
extern void LoadBalancingPeer_OpAuthenticate_mCD39661E2951112370B6A517FED9309059E1FD68 ();
// 0x000000DE System.Boolean Photon.Realtime.LoadBalancingPeer::OpAuthenticateOnce(System.String,System.String,Photon.Realtime.AuthenticationValues,System.String,Photon.Realtime.EncryptionMode,ExitGames.Client.Photon.ConnectionProtocol)
extern void LoadBalancingPeer_OpAuthenticateOnce_m837376F726445B4B72F8B9FBB44CB61DADAB4BAE ();
// 0x000000DF System.Boolean Photon.Realtime.LoadBalancingPeer::OpChangeGroups(System.Byte[],System.Byte[])
extern void LoadBalancingPeer_OpChangeGroups_mA6BC6B7D1F66608D35A38228525EBEEF767F4C95 ();
// 0x000000E0 System.Boolean Photon.Realtime.LoadBalancingPeer::OpRaiseEvent(System.Byte,System.Object,Photon.Realtime.RaiseEventOptions,ExitGames.Client.Photon.SendOptions)
extern void LoadBalancingPeer_OpRaiseEvent_m94C3828C5AE0E3A23C9072BC33BCC75FA8E66DC1 ();
// 0x000000E1 System.Boolean Photon.Realtime.LoadBalancingPeer::OpSettings(System.Boolean)
extern void LoadBalancingPeer_OpSettings_m696CF4ADA61882FB43470A158410CADD7E45DA57 ();
// 0x000000E2 System.Void Photon.Realtime.LoadBalancingPeer::.cctor()
extern void LoadBalancingPeer__cctor_m15FF0270AF5AE76F8D8FB289C441E636F535EC75 ();
// 0x000000E3 System.Int32 Photon.Realtime.FindFriendsOptions::ToIntFlags()
extern void FindFriendsOptions_ToIntFlags_m2F26BC5B39283350C3A2E7489F71E40BF8E7AEBE ();
// 0x000000E4 System.Void Photon.Realtime.FindFriendsOptions::.ctor()
extern void FindFriendsOptions__ctor_mEDA35D63C83F547827AC02637FCFCBF0D1E543F1 ();
// 0x000000E5 System.Void Photon.Realtime.OpJoinRandomRoomParams::.ctor()
extern void OpJoinRandomRoomParams__ctor_m7EAF5F874DE8AFABDFF8DD4BF96CEBD695E26880 ();
// 0x000000E6 System.Void Photon.Realtime.EnterRoomParams::.ctor()
extern void EnterRoomParams__ctor_mD79E1A3DA37FE25323DC2721FB0DFDC00D7F3EDE ();
// 0x000000E7 System.Void Photon.Realtime.ErrorCode::.ctor()
extern void ErrorCode__ctor_m76E0631999B20B536126A83C8F75EBF7BF5C219E ();
// 0x000000E8 System.Void Photon.Realtime.ActorProperties::.ctor()
extern void ActorProperties__ctor_mA0104A194D96AA6BC466E2CD41370572AF43DDF1 ();
// 0x000000E9 System.Void Photon.Realtime.GamePropertyKey::.ctor()
extern void GamePropertyKey__ctor_m0DAB913601AD9E333243D10E454397BB55187EA2 ();
// 0x000000EA System.Void Photon.Realtime.EventCode::.ctor()
extern void EventCode__ctor_m6A1C78CC6506BA88D96DC46861DC0B3C74FCDB1E ();
// 0x000000EB System.Void Photon.Realtime.ParameterCode::.ctor()
extern void ParameterCode__ctor_m7EDF2626AA921ACA7EE1B03D813F8219E15CB0BA ();
// 0x000000EC System.Void Photon.Realtime.OperationCode::.ctor()
extern void OperationCode__ctor_mDE1A1F05CBB3D4DE66EE837F430112E3849A4F26 ();
// 0x000000ED System.Boolean Photon.Realtime.RoomOptions::get_IsVisible()
extern void RoomOptions_get_IsVisible_mEBC48A6E7D7A0422A0CEA3A4AD339EA8B7061976 ();
// 0x000000EE System.Void Photon.Realtime.RoomOptions::set_IsVisible(System.Boolean)
extern void RoomOptions_set_IsVisible_mC18F0091F40004E92A6B0D65DADE6B8ACE4FC4AC ();
// 0x000000EF System.Boolean Photon.Realtime.RoomOptions::get_IsOpen()
extern void RoomOptions_get_IsOpen_m63600C2FCC2227779869060317764624F9536147 ();
// 0x000000F0 System.Void Photon.Realtime.RoomOptions::set_IsOpen(System.Boolean)
extern void RoomOptions_set_IsOpen_mB8FC22B596C9CD2534DB1F5911B352D9FC73F031 ();
// 0x000000F1 System.Boolean Photon.Realtime.RoomOptions::get_CleanupCacheOnLeave()
extern void RoomOptions_get_CleanupCacheOnLeave_m5BC86AE25FB6DCE474527C5FDB7363F1212C120B ();
// 0x000000F2 System.Void Photon.Realtime.RoomOptions::set_CleanupCacheOnLeave(System.Boolean)
extern void RoomOptions_set_CleanupCacheOnLeave_mC9C34F080C4EDFD2FF80D959CAEC057DC619D48B ();
// 0x000000F3 System.Boolean Photon.Realtime.RoomOptions::get_SuppressRoomEvents()
extern void RoomOptions_get_SuppressRoomEvents_mD4FCCD8AFF0DD842C04BA3D385FF0F9A128D1F50 ();
// 0x000000F4 System.Void Photon.Realtime.RoomOptions::set_SuppressRoomEvents(System.Boolean)
extern void RoomOptions_set_SuppressRoomEvents_mDE84DE82AACEB351CC93E17B781149D52E97DB1E ();
// 0x000000F5 System.Boolean Photon.Realtime.RoomOptions::get_PublishUserId()
extern void RoomOptions_get_PublishUserId_mEFF1EA9F5F3310DCED3A7BF9DE99174274DC5766 ();
// 0x000000F6 System.Void Photon.Realtime.RoomOptions::set_PublishUserId(System.Boolean)
extern void RoomOptions_set_PublishUserId_m2D2C26B98C031D132D3A7D450299BE55EBE2DE12 ();
// 0x000000F7 System.Boolean Photon.Realtime.RoomOptions::get_DeleteNullProperties()
extern void RoomOptions_get_DeleteNullProperties_m63EDCA05FFC2792D99F87A8BCB52F8D535236277 ();
// 0x000000F8 System.Void Photon.Realtime.RoomOptions::set_DeleteNullProperties(System.Boolean)
extern void RoomOptions_set_DeleteNullProperties_mD834C5A7E29312EAEBDE491AD005FCD7C4983AF2 ();
// 0x000000F9 System.Boolean Photon.Realtime.RoomOptions::get_BroadcastPropsChangeToAll()
extern void RoomOptions_get_BroadcastPropsChangeToAll_m148AB75D5E3A17B735D739B8C2217DA72774BE99 ();
// 0x000000FA System.Void Photon.Realtime.RoomOptions::set_BroadcastPropsChangeToAll(System.Boolean)
extern void RoomOptions_set_BroadcastPropsChangeToAll_m79EAEFCFEB7472FDD91FD10152BB60F6A1F0C9E7 ();
// 0x000000FB System.Void Photon.Realtime.RoomOptions::.ctor()
extern void RoomOptions__ctor_m254044C6ACFBFED793E00A41DF8E76F953EE16CF ();
// 0x000000FC System.Void Photon.Realtime.RaiseEventOptions::.ctor()
extern void RaiseEventOptions__ctor_m566DA262F193D98A75A09C2A00007CD8472A4329 ();
// 0x000000FD System.Void Photon.Realtime.RaiseEventOptions::.cctor()
extern void RaiseEventOptions__cctor_m0C865100F0214AB2184220C5B586BC98C80A92EB ();
// 0x000000FE System.Boolean Photon.Realtime.TypedLobby::get_IsDefault()
extern void TypedLobby_get_IsDefault_m9EABBD862010C30DC98DCFAC650D5DE378BDE77C ();
// 0x000000FF System.Void Photon.Realtime.TypedLobby::.ctor()
extern void TypedLobby__ctor_m00923523872B0E50E15357CBBED9A08D5F944586 ();
// 0x00000100 System.Void Photon.Realtime.TypedLobby::.ctor(System.String,Photon.Realtime.LobbyType)
extern void TypedLobby__ctor_mE5A5B745646CA0B8228EE2B6451B06BBE02D561D ();
// 0x00000101 System.String Photon.Realtime.TypedLobby::ToString()
extern void TypedLobby_ToString_mA56D3E449966F9879512DB3400679974B80DB5AA ();
// 0x00000102 System.Void Photon.Realtime.TypedLobby::.cctor()
extern void TypedLobby__cctor_m9A3C3621A62B5E941CBC71894D7318FA30EE84B7 ();
// 0x00000103 System.String Photon.Realtime.TypedLobbyInfo::ToString()
extern void TypedLobbyInfo_ToString_m5C7AB7E7308A8603657D1621D9AA98C027BD467C ();
// 0x00000104 System.Void Photon.Realtime.TypedLobbyInfo::.ctor()
extern void TypedLobbyInfo__ctor_m608A819C4E7B3D755880CBBE88AD68B11447F76B ();
// 0x00000105 Photon.Realtime.CustomAuthenticationType Photon.Realtime.AuthenticationValues::get_AuthType()
extern void AuthenticationValues_get_AuthType_m7644167D1D9D4B1EB47E98BF5FDA05D1B3F1770A ();
// 0x00000106 System.Void Photon.Realtime.AuthenticationValues::set_AuthType(Photon.Realtime.CustomAuthenticationType)
extern void AuthenticationValues_set_AuthType_mE9BBAB1E6FEFE22AE6318280C01AF83D0C96FDBB ();
// 0x00000107 System.String Photon.Realtime.AuthenticationValues::get_AuthGetParameters()
extern void AuthenticationValues_get_AuthGetParameters_m0D2F684B97C2814BFD84060C6A0CE55D49ACEB99 ();
// 0x00000108 System.Void Photon.Realtime.AuthenticationValues::set_AuthGetParameters(System.String)
extern void AuthenticationValues_set_AuthGetParameters_m62F77A15600CFD28F020C41EA6F853BC157BF6FE ();
// 0x00000109 System.Object Photon.Realtime.AuthenticationValues::get_AuthPostData()
extern void AuthenticationValues_get_AuthPostData_mF020D81C8FB8E5DAC6D3593EDD6590F0F50B2588 ();
// 0x0000010A System.Void Photon.Realtime.AuthenticationValues::set_AuthPostData(System.Object)
extern void AuthenticationValues_set_AuthPostData_m3AC15E1E1E29C1AE359C46DE691142CDF47820E5 ();
// 0x0000010B System.String Photon.Realtime.AuthenticationValues::get_Token()
extern void AuthenticationValues_get_Token_mA7C6F8A1DC5CE286C5E3BF198A08A82D573285DF ();
// 0x0000010C System.Void Photon.Realtime.AuthenticationValues::set_Token(System.String)
extern void AuthenticationValues_set_Token_m58AECA36241053F97241E0B3886BA8421ECA204C ();
// 0x0000010D System.String Photon.Realtime.AuthenticationValues::get_UserId()
extern void AuthenticationValues_get_UserId_m04BBEC6A7AEFD6975DB9F49E659A7963F8D840DC ();
// 0x0000010E System.Void Photon.Realtime.AuthenticationValues::set_UserId(System.String)
extern void AuthenticationValues_set_UserId_mC1264E09EC8EE5FE8717275120E4104643969E5D ();
// 0x0000010F System.Void Photon.Realtime.AuthenticationValues::.ctor()
extern void AuthenticationValues__ctor_mF5DC44DD388FE8648E072B765D5502C27D70FB8D ();
// 0x00000110 System.Void Photon.Realtime.AuthenticationValues::.ctor(System.String)
extern void AuthenticationValues__ctor_m5887BFFDEFDF7055AD84B5F94F0BF5E09F6D8190 ();
// 0x00000111 System.Void Photon.Realtime.AuthenticationValues::SetAuthPostData(System.String)
extern void AuthenticationValues_SetAuthPostData_m8CD17481FF2EE2FC2A880926D5314DC498498F41 ();
// 0x00000112 System.Void Photon.Realtime.AuthenticationValues::SetAuthPostData(System.Byte[])
extern void AuthenticationValues_SetAuthPostData_m92995794722C380DF97690DFEC803C871662B93F ();
// 0x00000113 System.Void Photon.Realtime.AuthenticationValues::SetAuthPostData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AuthenticationValues_SetAuthPostData_mF7283839644EAECF797CF6D9054A431F03E7C8E1 ();
// 0x00000114 System.Void Photon.Realtime.AuthenticationValues::AddAuthParameter(System.String,System.String)
extern void AuthenticationValues_AddAuthParameter_m2A7C499EA88A7CA15235304CA2FF080F4E13609E ();
// 0x00000115 System.String Photon.Realtime.AuthenticationValues::ToString()
extern void AuthenticationValues_ToString_m3D48D62AA7D57B8CF36E53C0E9A1AA8FABE9B858 ();
// 0x00000116 System.Boolean Photon.Realtime.PhotonPing::StartPing(System.String)
extern void PhotonPing_StartPing_m788B4EE59589E0D94D3AD208796B528AE4990411 ();
// 0x00000117 System.Boolean Photon.Realtime.PhotonPing::Done()
extern void PhotonPing_Done_m254644D4502B85A8A07E44BAA1E0F1B2C49FBFEB ();
// 0x00000118 System.Void Photon.Realtime.PhotonPing::Dispose()
extern void PhotonPing_Dispose_m0156F3A8D813288DDF437B61584F1EE133A9EF10 ();
// 0x00000119 System.Void Photon.Realtime.PhotonPing::Init()
extern void PhotonPing_Init_m54A5083386B01DF1DC852147913AC351863A4382 ();
// 0x0000011A System.Void Photon.Realtime.PhotonPing::.ctor()
extern void PhotonPing__ctor_mECF370ED85CFB0BD665631E8703A986485C17E5E ();
// 0x0000011B System.Void Photon.Realtime.PhotonPing::.cctor()
extern void PhotonPing__cctor_m56D1897D0278FABF7BCF7D5BA6A1B35629E237F7 ();
// 0x0000011C System.Boolean Photon.Realtime.PingMono::StartPing(System.String)
extern void PingMono_StartPing_m496A6971FE2DFBB3E2DE3B39EB0F32B12876BF20 ();
// 0x0000011D System.Boolean Photon.Realtime.PingMono::Done()
extern void PingMono_Done_mDE321FDDDA836C83A48624812C3E2EB267B6A4BC ();
// 0x0000011E System.Void Photon.Realtime.PingMono::Dispose()
extern void PingMono_Dispose_m3B12B9C9C32DF1B54EEFE1C1F25763146F2DE756 ();
// 0x0000011F System.Void Photon.Realtime.PingMono::.ctor()
extern void PingMono__ctor_mD2498F5A127182A684E088A9C4CE268B878CF101 ();
// 0x00000120 Photon.Realtime.Room Photon.Realtime.Player::get_RoomReference()
extern void Player_get_RoomReference_m930E8CD16F6BABE612F24D6C66D20749F7E34184 ();
// 0x00000121 System.Void Photon.Realtime.Player::set_RoomReference(Photon.Realtime.Room)
extern void Player_set_RoomReference_m94972048535DB91480569BC5E3B463A89CB12A91 ();
// 0x00000122 System.Int32 Photon.Realtime.Player::get_ActorNumber()
extern void Player_get_ActorNumber_m02157560C558C6007C8146F1FCDA7E759AD0CF1D ();
// 0x00000123 System.String Photon.Realtime.Player::get_NickName()
extern void Player_get_NickName_mB7D3A3FD8493B2FFF9B0537194CE9C697473C2EE ();
// 0x00000124 System.Void Photon.Realtime.Player::set_NickName(System.String)
extern void Player_set_NickName_m3AEC143158F0AC02C3139CC0501F90CF8062A32F ();
// 0x00000125 System.String Photon.Realtime.Player::get_UserId()
extern void Player_get_UserId_m1913731AD3876AA194CB56074FEBA28C6CB612E3 ();
// 0x00000126 System.Void Photon.Realtime.Player::set_UserId(System.String)
extern void Player_set_UserId_m84AA51D6267A1CD7E621206B543AB25ECAC80657 ();
// 0x00000127 System.Boolean Photon.Realtime.Player::get_IsMasterClient()
extern void Player_get_IsMasterClient_m54EECECB76D6BB178E5D7A75D32DEF78C3422695 ();
// 0x00000128 System.Boolean Photon.Realtime.Player::get_IsInactive()
extern void Player_get_IsInactive_m565C1FB590912C70E7720C6E1EFFFD5EE755C403 ();
// 0x00000129 System.Void Photon.Realtime.Player::set_IsInactive(System.Boolean)
extern void Player_set_IsInactive_mE662074B1394644E061C34E341119FF9F6C53245 ();
// 0x0000012A ExitGames.Client.Photon.Hashtable Photon.Realtime.Player::get_CustomProperties()
extern void Player_get_CustomProperties_m74B9E54472730B0454EFB1B2DD6056CCEEF8D44A ();
// 0x0000012B System.Void Photon.Realtime.Player::set_CustomProperties(ExitGames.Client.Photon.Hashtable)
extern void Player_set_CustomProperties_m306F31B2BAD0A6BE834BA7DECC7DBCE5C15FC901 ();
// 0x0000012C System.Void Photon.Realtime.Player::.ctor(System.String,System.Int32,System.Boolean)
extern void Player__ctor_m9BE0732C50CAC2F96D3ECDC5CB14BCD292E955CF ();
// 0x0000012D System.Void Photon.Realtime.Player::.ctor(System.String,System.Int32,System.Boolean,ExitGames.Client.Photon.Hashtable)
extern void Player__ctor_m4AB41D4C395CFEBEF41DC51B084186880645B4D5 ();
// 0x0000012E Photon.Realtime.Player Photon.Realtime.Player::Get(System.Int32)
extern void Player_Get_m4451719287EC83169424B9E50B85AAF385D9789D ();
// 0x0000012F Photon.Realtime.Player Photon.Realtime.Player::GetNext()
extern void Player_GetNext_m138BBE906F321BCEAA9C8E2B958C51E27343FE5E ();
// 0x00000130 Photon.Realtime.Player Photon.Realtime.Player::GetNextFor(Photon.Realtime.Player)
extern void Player_GetNextFor_mBB761E337EE5FB5FDCCB68CE7ABE00613C219007 ();
// 0x00000131 Photon.Realtime.Player Photon.Realtime.Player::GetNextFor(System.Int32)
extern void Player_GetNextFor_m6FC25EC7D3F354FB4525BE0236B579D4B3A05821 ();
// 0x00000132 System.Void Photon.Realtime.Player::InternalCacheProperties(ExitGames.Client.Photon.Hashtable)
extern void Player_InternalCacheProperties_m00AB6BABAF7989DC84EC913E2E562BEF47D24621 ();
// 0x00000133 System.String Photon.Realtime.Player::ToString()
extern void Player_ToString_m12CA93AF311B38987A36D29A13B6E5ECF165D9A0 ();
// 0x00000134 System.String Photon.Realtime.Player::ToStringFull()
extern void Player_ToStringFull_m3D8C720DEABC8F8104DC7C7B64831F80F740A926 ();
// 0x00000135 System.Boolean Photon.Realtime.Player::Equals(System.Object)
extern void Player_Equals_m20F6705EDCF31CA05EEE84209396E709019AC3DB ();
// 0x00000136 System.Int32 Photon.Realtime.Player::GetHashCode()
extern void Player_GetHashCode_m0B65F82910CDF0C5C0BFAA356172255EC6F55EC1 ();
// 0x00000137 System.Void Photon.Realtime.Player::ChangeLocalID(System.Int32)
extern void Player_ChangeLocalID_m70BF740B95E44DBC724FE10C66335DCFE9FFE9B8 ();
// 0x00000138 System.Void Photon.Realtime.Player::SetCustomProperties(ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,Photon.Realtime.WebFlags)
extern void Player_SetCustomProperties_m1D0AAD4B5F3A58D51842BCBF54581592FDF55B50 ();
// 0x00000139 System.Void Photon.Realtime.Player::SetPlayerNameProperty()
extern void Player_SetPlayerNameProperty_mCAB5BC326F55365FC7CF4CF811A94291E85AFE5A ();
// 0x0000013A System.String Photon.Realtime.Region::get_Code()
extern void Region_get_Code_m8669BE5070358182B467A85EFB042D25D5CDA3BC ();
// 0x0000013B System.Void Photon.Realtime.Region::set_Code(System.String)
extern void Region_set_Code_mDE9D1C927F6333EC50C6BA5E32DF4F0E07D6ECE6 ();
// 0x0000013C System.String Photon.Realtime.Region::get_Cluster()
extern void Region_get_Cluster_m850B9B953F6C87D089A2208F217662F8E3C90917 ();
// 0x0000013D System.Void Photon.Realtime.Region::set_Cluster(System.String)
extern void Region_set_Cluster_m989B1AE668634B2F4B1D2B065159CBA2FF026CA4 ();
// 0x0000013E System.String Photon.Realtime.Region::get_HostAndPort()
extern void Region_get_HostAndPort_m087CC4E1545F622AF35114318203F5BA85F287AC ();
// 0x0000013F System.Void Photon.Realtime.Region::set_HostAndPort(System.String)
extern void Region_set_HostAndPort_mA46DD23667655C529FE8F6D5F9AA469FA7268F30 ();
// 0x00000140 System.Int32 Photon.Realtime.Region::get_Ping()
extern void Region_get_Ping_mC348787B425CB426446A25A8CFC45030AB71C94E ();
// 0x00000141 System.Void Photon.Realtime.Region::set_Ping(System.Int32)
extern void Region_set_Ping_m6D98DD720653EEBC8C0A1D8B4E79CF80669ABA5D ();
// 0x00000142 System.Boolean Photon.Realtime.Region::get_WasPinged()
extern void Region_get_WasPinged_mD0FFE35A28AD219017CD894E52B9F113BCF88A82 ();
// 0x00000143 System.Void Photon.Realtime.Region::.ctor(System.String,System.String)
extern void Region__ctor_m4A0A97C2052C8BD4259777A271C4F3DA5EA420E0 ();
// 0x00000144 System.Void Photon.Realtime.Region::.ctor(System.String,System.Int32)
extern void Region__ctor_mB1E557941DB9463D60C45866B68870FEE7CE12D0 ();
// 0x00000145 System.Void Photon.Realtime.Region::SetCodeAndCluster(System.String)
extern void Region_SetCodeAndCluster_mB935EFD0D57480D78B21E1539E82DED3C243AD36 ();
// 0x00000146 System.String Photon.Realtime.Region::ToString()
extern void Region_ToString_m662501AB15980CD061ACEF5508E37628BDD28F00 ();
// 0x00000147 System.String Photon.Realtime.Region::ToString(System.Boolean)
extern void Region_ToString_m23EC241BD327ED9226F6E45B74FC433A4945F7E1 ();
// 0x00000148 System.Collections.Generic.List`1<Photon.Realtime.Region> Photon.Realtime.RegionHandler::get_EnabledRegions()
extern void RegionHandler_get_EnabledRegions_m9293FC4BE35917E9BC9F9689E5CA9C27DA639701 ();
// 0x00000149 System.Void Photon.Realtime.RegionHandler::set_EnabledRegions(System.Collections.Generic.List`1<Photon.Realtime.Region>)
extern void RegionHandler_set_EnabledRegions_m2BBACC92EA8CE600EFDB5D847992D9E22FDBF8B3 ();
// 0x0000014A Photon.Realtime.Region Photon.Realtime.RegionHandler::get_BestRegion()
extern void RegionHandler_get_BestRegion_m6C04157AB2A06C9FD8253DB875326225D9F2A9C2 ();
// 0x0000014B System.String Photon.Realtime.RegionHandler::get_SummaryToCache()
extern void RegionHandler_get_SummaryToCache_mE53B11556105433EBE7C76137D7FAAE4B5C57415 ();
// 0x0000014C System.String Photon.Realtime.RegionHandler::GetResults()
extern void RegionHandler_GetResults_mA141C10F13D5074C2FE740B8763C6C24832F1601 ();
// 0x0000014D System.Void Photon.Realtime.RegionHandler::SetRegions(ExitGames.Client.Photon.OperationResponse)
extern void RegionHandler_SetRegions_m8C5DED02195914843CDD84A3777BA7A5253F8FBB ();
// 0x0000014E System.Boolean Photon.Realtime.RegionHandler::get_IsPinging()
extern void RegionHandler_get_IsPinging_m90810A16C9825D7B6EA68B026F700FCB65C095D8 ();
// 0x0000014F System.Void Photon.Realtime.RegionHandler::set_IsPinging(System.Boolean)
extern void RegionHandler_set_IsPinging_mB9A230BD4F3B2B2564252341BD931F47968FB1A1 ();
// 0x00000150 System.Boolean Photon.Realtime.RegionHandler::PingMinimumOfRegions(System.Action`1<Photon.Realtime.RegionHandler>,System.String)
extern void RegionHandler_PingMinimumOfRegions_m5C8AE60A1AF4A9E06E3EA17A0B07C681FE455B17 ();
// 0x00000151 System.Void Photon.Realtime.RegionHandler::OnPreferredRegionPinged(Photon.Realtime.Region)
extern void RegionHandler_OnPreferredRegionPinged_m08CFBD385294B89DC35037B9EC36FB0309B6DA53 ();
// 0x00000152 System.Boolean Photon.Realtime.RegionHandler::PingEnabledRegions()
extern void RegionHandler_PingEnabledRegions_m58091984F466221DA5073C828543FECC667DACF4 ();
// 0x00000153 System.Void Photon.Realtime.RegionHandler::OnRegionDone(Photon.Realtime.Region)
extern void RegionHandler_OnRegionDone_m5416802428349BB3F28E925532C412A50DC3A172 ();
// 0x00000154 System.Void Photon.Realtime.RegionHandler::.ctor()
extern void RegionHandler__ctor_m97108E371414B468F72FAC6180DD4EC17E477D49 ();
// 0x00000155 System.Boolean Photon.Realtime.RegionPinger::get_Done()
extern void RegionPinger_get_Done_m2AA784BEAD23896BDC2D714D3C84CD1994469B0A ();
// 0x00000156 System.Void Photon.Realtime.RegionPinger::set_Done(System.Boolean)
extern void RegionPinger_set_Done_m3EFDA967730B0EEBC55AF5FE2C1F14D536D88AC2 ();
// 0x00000157 System.Void Photon.Realtime.RegionPinger::.ctor(Photon.Realtime.Region,System.Action`1<Photon.Realtime.Region>)
extern void RegionPinger__ctor_m2F25C93D7E6519E079AF5E3272004AF106E602E7 ();
// 0x00000158 Photon.Realtime.PhotonPing Photon.Realtime.RegionPinger::GetPingImplementation()
extern void RegionPinger_GetPingImplementation_m063FEEB76623D94A4B074E0257DDC90C92AC800A ();
// 0x00000159 System.Boolean Photon.Realtime.RegionPinger::Start()
extern void RegionPinger_Start_m7F7D855E24C252AB1BD68D035FBDFDF7989673A5 ();
// 0x0000015A System.Boolean Photon.Realtime.RegionPinger::RegionPingThreaded()
extern void RegionPinger_RegionPingThreaded_m483F046E255AACD42F9AC1758F9CEEAEE1E2629B ();
// 0x0000015B System.Collections.IEnumerator Photon.Realtime.RegionPinger::RegionPingCoroutine()
extern void RegionPinger_RegionPingCoroutine_mAA6EE986AB89E9B169021BD8698BDA9657AA5123 ();
// 0x0000015C System.String Photon.Realtime.RegionPinger::GetResults()
extern void RegionPinger_GetResults_mE78DBC4D3180AE742CDB2B077E1E01E647BBDC07 ();
// 0x0000015D System.String Photon.Realtime.RegionPinger::ResolveHost(System.String)
extern void RegionPinger_ResolveHost_m40FF2580A77B893BD7EF2A7B6A1C7B5713CB0CB0 ();
// 0x0000015E System.Void Photon.Realtime.RegionPinger::.cctor()
extern void RegionPinger__cctor_mB7CC8C974A4E6616D9682EB35BD32CBE3385E664 ();
// 0x0000015F Photon.Realtime.LoadBalancingClient Photon.Realtime.Room::get_LoadBalancingClient()
extern void Room_get_LoadBalancingClient_m352E09ADBA5592B3FC0F1432CE9F5D7D0FACC52B ();
// 0x00000160 System.Void Photon.Realtime.Room::set_LoadBalancingClient(Photon.Realtime.LoadBalancingClient)
extern void Room_set_LoadBalancingClient_mFA6EE0C996848CD1979DC014BDB9AAEF09B5728F ();
// 0x00000161 System.String Photon.Realtime.Room::get_Name()
extern void Room_get_Name_mCDF41719862596519BBE7091948656DF28B30607 ();
// 0x00000162 System.Void Photon.Realtime.Room::set_Name(System.String)
extern void Room_set_Name_mC4C4FFFFA614A20BFD543979EB818B9F332D77F9 ();
// 0x00000163 System.Boolean Photon.Realtime.Room::get_IsOffline()
extern void Room_get_IsOffline_mED7792F1D0C59ED40BE6B0F30D133D5B718DE73A ();
// 0x00000164 System.Void Photon.Realtime.Room::set_IsOffline(System.Boolean)
extern void Room_set_IsOffline_mB8087AC7DE2C5F438DE0EA52623EBEA97D6D9A01 ();
// 0x00000165 System.Boolean Photon.Realtime.Room::get_IsOpen()
extern void Room_get_IsOpen_m36A93CB7C99FF6ABB9F77C8E9DCE793362DC0436 ();
// 0x00000166 System.Void Photon.Realtime.Room::set_IsOpen(System.Boolean)
extern void Room_set_IsOpen_m9BF3D27C133879A9EF88E4EEB553878DC88AB4A2 ();
// 0x00000167 System.Boolean Photon.Realtime.Room::get_IsVisible()
extern void Room_get_IsVisible_m3C0E0466B3DFF449182378D88FA1D61251594838 ();
// 0x00000168 System.Void Photon.Realtime.Room::set_IsVisible(System.Boolean)
extern void Room_set_IsVisible_m5506840C476AFB8F2FF510C8A3320EC19042C010 ();
// 0x00000169 System.Byte Photon.Realtime.Room::get_MaxPlayers()
extern void Room_get_MaxPlayers_mD4BF3E0BF13AE675F4831A129E91D8F06D3EB124 ();
// 0x0000016A System.Void Photon.Realtime.Room::set_MaxPlayers(System.Byte)
extern void Room_set_MaxPlayers_mA500CB2E9F2F4A3E498FA8707B1409247BE790EA ();
// 0x0000016B System.Byte Photon.Realtime.Room::get_PlayerCount()
extern void Room_get_PlayerCount_mCBF32BA967FF797861D5E9C2B26C3A6BDACA2E99 ();
// 0x0000016C System.Collections.Generic.Dictionary`2<System.Int32,Photon.Realtime.Player> Photon.Realtime.Room::get_Players()
extern void Room_get_Players_m8F39EA784277611D7051C8A37EE813FFF4BCDDD7 ();
// 0x0000016D System.Void Photon.Realtime.Room::set_Players(System.Collections.Generic.Dictionary`2<System.Int32,Photon.Realtime.Player>)
extern void Room_set_Players_m0D5CFB1E82363D1AB270484990EE5D79D9811FB8 ();
// 0x0000016E System.String[] Photon.Realtime.Room::get_ExpectedUsers()
extern void Room_get_ExpectedUsers_mF6928B3A5B8CD93A75882BDDB08A2E759D4C565F ();
// 0x0000016F System.Int32 Photon.Realtime.Room::get_PlayerTtl()
extern void Room_get_PlayerTtl_m9947D50F953F4C37988D5496A3E03336F27CE370 ();
// 0x00000170 System.Void Photon.Realtime.Room::set_PlayerTtl(System.Int32)
extern void Room_set_PlayerTtl_mDC5CA1C7F2790DD26EDB6D8FD7FCFE723E60C55E ();
// 0x00000171 System.Int32 Photon.Realtime.Room::get_EmptyRoomTtl()
extern void Room_get_EmptyRoomTtl_mB2AB26E021613CAE926EED6C14FE2CB92100CBF9 ();
// 0x00000172 System.Void Photon.Realtime.Room::set_EmptyRoomTtl(System.Int32)
extern void Room_set_EmptyRoomTtl_mA79AEB412F560A793F95EF26DDDFF53367D5E8FD ();
// 0x00000173 System.Int32 Photon.Realtime.Room::get_MasterClientId()
extern void Room_get_MasterClientId_m1F799A66FDFD9D1BCB2F32B0F2327BF8CDF5075E ();
// 0x00000174 System.String[] Photon.Realtime.Room::get_PropertiesListedInLobby()
extern void Room_get_PropertiesListedInLobby_mBCB796C319E1DEF67E4F914072668F4844FB053D ();
// 0x00000175 System.Void Photon.Realtime.Room::set_PropertiesListedInLobby(System.String[])
extern void Room_set_PropertiesListedInLobby_m804175FB0D480F47629CA96F82DE6401DDF3F06F ();
// 0x00000176 System.Boolean Photon.Realtime.Room::get_AutoCleanUp()
extern void Room_get_AutoCleanUp_m2BA5EC0F7D2A7ADAB38AB0FEB5B0A130A252B48F ();
// 0x00000177 System.Void Photon.Realtime.Room::.ctor(System.String,Photon.Realtime.RoomOptions,System.Boolean)
extern void Room__ctor_mD644FF2C795EC5BBD50438BAB949C2414A873CB6 ();
// 0x00000178 System.Void Photon.Realtime.Room::InternalCacheProperties(ExitGames.Client.Photon.Hashtable)
extern void Room_InternalCacheProperties_m0E12655175DE56072B673AF38947C13CB04509A1 ();
// 0x00000179 System.Void Photon.Realtime.Room::SetCustomProperties(ExitGames.Client.Photon.Hashtable,ExitGames.Client.Photon.Hashtable,Photon.Realtime.WebFlags)
extern void Room_SetCustomProperties_m6E7842AF558D5148B4DF9AB5EF33827F9E9716EA ();
// 0x0000017A System.Void Photon.Realtime.Room::SetPropertiesListedInLobby(System.String[])
extern void Room_SetPropertiesListedInLobby_m01DA90D05074665DCCE6F73419C93325FF5504A1 ();
// 0x0000017B System.Void Photon.Realtime.Room::RemovePlayer(Photon.Realtime.Player)
extern void Room_RemovePlayer_m238395556BB7C376C19ADE84074EA4D3C043B215 ();
// 0x0000017C System.Void Photon.Realtime.Room::RemovePlayer(System.Int32)
extern void Room_RemovePlayer_mA6BA0D249B52408DE1C1C4B5E870CB2A27BF3BF5 ();
// 0x0000017D System.Boolean Photon.Realtime.Room::SetMasterClient(Photon.Realtime.Player)
extern void Room_SetMasterClient_m332177B7EA4EBF00CABB88FA984B651954CFA21C ();
// 0x0000017E System.Boolean Photon.Realtime.Room::AddPlayer(Photon.Realtime.Player)
extern void Room_AddPlayer_m563868E24D9EF0C9DAE29389430F2F07F3FFB147 ();
// 0x0000017F Photon.Realtime.Player Photon.Realtime.Room::StorePlayer(Photon.Realtime.Player)
extern void Room_StorePlayer_m12DAA94744AE461D74B16FDB01A71F6FEE3325CE ();
// 0x00000180 Photon.Realtime.Player Photon.Realtime.Room::GetPlayer(System.Int32)
extern void Room_GetPlayer_mFC75FEBE25AE023F5D8C4441EB6D168BF052BBD9 ();
// 0x00000181 System.Void Photon.Realtime.Room::ClearExpectedUsers()
extern void Room_ClearExpectedUsers_mD0908C38F02F79AED6461C32D8E8B344645E9068 ();
// 0x00000182 System.String Photon.Realtime.Room::ToString()
extern void Room_ToString_mEFDF611A6369048A0F0471689A43585AD19D76BB ();
// 0x00000183 System.String Photon.Realtime.Room::ToStringFull()
extern void Room_ToStringFull_m94133A3D76BCF79806886BBFCBEA1B6699BF0579 ();
// 0x00000184 ExitGames.Client.Photon.Hashtable Photon.Realtime.RoomInfo::get_CustomProperties()
extern void RoomInfo_get_CustomProperties_mA20AA5FAB69329282410561D4035F13132245AD0 ();
// 0x00000185 System.String Photon.Realtime.RoomInfo::get_Name()
extern void RoomInfo_get_Name_m24C3CFCDD6E862D5E0EA27A0B2736E09D72843DA ();
// 0x00000186 System.Int32 Photon.Realtime.RoomInfo::get_PlayerCount()
extern void RoomInfo_get_PlayerCount_m6E97FB80FDADD5A3B15ED2D51F071B3F343C1CC1 ();
// 0x00000187 System.Void Photon.Realtime.RoomInfo::set_PlayerCount(System.Int32)
extern void RoomInfo_set_PlayerCount_m406E55A69DFEB12E15AEB7B8DDF0B0C8F704911F ();
// 0x00000188 System.Byte Photon.Realtime.RoomInfo::get_MaxPlayers()
extern void RoomInfo_get_MaxPlayers_mB393159F17C716B593EC71158399F415ED15CA51 ();
// 0x00000189 System.Boolean Photon.Realtime.RoomInfo::get_IsOpen()
extern void RoomInfo_get_IsOpen_mB6999468BA5346E8D7C421DB3CDBA882C1288856 ();
// 0x0000018A System.Boolean Photon.Realtime.RoomInfo::get_IsVisible()
extern void RoomInfo_get_IsVisible_mB12677A2EC41DE627005F79B86C3CEB9A0532491 ();
// 0x0000018B System.Void Photon.Realtime.RoomInfo::.ctor(System.String,ExitGames.Client.Photon.Hashtable)
extern void RoomInfo__ctor_mFD07B7536EDF4DE7312D21661F123681D6581F2B ();
// 0x0000018C System.Boolean Photon.Realtime.RoomInfo::Equals(System.Object)
extern void RoomInfo_Equals_mA1F83E98B362D4DAF441D42C360164E18057BECC ();
// 0x0000018D System.Int32 Photon.Realtime.RoomInfo::GetHashCode()
extern void RoomInfo_GetHashCode_m86A2462634914114B25337548809456667A6240B ();
// 0x0000018E System.String Photon.Realtime.RoomInfo::ToString()
extern void RoomInfo_ToString_m47D9484C51886F87648BA365109134FB53CD933A ();
// 0x0000018F System.String Photon.Realtime.RoomInfo::ToStringFull()
extern void RoomInfo_ToStringFull_mE676F4200194103F85618888DE0790156EF89768 ();
// 0x00000190 System.Void Photon.Realtime.RoomInfo::InternalCacheProperties(ExitGames.Client.Photon.Hashtable)
extern void RoomInfo_InternalCacheProperties_m41D47859544098977936BF3964D1E3FB7746C249 ();
// 0x00000191 Photon.Realtime.LoadBalancingClient Photon.Realtime.SupportLogger::get_Client()
extern void SupportLogger_get_Client_mB26343CDA21A09F3EA30AD740275EAFC083D52B7 ();
// 0x00000192 System.Void Photon.Realtime.SupportLogger::set_Client(Photon.Realtime.LoadBalancingClient)
extern void SupportLogger_set_Client_m166B9D97ABDFAA4A8E7792F8C8861BE4C7193976 ();
// 0x00000193 System.Void Photon.Realtime.SupportLogger::Start()
extern void SupportLogger_Start_m1198B8A81F90188616E5942DCA38D0D79A8C7479 ();
// 0x00000194 System.Void Photon.Realtime.SupportLogger::OnApplicationPause(System.Boolean)
extern void SupportLogger_OnApplicationPause_m394AE8B8288BFCE03DD16287100911E4D922E01B ();
// 0x00000195 System.Void Photon.Realtime.SupportLogger::OnApplicationQuit()
extern void SupportLogger_OnApplicationQuit_mF14FE14659F54814A80D0DDCA9A8FFEC532CEC81 ();
// 0x00000196 System.Void Photon.Realtime.SupportLogger::StartLogStats()
extern void SupportLogger_StartLogStats_m973A1BFAAB04C8F0F9568E1A281C6EE553384610 ();
// 0x00000197 System.Void Photon.Realtime.SupportLogger::StopLogStats()
extern void SupportLogger_StopLogStats_m5EA95B7B9DD9BEB62FCB674FBDED85DB910C3B06 ();
// 0x00000198 System.Void Photon.Realtime.SupportLogger::StartTrackValues()
extern void SupportLogger_StartTrackValues_mCCA9284BFA342F2822E786C069773B61ED626F55 ();
// 0x00000199 System.Void Photon.Realtime.SupportLogger::StopTrackValues()
extern void SupportLogger_StopTrackValues_m0806BA608466711C295BC23883AFAF6AEE153CB4 ();
// 0x0000019A System.String Photon.Realtime.SupportLogger::GetFormattedTimestamp()
extern void SupportLogger_GetFormattedTimestamp_m32FC37B0F0AE804113DD4C24DB3BE4AE6193D821 ();
// 0x0000019B System.Void Photon.Realtime.SupportLogger::TrackValues()
extern void SupportLogger_TrackValues_m66D40C21DDFC4AA2988C44ACAF53AE3FE76FC39C ();
// 0x0000019C System.Void Photon.Realtime.SupportLogger::LogStats()
extern void SupportLogger_LogStats_m420DC4631D59DCE0F7D33C3146C8688D08C54F94 ();
// 0x0000019D System.Void Photon.Realtime.SupportLogger::LogBasics()
extern void SupportLogger_LogBasics_m1DD18EAF9701CABB4D230E939DC67A471B2C4B13 ();
// 0x0000019E System.Void Photon.Realtime.SupportLogger::OnConnected()
extern void SupportLogger_OnConnected_mA89BCA9B3319032CF17DEB3BE76B87AA54C06C76 ();
// 0x0000019F System.Void Photon.Realtime.SupportLogger::OnConnectedToMaster()
extern void SupportLogger_OnConnectedToMaster_mF52A4381DCC7C6CE19F3451D539020E39B2A11C4 ();
// 0x000001A0 System.Void Photon.Realtime.SupportLogger::OnFriendListUpdate(System.Collections.Generic.List`1<Photon.Realtime.FriendInfo>)
extern void SupportLogger_OnFriendListUpdate_mB422AD317F908DDCA99272016652A88A22FBAF47 ();
// 0x000001A1 System.Void Photon.Realtime.SupportLogger::OnJoinedLobby()
extern void SupportLogger_OnJoinedLobby_m3F1EFC503B05CA5A2410C4B5FF08405E6A4ECB2C ();
// 0x000001A2 System.Void Photon.Realtime.SupportLogger::OnLeftLobby()
extern void SupportLogger_OnLeftLobby_m33D2D8A6305BA65C80658D8B0AC3DADB7FAEC419 ();
// 0x000001A3 System.Void Photon.Realtime.SupportLogger::OnCreateRoomFailed(System.Int16,System.String)
extern void SupportLogger_OnCreateRoomFailed_mE38FC7667E7002A91626809317BECD47D71FDCCF ();
// 0x000001A4 System.Void Photon.Realtime.SupportLogger::OnJoinedRoom()
extern void SupportLogger_OnJoinedRoom_mB37EE4EAB942E5A6A8890C6FA4E1EB6740E16B82 ();
// 0x000001A5 System.Void Photon.Realtime.SupportLogger::OnJoinRoomFailed(System.Int16,System.String)
extern void SupportLogger_OnJoinRoomFailed_m3488EE58BF74437B2142A96EBF18DCB6B4B2C48C ();
// 0x000001A6 System.Void Photon.Realtime.SupportLogger::OnJoinRandomFailed(System.Int16,System.String)
extern void SupportLogger_OnJoinRandomFailed_mAE5ECBF451C6E4FDFE2F85E0DC30F525319D12F0 ();
// 0x000001A7 System.Void Photon.Realtime.SupportLogger::OnCreatedRoom()
extern void SupportLogger_OnCreatedRoom_mF35AB68837976AC88555409F51F24C21C4A8C2F2 ();
// 0x000001A8 System.Void Photon.Realtime.SupportLogger::OnLeftRoom()
extern void SupportLogger_OnLeftRoom_m05D5834FFADC1CCD590D7606819AA48399A3C76F ();
// 0x000001A9 System.Void Photon.Realtime.SupportLogger::OnDisconnected(Photon.Realtime.DisconnectCause)
extern void SupportLogger_OnDisconnected_mDE77493509B7A736D17258EABD0BD827843C9950 ();
// 0x000001AA System.Void Photon.Realtime.SupportLogger::OnRegionListReceived(Photon.Realtime.RegionHandler)
extern void SupportLogger_OnRegionListReceived_mC0CEDEA1F7A9B6376FAF45AFEB99FE7F0B36A720 ();
// 0x000001AB System.Void Photon.Realtime.SupportLogger::OnRoomListUpdate(System.Collections.Generic.List`1<Photon.Realtime.RoomInfo>)
extern void SupportLogger_OnRoomListUpdate_m8268A931B26BA87B2F0B52F6F21A02B76CF7E2F5 ();
// 0x000001AC System.Void Photon.Realtime.SupportLogger::OnPlayerEnteredRoom(Photon.Realtime.Player)
extern void SupportLogger_OnPlayerEnteredRoom_m154F337E9EB056F73C076F87B3D79F7C60203933 ();
// 0x000001AD System.Void Photon.Realtime.SupportLogger::OnPlayerLeftRoom(Photon.Realtime.Player)
extern void SupportLogger_OnPlayerLeftRoom_m341B83040EF40500E395885DEE61E6EF45B7FD31 ();
// 0x000001AE System.Void Photon.Realtime.SupportLogger::OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable)
extern void SupportLogger_OnRoomPropertiesUpdate_mB8A03017BE35E642DE3AC3C44F43D4746F1FB330 ();
// 0x000001AF System.Void Photon.Realtime.SupportLogger::OnPlayerPropertiesUpdate(Photon.Realtime.Player,ExitGames.Client.Photon.Hashtable)
extern void SupportLogger_OnPlayerPropertiesUpdate_m840D49D30EF7F8167EEA067779DAE434C778445C ();
// 0x000001B0 System.Void Photon.Realtime.SupportLogger::OnMasterClientSwitched(Photon.Realtime.Player)
extern void SupportLogger_OnMasterClientSwitched_m3658AC3899549A5D3788B25BD338CDDA9C842F8D ();
// 0x000001B1 System.Void Photon.Realtime.SupportLogger::OnCustomAuthenticationResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void SupportLogger_OnCustomAuthenticationResponse_mF5F844DB7163FEE64FB92021F75C644C44D2CF35 ();
// 0x000001B2 System.Void Photon.Realtime.SupportLogger::OnCustomAuthenticationFailed(System.String)
extern void SupportLogger_OnCustomAuthenticationFailed_m63D26B6876F0FD3409FD609D7354B59DCA6B6648 ();
// 0x000001B3 System.Void Photon.Realtime.SupportLogger::OnLobbyStatisticsUpdate(System.Collections.Generic.List`1<Photon.Realtime.TypedLobbyInfo>)
extern void SupportLogger_OnLobbyStatisticsUpdate_m9224062BC0172E2FE07283717A658EF01153EB68 ();
// 0x000001B4 System.Void Photon.Realtime.SupportLogger::.ctor()
extern void SupportLogger__ctor_m17A4C242A97DE0BC726D88511103F1AB72B49653 ();
// 0x000001B5 System.String Photon.Realtime.WebRpcResponse::get_Name()
extern void WebRpcResponse_get_Name_m0B6E38116B73FE932B4809D1510013A2D0E75FEB ();
// 0x000001B6 System.Void Photon.Realtime.WebRpcResponse::set_Name(System.String)
extern void WebRpcResponse_set_Name_mEADC4EB6ED220410F817BB3A5864230A0215CA01 ();
// 0x000001B7 System.Int32 Photon.Realtime.WebRpcResponse::get_ResultCode()
extern void WebRpcResponse_get_ResultCode_mFC73DF1E0EF9021C8C3E53EC0EF548915B566886 ();
// 0x000001B8 System.Void Photon.Realtime.WebRpcResponse::set_ResultCode(System.Int32)
extern void WebRpcResponse_set_ResultCode_m8E4EE89F34D749CCFEA3B1D2D777A5A8D3FA33AA ();
// 0x000001B9 System.Int32 Photon.Realtime.WebRpcResponse::get_ReturnCode()
extern void WebRpcResponse_get_ReturnCode_mC2DB2985F6E1F2AB60A26975A4D7DC88C5D7562A ();
// 0x000001BA System.String Photon.Realtime.WebRpcResponse::get_Message()
extern void WebRpcResponse_get_Message_m9F817E8E921967871322B123F6C1BEF6F8BF5138 ();
// 0x000001BB System.Void Photon.Realtime.WebRpcResponse::set_Message(System.String)
extern void WebRpcResponse_set_Message_m0A4DB2E9F565AD93775CAE27C140A9DB4E411B78 ();
// 0x000001BC System.String Photon.Realtime.WebRpcResponse::get_DebugMessage()
extern void WebRpcResponse_get_DebugMessage_mD4740D583D4CF227657519753D2145759B371714 ();
// 0x000001BD System.Collections.Generic.Dictionary`2<System.String,System.Object> Photon.Realtime.WebRpcResponse::get_Parameters()
extern void WebRpcResponse_get_Parameters_m73734E2B75DC69FF24E5EBFE1C58383DB7F01969 ();
// 0x000001BE System.Void Photon.Realtime.WebRpcResponse::set_Parameters(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void WebRpcResponse_set_Parameters_mC93C08CE135944569FF91BC362C850E628E0563A ();
// 0x000001BF System.Void Photon.Realtime.WebRpcResponse::.ctor(ExitGames.Client.Photon.OperationResponse)
extern void WebRpcResponse__ctor_m26AC599DEC9251A690AF67126B50C7A395E6E557 ();
// 0x000001C0 System.String Photon.Realtime.WebRpcResponse::ToStringFull()
extern void WebRpcResponse_ToStringFull_mD16B7B3F985553A86DC907BC3836B249EC84F203 ();
// 0x000001C1 System.Boolean Photon.Realtime.WebFlags::get_HttpForward()
extern void WebFlags_get_HttpForward_m06B3FF0980DD3F3AC493ECF0897CFAFEB6101AB7 ();
// 0x000001C2 System.Void Photon.Realtime.WebFlags::set_HttpForward(System.Boolean)
extern void WebFlags_set_HttpForward_m5057520D45E0EE8B75146FF633C741C88E6DB1E5 ();
// 0x000001C3 System.Boolean Photon.Realtime.WebFlags::get_SendAuthCookie()
extern void WebFlags_get_SendAuthCookie_m4C3809BF83E26E6A0A0BBCF1B656FB420DB099C5 ();
// 0x000001C4 System.Void Photon.Realtime.WebFlags::set_SendAuthCookie(System.Boolean)
extern void WebFlags_set_SendAuthCookie_m67D7AF57044292DA159A46AE8F4DC27F1DD3A46E ();
// 0x000001C5 System.Boolean Photon.Realtime.WebFlags::get_SendSync()
extern void WebFlags_get_SendSync_m8E4500A08237E7ACF599CF7A77EF1F0B419D7FE2 ();
// 0x000001C6 System.Void Photon.Realtime.WebFlags::set_SendSync(System.Boolean)
extern void WebFlags_set_SendSync_m1C9961C96558BF31ED185876EA55990A4F54CAE3 ();
// 0x000001C7 System.Boolean Photon.Realtime.WebFlags::get_SendState()
extern void WebFlags_get_SendState_m7085E7394ECDB9CDC07BF0A4C2A0037E344076C5 ();
// 0x000001C8 System.Void Photon.Realtime.WebFlags::set_SendState(System.Boolean)
extern void WebFlags_set_SendState_m772DFC77F5169E3548302A9F601B87DC11765909 ();
// 0x000001C9 System.Void Photon.Realtime.WebFlags::.ctor(System.Byte)
extern void WebFlags__ctor_mF30D282AD9F1D728C06672D22FCB7E108BCB1993 ();
// 0x000001CA System.Void Photon.Realtime.WebFlags::.cctor()
extern void WebFlags__cctor_m98EB282DAF4EDC93CC587315D2B06E55CB86DDE0 ();
// 0x000001CB System.Void Photon.Realtime.LoadBalancingClient_CallbackTargetChange::.ctor(System.Object,System.Boolean)
extern void CallbackTargetChange__ctor_m071B8C0B4920F5D8508B702D0F378FF09FEA6AAF ();
// 0x000001CC System.Void Photon.Realtime.RegionHandler_<>c::.cctor()
extern void U3CU3Ec__cctor_mE157ACC2760DCC7D615439E9D2BF9512AB689D75 ();
// 0x000001CD System.Void Photon.Realtime.RegionHandler_<>c::.ctor()
extern void U3CU3Ec__ctor_m4D4266CBFC2BBD4129FA851865400633B635659D ();
// 0x000001CE System.Int32 Photon.Realtime.RegionHandler_<>c::<get_BestRegion>b__7_0(Photon.Realtime.Region,Photon.Realtime.Region)
extern void U3CU3Ec_U3Cget_BestRegionU3Eb__7_0_m19DF237C8A5595D6C2461E1F41D5CE1514798CF5 ();
// 0x000001CF System.Void Photon.Realtime.RegionHandler_<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m0BB82F56172BF13DA08595DF2153BF58EDF62985 ();
// 0x000001D0 System.Boolean Photon.Realtime.RegionHandler_<>c__DisplayClass20_0::<PingMinimumOfRegions>b__0(Photon.Realtime.Region)
extern void U3CU3Ec__DisplayClass20_0_U3CPingMinimumOfRegionsU3Eb__0_m0AFA0CB1C83EE0A0704D4E20A922734556679FDE ();
// 0x000001D1 System.Void Photon.Realtime.RegionPinger_<RegionPingCoroutine>d__18::.ctor(System.Int32)
extern void U3CRegionPingCoroutineU3Ed__18__ctor_m1687C8F26AC6D6AB23A742557D443CAE7BC1694B ();
// 0x000001D2 System.Void Photon.Realtime.RegionPinger_<RegionPingCoroutine>d__18::System.IDisposable.Dispose()
extern void U3CRegionPingCoroutineU3Ed__18_System_IDisposable_Dispose_mE0FA56CBF47F36727542166ABB77803F24623733 ();
// 0x000001D3 System.Boolean Photon.Realtime.RegionPinger_<RegionPingCoroutine>d__18::MoveNext()
extern void U3CRegionPingCoroutineU3Ed__18_MoveNext_mAF2A7EDB1E021F98A41C5044ABF0B1B6DF712622 ();
// 0x000001D4 System.Object Photon.Realtime.RegionPinger_<RegionPingCoroutine>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRegionPingCoroutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EA0FF0C14DD701C64E5475B3AA4727FDC75FCF6 ();
// 0x000001D5 System.Void Photon.Realtime.RegionPinger_<RegionPingCoroutine>d__18::System.Collections.IEnumerator.Reset()
extern void U3CRegionPingCoroutineU3Ed__18_System_Collections_IEnumerator_Reset_mAD2B9AE9A175BDBEF80DEA1CEB6C1D2732626627 ();
// 0x000001D6 System.Object Photon.Realtime.RegionPinger_<RegionPingCoroutine>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CRegionPingCoroutineU3Ed__18_System_Collections_IEnumerator_get_Current_m63007C2C2FA0D2F9870974336AC6C7B7DCB42E0C ();
static Il2CppMethodPointer s_methodPointers[470] = 
{
	AppSettings_get_IsMasterServerAddress_m2F43A395E93E666FA86C8D6994E00260BAD434A5,
	AppSettings_get_IsBestRegion_m534861CD875A9D5E8801052024D12A5F31EA1FE7,
	AppSettings_get_IsDefaultNameServer_m70F8F26188A7EB0B8D55A9904A2F51498B36C941,
	AppSettings_get_IsDefaultPort_mFD502FC4090BAE524899B8FE8D73F783A7BEF851,
	AppSettings_ToStringFull_m1C7E9BA1BBCB3C2AA844F4928FE9676AD5359750,
	AppSettings__ctor_mEE7F23500B21898BF51BD410D1CB360DEBF66931,
	ConnectionHandler_get_Client_mD602A5800223D074F0CF6B96FFE8529F545A4E80,
	ConnectionHandler_set_Client_m41E8AA7C73CEC09E0C73FE3532C233F109DCBC45,
	ConnectionHandler_get_CountSendAcksOnly_m91028B52B53946478ACB442ED9DD1C16E7115C57,
	ConnectionHandler_set_CountSendAcksOnly_mD2D80CCEEBAACC896F8B35C6938F734E01C4104E,
	ConnectionHandler_get_FallbackThreadRunning_m1BE4BE852820C4869BBBC66C21F57B9E573620B7,
	ConnectionHandler_OnApplicationQuit_m3417E7F60D002C50325C2586F18DD3AF7E9550E6,
	ConnectionHandler_Awake_m83D273997434940D3BB1A5431B7FFB4C8C1C5DAB,
	ConnectionHandler_OnDisable_mDFD9EF4418130730F105A69F1554FD1DA0842F31,
	ConnectionHandler_StartFallbackSendAckThread_mC683B581861BC397A4F8B6D5DD497C3BBA20B7A7,
	ConnectionHandler_StopFallbackSendAckThread_m735B91669EDB32A4F14196C741629CAF97E8D13D,
	ConnectionHandler_RealtimeFallbackThread_m3D6526BB05E1494789907593A678F679B159C273,
	ConnectionHandler__ctor_mB639F2C7F1DCE9C49A17C1CBEBABAC03ABB218E2,
	Extensions_Merge_mE5A42AF13B84826877703729DB671E74D15D5AC9,
	Extensions_MergeStringKeys_mDCDF6621E96473D5021BFCEC97B22804F387951A,
	Extensions_ToStringFull_m7D5DC29B4341359CC4354664509068EF620E9DCC,
	NULL,
	Extensions_ToStringFull_m2121FA96DD4725901A192FA61DC03AAF6766D239,
	Extensions_StripToStringKeys_m3E7F93E0E13C29A850947BBA3D195FC43FF3C562,
	Extensions_StripKeysWithNullValues_m7708059EAF935DD384169C58E072392DB34E0055,
	Extensions_Contains_m93F4A6BA815601CE40B03D37FCA0F51C9BA1EC71,
	FriendInfo_get_Name_m7E6D9124C3CDE8D6045D3FA8B4AB2B4BE03D08AE,
	FriendInfo_get_UserId_m475DCCA64B31738682706B4B7D02A520394E951C,
	FriendInfo_set_UserId_m635303D0D3B6F265DC5EA4BC48DC6F7F7CAEAC4B,
	FriendInfo_get_IsOnline_mB5135E0CCCE94C97B89DC682844A93E04E7AC461,
	FriendInfo_set_IsOnline_mF82EFFC6D63C22396272B734F74176E4B46D3ABE,
	FriendInfo_get_Room_m67D7169E01E1F1C9C31EBCCB8BC706090BAB3119,
	FriendInfo_set_Room_m77970C7788BA7A903FA0E4C83CD2C53CC7CBF251,
	FriendInfo_get_IsInRoom_mF994126A3C7188F446816E4D1079A1327DE0F9D8,
	FriendInfo_ToString_mEADB7533405EBFBF96B8FD2BD79A9E421170A61C,
	FriendInfo__ctor_m3E7BB94A24F402D1A41247C30F470CE6F6AEB4B0,
	LoadBalancingClient_get_LoadBalancingPeer_m0997FEE69F076CDD9740F8A3B34038733175A243,
	LoadBalancingClient_set_LoadBalancingPeer_m8B21BEA9184F84DCDE7BBB9FBB60134B29B29C3D,
	LoadBalancingClient_get_AppVersion_m2D816CED5CD4B71B47C03B33D2C5DFAE08997B9D,
	LoadBalancingClient_set_AppVersion_mB366065ACFDA6142E92B1585194423E398A9260E,
	LoadBalancingClient_get_AppId_m49A1261CC79356CABF9A709AAC4674B406ECA1DD,
	LoadBalancingClient_set_AppId_m526B1D687E3EE08B889F2CBD2F84853C6DA86F27,
	LoadBalancingClient_get_AuthValues_mE56B5ADE8CC0EBB1CB9C5DFA64913AC5B36B7F02,
	LoadBalancingClient_set_AuthValues_mB58414B316951242AD2045E85DF048B5357A9C28,
	LoadBalancingClient_get_TokenForInit_m069DD8AE48586F2D321FC639DFF6C9D9182ADC13,
	LoadBalancingClient_get_IsUsingNameServer_m018A230E641F2CEC1B60DE60EB6BE472834F5092,
	LoadBalancingClient_set_IsUsingNameServer_mDF99F6C889F0139711EB50A21D0055A7E4A0F8B2,
	LoadBalancingClient_get_NameServerAddress_m3427DF83D68B873E55A3496C4B5C77E1324C7D06,
	LoadBalancingClient_get_UseAlternativeUdpPorts_m99F6E198B4B848CDF32991FBED08120DCB87915B,
	LoadBalancingClient_set_UseAlternativeUdpPorts_mE307DBDAB3A4BC8E2D81F5BF899830132F53C1A3,
	LoadBalancingClient_get_CurrentServerAddress_m617D5473B405AA52AA10EBC3FB8CEC4E65252A02,
	LoadBalancingClient_get_MasterServerAddress_mE550265A7F1B30E6D1464D0139E859483D507F6B,
	LoadBalancingClient_set_MasterServerAddress_m988C29C7041A715EDA996AD6B5A6370AD6842424,
	LoadBalancingClient_get_GameServerAddress_m572210F250D42394A3E6E0FAB7E15DBD6EE78E05,
	LoadBalancingClient_set_GameServerAddress_mD863BE3068C8A77BD9E85C5F090EB88A73EF9B65,
	LoadBalancingClient_get_Server_mF0DA3FA871E123896E53C39758BAF853F1A80EC3,
	LoadBalancingClient_set_Server_m6F4DE1ABC82A6FF949AA370EA18D18920597527E,
	LoadBalancingClient_get_State_mDCC22D02A85973C68623C764C96BE9A862C8BD11,
	LoadBalancingClient_set_State_m1D4AA099CA318F90B257BC03CF9F54979E677BCA,
	LoadBalancingClient_get_IsConnected_m90DAE8B064E99BA87DC5CDAC811D38647D468C2C,
	LoadBalancingClient_get_IsConnectedAndReady_m14A6F4A0A3D74E499F65B0360C76A33E94AFAC9C,
	LoadBalancingClient_add_StateChanged_m630663477C5815D2183AC49D0F5B9C1A5860B50B,
	LoadBalancingClient_remove_StateChanged_mC9DBCEE79FE46D63C9BDB1FDDD3509040BB0F622,
	LoadBalancingClient_add_EventReceived_m30CC69C5EEA1781F3DD52E0C1F907AF4DD16D338,
	LoadBalancingClient_remove_EventReceived_mB11D1D0A6B98AF8E47C0B12C2256F35455E9AD84,
	LoadBalancingClient_add_OpResponseReceived_mB846EA81D44DBB08FBA43398AAECCC32A40540C5,
	LoadBalancingClient_remove_OpResponseReceived_m9DC9E23D3DC953AEDDFD9EEE78DF2ED102E96E35,
	LoadBalancingClient_get_DisconnectedCause_mEC001DE2A4D9E6BC4A364D7F3039EDF72DF395B2,
	LoadBalancingClient_set_DisconnectedCause_mA384559B6EA4E119910272DF3BBBC1326C47C3E6,
	LoadBalancingClient_get_InLobby_mEF3C513D70D233C19ED61DAF2D4450DA5184A7B9,
	LoadBalancingClient_get_CurrentLobby_mA012C41E1DF7DF53F0CF4A024C9E0FABDDDC2456,
	LoadBalancingClient_set_CurrentLobby_m77CC4772915A138EE66B874EBB0A9F707C4A8FF8,
	LoadBalancingClient_get_LocalPlayer_mAB1446D19EBDAE3D7D47E92F560AE939B3F21CD8,
	LoadBalancingClient_set_LocalPlayer_m3AA86EC255BF530EC13FA13CB3BE4D8475D60137,
	LoadBalancingClient_get_NickName_m8075389432D8241A5681B82A8C63222B894E060F,
	LoadBalancingClient_set_NickName_m5DBB4C8C18626B5CC593EC56A564C55B51A3306E,
	LoadBalancingClient_get_UserId_mE44D3E9907F0B72F57E8D6CBF3A0E80B07064AA8,
	LoadBalancingClient_set_UserId_m8CE468179FE3969883147E165AD466021B0BABB6,
	LoadBalancingClient_get_CurrentRoom_mA294529AE11CDDF17597FADE478A392B8B534002,
	LoadBalancingClient_set_CurrentRoom_mE849A43CF3269E76CE909EE4DE23265175EEB596,
	LoadBalancingClient_get_InRoom_m7DAE0F52D1CF9FF0B2373BE9559788C1752E03B3,
	LoadBalancingClient_get_PlayersOnMasterCount_m8DBA036419F15FE61A7FCDFE8DE1906FE95DB67F,
	LoadBalancingClient_set_PlayersOnMasterCount_mBC03E29C60BE48118AD42E71EF2B123029B53E32,
	LoadBalancingClient_get_PlayersInRoomsCount_m064FF861BE6A381EC4D87C4F342F62AA6ACA3C21,
	LoadBalancingClient_set_PlayersInRoomsCount_m99D3F0A6CDA8A3475CBFE8EB3549E2192E970447,
	LoadBalancingClient_get_RoomsCount_mDE93A42C477C145F9643BF64FA678B7546819063,
	LoadBalancingClient_set_RoomsCount_m3F6B5B1193EB8419109C85CDCF5220B377932AEA,
	LoadBalancingClient_get_IsFetchingFriendList_m173C51FE398A7F356F7875DCA876B059960BFCD8,
	LoadBalancingClient_get_CloudRegion_m0AAC37B5520321CC88C90C625E56EBEC822C3DEB,
	LoadBalancingClient_set_CloudRegion_m6DBD960027DE67168D93A00A75861C9552ED9393,
	LoadBalancingClient_get_CurrentCluster_mEDA76121FC84D677906B31A2EF74EB72292C3B82,
	LoadBalancingClient_set_CurrentCluster_m8AA87CB42014FB4F1305ECF87124B4E2023F6154,
	LoadBalancingClient__ctor_m3FE1B0111907E5D6B0863EE8DDD2BAE687A2F12A,
	LoadBalancingClient__ctor_m055218307EA53ED610F32877AE7CD413352E6072,
	LoadBalancingClient_GetNameServerAddress_m11E7EB63DB1C7E0DDBE48A49D7DE82140ED634BC,
	LoadBalancingClient_Connect_mCF5DFFA5B00A2F5EF372AEC99932E51AE7E6E81C,
	LoadBalancingClient_ConnectToNameServer_m2DFF6510956EA30345D6A055272C86D7390BC9FC,
	LoadBalancingClient_ConnectToRegionMaster_m10E860D595ED32FBAF8F7809C20CEA18C477DD76,
	LoadBalancingClient_Connect_mD355C7839D9FFB63E33F1AC75E22355186E7E8F0,
	LoadBalancingClient_ConnectToGameServer_m38C66511C6AC3F6DDF2548832405ABD9415F651F,
	LoadBalancingClient_ReconnectToMaster_m4761338630C7B0FF8917913FC1FDB6AA6E78FCC1,
	LoadBalancingClient_ReconnectAndRejoin_m033BA70D4D0605C4CC679DCFEC7CDDDFA18AA563,
	LoadBalancingClient_Disconnect_mC22029D6906E00B19FE967EC5695177F9B262C51,
	LoadBalancingClient_DisconnectToReconnect_mD4506B2DB0BAC6E370909D28CA8C5B4630D631FE,
	LoadBalancingClient_SimulateConnectionLoss_m4C381965C26752DD6E9D6EACDBCEFB25D72CD6A1,
	LoadBalancingClient_CallAuthenticate_mCA1632629441AA360D28CAF559445F4DC3D0C9E3,
	LoadBalancingClient_Service_m443AD58B507AD1E7601CB7785BD4804ACFD1868A,
	LoadBalancingClient_OpGetRegions_m46A3AFA0AD29E32E2424BC20F78D438333D421FE,
	LoadBalancingClient_OpFindFriends_m7ECF4CAB63B89AA5D1AC0D6A2E6222BD16AB9985,
	LoadBalancingClient_OpJoinLobby_m6FEB6C0808D3963ADC90E55D9ED9B7357AD61DB7,
	LoadBalancingClient_OpLeaveLobby_m079C4D33791C5B254E2EC06A95D29CC6D618F9EA,
	LoadBalancingClient_OpJoinRandomRoom_m538FE487F277FF1F054E8A0309F751DA76704376,
	LoadBalancingClient_OpJoinRandomOrCreateRoom_m0594DC879B570D9C75EBA2B87181D50D804D2057,
	LoadBalancingClient_OpCreateRoom_m5B1E30682BEFB186628ABACBBDA004259AA37CD8,
	LoadBalancingClient_OpJoinOrCreateRoom_m7619F10D86CF4AC1F62C2B23CD5CFF56876753B6,
	LoadBalancingClient_OpJoinRoom_m3BD6B9BB0D6A67A9A0FA7D3F0B56D9A2580B26A8,
	LoadBalancingClient_OpRejoinRoom_m918AFF2E3CEDDFFBC6DE787F799C7A88D797CBC8,
	LoadBalancingClient_OpLeaveRoom_m210103CE0E2DA347DF6C962173DE4D59FE2C9C8B,
	LoadBalancingClient_OpGetGameList_mB230101A905B4322C01D32D1B148787EF6A6F48A,
	LoadBalancingClient_OpSetCustomPropertiesOfActor_mD0652B67E064E24A89BC240C8C274EA72714110B,
	LoadBalancingClient_OpSetPropertiesOfActor_mB1D67FAA256836C1F5ED1286B0A74565BADD324E,
	LoadBalancingClient_OpSetCustomPropertiesOfRoom_m868620D8B9154EFCE198CF5A670EAB7567598E0C,
	LoadBalancingClient_OpSetPropertyOfRoom_mA606EAD4CDB750B342CF07F0664F06506AF1E069,
	LoadBalancingClient_OpSetPropertiesOfRoom_mE86C47F8A6D529E5B50FC9FA78232ACAE25EFEF5,
	LoadBalancingClient_OpRaiseEvent_m720894D48B5C0E9DAE00770B19521A9F1586C106,
	LoadBalancingClient_OpChangeGroups_mBC257BAFF6ADFA776D8B357477332E22AD0D65CB,
	LoadBalancingClient_ReadoutProperties_mD222637E6484F8FE82195F4DC66D07981C033C33,
	LoadBalancingClient_ReadoutPropertiesForActorNr_mAC576B0BBA3348B7B7675C7CFA6E96EDF2A70006,
	LoadBalancingClient_ChangeLocalID_mAC51D66CAD91AD689FC634E450ED0CE8E3429C41,
	LoadBalancingClient_GameEnteredOnGameServer_m1908BF738A090B0DD252326998F5A0E7E86300C2,
	LoadBalancingClient_UpdatedActorList_mA355582E01CBAE38175910FC738C4B9EA5C80D5D,
	LoadBalancingClient_CreatePlayer_m4C371647216BB95B08545C0B940913B9C2D6A3BB,
	LoadBalancingClient_CreateRoom_m2A44592E172B369A6540C607BE5594E042366393,
	LoadBalancingClient_CheckIfOpAllowedOnServer_mD2284A0C2FFEA7824E4E08FE62A3C33A1620DBC2,
	LoadBalancingClient_CheckIfOpCanBeSent_m1E0ACAE5106D3E8D10B03F96E857E3E98244D64D,
	LoadBalancingClient_CheckIfClientIsReadyToCallOperation_m618BC7E29479E88C6ED9F6809D770E4A941D7099,
	LoadBalancingClient_DebugReturn_m0E1FF29F13D3853B8A74D0427B660019FD10AB06,
	LoadBalancingClient_CallbackRoomEnterFailed_mDF2C50FFDD9B65C9C2C3E77C88099D606093A8BD,
	LoadBalancingClient_OnOperationResponse_mC849C966853D4423FAC2DE520EED481F2B35D7B0,
	LoadBalancingClient_OnStatusChanged_m1698C1AB10A7A509331280885909345CDA36D91E,
	LoadBalancingClient_OnEvent_mE53CF2903579547528ED4FD8311A50D6D47AE02A,
	LoadBalancingClient_OnMessage_m6D60BAA1FE0540E3BF0D7D0C73790B8637AF89B1,
	LoadBalancingClient_SetupEncryption_m9BBD4278BAC5CC5FEC3679A49439405600DE3E76,
	LoadBalancingClient_OpWebRpc_m767BB659C353F0C1FC540301D1F3EBB300236542,
	LoadBalancingClient_AddCallbackTarget_m17553A4C53D4BC96DF74BD3DFE8D8487E3B52565,
	LoadBalancingClient_RemoveCallbackTarget_mA166E163D02518ED621B80E8F07B31FFE79507A0,
	LoadBalancingClient_UpdateCallbackTargets_m4F0C56B00E0DC74B26294FA61A83186A710DBFFA,
	NULL,
	LoadBalancingClient__cctor_m852A3AC2587002191CFA49935C4C2769DCEC7DCD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ConnectionCallbacksContainer__ctor_m86519082BDB51074C0A08444C9D058F9DFECC174,
	ConnectionCallbacksContainer_OnConnected_m86DCC1AFF556E53DAC6104AAF966FD2283AF8B54,
	ConnectionCallbacksContainer_OnConnectedToMaster_mCA9F1A159894B869AD631CE53B2EFB2F1082C513,
	ConnectionCallbacksContainer_OnRegionListReceived_mA1598011E293574C634F3DD5B8F6D9E9036E8FC9,
	ConnectionCallbacksContainer_OnDisconnected_m4A6BE9270567815E0A07624DD92D8C56C3AF74CC,
	ConnectionCallbacksContainer_OnCustomAuthenticationResponse_mFB070A71F70D10E9F785B97C1109517499342FEA,
	ConnectionCallbacksContainer_OnCustomAuthenticationFailed_m6F71D63E9DF50E376B12BE5DDE4856BB80D27028,
	MatchMakingCallbacksContainer__ctor_m2DC6BFC48067B04691D90703F1B598AAB1C39D0D,
	MatchMakingCallbacksContainer_OnCreatedRoom_mBFE7022F4C9E6E03FD47DA8272D33A819CC8F6D5,
	MatchMakingCallbacksContainer_OnJoinedRoom_m5779B6F7EBBF9C65B8E59BC8C1799B30EAF9D348,
	MatchMakingCallbacksContainer_OnCreateRoomFailed_mA64F4F5D2E1EE8AF1C2AE80ED1D81121CBF8092C,
	MatchMakingCallbacksContainer_OnJoinRandomFailed_m1D7DC1D9BDE49CDB34B20CEDFF105A932030C87A,
	MatchMakingCallbacksContainer_OnJoinRoomFailed_mEAC9F4C37B2F04F228DC585FE90DF4C44464A9CD,
	MatchMakingCallbacksContainer_OnLeftRoom_m463DC02884ABEA72E6329D0FCF8F9F29DF8138C2,
	MatchMakingCallbacksContainer_OnFriendListUpdate_m0B78E17F0EB2E1038F8C6351D6D895DF510E413F,
	InRoomCallbacksContainer__ctor_mAB5D415C40517C66AFA4EE51FDE27BABB34CB1BC,
	InRoomCallbacksContainer_OnPlayerEnteredRoom_m390990124F7343BFEE2082780A788AD7F2F6FA3C,
	InRoomCallbacksContainer_OnPlayerLeftRoom_m118CCAC5C3B5BB1271F2ACAFBC61461F49772123,
	InRoomCallbacksContainer_OnRoomPropertiesUpdate_m2EC10EA23B6766DAE53B58C18D22E28281A22725,
	InRoomCallbacksContainer_OnPlayerPropertiesUpdate_mBF368118DFF6C61AC66CDE9AA250C4390922C2B8,
	InRoomCallbacksContainer_OnMasterClientSwitched_mDE73A982F91D03B67103CD7083734A37ED3CB07D,
	LobbyCallbacksContainer__ctor_m5EA40FFCF7EE793CD70AFCDF27EC96F8B3A546E3,
	LobbyCallbacksContainer_OnJoinedLobby_mE527C59A2BD5CBD6761A07F1B70BECC1EEA7EBF3,
	LobbyCallbacksContainer_OnLeftLobby_m89700E7E4FE1528CCA913969F5D122EB1481DD6B,
	LobbyCallbacksContainer_OnRoomListUpdate_m4CB513C7248881B9FC9A7D020608B2FE1B9736F3,
	LobbyCallbacksContainer_OnLobbyStatisticsUpdate_m3DB8D26C9441DBD8D9DA605F568E6821D0B3A053,
	WebRpcCallbacksContainer__ctor_mC0E65E12EA20C9F14D401D8F08FDB85E806176AF,
	WebRpcCallbacksContainer_OnWebRpcResponse_m358DF5A7DECB9AAF706A79C4B01ED99E9266EF6B,
	LoadBalancingPeer__ctor_mEBA1C7F13CEB9C2666A58E9324B104F3A079C421,
	LoadBalancingPeer__ctor_m6223CA6A845A305BE735E4D61716A06E33355C6F,
	LoadBalancingPeer_ConfigUnitySockets_m408D89E41EE7698B701CB80AB3FB5CA9CEC8BDD2,
	LoadBalancingPeer_OpGetRegions_m9F4665F4D56ED67248CBA86B21DE935C9B389CAD,
	LoadBalancingPeer_OpJoinLobby_m7793464CC70817DCF1C649D900206941E23EA1D6,
	LoadBalancingPeer_OpLeaveLobby_mA6F5BC8D94B95A77E9C4FE307C6A8A995D59CCA3,
	LoadBalancingPeer_RoomOptionsToOpParameters_m493A0A04572A343401C95AFDB825979A1E493022,
	LoadBalancingPeer_OpCreateRoom_m2F616AFF2B1236D7CE8D50692972483DD39533D6,
	LoadBalancingPeer_OpJoinRoom_m824B43FB7C5B63B573C4FD19731373D240A20A2D,
	LoadBalancingPeer_OpJoinRandomRoom_mA20F3B8E4FD9AC8336B87DFC59CC4D1444C426E0,
	LoadBalancingPeer_OpJoinRandomOrCreateRoom_m8BA07BB581495C5D23E021A7C41559018E6EFD83,
	LoadBalancingPeer_OpLeaveRoom_m75F3F8B5E8DF51903186AAE2CD98558E10512D90,
	LoadBalancingPeer_OpGetGameList_m3F3C7405CA812472E08707F930958C66F2440E3F,
	LoadBalancingPeer_OpFindFriends_m27C2BBE02BFDEA67B761195F3739EEAE29252B21,
	LoadBalancingPeer_OpSetCustomPropertiesOfActor_mDA09CE598C32EEAB561956DDBD6E76D1708C09E2,
	LoadBalancingPeer_OpSetPropertiesOfActor_mBD84D0B5F7D7523D0316E5BF517C9AC5D9EE12AE,
	LoadBalancingPeer_OpSetPropertyOfRoom_m9ABE7E024BDF6703B4233533866F2EF205E505D2,
	LoadBalancingPeer_OpSetCustomPropertiesOfRoom_m722E56A045F768E9BAD102B2D3BBB66100CA949B,
	LoadBalancingPeer_OpSetPropertiesOfRoom_m184256399D43209989DF89AD5A95DCBA46C99F79,
	LoadBalancingPeer_OpAuthenticate_mCD39661E2951112370B6A517FED9309059E1FD68,
	LoadBalancingPeer_OpAuthenticateOnce_m837376F726445B4B72F8B9FBB44CB61DADAB4BAE,
	LoadBalancingPeer_OpChangeGroups_mA6BC6B7D1F66608D35A38228525EBEEF767F4C95,
	LoadBalancingPeer_OpRaiseEvent_m94C3828C5AE0E3A23C9072BC33BCC75FA8E66DC1,
	LoadBalancingPeer_OpSettings_m696CF4ADA61882FB43470A158410CADD7E45DA57,
	LoadBalancingPeer__cctor_m15FF0270AF5AE76F8D8FB289C441E636F535EC75,
	FindFriendsOptions_ToIntFlags_m2F26BC5B39283350C3A2E7489F71E40BF8E7AEBE,
	FindFriendsOptions__ctor_mEDA35D63C83F547827AC02637FCFCBF0D1E543F1,
	OpJoinRandomRoomParams__ctor_m7EAF5F874DE8AFABDFF8DD4BF96CEBD695E26880,
	EnterRoomParams__ctor_mD79E1A3DA37FE25323DC2721FB0DFDC00D7F3EDE,
	ErrorCode__ctor_m76E0631999B20B536126A83C8F75EBF7BF5C219E,
	ActorProperties__ctor_mA0104A194D96AA6BC466E2CD41370572AF43DDF1,
	GamePropertyKey__ctor_m0DAB913601AD9E333243D10E454397BB55187EA2,
	EventCode__ctor_m6A1C78CC6506BA88D96DC46861DC0B3C74FCDB1E,
	ParameterCode__ctor_m7EDF2626AA921ACA7EE1B03D813F8219E15CB0BA,
	OperationCode__ctor_mDE1A1F05CBB3D4DE66EE837F430112E3849A4F26,
	RoomOptions_get_IsVisible_mEBC48A6E7D7A0422A0CEA3A4AD339EA8B7061976,
	RoomOptions_set_IsVisible_mC18F0091F40004E92A6B0D65DADE6B8ACE4FC4AC,
	RoomOptions_get_IsOpen_m63600C2FCC2227779869060317764624F9536147,
	RoomOptions_set_IsOpen_mB8FC22B596C9CD2534DB1F5911B352D9FC73F031,
	RoomOptions_get_CleanupCacheOnLeave_m5BC86AE25FB6DCE474527C5FDB7363F1212C120B,
	RoomOptions_set_CleanupCacheOnLeave_mC9C34F080C4EDFD2FF80D959CAEC057DC619D48B,
	RoomOptions_get_SuppressRoomEvents_mD4FCCD8AFF0DD842C04BA3D385FF0F9A128D1F50,
	RoomOptions_set_SuppressRoomEvents_mDE84DE82AACEB351CC93E17B781149D52E97DB1E,
	RoomOptions_get_PublishUserId_mEFF1EA9F5F3310DCED3A7BF9DE99174274DC5766,
	RoomOptions_set_PublishUserId_m2D2C26B98C031D132D3A7D450299BE55EBE2DE12,
	RoomOptions_get_DeleteNullProperties_m63EDCA05FFC2792D99F87A8BCB52F8D535236277,
	RoomOptions_set_DeleteNullProperties_mD834C5A7E29312EAEBDE491AD005FCD7C4983AF2,
	RoomOptions_get_BroadcastPropsChangeToAll_m148AB75D5E3A17B735D739B8C2217DA72774BE99,
	RoomOptions_set_BroadcastPropsChangeToAll_m79EAEFCFEB7472FDD91FD10152BB60F6A1F0C9E7,
	RoomOptions__ctor_m254044C6ACFBFED793E00A41DF8E76F953EE16CF,
	RaiseEventOptions__ctor_m566DA262F193D98A75A09C2A00007CD8472A4329,
	RaiseEventOptions__cctor_m0C865100F0214AB2184220C5B586BC98C80A92EB,
	TypedLobby_get_IsDefault_m9EABBD862010C30DC98DCFAC650D5DE378BDE77C,
	TypedLobby__ctor_m00923523872B0E50E15357CBBED9A08D5F944586,
	TypedLobby__ctor_mE5A5B745646CA0B8228EE2B6451B06BBE02D561D,
	TypedLobby_ToString_mA56D3E449966F9879512DB3400679974B80DB5AA,
	TypedLobby__cctor_m9A3C3621A62B5E941CBC71894D7318FA30EE84B7,
	TypedLobbyInfo_ToString_m5C7AB7E7308A8603657D1621D9AA98C027BD467C,
	TypedLobbyInfo__ctor_m608A819C4E7B3D755880CBBE88AD68B11447F76B,
	AuthenticationValues_get_AuthType_m7644167D1D9D4B1EB47E98BF5FDA05D1B3F1770A,
	AuthenticationValues_set_AuthType_mE9BBAB1E6FEFE22AE6318280C01AF83D0C96FDBB,
	AuthenticationValues_get_AuthGetParameters_m0D2F684B97C2814BFD84060C6A0CE55D49ACEB99,
	AuthenticationValues_set_AuthGetParameters_m62F77A15600CFD28F020C41EA6F853BC157BF6FE,
	AuthenticationValues_get_AuthPostData_mF020D81C8FB8E5DAC6D3593EDD6590F0F50B2588,
	AuthenticationValues_set_AuthPostData_m3AC15E1E1E29C1AE359C46DE691142CDF47820E5,
	AuthenticationValues_get_Token_mA7C6F8A1DC5CE286C5E3BF198A08A82D573285DF,
	AuthenticationValues_set_Token_m58AECA36241053F97241E0B3886BA8421ECA204C,
	AuthenticationValues_get_UserId_m04BBEC6A7AEFD6975DB9F49E659A7963F8D840DC,
	AuthenticationValues_set_UserId_mC1264E09EC8EE5FE8717275120E4104643969E5D,
	AuthenticationValues__ctor_mF5DC44DD388FE8648E072B765D5502C27D70FB8D,
	AuthenticationValues__ctor_m5887BFFDEFDF7055AD84B5F94F0BF5E09F6D8190,
	AuthenticationValues_SetAuthPostData_m8CD17481FF2EE2FC2A880926D5314DC498498F41,
	AuthenticationValues_SetAuthPostData_m92995794722C380DF97690DFEC803C871662B93F,
	AuthenticationValues_SetAuthPostData_mF7283839644EAECF797CF6D9054A431F03E7C8E1,
	AuthenticationValues_AddAuthParameter_m2A7C499EA88A7CA15235304CA2FF080F4E13609E,
	AuthenticationValues_ToString_m3D48D62AA7D57B8CF36E53C0E9A1AA8FABE9B858,
	PhotonPing_StartPing_m788B4EE59589E0D94D3AD208796B528AE4990411,
	PhotonPing_Done_m254644D4502B85A8A07E44BAA1E0F1B2C49FBFEB,
	PhotonPing_Dispose_m0156F3A8D813288DDF437B61584F1EE133A9EF10,
	PhotonPing_Init_m54A5083386B01DF1DC852147913AC351863A4382,
	PhotonPing__ctor_mECF370ED85CFB0BD665631E8703A986485C17E5E,
	PhotonPing__cctor_m56D1897D0278FABF7BCF7D5BA6A1B35629E237F7,
	PingMono_StartPing_m496A6971FE2DFBB3E2DE3B39EB0F32B12876BF20,
	PingMono_Done_mDE321FDDDA836C83A48624812C3E2EB267B6A4BC,
	PingMono_Dispose_m3B12B9C9C32DF1B54EEFE1C1F25763146F2DE756,
	PingMono__ctor_mD2498F5A127182A684E088A9C4CE268B878CF101,
	Player_get_RoomReference_m930E8CD16F6BABE612F24D6C66D20749F7E34184,
	Player_set_RoomReference_m94972048535DB91480569BC5E3B463A89CB12A91,
	Player_get_ActorNumber_m02157560C558C6007C8146F1FCDA7E759AD0CF1D,
	Player_get_NickName_mB7D3A3FD8493B2FFF9B0537194CE9C697473C2EE,
	Player_set_NickName_m3AEC143158F0AC02C3139CC0501F90CF8062A32F,
	Player_get_UserId_m1913731AD3876AA194CB56074FEBA28C6CB612E3,
	Player_set_UserId_m84AA51D6267A1CD7E621206B543AB25ECAC80657,
	Player_get_IsMasterClient_m54EECECB76D6BB178E5D7A75D32DEF78C3422695,
	Player_get_IsInactive_m565C1FB590912C70E7720C6E1EFFFD5EE755C403,
	Player_set_IsInactive_mE662074B1394644E061C34E341119FF9F6C53245,
	Player_get_CustomProperties_m74B9E54472730B0454EFB1B2DD6056CCEEF8D44A,
	Player_set_CustomProperties_m306F31B2BAD0A6BE834BA7DECC7DBCE5C15FC901,
	Player__ctor_m9BE0732C50CAC2F96D3ECDC5CB14BCD292E955CF,
	Player__ctor_m4AB41D4C395CFEBEF41DC51B084186880645B4D5,
	Player_Get_m4451719287EC83169424B9E50B85AAF385D9789D,
	Player_GetNext_m138BBE906F321BCEAA9C8E2B958C51E27343FE5E,
	Player_GetNextFor_mBB761E337EE5FB5FDCCB68CE7ABE00613C219007,
	Player_GetNextFor_m6FC25EC7D3F354FB4525BE0236B579D4B3A05821,
	Player_InternalCacheProperties_m00AB6BABAF7989DC84EC913E2E562BEF47D24621,
	Player_ToString_m12CA93AF311B38987A36D29A13B6E5ECF165D9A0,
	Player_ToStringFull_m3D8C720DEABC8F8104DC7C7B64831F80F740A926,
	Player_Equals_m20F6705EDCF31CA05EEE84209396E709019AC3DB,
	Player_GetHashCode_m0B65F82910CDF0C5C0BFAA356172255EC6F55EC1,
	Player_ChangeLocalID_m70BF740B95E44DBC724FE10C66335DCFE9FFE9B8,
	Player_SetCustomProperties_m1D0AAD4B5F3A58D51842BCBF54581592FDF55B50,
	Player_SetPlayerNameProperty_mCAB5BC326F55365FC7CF4CF811A94291E85AFE5A,
	Region_get_Code_m8669BE5070358182B467A85EFB042D25D5CDA3BC,
	Region_set_Code_mDE9D1C927F6333EC50C6BA5E32DF4F0E07D6ECE6,
	Region_get_Cluster_m850B9B953F6C87D089A2208F217662F8E3C90917,
	Region_set_Cluster_m989B1AE668634B2F4B1D2B065159CBA2FF026CA4,
	Region_get_HostAndPort_m087CC4E1545F622AF35114318203F5BA85F287AC,
	Region_set_HostAndPort_mA46DD23667655C529FE8F6D5F9AA469FA7268F30,
	Region_get_Ping_mC348787B425CB426446A25A8CFC45030AB71C94E,
	Region_set_Ping_m6D98DD720653EEBC8C0A1D8B4E79CF80669ABA5D,
	Region_get_WasPinged_mD0FFE35A28AD219017CD894E52B9F113BCF88A82,
	Region__ctor_m4A0A97C2052C8BD4259777A271C4F3DA5EA420E0,
	Region__ctor_mB1E557941DB9463D60C45866B68870FEE7CE12D0,
	Region_SetCodeAndCluster_mB935EFD0D57480D78B21E1539E82DED3C243AD36,
	Region_ToString_m662501AB15980CD061ACEF5508E37628BDD28F00,
	Region_ToString_m23EC241BD327ED9226F6E45B74FC433A4945F7E1,
	RegionHandler_get_EnabledRegions_m9293FC4BE35917E9BC9F9689E5CA9C27DA639701,
	RegionHandler_set_EnabledRegions_m2BBACC92EA8CE600EFDB5D847992D9E22FDBF8B3,
	RegionHandler_get_BestRegion_m6C04157AB2A06C9FD8253DB875326225D9F2A9C2,
	RegionHandler_get_SummaryToCache_mE53B11556105433EBE7C76137D7FAAE4B5C57415,
	RegionHandler_GetResults_mA141C10F13D5074C2FE740B8763C6C24832F1601,
	RegionHandler_SetRegions_m8C5DED02195914843CDD84A3777BA7A5253F8FBB,
	RegionHandler_get_IsPinging_m90810A16C9825D7B6EA68B026F700FCB65C095D8,
	RegionHandler_set_IsPinging_mB9A230BD4F3B2B2564252341BD931F47968FB1A1,
	RegionHandler_PingMinimumOfRegions_m5C8AE60A1AF4A9E06E3EA17A0B07C681FE455B17,
	RegionHandler_OnPreferredRegionPinged_m08CFBD385294B89DC35037B9EC36FB0309B6DA53,
	RegionHandler_PingEnabledRegions_m58091984F466221DA5073C828543FECC667DACF4,
	RegionHandler_OnRegionDone_m5416802428349BB3F28E925532C412A50DC3A172,
	RegionHandler__ctor_m97108E371414B468F72FAC6180DD4EC17E477D49,
	RegionPinger_get_Done_m2AA784BEAD23896BDC2D714D3C84CD1994469B0A,
	RegionPinger_set_Done_m3EFDA967730B0EEBC55AF5FE2C1F14D536D88AC2,
	RegionPinger__ctor_m2F25C93D7E6519E079AF5E3272004AF106E602E7,
	RegionPinger_GetPingImplementation_m063FEEB76623D94A4B074E0257DDC90C92AC800A,
	RegionPinger_Start_m7F7D855E24C252AB1BD68D035FBDFDF7989673A5,
	RegionPinger_RegionPingThreaded_m483F046E255AACD42F9AC1758F9CEEAEE1E2629B,
	RegionPinger_RegionPingCoroutine_mAA6EE986AB89E9B169021BD8698BDA9657AA5123,
	RegionPinger_GetResults_mE78DBC4D3180AE742CDB2B077E1E01E647BBDC07,
	RegionPinger_ResolveHost_m40FF2580A77B893BD7EF2A7B6A1C7B5713CB0CB0,
	RegionPinger__cctor_mB7CC8C974A4E6616D9682EB35BD32CBE3385E664,
	Room_get_LoadBalancingClient_m352E09ADBA5592B3FC0F1432CE9F5D7D0FACC52B,
	Room_set_LoadBalancingClient_mFA6EE0C996848CD1979DC014BDB9AAEF09B5728F,
	Room_get_Name_mCDF41719862596519BBE7091948656DF28B30607,
	Room_set_Name_mC4C4FFFFA614A20BFD543979EB818B9F332D77F9,
	Room_get_IsOffline_mED7792F1D0C59ED40BE6B0F30D133D5B718DE73A,
	Room_set_IsOffline_mB8087AC7DE2C5F438DE0EA52623EBEA97D6D9A01,
	Room_get_IsOpen_m36A93CB7C99FF6ABB9F77C8E9DCE793362DC0436,
	Room_set_IsOpen_m9BF3D27C133879A9EF88E4EEB553878DC88AB4A2,
	Room_get_IsVisible_m3C0E0466B3DFF449182378D88FA1D61251594838,
	Room_set_IsVisible_m5506840C476AFB8F2FF510C8A3320EC19042C010,
	Room_get_MaxPlayers_mD4BF3E0BF13AE675F4831A129E91D8F06D3EB124,
	Room_set_MaxPlayers_mA500CB2E9F2F4A3E498FA8707B1409247BE790EA,
	Room_get_PlayerCount_mCBF32BA967FF797861D5E9C2B26C3A6BDACA2E99,
	Room_get_Players_m8F39EA784277611D7051C8A37EE813FFF4BCDDD7,
	Room_set_Players_m0D5CFB1E82363D1AB270484990EE5D79D9811FB8,
	Room_get_ExpectedUsers_mF6928B3A5B8CD93A75882BDDB08A2E759D4C565F,
	Room_get_PlayerTtl_m9947D50F953F4C37988D5496A3E03336F27CE370,
	Room_set_PlayerTtl_mDC5CA1C7F2790DD26EDB6D8FD7FCFE723E60C55E,
	Room_get_EmptyRoomTtl_mB2AB26E021613CAE926EED6C14FE2CB92100CBF9,
	Room_set_EmptyRoomTtl_mA79AEB412F560A793F95EF26DDDFF53367D5E8FD,
	Room_get_MasterClientId_m1F799A66FDFD9D1BCB2F32B0F2327BF8CDF5075E,
	Room_get_PropertiesListedInLobby_mBCB796C319E1DEF67E4F914072668F4844FB053D,
	Room_set_PropertiesListedInLobby_m804175FB0D480F47629CA96F82DE6401DDF3F06F,
	Room_get_AutoCleanUp_m2BA5EC0F7D2A7ADAB38AB0FEB5B0A130A252B48F,
	Room__ctor_mD644FF2C795EC5BBD50438BAB949C2414A873CB6,
	Room_InternalCacheProperties_m0E12655175DE56072B673AF38947C13CB04509A1,
	Room_SetCustomProperties_m6E7842AF558D5148B4DF9AB5EF33827F9E9716EA,
	Room_SetPropertiesListedInLobby_m01DA90D05074665DCCE6F73419C93325FF5504A1,
	Room_RemovePlayer_m238395556BB7C376C19ADE84074EA4D3C043B215,
	Room_RemovePlayer_mA6BA0D249B52408DE1C1C4B5E870CB2A27BF3BF5,
	Room_SetMasterClient_m332177B7EA4EBF00CABB88FA984B651954CFA21C,
	Room_AddPlayer_m563868E24D9EF0C9DAE29389430F2F07F3FFB147,
	Room_StorePlayer_m12DAA94744AE461D74B16FDB01A71F6FEE3325CE,
	Room_GetPlayer_mFC75FEBE25AE023F5D8C4441EB6D168BF052BBD9,
	Room_ClearExpectedUsers_mD0908C38F02F79AED6461C32D8E8B344645E9068,
	Room_ToString_mEFDF611A6369048A0F0471689A43585AD19D76BB,
	Room_ToStringFull_m94133A3D76BCF79806886BBFCBEA1B6699BF0579,
	RoomInfo_get_CustomProperties_mA20AA5FAB69329282410561D4035F13132245AD0,
	RoomInfo_get_Name_m24C3CFCDD6E862D5E0EA27A0B2736E09D72843DA,
	RoomInfo_get_PlayerCount_m6E97FB80FDADD5A3B15ED2D51F071B3F343C1CC1,
	RoomInfo_set_PlayerCount_m406E55A69DFEB12E15AEB7B8DDF0B0C8F704911F,
	RoomInfo_get_MaxPlayers_mB393159F17C716B593EC71158399F415ED15CA51,
	RoomInfo_get_IsOpen_mB6999468BA5346E8D7C421DB3CDBA882C1288856,
	RoomInfo_get_IsVisible_mB12677A2EC41DE627005F79B86C3CEB9A0532491,
	RoomInfo__ctor_mFD07B7536EDF4DE7312D21661F123681D6581F2B,
	RoomInfo_Equals_mA1F83E98B362D4DAF441D42C360164E18057BECC,
	RoomInfo_GetHashCode_m86A2462634914114B25337548809456667A6240B,
	RoomInfo_ToString_m47D9484C51886F87648BA365109134FB53CD933A,
	RoomInfo_ToStringFull_mE676F4200194103F85618888DE0790156EF89768,
	RoomInfo_InternalCacheProperties_m41D47859544098977936BF3964D1E3FB7746C249,
	SupportLogger_get_Client_mB26343CDA21A09F3EA30AD740275EAFC083D52B7,
	SupportLogger_set_Client_m166B9D97ABDFAA4A8E7792F8C8861BE4C7193976,
	SupportLogger_Start_m1198B8A81F90188616E5942DCA38D0D79A8C7479,
	SupportLogger_OnApplicationPause_m394AE8B8288BFCE03DD16287100911E4D922E01B,
	SupportLogger_OnApplicationQuit_mF14FE14659F54814A80D0DDCA9A8FFEC532CEC81,
	SupportLogger_StartLogStats_m973A1BFAAB04C8F0F9568E1A281C6EE553384610,
	SupportLogger_StopLogStats_m5EA95B7B9DD9BEB62FCB674FBDED85DB910C3B06,
	SupportLogger_StartTrackValues_mCCA9284BFA342F2822E786C069773B61ED626F55,
	SupportLogger_StopTrackValues_m0806BA608466711C295BC23883AFAF6AEE153CB4,
	SupportLogger_GetFormattedTimestamp_m32FC37B0F0AE804113DD4C24DB3BE4AE6193D821,
	SupportLogger_TrackValues_m66D40C21DDFC4AA2988C44ACAF53AE3FE76FC39C,
	SupportLogger_LogStats_m420DC4631D59DCE0F7D33C3146C8688D08C54F94,
	SupportLogger_LogBasics_m1DD18EAF9701CABB4D230E939DC67A471B2C4B13,
	SupportLogger_OnConnected_mA89BCA9B3319032CF17DEB3BE76B87AA54C06C76,
	SupportLogger_OnConnectedToMaster_mF52A4381DCC7C6CE19F3451D539020E39B2A11C4,
	SupportLogger_OnFriendListUpdate_mB422AD317F908DDCA99272016652A88A22FBAF47,
	SupportLogger_OnJoinedLobby_m3F1EFC503B05CA5A2410C4B5FF08405E6A4ECB2C,
	SupportLogger_OnLeftLobby_m33D2D8A6305BA65C80658D8B0AC3DADB7FAEC419,
	SupportLogger_OnCreateRoomFailed_mE38FC7667E7002A91626809317BECD47D71FDCCF,
	SupportLogger_OnJoinedRoom_mB37EE4EAB942E5A6A8890C6FA4E1EB6740E16B82,
	SupportLogger_OnJoinRoomFailed_m3488EE58BF74437B2142A96EBF18DCB6B4B2C48C,
	SupportLogger_OnJoinRandomFailed_mAE5ECBF451C6E4FDFE2F85E0DC30F525319D12F0,
	SupportLogger_OnCreatedRoom_mF35AB68837976AC88555409F51F24C21C4A8C2F2,
	SupportLogger_OnLeftRoom_m05D5834FFADC1CCD590D7606819AA48399A3C76F,
	SupportLogger_OnDisconnected_mDE77493509B7A736D17258EABD0BD827843C9950,
	SupportLogger_OnRegionListReceived_mC0CEDEA1F7A9B6376FAF45AFEB99FE7F0B36A720,
	SupportLogger_OnRoomListUpdate_m8268A931B26BA87B2F0B52F6F21A02B76CF7E2F5,
	SupportLogger_OnPlayerEnteredRoom_m154F337E9EB056F73C076F87B3D79F7C60203933,
	SupportLogger_OnPlayerLeftRoom_m341B83040EF40500E395885DEE61E6EF45B7FD31,
	SupportLogger_OnRoomPropertiesUpdate_mB8A03017BE35E642DE3AC3C44F43D4746F1FB330,
	SupportLogger_OnPlayerPropertiesUpdate_m840D49D30EF7F8167EEA067779DAE434C778445C,
	SupportLogger_OnMasterClientSwitched_m3658AC3899549A5D3788B25BD338CDDA9C842F8D,
	SupportLogger_OnCustomAuthenticationResponse_mF5F844DB7163FEE64FB92021F75C644C44D2CF35,
	SupportLogger_OnCustomAuthenticationFailed_m63D26B6876F0FD3409FD609D7354B59DCA6B6648,
	SupportLogger_OnLobbyStatisticsUpdate_m9224062BC0172E2FE07283717A658EF01153EB68,
	SupportLogger__ctor_m17A4C242A97DE0BC726D88511103F1AB72B49653,
	WebRpcResponse_get_Name_m0B6E38116B73FE932B4809D1510013A2D0E75FEB,
	WebRpcResponse_set_Name_mEADC4EB6ED220410F817BB3A5864230A0215CA01,
	WebRpcResponse_get_ResultCode_mFC73DF1E0EF9021C8C3E53EC0EF548915B566886,
	WebRpcResponse_set_ResultCode_m8E4EE89F34D749CCFEA3B1D2D777A5A8D3FA33AA,
	WebRpcResponse_get_ReturnCode_mC2DB2985F6E1F2AB60A26975A4D7DC88C5D7562A,
	WebRpcResponse_get_Message_m9F817E8E921967871322B123F6C1BEF6F8BF5138,
	WebRpcResponse_set_Message_m0A4DB2E9F565AD93775CAE27C140A9DB4E411B78,
	WebRpcResponse_get_DebugMessage_mD4740D583D4CF227657519753D2145759B371714,
	WebRpcResponse_get_Parameters_m73734E2B75DC69FF24E5EBFE1C58383DB7F01969,
	WebRpcResponse_set_Parameters_mC93C08CE135944569FF91BC362C850E628E0563A,
	WebRpcResponse__ctor_m26AC599DEC9251A690AF67126B50C7A395E6E557,
	WebRpcResponse_ToStringFull_mD16B7B3F985553A86DC907BC3836B249EC84F203,
	WebFlags_get_HttpForward_m06B3FF0980DD3F3AC493ECF0897CFAFEB6101AB7,
	WebFlags_set_HttpForward_m5057520D45E0EE8B75146FF633C741C88E6DB1E5,
	WebFlags_get_SendAuthCookie_m4C3809BF83E26E6A0A0BBCF1B656FB420DB099C5,
	WebFlags_set_SendAuthCookie_m67D7AF57044292DA159A46AE8F4DC27F1DD3A46E,
	WebFlags_get_SendSync_m8E4500A08237E7ACF599CF7A77EF1F0B419D7FE2,
	WebFlags_set_SendSync_m1C9961C96558BF31ED185876EA55990A4F54CAE3,
	WebFlags_get_SendState_m7085E7394ECDB9CDC07BF0A4C2A0037E344076C5,
	WebFlags_set_SendState_m772DFC77F5169E3548302A9F601B87DC11765909,
	WebFlags__ctor_mF30D282AD9F1D728C06672D22FCB7E108BCB1993,
	WebFlags__cctor_m98EB282DAF4EDC93CC587315D2B06E55CB86DDE0,
	CallbackTargetChange__ctor_m071B8C0B4920F5D8508B702D0F378FF09FEA6AAF,
	U3CU3Ec__cctor_mE157ACC2760DCC7D615439E9D2BF9512AB689D75,
	U3CU3Ec__ctor_m4D4266CBFC2BBD4129FA851865400633B635659D,
	U3CU3Ec_U3Cget_BestRegionU3Eb__7_0_m19DF237C8A5595D6C2461E1F41D5CE1514798CF5,
	U3CU3Ec__DisplayClass20_0__ctor_m0BB82F56172BF13DA08595DF2153BF58EDF62985,
	U3CU3Ec__DisplayClass20_0_U3CPingMinimumOfRegionsU3Eb__0_m0AFA0CB1C83EE0A0704D4E20A922734556679FDE,
	U3CRegionPingCoroutineU3Ed__18__ctor_m1687C8F26AC6D6AB23A742557D443CAE7BC1694B,
	U3CRegionPingCoroutineU3Ed__18_System_IDisposable_Dispose_mE0FA56CBF47F36727542166ABB77803F24623733,
	U3CRegionPingCoroutineU3Ed__18_MoveNext_mAF2A7EDB1E021F98A41C5044ABF0B1B6DF712622,
	U3CRegionPingCoroutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EA0FF0C14DD701C64E5475B3AA4727FDC75FCF6,
	U3CRegionPingCoroutineU3Ed__18_System_Collections_IEnumerator_Reset_mAD2B9AE9A175BDBEF80DEA1CEB6C1D2732626627,
	U3CRegionPingCoroutineU3Ed__18_System_Collections_IEnumerator_get_Current_m63007C2C2FA0D2F9870974336AC6C7B7DCB42E0C,
};
static const int32_t s_InvokerIndices[470] = 
{
	89,
	89,
	89,
	89,
	14,
	23,
	14,
	26,
	10,
	32,
	89,
	23,
	23,
	23,
	23,
	23,
	89,
	23,
	142,
	142,
	0,
	-1,
	0,
	0,
	168,
	140,
	14,
	14,
	26,
	89,
	31,
	14,
	26,
	89,
	14,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	89,
	31,
	14,
	89,
	31,
	14,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	89,
	89,
	26,
	26,
	26,
	26,
	26,
	26,
	10,
	32,
	89,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	89,
	10,
	32,
	10,
	32,
	10,
	32,
	89,
	14,
	26,
	14,
	26,
	31,
	979,
	14,
	89,
	89,
	9,
	146,
	89,
	89,
	89,
	23,
	23,
	31,
	89,
	23,
	89,
	90,
	9,
	89,
	9,
	90,
	9,
	9,
	9,
	9,
	665,
	90,
	1742,
	1742,
	938,
	88,
	938,
	1743,
	90,
	120,
	58,
	32,
	26,
	26,
	488,
	105,
	664,
	1744,
	227,
	88,
	26,
	26,
	32,
	26,
	26,
	26,
	1678,
	26,
	26,
	23,
	-1,
	3,
	23,
	23,
	32,
	26,
	26,
	26,
	23,
	23,
	26,
	26,
	26,
	23,
	1115,
	23,
	1115,
	1115,
	23,
	26,
	26,
	26,
	27,
	26,
	26,
	26,
	26,
	23,
	23,
	26,
	32,
	26,
	26,
	26,
	23,
	23,
	1115,
	1115,
	1115,
	23,
	26,
	26,
	26,
	26,
	26,
	27,
	26,
	26,
	23,
	23,
	26,
	26,
	26,
	26,
	31,
	461,
	23,
	9,
	9,
	89,
	161,
	9,
	9,
	9,
	90,
	665,
	90,
	90,
	1745,
	1742,
	88,
	9,
	938,
	1746,
	1747,
	90,
	1743,
	227,
	3,
	10,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	23,
	23,
	3,
	89,
	23,
	461,
	14,
	3,
	14,
	23,
	89,
	31,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	26,
	26,
	26,
	26,
	27,
	14,
	9,
	89,
	23,
	23,
	23,
	3,
	9,
	89,
	23,
	23,
	14,
	26,
	10,
	14,
	26,
	14,
	26,
	89,
	89,
	31,
	14,
	26,
	365,
	1748,
	34,
	14,
	28,
	34,
	26,
	14,
	14,
	9,
	10,
	32,
	211,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	10,
	32,
	89,
	27,
	137,
	26,
	14,
	130,
	14,
	26,
	14,
	14,
	14,
	26,
	89,
	31,
	90,
	26,
	89,
	26,
	23,
	89,
	31,
	27,
	14,
	89,
	89,
	14,
	14,
	0,
	3,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	14,
	26,
	14,
	10,
	32,
	10,
	32,
	10,
	14,
	26,
	89,
	161,
	26,
	211,
	26,
	26,
	32,
	9,
	9,
	28,
	34,
	23,
	14,
	14,
	14,
	14,
	10,
	32,
	89,
	89,
	89,
	27,
	9,
	10,
	14,
	14,
	26,
	14,
	26,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	1115,
	23,
	1115,
	1115,
	23,
	23,
	32,
	26,
	26,
	26,
	26,
	26,
	27,
	26,
	26,
	26,
	26,
	23,
	14,
	26,
	10,
	32,
	10,
	14,
	26,
	14,
	14,
	26,
	26,
	14,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	31,
	3,
	461,
	3,
	23,
	41,
	23,
	9,
	32,
	23,
	89,
	14,
	23,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000016, { 0, 3 } },
	{ 0x06000094, { 3, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[6] = 
{
	{ (Il2CppRGCTXDataType)3, 15458 },
	{ (Il2CppRGCTXDataType)3, 15459 },
	{ (Il2CppRGCTXDataType)2, 19520 },
	{ (Il2CppRGCTXDataType)2, 19554 },
	{ (Il2CppRGCTXDataType)3, 15460 },
	{ (Il2CppRGCTXDataType)3, 15461 },
};
extern const Il2CppCodeGenModule g_PhotonRealtimeCodeGenModule;
const Il2CppCodeGenModule g_PhotonRealtimeCodeGenModule = 
{
	"PhotonRealtime.dll",
	470,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	6,
	s_rgctxValues,
	NULL,
};
