﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ChatGui
struct ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE;
// HealthBarScript
struct HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173;
// MotionChanger
struct MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317;
// Photon.Chat.ChatClient
struct ChatClient_t00238E132CA795D7F98C1DE6F433BB231F875505;
// Photon.Pun.Demo.Asteroids.AsteroidsGameManager
struct AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4;
// Photon.Pun.Demo.Asteroids.Spaceship
struct Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B;
// Photon.Pun.Demo.Cockpit.Forms.ConnectToRegionUIForm/OnSubmitEvent
struct OnSubmitEvent_t50178258F7E1A79F7B032E67B3C7662EC9B8BCD1;
// Photon.Pun.Demo.Cockpit.Forms.CreateRoomUiForm/OnSubmitEvent
struct OnSubmitEvent_tEAF5D32F65BE88AAD89EDE90BC06E2E0AB6B59B4;
// Photon.Pun.Demo.Cockpit.Forms.LoadLevelUIForm/OnSubmitEvent
struct OnSubmitEvent_t48121DCBCBFA2A2D4D2051513ACC05438A437ED7;
// Photon.Pun.Demo.Cockpit.Forms.SetRoomCustomPropertyUIForm/OnSubmitEvent
struct OnSubmitEvent_tCCDDD3E5DB3876A2D7BF8D930959AB47D6B785BC;
// Photon.Pun.Demo.Cockpit.Forms.UserIdUiForm/OnSubmitEvent
struct OnSubmitEvent_t49EB5BBF0581C3E26C318B266AB48B6E05E68AC3;
// Photon.Pun.Demo.Cockpit.PropertyListenerBase
struct PropertyListenerBase_t05B9504A6B7DFE170062320525A7A5F60D48E3CC;
// Photon.Pun.Demo.Cockpit.PunCockpitEmbed
struct PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802;
// Photon.Pun.Demo.Procedural.WorldGenerator
struct WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5;
// Photon.Pun.Demo.PunBasics.LoaderAnime
struct LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484;
// Photon.Pun.Demo.PunBasics.PlayerManager
struct PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144;
// Photon.Pun.Demo.SlotRacer.PlayerControl
struct PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7;
// Photon.Pun.Demo.SlotRacer.Utils.BezierControlPointMode[]
struct BezierControlPointModeU5BU5D_tA7D7CDA0CC3A3DDF4CBC7A2065FE5E0F8FEE2577;
// Photon.Pun.Demo.SlotRacer.Utils.BezierSpline
struct BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944;
// Photon.Pun.Demo.SlotRacer.Utils.SplinePosition[]
struct SplinePositionU5BU5D_tC56837B60736C57A39CA7B1860FBA12B124E3027;
// Photon.Pun.Demo.SlotRacer.Utils.SplineWalker
struct SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F;
// Photon.Pun.PhotonView
struct PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B;
// Photon.Pun.UtilityScripts.ConnectAndJoinRandom
struct ConnectAndJoinRandom_t0EDE1854CB4711DAAF466201EEDFEA7011DF3A51;
// Photon.Realtime.AppSettings
struct AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8;
// Photon.Realtime.ConnectionHandler
struct ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0;
// Photon.Realtime.LoadBalancingClient
struct LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A;
// Photon.Realtime.Player
struct Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202;
// StandardizedBow
struct StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA;
// StandardizedBowForHand
struct StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79;
// StandardizedBowForHandBool
struct StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB;
// StandardizedProjectile
struct StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439;
// StandardizedProjectileForHand
struct StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6;
// StandardizedProjectileForHandBool
struct StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_tFE4C6D62D20EAD754114F260D3F311D06EFBFDEF;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject>
struct Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2;
// System.Collections.Generic.Dictionary`2<System.String,FriendItem>
struct Dictionary_2_t031F0C3C4496EC540F1945D6B5DB1BCD9DEC6666;
// System.Collections.Generic.Dictionary`2<System.String,Photon.Pun.Demo.Hub.DemoHubManager/DemoData>
struct Dictionary_2_t63C3F023241A817BD21FF4EB21DF3EB77C69266E;
// System.Collections.Generic.Dictionary`2<System.String,Photon.Realtime.RoomInfo>
struct Dictionary_2_tFB2AFFFE62832819FFF0D2F94E3B61356799D6D0;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t32E4906D51ACD553523F806F7DF01E16E0EB56CC;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UI.Toggle>
struct Dictionary_2_tA865CEFD08C2FAFE3889E26D574C8A433D8CB08F;
// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit>
struct List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52;
// System.Collections.Generic.Queue`1<UnityEngine.GameObject>
struct Queue_1_t0069840E94B59EC80C3FA49EF6ECA3A067555290;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<System.String,System.String>
struct Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.BoxCollider
struct BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t2291ED59024631BF653D2880E0EE63EC7F27B58F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.Gradient
struct Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A;
// UnityEngine.ISubsystemDescriptor
struct ISubsystemDescriptor_t44607A9BE5154ED0C7924B81776A709724545E6C;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Dropdown
struct Dropdown_tF6331401084B1213CAB10587A6EC81461501930F;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.Selectable
struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.Toggle
struct Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.XR.ARFoundation.ARRaycastManager
struct ARRaycastManager_t81A9513150BA5BE536DF064F1C6DE73349A60BE7;
// UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/OnAsyncConversionCompleteDelegate
struct OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF;
// UnityEngine.XR.ARSubsystems.XRCameraSubsystem/IProvider
struct IProvider_t5D7A855308E0C5E8F9175CA7CDEE91F929A5185C;
// UnityEngine.XR.ARSubsystems.XRDepthSubsystem/IDepthApi
struct IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4;
// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/IProvider
struct IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469;
// UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/IProvider
struct IProvider_tB9DFB2CA3241B1F6107D01753C0F00628994A3ED;
// UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem/IProvider
struct IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459;
// UnityEngine.XR.ARSubsystems.XRSessionSubsystem/IProvider
struct IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428;
// UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44;
// UsernameIn
struct UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t2D35D876FED2BDD175BE36DB8283DBD59BA7B06E 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4 
{
public:

public:
};


// <Module>
struct  U3CModuleU3E_t8276593D2182AD0E8D12AF6BAFC4DCCD5C1DB6C1 
{
public:

public:
};


// System.Object


// MotionChanger_<WaitDelay>d__24
struct  U3CWaitDelayU3Ed__24_t31D6FD36E48161D4BBB5E499900605DF27437E35  : public RuntimeObject
{
public:
	// System.Int32 MotionChanger_<WaitDelay>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MotionChanger_<WaitDelay>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MotionChanger MotionChanger_<WaitDelay>d__24::<>4__this
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317 * ___U3CU3E4__this_2;
	// System.String MotionChanger_<WaitDelay>d__24::txt
	String_t* ___txt_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitDelayU3Ed__24_t31D6FD36E48161D4BBB5E499900605DF27437E35, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitDelayU3Ed__24_t31D6FD36E48161D4BBB5E499900605DF27437E35, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitDelayU3Ed__24_t31D6FD36E48161D4BBB5E499900605DF27437E35, ___U3CU3E4__this_2)); }
	inline MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_txt_3() { return static_cast<int32_t>(offsetof(U3CWaitDelayU3Ed__24_t31D6FD36E48161D4BBB5E499900605DF27437E35, ___txt_3)); }
	inline String_t* get_txt_3() const { return ___txt_3; }
	inline String_t** get_address_of_txt_3() { return &___txt_3; }
	inline void set_txt_3(String_t* value)
	{
		___txt_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txt_3), (void*)value);
	}
};


// MotionChanger_<waitSec>d__21
struct  U3CwaitSecU3Ed__21_tEC60F5EB702D7207105E2754AEBE6D2A50A720DD  : public RuntimeObject
{
public:
	// System.Int32 MotionChanger_<waitSec>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MotionChanger_<waitSec>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.GameObject MotionChanger_<waitSec>d__21::arrow
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___arrow_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CwaitSecU3Ed__21_tEC60F5EB702D7207105E2754AEBE6D2A50A720DD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CwaitSecU3Ed__21_tEC60F5EB702D7207105E2754AEBE6D2A50A720DD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_arrow_2() { return static_cast<int32_t>(offsetof(U3CwaitSecU3Ed__21_tEC60F5EB702D7207105E2754AEBE6D2A50A720DD, ___arrow_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_arrow_2() const { return ___arrow_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_arrow_2() { return &___arrow_2; }
	inline void set_arrow_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___arrow_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arrow_2), (void*)value);
	}
};


// Photon.Pun.Demo.Asteroids.AsteroidsGame
struct  AsteroidsGame_t6530B69A465669603E35076B157B2B3DB43B0B04  : public RuntimeObject
{
public:

public:
};


// Photon.Pun.Demo.Asteroids.AsteroidsGameManager_<EndOfGame>d__8
struct  U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3  : public RuntimeObject
{
public:
	// System.Int32 Photon.Pun.Demo.Asteroids.AsteroidsGameManager_<EndOfGame>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Photon.Pun.Demo.Asteroids.AsteroidsGameManager_<EndOfGame>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Photon.Pun.Demo.Asteroids.AsteroidsGameManager Photon.Pun.Demo.Asteroids.AsteroidsGameManager_<EndOfGame>d__8::<>4__this
	AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4 * ___U3CU3E4__this_2;
	// System.String Photon.Pun.Demo.Asteroids.AsteroidsGameManager_<EndOfGame>d__8::winner
	String_t* ___winner_3;
	// System.Int32 Photon.Pun.Demo.Asteroids.AsteroidsGameManager_<EndOfGame>d__8::score
	int32_t ___score_4;
	// System.Single Photon.Pun.Demo.Asteroids.AsteroidsGameManager_<EndOfGame>d__8::<timer>5__2
	float ___U3CtimerU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3, ___U3CU3E4__this_2)); }
	inline AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_winner_3() { return static_cast<int32_t>(offsetof(U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3, ___winner_3)); }
	inline String_t* get_winner_3() const { return ___winner_3; }
	inline String_t** get_address_of_winner_3() { return &___winner_3; }
	inline void set_winner_3(String_t* value)
	{
		___winner_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___winner_3), (void*)value);
	}

	inline static int32_t get_offset_of_score_4() { return static_cast<int32_t>(offsetof(U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3, ___score_4)); }
	inline int32_t get_score_4() const { return ___score_4; }
	inline int32_t* get_address_of_score_4() { return &___score_4; }
	inline void set_score_4(int32_t value)
	{
		___score_4 = value;
	}

	inline static int32_t get_offset_of_U3CtimerU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3, ___U3CtimerU3E5__2_5)); }
	inline float get_U3CtimerU3E5__2_5() const { return ___U3CtimerU3E5__2_5; }
	inline float* get_address_of_U3CtimerU3E5__2_5() { return &___U3CtimerU3E5__2_5; }
	inline void set_U3CtimerU3E5__2_5(float value)
	{
		___U3CtimerU3E5__2_5 = value;
	}
};


// Photon.Pun.Demo.Asteroids.AsteroidsGameManager_<SpawnAsteroid>d__7
struct  U3CSpawnAsteroidU3Ed__7_t065C186F41B6C91B1AC4D85D2AF96A225092CB1A  : public RuntimeObject
{
public:
	// System.Int32 Photon.Pun.Demo.Asteroids.AsteroidsGameManager_<SpawnAsteroid>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Photon.Pun.Demo.Asteroids.AsteroidsGameManager_<SpawnAsteroid>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSpawnAsteroidU3Ed__7_t065C186F41B6C91B1AC4D85D2AF96A225092CB1A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSpawnAsteroidU3Ed__7_t065C186F41B6C91B1AC4D85D2AF96A225092CB1A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}
};


// Photon.Pun.Demo.Asteroids.Spaceship_<WaitForRespawn>d__18
struct  U3CWaitForRespawnU3Ed__18_t2334CAF9C93C7E99B1990B947CBE4343351FC67F  : public RuntimeObject
{
public:
	// System.Int32 Photon.Pun.Demo.Asteroids.Spaceship_<WaitForRespawn>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Photon.Pun.Demo.Asteroids.Spaceship_<WaitForRespawn>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Photon.Pun.Demo.Asteroids.Spaceship Photon.Pun.Demo.Asteroids.Spaceship_<WaitForRespawn>d__18::<>4__this
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForRespawnU3Ed__18_t2334CAF9C93C7E99B1990B947CBE4343351FC67F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForRespawnU3Ed__18_t2334CAF9C93C7E99B1990B947CBE4343351FC67F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForRespawnU3Ed__18_t2334CAF9C93C7E99B1990B947CBE4343351FC67F, ___U3CU3E4__this_2)); }
	inline Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// Photon.Pun.Demo.Cockpit.Forms.CreateRoomUiForm_<>c
struct  U3CU3Ec_t4040D60D562F65BFD9DAF6076260B1106996B994  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4040D60D562F65BFD9DAF6076260B1106996B994_StaticFields
{
public:
	// Photon.Pun.Demo.Cockpit.Forms.CreateRoomUiForm_<>c Photon.Pun.Demo.Cockpit.Forms.CreateRoomUiForm_<>c::<>9
	U3CU3Ec_t4040D60D562F65BFD9DAF6076260B1106996B994 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.String> Photon.Pun.Demo.Cockpit.Forms.CreateRoomUiForm_<>c::<>9__8_0
	Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8 * ___U3CU3E9__8_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4040D60D562F65BFD9DAF6076260B1106996B994_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4040D60D562F65BFD9DAF6076260B1106996B994 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4040D60D562F65BFD9DAF6076260B1106996B994 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4040D60D562F65BFD9DAF6076260B1106996B994 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4040D60D562F65BFD9DAF6076260B1106996B994_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t5A3AAB17C588F6BC1455072E89FF46CB0A4A59E8 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__8_0_1), (void*)value);
	}
};


// Photon.Pun.Demo.Cockpit.PunCockpitEmbed_<Start>d__6
struct  U3CStartU3Ed__6_tE78D5702A9E0B439BA3EA81BC2C0ABBDE4960ABA  : public RuntimeObject
{
public:
	// System.Int32 Photon.Pun.Demo.Cockpit.PunCockpitEmbed_<Start>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Photon.Pun.Demo.Cockpit.PunCockpitEmbed_<Start>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Photon.Pun.Demo.Cockpit.PunCockpitEmbed Photon.Pun.Demo.Cockpit.PunCockpitEmbed_<Start>d__6::<>4__this
	PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__6_tE78D5702A9E0B439BA3EA81BC2C0ABBDE4960ABA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__6_tE78D5702A9E0B439BA3EA81BC2C0ABBDE4960ABA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__6_tE78D5702A9E0B439BA3EA81BC2C0ABBDE4960ABA, ___U3CU3E4__this_2)); }
	inline PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// Photon.Pun.Demo.Procedural.IngameControlPanel_<>c
struct  U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB_StaticFields
{
public:
	// Photon.Pun.Demo.Procedural.IngameControlPanel_<>c Photon.Pun.Demo.Procedural.IngameControlPanel_<>c::<>9
	U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB * ___U3CU3E9_0;
	// UnityEngine.Events.UnityAction`1<System.Int32> Photon.Pun.Demo.Procedural.IngameControlPanel_<>c::<>9__6_1
	UnityAction_1_t2291ED59024631BF653D2880E0EE63EC7F27B58F * ___U3CU3E9__6_1_1;
	// UnityEngine.Events.UnityAction`1<System.Int32> Photon.Pun.Demo.Procedural.IngameControlPanel_<>c::<>9__6_2
	UnityAction_1_t2291ED59024631BF653D2880E0EE63EC7F27B58F * ___U3CU3E9__6_2_2;
	// UnityEngine.Events.UnityAction`1<System.Int32> Photon.Pun.Demo.Procedural.IngameControlPanel_<>c::<>9__6_3
	UnityAction_1_t2291ED59024631BF653D2880E0EE63EC7F27B58F * ___U3CU3E9__6_3_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB_StaticFields, ___U3CU3E9__6_1_1)); }
	inline UnityAction_1_t2291ED59024631BF653D2880E0EE63EC7F27B58F * get_U3CU3E9__6_1_1() const { return ___U3CU3E9__6_1_1; }
	inline UnityAction_1_t2291ED59024631BF653D2880E0EE63EC7F27B58F ** get_address_of_U3CU3E9__6_1_1() { return &___U3CU3E9__6_1_1; }
	inline void set_U3CU3E9__6_1_1(UnityAction_1_t2291ED59024631BF653D2880E0EE63EC7F27B58F * value)
	{
		___U3CU3E9__6_1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_1_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB_StaticFields, ___U3CU3E9__6_2_2)); }
	inline UnityAction_1_t2291ED59024631BF653D2880E0EE63EC7F27B58F * get_U3CU3E9__6_2_2() const { return ___U3CU3E9__6_2_2; }
	inline UnityAction_1_t2291ED59024631BF653D2880E0EE63EC7F27B58F ** get_address_of_U3CU3E9__6_2_2() { return &___U3CU3E9__6_2_2; }
	inline void set_U3CU3E9__6_2_2(UnityAction_1_t2291ED59024631BF653D2880E0EE63EC7F27B58F * value)
	{
		___U3CU3E9__6_2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_2_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_3_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB_StaticFields, ___U3CU3E9__6_3_3)); }
	inline UnityAction_1_t2291ED59024631BF653D2880E0EE63EC7F27B58F * get_U3CU3E9__6_3_3() const { return ___U3CU3E9__6_3_3; }
	inline UnityAction_1_t2291ED59024631BF653D2880E0EE63EC7F27B58F ** get_address_of_U3CU3E9__6_3_3() { return &___U3CU3E9__6_3_3; }
	inline void set_U3CU3E9__6_3_3(UnityAction_1_t2291ED59024631BF653D2880E0EE63EC7F27B58F * value)
	{
		___U3CU3E9__6_3_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_3_3), (void*)value);
	}
};


// Photon.Pun.Demo.SlotRacer.PlayerControl_<Start>d__12
struct  U3CStartU3Ed__12_tDFCE7CD2DA1B2E6DFA1A68E4D9E7440F320E25C4  : public RuntimeObject
{
public:
	// System.Int32 Photon.Pun.Demo.SlotRacer.PlayerControl_<Start>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Photon.Pun.Demo.SlotRacer.PlayerControl_<Start>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Photon.Pun.Demo.SlotRacer.PlayerControl Photon.Pun.Demo.SlotRacer.PlayerControl_<Start>d__12::<>4__this
	PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__12_tDFCE7CD2DA1B2E6DFA1A68E4D9E7440F320E25C4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__12_tDFCE7CD2DA1B2E6DFA1A68E4D9E7440F320E25C4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__12_tDFCE7CD2DA1B2E6DFA1A68E4D9E7440F320E25C4, ___U3CU3E4__this_2)); }
	inline PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// Photon.Pun.Demo.SlotRacer.Utils.Bezier
struct  Bezier_t8F0CCEB73F1B9673BFC3D9808D50F8F336C148D8  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// UnityEngine.Subsystem
struct  Subsystem_t6B987736D8E48098F860AC55D76905DE1F48CE8C  : public RuntimeObject
{
public:
	// UnityEngine.ISubsystemDescriptor UnityEngine.Subsystem::m_subsystemDescriptor
	RuntimeObject* ___m_subsystemDescriptor_0;

public:
	inline static int32_t get_offset_of_m_subsystemDescriptor_0() { return static_cast<int32_t>(offsetof(Subsystem_t6B987736D8E48098F860AC55D76905DE1F48CE8C, ___m_subsystemDescriptor_0)); }
	inline RuntimeObject* get_m_subsystemDescriptor_0() const { return ___m_subsystemDescriptor_0; }
	inline RuntimeObject** get_address_of_m_subsystemDescriptor_0() { return &___m_subsystemDescriptor_0; }
	inline void set_m_subsystemDescriptor_0(RuntimeObject* value)
	{
		___m_subsystemDescriptor_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_subsystemDescriptor_0), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARKitCameraSubsystem_NativeApi
struct  NativeApi_tAC74EBF9B7EAB0504916300254C661F63CF9173A  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitRaycastSubsystem_NativeApi
struct  NativeApi_tB7917295BEDEB8E60FC3C8181CFDEB126B4DDB2A  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi
struct  NativeApi_t164DECAC3F6004936824870871CC817A16AC9050  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARWorldMapRequestStatusExtensions
struct  ARWorldMapRequestStatusExtensions_t8FC86F2BC224C9CCC808FF3B4610B22BE5F8051F  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.Api
struct  Api_t4000D9F2E2A2012E34CA6CA8B3EA3E0BF565182C  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRCameraSubsystem_IProvider
struct  IProvider_t5D7A855308E0C5E8F9175CA7CDEE91F929A5185C  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRDepthSubsystem_IDepthApi
struct  IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_IProvider
struct  IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_IProvider
struct  IProvider_tB9DFB2CA3241B1F6107D01753C0F00628994A3ED  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_IProvider
struct  IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRSessionSubsystem_IProvider
struct  IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428  : public RuntimeObject
{
public:

public:
};


// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D512
struct  __StaticArrayInitTypeSizeU3D512_t9F653113C990E176B6E55C042E7786EDD02BD76E 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D512_t9F653113C990E176B6E55C042E7786EDD02BD76E__padding[512];
	};

public:
};


// Photon.Pun.Demo.Hub.DemoHubManager_DemoData
struct  DemoData_t459727E1190C57859B1586322996ECC573894F80 
{
public:
	// System.String Photon.Pun.Demo.Hub.DemoHubManager_DemoData::Title
	String_t* ___Title_0;
	// System.String Photon.Pun.Demo.Hub.DemoHubManager_DemoData::Description
	String_t* ___Description_1;
	// System.String Photon.Pun.Demo.Hub.DemoHubManager_DemoData::Scene
	String_t* ___Scene_2;
	// System.String Photon.Pun.Demo.Hub.DemoHubManager_DemoData::TutorialLink
	String_t* ___TutorialLink_3;
	// System.String Photon.Pun.Demo.Hub.DemoHubManager_DemoData::DocLink
	String_t* ___DocLink_4;

public:
	inline static int32_t get_offset_of_Title_0() { return static_cast<int32_t>(offsetof(DemoData_t459727E1190C57859B1586322996ECC573894F80, ___Title_0)); }
	inline String_t* get_Title_0() const { return ___Title_0; }
	inline String_t** get_address_of_Title_0() { return &___Title_0; }
	inline void set_Title_0(String_t* value)
	{
		___Title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Title_0), (void*)value);
	}

	inline static int32_t get_offset_of_Description_1() { return static_cast<int32_t>(offsetof(DemoData_t459727E1190C57859B1586322996ECC573894F80, ___Description_1)); }
	inline String_t* get_Description_1() const { return ___Description_1; }
	inline String_t** get_address_of_Description_1() { return &___Description_1; }
	inline void set_Description_1(String_t* value)
	{
		___Description_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Description_1), (void*)value);
	}

	inline static int32_t get_offset_of_Scene_2() { return static_cast<int32_t>(offsetof(DemoData_t459727E1190C57859B1586322996ECC573894F80, ___Scene_2)); }
	inline String_t* get_Scene_2() const { return ___Scene_2; }
	inline String_t** get_address_of_Scene_2() { return &___Scene_2; }
	inline void set_Scene_2(String_t* value)
	{
		___Scene_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Scene_2), (void*)value);
	}

	inline static int32_t get_offset_of_TutorialLink_3() { return static_cast<int32_t>(offsetof(DemoData_t459727E1190C57859B1586322996ECC573894F80, ___TutorialLink_3)); }
	inline String_t* get_TutorialLink_3() const { return ___TutorialLink_3; }
	inline String_t** get_address_of_TutorialLink_3() { return &___TutorialLink_3; }
	inline void set_TutorialLink_3(String_t* value)
	{
		___TutorialLink_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TutorialLink_3), (void*)value);
	}

	inline static int32_t get_offset_of_DocLink_4() { return static_cast<int32_t>(offsetof(DemoData_t459727E1190C57859B1586322996ECC573894F80, ___DocLink_4)); }
	inline String_t* get_DocLink_4() const { return ___DocLink_4; }
	inline String_t** get_address_of_DocLink_4() { return &___DocLink_4; }
	inline void set_DocLink_4(String_t* value)
	{
		___DocLink_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DocLink_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Photon.Pun.Demo.Hub.DemoHubManager/DemoData
struct DemoData_t459727E1190C57859B1586322996ECC573894F80_marshaled_pinvoke
{
	char* ___Title_0;
	char* ___Description_1;
	char* ___Scene_2;
	char* ___TutorialLink_3;
	char* ___DocLink_4;
};
// Native definition for COM marshalling of Photon.Pun.Demo.Hub.DemoHubManager/DemoData
struct DemoData_t459727E1190C57859B1586322996ECC573894F80_marshaled_com
{
	Il2CppChar* ___Title_0;
	Il2CppChar* ___Description_1;
	Il2CppChar* ___Scene_2;
	Il2CppChar* ___TutorialLink_3;
	Il2CppChar* ___DocLink_4;
};

// System.Collections.Generic.Dictionary`2_ValueCollection_Enumerator<System.Int32,UnityEngine.GameObject>
struct  Enumerator_t65C6440C8634D06850057DFEFFC82BE6B73001F1 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2_ValueCollection_Enumerator::dictionary
	Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2_ValueCollection_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2_ValueCollection_Enumerator::version
	int32_t ___version_2;
	// TValue System.Collections.Generic.Dictionary`2_ValueCollection_Enumerator::currentValue
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___currentValue_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t65C6440C8634D06850057DFEFFC82BE6B73001F1, ___dictionary_0)); }
	inline Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t65C6440C8634D06850057DFEFFC82BE6B73001F1, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t65C6440C8634D06850057DFEFFC82BE6B73001F1, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_currentValue_3() { return static_cast<int32_t>(offsetof(Enumerator_t65C6440C8634D06850057DFEFFC82BE6B73001F1, ___currentValue_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_currentValue_3() const { return ___currentValue_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_currentValue_3() { return &___currentValue_3; }
	inline void set_currentValue_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___currentValue_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentValue_3), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.AnimatorStateInfo
struct  AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t890F45761F13DD1B3D58738365827FDB6629BA7F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t890F45761F13DD1B3D58738365827FDB6629BA7F, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`4<System.String,System.String,Photon.Realtime.LobbyType,System.String[]>
struct  UnityEvent_4_t1396E915D33D287DF3B74215C9F26DA35CD9C2A7  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`4::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_4_t1396E915D33D287DF3B74215C9F26DA35CD9C2A7, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Subsystem`1<UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor>
struct  Subsystem_1_t54655F7C7F723FDA59325E1AC875A3FD66C9C684  : public Subsystem_t6B987736D8E48098F860AC55D76905DE1F48CE8C
{
public:

public:
};


// UnityEngine.Subsystem`1<UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor>
struct  Subsystem_1_t699EE52FD744695EFB393B312D6BFA04DCC68870  : public Subsystem_t6B987736D8E48098F860AC55D76905DE1F48CE8C
{
public:

public:
};


// UnityEngine.Subsystem`1<UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor>
struct  Subsystem_1_tA832EBA21E41FF7EBCA30AE7C32EC78033F63E37  : public Subsystem_t6B987736D8E48098F860AC55D76905DE1F48CE8C
{
public:

public:
};


// UnityEngine.Subsystem`1<UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor>
struct  Subsystem_1_t1D44399B8190A9E3533EE5C5D5915B75D79B2E66  : public Subsystem_t6B987736D8E48098F860AC55D76905DE1F48CE8C
{
public:

public:
};


// UnityEngine.Subsystem`1<UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor>
struct  Subsystem_1_t1A38EF1402A7E5DBC48E0755E358926BB0054890  : public Subsystem_t6B987736D8E48098F860AC55D76905DE1F48CE8C
{
public:

public:
};


// UnityEngine.Subsystem`1<UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor>
struct  Subsystem_1_t749909AA5D71BD615BDF40BDE7C8F81B8D63F4C7  : public Subsystem_t6B987736D8E48098F860AC55D76905DE1F48CE8C
{
public:

public:
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitCameraSubsystem_Provider
struct  Provider_t10F5BFB8A0883F5ABFC689F55DC4B6DFA0E9535E  : public IProvider_t5D7A855308E0C5E8F9175CA7CDEE91F929A5185C
{
public:

public:
};

struct Provider_t10F5BFB8A0883F5ABFC689F55DC4B6DFA0E9535E_StaticFields
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARKitCameraSubsystem_Provider::k_TextureYPropertyNameId
	int32_t ___k_TextureYPropertyNameId_0;
	// System.Int32 UnityEngine.XR.ARKit.ARKitCameraSubsystem_Provider::k_TextureCbCrPropertyNameId
	int32_t ___k_TextureCbCrPropertyNameId_1;

public:
	inline static int32_t get_offset_of_k_TextureYPropertyNameId_0() { return static_cast<int32_t>(offsetof(Provider_t10F5BFB8A0883F5ABFC689F55DC4B6DFA0E9535E_StaticFields, ___k_TextureYPropertyNameId_0)); }
	inline int32_t get_k_TextureYPropertyNameId_0() const { return ___k_TextureYPropertyNameId_0; }
	inline int32_t* get_address_of_k_TextureYPropertyNameId_0() { return &___k_TextureYPropertyNameId_0; }
	inline void set_k_TextureYPropertyNameId_0(int32_t value)
	{
		___k_TextureYPropertyNameId_0 = value;
	}

	inline static int32_t get_offset_of_k_TextureCbCrPropertyNameId_1() { return static_cast<int32_t>(offsetof(Provider_t10F5BFB8A0883F5ABFC689F55DC4B6DFA0E9535E_StaticFields, ___k_TextureCbCrPropertyNameId_1)); }
	inline int32_t get_k_TextureCbCrPropertyNameId_1() const { return ___k_TextureCbCrPropertyNameId_1; }
	inline int32_t* get_address_of_k_TextureCbCrPropertyNameId_1() { return &___k_TextureCbCrPropertyNameId_1; }
	inline void set_k_TextureCbCrPropertyNameId_1(int32_t value)
	{
		___k_TextureCbCrPropertyNameId_1 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitRaycastSubsystem_Provider
struct  Provider_tC61D3306173539ECD1E3F0E7184408EA855AA0B3  : public IProvider_tB9DFB2CA3241B1F6107D01753C0F00628994A3ED
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitReferencePointSubsystem_Provider
struct  Provider_t509F1B75CECA94E702BEAA9F7C67EA2313C58986  : public IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem_Provider
struct  Provider_t475F303CC6F0955D8B266D4CD5B7022F3658389B  : public IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_Provider
struct  Provider_t38A44526F6D5F1EBFDA048E10377C8F3FA82EF70  : public IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_Provider
struct  Provider_tE828C43D91B7E57F44E04A10F068C304DBAE5A6A  : public IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469
{
public:

public:
};


// UnityEngine.XR.ARKit.ARWorldMap
struct  ARWorldMap_t8BAE5D083A023D7DD23C29E4082B6BBD329010DE 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMap::<nativeHandle>k__BackingField
	int32_t ___U3CnativeHandleU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnativeHandleU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ARWorldMap_t8BAE5D083A023D7DD23C29E4082B6BBD329010DE, ___U3CnativeHandleU3Ek__BackingField_0)); }
	inline int32_t get_U3CnativeHandleU3Ek__BackingField_0() const { return ___U3CnativeHandleU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CnativeHandleU3Ek__BackingField_0() { return &___U3CnativeHandleU3Ek__BackingField_0; }
	inline void set_U3CnativeHandleU3Ek__BackingField_0(int32_t value)
	{
		___U3CnativeHandleU3Ek__BackingField_0 = value;
	}
};


// UnityEngine.XR.ARSubsystems.TrackableId
struct  TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 
{
public:
	// System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::m_SubId1
	uint64_t ___m_SubId1_1;
	// System.UInt64 UnityEngine.XR.ARSubsystems.TrackableId::m_SubId2
	uint64_t ___m_SubId2_2;

public:
	inline static int32_t get_offset_of_m_SubId1_1() { return static_cast<int32_t>(offsetof(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47, ___m_SubId1_1)); }
	inline uint64_t get_m_SubId1_1() const { return ___m_SubId1_1; }
	inline uint64_t* get_address_of_m_SubId1_1() { return &___m_SubId1_1; }
	inline void set_m_SubId1_1(uint64_t value)
	{
		___m_SubId1_1 = value;
	}

	inline static int32_t get_offset_of_m_SubId2_2() { return static_cast<int32_t>(offsetof(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47, ___m_SubId2_2)); }
	inline uint64_t get_m_SubId2_2() const { return ___m_SubId2_2; }
	inline uint64_t* get_address_of_m_SubId2_2() { return &___m_SubId2_2; }
	inline void set_m_SubId2_2(uint64_t value)
	{
		___m_SubId2_2 = value;
	}
};

struct TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_StaticFields
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.TrackableId::s_InvalidId
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___s_InvalidId_0;

public:
	inline static int32_t get_offset_of_s_InvalidId_0() { return static_cast<int32_t>(offsetof(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47_StaticFields, ___s_InvalidId_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_s_InvalidId_0() const { return ___s_InvalidId_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_s_InvalidId_0() { return &___s_InvalidId_0; }
	inline void set_s_InvalidId_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___s_InvalidId_0 = value;
	}
};


// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1DF3A1BCDFE891F17702C1ED79E71D854AEBD2AD  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1DF3A1BCDFE891F17702C1ED79E71D854AEBD2AD_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D512 <PrivateImplementationDetails>::E74588860CA220F5327520E16546CBB6016903F4
	__StaticArrayInitTypeSizeU3D512_t9F653113C990E176B6E55C042E7786EDD02BD76E  ___E74588860CA220F5327520E16546CBB6016903F4_0;

public:
	inline static int32_t get_offset_of_E74588860CA220F5327520E16546CBB6016903F4_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1DF3A1BCDFE891F17702C1ED79E71D854AEBD2AD_StaticFields, ___E74588860CA220F5327520E16546CBB6016903F4_0)); }
	inline __StaticArrayInitTypeSizeU3D512_t9F653113C990E176B6E55C042E7786EDD02BD76E  get_E74588860CA220F5327520E16546CBB6016903F4_0() const { return ___E74588860CA220F5327520E16546CBB6016903F4_0; }
	inline __StaticArrayInitTypeSizeU3D512_t9F653113C990E176B6E55C042E7786EDD02BD76E * get_address_of_E74588860CA220F5327520E16546CBB6016903F4_0() { return &___E74588860CA220F5327520E16546CBB6016903F4_0; }
	inline void set_E74588860CA220F5327520E16546CBB6016903F4_0(__StaticArrayInitTypeSizeU3D512_t9F653113C990E176B6E55C042E7786EDD02BD76E  value)
	{
		___E74588860CA220F5327520E16546CBB6016903F4_0 = value;
	}
};


// Photon.Pun.Demo.Cockpit.Forms.ConnectToRegionUIForm_OnSubmitEvent
struct  OnSubmitEvent_t50178258F7E1A79F7B032E67B3C7662EC9B8BCD1  : public UnityEvent_1_t890F45761F13DD1B3D58738365827FDB6629BA7F
{
public:

public:
};


// Photon.Pun.Demo.Cockpit.Forms.CreateRoomUiForm_OnSubmitEvent
struct  OnSubmitEvent_tEAF5D32F65BE88AAD89EDE90BC06E2E0AB6B59B4  : public UnityEvent_4_t1396E915D33D287DF3B74215C9F26DA35CD9C2A7
{
public:

public:
};


// Photon.Pun.Demo.Cockpit.Forms.LoadLevelUIForm_OnSubmitEvent
struct  OnSubmitEvent_t48121DCBCBFA2A2D4D2051513ACC05438A437ED7  : public UnityEvent_1_t890F45761F13DD1B3D58738365827FDB6629BA7F
{
public:

public:
};


// Photon.Pun.Demo.Cockpit.Forms.SetRoomCustomPropertyUIForm_OnSubmitEvent
struct  OnSubmitEvent_tCCDDD3E5DB3876A2D7BF8D930959AB47D6B785BC  : public UnityEvent_1_t890F45761F13DD1B3D58738365827FDB6629BA7F
{
public:

public:
};


// Photon.Pun.Demo.Cockpit.Forms.UserIdUiForm_OnSubmitEvent
struct  OnSubmitEvent_t49EB5BBF0581C3E26C318B266AB48B6E05E68AC3  : public UnityEvent_1_t890F45761F13DD1B3D58738365827FDB6629BA7F
{
public:

public:
};


// Photon.Pun.Demo.Cockpit.PropertyListenerBase_<FadeOut>d__4
struct  U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC  : public RuntimeObject
{
public:
	// System.Int32 Photon.Pun.Demo.Cockpit.PropertyListenerBase_<FadeOut>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Photon.Pun.Demo.Cockpit.PropertyListenerBase_<FadeOut>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.UI.Graphic Photon.Pun.Demo.Cockpit.PropertyListenerBase_<FadeOut>d__4::image
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___image_2;
	// Photon.Pun.Demo.Cockpit.PropertyListenerBase Photon.Pun.Demo.Cockpit.PropertyListenerBase_<FadeOut>d__4::<>4__this
	PropertyListenerBase_t05B9504A6B7DFE170062320525A7A5F60D48E3CC * ___U3CU3E4__this_3;
	// System.Single Photon.Pun.Demo.Cockpit.PropertyListenerBase_<FadeOut>d__4::<elapsedTime>5__2
	float ___U3CelapsedTimeU3E5__2_4;
	// UnityEngine.Color Photon.Pun.Demo.Cockpit.PropertyListenerBase_<FadeOut>d__4::<c>5__3
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___U3CcU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_image_2() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC, ___image_2)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_image_2() const { return ___image_2; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_image_2() { return &___image_2; }
	inline void set_image_2(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___image_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___image_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC, ___U3CU3E4__this_3)); }
	inline PropertyListenerBase_t05B9504A6B7DFE170062320525A7A5F60D48E3CC * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline PropertyListenerBase_t05B9504A6B7DFE170062320525A7A5F60D48E3CC ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(PropertyListenerBase_t05B9504A6B7DFE170062320525A7A5F60D48E3CC * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC, ___U3CelapsedTimeU3E5__2_4)); }
	inline float get_U3CelapsedTimeU3E5__2_4() const { return ___U3CelapsedTimeU3E5__2_4; }
	inline float* get_address_of_U3CelapsedTimeU3E5__2_4() { return &___U3CelapsedTimeU3E5__2_4; }
	inline void set_U3CelapsedTimeU3E5__2_4(float value)
	{
		___U3CelapsedTimeU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CcU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC, ___U3CcU3E5__3_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_U3CcU3E5__3_5() const { return ___U3CcU3E5__3_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_U3CcU3E5__3_5() { return &___U3CcU3E5__3_5; }
	inline void set_U3CcU3E5__3_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___U3CcU3E5__3_5 = value;
	}
};


// Photon.Pun.Demo.Procedural.ClusterSize
struct  ClusterSize_t4368167F200E0194C0CE779F113528A4AAA3FCDB 
{
public:
	// System.Int32 Photon.Pun.Demo.Procedural.ClusterSize::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClusterSize_t4368167F200E0194C0CE779F113528A4AAA3FCDB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Pun.Demo.Procedural.WorldGenerator_<GenerateWorld>d__31
struct  U3CGenerateWorldU3Ed__31_t820A15AAC146A12428D1F4559D0391DFE2947A9D  : public RuntimeObject
{
public:
	// System.Int32 Photon.Pun.Demo.Procedural.WorldGenerator_<GenerateWorld>d__31::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Photon.Pun.Demo.Procedural.WorldGenerator_<GenerateWorld>d__31::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Photon.Pun.Demo.Procedural.WorldGenerator Photon.Pun.Demo.Procedural.WorldGenerator_<GenerateWorld>d__31::<>4__this
	WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5 * ___U3CU3E4__this_2;
	// System.Collections.Generic.Dictionary`2_ValueCollection_Enumerator<System.Int32,UnityEngine.GameObject> Photon.Pun.Demo.Procedural.WorldGenerator_<GenerateWorld>d__31::<>7__wrap1
	Enumerator_t65C6440C8634D06850057DFEFFC82BE6B73001F1  ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGenerateWorldU3Ed__31_t820A15AAC146A12428D1F4559D0391DFE2947A9D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGenerateWorldU3Ed__31_t820A15AAC146A12428D1F4559D0391DFE2947A9D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGenerateWorldU3Ed__31_t820A15AAC146A12428D1F4559D0391DFE2947A9D, ___U3CU3E4__this_2)); }
	inline WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGenerateWorldU3Ed__31_t820A15AAC146A12428D1F4559D0391DFE2947A9D, ___U3CU3E7__wrap1_3)); }
	inline Enumerator_t65C6440C8634D06850057DFEFFC82BE6B73001F1  get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline Enumerator_t65C6440C8634D06850057DFEFFC82BE6B73001F1 * get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(Enumerator_t65C6440C8634D06850057DFEFFC82BE6B73001F1  value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E7__wrap1_3))->___dictionary_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3E7__wrap1_3))->___currentValue_3), (void*)NULL);
		#endif
	}
};


// Photon.Pun.Demo.Procedural.WorldSize
struct  WorldSize_t3928CD2BF566A27AE96516C01333861B0427E6AE 
{
public:
	// System.Int32 Photon.Pun.Demo.Procedural.WorldSize::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WorldSize_t3928CD2BF566A27AE96516C01333861B0427E6AE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Pun.Demo.Procedural.WorldType
struct  WorldType_tC2ACAD0C1E2C30988845C480CEBF831D7A25D2C1 
{
public:
	// System.Int32 Photon.Pun.Demo.Procedural.WorldType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WorldType_tC2ACAD0C1E2C30988845C480CEBF831D7A25D2C1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Pun.Demo.SlotRacer.Utils.BezierControlPointMode
struct  BezierControlPointMode_t2B23887B50F4C7B9D8E539B09CA6F520D2123F4F 
{
public:
	// System.Int32 Photon.Pun.Demo.SlotRacer.Utils.BezierControlPointMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BezierControlPointMode_t2B23887B50F4C7B9D8E539B09CA6F520D2123F4F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Photon.Realtime.ServerConnection
struct  ServerConnection_t5E49560AE3E1B85523C39B5EAB7EA09FE5363F68 
{
public:
	// System.Int32 Photon.Realtime.ServerConnection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ServerConnection_t5E49560AE3E1B85523C39B5EAB7EA09FE5363F68, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBow_accuracyProjectile
struct  accuracyProjectile_tC198D8C60616B536266A9F076E20E0EE45D61063 
{
public:
	// System.Int32 StandardizedBow_accuracyProjectile::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(accuracyProjectile_tC198D8C60616B536266A9F076E20E0EE45D61063, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBow_axisDirection
struct  axisDirection_t5F663D90C6A0E0F8EC09255603A7877311A1736A 
{
public:
	// System.Int32 StandardizedBow_axisDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(axisDirection_t5F663D90C6A0E0F8EC09255603A7877311A1736A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBow_jointsDirection
struct  jointsDirection_tE9CD4E8C72ACA5A602B6FEFC349CC2CB4FACBAE6 
{
public:
	// System.Int32 StandardizedBow_jointsDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(jointsDirection_tE9CD4E8C72ACA5A602B6FEFC349CC2CB4FACBAE6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBow_stringRetractEaseType
struct  stringRetractEaseType_t21F517458624D647C12D86FDD981C03AFFC42CFA 
{
public:
	// System.Int32 StandardizedBow_stringRetractEaseType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(stringRetractEaseType_t21F517458624D647C12D86FDD981C03AFFC42CFA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBow_stringStressEaseType
struct  stringStressEaseType_tB513B5B994C13396BC40062E3AD544AA28436994 
{
public:
	// System.Int32 StandardizedBow_stringStressEaseType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(stringStressEaseType_tB513B5B994C13396BC40062E3AD544AA28436994, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBowForHand_accuracyProjectile
struct  accuracyProjectile_t415634A6C9063DB7D30413FA9D59FA35F6761BD9 
{
public:
	// System.Int32 StandardizedBowForHand_accuracyProjectile::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(accuracyProjectile_t415634A6C9063DB7D30413FA9D59FA35F6761BD9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBowForHand_axisDirection
struct  axisDirection_t51F56CA449D7BD849335C8993D885CA9B9D836F1 
{
public:
	// System.Int32 StandardizedBowForHand_axisDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(axisDirection_t51F56CA449D7BD849335C8993D885CA9B9D836F1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBowForHand_jointsDirection
struct  jointsDirection_tC2928F58D46D2029DF98D1911627E5A2D887FBC2 
{
public:
	// System.Int32 StandardizedBowForHand_jointsDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(jointsDirection_tC2928F58D46D2029DF98D1911627E5A2D887FBC2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBowForHand_stringRetractEaseType
struct  stringRetractEaseType_tF81AB2B08105AA25B00E671BA1873DDD36A89FBE 
{
public:
	// System.Int32 StandardizedBowForHand_stringRetractEaseType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(stringRetractEaseType_tF81AB2B08105AA25B00E671BA1873DDD36A89FBE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBowForHand_stringStressEaseType
struct  stringStressEaseType_tB6F2A4811C3F5783C33E0191200245FCCC7FE001 
{
public:
	// System.Int32 StandardizedBowForHand_stringStressEaseType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(stringStressEaseType_tB6F2A4811C3F5783C33E0191200245FCCC7FE001, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBowForHandBool_accuracyProjectile
struct  accuracyProjectile_t3E5E3F52F548E246160ECA18C99777CB3731579C 
{
public:
	// System.Int32 StandardizedBowForHandBool_accuracyProjectile::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(accuracyProjectile_t3E5E3F52F548E246160ECA18C99777CB3731579C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBowForHandBool_axisDirection
struct  axisDirection_t57C8521B5BC07DA14A6C0C308B9B8CDFA63E459A 
{
public:
	// System.Int32 StandardizedBowForHandBool_axisDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(axisDirection_t57C8521B5BC07DA14A6C0C308B9B8CDFA63E459A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBowForHandBool_jointsDirection
struct  jointsDirection_t56AC6A4C0E2038F79D026727E27F4CF5782E6DCA 
{
public:
	// System.Int32 StandardizedBowForHandBool_jointsDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(jointsDirection_t56AC6A4C0E2038F79D026727E27F4CF5782E6DCA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBowForHandBool_stringRetractEaseType
struct  stringRetractEaseType_t2570D2A62B7FFEA234510AE7E0512CE64D663FD1 
{
public:
	// System.Int32 StandardizedBowForHandBool_stringRetractEaseType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(stringRetractEaseType_t2570D2A62B7FFEA234510AE7E0512CE64D663FD1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StandardizedBowForHandBool_stringStressEaseType
struct  stringStressEaseType_tD8A79F112448CEC235337553E7A06DD640914834 
{
public:
	// System.Int32 StandardizedBowForHandBool_stringStressEaseType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(stringStressEaseType_tD8A79F112448CEC235337553E7A06DD640914834, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// Unity.Collections.Allocator
struct  Allocator_t62A091275262E7067EAAD565B67764FA877D58D6 
{
public:
	// System.Int32 Unity.Collections.Allocator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Allocator_t62A091275262E7067EAAD565B67764FA877D58D6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.ParticleSystemCurveMode
struct  ParticleSystemCurveMode_tD8A2390BB482B39C0C0714F3DDE715386BC7D48D 
{
public:
	// System.Int32 UnityEngine.ParticleSystemCurveMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemCurveMode_tD8A2390BB482B39C0C0714F3DDE715386BC7D48D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Pose
struct  Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 
{
public:
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___rotation_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotation_1 = value;
	}
};

struct Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields
{
public:
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___k_Identity_2;

public:
	inline static int32_t get_offset_of_k_Identity_2() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields, ___k_Identity_2)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_k_Identity_2() const { return ___k_Identity_2; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_k_Identity_2() { return &___k_Identity_2; }
	inline void set_k_Identity_2(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___k_Identity_2 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_Availability
struct  Availability_tFD76A2177DDFE5A5A8CDB75EB9CF784CDD9A7487 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_Availability::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Availability_tFD76A2177DDFE5A5A8CDB75EB9CF784CDD9A7487, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARWorldMapRequestStatus
struct  ARWorldMapRequestStatus_tF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMapRequestStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARWorldMapRequestStatus_tF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.CameraFocusMode
struct  CameraFocusMode_t004A78F37B73E0A13307FBF88BF32191115F1B28 
{
public:
	// System.Int32 UnityEngine.XR.ARSubsystems.CameraFocusMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraFocusMode_t004A78F37B73E0A13307FBF88BF32191115F1B28, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.LightEstimationMode
struct  LightEstimationMode_t313B5D9CF8DC78254E7E237F9F66E05D174CA9C9 
{
public:
	// System.Int32 UnityEngine.XR.ARSubsystems.LightEstimationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LightEstimationMode_t313B5D9CF8DC78254E7E237F9F66E05D174CA9C9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.PlaneAlignment
struct  PlaneAlignment_t8959E33A181E8A5B46387DDC30F957DD14B48783 
{
public:
	// System.Int32 UnityEngine.XR.ARSubsystems.PlaneAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlaneAlignment_t8959E33A181E8A5B46387DDC30F957DD14B48783, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.TrackableType
struct  TrackableType_t078FFF635AE2E4FC51E7D7DB8AB1CB884D30EA1F 
{
public:
	// System.Int32 UnityEngine.XR.ARSubsystems.TrackableType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackableType_t078FFF635AE2E4FC51E7D7DB8AB1CB884D30EA1F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.TrackingState
struct  TrackingState_t124D9E603E4E0453A85409CF7762EE8C946233F6 
{
public:
	// System.Int32 UnityEngine.XR.ARSubsystems.TrackingState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingState_t124D9E603E4E0453A85409CF7762EE8C946233F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.BoundedPlane,UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor>
struct  TrackingSubsystem_2_t758E80FC2D0E4A224AA733F5ED6419083CD56389  : public Subsystem_1_tA832EBA21E41FF7EBCA30AE7C32EC78033F63E37
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_t758E80FC2D0E4A224AA733F5ED6419083CD56389, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRPointCloud,UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor>
struct  TrackingSubsystem_2_t70B6F8BBDCEA193299D7F4FC34F9D63A1996CE0C  : public Subsystem_1_t699EE52FD744695EFB393B312D6BFA04DCC68870
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_t70B6F8BBDCEA193299D7F4FC34F9D63A1996CE0C, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`2<UnityEngine.XR.ARSubsystems.XRReferencePoint,UnityEngine.XR.ARSubsystems.XRReferencePointSubsystemDescriptor>
struct  TrackingSubsystem_2_t62AFA2295FCEFC2C3818E3B9EDB3C1AF80509899  : public Subsystem_1_t1A38EF1402A7E5DBC48E0755E358926BB0054890
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.TrackingSubsystem`2::m_Running
	bool ___m_Running_1;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(TrackingSubsystem_2_t62AFA2295FCEFC2C3818E3B9EDB3C1AF80509899, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRSessionSubsystem
struct  XRSessionSubsystem_t9B9C16B4BDB611559FB6FA728BE399001E47EFF0  : public Subsystem_1_t749909AA5D71BD615BDF40BDE7C8F81B8D63F4C7
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.XRSessionSubsystem::m_Running
	bool ___m_Running_1;
	// UnityEngine.XR.ARSubsystems.XRSessionSubsystem_IProvider UnityEngine.XR.ARSubsystems.XRSessionSubsystem::m_Provider
	IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428 * ___m_Provider_2;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(XRSessionSubsystem_t9B9C16B4BDB611559FB6FA728BE399001E47EFF0, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}

	inline static int32_t get_offset_of_m_Provider_2() { return static_cast<int32_t>(offsetof(XRSessionSubsystem_t9B9C16B4BDB611559FB6FA728BE399001E47EFF0, ___m_Provider_2)); }
	inline IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428 * get_m_Provider_2() const { return ___m_Provider_2; }
	inline IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428 ** get_address_of_m_Provider_2() { return &___m_Provider_2; }
	inline void set_m_Provider_2(IProvider_tCE3A36B52F24E47DF8D9945EAB79493A822E9428 * value)
	{
		___m_Provider_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Provider_2), (void*)value);
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// Unity.Collections.NativeArray`1<UnityEngine.Quaternion>
struct  NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif

// Unity.Collections.NativeArray`1<UnityEngine.Vector2>
struct  NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif

// Unity.Collections.NativeArray`1<UnityEngine.Vector3>
struct  NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif

// Unity.Collections.NativeArray`1<UnityEngine.Vector4>
struct  NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.ParticleSystem_MinMaxCurve
struct  MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71 
{
public:
	// UnityEngine.ParticleSystemCurveMode UnityEngine.ParticleSystem_MinMaxCurve::m_Mode
	int32_t ___m_Mode_0;
	// System.Single UnityEngine.ParticleSystem_MinMaxCurve::m_CurveMultiplier
	float ___m_CurveMultiplier_1;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem_MinMaxCurve::m_CurveMin
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_CurveMin_2;
	// UnityEngine.AnimationCurve UnityEngine.ParticleSystem_MinMaxCurve::m_CurveMax
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_CurveMax_3;
	// System.Single UnityEngine.ParticleSystem_MinMaxCurve::m_ConstantMin
	float ___m_ConstantMin_4;
	// System.Single UnityEngine.ParticleSystem_MinMaxCurve::m_ConstantMax
	float ___m_ConstantMax_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_CurveMultiplier_1() { return static_cast<int32_t>(offsetof(MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71, ___m_CurveMultiplier_1)); }
	inline float get_m_CurveMultiplier_1() const { return ___m_CurveMultiplier_1; }
	inline float* get_address_of_m_CurveMultiplier_1() { return &___m_CurveMultiplier_1; }
	inline void set_m_CurveMultiplier_1(float value)
	{
		___m_CurveMultiplier_1 = value;
	}

	inline static int32_t get_offset_of_m_CurveMin_2() { return static_cast<int32_t>(offsetof(MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71, ___m_CurveMin_2)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_CurveMin_2() const { return ___m_CurveMin_2; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_CurveMin_2() { return &___m_CurveMin_2; }
	inline void set_m_CurveMin_2(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_CurveMin_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurveMin_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurveMax_3() { return static_cast<int32_t>(offsetof(MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71, ___m_CurveMax_3)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_CurveMax_3() const { return ___m_CurveMax_3; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_CurveMax_3() { return &___m_CurveMax_3; }
	inline void set_m_CurveMax_3(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_CurveMax_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurveMax_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_ConstantMin_4() { return static_cast<int32_t>(offsetof(MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71, ___m_ConstantMin_4)); }
	inline float get_m_ConstantMin_4() const { return ___m_ConstantMin_4; }
	inline float* get_address_of_m_ConstantMin_4() { return &___m_ConstantMin_4; }
	inline void set_m_ConstantMin_4(float value)
	{
		___m_ConstantMin_4 = value;
	}

	inline static int32_t get_offset_of_m_ConstantMax_5() { return static_cast<int32_t>(offsetof(MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71, ___m_ConstantMax_5)); }
	inline float get_m_ConstantMax_5() const { return ___m_ConstantMax_5; }
	inline float* get_address_of_m_ConstantMax_5() { return &___m_ConstantMax_5; }
	inline void set_m_ConstantMax_5(float value)
	{
		___m_ConstantMax_5 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem
struct  ARKitSessionSubsystem_tD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A  : public XRSessionSubsystem_t9B9C16B4BDB611559FB6FA728BE399001E47EFF0
{
public:

public:
};

struct ARKitSessionSubsystem_tD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A_StaticFields
{
public:
	// UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_OnAsyncConversionCompleteDelegate UnityEngine.XR.ARKit.ARKitSessionSubsystem::s_OnAsyncWorldMapCompleted
	OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF * ___s_OnAsyncWorldMapCompleted_3;

public:
	inline static int32_t get_offset_of_s_OnAsyncWorldMapCompleted_3() { return static_cast<int32_t>(offsetof(ARKitSessionSubsystem_tD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A_StaticFields, ___s_OnAsyncWorldMapCompleted_3)); }
	inline OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF * get_s_OnAsyncWorldMapCompleted_3() const { return ___s_OnAsyncWorldMapCompleted_3; }
	inline OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF ** get_address_of_s_OnAsyncWorldMapCompleted_3() { return &___s_OnAsyncWorldMapCompleted_3; }
	inline void set_s_OnAsyncWorldMapCompleted_3(OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF * value)
	{
		___s_OnAsyncWorldMapCompleted_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_OnAsyncWorldMapCompleted_3), (void*)value);
	}
};


// UnityEngine.XR.ARSubsystems.BoundedPlane
struct  BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.BoundedPlane::m_TrackableId
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_TrackableId_0;
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.BoundedPlane::m_SubsumedById
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_SubsumedById_1;
	// UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.BoundedPlane::m_Center
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Center_2;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.BoundedPlane::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_3;
	// UnityEngine.Vector2 UnityEngine.XR.ARSubsystems.BoundedPlane::m_Size
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Size_4;
	// UnityEngine.XR.ARSubsystems.PlaneAlignment UnityEngine.XR.ARSubsystems.BoundedPlane::m_Alignment
	int32_t ___m_Alignment_5;
	// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.BoundedPlane::m_TrackingState
	int32_t ___m_TrackingState_6;
	// System.IntPtr UnityEngine.XR.ARSubsystems.BoundedPlane::m_NativePtr
	intptr_t ___m_NativePtr_7;

public:
	inline static int32_t get_offset_of_m_TrackableId_0() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_TrackableId_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_TrackableId_0() const { return ___m_TrackableId_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_TrackableId_0() { return &___m_TrackableId_0; }
	inline void set_m_TrackableId_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_m_SubsumedById_1() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_SubsumedById_1)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_SubsumedById_1() const { return ___m_SubsumedById_1; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_SubsumedById_1() { return &___m_SubsumedById_1; }
	inline void set_m_SubsumedById_1(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_SubsumedById_1 = value;
	}

	inline static int32_t get_offset_of_m_Center_2() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_Center_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Center_2() const { return ___m_Center_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Center_2() { return &___m_Center_2; }
	inline void set_m_Center_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Center_2 = value;
	}

	inline static int32_t get_offset_of_m_Pose_3() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_Pose_3)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_3() const { return ___m_Pose_3; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_3() { return &___m_Pose_3; }
	inline void set_m_Pose_3(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_3 = value;
	}

	inline static int32_t get_offset_of_m_Size_4() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_Size_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Size_4() const { return ___m_Size_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Size_4() { return &___m_Size_4; }
	inline void set_m_Size_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Size_4 = value;
	}

	inline static int32_t get_offset_of_m_Alignment_5() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_Alignment_5)); }
	inline int32_t get_m_Alignment_5() const { return ___m_Alignment_5; }
	inline int32_t* get_address_of_m_Alignment_5() { return &___m_Alignment_5; }
	inline void set_m_Alignment_5(int32_t value)
	{
		___m_Alignment_5 = value;
	}

	inline static int32_t get_offset_of_m_TrackingState_6() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_TrackingState_6)); }
	inline int32_t get_m_TrackingState_6() const { return ___m_TrackingState_6; }
	inline int32_t* get_address_of_m_TrackingState_6() { return &___m_TrackingState_6; }
	inline void set_m_TrackingState_6(int32_t value)
	{
		___m_TrackingState_6 = value;
	}

	inline static int32_t get_offset_of_m_NativePtr_7() { return static_cast<int32_t>(offsetof(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227, ___m_NativePtr_7)); }
	inline intptr_t get_m_NativePtr_7() const { return ___m_NativePtr_7; }
	inline intptr_t* get_address_of_m_NativePtr_7() { return &___m_NativePtr_7; }
	inline void set_m_NativePtr_7(intptr_t value)
	{
		___m_NativePtr_7 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRCameraSubsystem
struct  XRCameraSubsystem_t60344763BCE87679E570A4AD0BB068B74DBF20AF  : public Subsystem_1_t54655F7C7F723FDA59325E1AC875A3FD66C9C684
{
public:
	// UnityEngine.XR.ARSubsystems.XRCameraSubsystem_IProvider UnityEngine.XR.ARSubsystems.XRCameraSubsystem::m_Provider
	IProvider_t5D7A855308E0C5E8F9175CA7CDEE91F929A5185C * ___m_Provider_1;
	// UnityEngine.XR.ARSubsystems.CameraFocusMode UnityEngine.XR.ARSubsystems.XRCameraSubsystem::m_FocusMode
	int32_t ___m_FocusMode_2;
	// UnityEngine.XR.ARSubsystems.LightEstimationMode UnityEngine.XR.ARSubsystems.XRCameraSubsystem::m_LightEstimationMode
	int32_t ___m_LightEstimationMode_3;
	// System.Boolean UnityEngine.XR.ARSubsystems.XRCameraSubsystem::m_Running
	bool ___m_Running_4;

public:
	inline static int32_t get_offset_of_m_Provider_1() { return static_cast<int32_t>(offsetof(XRCameraSubsystem_t60344763BCE87679E570A4AD0BB068B74DBF20AF, ___m_Provider_1)); }
	inline IProvider_t5D7A855308E0C5E8F9175CA7CDEE91F929A5185C * get_m_Provider_1() const { return ___m_Provider_1; }
	inline IProvider_t5D7A855308E0C5E8F9175CA7CDEE91F929A5185C ** get_address_of_m_Provider_1() { return &___m_Provider_1; }
	inline void set_m_Provider_1(IProvider_t5D7A855308E0C5E8F9175CA7CDEE91F929A5185C * value)
	{
		___m_Provider_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Provider_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_FocusMode_2() { return static_cast<int32_t>(offsetof(XRCameraSubsystem_t60344763BCE87679E570A4AD0BB068B74DBF20AF, ___m_FocusMode_2)); }
	inline int32_t get_m_FocusMode_2() const { return ___m_FocusMode_2; }
	inline int32_t* get_address_of_m_FocusMode_2() { return &___m_FocusMode_2; }
	inline void set_m_FocusMode_2(int32_t value)
	{
		___m_FocusMode_2 = value;
	}

	inline static int32_t get_offset_of_m_LightEstimationMode_3() { return static_cast<int32_t>(offsetof(XRCameraSubsystem_t60344763BCE87679E570A4AD0BB068B74DBF20AF, ___m_LightEstimationMode_3)); }
	inline int32_t get_m_LightEstimationMode_3() const { return ___m_LightEstimationMode_3; }
	inline int32_t* get_address_of_m_LightEstimationMode_3() { return &___m_LightEstimationMode_3; }
	inline void set_m_LightEstimationMode_3(int32_t value)
	{
		___m_LightEstimationMode_3 = value;
	}

	inline static int32_t get_offset_of_m_Running_4() { return static_cast<int32_t>(offsetof(XRCameraSubsystem_t60344763BCE87679E570A4AD0BB068B74DBF20AF, ___m_Running_4)); }
	inline bool get_m_Running_4() const { return ___m_Running_4; }
	inline bool* get_address_of_m_Running_4() { return &___m_Running_4; }
	inline void set_m_Running_4(bool value)
	{
		___m_Running_4 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRPointCloud
struct  XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRPointCloud::m_TrackableId
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_TrackableId_0;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRPointCloud::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_1;
	// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRPointCloud::m_TrackingState
	int32_t ___m_TrackingState_2;
	// System.IntPtr UnityEngine.XR.ARSubsystems.XRPointCloud::m_NativePtr
	intptr_t ___m_NativePtr_3;

public:
	inline static int32_t get_offset_of_m_TrackableId_0() { return static_cast<int32_t>(offsetof(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0, ___m_TrackableId_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_TrackableId_0() const { return ___m_TrackableId_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_TrackableId_0() { return &___m_TrackableId_0; }
	inline void set_m_TrackableId_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_m_Pose_1() { return static_cast<int32_t>(offsetof(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0, ___m_Pose_1)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_1() const { return ___m_Pose_1; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_1() { return &___m_Pose_1; }
	inline void set_m_Pose_1(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_1 = value;
	}

	inline static int32_t get_offset_of_m_TrackingState_2() { return static_cast<int32_t>(offsetof(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0, ___m_TrackingState_2)); }
	inline int32_t get_m_TrackingState_2() const { return ___m_TrackingState_2; }
	inline int32_t* get_address_of_m_TrackingState_2() { return &___m_TrackingState_2; }
	inline void set_m_TrackingState_2(int32_t value)
	{
		___m_TrackingState_2 = value;
	}

	inline static int32_t get_offset_of_m_NativePtr_3() { return static_cast<int32_t>(offsetof(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0, ___m_NativePtr_3)); }
	inline intptr_t get_m_NativePtr_3() const { return ___m_NativePtr_3; }
	inline intptr_t* get_address_of_m_NativePtr_3() { return &___m_NativePtr_3; }
	inline void set_m_NativePtr_3(intptr_t value)
	{
		___m_NativePtr_3 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRRaycastHit
struct  XRRaycastHit_t2DE122E601B75E2070D4A542A13E2486DEE60E82 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRRaycastHit::m_TrackableId
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_TrackableId_0;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRRaycastHit::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_1;
	// System.Single UnityEngine.XR.ARSubsystems.XRRaycastHit::m_Distance
	float ___m_Distance_2;
	// UnityEngine.XR.ARSubsystems.TrackableType UnityEngine.XR.ARSubsystems.XRRaycastHit::m_HitType
	int32_t ___m_HitType_3;

public:
	inline static int32_t get_offset_of_m_TrackableId_0() { return static_cast<int32_t>(offsetof(XRRaycastHit_t2DE122E601B75E2070D4A542A13E2486DEE60E82, ___m_TrackableId_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_TrackableId_0() const { return ___m_TrackableId_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_TrackableId_0() { return &___m_TrackableId_0; }
	inline void set_m_TrackableId_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_m_Pose_1() { return static_cast<int32_t>(offsetof(XRRaycastHit_t2DE122E601B75E2070D4A542A13E2486DEE60E82, ___m_Pose_1)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_1() const { return ___m_Pose_1; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_1() { return &___m_Pose_1; }
	inline void set_m_Pose_1(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_1 = value;
	}

	inline static int32_t get_offset_of_m_Distance_2() { return static_cast<int32_t>(offsetof(XRRaycastHit_t2DE122E601B75E2070D4A542A13E2486DEE60E82, ___m_Distance_2)); }
	inline float get_m_Distance_2() const { return ___m_Distance_2; }
	inline float* get_address_of_m_Distance_2() { return &___m_Distance_2; }
	inline void set_m_Distance_2(float value)
	{
		___m_Distance_2 = value;
	}

	inline static int32_t get_offset_of_m_HitType_3() { return static_cast<int32_t>(offsetof(XRRaycastHit_t2DE122E601B75E2070D4A542A13E2486DEE60E82, ___m_HitType_3)); }
	inline int32_t get_m_HitType_3() const { return ___m_HitType_3; }
	inline int32_t* get_address_of_m_HitType_3() { return &___m_HitType_3; }
	inline void set_m_HitType_3(int32_t value)
	{
		___m_HitType_3 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRReferencePoint
struct  XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9 
{
public:
	// UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARSubsystems.XRReferencePoint::m_Id
	TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  ___m_Id_0;
	// UnityEngine.Pose UnityEngine.XR.ARSubsystems.XRReferencePoint::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_1;
	// UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARSubsystems.XRReferencePoint::m_TrackingState
	int32_t ___m_TrackingState_2;
	// System.IntPtr UnityEngine.XR.ARSubsystems.XRReferencePoint::m_NativePtr
	intptr_t ___m_NativePtr_3;

public:
	inline static int32_t get_offset_of_m_Id_0() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_Id_0)); }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  get_m_Id_0() const { return ___m_Id_0; }
	inline TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47 * get_address_of_m_Id_0() { return &___m_Id_0; }
	inline void set_m_Id_0(TrackableId_tA7E19AFE62176E25E3759548887E9068E1E4AE47  value)
	{
		___m_Id_0 = value;
	}

	inline static int32_t get_offset_of_m_Pose_1() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_Pose_1)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_1() const { return ___m_Pose_1; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_1() { return &___m_Pose_1; }
	inline void set_m_Pose_1(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_1 = value;
	}

	inline static int32_t get_offset_of_m_TrackingState_2() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_TrackingState_2)); }
	inline int32_t get_m_TrackingState_2() const { return ___m_TrackingState_2; }
	inline int32_t* get_address_of_m_TrackingState_2() { return &___m_TrackingState_2; }
	inline void set_m_TrackingState_2(int32_t value)
	{
		___m_TrackingState_2 = value;
	}

	inline static int32_t get_offset_of_m_NativePtr_3() { return static_cast<int32_t>(offsetof(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9, ___m_NativePtr_3)); }
	inline intptr_t get_m_NativePtr_3() const { return ___m_NativePtr_3; }
	inline intptr_t* get_address_of_m_NativePtr_3() { return &___m_NativePtr_3; }
	inline void set_m_NativePtr_3(intptr_t value)
	{
		___m_NativePtr_3 = value;
	}
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.ParticleSystem_Burst
struct  Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB 
{
public:
	// System.Single UnityEngine.ParticleSystem_Burst::m_Time
	float ___m_Time_0;
	// UnityEngine.ParticleSystem_MinMaxCurve UnityEngine.ParticleSystem_Burst::m_Count
	MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71  ___m_Count_1;
	// System.Int32 UnityEngine.ParticleSystem_Burst::m_RepeatCount
	int32_t ___m_RepeatCount_2;
	// System.Single UnityEngine.ParticleSystem_Burst::m_RepeatInterval
	float ___m_RepeatInterval_3;
	// System.Single UnityEngine.ParticleSystem_Burst::m_InvProbability
	float ___m_InvProbability_4;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Count_1() { return static_cast<int32_t>(offsetof(Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB, ___m_Count_1)); }
	inline MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71  get_m_Count_1() const { return ___m_Count_1; }
	inline MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71 * get_address_of_m_Count_1() { return &___m_Count_1; }
	inline void set_m_Count_1(MinMaxCurve_tDB335EDEBEBD4CFA753081D7C3A2FE2EECFA6D71  value)
	{
		___m_Count_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Count_1))->___m_CurveMin_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Count_1))->___m_CurveMax_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_RepeatCount_2() { return static_cast<int32_t>(offsetof(Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB, ___m_RepeatCount_2)); }
	inline int32_t get_m_RepeatCount_2() const { return ___m_RepeatCount_2; }
	inline int32_t* get_address_of_m_RepeatCount_2() { return &___m_RepeatCount_2; }
	inline void set_m_RepeatCount_2(int32_t value)
	{
		___m_RepeatCount_2 = value;
	}

	inline static int32_t get_offset_of_m_RepeatInterval_3() { return static_cast<int32_t>(offsetof(Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB, ___m_RepeatInterval_3)); }
	inline float get_m_RepeatInterval_3() const { return ___m_RepeatInterval_3; }
	inline float* get_address_of_m_RepeatInterval_3() { return &___m_RepeatInterval_3; }
	inline void set_m_RepeatInterval_3(float value)
	{
		___m_RepeatInterval_3 = value;
	}

	inline static int32_t get_offset_of_m_InvProbability_4() { return static_cast<int32_t>(offsetof(Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB, ___m_InvProbability_4)); }
	inline float get_m_InvProbability_4() const { return ___m_InvProbability_4; }
	inline float* get_address_of_m_InvProbability_4() { return &___m_InvProbability_4; }
	inline void set_m_InvProbability_4(float value)
	{
		___m_InvProbability_4 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitCameraSubsystem
struct  ARKitCameraSubsystem_t60568A6D41C4421316052C06185DC95BA291F614  : public XRCameraSubsystem_t60344763BCE87679E570A4AD0BB068B74DBF20AF
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem_NativeApi_OnAsyncConversionCompleteDelegate
struct  OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_TransformPositionsJob
struct  TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283 
{
public:
	// Unity.Collections.NativeArray`1<UnityEngine.Quaternion> UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_TransformPositionsJob::positionsIn
	NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684  ___positionsIn_0;
	// Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARKit.ARKitXRDepthSubsystem_TransformPositionsJob::positionsOut
	NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74  ___positionsOut_1;

public:
	inline static int32_t get_offset_of_positionsIn_0() { return static_cast<int32_t>(offsetof(TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283, ___positionsIn_0)); }
	inline NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684  get_positionsIn_0() const { return ___positionsIn_0; }
	inline NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684 * get_address_of_positionsIn_0() { return &___positionsIn_0; }
	inline void set_positionsIn_0(NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684  value)
	{
		___positionsIn_0 = value;
	}

	inline static int32_t get_offset_of_positionsOut_1() { return static_cast<int32_t>(offsetof(TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283, ___positionsOut_1)); }
	inline NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74  get_positionsOut_1() const { return ___positionsOut_1; }
	inline NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74 * get_address_of_positionsOut_1() { return &___positionsOut_1; }
	inline void set_positionsOut_1(NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74  value)
	{
		___positionsOut_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/TransformPositionsJob
struct TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283_marshaled_pinvoke
{
	NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684  ___positionsIn_0;
	NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74  ___positionsOut_1;
};
// Native definition for COM marshalling of UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/TransformPositionsJob
struct TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283_marshaled_com
{
	NativeArray_1_t9C70B1A7759D3AEB5D78FECCCDB8DCDEB9CCA684  ___positionsIn_0;
	NativeArray_1_t20B0E97D613353CCC2889E9B3D97A5612374FE74  ___positionsOut_1;
};

// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_Provider_FlipBoundaryWindingJob
struct  FlipBoundaryWindingJob_tAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312 
{
public:
	// Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_Provider_FlipBoundaryWindingJob::positions
	NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  ___positions_0;

public:
	inline static int32_t get_offset_of_positions_0() { return static_cast<int32_t>(offsetof(FlipBoundaryWindingJob_tAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312, ___positions_0)); }
	inline NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  get_positions_0() const { return ___positions_0; }
	inline NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71 * get_address_of_positions_0() { return &___positions_0; }
	inline void set_positions_0(NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  value)
	{
		___positions_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider/FlipBoundaryWindingJob
struct FlipBoundaryWindingJob_tAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312_marshaled_pinvoke
{
	NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  ___positions_0;
};
// Native definition for COM marshalling of UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider/FlipBoundaryWindingJob
struct FlipBoundaryWindingJob_tAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312_marshaled_com
{
	NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  ___positions_0;
};

// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_Provider_TransformBoundaryPositionsJob
struct  TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A 
{
public:
	// Unity.Collections.NativeArray`1<UnityEngine.Vector4> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_Provider_TransformBoundaryPositionsJob::positionsIn
	NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96  ___positionsIn_0;
	// Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem_Provider_TransformBoundaryPositionsJob::positionsOut
	NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  ___positionsOut_1;

public:
	inline static int32_t get_offset_of_positionsIn_0() { return static_cast<int32_t>(offsetof(TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A, ___positionsIn_0)); }
	inline NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96  get_positionsIn_0() const { return ___positionsIn_0; }
	inline NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96 * get_address_of_positionsIn_0() { return &___positionsIn_0; }
	inline void set_positionsIn_0(NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96  value)
	{
		___positionsIn_0 = value;
	}

	inline static int32_t get_offset_of_positionsOut_1() { return static_cast<int32_t>(offsetof(TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A, ___positionsOut_1)); }
	inline NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  get_positionsOut_1() const { return ___positionsOut_1; }
	inline NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71 * get_address_of_positionsOut_1() { return &___positionsOut_1; }
	inline void set_positionsOut_1(NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  value)
	{
		___positionsOut_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider/TransformBoundaryPositionsJob
struct TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A_marshaled_pinvoke
{
	NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96  ___positionsIn_0;
	NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  ___positionsOut_1;
};
// Native definition for COM marshalling of UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/Provider/TransformBoundaryPositionsJob
struct TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A_marshaled_com
{
	NativeArray_1_t32E6297AF854BD125529357115F7C02BA25C4C96  ___positionsIn_0;
	NativeArray_1_t86AEDFC03CDAC131E54ED6A68B102EBD947A3F71  ___positionsOut_1;
};

// UnityEngine.XR.ARSubsystems.XRDepthSubsystem
struct  XRDepthSubsystem_tA1188AEE9FA009EAF2BDA064BEA1482FF4CD8AD7  : public TrackingSubsystem_2_t70B6F8BBDCEA193299D7F4FC34F9D63A1996CE0C
{
public:
	// UnityEngine.XR.ARSubsystems.XRDepthSubsystem_IDepthApi UnityEngine.XR.ARSubsystems.XRDepthSubsystem::m_Interface
	IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4 * ___m_Interface_2;
	// UnityEngine.XR.ARSubsystems.XRPointCloud UnityEngine.XR.ARSubsystems.XRDepthSubsystem::m_DefaultPointCloud
	XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0  ___m_DefaultPointCloud_3;

public:
	inline static int32_t get_offset_of_m_Interface_2() { return static_cast<int32_t>(offsetof(XRDepthSubsystem_tA1188AEE9FA009EAF2BDA064BEA1482FF4CD8AD7, ___m_Interface_2)); }
	inline IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4 * get_m_Interface_2() const { return ___m_Interface_2; }
	inline IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4 ** get_address_of_m_Interface_2() { return &___m_Interface_2; }
	inline void set_m_Interface_2(IDepthApi_t7CEE33C76911E53220D0242D5E38503D64736BE4 * value)
	{
		___m_Interface_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Interface_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DefaultPointCloud_3() { return static_cast<int32_t>(offsetof(XRDepthSubsystem_tA1188AEE9FA009EAF2BDA064BEA1482FF4CD8AD7, ___m_DefaultPointCloud_3)); }
	inline XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0  get_m_DefaultPointCloud_3() const { return ___m_DefaultPointCloud_3; }
	inline XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0 * get_address_of_m_DefaultPointCloud_3() { return &___m_DefaultPointCloud_3; }
	inline void set_m_DefaultPointCloud_3(XRPointCloud_tA4A412DE503530E1B2953919F1463B9B48504ED0  value)
	{
		___m_DefaultPointCloud_3 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem
struct  XRPlaneSubsystem_tCAFBDE4A030D5EB56FC6D968DB31FA2852836909  : public TrackingSubsystem_2_t758E80FC2D0E4A224AA733F5ED6419083CD56389
{
public:
	// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_IProvider UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::m_Provider
	IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469 * ___m_Provider_2;
	// UnityEngine.XR.ARSubsystems.BoundedPlane UnityEngine.XR.ARSubsystems.XRPlaneSubsystem::m_DefaultPlane
	BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227  ___m_DefaultPlane_3;

public:
	inline static int32_t get_offset_of_m_Provider_2() { return static_cast<int32_t>(offsetof(XRPlaneSubsystem_tCAFBDE4A030D5EB56FC6D968DB31FA2852836909, ___m_Provider_2)); }
	inline IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469 * get_m_Provider_2() const { return ___m_Provider_2; }
	inline IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469 ** get_address_of_m_Provider_2() { return &___m_Provider_2; }
	inline void set_m_Provider_2(IProvider_t40982CEC3244CC0C726CED2C27336E95321F7469 * value)
	{
		___m_Provider_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Provider_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DefaultPlane_3() { return static_cast<int32_t>(offsetof(XRPlaneSubsystem_tCAFBDE4A030D5EB56FC6D968DB31FA2852836909, ___m_DefaultPlane_3)); }
	inline BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227  get_m_DefaultPlane_3() const { return ___m_DefaultPlane_3; }
	inline BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227 * get_address_of_m_DefaultPlane_3() { return &___m_DefaultPlane_3; }
	inline void set_m_DefaultPlane_3(BoundedPlane_t5FBCC289E172852D4A9C51F6581B3C7EEE17A227  value)
	{
		___m_DefaultPlane_3 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRRaycastSubsystem
struct  XRRaycastSubsystem_t1181EA314910ABB4E1F50BF2F7650EC1512A0A20  : public Subsystem_1_t1D44399B8190A9E3533EE5C5D5915B75D79B2E66
{
public:
	// System.Boolean UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::m_Running
	bool ___m_Running_1;
	// UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_IProvider UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::m_Provider
	IProvider_tB9DFB2CA3241B1F6107D01753C0F00628994A3ED * ___m_Provider_2;
	// UnityEngine.XR.ARSubsystems.XRRaycastHit UnityEngine.XR.ARSubsystems.XRRaycastSubsystem::m_DefaultRaycastHit
	XRRaycastHit_t2DE122E601B75E2070D4A542A13E2486DEE60E82  ___m_DefaultRaycastHit_3;

public:
	inline static int32_t get_offset_of_m_Running_1() { return static_cast<int32_t>(offsetof(XRRaycastSubsystem_t1181EA314910ABB4E1F50BF2F7650EC1512A0A20, ___m_Running_1)); }
	inline bool get_m_Running_1() const { return ___m_Running_1; }
	inline bool* get_address_of_m_Running_1() { return &___m_Running_1; }
	inline void set_m_Running_1(bool value)
	{
		___m_Running_1 = value;
	}

	inline static int32_t get_offset_of_m_Provider_2() { return static_cast<int32_t>(offsetof(XRRaycastSubsystem_t1181EA314910ABB4E1F50BF2F7650EC1512A0A20, ___m_Provider_2)); }
	inline IProvider_tB9DFB2CA3241B1F6107D01753C0F00628994A3ED * get_m_Provider_2() const { return ___m_Provider_2; }
	inline IProvider_tB9DFB2CA3241B1F6107D01753C0F00628994A3ED ** get_address_of_m_Provider_2() { return &___m_Provider_2; }
	inline void set_m_Provider_2(IProvider_tB9DFB2CA3241B1F6107D01753C0F00628994A3ED * value)
	{
		___m_Provider_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Provider_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DefaultRaycastHit_3() { return static_cast<int32_t>(offsetof(XRRaycastSubsystem_t1181EA314910ABB4E1F50BF2F7650EC1512A0A20, ___m_DefaultRaycastHit_3)); }
	inline XRRaycastHit_t2DE122E601B75E2070D4A542A13E2486DEE60E82  get_m_DefaultRaycastHit_3() const { return ___m_DefaultRaycastHit_3; }
	inline XRRaycastHit_t2DE122E601B75E2070D4A542A13E2486DEE60E82 * get_address_of_m_DefaultRaycastHit_3() { return &___m_DefaultRaycastHit_3; }
	inline void set_m_DefaultRaycastHit_3(XRRaycastHit_t2DE122E601B75E2070D4A542A13E2486DEE60E82  value)
	{
		___m_DefaultRaycastHit_3 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem
struct  XRReferencePointSubsystem_tF175EC0188CC6EFB0A0633BA11FCA4D81A6A88E3  : public TrackingSubsystem_2_t62AFA2295FCEFC2C3818E3B9EDB3C1AF80509899
{
public:
	// UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_IProvider UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::m_Provider
	IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459 * ___m_Provider_2;
	// UnityEngine.XR.ARSubsystems.XRReferencePoint UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem::m_DefaultReferencePoint
	XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9  ___m_DefaultReferencePoint_3;

public:
	inline static int32_t get_offset_of_m_Provider_2() { return static_cast<int32_t>(offsetof(XRReferencePointSubsystem_tF175EC0188CC6EFB0A0633BA11FCA4D81A6A88E3, ___m_Provider_2)); }
	inline IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459 * get_m_Provider_2() const { return ___m_Provider_2; }
	inline IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459 ** get_address_of_m_Provider_2() { return &___m_Provider_2; }
	inline void set_m_Provider_2(IProvider_tA80639246D0ED291E8332BE2FBC3FE3EA13A4459 * value)
	{
		___m_Provider_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Provider_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DefaultReferencePoint_3() { return static_cast<int32_t>(offsetof(XRReferencePointSubsystem_tF175EC0188CC6EFB0A0633BA11FCA4D81A6A88E3, ___m_DefaultReferencePoint_3)); }
	inline XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9  get_m_DefaultReferencePoint_3() const { return ___m_DefaultReferencePoint_3; }
	inline XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9 * get_address_of_m_DefaultReferencePoint_3() { return &___m_DefaultReferencePoint_3; }
	inline void set_m_DefaultReferencePoint_3(XRReferencePoint_tA8592C08A27EC91D9B1FB3B083C95C5D372FF1F9  value)
	{
		___m_DefaultReferencePoint_3 = value;
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitRaycastSubsystem
struct  ARKitRaycastSubsystem_t603C87A6FBC8139D35116A61251EB289E21ABC09  : public XRRaycastSubsystem_t1181EA314910ABB4E1F50BF2F7650EC1512A0A20
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitReferencePointSubsystem
struct  ARKitReferencePointSubsystem_t7883B8562F4226A121B744F99A8CE262F2D0E017  : public XRReferencePointSubsystem_tF175EC0188CC6EFB0A0633BA11FCA4D81A6A88E3
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRDepthSubsystem
struct  ARKitXRDepthSubsystem_tA5A5BCEDB5F2217FEE76B4751167757193534501  : public XRDepthSubsystem_tA1188AEE9FA009EAF2BDA064BEA1482FF4CD8AD7
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem
struct  ARKitXRPlaneSubsystem_tBCCDC8EA086FD3B3AD556F50AECA1BBFA9A8272A  : public XRPlaneSubsystem_tCAFBDE4A030D5EB56FC6D968DB31FA2852836909
{
public:

public:
};


// AcquireChanController
struct  AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single AcquireChanController::m_WalkSpeed
	float ___m_WalkSpeed_4;
	// System.Single AcquireChanController::m_RunSpeed
	float ___m_RunSpeed_5;
	// System.Single AcquireChanController::m_RotateSpeed
	float ___m_RotateSpeed_6;
	// System.Single AcquireChanController::m_JumpForce
	float ___m_JumpForce_7;
	// System.Single AcquireChanController::m_RunningStart
	float ___m_RunningStart_8;
	// UnityEngine.Rigidbody AcquireChanController::m_RigidBody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___m_RigidBody_9;
	// UnityEngine.Animator AcquireChanController::m_Animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_Animator_10;
	// System.Single AcquireChanController::m_MoveTime
	float ___m_MoveTime_11;
	// System.Single AcquireChanController::m_MoveSpeed
	float ___m_MoveSpeed_12;
	// System.Boolean AcquireChanController::m_IsGround
	bool ___m_IsGround_13;

public:
	inline static int32_t get_offset_of_m_WalkSpeed_4() { return static_cast<int32_t>(offsetof(AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794, ___m_WalkSpeed_4)); }
	inline float get_m_WalkSpeed_4() const { return ___m_WalkSpeed_4; }
	inline float* get_address_of_m_WalkSpeed_4() { return &___m_WalkSpeed_4; }
	inline void set_m_WalkSpeed_4(float value)
	{
		___m_WalkSpeed_4 = value;
	}

	inline static int32_t get_offset_of_m_RunSpeed_5() { return static_cast<int32_t>(offsetof(AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794, ___m_RunSpeed_5)); }
	inline float get_m_RunSpeed_5() const { return ___m_RunSpeed_5; }
	inline float* get_address_of_m_RunSpeed_5() { return &___m_RunSpeed_5; }
	inline void set_m_RunSpeed_5(float value)
	{
		___m_RunSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_RotateSpeed_6() { return static_cast<int32_t>(offsetof(AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794, ___m_RotateSpeed_6)); }
	inline float get_m_RotateSpeed_6() const { return ___m_RotateSpeed_6; }
	inline float* get_address_of_m_RotateSpeed_6() { return &___m_RotateSpeed_6; }
	inline void set_m_RotateSpeed_6(float value)
	{
		___m_RotateSpeed_6 = value;
	}

	inline static int32_t get_offset_of_m_JumpForce_7() { return static_cast<int32_t>(offsetof(AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794, ___m_JumpForce_7)); }
	inline float get_m_JumpForce_7() const { return ___m_JumpForce_7; }
	inline float* get_address_of_m_JumpForce_7() { return &___m_JumpForce_7; }
	inline void set_m_JumpForce_7(float value)
	{
		___m_JumpForce_7 = value;
	}

	inline static int32_t get_offset_of_m_RunningStart_8() { return static_cast<int32_t>(offsetof(AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794, ___m_RunningStart_8)); }
	inline float get_m_RunningStart_8() const { return ___m_RunningStart_8; }
	inline float* get_address_of_m_RunningStart_8() { return &___m_RunningStart_8; }
	inline void set_m_RunningStart_8(float value)
	{
		___m_RunningStart_8 = value;
	}

	inline static int32_t get_offset_of_m_RigidBody_9() { return static_cast<int32_t>(offsetof(AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794, ___m_RigidBody_9)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_m_RigidBody_9() const { return ___m_RigidBody_9; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_m_RigidBody_9() { return &___m_RigidBody_9; }
	inline void set_m_RigidBody_9(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___m_RigidBody_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RigidBody_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Animator_10() { return static_cast<int32_t>(offsetof(AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794, ___m_Animator_10)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_Animator_10() const { return ___m_Animator_10; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_Animator_10() { return &___m_Animator_10; }
	inline void set_m_Animator_10(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_Animator_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Animator_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_MoveTime_11() { return static_cast<int32_t>(offsetof(AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794, ___m_MoveTime_11)); }
	inline float get_m_MoveTime_11() const { return ___m_MoveTime_11; }
	inline float* get_address_of_m_MoveTime_11() { return &___m_MoveTime_11; }
	inline void set_m_MoveTime_11(float value)
	{
		___m_MoveTime_11 = value;
	}

	inline static int32_t get_offset_of_m_MoveSpeed_12() { return static_cast<int32_t>(offsetof(AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794, ___m_MoveSpeed_12)); }
	inline float get_m_MoveSpeed_12() const { return ___m_MoveSpeed_12; }
	inline float* get_address_of_m_MoveSpeed_12() { return &___m_MoveSpeed_12; }
	inline void set_m_MoveSpeed_12(float value)
	{
		___m_MoveSpeed_12 = value;
	}

	inline static int32_t get_offset_of_m_IsGround_13() { return static_cast<int32_t>(offsetof(AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794, ___m_IsGround_13)); }
	inline bool get_m_IsGround_13() const { return ___m_IsGround_13; }
	inline bool* get_address_of_m_IsGround_13() { return &___m_IsGround_13; }
	inline void set_m_IsGround_13(bool value)
	{
		___m_IsGround_13 = value;
	}
};


// ChannelSelector
struct  ChannelSelector_tB0A23474F199F2A0EB95C2154188D7F84AD5DD01  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String ChannelSelector::Channel
	String_t* ___Channel_4;

public:
	inline static int32_t get_offset_of_Channel_4() { return static_cast<int32_t>(offsetof(ChannelSelector_tB0A23474F199F2A0EB95C2154188D7F84AD5DD01, ___Channel_4)); }
	inline String_t* get_Channel_4() const { return ___Channel_4; }
	inline String_t** get_address_of_Channel_4() { return &___Channel_4; }
	inline void set_Channel_4(String_t* value)
	{
		___Channel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Channel_4), (void*)value);
	}
};


// ChatAppIdCheckerUI
struct  ChatAppIdCheckerUI_t1340C90B22E44EECD1D00DC60E9446F875931038  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text ChatAppIdCheckerUI::Description
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___Description_4;

public:
	inline static int32_t get_offset_of_Description_4() { return static_cast<int32_t>(offsetof(ChatAppIdCheckerUI_t1340C90B22E44EECD1D00DC60E9446F875931038, ___Description_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_Description_4() const { return ___Description_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_Description_4() { return &___Description_4; }
	inline void set_Description_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___Description_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Description_4), (void*)value);
	}
};


// ChatGui
struct  ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] ChatGui::ChannelsToJoinOnConnect
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___ChannelsToJoinOnConnect_4;
	// System.String[] ChatGui::FriendsList
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___FriendsList_5;
	// System.Int32 ChatGui::HistoryLengthToFetch
	int32_t ___HistoryLengthToFetch_6;
	// System.String ChatGui::<UserName>k__BackingField
	String_t* ___U3CUserNameU3Ek__BackingField_7;
	// System.String ChatGui::selectedChannelName
	String_t* ___selectedChannelName_8;
	// Photon.Chat.ChatClient ChatGui::chatClient
	ChatClient_t00238E132CA795D7F98C1DE6F433BB231F875505 * ___chatClient_9;
	// Photon.Realtime.AppSettings ChatGui::chatAppSettings
	AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * ___chatAppSettings_10;
	// UnityEngine.GameObject ChatGui::missingAppIdErrorPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___missingAppIdErrorPanel_11;
	// UnityEngine.GameObject ChatGui::ConnectingLabel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ConnectingLabel_12;
	// UnityEngine.RectTransform ChatGui::ChatPanel
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___ChatPanel_13;
	// UnityEngine.GameObject ChatGui::UserIdFormPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___UserIdFormPanel_14;
	// UnityEngine.UI.InputField ChatGui::InputFieldChat
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___InputFieldChat_15;
	// UnityEngine.UI.Text ChatGui::CurrentChannelText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___CurrentChannelText_16;
	// UnityEngine.UI.Toggle ChatGui::ChannelToggleToInstantiate
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___ChannelToggleToInstantiate_17;
	// UnityEngine.GameObject ChatGui::FriendListUiItemtoInstantiate
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___FriendListUiItemtoInstantiate_18;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UI.Toggle> ChatGui::channelToggles
	Dictionary_2_tA865CEFD08C2FAFE3889E26D574C8A433D8CB08F * ___channelToggles_19;
	// System.Collections.Generic.Dictionary`2<System.String,FriendItem> ChatGui::friendListItemLUT
	Dictionary_2_t031F0C3C4496EC540F1945D6B5DB1BCD9DEC6666 * ___friendListItemLUT_20;
	// System.Boolean ChatGui::ShowState
	bool ___ShowState_21;
	// UnityEngine.GameObject ChatGui::Title
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Title_22;
	// UnityEngine.UI.Text ChatGui::StateText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___StateText_23;
	// UnityEngine.UI.Text ChatGui::UserIdText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___UserIdText_24;
	// System.Int32 ChatGui::TestLength
	int32_t ___TestLength_26;
	// System.Byte[] ChatGui::testBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___testBytes_27;

public:
	inline static int32_t get_offset_of_ChannelsToJoinOnConnect_4() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___ChannelsToJoinOnConnect_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_ChannelsToJoinOnConnect_4() const { return ___ChannelsToJoinOnConnect_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_ChannelsToJoinOnConnect_4() { return &___ChannelsToJoinOnConnect_4; }
	inline void set_ChannelsToJoinOnConnect_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___ChannelsToJoinOnConnect_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChannelsToJoinOnConnect_4), (void*)value);
	}

	inline static int32_t get_offset_of_FriendsList_5() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___FriendsList_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_FriendsList_5() const { return ___FriendsList_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_FriendsList_5() { return &___FriendsList_5; }
	inline void set_FriendsList_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___FriendsList_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FriendsList_5), (void*)value);
	}

	inline static int32_t get_offset_of_HistoryLengthToFetch_6() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___HistoryLengthToFetch_6)); }
	inline int32_t get_HistoryLengthToFetch_6() const { return ___HistoryLengthToFetch_6; }
	inline int32_t* get_address_of_HistoryLengthToFetch_6() { return &___HistoryLengthToFetch_6; }
	inline void set_HistoryLengthToFetch_6(int32_t value)
	{
		___HistoryLengthToFetch_6 = value;
	}

	inline static int32_t get_offset_of_U3CUserNameU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___U3CUserNameU3Ek__BackingField_7)); }
	inline String_t* get_U3CUserNameU3Ek__BackingField_7() const { return ___U3CUserNameU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CUserNameU3Ek__BackingField_7() { return &___U3CUserNameU3Ek__BackingField_7; }
	inline void set_U3CUserNameU3Ek__BackingField_7(String_t* value)
	{
		___U3CUserNameU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CUserNameU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_selectedChannelName_8() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___selectedChannelName_8)); }
	inline String_t* get_selectedChannelName_8() const { return ___selectedChannelName_8; }
	inline String_t** get_address_of_selectedChannelName_8() { return &___selectedChannelName_8; }
	inline void set_selectedChannelName_8(String_t* value)
	{
		___selectedChannelName_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selectedChannelName_8), (void*)value);
	}

	inline static int32_t get_offset_of_chatClient_9() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___chatClient_9)); }
	inline ChatClient_t00238E132CA795D7F98C1DE6F433BB231F875505 * get_chatClient_9() const { return ___chatClient_9; }
	inline ChatClient_t00238E132CA795D7F98C1DE6F433BB231F875505 ** get_address_of_chatClient_9() { return &___chatClient_9; }
	inline void set_chatClient_9(ChatClient_t00238E132CA795D7F98C1DE6F433BB231F875505 * value)
	{
		___chatClient_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chatClient_9), (void*)value);
	}

	inline static int32_t get_offset_of_chatAppSettings_10() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___chatAppSettings_10)); }
	inline AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * get_chatAppSettings_10() const { return ___chatAppSettings_10; }
	inline AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 ** get_address_of_chatAppSettings_10() { return &___chatAppSettings_10; }
	inline void set_chatAppSettings_10(AppSettings_t6277B2AD0A574551E9746ED04FA848B0049A75A8 * value)
	{
		___chatAppSettings_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chatAppSettings_10), (void*)value);
	}

	inline static int32_t get_offset_of_missingAppIdErrorPanel_11() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___missingAppIdErrorPanel_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_missingAppIdErrorPanel_11() const { return ___missingAppIdErrorPanel_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_missingAppIdErrorPanel_11() { return &___missingAppIdErrorPanel_11; }
	inline void set_missingAppIdErrorPanel_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___missingAppIdErrorPanel_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___missingAppIdErrorPanel_11), (void*)value);
	}

	inline static int32_t get_offset_of_ConnectingLabel_12() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___ConnectingLabel_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ConnectingLabel_12() const { return ___ConnectingLabel_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ConnectingLabel_12() { return &___ConnectingLabel_12; }
	inline void set_ConnectingLabel_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ConnectingLabel_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConnectingLabel_12), (void*)value);
	}

	inline static int32_t get_offset_of_ChatPanel_13() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___ChatPanel_13)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_ChatPanel_13() const { return ___ChatPanel_13; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_ChatPanel_13() { return &___ChatPanel_13; }
	inline void set_ChatPanel_13(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___ChatPanel_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChatPanel_13), (void*)value);
	}

	inline static int32_t get_offset_of_UserIdFormPanel_14() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___UserIdFormPanel_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_UserIdFormPanel_14() const { return ___UserIdFormPanel_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_UserIdFormPanel_14() { return &___UserIdFormPanel_14; }
	inline void set_UserIdFormPanel_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___UserIdFormPanel_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UserIdFormPanel_14), (void*)value);
	}

	inline static int32_t get_offset_of_InputFieldChat_15() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___InputFieldChat_15)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_InputFieldChat_15() const { return ___InputFieldChat_15; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_InputFieldChat_15() { return &___InputFieldChat_15; }
	inline void set_InputFieldChat_15(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___InputFieldChat_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InputFieldChat_15), (void*)value);
	}

	inline static int32_t get_offset_of_CurrentChannelText_16() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___CurrentChannelText_16)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_CurrentChannelText_16() const { return ___CurrentChannelText_16; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_CurrentChannelText_16() { return &___CurrentChannelText_16; }
	inline void set_CurrentChannelText_16(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___CurrentChannelText_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CurrentChannelText_16), (void*)value);
	}

	inline static int32_t get_offset_of_ChannelToggleToInstantiate_17() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___ChannelToggleToInstantiate_17)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_ChannelToggleToInstantiate_17() const { return ___ChannelToggleToInstantiate_17; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_ChannelToggleToInstantiate_17() { return &___ChannelToggleToInstantiate_17; }
	inline void set_ChannelToggleToInstantiate_17(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___ChannelToggleToInstantiate_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChannelToggleToInstantiate_17), (void*)value);
	}

	inline static int32_t get_offset_of_FriendListUiItemtoInstantiate_18() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___FriendListUiItemtoInstantiate_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_FriendListUiItemtoInstantiate_18() const { return ___FriendListUiItemtoInstantiate_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_FriendListUiItemtoInstantiate_18() { return &___FriendListUiItemtoInstantiate_18; }
	inline void set_FriendListUiItemtoInstantiate_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___FriendListUiItemtoInstantiate_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FriendListUiItemtoInstantiate_18), (void*)value);
	}

	inline static int32_t get_offset_of_channelToggles_19() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___channelToggles_19)); }
	inline Dictionary_2_tA865CEFD08C2FAFE3889E26D574C8A433D8CB08F * get_channelToggles_19() const { return ___channelToggles_19; }
	inline Dictionary_2_tA865CEFD08C2FAFE3889E26D574C8A433D8CB08F ** get_address_of_channelToggles_19() { return &___channelToggles_19; }
	inline void set_channelToggles_19(Dictionary_2_tA865CEFD08C2FAFE3889E26D574C8A433D8CB08F * value)
	{
		___channelToggles_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___channelToggles_19), (void*)value);
	}

	inline static int32_t get_offset_of_friendListItemLUT_20() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___friendListItemLUT_20)); }
	inline Dictionary_2_t031F0C3C4496EC540F1945D6B5DB1BCD9DEC6666 * get_friendListItemLUT_20() const { return ___friendListItemLUT_20; }
	inline Dictionary_2_t031F0C3C4496EC540F1945D6B5DB1BCD9DEC6666 ** get_address_of_friendListItemLUT_20() { return &___friendListItemLUT_20; }
	inline void set_friendListItemLUT_20(Dictionary_2_t031F0C3C4496EC540F1945D6B5DB1BCD9DEC6666 * value)
	{
		___friendListItemLUT_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___friendListItemLUT_20), (void*)value);
	}

	inline static int32_t get_offset_of_ShowState_21() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___ShowState_21)); }
	inline bool get_ShowState_21() const { return ___ShowState_21; }
	inline bool* get_address_of_ShowState_21() { return &___ShowState_21; }
	inline void set_ShowState_21(bool value)
	{
		___ShowState_21 = value;
	}

	inline static int32_t get_offset_of_Title_22() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___Title_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Title_22() const { return ___Title_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Title_22() { return &___Title_22; }
	inline void set_Title_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Title_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Title_22), (void*)value);
	}

	inline static int32_t get_offset_of_StateText_23() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___StateText_23)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_StateText_23() const { return ___StateText_23; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_StateText_23() { return &___StateText_23; }
	inline void set_StateText_23(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___StateText_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StateText_23), (void*)value);
	}

	inline static int32_t get_offset_of_UserIdText_24() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___UserIdText_24)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_UserIdText_24() const { return ___UserIdText_24; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_UserIdText_24() { return &___UserIdText_24; }
	inline void set_UserIdText_24(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___UserIdText_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UserIdText_24), (void*)value);
	}

	inline static int32_t get_offset_of_TestLength_26() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___TestLength_26)); }
	inline int32_t get_TestLength_26() const { return ___TestLength_26; }
	inline int32_t* get_address_of_TestLength_26() { return &___TestLength_26; }
	inline void set_TestLength_26(int32_t value)
	{
		___TestLength_26 = value;
	}

	inline static int32_t get_offset_of_testBytes_27() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE, ___testBytes_27)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_testBytes_27() const { return ___testBytes_27; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_testBytes_27() { return &___testBytes_27; }
	inline void set_testBytes_27(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___testBytes_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___testBytes_27), (void*)value);
	}
};

struct ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE_StaticFields
{
public:
	// System.String ChatGui::HelpText
	String_t* ___HelpText_25;

public:
	inline static int32_t get_offset_of_HelpText_25() { return static_cast<int32_t>(offsetof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE_StaticFields, ___HelpText_25)); }
	inline String_t* get_HelpText_25() const { return ___HelpText_25; }
	inline String_t** get_address_of_HelpText_25() { return &___HelpText_25; }
	inline void set_HelpText_25(String_t* value)
	{
		___HelpText_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HelpText_25), (void*)value);
	}
};


// DragObject
struct  DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera DragObject::arcamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___arcamera_4;
	// UnityEngine.XR.ARFoundation.ARRaycastManager DragObject::arRayMng
	ARRaycastManager_t81A9513150BA5BE536DF064F1C6DE73349A60BE7 * ___arRayMng_5;
	// System.Boolean DragObject::onTouchHold
	bool ___onTouchHold_6;
	// UnityEngine.Vector2 DragObject::touchPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___touchPosition_7;
	// UnityEngine.GameObject DragObject::Bow
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Bow_8;
	// UnityEngine.GameObject DragObject::Txt
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Txt_9;
	// UnityEngine.AudioSource DragObject::bowaudio
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___bowaudio_10;
	// UnityEngine.AudioClip DragObject::bowFx
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___bowFx_11;
	// Photon.Pun.PhotonView DragObject::DragPv
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___DragPv_12;

public:
	inline static int32_t get_offset_of_arcamera_4() { return static_cast<int32_t>(offsetof(DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857, ___arcamera_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_arcamera_4() const { return ___arcamera_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_arcamera_4() { return &___arcamera_4; }
	inline void set_arcamera_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___arcamera_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arcamera_4), (void*)value);
	}

	inline static int32_t get_offset_of_arRayMng_5() { return static_cast<int32_t>(offsetof(DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857, ___arRayMng_5)); }
	inline ARRaycastManager_t81A9513150BA5BE536DF064F1C6DE73349A60BE7 * get_arRayMng_5() const { return ___arRayMng_5; }
	inline ARRaycastManager_t81A9513150BA5BE536DF064F1C6DE73349A60BE7 ** get_address_of_arRayMng_5() { return &___arRayMng_5; }
	inline void set_arRayMng_5(ARRaycastManager_t81A9513150BA5BE536DF064F1C6DE73349A60BE7 * value)
	{
		___arRayMng_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arRayMng_5), (void*)value);
	}

	inline static int32_t get_offset_of_onTouchHold_6() { return static_cast<int32_t>(offsetof(DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857, ___onTouchHold_6)); }
	inline bool get_onTouchHold_6() const { return ___onTouchHold_6; }
	inline bool* get_address_of_onTouchHold_6() { return &___onTouchHold_6; }
	inline void set_onTouchHold_6(bool value)
	{
		___onTouchHold_6 = value;
	}

	inline static int32_t get_offset_of_touchPosition_7() { return static_cast<int32_t>(offsetof(DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857, ___touchPosition_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_touchPosition_7() const { return ___touchPosition_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_touchPosition_7() { return &___touchPosition_7; }
	inline void set_touchPosition_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___touchPosition_7 = value;
	}

	inline static int32_t get_offset_of_Bow_8() { return static_cast<int32_t>(offsetof(DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857, ___Bow_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Bow_8() const { return ___Bow_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Bow_8() { return &___Bow_8; }
	inline void set_Bow_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Bow_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Bow_8), (void*)value);
	}

	inline static int32_t get_offset_of_Txt_9() { return static_cast<int32_t>(offsetof(DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857, ___Txt_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Txt_9() const { return ___Txt_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Txt_9() { return &___Txt_9; }
	inline void set_Txt_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Txt_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Txt_9), (void*)value);
	}

	inline static int32_t get_offset_of_bowaudio_10() { return static_cast<int32_t>(offsetof(DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857, ___bowaudio_10)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_bowaudio_10() const { return ___bowaudio_10; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_bowaudio_10() { return &___bowaudio_10; }
	inline void set_bowaudio_10(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___bowaudio_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowaudio_10), (void*)value);
	}

	inline static int32_t get_offset_of_bowFx_11() { return static_cast<int32_t>(offsetof(DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857, ___bowFx_11)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_bowFx_11() const { return ___bowFx_11; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_bowFx_11() { return &___bowFx_11; }
	inline void set_bowFx_11(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___bowFx_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowFx_11), (void*)value);
	}

	inline static int32_t get_offset_of_DragPv_12() { return static_cast<int32_t>(offsetof(DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857, ___DragPv_12)); }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * get_DragPv_12() const { return ___DragPv_12; }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** get_address_of_DragPv_12() { return &___DragPv_12; }
	inline void set_DragPv_12(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		___DragPv_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DragPv_12), (void*)value);
	}
};

struct DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit> DragObject::hits
	List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * ___hits_13;

public:
	inline static int32_t get_offset_of_hits_13() { return static_cast<int32_t>(offsetof(DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857_StaticFields, ___hits_13)); }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * get_hits_13() const { return ___hits_13; }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 ** get_address_of_hits_13() { return &___hits_13; }
	inline void set_hits_13(List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * value)
	{
		___hits_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hits_13), (void*)value);
	}
};


// EndCredit
struct  EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject EndCredit::DisButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DisButton_4;
	// System.Int32 EndCredit::menuScene
	int32_t ___menuScene_5;
	// UnityEngine.AudioSource EndCredit::audioPlay
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioPlay_6;
	// UnityEngine.AudioClip EndCredit::greeting
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___greeting_7;
	// UnityEngine.AudioClip EndCredit::crying
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___crying_8;
	// UnityEngine.UI.Text EndCredit::checkTxt
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___checkTxt_9;

public:
	inline static int32_t get_offset_of_DisButton_4() { return static_cast<int32_t>(offsetof(EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D, ___DisButton_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DisButton_4() const { return ___DisButton_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DisButton_4() { return &___DisButton_4; }
	inline void set_DisButton_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DisButton_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DisButton_4), (void*)value);
	}

	inline static int32_t get_offset_of_menuScene_5() { return static_cast<int32_t>(offsetof(EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D, ___menuScene_5)); }
	inline int32_t get_menuScene_5() const { return ___menuScene_5; }
	inline int32_t* get_address_of_menuScene_5() { return &___menuScene_5; }
	inline void set_menuScene_5(int32_t value)
	{
		___menuScene_5 = value;
	}

	inline static int32_t get_offset_of_audioPlay_6() { return static_cast<int32_t>(offsetof(EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D, ___audioPlay_6)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioPlay_6() const { return ___audioPlay_6; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioPlay_6() { return &___audioPlay_6; }
	inline void set_audioPlay_6(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioPlay_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioPlay_6), (void*)value);
	}

	inline static int32_t get_offset_of_greeting_7() { return static_cast<int32_t>(offsetof(EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D, ___greeting_7)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_greeting_7() const { return ___greeting_7; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_greeting_7() { return &___greeting_7; }
	inline void set_greeting_7(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___greeting_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___greeting_7), (void*)value);
	}

	inline static int32_t get_offset_of_crying_8() { return static_cast<int32_t>(offsetof(EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D, ___crying_8)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_crying_8() const { return ___crying_8; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_crying_8() { return &___crying_8; }
	inline void set_crying_8(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___crying_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___crying_8), (void*)value);
	}

	inline static int32_t get_offset_of_checkTxt_9() { return static_cast<int32_t>(offsetof(EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D, ___checkTxt_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_checkTxt_9() const { return ___checkTxt_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_checkTxt_9() { return &___checkTxt_9; }
	inline void set_checkTxt_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___checkTxt_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___checkTxt_9), (void*)value);
	}
};


// ExampleBowUserControl
struct  ExampleBowUserControl_tCC12B82A59C26FDF6E7E5B690B3D1F8D83BDFDC1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator ExampleBowUserControl::animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___animator_4;
	// System.Int32 ExampleBowUserControl::paramHash
	int32_t ___paramHash_5;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(ExampleBowUserControl_tCC12B82A59C26FDF6E7E5B690B3D1F8D83BDFDC1, ___animator_4)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_animator_4() const { return ___animator_4; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animator_4), (void*)value);
	}

	inline static int32_t get_offset_of_paramHash_5() { return static_cast<int32_t>(offsetof(ExampleBowUserControl_tCC12B82A59C26FDF6E7E5B690B3D1F8D83BDFDC1, ___paramHash_5)); }
	inline int32_t get_paramHash_5() const { return ___paramHash_5; }
	inline int32_t* get_address_of_paramHash_5() { return &___paramHash_5; }
	inline void set_paramHash_5(int32_t value)
	{
		___paramHash_5 = value;
	}
};


// FriendItem
struct  FriendItem_tE22AE289C68E0B00B4E35C59CC745C6FBDE9B972  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text FriendItem::NameLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___NameLabel_4;
	// UnityEngine.UI.Text FriendItem::StatusLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___StatusLabel_5;
	// UnityEngine.UI.Text FriendItem::Health
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___Health_6;

public:
	inline static int32_t get_offset_of_NameLabel_4() { return static_cast<int32_t>(offsetof(FriendItem_tE22AE289C68E0B00B4E35C59CC745C6FBDE9B972, ___NameLabel_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_NameLabel_4() const { return ___NameLabel_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_NameLabel_4() { return &___NameLabel_4; }
	inline void set_NameLabel_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___NameLabel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NameLabel_4), (void*)value);
	}

	inline static int32_t get_offset_of_StatusLabel_5() { return static_cast<int32_t>(offsetof(FriendItem_tE22AE289C68E0B00B4E35C59CC745C6FBDE9B972, ___StatusLabel_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_StatusLabel_5() const { return ___StatusLabel_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_StatusLabel_5() { return &___StatusLabel_5; }
	inline void set_StatusLabel_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___StatusLabel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StatusLabel_5), (void*)value);
	}

	inline static int32_t get_offset_of_Health_6() { return static_cast<int32_t>(offsetof(FriendItem_tE22AE289C68E0B00B4E35C59CC745C6FBDE9B972, ___Health_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_Health_6() const { return ___Health_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_Health_6() { return &___Health_6; }
	inline void set_Health_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___Health_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Health_6), (void*)value);
	}
};


// GameSetuoController
struct  GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject GameSetuoController::delayCancelButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___delayCancelButton_4;
	// System.Int32 GameSetuoController::menuSceneIndex
	int32_t ___menuSceneIndex_5;
	// UnityEngine.GameObject GameSetuoController::animationTxt
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___animationTxt_6;
	// UnityEngine.GameObject GameSetuoController::holdTxt
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___holdTxt_7;
	// UnityEngine.Camera GameSetuoController::arCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___arCamera_8;
	// UnityEngine.GameObject GameSetuoController::model
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___model_9;
	// UnityEngine.XR.ARFoundation.ARRaycastManager GameSetuoController::arRayMng
	ARRaycastManager_t81A9513150BA5BE536DF064F1C6DE73349A60BE7 * ___arRayMng_10;
	// UnityEngine.Vector3 GameSetuoController::camOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___camOffset_11;
	// UnityEngine.GameObject GameSetuoController::question
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___question_12;
	// UnityEngine.GameObject GameSetuoController::mssInput
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mssInput_13;
	// UnityEngine.GameObject GameSetuoController::waiting
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___waiting_14;
	// System.Boolean GameSetuoController::swap
	bool ___swap_15;
	// Photon.Pun.PhotonView GameSetuoController::myPV
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___myPV_16;
	// System.String GameSetuoController::mssChatTxt
	String_t* ___mssChatTxt_17;
	// UnityEngine.GameObject GameSetuoController::chatTxt
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___chatTxt_18;
	// UnityEngine.GameObject GameSetuoController::forgiveQuestion
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___forgiveQuestion_19;
	// UnityEngine.AudioSource GameSetuoController::audiosource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audiosource_20;
	// UnityEngine.AudioClip GameSetuoController::bgSound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___bgSound_21;
	// UnityEngine.AudioSource GameSetuoController::BgAudio
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___BgAudio_22;

public:
	inline static int32_t get_offset_of_delayCancelButton_4() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___delayCancelButton_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_delayCancelButton_4() const { return ___delayCancelButton_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_delayCancelButton_4() { return &___delayCancelButton_4; }
	inline void set_delayCancelButton_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___delayCancelButton_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delayCancelButton_4), (void*)value);
	}

	inline static int32_t get_offset_of_menuSceneIndex_5() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___menuSceneIndex_5)); }
	inline int32_t get_menuSceneIndex_5() const { return ___menuSceneIndex_5; }
	inline int32_t* get_address_of_menuSceneIndex_5() { return &___menuSceneIndex_5; }
	inline void set_menuSceneIndex_5(int32_t value)
	{
		___menuSceneIndex_5 = value;
	}

	inline static int32_t get_offset_of_animationTxt_6() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___animationTxt_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_animationTxt_6() const { return ___animationTxt_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_animationTxt_6() { return &___animationTxt_6; }
	inline void set_animationTxt_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___animationTxt_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animationTxt_6), (void*)value);
	}

	inline static int32_t get_offset_of_holdTxt_7() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___holdTxt_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_holdTxt_7() const { return ___holdTxt_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_holdTxt_7() { return &___holdTxt_7; }
	inline void set_holdTxt_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___holdTxt_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___holdTxt_7), (void*)value);
	}

	inline static int32_t get_offset_of_arCamera_8() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___arCamera_8)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_arCamera_8() const { return ___arCamera_8; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_arCamera_8() { return &___arCamera_8; }
	inline void set_arCamera_8(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___arCamera_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arCamera_8), (void*)value);
	}

	inline static int32_t get_offset_of_model_9() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___model_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_model_9() const { return ___model_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_model_9() { return &___model_9; }
	inline void set_model_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___model_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___model_9), (void*)value);
	}

	inline static int32_t get_offset_of_arRayMng_10() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___arRayMng_10)); }
	inline ARRaycastManager_t81A9513150BA5BE536DF064F1C6DE73349A60BE7 * get_arRayMng_10() const { return ___arRayMng_10; }
	inline ARRaycastManager_t81A9513150BA5BE536DF064F1C6DE73349A60BE7 ** get_address_of_arRayMng_10() { return &___arRayMng_10; }
	inline void set_arRayMng_10(ARRaycastManager_t81A9513150BA5BE536DF064F1C6DE73349A60BE7 * value)
	{
		___arRayMng_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arRayMng_10), (void*)value);
	}

	inline static int32_t get_offset_of_camOffset_11() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___camOffset_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_camOffset_11() const { return ___camOffset_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_camOffset_11() { return &___camOffset_11; }
	inline void set_camOffset_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___camOffset_11 = value;
	}

	inline static int32_t get_offset_of_question_12() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___question_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_question_12() const { return ___question_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_question_12() { return &___question_12; }
	inline void set_question_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___question_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___question_12), (void*)value);
	}

	inline static int32_t get_offset_of_mssInput_13() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___mssInput_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mssInput_13() const { return ___mssInput_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mssInput_13() { return &___mssInput_13; }
	inline void set_mssInput_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mssInput_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mssInput_13), (void*)value);
	}

	inline static int32_t get_offset_of_waiting_14() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___waiting_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_waiting_14() const { return ___waiting_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_waiting_14() { return &___waiting_14; }
	inline void set_waiting_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___waiting_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___waiting_14), (void*)value);
	}

	inline static int32_t get_offset_of_swap_15() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___swap_15)); }
	inline bool get_swap_15() const { return ___swap_15; }
	inline bool* get_address_of_swap_15() { return &___swap_15; }
	inline void set_swap_15(bool value)
	{
		___swap_15 = value;
	}

	inline static int32_t get_offset_of_myPV_16() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___myPV_16)); }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * get_myPV_16() const { return ___myPV_16; }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** get_address_of_myPV_16() { return &___myPV_16; }
	inline void set_myPV_16(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		___myPV_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myPV_16), (void*)value);
	}

	inline static int32_t get_offset_of_mssChatTxt_17() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___mssChatTxt_17)); }
	inline String_t* get_mssChatTxt_17() const { return ___mssChatTxt_17; }
	inline String_t** get_address_of_mssChatTxt_17() { return &___mssChatTxt_17; }
	inline void set_mssChatTxt_17(String_t* value)
	{
		___mssChatTxt_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mssChatTxt_17), (void*)value);
	}

	inline static int32_t get_offset_of_chatTxt_18() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___chatTxt_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_chatTxt_18() const { return ___chatTxt_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_chatTxt_18() { return &___chatTxt_18; }
	inline void set_chatTxt_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___chatTxt_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chatTxt_18), (void*)value);
	}

	inline static int32_t get_offset_of_forgiveQuestion_19() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___forgiveQuestion_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_forgiveQuestion_19() const { return ___forgiveQuestion_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_forgiveQuestion_19() { return &___forgiveQuestion_19; }
	inline void set_forgiveQuestion_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___forgiveQuestion_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___forgiveQuestion_19), (void*)value);
	}

	inline static int32_t get_offset_of_audiosource_20() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___audiosource_20)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audiosource_20() const { return ___audiosource_20; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audiosource_20() { return &___audiosource_20; }
	inline void set_audiosource_20(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audiosource_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audiosource_20), (void*)value);
	}

	inline static int32_t get_offset_of_bgSound_21() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___bgSound_21)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_bgSound_21() const { return ___bgSound_21; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_bgSound_21() { return &___bgSound_21; }
	inline void set_bgSound_21(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___bgSound_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bgSound_21), (void*)value);
	}

	inline static int32_t get_offset_of_BgAudio_22() { return static_cast<int32_t>(offsetof(GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA, ___BgAudio_22)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_BgAudio_22() const { return ___BgAudio_22; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_BgAudio_22() { return &___BgAudio_22; }
	inline void set_BgAudio_22(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___BgAudio_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BgAudio_22), (void*)value);
	}
};


// IgnoreUiRaycastWhenInactive
struct  IgnoreUiRaycastWhenInactive_tA341D2C70F8A63B7CFBC412B2B41A1A04060A8E6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// MoveAndShoot
struct  MoveAndShoot_t30B182CC8E3D84C59BAE0D52BF2B0483191CFC94  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single MoveAndShoot::speed
	float ___speed_4;
	// System.Single MoveAndShoot::speedRot
	float ___speedRot_5;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(MoveAndShoot_t30B182CC8E3D84C59BAE0D52BF2B0483191CFC94, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_speedRot_5() { return static_cast<int32_t>(offsetof(MoveAndShoot_t30B182CC8E3D84C59BAE0D52BF2B0483191CFC94, ___speedRot_5)); }
	inline float get_speedRot_5() const { return ___speedRot_5; }
	inline float* get_address_of_speedRot_5() { return &___speedRot_5; }
	inline void set_speedRot_5(float value)
	{
		___speedRot_5 = value;
	}
};


// NamePickGui
struct  NamePickGui_tC365A70D500B67AF8B9DD2520A525CA1AA135AAD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// ChatGui NamePickGui::chatNewComponent
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE * ___chatNewComponent_5;
	// UnityEngine.UI.InputField NamePickGui::idInput
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___idInput_6;

public:
	inline static int32_t get_offset_of_chatNewComponent_5() { return static_cast<int32_t>(offsetof(NamePickGui_tC365A70D500B67AF8B9DD2520A525CA1AA135AAD, ___chatNewComponent_5)); }
	inline ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE * get_chatNewComponent_5() const { return ___chatNewComponent_5; }
	inline ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE ** get_address_of_chatNewComponent_5() { return &___chatNewComponent_5; }
	inline void set_chatNewComponent_5(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE * value)
	{
		___chatNewComponent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chatNewComponent_5), (void*)value);
	}

	inline static int32_t get_offset_of_idInput_6() { return static_cast<int32_t>(offsetof(NamePickGui_tC365A70D500B67AF8B9DD2520A525CA1AA135AAD, ___idInput_6)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_idInput_6() const { return ___idInput_6; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_idInput_6() { return &___idInput_6; }
	inline void set_idInput_6(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___idInput_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___idInput_6), (void*)value);
	}
};


// Photon.Chat.UtilityScripts.EventSystemSpawner
struct  EventSystemSpawner_tBD63CD17625F56A1597D4C873FFD361924446474  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// Photon.Chat.UtilityScripts.OnStartDelete
struct  OnStartDelete_t89163FA953176DC83BBD1291768C4FB034ED9692  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// Photon.Chat.UtilityScripts.TextButtonTransition
struct  TextButtonTransition_t427318D1060DFF7706A60FB1475FEEBFB52941DB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Photon.Chat.UtilityScripts.TextButtonTransition::_text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____text_4;
	// UnityEngine.UI.Selectable Photon.Chat.UtilityScripts.TextButtonTransition::Selectable
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___Selectable_5;
	// UnityEngine.Color Photon.Chat.UtilityScripts.TextButtonTransition::NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___NormalColor_6;
	// UnityEngine.Color Photon.Chat.UtilityScripts.TextButtonTransition::HoverColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___HoverColor_7;

public:
	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(TextButtonTransition_t427318D1060DFF7706A60FB1475FEEBFB52941DB, ____text_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__text_4() const { return ____text_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____text_4), (void*)value);
	}

	inline static int32_t get_offset_of_Selectable_5() { return static_cast<int32_t>(offsetof(TextButtonTransition_t427318D1060DFF7706A60FB1475FEEBFB52941DB, ___Selectable_5)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_Selectable_5() const { return ___Selectable_5; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_Selectable_5() { return &___Selectable_5; }
	inline void set_Selectable_5(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___Selectable_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Selectable_5), (void*)value);
	}

	inline static int32_t get_offset_of_NormalColor_6() { return static_cast<int32_t>(offsetof(TextButtonTransition_t427318D1060DFF7706A60FB1475FEEBFB52941DB, ___NormalColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_NormalColor_6() const { return ___NormalColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_NormalColor_6() { return &___NormalColor_6; }
	inline void set_NormalColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___NormalColor_6 = value;
	}

	inline static int32_t get_offset_of_HoverColor_7() { return static_cast<int32_t>(offsetof(TextButtonTransition_t427318D1060DFF7706A60FB1475FEEBFB52941DB, ___HoverColor_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_HoverColor_7() const { return ___HoverColor_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_HoverColor_7() { return &___HoverColor_7; }
	inline void set_HoverColor_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___HoverColor_7 = value;
	}
};


// Photon.Chat.UtilityScripts.TextToggleIsOnTransition
struct  TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Toggle Photon.Chat.UtilityScripts.TextToggleIsOnTransition::toggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ___toggle_4;
	// UnityEngine.UI.Text Photon.Chat.UtilityScripts.TextToggleIsOnTransition::_text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____text_5;
	// UnityEngine.Color Photon.Chat.UtilityScripts.TextToggleIsOnTransition::NormalOnColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___NormalOnColor_6;
	// UnityEngine.Color Photon.Chat.UtilityScripts.TextToggleIsOnTransition::NormalOffColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___NormalOffColor_7;
	// UnityEngine.Color Photon.Chat.UtilityScripts.TextToggleIsOnTransition::HoverOnColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___HoverOnColor_8;
	// UnityEngine.Color Photon.Chat.UtilityScripts.TextToggleIsOnTransition::HoverOffColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___HoverOffColor_9;
	// System.Boolean Photon.Chat.UtilityScripts.TextToggleIsOnTransition::isHover
	bool ___isHover_10;

public:
	inline static int32_t get_offset_of_toggle_4() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA, ___toggle_4)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get_toggle_4() const { return ___toggle_4; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of_toggle_4() { return &___toggle_4; }
	inline void set_toggle_4(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		___toggle_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toggle_4), (void*)value);
	}

	inline static int32_t get_offset_of__text_5() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA, ____text_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__text_5() const { return ____text_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__text_5() { return &____text_5; }
	inline void set__text_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____text_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____text_5), (void*)value);
	}

	inline static int32_t get_offset_of_NormalOnColor_6() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA, ___NormalOnColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_NormalOnColor_6() const { return ___NormalOnColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_NormalOnColor_6() { return &___NormalOnColor_6; }
	inline void set_NormalOnColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___NormalOnColor_6 = value;
	}

	inline static int32_t get_offset_of_NormalOffColor_7() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA, ___NormalOffColor_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_NormalOffColor_7() const { return ___NormalOffColor_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_NormalOffColor_7() { return &___NormalOffColor_7; }
	inline void set_NormalOffColor_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___NormalOffColor_7 = value;
	}

	inline static int32_t get_offset_of_HoverOnColor_8() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA, ___HoverOnColor_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_HoverOnColor_8() const { return ___HoverOnColor_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_HoverOnColor_8() { return &___HoverOnColor_8; }
	inline void set_HoverOnColor_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___HoverOnColor_8 = value;
	}

	inline static int32_t get_offset_of_HoverOffColor_9() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA, ___HoverOffColor_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_HoverOffColor_9() const { return ___HoverOffColor_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_HoverOffColor_9() { return &___HoverOffColor_9; }
	inline void set_HoverOffColor_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___HoverOffColor_9 = value;
	}

	inline static int32_t get_offset_of_isHover_10() { return static_cast<int32_t>(offsetof(TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA, ___isHover_10)); }
	inline bool get_isHover_10() const { return ___isHover_10; }
	inline bool* get_address_of_isHover_10() { return &___isHover_10; }
	inline void set_isHover_10(bool value)
	{
		___isHover_10 = value;
	}
};


// Photon.Pun.Demo.Asteroids.Asteroid
struct  Asteroid_tAF18F8357F6F3A67E52FA518AD1B85DE84579424  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Photon.Pun.Demo.Asteroids.Asteroid::isLargeAsteroid
	bool ___isLargeAsteroid_4;
	// System.Boolean Photon.Pun.Demo.Asteroids.Asteroid::isDestroyed
	bool ___isDestroyed_5;
	// Photon.Pun.PhotonView Photon.Pun.Demo.Asteroids.Asteroid::photonView
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___photonView_6;
	// UnityEngine.Rigidbody Photon.Pun.Demo.Asteroids.Asteroid::rigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___rigidbody_7;

public:
	inline static int32_t get_offset_of_isLargeAsteroid_4() { return static_cast<int32_t>(offsetof(Asteroid_tAF18F8357F6F3A67E52FA518AD1B85DE84579424, ___isLargeAsteroid_4)); }
	inline bool get_isLargeAsteroid_4() const { return ___isLargeAsteroid_4; }
	inline bool* get_address_of_isLargeAsteroid_4() { return &___isLargeAsteroid_4; }
	inline void set_isLargeAsteroid_4(bool value)
	{
		___isLargeAsteroid_4 = value;
	}

	inline static int32_t get_offset_of_isDestroyed_5() { return static_cast<int32_t>(offsetof(Asteroid_tAF18F8357F6F3A67E52FA518AD1B85DE84579424, ___isDestroyed_5)); }
	inline bool get_isDestroyed_5() const { return ___isDestroyed_5; }
	inline bool* get_address_of_isDestroyed_5() { return &___isDestroyed_5; }
	inline void set_isDestroyed_5(bool value)
	{
		___isDestroyed_5 = value;
	}

	inline static int32_t get_offset_of_photonView_6() { return static_cast<int32_t>(offsetof(Asteroid_tAF18F8357F6F3A67E52FA518AD1B85DE84579424, ___photonView_6)); }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * get_photonView_6() const { return ___photonView_6; }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** get_address_of_photonView_6() { return &___photonView_6; }
	inline void set_photonView_6(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		___photonView_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___photonView_6), (void*)value);
	}

	inline static int32_t get_offset_of_rigidbody_7() { return static_cast<int32_t>(offsetof(Asteroid_tAF18F8357F6F3A67E52FA518AD1B85DE84579424, ___rigidbody_7)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_rigidbody_7() const { return ___rigidbody_7; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_rigidbody_7() { return &___rigidbody_7; }
	inline void set_rigidbody_7(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___rigidbody_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rigidbody_7), (void*)value);
	}
};


// Photon.Pun.Demo.Asteroids.Bullet
struct  Bullet_tBE11F9632642885D1ADCB036EE0E74CC13D9706A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Photon.Realtime.Player Photon.Pun.Demo.Asteroids.Bullet::<Owner>k__BackingField
	Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * ___U3COwnerU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3COwnerU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Bullet_tBE11F9632642885D1ADCB036EE0E74CC13D9706A, ___U3COwnerU3Ek__BackingField_4)); }
	inline Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * get_U3COwnerU3Ek__BackingField_4() const { return ___U3COwnerU3Ek__BackingField_4; }
	inline Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 ** get_address_of_U3COwnerU3Ek__BackingField_4() { return &___U3COwnerU3Ek__BackingField_4; }
	inline void set_U3COwnerU3Ek__BackingField_4(Player_tFB06F12211DD89BEE90AD848E6C7BD9D889F1202 * value)
	{
		___U3COwnerU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COwnerU3Ek__BackingField_4), (void*)value);
	}
};


// Photon.Pun.Demo.Asteroids.LobbyTopPanel
struct  LobbyTopPanel_t78575A2615C6AEEE06DD68E2095C8CF9C8B21A5F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Photon.Pun.Demo.Asteroids.LobbyTopPanel::connectionStatusMessage
	String_t* ___connectionStatusMessage_4;
	// UnityEngine.UI.Text Photon.Pun.Demo.Asteroids.LobbyTopPanel::ConnectionStatusText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___ConnectionStatusText_5;

public:
	inline static int32_t get_offset_of_connectionStatusMessage_4() { return static_cast<int32_t>(offsetof(LobbyTopPanel_t78575A2615C6AEEE06DD68E2095C8CF9C8B21A5F, ___connectionStatusMessage_4)); }
	inline String_t* get_connectionStatusMessage_4() const { return ___connectionStatusMessage_4; }
	inline String_t** get_address_of_connectionStatusMessage_4() { return &___connectionStatusMessage_4; }
	inline void set_connectionStatusMessage_4(String_t* value)
	{
		___connectionStatusMessage_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___connectionStatusMessage_4), (void*)value);
	}

	inline static int32_t get_offset_of_ConnectionStatusText_5() { return static_cast<int32_t>(offsetof(LobbyTopPanel_t78575A2615C6AEEE06DD68E2095C8CF9C8B21A5F, ___ConnectionStatusText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_ConnectionStatusText_5() const { return ___ConnectionStatusText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_ConnectionStatusText_5() { return &___ConnectionStatusText_5; }
	inline void set_ConnectionStatusText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___ConnectionStatusText_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConnectionStatusText_5), (void*)value);
	}
};


// Photon.Pun.Demo.Asteroids.PlayerListEntry
struct  PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Asteroids.PlayerListEntry::PlayerNameText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___PlayerNameText_4;
	// UnityEngine.UI.Image Photon.Pun.Demo.Asteroids.PlayerListEntry::PlayerColorImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___PlayerColorImage_5;
	// UnityEngine.UI.Button Photon.Pun.Demo.Asteroids.PlayerListEntry::PlayerReadyButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___PlayerReadyButton_6;
	// UnityEngine.UI.Image Photon.Pun.Demo.Asteroids.PlayerListEntry::PlayerReadyImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___PlayerReadyImage_7;
	// System.Int32 Photon.Pun.Demo.Asteroids.PlayerListEntry::ownerId
	int32_t ___ownerId_8;
	// System.Boolean Photon.Pun.Demo.Asteroids.PlayerListEntry::isPlayerReady
	bool ___isPlayerReady_9;

public:
	inline static int32_t get_offset_of_PlayerNameText_4() { return static_cast<int32_t>(offsetof(PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8, ___PlayerNameText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_PlayerNameText_4() const { return ___PlayerNameText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_PlayerNameText_4() { return &___PlayerNameText_4; }
	inline void set_PlayerNameText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___PlayerNameText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerNameText_4), (void*)value);
	}

	inline static int32_t get_offset_of_PlayerColorImage_5() { return static_cast<int32_t>(offsetof(PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8, ___PlayerColorImage_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_PlayerColorImage_5() const { return ___PlayerColorImage_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_PlayerColorImage_5() { return &___PlayerColorImage_5; }
	inline void set_PlayerColorImage_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___PlayerColorImage_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerColorImage_5), (void*)value);
	}

	inline static int32_t get_offset_of_PlayerReadyButton_6() { return static_cast<int32_t>(offsetof(PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8, ___PlayerReadyButton_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_PlayerReadyButton_6() const { return ___PlayerReadyButton_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_PlayerReadyButton_6() { return &___PlayerReadyButton_6; }
	inline void set_PlayerReadyButton_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___PlayerReadyButton_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerReadyButton_6), (void*)value);
	}

	inline static int32_t get_offset_of_PlayerReadyImage_7() { return static_cast<int32_t>(offsetof(PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8, ___PlayerReadyImage_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_PlayerReadyImage_7() const { return ___PlayerReadyImage_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_PlayerReadyImage_7() { return &___PlayerReadyImage_7; }
	inline void set_PlayerReadyImage_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___PlayerReadyImage_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerReadyImage_7), (void*)value);
	}

	inline static int32_t get_offset_of_ownerId_8() { return static_cast<int32_t>(offsetof(PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8, ___ownerId_8)); }
	inline int32_t get_ownerId_8() const { return ___ownerId_8; }
	inline int32_t* get_address_of_ownerId_8() { return &___ownerId_8; }
	inline void set_ownerId_8(int32_t value)
	{
		___ownerId_8 = value;
	}

	inline static int32_t get_offset_of_isPlayerReady_9() { return static_cast<int32_t>(offsetof(PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8, ___isPlayerReady_9)); }
	inline bool get_isPlayerReady_9() const { return ___isPlayerReady_9; }
	inline bool* get_address_of_isPlayerReady_9() { return &___isPlayerReady_9; }
	inline void set_isPlayerReady_9(bool value)
	{
		___isPlayerReady_9 = value;
	}
};


// Photon.Pun.Demo.Asteroids.RoomListEntry
struct  RoomListEntry_t70DF026FFAFD48301B1496F29C7024F34C5D11FA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Asteroids.RoomListEntry::RoomNameText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___RoomNameText_4;
	// UnityEngine.UI.Text Photon.Pun.Demo.Asteroids.RoomListEntry::RoomPlayersText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___RoomPlayersText_5;
	// UnityEngine.UI.Button Photon.Pun.Demo.Asteroids.RoomListEntry::JoinRoomButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___JoinRoomButton_6;
	// System.String Photon.Pun.Demo.Asteroids.RoomListEntry::roomName
	String_t* ___roomName_7;

public:
	inline static int32_t get_offset_of_RoomNameText_4() { return static_cast<int32_t>(offsetof(RoomListEntry_t70DF026FFAFD48301B1496F29C7024F34C5D11FA, ___RoomNameText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_RoomNameText_4() const { return ___RoomNameText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_RoomNameText_4() { return &___RoomNameText_4; }
	inline void set_RoomNameText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___RoomNameText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RoomNameText_4), (void*)value);
	}

	inline static int32_t get_offset_of_RoomPlayersText_5() { return static_cast<int32_t>(offsetof(RoomListEntry_t70DF026FFAFD48301B1496F29C7024F34C5D11FA, ___RoomPlayersText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_RoomPlayersText_5() const { return ___RoomPlayersText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_RoomPlayersText_5() { return &___RoomPlayersText_5; }
	inline void set_RoomPlayersText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___RoomPlayersText_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RoomPlayersText_5), (void*)value);
	}

	inline static int32_t get_offset_of_JoinRoomButton_6() { return static_cast<int32_t>(offsetof(RoomListEntry_t70DF026FFAFD48301B1496F29C7024F34C5D11FA, ___JoinRoomButton_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_JoinRoomButton_6() const { return ___JoinRoomButton_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_JoinRoomButton_6() { return &___JoinRoomButton_6; }
	inline void set_JoinRoomButton_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___JoinRoomButton_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JoinRoomButton_6), (void*)value);
	}

	inline static int32_t get_offset_of_roomName_7() { return static_cast<int32_t>(offsetof(RoomListEntry_t70DF026FFAFD48301B1496F29C7024F34C5D11FA, ___roomName_7)); }
	inline String_t* get_roomName_7() const { return ___roomName_7; }
	inline String_t** get_address_of_roomName_7() { return &___roomName_7; }
	inline void set_roomName_7(String_t* value)
	{
		___roomName_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___roomName_7), (void*)value);
	}
};


// Photon.Pun.Demo.Asteroids.Spaceship
struct  Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Photon.Pun.Demo.Asteroids.Spaceship::RotationSpeed
	float ___RotationSpeed_4;
	// System.Single Photon.Pun.Demo.Asteroids.Spaceship::MovementSpeed
	float ___MovementSpeed_5;
	// System.Single Photon.Pun.Demo.Asteroids.Spaceship::MaxSpeed
	float ___MaxSpeed_6;
	// UnityEngine.ParticleSystem Photon.Pun.Demo.Asteroids.Spaceship::Destruction
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___Destruction_7;
	// UnityEngine.GameObject Photon.Pun.Demo.Asteroids.Spaceship::EngineTrail
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___EngineTrail_8;
	// UnityEngine.GameObject Photon.Pun.Demo.Asteroids.Spaceship::BulletPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___BulletPrefab_9;
	// Photon.Pun.PhotonView Photon.Pun.Demo.Asteroids.Spaceship::photonView
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___photonView_10;
	// UnityEngine.Rigidbody Photon.Pun.Demo.Asteroids.Spaceship::rigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___rigidbody_11;
	// UnityEngine.Collider Photon.Pun.Demo.Asteroids.Spaceship::collider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___collider_12;
	// UnityEngine.Renderer Photon.Pun.Demo.Asteroids.Spaceship::renderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___renderer_13;
	// System.Single Photon.Pun.Demo.Asteroids.Spaceship::rotation
	float ___rotation_14;
	// System.Single Photon.Pun.Demo.Asteroids.Spaceship::acceleration
	float ___acceleration_15;
	// System.Single Photon.Pun.Demo.Asteroids.Spaceship::shootingTimer
	float ___shootingTimer_16;
	// System.Boolean Photon.Pun.Demo.Asteroids.Spaceship::controllable
	bool ___controllable_17;

public:
	inline static int32_t get_offset_of_RotationSpeed_4() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___RotationSpeed_4)); }
	inline float get_RotationSpeed_4() const { return ___RotationSpeed_4; }
	inline float* get_address_of_RotationSpeed_4() { return &___RotationSpeed_4; }
	inline void set_RotationSpeed_4(float value)
	{
		___RotationSpeed_4 = value;
	}

	inline static int32_t get_offset_of_MovementSpeed_5() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___MovementSpeed_5)); }
	inline float get_MovementSpeed_5() const { return ___MovementSpeed_5; }
	inline float* get_address_of_MovementSpeed_5() { return &___MovementSpeed_5; }
	inline void set_MovementSpeed_5(float value)
	{
		___MovementSpeed_5 = value;
	}

	inline static int32_t get_offset_of_MaxSpeed_6() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___MaxSpeed_6)); }
	inline float get_MaxSpeed_6() const { return ___MaxSpeed_6; }
	inline float* get_address_of_MaxSpeed_6() { return &___MaxSpeed_6; }
	inline void set_MaxSpeed_6(float value)
	{
		___MaxSpeed_6 = value;
	}

	inline static int32_t get_offset_of_Destruction_7() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___Destruction_7)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_Destruction_7() const { return ___Destruction_7; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_Destruction_7() { return &___Destruction_7; }
	inline void set_Destruction_7(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___Destruction_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Destruction_7), (void*)value);
	}

	inline static int32_t get_offset_of_EngineTrail_8() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___EngineTrail_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_EngineTrail_8() const { return ___EngineTrail_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_EngineTrail_8() { return &___EngineTrail_8; }
	inline void set_EngineTrail_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___EngineTrail_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EngineTrail_8), (void*)value);
	}

	inline static int32_t get_offset_of_BulletPrefab_9() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___BulletPrefab_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_BulletPrefab_9() const { return ___BulletPrefab_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_BulletPrefab_9() { return &___BulletPrefab_9; }
	inline void set_BulletPrefab_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___BulletPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BulletPrefab_9), (void*)value);
	}

	inline static int32_t get_offset_of_photonView_10() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___photonView_10)); }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * get_photonView_10() const { return ___photonView_10; }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** get_address_of_photonView_10() { return &___photonView_10; }
	inline void set_photonView_10(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		___photonView_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___photonView_10), (void*)value);
	}

	inline static int32_t get_offset_of_rigidbody_11() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___rigidbody_11)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_rigidbody_11() const { return ___rigidbody_11; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_rigidbody_11() { return &___rigidbody_11; }
	inline void set_rigidbody_11(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___rigidbody_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rigidbody_11), (void*)value);
	}

	inline static int32_t get_offset_of_collider_12() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___collider_12)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_collider_12() const { return ___collider_12; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_collider_12() { return &___collider_12; }
	inline void set_collider_12(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___collider_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collider_12), (void*)value);
	}

	inline static int32_t get_offset_of_renderer_13() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___renderer_13)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_renderer_13() const { return ___renderer_13; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_renderer_13() { return &___renderer_13; }
	inline void set_renderer_13(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___renderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___renderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_rotation_14() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___rotation_14)); }
	inline float get_rotation_14() const { return ___rotation_14; }
	inline float* get_address_of_rotation_14() { return &___rotation_14; }
	inline void set_rotation_14(float value)
	{
		___rotation_14 = value;
	}

	inline static int32_t get_offset_of_acceleration_15() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___acceleration_15)); }
	inline float get_acceleration_15() const { return ___acceleration_15; }
	inline float* get_address_of_acceleration_15() { return &___acceleration_15; }
	inline void set_acceleration_15(float value)
	{
		___acceleration_15 = value;
	}

	inline static int32_t get_offset_of_shootingTimer_16() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___shootingTimer_16)); }
	inline float get_shootingTimer_16() const { return ___shootingTimer_16; }
	inline float* get_address_of_shootingTimer_16() { return &___shootingTimer_16; }
	inline void set_shootingTimer_16(float value)
	{
		___shootingTimer_16 = value;
	}

	inline static int32_t get_offset_of_controllable_17() { return static_cast<int32_t>(offsetof(Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B, ___controllable_17)); }
	inline bool get_controllable_17() const { return ___controllable_17; }
	inline bool* get_address_of_controllable_17() { return &___controllable_17; }
	inline void set_controllable_17(bool value)
	{
		___controllable_17 = value;
	}
};


// Photon.Pun.Demo.Cockpit.Forms.ConnectToRegionUIForm
struct  ConnectToRegionUIForm_t51B8AC0A75675EBE77175D754677010516216319  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.InputField Photon.Pun.Demo.Cockpit.Forms.ConnectToRegionUIForm::RegionInput
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___RegionInput_4;
	// UnityEngine.UI.Dropdown Photon.Pun.Demo.Cockpit.Forms.ConnectToRegionUIForm::RegionListInput
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___RegionListInput_5;
	// Photon.Pun.Demo.Cockpit.Forms.ConnectToRegionUIForm_OnSubmitEvent Photon.Pun.Demo.Cockpit.Forms.ConnectToRegionUIForm::OnSubmit
	OnSubmitEvent_t50178258F7E1A79F7B032E67B3C7662EC9B8BCD1 * ___OnSubmit_6;

public:
	inline static int32_t get_offset_of_RegionInput_4() { return static_cast<int32_t>(offsetof(ConnectToRegionUIForm_t51B8AC0A75675EBE77175D754677010516216319, ___RegionInput_4)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_RegionInput_4() const { return ___RegionInput_4; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_RegionInput_4() { return &___RegionInput_4; }
	inline void set_RegionInput_4(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___RegionInput_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RegionInput_4), (void*)value);
	}

	inline static int32_t get_offset_of_RegionListInput_5() { return static_cast<int32_t>(offsetof(ConnectToRegionUIForm_t51B8AC0A75675EBE77175D754677010516216319, ___RegionListInput_5)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_RegionListInput_5() const { return ___RegionListInput_5; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_RegionListInput_5() { return &___RegionListInput_5; }
	inline void set_RegionListInput_5(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___RegionListInput_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RegionListInput_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnSubmit_6() { return static_cast<int32_t>(offsetof(ConnectToRegionUIForm_t51B8AC0A75675EBE77175D754677010516216319, ___OnSubmit_6)); }
	inline OnSubmitEvent_t50178258F7E1A79F7B032E67B3C7662EC9B8BCD1 * get_OnSubmit_6() const { return ___OnSubmit_6; }
	inline OnSubmitEvent_t50178258F7E1A79F7B032E67B3C7662EC9B8BCD1 ** get_address_of_OnSubmit_6() { return &___OnSubmit_6; }
	inline void set_OnSubmit_6(OnSubmitEvent_t50178258F7E1A79F7B032E67B3C7662EC9B8BCD1 * value)
	{
		___OnSubmit_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSubmit_6), (void*)value);
	}
};


// Photon.Pun.Demo.Cockpit.Forms.CreateRoomUiForm
struct  CreateRoomUiForm_tB18CFC75CB3E9710756EE340DC4BA39AB5E1CFD5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.InputField Photon.Pun.Demo.Cockpit.Forms.CreateRoomUiForm::RoomNameInput
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___RoomNameInput_4;
	// UnityEngine.UI.InputField Photon.Pun.Demo.Cockpit.Forms.CreateRoomUiForm::LobbyNameInput
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___LobbyNameInput_5;
	// UnityEngine.UI.InputField Photon.Pun.Demo.Cockpit.Forms.CreateRoomUiForm::ExpectedUsersInput
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___ExpectedUsersInput_6;
	// UnityEngine.UI.Dropdown Photon.Pun.Demo.Cockpit.Forms.CreateRoomUiForm::LobbyTypeInput
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___LobbyTypeInput_7;
	// Photon.Pun.Demo.Cockpit.Forms.CreateRoomUiForm_OnSubmitEvent Photon.Pun.Demo.Cockpit.Forms.CreateRoomUiForm::OnSubmit
	OnSubmitEvent_tEAF5D32F65BE88AAD89EDE90BC06E2E0AB6B59B4 * ___OnSubmit_8;

public:
	inline static int32_t get_offset_of_RoomNameInput_4() { return static_cast<int32_t>(offsetof(CreateRoomUiForm_tB18CFC75CB3E9710756EE340DC4BA39AB5E1CFD5, ___RoomNameInput_4)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_RoomNameInput_4() const { return ___RoomNameInput_4; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_RoomNameInput_4() { return &___RoomNameInput_4; }
	inline void set_RoomNameInput_4(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___RoomNameInput_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RoomNameInput_4), (void*)value);
	}

	inline static int32_t get_offset_of_LobbyNameInput_5() { return static_cast<int32_t>(offsetof(CreateRoomUiForm_tB18CFC75CB3E9710756EE340DC4BA39AB5E1CFD5, ___LobbyNameInput_5)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_LobbyNameInput_5() const { return ___LobbyNameInput_5; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_LobbyNameInput_5() { return &___LobbyNameInput_5; }
	inline void set_LobbyNameInput_5(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___LobbyNameInput_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LobbyNameInput_5), (void*)value);
	}

	inline static int32_t get_offset_of_ExpectedUsersInput_6() { return static_cast<int32_t>(offsetof(CreateRoomUiForm_tB18CFC75CB3E9710756EE340DC4BA39AB5E1CFD5, ___ExpectedUsersInput_6)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_ExpectedUsersInput_6() const { return ___ExpectedUsersInput_6; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_ExpectedUsersInput_6() { return &___ExpectedUsersInput_6; }
	inline void set_ExpectedUsersInput_6(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___ExpectedUsersInput_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ExpectedUsersInput_6), (void*)value);
	}

	inline static int32_t get_offset_of_LobbyTypeInput_7() { return static_cast<int32_t>(offsetof(CreateRoomUiForm_tB18CFC75CB3E9710756EE340DC4BA39AB5E1CFD5, ___LobbyTypeInput_7)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_LobbyTypeInput_7() const { return ___LobbyTypeInput_7; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_LobbyTypeInput_7() { return &___LobbyTypeInput_7; }
	inline void set_LobbyTypeInput_7(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___LobbyTypeInput_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LobbyTypeInput_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnSubmit_8() { return static_cast<int32_t>(offsetof(CreateRoomUiForm_tB18CFC75CB3E9710756EE340DC4BA39AB5E1CFD5, ___OnSubmit_8)); }
	inline OnSubmitEvent_tEAF5D32F65BE88AAD89EDE90BC06E2E0AB6B59B4 * get_OnSubmit_8() const { return ___OnSubmit_8; }
	inline OnSubmitEvent_tEAF5D32F65BE88AAD89EDE90BC06E2E0AB6B59B4 ** get_address_of_OnSubmit_8() { return &___OnSubmit_8; }
	inline void set_OnSubmit_8(OnSubmitEvent_tEAF5D32F65BE88AAD89EDE90BC06E2E0AB6B59B4 * value)
	{
		___OnSubmit_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSubmit_8), (void*)value);
	}
};


// Photon.Pun.Demo.Cockpit.Forms.LoadLevelUIForm
struct  LoadLevelUIForm_t3C504A2C791E924CCE39CC93F1A351BFAB14C166  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.InputField Photon.Pun.Demo.Cockpit.Forms.LoadLevelUIForm::PropertyValueInput
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___PropertyValueInput_4;
	// Photon.Pun.Demo.Cockpit.Forms.LoadLevelUIForm_OnSubmitEvent Photon.Pun.Demo.Cockpit.Forms.LoadLevelUIForm::OnSubmit
	OnSubmitEvent_t48121DCBCBFA2A2D4D2051513ACC05438A437ED7 * ___OnSubmit_5;

public:
	inline static int32_t get_offset_of_PropertyValueInput_4() { return static_cast<int32_t>(offsetof(LoadLevelUIForm_t3C504A2C791E924CCE39CC93F1A351BFAB14C166, ___PropertyValueInput_4)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_PropertyValueInput_4() const { return ___PropertyValueInput_4; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_PropertyValueInput_4() { return &___PropertyValueInput_4; }
	inline void set_PropertyValueInput_4(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___PropertyValueInput_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PropertyValueInput_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnSubmit_5() { return static_cast<int32_t>(offsetof(LoadLevelUIForm_t3C504A2C791E924CCE39CC93F1A351BFAB14C166, ___OnSubmit_5)); }
	inline OnSubmitEvent_t48121DCBCBFA2A2D4D2051513ACC05438A437ED7 * get_OnSubmit_5() const { return ___OnSubmit_5; }
	inline OnSubmitEvent_t48121DCBCBFA2A2D4D2051513ACC05438A437ED7 ** get_address_of_OnSubmit_5() { return &___OnSubmit_5; }
	inline void set_OnSubmit_5(OnSubmitEvent_t48121DCBCBFA2A2D4D2051513ACC05438A437ED7 * value)
	{
		___OnSubmit_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSubmit_5), (void*)value);
	}
};


// Photon.Pun.Demo.Cockpit.Forms.SetRoomCustomPropertyUIForm
struct  SetRoomCustomPropertyUIForm_tBD7E4023932AA3B4F89E5D8FF367044298CFEE0F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.InputField Photon.Pun.Demo.Cockpit.Forms.SetRoomCustomPropertyUIForm::PropertyValueInput
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___PropertyValueInput_4;
	// Photon.Pun.Demo.Cockpit.Forms.SetRoomCustomPropertyUIForm_OnSubmitEvent Photon.Pun.Demo.Cockpit.Forms.SetRoomCustomPropertyUIForm::OnSubmit
	OnSubmitEvent_tCCDDD3E5DB3876A2D7BF8D930959AB47D6B785BC * ___OnSubmit_5;

public:
	inline static int32_t get_offset_of_PropertyValueInput_4() { return static_cast<int32_t>(offsetof(SetRoomCustomPropertyUIForm_tBD7E4023932AA3B4F89E5D8FF367044298CFEE0F, ___PropertyValueInput_4)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_PropertyValueInput_4() const { return ___PropertyValueInput_4; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_PropertyValueInput_4() { return &___PropertyValueInput_4; }
	inline void set_PropertyValueInput_4(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___PropertyValueInput_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PropertyValueInput_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnSubmit_5() { return static_cast<int32_t>(offsetof(SetRoomCustomPropertyUIForm_tBD7E4023932AA3B4F89E5D8FF367044298CFEE0F, ___OnSubmit_5)); }
	inline OnSubmitEvent_tCCDDD3E5DB3876A2D7BF8D930959AB47D6B785BC * get_OnSubmit_5() const { return ___OnSubmit_5; }
	inline OnSubmitEvent_tCCDDD3E5DB3876A2D7BF8D930959AB47D6B785BC ** get_address_of_OnSubmit_5() { return &___OnSubmit_5; }
	inline void set_OnSubmit_5(OnSubmitEvent_tCCDDD3E5DB3876A2D7BF8D930959AB47D6B785BC * value)
	{
		___OnSubmit_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSubmit_5), (void*)value);
	}
};


// Photon.Pun.Demo.Cockpit.Forms.UserIdUiForm
struct  UserIdUiForm_tFCE24B50C3A6BCBB56E6C78268FB541B811771E7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.InputField Photon.Pun.Demo.Cockpit.Forms.UserIdUiForm::idInput
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___idInput_5;
	// Photon.Pun.Demo.Cockpit.Forms.UserIdUiForm_OnSubmitEvent Photon.Pun.Demo.Cockpit.Forms.UserIdUiForm::OnSubmit
	OnSubmitEvent_t49EB5BBF0581C3E26C318B266AB48B6E05E68AC3 * ___OnSubmit_6;

public:
	inline static int32_t get_offset_of_idInput_5() { return static_cast<int32_t>(offsetof(UserIdUiForm_tFCE24B50C3A6BCBB56E6C78268FB541B811771E7, ___idInput_5)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_idInput_5() const { return ___idInput_5; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_idInput_5() { return &___idInput_5; }
	inline void set_idInput_5(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___idInput_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___idInput_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnSubmit_6() { return static_cast<int32_t>(offsetof(UserIdUiForm_tFCE24B50C3A6BCBB56E6C78268FB541B811771E7, ___OnSubmit_6)); }
	inline OnSubmitEvent_t49EB5BBF0581C3E26C318B266AB48B6E05E68AC3 * get_OnSubmit_6() const { return ___OnSubmit_6; }
	inline OnSubmitEvent_t49EB5BBF0581C3E26C318B266AB48B6E05E68AC3 ** get_address_of_OnSubmit_6() { return &___OnSubmit_6; }
	inline void set_OnSubmit_6(OnSubmitEvent_t49EB5BBF0581C3E26C318B266AB48B6E05E68AC3 * value)
	{
		___OnSubmit_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSubmit_6), (void*)value);
	}
};


// Photon.Pun.Demo.Cockpit.PropertyListenerBase
struct  PropertyListenerBase_t05B9504A6B7DFE170062320525A7A5F60D48E3CC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Graphic Photon.Pun.Demo.Cockpit.PropertyListenerBase::UpdateIndicator
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___UpdateIndicator_4;
	// UnityEngine.YieldInstruction Photon.Pun.Demo.Cockpit.PropertyListenerBase::fadeInstruction
	YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44 * ___fadeInstruction_5;
	// System.Single Photon.Pun.Demo.Cockpit.PropertyListenerBase::Duration
	float ___Duration_6;

public:
	inline static int32_t get_offset_of_UpdateIndicator_4() { return static_cast<int32_t>(offsetof(PropertyListenerBase_t05B9504A6B7DFE170062320525A7A5F60D48E3CC, ___UpdateIndicator_4)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_UpdateIndicator_4() const { return ___UpdateIndicator_4; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_UpdateIndicator_4() { return &___UpdateIndicator_4; }
	inline void set_UpdateIndicator_4(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___UpdateIndicator_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UpdateIndicator_4), (void*)value);
	}

	inline static int32_t get_offset_of_fadeInstruction_5() { return static_cast<int32_t>(offsetof(PropertyListenerBase_t05B9504A6B7DFE170062320525A7A5F60D48E3CC, ___fadeInstruction_5)); }
	inline YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44 * get_fadeInstruction_5() const { return ___fadeInstruction_5; }
	inline YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44 ** get_address_of_fadeInstruction_5() { return &___fadeInstruction_5; }
	inline void set_fadeInstruction_5(YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44 * value)
	{
		___fadeInstruction_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fadeInstruction_5), (void*)value);
	}

	inline static int32_t get_offset_of_Duration_6() { return static_cast<int32_t>(offsetof(PropertyListenerBase_t05B9504A6B7DFE170062320525A7A5F60D48E3CC, ___Duration_6)); }
	inline float get_Duration_6() const { return ___Duration_6; }
	inline float* get_address_of_Duration_6() { return &___Duration_6; }
	inline void set_Duration_6(float value)
	{
		___Duration_6 = value;
	}
};


// Photon.Pun.Demo.Cockpit.ServerAddressProperty
struct  ServerAddressProperty_tE9830998791F788D9B8DC7FD4508EB9782143270  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.ServerAddressProperty::Text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___Text_4;
	// System.String Photon.Pun.Demo.Cockpit.ServerAddressProperty::_cache
	String_t* ____cache_5;

public:
	inline static int32_t get_offset_of_Text_4() { return static_cast<int32_t>(offsetof(ServerAddressProperty_tE9830998791F788D9B8DC7FD4508EB9782143270, ___Text_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_Text_4() const { return ___Text_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_Text_4() { return &___Text_4; }
	inline void set_Text_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___Text_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text_4), (void*)value);
	}

	inline static int32_t get_offset_of__cache_5() { return static_cast<int32_t>(offsetof(ServerAddressProperty_tE9830998791F788D9B8DC7FD4508EB9782143270, ____cache_5)); }
	inline String_t* get__cache_5() const { return ____cache_5; }
	inline String_t** get_address_of__cache_5() { return &____cache_5; }
	inline void set__cache_5(String_t* value)
	{
		____cache_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cache_5), (void*)value);
	}
};


// Photon.Pun.Demo.Hub.DemoHubManager
struct  DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Hub.DemoHubManager::TitleText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___TitleText_4;
	// UnityEngine.UI.Text Photon.Pun.Demo.Hub.DemoHubManager::DescriptionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___DescriptionText_5;
	// UnityEngine.GameObject Photon.Pun.Demo.Hub.DemoHubManager::OpenSceneButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___OpenSceneButton_6;
	// UnityEngine.GameObject Photon.Pun.Demo.Hub.DemoHubManager::OpenTutorialLinkButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___OpenTutorialLinkButton_7;
	// UnityEngine.GameObject Photon.Pun.Demo.Hub.DemoHubManager::OpenDocLinkButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___OpenDocLinkButton_8;
	// System.String Photon.Pun.Demo.Hub.DemoHubManager::MainDemoWebLink
	String_t* ___MainDemoWebLink_9;
	// System.Collections.Generic.Dictionary`2<System.String,Photon.Pun.Demo.Hub.DemoHubManager_DemoData> Photon.Pun.Demo.Hub.DemoHubManager::_data
	Dictionary_2_t63C3F023241A817BD21FF4EB21DF3EB77C69266E * ____data_10;
	// System.String Photon.Pun.Demo.Hub.DemoHubManager::currentSelection
	String_t* ___currentSelection_11;

public:
	inline static int32_t get_offset_of_TitleText_4() { return static_cast<int32_t>(offsetof(DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906, ___TitleText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_TitleText_4() const { return ___TitleText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_TitleText_4() { return &___TitleText_4; }
	inline void set_TitleText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___TitleText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TitleText_4), (void*)value);
	}

	inline static int32_t get_offset_of_DescriptionText_5() { return static_cast<int32_t>(offsetof(DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906, ___DescriptionText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_DescriptionText_5() const { return ___DescriptionText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_DescriptionText_5() { return &___DescriptionText_5; }
	inline void set_DescriptionText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___DescriptionText_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DescriptionText_5), (void*)value);
	}

	inline static int32_t get_offset_of_OpenSceneButton_6() { return static_cast<int32_t>(offsetof(DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906, ___OpenSceneButton_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_OpenSceneButton_6() const { return ___OpenSceneButton_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_OpenSceneButton_6() { return &___OpenSceneButton_6; }
	inline void set_OpenSceneButton_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___OpenSceneButton_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OpenSceneButton_6), (void*)value);
	}

	inline static int32_t get_offset_of_OpenTutorialLinkButton_7() { return static_cast<int32_t>(offsetof(DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906, ___OpenTutorialLinkButton_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_OpenTutorialLinkButton_7() const { return ___OpenTutorialLinkButton_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_OpenTutorialLinkButton_7() { return &___OpenTutorialLinkButton_7; }
	inline void set_OpenTutorialLinkButton_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___OpenTutorialLinkButton_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OpenTutorialLinkButton_7), (void*)value);
	}

	inline static int32_t get_offset_of_OpenDocLinkButton_8() { return static_cast<int32_t>(offsetof(DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906, ___OpenDocLinkButton_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_OpenDocLinkButton_8() const { return ___OpenDocLinkButton_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_OpenDocLinkButton_8() { return &___OpenDocLinkButton_8; }
	inline void set_OpenDocLinkButton_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___OpenDocLinkButton_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OpenDocLinkButton_8), (void*)value);
	}

	inline static int32_t get_offset_of_MainDemoWebLink_9() { return static_cast<int32_t>(offsetof(DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906, ___MainDemoWebLink_9)); }
	inline String_t* get_MainDemoWebLink_9() const { return ___MainDemoWebLink_9; }
	inline String_t** get_address_of_MainDemoWebLink_9() { return &___MainDemoWebLink_9; }
	inline void set_MainDemoWebLink_9(String_t* value)
	{
		___MainDemoWebLink_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MainDemoWebLink_9), (void*)value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906, ____data_10)); }
	inline Dictionary_2_t63C3F023241A817BD21FF4EB21DF3EB77C69266E * get__data_10() const { return ____data_10; }
	inline Dictionary_2_t63C3F023241A817BD21FF4EB21DF3EB77C69266E ** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(Dictionary_2_t63C3F023241A817BD21FF4EB21DF3EB77C69266E * value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_10), (void*)value);
	}

	inline static int32_t get_offset_of_currentSelection_11() { return static_cast<int32_t>(offsetof(DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906, ___currentSelection_11)); }
	inline String_t* get_currentSelection_11() const { return ___currentSelection_11; }
	inline String_t** get_address_of_currentSelection_11() { return &___currentSelection_11; }
	inline void set_currentSelection_11(String_t* value)
	{
		___currentSelection_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentSelection_11), (void*)value);
	}
};


// Photon.Pun.Demo.Hub.ToDemoHubButton
struct  ToDemoHubButton_t5E0B3194BFEBB5D1C511FEB5653DFEBA161E2ABE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.CanvasGroup Photon.Pun.Demo.Hub.ToDemoHubButton::_canvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ____canvasGroup_5;

public:
	inline static int32_t get_offset_of__canvasGroup_5() { return static_cast<int32_t>(offsetof(ToDemoHubButton_t5E0B3194BFEBB5D1C511FEB5653DFEBA161E2ABE, ____canvasGroup_5)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get__canvasGroup_5() const { return ____canvasGroup_5; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of__canvasGroup_5() { return &____canvasGroup_5; }
	inline void set__canvasGroup_5(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		____canvasGroup_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____canvasGroup_5), (void*)value);
	}
};

struct ToDemoHubButton_t5E0B3194BFEBB5D1C511FEB5653DFEBA161E2ABE_StaticFields
{
public:
	// Photon.Pun.Demo.Hub.ToDemoHubButton Photon.Pun.Demo.Hub.ToDemoHubButton::instance
	ToDemoHubButton_t5E0B3194BFEBB5D1C511FEB5653DFEBA161E2ABE * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(ToDemoHubButton_t5E0B3194BFEBB5D1C511FEB5653DFEBA161E2ABE_StaticFields, ___instance_4)); }
	inline ToDemoHubButton_t5E0B3194BFEBB5D1C511FEB5653DFEBA161E2ABE * get_instance_4() const { return ___instance_4; }
	inline ToDemoHubButton_t5E0B3194BFEBB5D1C511FEB5653DFEBA161E2ABE ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(ToDemoHubButton_t5E0B3194BFEBB5D1C511FEB5653DFEBA161E2ABE * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// Photon.Pun.Demo.Procedural.Block
struct  Block_tCB9DFFA320A03A833F82D902FDF4F905FC1BE616  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Photon.Pun.Demo.Procedural.Block::<BlockId>k__BackingField
	int32_t ___U3CBlockIdU3Ek__BackingField_4;
	// System.Int32 Photon.Pun.Demo.Procedural.Block::<ClusterId>k__BackingField
	int32_t ___U3CClusterIdU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CBlockIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Block_tCB9DFFA320A03A833F82D902FDF4F905FC1BE616, ___U3CBlockIdU3Ek__BackingField_4)); }
	inline int32_t get_U3CBlockIdU3Ek__BackingField_4() const { return ___U3CBlockIdU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CBlockIdU3Ek__BackingField_4() { return &___U3CBlockIdU3Ek__BackingField_4; }
	inline void set_U3CBlockIdU3Ek__BackingField_4(int32_t value)
	{
		___U3CBlockIdU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CClusterIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Block_tCB9DFFA320A03A833F82D902FDF4F905FC1BE616, ___U3CClusterIdU3Ek__BackingField_5)); }
	inline int32_t get_U3CClusterIdU3Ek__BackingField_5() const { return ___U3CClusterIdU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CClusterIdU3Ek__BackingField_5() { return &___U3CClusterIdU3Ek__BackingField_5; }
	inline void set_U3CClusterIdU3Ek__BackingField_5(int32_t value)
	{
		___U3CClusterIdU3Ek__BackingField_5 = value;
	}
};


// Photon.Pun.Demo.Procedural.ProceduralController
struct  ProceduralController_t53B3332F30DCA1222A797F949190B6E81D76425C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera Photon.Pun.Demo.Procedural.ProceduralController::cam
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cam_4;

public:
	inline static int32_t get_offset_of_cam_4() { return static_cast<int32_t>(offsetof(ProceduralController_t53B3332F30DCA1222A797F949190B6E81D76425C, ___cam_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cam_4() const { return ___cam_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cam_4() { return &___cam_4; }
	inline void set_cam_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cam_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cam_4), (void*)value);
	}
};


// Photon.Pun.Demo.Procedural.WorldGenerator
struct  WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Photon.Pun.Demo.Procedural.WorldGenerator::SeedPropertiesKey
	String_t* ___SeedPropertiesKey_4;
	// System.String Photon.Pun.Demo.Procedural.WorldGenerator::WorldSizePropertiesKey
	String_t* ___WorldSizePropertiesKey_5;
	// System.String Photon.Pun.Demo.Procedural.WorldGenerator::ClusterSizePropertiesKey
	String_t* ___ClusterSizePropertiesKey_6;
	// System.String Photon.Pun.Demo.Procedural.WorldGenerator::WorldTypePropertiesKey
	String_t* ___WorldTypePropertiesKey_7;
	// System.Int32 Photon.Pun.Demo.Procedural.WorldGenerator::<Seed>k__BackingField
	int32_t ___U3CSeedU3Ek__BackingField_9;
	// Photon.Pun.Demo.Procedural.WorldSize Photon.Pun.Demo.Procedural.WorldGenerator::<WorldSize>k__BackingField
	int32_t ___U3CWorldSizeU3Ek__BackingField_10;
	// Photon.Pun.Demo.Procedural.ClusterSize Photon.Pun.Demo.Procedural.WorldGenerator::<ClusterSize>k__BackingField
	int32_t ___U3CClusterSizeU3Ek__BackingField_11;
	// Photon.Pun.Demo.Procedural.WorldType Photon.Pun.Demo.Procedural.WorldGenerator::<WorldType>k__BackingField
	int32_t ___U3CWorldTypeU3Ek__BackingField_12;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> Photon.Pun.Demo.Procedural.WorldGenerator::clusterList
	Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * ___clusterList_13;
	// UnityEngine.Material[] Photon.Pun.Demo.Procedural.WorldGenerator::WorldMaterials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___WorldMaterials_14;

public:
	inline static int32_t get_offset_of_SeedPropertiesKey_4() { return static_cast<int32_t>(offsetof(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5, ___SeedPropertiesKey_4)); }
	inline String_t* get_SeedPropertiesKey_4() const { return ___SeedPropertiesKey_4; }
	inline String_t** get_address_of_SeedPropertiesKey_4() { return &___SeedPropertiesKey_4; }
	inline void set_SeedPropertiesKey_4(String_t* value)
	{
		___SeedPropertiesKey_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SeedPropertiesKey_4), (void*)value);
	}

	inline static int32_t get_offset_of_WorldSizePropertiesKey_5() { return static_cast<int32_t>(offsetof(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5, ___WorldSizePropertiesKey_5)); }
	inline String_t* get_WorldSizePropertiesKey_5() const { return ___WorldSizePropertiesKey_5; }
	inline String_t** get_address_of_WorldSizePropertiesKey_5() { return &___WorldSizePropertiesKey_5; }
	inline void set_WorldSizePropertiesKey_5(String_t* value)
	{
		___WorldSizePropertiesKey_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WorldSizePropertiesKey_5), (void*)value);
	}

	inline static int32_t get_offset_of_ClusterSizePropertiesKey_6() { return static_cast<int32_t>(offsetof(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5, ___ClusterSizePropertiesKey_6)); }
	inline String_t* get_ClusterSizePropertiesKey_6() const { return ___ClusterSizePropertiesKey_6; }
	inline String_t** get_address_of_ClusterSizePropertiesKey_6() { return &___ClusterSizePropertiesKey_6; }
	inline void set_ClusterSizePropertiesKey_6(String_t* value)
	{
		___ClusterSizePropertiesKey_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ClusterSizePropertiesKey_6), (void*)value);
	}

	inline static int32_t get_offset_of_WorldTypePropertiesKey_7() { return static_cast<int32_t>(offsetof(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5, ___WorldTypePropertiesKey_7)); }
	inline String_t* get_WorldTypePropertiesKey_7() const { return ___WorldTypePropertiesKey_7; }
	inline String_t** get_address_of_WorldTypePropertiesKey_7() { return &___WorldTypePropertiesKey_7; }
	inline void set_WorldTypePropertiesKey_7(String_t* value)
	{
		___WorldTypePropertiesKey_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WorldTypePropertiesKey_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSeedU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5, ___U3CSeedU3Ek__BackingField_9)); }
	inline int32_t get_U3CSeedU3Ek__BackingField_9() const { return ___U3CSeedU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CSeedU3Ek__BackingField_9() { return &___U3CSeedU3Ek__BackingField_9; }
	inline void set_U3CSeedU3Ek__BackingField_9(int32_t value)
	{
		___U3CSeedU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CWorldSizeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5, ___U3CWorldSizeU3Ek__BackingField_10)); }
	inline int32_t get_U3CWorldSizeU3Ek__BackingField_10() const { return ___U3CWorldSizeU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CWorldSizeU3Ek__BackingField_10() { return &___U3CWorldSizeU3Ek__BackingField_10; }
	inline void set_U3CWorldSizeU3Ek__BackingField_10(int32_t value)
	{
		___U3CWorldSizeU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CClusterSizeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5, ___U3CClusterSizeU3Ek__BackingField_11)); }
	inline int32_t get_U3CClusterSizeU3Ek__BackingField_11() const { return ___U3CClusterSizeU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CClusterSizeU3Ek__BackingField_11() { return &___U3CClusterSizeU3Ek__BackingField_11; }
	inline void set_U3CClusterSizeU3Ek__BackingField_11(int32_t value)
	{
		___U3CClusterSizeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CWorldTypeU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5, ___U3CWorldTypeU3Ek__BackingField_12)); }
	inline int32_t get_U3CWorldTypeU3Ek__BackingField_12() const { return ___U3CWorldTypeU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CWorldTypeU3Ek__BackingField_12() { return &___U3CWorldTypeU3Ek__BackingField_12; }
	inline void set_U3CWorldTypeU3Ek__BackingField_12(int32_t value)
	{
		___U3CWorldTypeU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_clusterList_13() { return static_cast<int32_t>(offsetof(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5, ___clusterList_13)); }
	inline Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * get_clusterList_13() const { return ___clusterList_13; }
	inline Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 ** get_address_of_clusterList_13() { return &___clusterList_13; }
	inline void set_clusterList_13(Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * value)
	{
		___clusterList_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clusterList_13), (void*)value);
	}

	inline static int32_t get_offset_of_WorldMaterials_14() { return static_cast<int32_t>(offsetof(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5, ___WorldMaterials_14)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_WorldMaterials_14() const { return ___WorldMaterials_14; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_WorldMaterials_14() { return &___WorldMaterials_14; }
	inline void set_WorldMaterials_14(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___WorldMaterials_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WorldMaterials_14), (void*)value);
	}
};

struct WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5_StaticFields
{
public:
	// Photon.Pun.Demo.Procedural.WorldGenerator Photon.Pun.Demo.Procedural.WorldGenerator::instance
	WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5 * ___instance_8;

public:
	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5_StaticFields, ___instance_8)); }
	inline WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5 * get_instance_8() const { return ___instance_8; }
	inline WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5 ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5 * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_8), (void*)value);
	}
};


// Photon.Pun.Demo.PunBasics.CameraWork
struct  CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Photon.Pun.Demo.PunBasics.CameraWork::distance
	float ___distance_4;
	// System.Single Photon.Pun.Demo.PunBasics.CameraWork::height
	float ___height_5;
	// System.Single Photon.Pun.Demo.PunBasics.CameraWork::heightSmoothLag
	float ___heightSmoothLag_6;
	// UnityEngine.Vector3 Photon.Pun.Demo.PunBasics.CameraWork::centerOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___centerOffset_7;
	// System.Boolean Photon.Pun.Demo.PunBasics.CameraWork::followOnStart
	bool ___followOnStart_8;
	// UnityEngine.Transform Photon.Pun.Demo.PunBasics.CameraWork::cameraTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___cameraTransform_9;
	// System.Boolean Photon.Pun.Demo.PunBasics.CameraWork::isFollowing
	bool ___isFollowing_10;
	// System.Single Photon.Pun.Demo.PunBasics.CameraWork::heightVelocity
	float ___heightVelocity_11;
	// System.Single Photon.Pun.Demo.PunBasics.CameraWork::targetHeight
	float ___targetHeight_12;

public:
	inline static int32_t get_offset_of_distance_4() { return static_cast<int32_t>(offsetof(CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F, ___distance_4)); }
	inline float get_distance_4() const { return ___distance_4; }
	inline float* get_address_of_distance_4() { return &___distance_4; }
	inline void set_distance_4(float value)
	{
		___distance_4 = value;
	}

	inline static int32_t get_offset_of_height_5() { return static_cast<int32_t>(offsetof(CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F, ___height_5)); }
	inline float get_height_5() const { return ___height_5; }
	inline float* get_address_of_height_5() { return &___height_5; }
	inline void set_height_5(float value)
	{
		___height_5 = value;
	}

	inline static int32_t get_offset_of_heightSmoothLag_6() { return static_cast<int32_t>(offsetof(CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F, ___heightSmoothLag_6)); }
	inline float get_heightSmoothLag_6() const { return ___heightSmoothLag_6; }
	inline float* get_address_of_heightSmoothLag_6() { return &___heightSmoothLag_6; }
	inline void set_heightSmoothLag_6(float value)
	{
		___heightSmoothLag_6 = value;
	}

	inline static int32_t get_offset_of_centerOffset_7() { return static_cast<int32_t>(offsetof(CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F, ___centerOffset_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_centerOffset_7() const { return ___centerOffset_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_centerOffset_7() { return &___centerOffset_7; }
	inline void set_centerOffset_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___centerOffset_7 = value;
	}

	inline static int32_t get_offset_of_followOnStart_8() { return static_cast<int32_t>(offsetof(CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F, ___followOnStart_8)); }
	inline bool get_followOnStart_8() const { return ___followOnStart_8; }
	inline bool* get_address_of_followOnStart_8() { return &___followOnStart_8; }
	inline void set_followOnStart_8(bool value)
	{
		___followOnStart_8 = value;
	}

	inline static int32_t get_offset_of_cameraTransform_9() { return static_cast<int32_t>(offsetof(CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F, ___cameraTransform_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_cameraTransform_9() const { return ___cameraTransform_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_cameraTransform_9() { return &___cameraTransform_9; }
	inline void set_cameraTransform_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___cameraTransform_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameraTransform_9), (void*)value);
	}

	inline static int32_t get_offset_of_isFollowing_10() { return static_cast<int32_t>(offsetof(CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F, ___isFollowing_10)); }
	inline bool get_isFollowing_10() const { return ___isFollowing_10; }
	inline bool* get_address_of_isFollowing_10() { return &___isFollowing_10; }
	inline void set_isFollowing_10(bool value)
	{
		___isFollowing_10 = value;
	}

	inline static int32_t get_offset_of_heightVelocity_11() { return static_cast<int32_t>(offsetof(CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F, ___heightVelocity_11)); }
	inline float get_heightVelocity_11() const { return ___heightVelocity_11; }
	inline float* get_address_of_heightVelocity_11() { return &___heightVelocity_11; }
	inline void set_heightVelocity_11(float value)
	{
		___heightVelocity_11 = value;
	}

	inline static int32_t get_offset_of_targetHeight_12() { return static_cast<int32_t>(offsetof(CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F, ___targetHeight_12)); }
	inline float get_targetHeight_12() const { return ___targetHeight_12; }
	inline float* get_address_of_targetHeight_12() { return &___targetHeight_12; }
	inline void set_targetHeight_12(float value)
	{
		___targetHeight_12 = value;
	}
};


// Photon.Pun.Demo.PunBasics.LoaderAnime
struct  LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Photon.Pun.Demo.PunBasics.LoaderAnime::speed
	float ___speed_4;
	// System.Single Photon.Pun.Demo.PunBasics.LoaderAnime::radius
	float ___radius_5;
	// UnityEngine.GameObject Photon.Pun.Demo.PunBasics.LoaderAnime::particles
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___particles_6;
	// UnityEngine.Vector3 Photon.Pun.Demo.PunBasics.LoaderAnime::_offset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ____offset_7;
	// UnityEngine.Transform Photon.Pun.Demo.PunBasics.LoaderAnime::_transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____transform_8;
	// UnityEngine.Transform Photon.Pun.Demo.PunBasics.LoaderAnime::_particleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____particleTransform_9;
	// System.Boolean Photon.Pun.Demo.PunBasics.LoaderAnime::_isAnimating
	bool ____isAnimating_10;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_radius_5() { return static_cast<int32_t>(offsetof(LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484, ___radius_5)); }
	inline float get_radius_5() const { return ___radius_5; }
	inline float* get_address_of_radius_5() { return &___radius_5; }
	inline void set_radius_5(float value)
	{
		___radius_5 = value;
	}

	inline static int32_t get_offset_of_particles_6() { return static_cast<int32_t>(offsetof(LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484, ___particles_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_particles_6() const { return ___particles_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_particles_6() { return &___particles_6; }
	inline void set_particles_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___particles_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particles_6), (void*)value);
	}

	inline static int32_t get_offset_of__offset_7() { return static_cast<int32_t>(offsetof(LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484, ____offset_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get__offset_7() const { return ____offset_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of__offset_7() { return &____offset_7; }
	inline void set__offset_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		____offset_7 = value;
	}

	inline static int32_t get_offset_of__transform_8() { return static_cast<int32_t>(offsetof(LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484, ____transform_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__transform_8() const { return ____transform_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__transform_8() { return &____transform_8; }
	inline void set__transform_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____transform_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____transform_8), (void*)value);
	}

	inline static int32_t get_offset_of__particleTransform_9() { return static_cast<int32_t>(offsetof(LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484, ____particleTransform_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__particleTransform_9() const { return ____particleTransform_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__particleTransform_9() { return &____particleTransform_9; }
	inline void set__particleTransform_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____particleTransform_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____particleTransform_9), (void*)value);
	}

	inline static int32_t get_offset_of__isAnimating_10() { return static_cast<int32_t>(offsetof(LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484, ____isAnimating_10)); }
	inline bool get__isAnimating_10() const { return ____isAnimating_10; }
	inline bool* get_address_of__isAnimating_10() { return &____isAnimating_10; }
	inline void set__isAnimating_10(bool value)
	{
		____isAnimating_10 = value;
	}
};


// Photon.Pun.Demo.PunBasics.PlayerNameInputField
struct  PlayerNameInputField_t1E8DFFB41F20135465C6CC7A6379387865955010  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// Photon.Pun.Demo.PunBasics.PlayerUI
struct  PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3 Photon.Pun.Demo.PunBasics.PlayerUI::screenOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___screenOffset_4;
	// UnityEngine.UI.Text Photon.Pun.Demo.PunBasics.PlayerUI::playerNameText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___playerNameText_5;
	// UnityEngine.UI.Slider Photon.Pun.Demo.PunBasics.PlayerUI::playerHealthSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___playerHealthSlider_6;
	// Photon.Pun.Demo.PunBasics.PlayerManager Photon.Pun.Demo.PunBasics.PlayerUI::target
	PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144 * ___target_7;
	// System.Single Photon.Pun.Demo.PunBasics.PlayerUI::characterControllerHeight
	float ___characterControllerHeight_8;
	// UnityEngine.Transform Photon.Pun.Demo.PunBasics.PlayerUI::targetTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___targetTransform_9;
	// UnityEngine.Renderer Photon.Pun.Demo.PunBasics.PlayerUI::targetRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___targetRenderer_10;
	// UnityEngine.CanvasGroup Photon.Pun.Demo.PunBasics.PlayerUI::_canvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ____canvasGroup_11;
	// UnityEngine.Vector3 Photon.Pun.Demo.PunBasics.PlayerUI::targetPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPosition_12;

public:
	inline static int32_t get_offset_of_screenOffset_4() { return static_cast<int32_t>(offsetof(PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735, ___screenOffset_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_screenOffset_4() const { return ___screenOffset_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_screenOffset_4() { return &___screenOffset_4; }
	inline void set_screenOffset_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___screenOffset_4 = value;
	}

	inline static int32_t get_offset_of_playerNameText_5() { return static_cast<int32_t>(offsetof(PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735, ___playerNameText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_playerNameText_5() const { return ___playerNameText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_playerNameText_5() { return &___playerNameText_5; }
	inline void set_playerNameText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___playerNameText_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerNameText_5), (void*)value);
	}

	inline static int32_t get_offset_of_playerHealthSlider_6() { return static_cast<int32_t>(offsetof(PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735, ___playerHealthSlider_6)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_playerHealthSlider_6() const { return ___playerHealthSlider_6; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_playerHealthSlider_6() { return &___playerHealthSlider_6; }
	inline void set_playerHealthSlider_6(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___playerHealthSlider_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerHealthSlider_6), (void*)value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735, ___target_7)); }
	inline PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144 * get_target_7() const { return ___target_7; }
	inline PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144 ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144 * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_7), (void*)value);
	}

	inline static int32_t get_offset_of_characterControllerHeight_8() { return static_cast<int32_t>(offsetof(PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735, ___characterControllerHeight_8)); }
	inline float get_characterControllerHeight_8() const { return ___characterControllerHeight_8; }
	inline float* get_address_of_characterControllerHeight_8() { return &___characterControllerHeight_8; }
	inline void set_characterControllerHeight_8(float value)
	{
		___characterControllerHeight_8 = value;
	}

	inline static int32_t get_offset_of_targetTransform_9() { return static_cast<int32_t>(offsetof(PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735, ___targetTransform_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_targetTransform_9() const { return ___targetTransform_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_targetTransform_9() { return &___targetTransform_9; }
	inline void set_targetTransform_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___targetTransform_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetTransform_9), (void*)value);
	}

	inline static int32_t get_offset_of_targetRenderer_10() { return static_cast<int32_t>(offsetof(PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735, ___targetRenderer_10)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_targetRenderer_10() const { return ___targetRenderer_10; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_targetRenderer_10() { return &___targetRenderer_10; }
	inline void set_targetRenderer_10(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___targetRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetRenderer_10), (void*)value);
	}

	inline static int32_t get_offset_of__canvasGroup_11() { return static_cast<int32_t>(offsetof(PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735, ____canvasGroup_11)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get__canvasGroup_11() const { return ____canvasGroup_11; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of__canvasGroup_11() { return &____canvasGroup_11; }
	inline void set__canvasGroup_11(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		____canvasGroup_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____canvasGroup_11), (void*)value);
	}

	inline static int32_t get_offset_of_targetPosition_12() { return static_cast<int32_t>(offsetof(PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735, ___targetPosition_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPosition_12() const { return ___targetPosition_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPosition_12() { return &___targetPosition_12; }
	inline void set_targetPosition_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPosition_12 = value;
	}
};


// Photon.Pun.Demo.SlotRacer.SlotLanes
struct  SlotLanes_t94D5B08A4B819627731EE5C5AC3FE4860198CD5B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Photon.Pun.Demo.SlotRacer.Utils.SplinePosition[] Photon.Pun.Demo.SlotRacer.SlotLanes::GridPositions
	SplinePositionU5BU5D_tC56837B60736C57A39CA7B1860FBA12B124E3027* ___GridPositions_5;

public:
	inline static int32_t get_offset_of_GridPositions_5() { return static_cast<int32_t>(offsetof(SlotLanes_t94D5B08A4B819627731EE5C5AC3FE4860198CD5B, ___GridPositions_5)); }
	inline SplinePositionU5BU5D_tC56837B60736C57A39CA7B1860FBA12B124E3027* get_GridPositions_5() const { return ___GridPositions_5; }
	inline SplinePositionU5BU5D_tC56837B60736C57A39CA7B1860FBA12B124E3027** get_address_of_GridPositions_5() { return &___GridPositions_5; }
	inline void set_GridPositions_5(SplinePositionU5BU5D_tC56837B60736C57A39CA7B1860FBA12B124E3027* value)
	{
		___GridPositions_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GridPositions_5), (void*)value);
	}
};

struct SlotLanes_t94D5B08A4B819627731EE5C5AC3FE4860198CD5B_StaticFields
{
public:
	// Photon.Pun.Demo.SlotRacer.SlotLanes Photon.Pun.Demo.SlotRacer.SlotLanes::Instance
	SlotLanes_t94D5B08A4B819627731EE5C5AC3FE4860198CD5B * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(SlotLanes_t94D5B08A4B819627731EE5C5AC3FE4860198CD5B_StaticFields, ___Instance_4)); }
	inline SlotLanes_t94D5B08A4B819627731EE5C5AC3FE4860198CD5B * get_Instance_4() const { return ___Instance_4; }
	inline SlotLanes_t94D5B08A4B819627731EE5C5AC3FE4860198CD5B ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(SlotLanes_t94D5B08A4B819627731EE5C5AC3FE4860198CD5B * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Instance_4), (void*)value);
	}
};


// Photon.Pun.Demo.SlotRacer.SlotRacerSplashScreen
struct  SlotRacerSplashScreen_t9653B897FC883CCE01E40D345A69D973CFBC3BC9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Photon.Pun.Demo.SlotRacer.SlotRacerSplashScreen::PunCockpit_scene
	String_t* ___PunCockpit_scene_4;
	// UnityEngine.UI.Text Photon.Pun.Demo.SlotRacer.SlotRacerSplashScreen::WarningText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___WarningText_5;
	// UnityEngine.GameObject Photon.Pun.Demo.SlotRacer.SlotRacerSplashScreen::SplashScreen
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___SplashScreen_6;

public:
	inline static int32_t get_offset_of_PunCockpit_scene_4() { return static_cast<int32_t>(offsetof(SlotRacerSplashScreen_t9653B897FC883CCE01E40D345A69D973CFBC3BC9, ___PunCockpit_scene_4)); }
	inline String_t* get_PunCockpit_scene_4() const { return ___PunCockpit_scene_4; }
	inline String_t** get_address_of_PunCockpit_scene_4() { return &___PunCockpit_scene_4; }
	inline void set_PunCockpit_scene_4(String_t* value)
	{
		___PunCockpit_scene_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PunCockpit_scene_4), (void*)value);
	}

	inline static int32_t get_offset_of_WarningText_5() { return static_cast<int32_t>(offsetof(SlotRacerSplashScreen_t9653B897FC883CCE01E40D345A69D973CFBC3BC9, ___WarningText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_WarningText_5() const { return ___WarningText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_WarningText_5() { return &___WarningText_5; }
	inline void set_WarningText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___WarningText_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WarningText_5), (void*)value);
	}

	inline static int32_t get_offset_of_SplashScreen_6() { return static_cast<int32_t>(offsetof(SlotRacerSplashScreen_t9653B897FC883CCE01E40D345A69D973CFBC3BC9, ___SplashScreen_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_SplashScreen_6() const { return ___SplashScreen_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_SplashScreen_6() { return &___SplashScreen_6; }
	inline void set_SplashScreen_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___SplashScreen_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SplashScreen_6), (void*)value);
	}
};


// Photon.Pun.Demo.SlotRacer.Utils.BezierCurve
struct  BezierCurve_tB4DF224E68E6BDD10E36E963033E370DA8A62714  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3[] Photon.Pun.Demo.SlotRacer.Utils.BezierCurve::points
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___points_4;

public:
	inline static int32_t get_offset_of_points_4() { return static_cast<int32_t>(offsetof(BezierCurve_tB4DF224E68E6BDD10E36E963033E370DA8A62714, ___points_4)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_points_4() const { return ___points_4; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_points_4() { return &___points_4; }
	inline void set_points_4(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___points_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___points_4), (void*)value);
	}
};


// Photon.Pun.Demo.SlotRacer.Utils.BezierSpline
struct  BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3[] Photon.Pun.Demo.SlotRacer.Utils.BezierSpline::points
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___points_4;
	// System.Single[] Photon.Pun.Demo.SlotRacer.Utils.BezierSpline::lengths
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___lengths_5;
	// System.Single[] Photon.Pun.Demo.SlotRacer.Utils.BezierSpline::lengthsTime
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___lengthsTime_6;
	// System.Single Photon.Pun.Demo.SlotRacer.Utils.BezierSpline::TotalLength
	float ___TotalLength_7;
	// Photon.Pun.Demo.SlotRacer.Utils.BezierControlPointMode[] Photon.Pun.Demo.SlotRacer.Utils.BezierSpline::modes
	BezierControlPointModeU5BU5D_tA7D7CDA0CC3A3DDF4CBC7A2065FE5E0F8FEE2577* ___modes_8;
	// System.Boolean Photon.Pun.Demo.SlotRacer.Utils.BezierSpline::loop
	bool ___loop_9;

public:
	inline static int32_t get_offset_of_points_4() { return static_cast<int32_t>(offsetof(BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944, ___points_4)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_points_4() const { return ___points_4; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_points_4() { return &___points_4; }
	inline void set_points_4(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___points_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___points_4), (void*)value);
	}

	inline static int32_t get_offset_of_lengths_5() { return static_cast<int32_t>(offsetof(BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944, ___lengths_5)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_lengths_5() const { return ___lengths_5; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_lengths_5() { return &___lengths_5; }
	inline void set_lengths_5(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___lengths_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lengths_5), (void*)value);
	}

	inline static int32_t get_offset_of_lengthsTime_6() { return static_cast<int32_t>(offsetof(BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944, ___lengthsTime_6)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_lengthsTime_6() const { return ___lengthsTime_6; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_lengthsTime_6() { return &___lengthsTime_6; }
	inline void set_lengthsTime_6(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___lengthsTime_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lengthsTime_6), (void*)value);
	}

	inline static int32_t get_offset_of_TotalLength_7() { return static_cast<int32_t>(offsetof(BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944, ___TotalLength_7)); }
	inline float get_TotalLength_7() const { return ___TotalLength_7; }
	inline float* get_address_of_TotalLength_7() { return &___TotalLength_7; }
	inline void set_TotalLength_7(float value)
	{
		___TotalLength_7 = value;
	}

	inline static int32_t get_offset_of_modes_8() { return static_cast<int32_t>(offsetof(BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944, ___modes_8)); }
	inline BezierControlPointModeU5BU5D_tA7D7CDA0CC3A3DDF4CBC7A2065FE5E0F8FEE2577* get_modes_8() const { return ___modes_8; }
	inline BezierControlPointModeU5BU5D_tA7D7CDA0CC3A3DDF4CBC7A2065FE5E0F8FEE2577** get_address_of_modes_8() { return &___modes_8; }
	inline void set_modes_8(BezierControlPointModeU5BU5D_tA7D7CDA0CC3A3DDF4CBC7A2065FE5E0F8FEE2577* value)
	{
		___modes_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___modes_8), (void*)value);
	}

	inline static int32_t get_offset_of_loop_9() { return static_cast<int32_t>(offsetof(BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944, ___loop_9)); }
	inline bool get_loop_9() const { return ___loop_9; }
	inline bool* get_address_of_loop_9() { return &___loop_9; }
	inline void set_loop_9(bool value)
	{
		___loop_9 = value;
	}
};


// Photon.Pun.Demo.SlotRacer.Utils.Line
struct  Line_t4FA3BA75F52436878AC0ED91D0AE1FF9CF112DEC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3 Photon.Pun.Demo.SlotRacer.Utils.Line::p0
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p0_4;
	// UnityEngine.Vector3 Photon.Pun.Demo.SlotRacer.Utils.Line::p1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___p1_5;

public:
	inline static int32_t get_offset_of_p0_4() { return static_cast<int32_t>(offsetof(Line_t4FA3BA75F52436878AC0ED91D0AE1FF9CF112DEC, ___p0_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_p0_4() const { return ___p0_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_p0_4() { return &___p0_4; }
	inline void set_p0_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___p0_4 = value;
	}

	inline static int32_t get_offset_of_p1_5() { return static_cast<int32_t>(offsetof(Line_t4FA3BA75F52436878AC0ED91D0AE1FF9CF112DEC, ___p1_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_p1_5() const { return ___p1_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_p1_5() { return &___p1_5; }
	inline void set_p1_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___p1_5 = value;
	}
};


// Photon.Pun.Demo.SlotRacer.Utils.SplinePosition
struct  SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Photon.Pun.Demo.SlotRacer.Utils.BezierSpline Photon.Pun.Demo.SlotRacer.Utils.SplinePosition::Spline
	BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944 * ___Spline_4;
	// System.Boolean Photon.Pun.Demo.SlotRacer.Utils.SplinePosition::reverse
	bool ___reverse_5;
	// System.Boolean Photon.Pun.Demo.SlotRacer.Utils.SplinePosition::lookForward
	bool ___lookForward_6;
	// System.Single Photon.Pun.Demo.SlotRacer.Utils.SplinePosition::currentDistance
	float ___currentDistance_7;
	// System.Single Photon.Pun.Demo.SlotRacer.Utils.SplinePosition::currentClampedDistance
	float ___currentClampedDistance_8;
	// System.Single Photon.Pun.Demo.SlotRacer.Utils.SplinePosition::LastDistance
	float ___LastDistance_9;

public:
	inline static int32_t get_offset_of_Spline_4() { return static_cast<int32_t>(offsetof(SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392, ___Spline_4)); }
	inline BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944 * get_Spline_4() const { return ___Spline_4; }
	inline BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944 ** get_address_of_Spline_4() { return &___Spline_4; }
	inline void set_Spline_4(BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944 * value)
	{
		___Spline_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Spline_4), (void*)value);
	}

	inline static int32_t get_offset_of_reverse_5() { return static_cast<int32_t>(offsetof(SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392, ___reverse_5)); }
	inline bool get_reverse_5() const { return ___reverse_5; }
	inline bool* get_address_of_reverse_5() { return &___reverse_5; }
	inline void set_reverse_5(bool value)
	{
		___reverse_5 = value;
	}

	inline static int32_t get_offset_of_lookForward_6() { return static_cast<int32_t>(offsetof(SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392, ___lookForward_6)); }
	inline bool get_lookForward_6() const { return ___lookForward_6; }
	inline bool* get_address_of_lookForward_6() { return &___lookForward_6; }
	inline void set_lookForward_6(bool value)
	{
		___lookForward_6 = value;
	}

	inline static int32_t get_offset_of_currentDistance_7() { return static_cast<int32_t>(offsetof(SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392, ___currentDistance_7)); }
	inline float get_currentDistance_7() const { return ___currentDistance_7; }
	inline float* get_address_of_currentDistance_7() { return &___currentDistance_7; }
	inline void set_currentDistance_7(float value)
	{
		___currentDistance_7 = value;
	}

	inline static int32_t get_offset_of_currentClampedDistance_8() { return static_cast<int32_t>(offsetof(SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392, ___currentClampedDistance_8)); }
	inline float get_currentClampedDistance_8() const { return ___currentClampedDistance_8; }
	inline float* get_address_of_currentClampedDistance_8() { return &___currentClampedDistance_8; }
	inline void set_currentClampedDistance_8(float value)
	{
		___currentClampedDistance_8 = value;
	}

	inline static int32_t get_offset_of_LastDistance_9() { return static_cast<int32_t>(offsetof(SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392, ___LastDistance_9)); }
	inline float get_LastDistance_9() const { return ___LastDistance_9; }
	inline float* get_address_of_LastDistance_9() { return &___LastDistance_9; }
	inline void set_LastDistance_9(float value)
	{
		___LastDistance_9 = value;
	}
};


// Photon.Pun.Demo.SlotRacer.Utils.SplineWalker
struct  SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Photon.Pun.Demo.SlotRacer.Utils.BezierSpline Photon.Pun.Demo.SlotRacer.Utils.SplineWalker::spline
	BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944 * ___spline_4;
	// System.Single Photon.Pun.Demo.SlotRacer.Utils.SplineWalker::Speed
	float ___Speed_5;
	// System.Boolean Photon.Pun.Demo.SlotRacer.Utils.SplineWalker::lookForward
	bool ___lookForward_6;
	// System.Boolean Photon.Pun.Demo.SlotRacer.Utils.SplineWalker::reverse
	bool ___reverse_7;
	// System.Single Photon.Pun.Demo.SlotRacer.Utils.SplineWalker::progress
	float ___progress_8;
	// System.Single Photon.Pun.Demo.SlotRacer.Utils.SplineWalker::currentDistance
	float ___currentDistance_9;
	// System.Single Photon.Pun.Demo.SlotRacer.Utils.SplineWalker::currentClampedDistance
	float ___currentClampedDistance_10;

public:
	inline static int32_t get_offset_of_spline_4() { return static_cast<int32_t>(offsetof(SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F, ___spline_4)); }
	inline BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944 * get_spline_4() const { return ___spline_4; }
	inline BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944 ** get_address_of_spline_4() { return &___spline_4; }
	inline void set_spline_4(BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944 * value)
	{
		___spline_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spline_4), (void*)value);
	}

	inline static int32_t get_offset_of_Speed_5() { return static_cast<int32_t>(offsetof(SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F, ___Speed_5)); }
	inline float get_Speed_5() const { return ___Speed_5; }
	inline float* get_address_of_Speed_5() { return &___Speed_5; }
	inline void set_Speed_5(float value)
	{
		___Speed_5 = value;
	}

	inline static int32_t get_offset_of_lookForward_6() { return static_cast<int32_t>(offsetof(SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F, ___lookForward_6)); }
	inline bool get_lookForward_6() const { return ___lookForward_6; }
	inline bool* get_address_of_lookForward_6() { return &___lookForward_6; }
	inline void set_lookForward_6(bool value)
	{
		___lookForward_6 = value;
	}

	inline static int32_t get_offset_of_reverse_7() { return static_cast<int32_t>(offsetof(SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F, ___reverse_7)); }
	inline bool get_reverse_7() const { return ___reverse_7; }
	inline bool* get_address_of_reverse_7() { return &___reverse_7; }
	inline void set_reverse_7(bool value)
	{
		___reverse_7 = value;
	}

	inline static int32_t get_offset_of_progress_8() { return static_cast<int32_t>(offsetof(SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F, ___progress_8)); }
	inline float get_progress_8() const { return ___progress_8; }
	inline float* get_address_of_progress_8() { return &___progress_8; }
	inline void set_progress_8(float value)
	{
		___progress_8 = value;
	}

	inline static int32_t get_offset_of_currentDistance_9() { return static_cast<int32_t>(offsetof(SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F, ___currentDistance_9)); }
	inline float get_currentDistance_9() const { return ___currentDistance_9; }
	inline float* get_address_of_currentDistance_9() { return &___currentDistance_9; }
	inline void set_currentDistance_9(float value)
	{
		___currentDistance_9 = value;
	}

	inline static int32_t get_offset_of_currentClampedDistance_10() { return static_cast<int32_t>(offsetof(SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F, ___currentClampedDistance_10)); }
	inline float get_currentClampedDistance_10() const { return ___currentClampedDistance_10; }
	inline float* get_address_of_currentClampedDistance_10() { return &___currentClampedDistance_10; }
	inline void set_currentClampedDistance_10(float value)
	{
		___currentClampedDistance_10 = value;
	}
};


// Photon.Pun.MonoBehaviourPun
struct  MonoBehaviourPun_t41335C3B2B6804088B520155103E3A0EEA4F814A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Photon.Pun.PhotonView Photon.Pun.MonoBehaviourPun::pvCache
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___pvCache_4;

public:
	inline static int32_t get_offset_of_pvCache_4() { return static_cast<int32_t>(offsetof(MonoBehaviourPun_t41335C3B2B6804088B520155103E3A0EEA4F814A, ___pvCache_4)); }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * get_pvCache_4() const { return ___pvCache_4; }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** get_address_of_pvCache_4() { return &___pvCache_4; }
	inline void set_pvCache_4(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		___pvCache_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pvCache_4), (void*)value);
	}
};


// Photon.Realtime.Demo.ConnectAndJoinRandomLb
struct  ConnectAndJoinRandomLb_t5FC18B8B9775DA842D8BA0629CA52F5348FDC462  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Photon.Realtime.Demo.ConnectAndJoinRandomLb::AppId
	String_t* ___AppId_4;
	// Photon.Realtime.LoadBalancingClient Photon.Realtime.Demo.ConnectAndJoinRandomLb::lbc
	LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A * ___lbc_5;
	// Photon.Realtime.ConnectionHandler Photon.Realtime.Demo.ConnectAndJoinRandomLb::ch
	ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0 * ___ch_6;
	// UnityEngine.UI.Text Photon.Realtime.Demo.ConnectAndJoinRandomLb::StateUiText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___StateUiText_7;

public:
	inline static int32_t get_offset_of_AppId_4() { return static_cast<int32_t>(offsetof(ConnectAndJoinRandomLb_t5FC18B8B9775DA842D8BA0629CA52F5348FDC462, ___AppId_4)); }
	inline String_t* get_AppId_4() const { return ___AppId_4; }
	inline String_t** get_address_of_AppId_4() { return &___AppId_4; }
	inline void set_AppId_4(String_t* value)
	{
		___AppId_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AppId_4), (void*)value);
	}

	inline static int32_t get_offset_of_lbc_5() { return static_cast<int32_t>(offsetof(ConnectAndJoinRandomLb_t5FC18B8B9775DA842D8BA0629CA52F5348FDC462, ___lbc_5)); }
	inline LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A * get_lbc_5() const { return ___lbc_5; }
	inline LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A ** get_address_of_lbc_5() { return &___lbc_5; }
	inline void set_lbc_5(LoadBalancingClient_t83BDB381BB0B2B367F4B8AA0280EB8204428611A * value)
	{
		___lbc_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lbc_5), (void*)value);
	}

	inline static int32_t get_offset_of_ch_6() { return static_cast<int32_t>(offsetof(ConnectAndJoinRandomLb_t5FC18B8B9775DA842D8BA0629CA52F5348FDC462, ___ch_6)); }
	inline ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0 * get_ch_6() const { return ___ch_6; }
	inline ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0 ** get_address_of_ch_6() { return &___ch_6; }
	inline void set_ch_6(ConnectionHandler_tF5BF60817D048DB87BEEAEE436A4BA77E4BAC0C0 * value)
	{
		___ch_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ch_6), (void*)value);
	}

	inline static int32_t get_offset_of_StateUiText_7() { return static_cast<int32_t>(offsetof(ConnectAndJoinRandomLb_t5FC18B8B9775DA842D8BA0629CA52F5348FDC462, ___StateUiText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_StateUiText_7() const { return ___StateUiText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_StateUiText_7() { return &___StateUiText_7; }
	inline void set_StateUiText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___StateUiText_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StateUiText_7), (void*)value);
	}
};


// SimpleFollowCam
struct  SimpleFollowCam_t07E02D8F29399188F5ACC766708003FCACE71A5E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform SimpleFollowCam::m_Target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_Target_4;
	// System.Single SimpleFollowCam::m_FollowSpeed
	float ___m_FollowSpeed_5;

public:
	inline static int32_t get_offset_of_m_Target_4() { return static_cast<int32_t>(offsetof(SimpleFollowCam_t07E02D8F29399188F5ACC766708003FCACE71A5E, ___m_Target_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_Target_4() const { return ___m_Target_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_Target_4() { return &___m_Target_4; }
	inline void set_m_Target_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_Target_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Target_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_FollowSpeed_5() { return static_cast<int32_t>(offsetof(SimpleFollowCam_t07E02D8F29399188F5ACC766708003FCACE71A5E, ___m_FollowSpeed_5)); }
	inline float get_m_FollowSpeed_5() const { return ___m_FollowSpeed_5; }
	inline float* get_address_of_m_FollowSpeed_5() { return &___m_FollowSpeed_5; }
	inline void set_m_FollowSpeed_5(float value)
	{
		___m_FollowSpeed_5 = value;
	}
};


// SimpleRotate
struct  SimpleRotate_t9FC225F770B17BDD805878D0B8945D58CB80AB35  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// StandardizedBowForHand
struct  StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single StandardizedBowForHand::currentTime
	float ___currentTime_4;
	// System.Single StandardizedBowForHand::lerpPercentage
	float ___lerpPercentage_5;
	// System.Single StandardizedBowForHand::startingAngleDown
	float ___startingAngleDown_6;
	// System.Single StandardizedBowForHand::startingAngleUp
	float ___startingAngleUp_7;
	// System.Single StandardizedBowForHand::currentAngleDown
	float ___currentAngleDown_8;
	// System.Single StandardizedBowForHand::currentAngleUp
	float ___currentAngleUp_9;
	// UnityEngine.Vector3 StandardizedBowForHand::stringStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___stringStartPos_10;
	// UnityEngine.Vector3 StandardizedBowForHand::stringEndPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___stringEndPos_11;
	// UnityEngine.Vector3 StandardizedBowForHand::stringLastPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___stringLastPos_12;
	// UnityEngine.Vector3 StandardizedBowForHand::bowDirPull
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bowDirPull_13;
	// UnityEngine.Vector3 StandardizedBowForHand::bowDirRetract
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bowDirRetract_14;
	// UnityEngine.Vector3 StandardizedBowForHand::firstStartUpJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartUpJointRot1_15;
	// UnityEngine.Vector3 StandardizedBowForHand::firstStartUpJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartUpJointRot2_16;
	// UnityEngine.Vector3 StandardizedBowForHand::firstStartUpJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartUpJointRot3_17;
	// UnityEngine.Vector3 StandardizedBowForHand::firstStartDownJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartDownJointRot1_18;
	// UnityEngine.Vector3 StandardizedBowForHand::firstStartDownJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartDownJointRot2_19;
	// UnityEngine.Vector3 StandardizedBowForHand::firstStartDownJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartDownJointRot3_20;
	// UnityEngine.Vector3 StandardizedBowForHand::firstEndUpJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndUpJointRot1_21;
	// UnityEngine.Vector3 StandardizedBowForHand::firstEndUpJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndUpJointRot2_22;
	// UnityEngine.Vector3 StandardizedBowForHand::firstEndUpJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndUpJointRot3_23;
	// UnityEngine.Vector3 StandardizedBowForHand::firstEndDownJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndDownJointRot1_24;
	// UnityEngine.Vector3 StandardizedBowForHand::firstEndDownJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndDownJointRot2_25;
	// UnityEngine.Vector3 StandardizedBowForHand::firstEndDownJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndDownJointRot3_26;
	// UnityEngine.Vector3 StandardizedBowForHand::firstLastUpJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastUpJointRot1_27;
	// UnityEngine.Vector3 StandardizedBowForHand::firstLastUpJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastUpJointRot2_28;
	// UnityEngine.Vector3 StandardizedBowForHand::firstLastUpJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastUpJointRot3_29;
	// UnityEngine.Vector3 StandardizedBowForHand::firstLastDownJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastDownJointRot1_30;
	// UnityEngine.Vector3 StandardizedBowForHand::firstLastDownJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastDownJointRot2_31;
	// UnityEngine.Vector3 StandardizedBowForHand::firstLastDownJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastDownJointRot3_32;
	// UnityEngine.GameObject StandardizedBowForHand::lastProjectile
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lastProjectile_33;
	// UnityEngine.GameObject StandardizedBowForHand::stringStartObj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stringStartObj_34;
	// UnityEngine.GameObject StandardizedBowForHand::stringEndObj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stringEndObj_35;
	// UnityEngine.GameObject StandardizedBowForHand::poolHolder
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___poolHolder_36;
	// UnityEngine.Transform StandardizedBowForHand::stringEndObjTrans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___stringEndObjTrans_37;
	// UnityEngine.Transform StandardizedBowForHand::stringStartObjTrans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___stringStartObjTrans_38;
	// UnityEngine.Transform StandardizedBowForHand::poolHolderTrans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___poolHolderTrans_39;
	// StandardizedProjectileForHand StandardizedBowForHand::lastProjectileScript
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6 * ___lastProjectileScript_40;
	// System.Collections.Generic.Queue`1<UnityEngine.GameObject> StandardizedBowForHand::projectilePool
	Queue_1_t0069840E94B59EC80C3FA49EF6ECA3A067555290 * ___projectilePool_41;
	// UnityEngine.Transform StandardizedBowForHand::lastProjectileTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___lastProjectileTransform_42;
	// UnityEngine.Rigidbody StandardizedBowForHand::lastProjectileRigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___lastProjectileRigidbody_43;
	// UnityEngine.AudioSource StandardizedBowForHand::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_44;
	// System.Single StandardizedBowForHand::currentStressOnString
	float ___currentStressOnString_45;
	// System.Int32 StandardizedBowForHand::pullStateHash
	int32_t ___pullStateHash_46;
	// System.Int32 StandardizedBowForHand::pullStateHash2
	int32_t ___pullStateHash2_47;
	// UnityEngine.AnimatorStateInfo StandardizedBowForHand::currentStateInfo
	AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  ___currentStateInfo_48;
	// System.Single StandardizedBowForHand::projectileDelayTimer
	float ___projectileDelayTimer_49;
	// System.Single StandardizedBowForHand::pullDelayTimer
	float ___pullDelayTimer_50;
	// System.Boolean StandardizedBowForHand::justLeftString
	bool ___justLeftString_51;
	// System.Boolean StandardizedBowForHand::justPulledString
	bool ___justPulledString_52;
	// System.Boolean StandardizedBowForHand::justStoppedPulling
	bool ___justStoppedPulling_53;
	// UnityEngine.Animator StandardizedBowForHand::bowUserAnim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___bowUserAnim_56;
	// UnityEngine.Transform StandardizedBowForHand::bowUserPullHand
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowUserPullHand_57;
	// System.Int32 StandardizedBowForHand::pullStateLayerIndex
	int32_t ___pullStateLayerIndex_58;
	// System.String StandardizedBowForHand::pullStateName
	String_t* ___pullStateName_59;
	// System.String StandardizedBowForHand::pullStateName2
	String_t* ___pullStateName2_60;
	// System.Single StandardizedBowForHand::pullStateDelay
	float ___pullStateDelay_61;
	// System.Single StandardizedBowForHand::projectileHoldDelay
	float ___projectileHoldDelay_62;
	// System.Boolean StandardizedBowForHand::projectileOnHand
	bool ___projectileOnHand_63;
	// UnityEngine.Transform StandardizedBowForHand::bowUpJoint1
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowUpJoint1_64;
	// UnityEngine.Transform StandardizedBowForHand::bowUpJoint2
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowUpJoint2_65;
	// UnityEngine.Transform StandardizedBowForHand::bowUpJoint3
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowUpJoint3_66;
	// UnityEngine.Transform StandardizedBowForHand::bowDownJoint1
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowDownJoint1_67;
	// UnityEngine.Transform StandardizedBowForHand::bowDownJoint2
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowDownJoint2_68;
	// UnityEngine.Transform StandardizedBowForHand::bowDownJoint3
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowDownJoint3_69;
	// UnityEngine.Transform StandardizedBowForHand::bowStringPoint
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowStringPoint_70;
	// StandardizedBowForHand_jointsDirection StandardizedBowForHand::joint1RotateDirectionAxis
	int32_t ___joint1RotateDirectionAxis_71;
	// StandardizedBowForHand_jointsDirection StandardizedBowForHand::joint2RotateDirectionAxis
	int32_t ___joint2RotateDirectionAxis_72;
	// StandardizedBowForHand_jointsDirection StandardizedBowForHand::joint3RotateDirectionAxis
	int32_t ___joint3RotateDirectionAxis_73;
	// System.Single StandardizedBowForHand::bendAngleLimitFirstJoints
	float ___bendAngleLimitFirstJoints_74;
	// System.Single StandardizedBowForHand::bendAngleLimitSecondJoints
	float ___bendAngleLimitSecondJoints_75;
	// System.Single StandardizedBowForHand::bendAngleLimitThirdJoints
	float ___bendAngleLimitThirdJoints_76;
	// System.Boolean StandardizedBowForHand::inverseBend
	bool ___inverseBend_77;
	// StandardizedBowForHand_axisDirection StandardizedBowForHand::stringMoveDirectionAxis
	int32_t ___stringMoveDirectionAxis_78;
	// System.Single StandardizedBowForHand::stringMoveAmount
	float ___stringMoveAmount_79;
	// System.Single StandardizedBowForHand::stringMoveTime
	float ___stringMoveTime_80;
	// System.Single StandardizedBowForHand::stringRetractTime
	float ___stringRetractTime_81;
	// System.Boolean StandardizedBowForHand::inversePull
	bool ___inversePull_82;
	// StandardizedBowForHand_stringStressEaseType StandardizedBowForHand::stringMovementChoice
	int32_t ___stringMovementChoice_83;
	// StandardizedBowForHand_stringRetractEaseType StandardizedBowForHand::stringRetractChoice
	int32_t ___stringRetractChoice_84;
	// System.Single StandardizedBowForHand::maxStringStrength
	float ___maxStringStrength_85;
	// StandardizedBowForHand_accuracyProjectile StandardizedBowForHand::projectileAccuracy
	int32_t ___projectileAccuracy_86;
	// UnityEngine.GameObject StandardizedBowForHand::projectile
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectile_87;
	// System.Int32 StandardizedBowForHand::projectilePoolSize
	int32_t ___projectilePoolSize_88;
	// UnityEngine.Vector3 StandardizedBowForHand::projectileHoldPosOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___projectileHoldPosOffset_89;
	// System.Boolean StandardizedBowForHand::isPosOffsetLocal
	bool ___isPosOffsetLocal_90;
	// UnityEngine.Vector3 StandardizedBowForHand::projectileHoldRotOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___projectileHoldRotOffset_91;
	// StandardizedBowForHand_axisDirection StandardizedBowForHand::projectileForwardAxis
	int32_t ___projectileForwardAxis_92;
	// System.Single StandardizedBowForHand::soundVolume
	float ___soundVolume_93;
	// UnityEngine.AudioClip StandardizedBowForHand::pullSound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___pullSound_94;
	// UnityEngine.AudioClip StandardizedBowForHand::retractSound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___retractSound_95;
	// System.Boolean StandardizedBowForHand::stressEffectOnSound
	bool ___stressEffectOnSound_96;
	// System.Single StandardizedBowForHand::x
	float ___x_97;
	// System.Single StandardizedBowForHand::y
	float ___y_98;

public:
	inline static int32_t get_offset_of_currentTime_4() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___currentTime_4)); }
	inline float get_currentTime_4() const { return ___currentTime_4; }
	inline float* get_address_of_currentTime_4() { return &___currentTime_4; }
	inline void set_currentTime_4(float value)
	{
		___currentTime_4 = value;
	}

	inline static int32_t get_offset_of_lerpPercentage_5() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___lerpPercentage_5)); }
	inline float get_lerpPercentage_5() const { return ___lerpPercentage_5; }
	inline float* get_address_of_lerpPercentage_5() { return &___lerpPercentage_5; }
	inline void set_lerpPercentage_5(float value)
	{
		___lerpPercentage_5 = value;
	}

	inline static int32_t get_offset_of_startingAngleDown_6() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___startingAngleDown_6)); }
	inline float get_startingAngleDown_6() const { return ___startingAngleDown_6; }
	inline float* get_address_of_startingAngleDown_6() { return &___startingAngleDown_6; }
	inline void set_startingAngleDown_6(float value)
	{
		___startingAngleDown_6 = value;
	}

	inline static int32_t get_offset_of_startingAngleUp_7() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___startingAngleUp_7)); }
	inline float get_startingAngleUp_7() const { return ___startingAngleUp_7; }
	inline float* get_address_of_startingAngleUp_7() { return &___startingAngleUp_7; }
	inline void set_startingAngleUp_7(float value)
	{
		___startingAngleUp_7 = value;
	}

	inline static int32_t get_offset_of_currentAngleDown_8() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___currentAngleDown_8)); }
	inline float get_currentAngleDown_8() const { return ___currentAngleDown_8; }
	inline float* get_address_of_currentAngleDown_8() { return &___currentAngleDown_8; }
	inline void set_currentAngleDown_8(float value)
	{
		___currentAngleDown_8 = value;
	}

	inline static int32_t get_offset_of_currentAngleUp_9() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___currentAngleUp_9)); }
	inline float get_currentAngleUp_9() const { return ___currentAngleUp_9; }
	inline float* get_address_of_currentAngleUp_9() { return &___currentAngleUp_9; }
	inline void set_currentAngleUp_9(float value)
	{
		___currentAngleUp_9 = value;
	}

	inline static int32_t get_offset_of_stringStartPos_10() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stringStartPos_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_stringStartPos_10() const { return ___stringStartPos_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_stringStartPos_10() { return &___stringStartPos_10; }
	inline void set_stringStartPos_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___stringStartPos_10 = value;
	}

	inline static int32_t get_offset_of_stringEndPos_11() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stringEndPos_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_stringEndPos_11() const { return ___stringEndPos_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_stringEndPos_11() { return &___stringEndPos_11; }
	inline void set_stringEndPos_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___stringEndPos_11 = value;
	}

	inline static int32_t get_offset_of_stringLastPos_12() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stringLastPos_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_stringLastPos_12() const { return ___stringLastPos_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_stringLastPos_12() { return &___stringLastPos_12; }
	inline void set_stringLastPos_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___stringLastPos_12 = value;
	}

	inline static int32_t get_offset_of_bowDirPull_13() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bowDirPull_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bowDirPull_13() const { return ___bowDirPull_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bowDirPull_13() { return &___bowDirPull_13; }
	inline void set_bowDirPull_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bowDirPull_13 = value;
	}

	inline static int32_t get_offset_of_bowDirRetract_14() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bowDirRetract_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bowDirRetract_14() const { return ___bowDirRetract_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bowDirRetract_14() { return &___bowDirRetract_14; }
	inline void set_bowDirRetract_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bowDirRetract_14 = value;
	}

	inline static int32_t get_offset_of_firstStartUpJointRot1_15() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstStartUpJointRot1_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartUpJointRot1_15() const { return ___firstStartUpJointRot1_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartUpJointRot1_15() { return &___firstStartUpJointRot1_15; }
	inline void set_firstStartUpJointRot1_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartUpJointRot1_15 = value;
	}

	inline static int32_t get_offset_of_firstStartUpJointRot2_16() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstStartUpJointRot2_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartUpJointRot2_16() const { return ___firstStartUpJointRot2_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartUpJointRot2_16() { return &___firstStartUpJointRot2_16; }
	inline void set_firstStartUpJointRot2_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartUpJointRot2_16 = value;
	}

	inline static int32_t get_offset_of_firstStartUpJointRot3_17() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstStartUpJointRot3_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartUpJointRot3_17() const { return ___firstStartUpJointRot3_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartUpJointRot3_17() { return &___firstStartUpJointRot3_17; }
	inline void set_firstStartUpJointRot3_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartUpJointRot3_17 = value;
	}

	inline static int32_t get_offset_of_firstStartDownJointRot1_18() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstStartDownJointRot1_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartDownJointRot1_18() const { return ___firstStartDownJointRot1_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartDownJointRot1_18() { return &___firstStartDownJointRot1_18; }
	inline void set_firstStartDownJointRot1_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartDownJointRot1_18 = value;
	}

	inline static int32_t get_offset_of_firstStartDownJointRot2_19() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstStartDownJointRot2_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartDownJointRot2_19() const { return ___firstStartDownJointRot2_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartDownJointRot2_19() { return &___firstStartDownJointRot2_19; }
	inline void set_firstStartDownJointRot2_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartDownJointRot2_19 = value;
	}

	inline static int32_t get_offset_of_firstStartDownJointRot3_20() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstStartDownJointRot3_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartDownJointRot3_20() const { return ___firstStartDownJointRot3_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartDownJointRot3_20() { return &___firstStartDownJointRot3_20; }
	inline void set_firstStartDownJointRot3_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartDownJointRot3_20 = value;
	}

	inline static int32_t get_offset_of_firstEndUpJointRot1_21() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstEndUpJointRot1_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndUpJointRot1_21() const { return ___firstEndUpJointRot1_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndUpJointRot1_21() { return &___firstEndUpJointRot1_21; }
	inline void set_firstEndUpJointRot1_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndUpJointRot1_21 = value;
	}

	inline static int32_t get_offset_of_firstEndUpJointRot2_22() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstEndUpJointRot2_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndUpJointRot2_22() const { return ___firstEndUpJointRot2_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndUpJointRot2_22() { return &___firstEndUpJointRot2_22; }
	inline void set_firstEndUpJointRot2_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndUpJointRot2_22 = value;
	}

	inline static int32_t get_offset_of_firstEndUpJointRot3_23() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstEndUpJointRot3_23)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndUpJointRot3_23() const { return ___firstEndUpJointRot3_23; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndUpJointRot3_23() { return &___firstEndUpJointRot3_23; }
	inline void set_firstEndUpJointRot3_23(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndUpJointRot3_23 = value;
	}

	inline static int32_t get_offset_of_firstEndDownJointRot1_24() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstEndDownJointRot1_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndDownJointRot1_24() const { return ___firstEndDownJointRot1_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndDownJointRot1_24() { return &___firstEndDownJointRot1_24; }
	inline void set_firstEndDownJointRot1_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndDownJointRot1_24 = value;
	}

	inline static int32_t get_offset_of_firstEndDownJointRot2_25() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstEndDownJointRot2_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndDownJointRot2_25() const { return ___firstEndDownJointRot2_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndDownJointRot2_25() { return &___firstEndDownJointRot2_25; }
	inline void set_firstEndDownJointRot2_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndDownJointRot2_25 = value;
	}

	inline static int32_t get_offset_of_firstEndDownJointRot3_26() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstEndDownJointRot3_26)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndDownJointRot3_26() const { return ___firstEndDownJointRot3_26; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndDownJointRot3_26() { return &___firstEndDownJointRot3_26; }
	inline void set_firstEndDownJointRot3_26(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndDownJointRot3_26 = value;
	}

	inline static int32_t get_offset_of_firstLastUpJointRot1_27() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstLastUpJointRot1_27)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastUpJointRot1_27() const { return ___firstLastUpJointRot1_27; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastUpJointRot1_27() { return &___firstLastUpJointRot1_27; }
	inline void set_firstLastUpJointRot1_27(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastUpJointRot1_27 = value;
	}

	inline static int32_t get_offset_of_firstLastUpJointRot2_28() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstLastUpJointRot2_28)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastUpJointRot2_28() const { return ___firstLastUpJointRot2_28; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastUpJointRot2_28() { return &___firstLastUpJointRot2_28; }
	inline void set_firstLastUpJointRot2_28(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastUpJointRot2_28 = value;
	}

	inline static int32_t get_offset_of_firstLastUpJointRot3_29() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstLastUpJointRot3_29)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastUpJointRot3_29() const { return ___firstLastUpJointRot3_29; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastUpJointRot3_29() { return &___firstLastUpJointRot3_29; }
	inline void set_firstLastUpJointRot3_29(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastUpJointRot3_29 = value;
	}

	inline static int32_t get_offset_of_firstLastDownJointRot1_30() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstLastDownJointRot1_30)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastDownJointRot1_30() const { return ___firstLastDownJointRot1_30; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastDownJointRot1_30() { return &___firstLastDownJointRot1_30; }
	inline void set_firstLastDownJointRot1_30(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastDownJointRot1_30 = value;
	}

	inline static int32_t get_offset_of_firstLastDownJointRot2_31() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstLastDownJointRot2_31)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastDownJointRot2_31() const { return ___firstLastDownJointRot2_31; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastDownJointRot2_31() { return &___firstLastDownJointRot2_31; }
	inline void set_firstLastDownJointRot2_31(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastDownJointRot2_31 = value;
	}

	inline static int32_t get_offset_of_firstLastDownJointRot3_32() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___firstLastDownJointRot3_32)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastDownJointRot3_32() const { return ___firstLastDownJointRot3_32; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastDownJointRot3_32() { return &___firstLastDownJointRot3_32; }
	inline void set_firstLastDownJointRot3_32(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastDownJointRot3_32 = value;
	}

	inline static int32_t get_offset_of_lastProjectile_33() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___lastProjectile_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_lastProjectile_33() const { return ___lastProjectile_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_lastProjectile_33() { return &___lastProjectile_33; }
	inline void set_lastProjectile_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___lastProjectile_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastProjectile_33), (void*)value);
	}

	inline static int32_t get_offset_of_stringStartObj_34() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stringStartObj_34)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stringStartObj_34() const { return ___stringStartObj_34; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stringStartObj_34() { return &___stringStartObj_34; }
	inline void set_stringStartObj_34(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stringStartObj_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringStartObj_34), (void*)value);
	}

	inline static int32_t get_offset_of_stringEndObj_35() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stringEndObj_35)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stringEndObj_35() const { return ___stringEndObj_35; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stringEndObj_35() { return &___stringEndObj_35; }
	inline void set_stringEndObj_35(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stringEndObj_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringEndObj_35), (void*)value);
	}

	inline static int32_t get_offset_of_poolHolder_36() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___poolHolder_36)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_poolHolder_36() const { return ___poolHolder_36; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_poolHolder_36() { return &___poolHolder_36; }
	inline void set_poolHolder_36(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___poolHolder_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___poolHolder_36), (void*)value);
	}

	inline static int32_t get_offset_of_stringEndObjTrans_37() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stringEndObjTrans_37)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_stringEndObjTrans_37() const { return ___stringEndObjTrans_37; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_stringEndObjTrans_37() { return &___stringEndObjTrans_37; }
	inline void set_stringEndObjTrans_37(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___stringEndObjTrans_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringEndObjTrans_37), (void*)value);
	}

	inline static int32_t get_offset_of_stringStartObjTrans_38() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stringStartObjTrans_38)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_stringStartObjTrans_38() const { return ___stringStartObjTrans_38; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_stringStartObjTrans_38() { return &___stringStartObjTrans_38; }
	inline void set_stringStartObjTrans_38(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___stringStartObjTrans_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringStartObjTrans_38), (void*)value);
	}

	inline static int32_t get_offset_of_poolHolderTrans_39() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___poolHolderTrans_39)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_poolHolderTrans_39() const { return ___poolHolderTrans_39; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_poolHolderTrans_39() { return &___poolHolderTrans_39; }
	inline void set_poolHolderTrans_39(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___poolHolderTrans_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___poolHolderTrans_39), (void*)value);
	}

	inline static int32_t get_offset_of_lastProjectileScript_40() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___lastProjectileScript_40)); }
	inline StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6 * get_lastProjectileScript_40() const { return ___lastProjectileScript_40; }
	inline StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6 ** get_address_of_lastProjectileScript_40() { return &___lastProjectileScript_40; }
	inline void set_lastProjectileScript_40(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6 * value)
	{
		___lastProjectileScript_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastProjectileScript_40), (void*)value);
	}

	inline static int32_t get_offset_of_projectilePool_41() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___projectilePool_41)); }
	inline Queue_1_t0069840E94B59EC80C3FA49EF6ECA3A067555290 * get_projectilePool_41() const { return ___projectilePool_41; }
	inline Queue_1_t0069840E94B59EC80C3FA49EF6ECA3A067555290 ** get_address_of_projectilePool_41() { return &___projectilePool_41; }
	inline void set_projectilePool_41(Queue_1_t0069840E94B59EC80C3FA49EF6ECA3A067555290 * value)
	{
		___projectilePool_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectilePool_41), (void*)value);
	}

	inline static int32_t get_offset_of_lastProjectileTransform_42() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___lastProjectileTransform_42)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_lastProjectileTransform_42() const { return ___lastProjectileTransform_42; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_lastProjectileTransform_42() { return &___lastProjectileTransform_42; }
	inline void set_lastProjectileTransform_42(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___lastProjectileTransform_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastProjectileTransform_42), (void*)value);
	}

	inline static int32_t get_offset_of_lastProjectileRigidbody_43() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___lastProjectileRigidbody_43)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_lastProjectileRigidbody_43() const { return ___lastProjectileRigidbody_43; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_lastProjectileRigidbody_43() { return &___lastProjectileRigidbody_43; }
	inline void set_lastProjectileRigidbody_43(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___lastProjectileRigidbody_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastProjectileRigidbody_43), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_44() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___audioSource_44)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_44() const { return ___audioSource_44; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_44() { return &___audioSource_44; }
	inline void set_audioSource_44(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_44), (void*)value);
	}

	inline static int32_t get_offset_of_currentStressOnString_45() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___currentStressOnString_45)); }
	inline float get_currentStressOnString_45() const { return ___currentStressOnString_45; }
	inline float* get_address_of_currentStressOnString_45() { return &___currentStressOnString_45; }
	inline void set_currentStressOnString_45(float value)
	{
		___currentStressOnString_45 = value;
	}

	inline static int32_t get_offset_of_pullStateHash_46() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___pullStateHash_46)); }
	inline int32_t get_pullStateHash_46() const { return ___pullStateHash_46; }
	inline int32_t* get_address_of_pullStateHash_46() { return &___pullStateHash_46; }
	inline void set_pullStateHash_46(int32_t value)
	{
		___pullStateHash_46 = value;
	}

	inline static int32_t get_offset_of_pullStateHash2_47() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___pullStateHash2_47)); }
	inline int32_t get_pullStateHash2_47() const { return ___pullStateHash2_47; }
	inline int32_t* get_address_of_pullStateHash2_47() { return &___pullStateHash2_47; }
	inline void set_pullStateHash2_47(int32_t value)
	{
		___pullStateHash2_47 = value;
	}

	inline static int32_t get_offset_of_currentStateInfo_48() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___currentStateInfo_48)); }
	inline AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  get_currentStateInfo_48() const { return ___currentStateInfo_48; }
	inline AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 * get_address_of_currentStateInfo_48() { return &___currentStateInfo_48; }
	inline void set_currentStateInfo_48(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  value)
	{
		___currentStateInfo_48 = value;
	}

	inline static int32_t get_offset_of_projectileDelayTimer_49() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___projectileDelayTimer_49)); }
	inline float get_projectileDelayTimer_49() const { return ___projectileDelayTimer_49; }
	inline float* get_address_of_projectileDelayTimer_49() { return &___projectileDelayTimer_49; }
	inline void set_projectileDelayTimer_49(float value)
	{
		___projectileDelayTimer_49 = value;
	}

	inline static int32_t get_offset_of_pullDelayTimer_50() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___pullDelayTimer_50)); }
	inline float get_pullDelayTimer_50() const { return ___pullDelayTimer_50; }
	inline float* get_address_of_pullDelayTimer_50() { return &___pullDelayTimer_50; }
	inline void set_pullDelayTimer_50(float value)
	{
		___pullDelayTimer_50 = value;
	}

	inline static int32_t get_offset_of_justLeftString_51() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___justLeftString_51)); }
	inline bool get_justLeftString_51() const { return ___justLeftString_51; }
	inline bool* get_address_of_justLeftString_51() { return &___justLeftString_51; }
	inline void set_justLeftString_51(bool value)
	{
		___justLeftString_51 = value;
	}

	inline static int32_t get_offset_of_justPulledString_52() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___justPulledString_52)); }
	inline bool get_justPulledString_52() const { return ___justPulledString_52; }
	inline bool* get_address_of_justPulledString_52() { return &___justPulledString_52; }
	inline void set_justPulledString_52(bool value)
	{
		___justPulledString_52 = value;
	}

	inline static int32_t get_offset_of_justStoppedPulling_53() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___justStoppedPulling_53)); }
	inline bool get_justStoppedPulling_53() const { return ___justStoppedPulling_53; }
	inline bool* get_address_of_justStoppedPulling_53() { return &___justStoppedPulling_53; }
	inline void set_justStoppedPulling_53(bool value)
	{
		___justStoppedPulling_53 = value;
	}

	inline static int32_t get_offset_of_bowUserAnim_56() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bowUserAnim_56)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_bowUserAnim_56() const { return ___bowUserAnim_56; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_bowUserAnim_56() { return &___bowUserAnim_56; }
	inline void set_bowUserAnim_56(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___bowUserAnim_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowUserAnim_56), (void*)value);
	}

	inline static int32_t get_offset_of_bowUserPullHand_57() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bowUserPullHand_57)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowUserPullHand_57() const { return ___bowUserPullHand_57; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowUserPullHand_57() { return &___bowUserPullHand_57; }
	inline void set_bowUserPullHand_57(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowUserPullHand_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowUserPullHand_57), (void*)value);
	}

	inline static int32_t get_offset_of_pullStateLayerIndex_58() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___pullStateLayerIndex_58)); }
	inline int32_t get_pullStateLayerIndex_58() const { return ___pullStateLayerIndex_58; }
	inline int32_t* get_address_of_pullStateLayerIndex_58() { return &___pullStateLayerIndex_58; }
	inline void set_pullStateLayerIndex_58(int32_t value)
	{
		___pullStateLayerIndex_58 = value;
	}

	inline static int32_t get_offset_of_pullStateName_59() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___pullStateName_59)); }
	inline String_t* get_pullStateName_59() const { return ___pullStateName_59; }
	inline String_t** get_address_of_pullStateName_59() { return &___pullStateName_59; }
	inline void set_pullStateName_59(String_t* value)
	{
		___pullStateName_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pullStateName_59), (void*)value);
	}

	inline static int32_t get_offset_of_pullStateName2_60() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___pullStateName2_60)); }
	inline String_t* get_pullStateName2_60() const { return ___pullStateName2_60; }
	inline String_t** get_address_of_pullStateName2_60() { return &___pullStateName2_60; }
	inline void set_pullStateName2_60(String_t* value)
	{
		___pullStateName2_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pullStateName2_60), (void*)value);
	}

	inline static int32_t get_offset_of_pullStateDelay_61() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___pullStateDelay_61)); }
	inline float get_pullStateDelay_61() const { return ___pullStateDelay_61; }
	inline float* get_address_of_pullStateDelay_61() { return &___pullStateDelay_61; }
	inline void set_pullStateDelay_61(float value)
	{
		___pullStateDelay_61 = value;
	}

	inline static int32_t get_offset_of_projectileHoldDelay_62() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___projectileHoldDelay_62)); }
	inline float get_projectileHoldDelay_62() const { return ___projectileHoldDelay_62; }
	inline float* get_address_of_projectileHoldDelay_62() { return &___projectileHoldDelay_62; }
	inline void set_projectileHoldDelay_62(float value)
	{
		___projectileHoldDelay_62 = value;
	}

	inline static int32_t get_offset_of_projectileOnHand_63() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___projectileOnHand_63)); }
	inline bool get_projectileOnHand_63() const { return ___projectileOnHand_63; }
	inline bool* get_address_of_projectileOnHand_63() { return &___projectileOnHand_63; }
	inline void set_projectileOnHand_63(bool value)
	{
		___projectileOnHand_63 = value;
	}

	inline static int32_t get_offset_of_bowUpJoint1_64() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bowUpJoint1_64)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowUpJoint1_64() const { return ___bowUpJoint1_64; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowUpJoint1_64() { return &___bowUpJoint1_64; }
	inline void set_bowUpJoint1_64(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowUpJoint1_64 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowUpJoint1_64), (void*)value);
	}

	inline static int32_t get_offset_of_bowUpJoint2_65() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bowUpJoint2_65)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowUpJoint2_65() const { return ___bowUpJoint2_65; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowUpJoint2_65() { return &___bowUpJoint2_65; }
	inline void set_bowUpJoint2_65(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowUpJoint2_65 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowUpJoint2_65), (void*)value);
	}

	inline static int32_t get_offset_of_bowUpJoint3_66() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bowUpJoint3_66)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowUpJoint3_66() const { return ___bowUpJoint3_66; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowUpJoint3_66() { return &___bowUpJoint3_66; }
	inline void set_bowUpJoint3_66(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowUpJoint3_66 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowUpJoint3_66), (void*)value);
	}

	inline static int32_t get_offset_of_bowDownJoint1_67() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bowDownJoint1_67)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowDownJoint1_67() const { return ___bowDownJoint1_67; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowDownJoint1_67() { return &___bowDownJoint1_67; }
	inline void set_bowDownJoint1_67(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowDownJoint1_67 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowDownJoint1_67), (void*)value);
	}

	inline static int32_t get_offset_of_bowDownJoint2_68() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bowDownJoint2_68)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowDownJoint2_68() const { return ___bowDownJoint2_68; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowDownJoint2_68() { return &___bowDownJoint2_68; }
	inline void set_bowDownJoint2_68(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowDownJoint2_68 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowDownJoint2_68), (void*)value);
	}

	inline static int32_t get_offset_of_bowDownJoint3_69() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bowDownJoint3_69)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowDownJoint3_69() const { return ___bowDownJoint3_69; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowDownJoint3_69() { return &___bowDownJoint3_69; }
	inline void set_bowDownJoint3_69(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowDownJoint3_69 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowDownJoint3_69), (void*)value);
	}

	inline static int32_t get_offset_of_bowStringPoint_70() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bowStringPoint_70)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowStringPoint_70() const { return ___bowStringPoint_70; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowStringPoint_70() { return &___bowStringPoint_70; }
	inline void set_bowStringPoint_70(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowStringPoint_70 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowStringPoint_70), (void*)value);
	}

	inline static int32_t get_offset_of_joint1RotateDirectionAxis_71() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___joint1RotateDirectionAxis_71)); }
	inline int32_t get_joint1RotateDirectionAxis_71() const { return ___joint1RotateDirectionAxis_71; }
	inline int32_t* get_address_of_joint1RotateDirectionAxis_71() { return &___joint1RotateDirectionAxis_71; }
	inline void set_joint1RotateDirectionAxis_71(int32_t value)
	{
		___joint1RotateDirectionAxis_71 = value;
	}

	inline static int32_t get_offset_of_joint2RotateDirectionAxis_72() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___joint2RotateDirectionAxis_72)); }
	inline int32_t get_joint2RotateDirectionAxis_72() const { return ___joint2RotateDirectionAxis_72; }
	inline int32_t* get_address_of_joint2RotateDirectionAxis_72() { return &___joint2RotateDirectionAxis_72; }
	inline void set_joint2RotateDirectionAxis_72(int32_t value)
	{
		___joint2RotateDirectionAxis_72 = value;
	}

	inline static int32_t get_offset_of_joint3RotateDirectionAxis_73() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___joint3RotateDirectionAxis_73)); }
	inline int32_t get_joint3RotateDirectionAxis_73() const { return ___joint3RotateDirectionAxis_73; }
	inline int32_t* get_address_of_joint3RotateDirectionAxis_73() { return &___joint3RotateDirectionAxis_73; }
	inline void set_joint3RotateDirectionAxis_73(int32_t value)
	{
		___joint3RotateDirectionAxis_73 = value;
	}

	inline static int32_t get_offset_of_bendAngleLimitFirstJoints_74() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bendAngleLimitFirstJoints_74)); }
	inline float get_bendAngleLimitFirstJoints_74() const { return ___bendAngleLimitFirstJoints_74; }
	inline float* get_address_of_bendAngleLimitFirstJoints_74() { return &___bendAngleLimitFirstJoints_74; }
	inline void set_bendAngleLimitFirstJoints_74(float value)
	{
		___bendAngleLimitFirstJoints_74 = value;
	}

	inline static int32_t get_offset_of_bendAngleLimitSecondJoints_75() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bendAngleLimitSecondJoints_75)); }
	inline float get_bendAngleLimitSecondJoints_75() const { return ___bendAngleLimitSecondJoints_75; }
	inline float* get_address_of_bendAngleLimitSecondJoints_75() { return &___bendAngleLimitSecondJoints_75; }
	inline void set_bendAngleLimitSecondJoints_75(float value)
	{
		___bendAngleLimitSecondJoints_75 = value;
	}

	inline static int32_t get_offset_of_bendAngleLimitThirdJoints_76() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___bendAngleLimitThirdJoints_76)); }
	inline float get_bendAngleLimitThirdJoints_76() const { return ___bendAngleLimitThirdJoints_76; }
	inline float* get_address_of_bendAngleLimitThirdJoints_76() { return &___bendAngleLimitThirdJoints_76; }
	inline void set_bendAngleLimitThirdJoints_76(float value)
	{
		___bendAngleLimitThirdJoints_76 = value;
	}

	inline static int32_t get_offset_of_inverseBend_77() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___inverseBend_77)); }
	inline bool get_inverseBend_77() const { return ___inverseBend_77; }
	inline bool* get_address_of_inverseBend_77() { return &___inverseBend_77; }
	inline void set_inverseBend_77(bool value)
	{
		___inverseBend_77 = value;
	}

	inline static int32_t get_offset_of_stringMoveDirectionAxis_78() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stringMoveDirectionAxis_78)); }
	inline int32_t get_stringMoveDirectionAxis_78() const { return ___stringMoveDirectionAxis_78; }
	inline int32_t* get_address_of_stringMoveDirectionAxis_78() { return &___stringMoveDirectionAxis_78; }
	inline void set_stringMoveDirectionAxis_78(int32_t value)
	{
		___stringMoveDirectionAxis_78 = value;
	}

	inline static int32_t get_offset_of_stringMoveAmount_79() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stringMoveAmount_79)); }
	inline float get_stringMoveAmount_79() const { return ___stringMoveAmount_79; }
	inline float* get_address_of_stringMoveAmount_79() { return &___stringMoveAmount_79; }
	inline void set_stringMoveAmount_79(float value)
	{
		___stringMoveAmount_79 = value;
	}

	inline static int32_t get_offset_of_stringMoveTime_80() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stringMoveTime_80)); }
	inline float get_stringMoveTime_80() const { return ___stringMoveTime_80; }
	inline float* get_address_of_stringMoveTime_80() { return &___stringMoveTime_80; }
	inline void set_stringMoveTime_80(float value)
	{
		___stringMoveTime_80 = value;
	}

	inline static int32_t get_offset_of_stringRetractTime_81() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stringRetractTime_81)); }
	inline float get_stringRetractTime_81() const { return ___stringRetractTime_81; }
	inline float* get_address_of_stringRetractTime_81() { return &___stringRetractTime_81; }
	inline void set_stringRetractTime_81(float value)
	{
		___stringRetractTime_81 = value;
	}

	inline static int32_t get_offset_of_inversePull_82() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___inversePull_82)); }
	inline bool get_inversePull_82() const { return ___inversePull_82; }
	inline bool* get_address_of_inversePull_82() { return &___inversePull_82; }
	inline void set_inversePull_82(bool value)
	{
		___inversePull_82 = value;
	}

	inline static int32_t get_offset_of_stringMovementChoice_83() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stringMovementChoice_83)); }
	inline int32_t get_stringMovementChoice_83() const { return ___stringMovementChoice_83; }
	inline int32_t* get_address_of_stringMovementChoice_83() { return &___stringMovementChoice_83; }
	inline void set_stringMovementChoice_83(int32_t value)
	{
		___stringMovementChoice_83 = value;
	}

	inline static int32_t get_offset_of_stringRetractChoice_84() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stringRetractChoice_84)); }
	inline int32_t get_stringRetractChoice_84() const { return ___stringRetractChoice_84; }
	inline int32_t* get_address_of_stringRetractChoice_84() { return &___stringRetractChoice_84; }
	inline void set_stringRetractChoice_84(int32_t value)
	{
		___stringRetractChoice_84 = value;
	}

	inline static int32_t get_offset_of_maxStringStrength_85() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___maxStringStrength_85)); }
	inline float get_maxStringStrength_85() const { return ___maxStringStrength_85; }
	inline float* get_address_of_maxStringStrength_85() { return &___maxStringStrength_85; }
	inline void set_maxStringStrength_85(float value)
	{
		___maxStringStrength_85 = value;
	}

	inline static int32_t get_offset_of_projectileAccuracy_86() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___projectileAccuracy_86)); }
	inline int32_t get_projectileAccuracy_86() const { return ___projectileAccuracy_86; }
	inline int32_t* get_address_of_projectileAccuracy_86() { return &___projectileAccuracy_86; }
	inline void set_projectileAccuracy_86(int32_t value)
	{
		___projectileAccuracy_86 = value;
	}

	inline static int32_t get_offset_of_projectile_87() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___projectile_87)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectile_87() const { return ___projectile_87; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectile_87() { return &___projectile_87; }
	inline void set_projectile_87(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectile_87 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectile_87), (void*)value);
	}

	inline static int32_t get_offset_of_projectilePoolSize_88() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___projectilePoolSize_88)); }
	inline int32_t get_projectilePoolSize_88() const { return ___projectilePoolSize_88; }
	inline int32_t* get_address_of_projectilePoolSize_88() { return &___projectilePoolSize_88; }
	inline void set_projectilePoolSize_88(int32_t value)
	{
		___projectilePoolSize_88 = value;
	}

	inline static int32_t get_offset_of_projectileHoldPosOffset_89() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___projectileHoldPosOffset_89)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_projectileHoldPosOffset_89() const { return ___projectileHoldPosOffset_89; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_projectileHoldPosOffset_89() { return &___projectileHoldPosOffset_89; }
	inline void set_projectileHoldPosOffset_89(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___projectileHoldPosOffset_89 = value;
	}

	inline static int32_t get_offset_of_isPosOffsetLocal_90() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___isPosOffsetLocal_90)); }
	inline bool get_isPosOffsetLocal_90() const { return ___isPosOffsetLocal_90; }
	inline bool* get_address_of_isPosOffsetLocal_90() { return &___isPosOffsetLocal_90; }
	inline void set_isPosOffsetLocal_90(bool value)
	{
		___isPosOffsetLocal_90 = value;
	}

	inline static int32_t get_offset_of_projectileHoldRotOffset_91() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___projectileHoldRotOffset_91)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_projectileHoldRotOffset_91() const { return ___projectileHoldRotOffset_91; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_projectileHoldRotOffset_91() { return &___projectileHoldRotOffset_91; }
	inline void set_projectileHoldRotOffset_91(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___projectileHoldRotOffset_91 = value;
	}

	inline static int32_t get_offset_of_projectileForwardAxis_92() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___projectileForwardAxis_92)); }
	inline int32_t get_projectileForwardAxis_92() const { return ___projectileForwardAxis_92; }
	inline int32_t* get_address_of_projectileForwardAxis_92() { return &___projectileForwardAxis_92; }
	inline void set_projectileForwardAxis_92(int32_t value)
	{
		___projectileForwardAxis_92 = value;
	}

	inline static int32_t get_offset_of_soundVolume_93() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___soundVolume_93)); }
	inline float get_soundVolume_93() const { return ___soundVolume_93; }
	inline float* get_address_of_soundVolume_93() { return &___soundVolume_93; }
	inline void set_soundVolume_93(float value)
	{
		___soundVolume_93 = value;
	}

	inline static int32_t get_offset_of_pullSound_94() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___pullSound_94)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_pullSound_94() const { return ___pullSound_94; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_pullSound_94() { return &___pullSound_94; }
	inline void set_pullSound_94(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___pullSound_94 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pullSound_94), (void*)value);
	}

	inline static int32_t get_offset_of_retractSound_95() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___retractSound_95)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_retractSound_95() const { return ___retractSound_95; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_retractSound_95() { return &___retractSound_95; }
	inline void set_retractSound_95(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___retractSound_95 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___retractSound_95), (void*)value);
	}

	inline static int32_t get_offset_of_stressEffectOnSound_96() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___stressEffectOnSound_96)); }
	inline bool get_stressEffectOnSound_96() const { return ___stressEffectOnSound_96; }
	inline bool* get_address_of_stressEffectOnSound_96() { return &___stressEffectOnSound_96; }
	inline void set_stressEffectOnSound_96(bool value)
	{
		___stressEffectOnSound_96 = value;
	}

	inline static int32_t get_offset_of_x_97() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___x_97)); }
	inline float get_x_97() const { return ___x_97; }
	inline float* get_address_of_x_97() { return &___x_97; }
	inline void set_x_97(float value)
	{
		___x_97 = value;
	}

	inline static int32_t get_offset_of_y_98() { return static_cast<int32_t>(offsetof(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79, ___y_98)); }
	inline float get_y_98() const { return ___y_98; }
	inline float* get_address_of_y_98() { return &___y_98; }
	inline void set_y_98(float value)
	{
		___y_98 = value;
	}
};


// StandardizedBowForHandBool
struct  StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single StandardizedBowForHandBool::currentTime
	float ___currentTime_4;
	// System.Single StandardizedBowForHandBool::lerpPercentage
	float ___lerpPercentage_5;
	// System.Single StandardizedBowForHandBool::startingAngleDown
	float ___startingAngleDown_6;
	// System.Single StandardizedBowForHandBool::startingAngleUp
	float ___startingAngleUp_7;
	// System.Single StandardizedBowForHandBool::currentAngleDown
	float ___currentAngleDown_8;
	// System.Single StandardizedBowForHandBool::currentAngleUp
	float ___currentAngleUp_9;
	// UnityEngine.Vector3 StandardizedBowForHandBool::stringStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___stringStartPos_10;
	// UnityEngine.Vector3 StandardizedBowForHandBool::stringEndPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___stringEndPos_11;
	// UnityEngine.Vector3 StandardizedBowForHandBool::stringLastPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___stringLastPos_12;
	// UnityEngine.Vector3 StandardizedBowForHandBool::bowDirPull
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bowDirPull_13;
	// UnityEngine.Vector3 StandardizedBowForHandBool::bowDirRetract
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bowDirRetract_14;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstStartUpJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartUpJointRot1_15;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstStartUpJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartUpJointRot2_16;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstStartUpJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartUpJointRot3_17;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstStartDownJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartDownJointRot1_18;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstStartDownJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartDownJointRot2_19;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstStartDownJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartDownJointRot3_20;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstEndUpJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndUpJointRot1_21;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstEndUpJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndUpJointRot2_22;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstEndUpJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndUpJointRot3_23;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstEndDownJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndDownJointRot1_24;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstEndDownJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndDownJointRot2_25;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstEndDownJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndDownJointRot3_26;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstLastUpJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastUpJointRot1_27;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstLastUpJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastUpJointRot2_28;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstLastUpJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastUpJointRot3_29;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstLastDownJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastDownJointRot1_30;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstLastDownJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastDownJointRot2_31;
	// UnityEngine.Vector3 StandardizedBowForHandBool::firstLastDownJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastDownJointRot3_32;
	// UnityEngine.GameObject StandardizedBowForHandBool::lastProjectile
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lastProjectile_33;
	// UnityEngine.GameObject StandardizedBowForHandBool::stringStartObj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stringStartObj_34;
	// UnityEngine.GameObject StandardizedBowForHandBool::stringEndObj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stringEndObj_35;
	// UnityEngine.GameObject StandardizedBowForHandBool::poolHolder
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___poolHolder_36;
	// UnityEngine.Transform StandardizedBowForHandBool::stringEndObjTrans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___stringEndObjTrans_37;
	// UnityEngine.Transform StandardizedBowForHandBool::stringStartObjTrans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___stringStartObjTrans_38;
	// UnityEngine.Transform StandardizedBowForHandBool::poolHolderTrans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___poolHolderTrans_39;
	// StandardizedProjectileForHandBool StandardizedBowForHandBool::lastProjectileScript
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F * ___lastProjectileScript_40;
	// System.Collections.Generic.Queue`1<UnityEngine.GameObject> StandardizedBowForHandBool::projectilePool
	Queue_1_t0069840E94B59EC80C3FA49EF6ECA3A067555290 * ___projectilePool_41;
	// UnityEngine.Transform StandardizedBowForHandBool::lastProjectileTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___lastProjectileTransform_42;
	// UnityEngine.Rigidbody StandardizedBowForHandBool::lastProjectileRigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___lastProjectileRigidbody_43;
	// UnityEngine.AudioSource StandardizedBowForHandBool::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_44;
	// System.Single StandardizedBowForHandBool::currentStressOnString
	float ___currentStressOnString_45;
	// System.Int32 StandardizedBowForHandBool::pullParamHash
	int32_t ___pullParamHash_46;
	// System.Single StandardizedBowForHandBool::projectileDelayTimer
	float ___projectileDelayTimer_47;
	// System.Single StandardizedBowForHandBool::pullDelayTimer
	float ___pullDelayTimer_48;
	// System.Boolean StandardizedBowForHandBool::justLeftString
	bool ___justLeftString_49;
	// System.Boolean StandardizedBowForHandBool::justPulledString
	bool ___justPulledString_50;
	// System.Boolean StandardizedBowForHandBool::justStoppedPulling
	bool ___justStoppedPulling_51;
	// UnityEngine.Animator StandardizedBowForHandBool::bowUserAnim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___bowUserAnim_54;
	// UnityEngine.Transform StandardizedBowForHandBool::bowUserPullHand
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowUserPullHand_55;
	// System.String StandardizedBowForHandBool::pullParamName
	String_t* ___pullParamName_56;
	// System.Single StandardizedBowForHandBool::pullStateDelay
	float ___pullStateDelay_57;
	// System.Single StandardizedBowForHandBool::projectileHoldDelay
	float ___projectileHoldDelay_58;
	// System.Boolean StandardizedBowForHandBool::projectileOnHand
	bool ___projectileOnHand_59;
	// UnityEngine.Transform StandardizedBowForHandBool::bowUpJoint1
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowUpJoint1_60;
	// UnityEngine.Transform StandardizedBowForHandBool::bowUpJoint2
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowUpJoint2_61;
	// UnityEngine.Transform StandardizedBowForHandBool::bowUpJoint3
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowUpJoint3_62;
	// UnityEngine.Transform StandardizedBowForHandBool::bowDownJoint1
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowDownJoint1_63;
	// UnityEngine.Transform StandardizedBowForHandBool::bowDownJoint2
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowDownJoint2_64;
	// UnityEngine.Transform StandardizedBowForHandBool::bowDownJoint3
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowDownJoint3_65;
	// UnityEngine.Transform StandardizedBowForHandBool::bowStringPoint
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowStringPoint_66;
	// StandardizedBowForHandBool_jointsDirection StandardizedBowForHandBool::joint1RotateDirectionAxis
	int32_t ___joint1RotateDirectionAxis_67;
	// StandardizedBowForHandBool_jointsDirection StandardizedBowForHandBool::joint2RotateDirectionAxis
	int32_t ___joint2RotateDirectionAxis_68;
	// StandardizedBowForHandBool_jointsDirection StandardizedBowForHandBool::joint3RotateDirectionAxis
	int32_t ___joint3RotateDirectionAxis_69;
	// System.Single StandardizedBowForHandBool::bendAngleLimitFirstJoints
	float ___bendAngleLimitFirstJoints_70;
	// System.Single StandardizedBowForHandBool::bendAngleLimitSecondJoints
	float ___bendAngleLimitSecondJoints_71;
	// System.Single StandardizedBowForHandBool::bendAngleLimitThirdJoints
	float ___bendAngleLimitThirdJoints_72;
	// System.Boolean StandardizedBowForHandBool::inverseBend
	bool ___inverseBend_73;
	// StandardizedBowForHandBool_axisDirection StandardizedBowForHandBool::stringMoveDirectionAxis
	int32_t ___stringMoveDirectionAxis_74;
	// System.Single StandardizedBowForHandBool::stringMoveAmount
	float ___stringMoveAmount_75;
	// System.Single StandardizedBowForHandBool::stringMoveTime
	float ___stringMoveTime_76;
	// System.Single StandardizedBowForHandBool::stringRetractTime
	float ___stringRetractTime_77;
	// System.Boolean StandardizedBowForHandBool::inversePull
	bool ___inversePull_78;
	// StandardizedBowForHandBool_stringStressEaseType StandardizedBowForHandBool::stringMovementChoice
	int32_t ___stringMovementChoice_79;
	// StandardizedBowForHandBool_stringRetractEaseType StandardizedBowForHandBool::stringRetractChoice
	int32_t ___stringRetractChoice_80;
	// System.Single StandardizedBowForHandBool::maxStringStrength
	float ___maxStringStrength_81;
	// StandardizedBowForHandBool_accuracyProjectile StandardizedBowForHandBool::projectileAccuracy
	int32_t ___projectileAccuracy_82;
	// UnityEngine.GameObject StandardizedBowForHandBool::projectile
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectile_83;
	// System.Int32 StandardizedBowForHandBool::projectilePoolSize
	int32_t ___projectilePoolSize_84;
	// UnityEngine.Vector3 StandardizedBowForHandBool::projectileHoldPosOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___projectileHoldPosOffset_85;
	// System.Boolean StandardizedBowForHandBool::isPosOffsetLocal
	bool ___isPosOffsetLocal_86;
	// UnityEngine.Vector3 StandardizedBowForHandBool::projectileHoldRotOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___projectileHoldRotOffset_87;
	// StandardizedBowForHandBool_axisDirection StandardizedBowForHandBool::projectileForwardAxis
	int32_t ___projectileForwardAxis_88;
	// System.Single StandardizedBowForHandBool::soundVolume
	float ___soundVolume_89;
	// UnityEngine.AudioClip StandardizedBowForHandBool::pullSound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___pullSound_90;
	// UnityEngine.AudioClip StandardizedBowForHandBool::retractSound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___retractSound_91;
	// System.Boolean StandardizedBowForHandBool::stressEffectOnSound
	bool ___stressEffectOnSound_92;
	// System.Single StandardizedBowForHandBool::x
	float ___x_93;
	// System.Single StandardizedBowForHandBool::y
	float ___y_94;

public:
	inline static int32_t get_offset_of_currentTime_4() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___currentTime_4)); }
	inline float get_currentTime_4() const { return ___currentTime_4; }
	inline float* get_address_of_currentTime_4() { return &___currentTime_4; }
	inline void set_currentTime_4(float value)
	{
		___currentTime_4 = value;
	}

	inline static int32_t get_offset_of_lerpPercentage_5() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___lerpPercentage_5)); }
	inline float get_lerpPercentage_5() const { return ___lerpPercentage_5; }
	inline float* get_address_of_lerpPercentage_5() { return &___lerpPercentage_5; }
	inline void set_lerpPercentage_5(float value)
	{
		___lerpPercentage_5 = value;
	}

	inline static int32_t get_offset_of_startingAngleDown_6() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___startingAngleDown_6)); }
	inline float get_startingAngleDown_6() const { return ___startingAngleDown_6; }
	inline float* get_address_of_startingAngleDown_6() { return &___startingAngleDown_6; }
	inline void set_startingAngleDown_6(float value)
	{
		___startingAngleDown_6 = value;
	}

	inline static int32_t get_offset_of_startingAngleUp_7() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___startingAngleUp_7)); }
	inline float get_startingAngleUp_7() const { return ___startingAngleUp_7; }
	inline float* get_address_of_startingAngleUp_7() { return &___startingAngleUp_7; }
	inline void set_startingAngleUp_7(float value)
	{
		___startingAngleUp_7 = value;
	}

	inline static int32_t get_offset_of_currentAngleDown_8() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___currentAngleDown_8)); }
	inline float get_currentAngleDown_8() const { return ___currentAngleDown_8; }
	inline float* get_address_of_currentAngleDown_8() { return &___currentAngleDown_8; }
	inline void set_currentAngleDown_8(float value)
	{
		___currentAngleDown_8 = value;
	}

	inline static int32_t get_offset_of_currentAngleUp_9() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___currentAngleUp_9)); }
	inline float get_currentAngleUp_9() const { return ___currentAngleUp_9; }
	inline float* get_address_of_currentAngleUp_9() { return &___currentAngleUp_9; }
	inline void set_currentAngleUp_9(float value)
	{
		___currentAngleUp_9 = value;
	}

	inline static int32_t get_offset_of_stringStartPos_10() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stringStartPos_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_stringStartPos_10() const { return ___stringStartPos_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_stringStartPos_10() { return &___stringStartPos_10; }
	inline void set_stringStartPos_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___stringStartPos_10 = value;
	}

	inline static int32_t get_offset_of_stringEndPos_11() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stringEndPos_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_stringEndPos_11() const { return ___stringEndPos_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_stringEndPos_11() { return &___stringEndPos_11; }
	inline void set_stringEndPos_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___stringEndPos_11 = value;
	}

	inline static int32_t get_offset_of_stringLastPos_12() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stringLastPos_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_stringLastPos_12() const { return ___stringLastPos_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_stringLastPos_12() { return &___stringLastPos_12; }
	inline void set_stringLastPos_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___stringLastPos_12 = value;
	}

	inline static int32_t get_offset_of_bowDirPull_13() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bowDirPull_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bowDirPull_13() const { return ___bowDirPull_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bowDirPull_13() { return &___bowDirPull_13; }
	inline void set_bowDirPull_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bowDirPull_13 = value;
	}

	inline static int32_t get_offset_of_bowDirRetract_14() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bowDirRetract_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bowDirRetract_14() const { return ___bowDirRetract_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bowDirRetract_14() { return &___bowDirRetract_14; }
	inline void set_bowDirRetract_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bowDirRetract_14 = value;
	}

	inline static int32_t get_offset_of_firstStartUpJointRot1_15() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstStartUpJointRot1_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartUpJointRot1_15() const { return ___firstStartUpJointRot1_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartUpJointRot1_15() { return &___firstStartUpJointRot1_15; }
	inline void set_firstStartUpJointRot1_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartUpJointRot1_15 = value;
	}

	inline static int32_t get_offset_of_firstStartUpJointRot2_16() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstStartUpJointRot2_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartUpJointRot2_16() const { return ___firstStartUpJointRot2_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartUpJointRot2_16() { return &___firstStartUpJointRot2_16; }
	inline void set_firstStartUpJointRot2_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartUpJointRot2_16 = value;
	}

	inline static int32_t get_offset_of_firstStartUpJointRot3_17() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstStartUpJointRot3_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartUpJointRot3_17() const { return ___firstStartUpJointRot3_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartUpJointRot3_17() { return &___firstStartUpJointRot3_17; }
	inline void set_firstStartUpJointRot3_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartUpJointRot3_17 = value;
	}

	inline static int32_t get_offset_of_firstStartDownJointRot1_18() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstStartDownJointRot1_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartDownJointRot1_18() const { return ___firstStartDownJointRot1_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartDownJointRot1_18() { return &___firstStartDownJointRot1_18; }
	inline void set_firstStartDownJointRot1_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartDownJointRot1_18 = value;
	}

	inline static int32_t get_offset_of_firstStartDownJointRot2_19() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstStartDownJointRot2_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartDownJointRot2_19() const { return ___firstStartDownJointRot2_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartDownJointRot2_19() { return &___firstStartDownJointRot2_19; }
	inline void set_firstStartDownJointRot2_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartDownJointRot2_19 = value;
	}

	inline static int32_t get_offset_of_firstStartDownJointRot3_20() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstStartDownJointRot3_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartDownJointRot3_20() const { return ___firstStartDownJointRot3_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartDownJointRot3_20() { return &___firstStartDownJointRot3_20; }
	inline void set_firstStartDownJointRot3_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartDownJointRot3_20 = value;
	}

	inline static int32_t get_offset_of_firstEndUpJointRot1_21() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstEndUpJointRot1_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndUpJointRot1_21() const { return ___firstEndUpJointRot1_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndUpJointRot1_21() { return &___firstEndUpJointRot1_21; }
	inline void set_firstEndUpJointRot1_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndUpJointRot1_21 = value;
	}

	inline static int32_t get_offset_of_firstEndUpJointRot2_22() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstEndUpJointRot2_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndUpJointRot2_22() const { return ___firstEndUpJointRot2_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndUpJointRot2_22() { return &___firstEndUpJointRot2_22; }
	inline void set_firstEndUpJointRot2_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndUpJointRot2_22 = value;
	}

	inline static int32_t get_offset_of_firstEndUpJointRot3_23() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstEndUpJointRot3_23)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndUpJointRot3_23() const { return ___firstEndUpJointRot3_23; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndUpJointRot3_23() { return &___firstEndUpJointRot3_23; }
	inline void set_firstEndUpJointRot3_23(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndUpJointRot3_23 = value;
	}

	inline static int32_t get_offset_of_firstEndDownJointRot1_24() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstEndDownJointRot1_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndDownJointRot1_24() const { return ___firstEndDownJointRot1_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndDownJointRot1_24() { return &___firstEndDownJointRot1_24; }
	inline void set_firstEndDownJointRot1_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndDownJointRot1_24 = value;
	}

	inline static int32_t get_offset_of_firstEndDownJointRot2_25() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstEndDownJointRot2_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndDownJointRot2_25() const { return ___firstEndDownJointRot2_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndDownJointRot2_25() { return &___firstEndDownJointRot2_25; }
	inline void set_firstEndDownJointRot2_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndDownJointRot2_25 = value;
	}

	inline static int32_t get_offset_of_firstEndDownJointRot3_26() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstEndDownJointRot3_26)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndDownJointRot3_26() const { return ___firstEndDownJointRot3_26; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndDownJointRot3_26() { return &___firstEndDownJointRot3_26; }
	inline void set_firstEndDownJointRot3_26(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndDownJointRot3_26 = value;
	}

	inline static int32_t get_offset_of_firstLastUpJointRot1_27() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstLastUpJointRot1_27)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastUpJointRot1_27() const { return ___firstLastUpJointRot1_27; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastUpJointRot1_27() { return &___firstLastUpJointRot1_27; }
	inline void set_firstLastUpJointRot1_27(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastUpJointRot1_27 = value;
	}

	inline static int32_t get_offset_of_firstLastUpJointRot2_28() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstLastUpJointRot2_28)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastUpJointRot2_28() const { return ___firstLastUpJointRot2_28; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastUpJointRot2_28() { return &___firstLastUpJointRot2_28; }
	inline void set_firstLastUpJointRot2_28(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastUpJointRot2_28 = value;
	}

	inline static int32_t get_offset_of_firstLastUpJointRot3_29() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstLastUpJointRot3_29)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastUpJointRot3_29() const { return ___firstLastUpJointRot3_29; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastUpJointRot3_29() { return &___firstLastUpJointRot3_29; }
	inline void set_firstLastUpJointRot3_29(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastUpJointRot3_29 = value;
	}

	inline static int32_t get_offset_of_firstLastDownJointRot1_30() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstLastDownJointRot1_30)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastDownJointRot1_30() const { return ___firstLastDownJointRot1_30; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastDownJointRot1_30() { return &___firstLastDownJointRot1_30; }
	inline void set_firstLastDownJointRot1_30(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastDownJointRot1_30 = value;
	}

	inline static int32_t get_offset_of_firstLastDownJointRot2_31() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstLastDownJointRot2_31)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastDownJointRot2_31() const { return ___firstLastDownJointRot2_31; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastDownJointRot2_31() { return &___firstLastDownJointRot2_31; }
	inline void set_firstLastDownJointRot2_31(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastDownJointRot2_31 = value;
	}

	inline static int32_t get_offset_of_firstLastDownJointRot3_32() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___firstLastDownJointRot3_32)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastDownJointRot3_32() const { return ___firstLastDownJointRot3_32; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastDownJointRot3_32() { return &___firstLastDownJointRot3_32; }
	inline void set_firstLastDownJointRot3_32(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastDownJointRot3_32 = value;
	}

	inline static int32_t get_offset_of_lastProjectile_33() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___lastProjectile_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_lastProjectile_33() const { return ___lastProjectile_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_lastProjectile_33() { return &___lastProjectile_33; }
	inline void set_lastProjectile_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___lastProjectile_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastProjectile_33), (void*)value);
	}

	inline static int32_t get_offset_of_stringStartObj_34() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stringStartObj_34)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stringStartObj_34() const { return ___stringStartObj_34; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stringStartObj_34() { return &___stringStartObj_34; }
	inline void set_stringStartObj_34(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stringStartObj_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringStartObj_34), (void*)value);
	}

	inline static int32_t get_offset_of_stringEndObj_35() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stringEndObj_35)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stringEndObj_35() const { return ___stringEndObj_35; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stringEndObj_35() { return &___stringEndObj_35; }
	inline void set_stringEndObj_35(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stringEndObj_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringEndObj_35), (void*)value);
	}

	inline static int32_t get_offset_of_poolHolder_36() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___poolHolder_36)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_poolHolder_36() const { return ___poolHolder_36; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_poolHolder_36() { return &___poolHolder_36; }
	inline void set_poolHolder_36(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___poolHolder_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___poolHolder_36), (void*)value);
	}

	inline static int32_t get_offset_of_stringEndObjTrans_37() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stringEndObjTrans_37)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_stringEndObjTrans_37() const { return ___stringEndObjTrans_37; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_stringEndObjTrans_37() { return &___stringEndObjTrans_37; }
	inline void set_stringEndObjTrans_37(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___stringEndObjTrans_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringEndObjTrans_37), (void*)value);
	}

	inline static int32_t get_offset_of_stringStartObjTrans_38() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stringStartObjTrans_38)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_stringStartObjTrans_38() const { return ___stringStartObjTrans_38; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_stringStartObjTrans_38() { return &___stringStartObjTrans_38; }
	inline void set_stringStartObjTrans_38(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___stringStartObjTrans_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringStartObjTrans_38), (void*)value);
	}

	inline static int32_t get_offset_of_poolHolderTrans_39() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___poolHolderTrans_39)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_poolHolderTrans_39() const { return ___poolHolderTrans_39; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_poolHolderTrans_39() { return &___poolHolderTrans_39; }
	inline void set_poolHolderTrans_39(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___poolHolderTrans_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___poolHolderTrans_39), (void*)value);
	}

	inline static int32_t get_offset_of_lastProjectileScript_40() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___lastProjectileScript_40)); }
	inline StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F * get_lastProjectileScript_40() const { return ___lastProjectileScript_40; }
	inline StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F ** get_address_of_lastProjectileScript_40() { return &___lastProjectileScript_40; }
	inline void set_lastProjectileScript_40(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F * value)
	{
		___lastProjectileScript_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastProjectileScript_40), (void*)value);
	}

	inline static int32_t get_offset_of_projectilePool_41() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___projectilePool_41)); }
	inline Queue_1_t0069840E94B59EC80C3FA49EF6ECA3A067555290 * get_projectilePool_41() const { return ___projectilePool_41; }
	inline Queue_1_t0069840E94B59EC80C3FA49EF6ECA3A067555290 ** get_address_of_projectilePool_41() { return &___projectilePool_41; }
	inline void set_projectilePool_41(Queue_1_t0069840E94B59EC80C3FA49EF6ECA3A067555290 * value)
	{
		___projectilePool_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectilePool_41), (void*)value);
	}

	inline static int32_t get_offset_of_lastProjectileTransform_42() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___lastProjectileTransform_42)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_lastProjectileTransform_42() const { return ___lastProjectileTransform_42; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_lastProjectileTransform_42() { return &___lastProjectileTransform_42; }
	inline void set_lastProjectileTransform_42(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___lastProjectileTransform_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastProjectileTransform_42), (void*)value);
	}

	inline static int32_t get_offset_of_lastProjectileRigidbody_43() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___lastProjectileRigidbody_43)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_lastProjectileRigidbody_43() const { return ___lastProjectileRigidbody_43; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_lastProjectileRigidbody_43() { return &___lastProjectileRigidbody_43; }
	inline void set_lastProjectileRigidbody_43(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___lastProjectileRigidbody_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastProjectileRigidbody_43), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_44() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___audioSource_44)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_44() const { return ___audioSource_44; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_44() { return &___audioSource_44; }
	inline void set_audioSource_44(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_44), (void*)value);
	}

	inline static int32_t get_offset_of_currentStressOnString_45() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___currentStressOnString_45)); }
	inline float get_currentStressOnString_45() const { return ___currentStressOnString_45; }
	inline float* get_address_of_currentStressOnString_45() { return &___currentStressOnString_45; }
	inline void set_currentStressOnString_45(float value)
	{
		___currentStressOnString_45 = value;
	}

	inline static int32_t get_offset_of_pullParamHash_46() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___pullParamHash_46)); }
	inline int32_t get_pullParamHash_46() const { return ___pullParamHash_46; }
	inline int32_t* get_address_of_pullParamHash_46() { return &___pullParamHash_46; }
	inline void set_pullParamHash_46(int32_t value)
	{
		___pullParamHash_46 = value;
	}

	inline static int32_t get_offset_of_projectileDelayTimer_47() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___projectileDelayTimer_47)); }
	inline float get_projectileDelayTimer_47() const { return ___projectileDelayTimer_47; }
	inline float* get_address_of_projectileDelayTimer_47() { return &___projectileDelayTimer_47; }
	inline void set_projectileDelayTimer_47(float value)
	{
		___projectileDelayTimer_47 = value;
	}

	inline static int32_t get_offset_of_pullDelayTimer_48() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___pullDelayTimer_48)); }
	inline float get_pullDelayTimer_48() const { return ___pullDelayTimer_48; }
	inline float* get_address_of_pullDelayTimer_48() { return &___pullDelayTimer_48; }
	inline void set_pullDelayTimer_48(float value)
	{
		___pullDelayTimer_48 = value;
	}

	inline static int32_t get_offset_of_justLeftString_49() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___justLeftString_49)); }
	inline bool get_justLeftString_49() const { return ___justLeftString_49; }
	inline bool* get_address_of_justLeftString_49() { return &___justLeftString_49; }
	inline void set_justLeftString_49(bool value)
	{
		___justLeftString_49 = value;
	}

	inline static int32_t get_offset_of_justPulledString_50() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___justPulledString_50)); }
	inline bool get_justPulledString_50() const { return ___justPulledString_50; }
	inline bool* get_address_of_justPulledString_50() { return &___justPulledString_50; }
	inline void set_justPulledString_50(bool value)
	{
		___justPulledString_50 = value;
	}

	inline static int32_t get_offset_of_justStoppedPulling_51() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___justStoppedPulling_51)); }
	inline bool get_justStoppedPulling_51() const { return ___justStoppedPulling_51; }
	inline bool* get_address_of_justStoppedPulling_51() { return &___justStoppedPulling_51; }
	inline void set_justStoppedPulling_51(bool value)
	{
		___justStoppedPulling_51 = value;
	}

	inline static int32_t get_offset_of_bowUserAnim_54() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bowUserAnim_54)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_bowUserAnim_54() const { return ___bowUserAnim_54; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_bowUserAnim_54() { return &___bowUserAnim_54; }
	inline void set_bowUserAnim_54(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___bowUserAnim_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowUserAnim_54), (void*)value);
	}

	inline static int32_t get_offset_of_bowUserPullHand_55() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bowUserPullHand_55)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowUserPullHand_55() const { return ___bowUserPullHand_55; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowUserPullHand_55() { return &___bowUserPullHand_55; }
	inline void set_bowUserPullHand_55(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowUserPullHand_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowUserPullHand_55), (void*)value);
	}

	inline static int32_t get_offset_of_pullParamName_56() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___pullParamName_56)); }
	inline String_t* get_pullParamName_56() const { return ___pullParamName_56; }
	inline String_t** get_address_of_pullParamName_56() { return &___pullParamName_56; }
	inline void set_pullParamName_56(String_t* value)
	{
		___pullParamName_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pullParamName_56), (void*)value);
	}

	inline static int32_t get_offset_of_pullStateDelay_57() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___pullStateDelay_57)); }
	inline float get_pullStateDelay_57() const { return ___pullStateDelay_57; }
	inline float* get_address_of_pullStateDelay_57() { return &___pullStateDelay_57; }
	inline void set_pullStateDelay_57(float value)
	{
		___pullStateDelay_57 = value;
	}

	inline static int32_t get_offset_of_projectileHoldDelay_58() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___projectileHoldDelay_58)); }
	inline float get_projectileHoldDelay_58() const { return ___projectileHoldDelay_58; }
	inline float* get_address_of_projectileHoldDelay_58() { return &___projectileHoldDelay_58; }
	inline void set_projectileHoldDelay_58(float value)
	{
		___projectileHoldDelay_58 = value;
	}

	inline static int32_t get_offset_of_projectileOnHand_59() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___projectileOnHand_59)); }
	inline bool get_projectileOnHand_59() const { return ___projectileOnHand_59; }
	inline bool* get_address_of_projectileOnHand_59() { return &___projectileOnHand_59; }
	inline void set_projectileOnHand_59(bool value)
	{
		___projectileOnHand_59 = value;
	}

	inline static int32_t get_offset_of_bowUpJoint1_60() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bowUpJoint1_60)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowUpJoint1_60() const { return ___bowUpJoint1_60; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowUpJoint1_60() { return &___bowUpJoint1_60; }
	inline void set_bowUpJoint1_60(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowUpJoint1_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowUpJoint1_60), (void*)value);
	}

	inline static int32_t get_offset_of_bowUpJoint2_61() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bowUpJoint2_61)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowUpJoint2_61() const { return ___bowUpJoint2_61; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowUpJoint2_61() { return &___bowUpJoint2_61; }
	inline void set_bowUpJoint2_61(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowUpJoint2_61 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowUpJoint2_61), (void*)value);
	}

	inline static int32_t get_offset_of_bowUpJoint3_62() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bowUpJoint3_62)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowUpJoint3_62() const { return ___bowUpJoint3_62; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowUpJoint3_62() { return &___bowUpJoint3_62; }
	inline void set_bowUpJoint3_62(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowUpJoint3_62 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowUpJoint3_62), (void*)value);
	}

	inline static int32_t get_offset_of_bowDownJoint1_63() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bowDownJoint1_63)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowDownJoint1_63() const { return ___bowDownJoint1_63; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowDownJoint1_63() { return &___bowDownJoint1_63; }
	inline void set_bowDownJoint1_63(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowDownJoint1_63 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowDownJoint1_63), (void*)value);
	}

	inline static int32_t get_offset_of_bowDownJoint2_64() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bowDownJoint2_64)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowDownJoint2_64() const { return ___bowDownJoint2_64; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowDownJoint2_64() { return &___bowDownJoint2_64; }
	inline void set_bowDownJoint2_64(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowDownJoint2_64 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowDownJoint2_64), (void*)value);
	}

	inline static int32_t get_offset_of_bowDownJoint3_65() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bowDownJoint3_65)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowDownJoint3_65() const { return ___bowDownJoint3_65; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowDownJoint3_65() { return &___bowDownJoint3_65; }
	inline void set_bowDownJoint3_65(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowDownJoint3_65 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowDownJoint3_65), (void*)value);
	}

	inline static int32_t get_offset_of_bowStringPoint_66() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bowStringPoint_66)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowStringPoint_66() const { return ___bowStringPoint_66; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowStringPoint_66() { return &___bowStringPoint_66; }
	inline void set_bowStringPoint_66(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowStringPoint_66 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowStringPoint_66), (void*)value);
	}

	inline static int32_t get_offset_of_joint1RotateDirectionAxis_67() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___joint1RotateDirectionAxis_67)); }
	inline int32_t get_joint1RotateDirectionAxis_67() const { return ___joint1RotateDirectionAxis_67; }
	inline int32_t* get_address_of_joint1RotateDirectionAxis_67() { return &___joint1RotateDirectionAxis_67; }
	inline void set_joint1RotateDirectionAxis_67(int32_t value)
	{
		___joint1RotateDirectionAxis_67 = value;
	}

	inline static int32_t get_offset_of_joint2RotateDirectionAxis_68() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___joint2RotateDirectionAxis_68)); }
	inline int32_t get_joint2RotateDirectionAxis_68() const { return ___joint2RotateDirectionAxis_68; }
	inline int32_t* get_address_of_joint2RotateDirectionAxis_68() { return &___joint2RotateDirectionAxis_68; }
	inline void set_joint2RotateDirectionAxis_68(int32_t value)
	{
		___joint2RotateDirectionAxis_68 = value;
	}

	inline static int32_t get_offset_of_joint3RotateDirectionAxis_69() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___joint3RotateDirectionAxis_69)); }
	inline int32_t get_joint3RotateDirectionAxis_69() const { return ___joint3RotateDirectionAxis_69; }
	inline int32_t* get_address_of_joint3RotateDirectionAxis_69() { return &___joint3RotateDirectionAxis_69; }
	inline void set_joint3RotateDirectionAxis_69(int32_t value)
	{
		___joint3RotateDirectionAxis_69 = value;
	}

	inline static int32_t get_offset_of_bendAngleLimitFirstJoints_70() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bendAngleLimitFirstJoints_70)); }
	inline float get_bendAngleLimitFirstJoints_70() const { return ___bendAngleLimitFirstJoints_70; }
	inline float* get_address_of_bendAngleLimitFirstJoints_70() { return &___bendAngleLimitFirstJoints_70; }
	inline void set_bendAngleLimitFirstJoints_70(float value)
	{
		___bendAngleLimitFirstJoints_70 = value;
	}

	inline static int32_t get_offset_of_bendAngleLimitSecondJoints_71() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bendAngleLimitSecondJoints_71)); }
	inline float get_bendAngleLimitSecondJoints_71() const { return ___bendAngleLimitSecondJoints_71; }
	inline float* get_address_of_bendAngleLimitSecondJoints_71() { return &___bendAngleLimitSecondJoints_71; }
	inline void set_bendAngleLimitSecondJoints_71(float value)
	{
		___bendAngleLimitSecondJoints_71 = value;
	}

	inline static int32_t get_offset_of_bendAngleLimitThirdJoints_72() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___bendAngleLimitThirdJoints_72)); }
	inline float get_bendAngleLimitThirdJoints_72() const { return ___bendAngleLimitThirdJoints_72; }
	inline float* get_address_of_bendAngleLimitThirdJoints_72() { return &___bendAngleLimitThirdJoints_72; }
	inline void set_bendAngleLimitThirdJoints_72(float value)
	{
		___bendAngleLimitThirdJoints_72 = value;
	}

	inline static int32_t get_offset_of_inverseBend_73() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___inverseBend_73)); }
	inline bool get_inverseBend_73() const { return ___inverseBend_73; }
	inline bool* get_address_of_inverseBend_73() { return &___inverseBend_73; }
	inline void set_inverseBend_73(bool value)
	{
		___inverseBend_73 = value;
	}

	inline static int32_t get_offset_of_stringMoveDirectionAxis_74() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stringMoveDirectionAxis_74)); }
	inline int32_t get_stringMoveDirectionAxis_74() const { return ___stringMoveDirectionAxis_74; }
	inline int32_t* get_address_of_stringMoveDirectionAxis_74() { return &___stringMoveDirectionAxis_74; }
	inline void set_stringMoveDirectionAxis_74(int32_t value)
	{
		___stringMoveDirectionAxis_74 = value;
	}

	inline static int32_t get_offset_of_stringMoveAmount_75() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stringMoveAmount_75)); }
	inline float get_stringMoveAmount_75() const { return ___stringMoveAmount_75; }
	inline float* get_address_of_stringMoveAmount_75() { return &___stringMoveAmount_75; }
	inline void set_stringMoveAmount_75(float value)
	{
		___stringMoveAmount_75 = value;
	}

	inline static int32_t get_offset_of_stringMoveTime_76() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stringMoveTime_76)); }
	inline float get_stringMoveTime_76() const { return ___stringMoveTime_76; }
	inline float* get_address_of_stringMoveTime_76() { return &___stringMoveTime_76; }
	inline void set_stringMoveTime_76(float value)
	{
		___stringMoveTime_76 = value;
	}

	inline static int32_t get_offset_of_stringRetractTime_77() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stringRetractTime_77)); }
	inline float get_stringRetractTime_77() const { return ___stringRetractTime_77; }
	inline float* get_address_of_stringRetractTime_77() { return &___stringRetractTime_77; }
	inline void set_stringRetractTime_77(float value)
	{
		___stringRetractTime_77 = value;
	}

	inline static int32_t get_offset_of_inversePull_78() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___inversePull_78)); }
	inline bool get_inversePull_78() const { return ___inversePull_78; }
	inline bool* get_address_of_inversePull_78() { return &___inversePull_78; }
	inline void set_inversePull_78(bool value)
	{
		___inversePull_78 = value;
	}

	inline static int32_t get_offset_of_stringMovementChoice_79() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stringMovementChoice_79)); }
	inline int32_t get_stringMovementChoice_79() const { return ___stringMovementChoice_79; }
	inline int32_t* get_address_of_stringMovementChoice_79() { return &___stringMovementChoice_79; }
	inline void set_stringMovementChoice_79(int32_t value)
	{
		___stringMovementChoice_79 = value;
	}

	inline static int32_t get_offset_of_stringRetractChoice_80() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stringRetractChoice_80)); }
	inline int32_t get_stringRetractChoice_80() const { return ___stringRetractChoice_80; }
	inline int32_t* get_address_of_stringRetractChoice_80() { return &___stringRetractChoice_80; }
	inline void set_stringRetractChoice_80(int32_t value)
	{
		___stringRetractChoice_80 = value;
	}

	inline static int32_t get_offset_of_maxStringStrength_81() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___maxStringStrength_81)); }
	inline float get_maxStringStrength_81() const { return ___maxStringStrength_81; }
	inline float* get_address_of_maxStringStrength_81() { return &___maxStringStrength_81; }
	inline void set_maxStringStrength_81(float value)
	{
		___maxStringStrength_81 = value;
	}

	inline static int32_t get_offset_of_projectileAccuracy_82() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___projectileAccuracy_82)); }
	inline int32_t get_projectileAccuracy_82() const { return ___projectileAccuracy_82; }
	inline int32_t* get_address_of_projectileAccuracy_82() { return &___projectileAccuracy_82; }
	inline void set_projectileAccuracy_82(int32_t value)
	{
		___projectileAccuracy_82 = value;
	}

	inline static int32_t get_offset_of_projectile_83() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___projectile_83)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectile_83() const { return ___projectile_83; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectile_83() { return &___projectile_83; }
	inline void set_projectile_83(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectile_83 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectile_83), (void*)value);
	}

	inline static int32_t get_offset_of_projectilePoolSize_84() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___projectilePoolSize_84)); }
	inline int32_t get_projectilePoolSize_84() const { return ___projectilePoolSize_84; }
	inline int32_t* get_address_of_projectilePoolSize_84() { return &___projectilePoolSize_84; }
	inline void set_projectilePoolSize_84(int32_t value)
	{
		___projectilePoolSize_84 = value;
	}

	inline static int32_t get_offset_of_projectileHoldPosOffset_85() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___projectileHoldPosOffset_85)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_projectileHoldPosOffset_85() const { return ___projectileHoldPosOffset_85; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_projectileHoldPosOffset_85() { return &___projectileHoldPosOffset_85; }
	inline void set_projectileHoldPosOffset_85(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___projectileHoldPosOffset_85 = value;
	}

	inline static int32_t get_offset_of_isPosOffsetLocal_86() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___isPosOffsetLocal_86)); }
	inline bool get_isPosOffsetLocal_86() const { return ___isPosOffsetLocal_86; }
	inline bool* get_address_of_isPosOffsetLocal_86() { return &___isPosOffsetLocal_86; }
	inline void set_isPosOffsetLocal_86(bool value)
	{
		___isPosOffsetLocal_86 = value;
	}

	inline static int32_t get_offset_of_projectileHoldRotOffset_87() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___projectileHoldRotOffset_87)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_projectileHoldRotOffset_87() const { return ___projectileHoldRotOffset_87; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_projectileHoldRotOffset_87() { return &___projectileHoldRotOffset_87; }
	inline void set_projectileHoldRotOffset_87(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___projectileHoldRotOffset_87 = value;
	}

	inline static int32_t get_offset_of_projectileForwardAxis_88() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___projectileForwardAxis_88)); }
	inline int32_t get_projectileForwardAxis_88() const { return ___projectileForwardAxis_88; }
	inline int32_t* get_address_of_projectileForwardAxis_88() { return &___projectileForwardAxis_88; }
	inline void set_projectileForwardAxis_88(int32_t value)
	{
		___projectileForwardAxis_88 = value;
	}

	inline static int32_t get_offset_of_soundVolume_89() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___soundVolume_89)); }
	inline float get_soundVolume_89() const { return ___soundVolume_89; }
	inline float* get_address_of_soundVolume_89() { return &___soundVolume_89; }
	inline void set_soundVolume_89(float value)
	{
		___soundVolume_89 = value;
	}

	inline static int32_t get_offset_of_pullSound_90() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___pullSound_90)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_pullSound_90() const { return ___pullSound_90; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_pullSound_90() { return &___pullSound_90; }
	inline void set_pullSound_90(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___pullSound_90 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pullSound_90), (void*)value);
	}

	inline static int32_t get_offset_of_retractSound_91() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___retractSound_91)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_retractSound_91() const { return ___retractSound_91; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_retractSound_91() { return &___retractSound_91; }
	inline void set_retractSound_91(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___retractSound_91 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___retractSound_91), (void*)value);
	}

	inline static int32_t get_offset_of_stressEffectOnSound_92() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___stressEffectOnSound_92)); }
	inline bool get_stressEffectOnSound_92() const { return ___stressEffectOnSound_92; }
	inline bool* get_address_of_stressEffectOnSound_92() { return &___stressEffectOnSound_92; }
	inline void set_stressEffectOnSound_92(bool value)
	{
		___stressEffectOnSound_92 = value;
	}

	inline static int32_t get_offset_of_x_93() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___x_93)); }
	inline float get_x_93() const { return ___x_93; }
	inline float* get_address_of_x_93() { return &___x_93; }
	inline void set_x_93(float value)
	{
		___x_93 = value;
	}

	inline static int32_t get_offset_of_y_94() { return static_cast<int32_t>(offsetof(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB, ___y_94)); }
	inline float get_y_94() const { return ___y_94; }
	inline float* get_address_of_y_94() { return &___y_94; }
	inline void set_y_94(float value)
	{
		___y_94 = value;
	}
};


// StandardizedProjectileForHand
struct  StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Rigidbody StandardizedProjectileForHand::rigid
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___rigid_4;
	// StandardizedBowForHand StandardizedProjectileForHand::bowScript
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79 * ___bowScript_5;
	// UnityEngine.Transform StandardizedProjectileForHand::quiver
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___quiver_6;
	// UnityEngine.BoxCollider StandardizedProjectileForHand::boxCollider
	BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * ___boxCollider_7;
	// UnityEngine.AudioSource StandardizedProjectileForHand::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_8;
	// System.Single StandardizedProjectileForHand::currentTime
	float ___currentTime_9;
	// System.Boolean StandardizedProjectileForHand::collisionHappened
	bool ___collisionHappened_10;
	// System.Single StandardizedProjectileForHand::timeLimitToDePool
	float ___timeLimitToDePool_11;
	// System.Single StandardizedProjectileForHand::effectOfVelocityOnParticleEmission
	float ___effectOfVelocityOnParticleEmission_12;
	// UnityEngine.GameObject StandardizedProjectileForHand::projectileHitParticleFlesh
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectileHitParticleFlesh_13;
	// UnityEngine.GameObject StandardizedProjectileForHand::projectileHitParticleWood
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectileHitParticleWood_14;
	// UnityEngine.GameObject StandardizedProjectileForHand::projectileHitParticleStone
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectileHitParticleStone_15;
	// UnityEngine.GameObject StandardizedProjectileForHand::projectileHitParticleMetal
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectileHitParticleMetal_16;
	// UnityEngine.AudioClip StandardizedProjectileForHand::hitSoundFlesh
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___hitSoundFlesh_17;
	// UnityEngine.AudioClip StandardizedProjectileForHand::hitSoundWood
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___hitSoundWood_18;
	// UnityEngine.AudioClip StandardizedProjectileForHand::hitSoundStone
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___hitSoundStone_19;
	// UnityEngine.AudioClip StandardizedProjectileForHand::hitSoundMetal
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___hitSoundMetal_20;
	// System.String StandardizedProjectileForHand::fleshDetectionTagToHash
	String_t* ___fleshDetectionTagToHash_21;
	// System.String StandardizedProjectileForHand::woodDetectionTagToHash
	String_t* ___woodDetectionTagToHash_22;
	// System.String StandardizedProjectileForHand::stoneDetectionTagToHash
	String_t* ___stoneDetectionTagToHash_23;
	// System.String StandardizedProjectileForHand::metalDetectionTagToHash
	String_t* ___metalDetectionTagToHash_24;
	// UnityEngine.ParticleSystem StandardizedProjectileForHand::fleshHitPS
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___fleshHitPS_31;
	// UnityEngine.ParticleSystem StandardizedProjectileForHand::woodHitPS
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___woodHitPS_32;
	// UnityEngine.ParticleSystem StandardizedProjectileForHand::stoneHitPS
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___stoneHitPS_33;
	// UnityEngine.ParticleSystem StandardizedProjectileForHand::metalHitPS
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___metalHitPS_34;
	// UnityEngine.GameObject StandardizedProjectileForHand::fleshParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___fleshParticleInstance_35;
	// UnityEngine.GameObject StandardizedProjectileForHand::woodParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___woodParticleInstance_36;
	// UnityEngine.GameObject StandardizedProjectileForHand::stoneParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stoneParticleInstance_37;
	// UnityEngine.GameObject StandardizedProjectileForHand::metalParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___metalParticleInstance_38;
	// UnityEngine.GameObject StandardizedProjectileForHand::currentParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___currentParticleInstance_39;
	// UnityEngine.Transform StandardizedProjectileForHand::fleshParticleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___fleshParticleTransform_40;
	// UnityEngine.Transform StandardizedProjectileForHand::woodParticleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___woodParticleTransform_41;
	// UnityEngine.Transform StandardizedProjectileForHand::stoneParticleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___stoneParticleTransform_42;
	// UnityEngine.Transform StandardizedProjectileForHand::metalParticleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___metalParticleTransform_43;
	// UnityEngine.ParticleSystem_Burst StandardizedProjectileForHand::burstControl
	Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB  ___burstControl_44;

public:
	inline static int32_t get_offset_of_rigid_4() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___rigid_4)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_rigid_4() const { return ___rigid_4; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_rigid_4() { return &___rigid_4; }
	inline void set_rigid_4(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___rigid_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rigid_4), (void*)value);
	}

	inline static int32_t get_offset_of_bowScript_5() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___bowScript_5)); }
	inline StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79 * get_bowScript_5() const { return ___bowScript_5; }
	inline StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79 ** get_address_of_bowScript_5() { return &___bowScript_5; }
	inline void set_bowScript_5(StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79 * value)
	{
		___bowScript_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowScript_5), (void*)value);
	}

	inline static int32_t get_offset_of_quiver_6() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___quiver_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_quiver_6() const { return ___quiver_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_quiver_6() { return &___quiver_6; }
	inline void set_quiver_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___quiver_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___quiver_6), (void*)value);
	}

	inline static int32_t get_offset_of_boxCollider_7() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___boxCollider_7)); }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * get_boxCollider_7() const { return ___boxCollider_7; }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA ** get_address_of_boxCollider_7() { return &___boxCollider_7; }
	inline void set_boxCollider_7(BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * value)
	{
		___boxCollider_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___boxCollider_7), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_8() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___audioSource_8)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_8() const { return ___audioSource_8; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_8() { return &___audioSource_8; }
	inline void set_audioSource_8(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_8), (void*)value);
	}

	inline static int32_t get_offset_of_currentTime_9() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___currentTime_9)); }
	inline float get_currentTime_9() const { return ___currentTime_9; }
	inline float* get_address_of_currentTime_9() { return &___currentTime_9; }
	inline void set_currentTime_9(float value)
	{
		___currentTime_9 = value;
	}

	inline static int32_t get_offset_of_collisionHappened_10() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___collisionHappened_10)); }
	inline bool get_collisionHappened_10() const { return ___collisionHappened_10; }
	inline bool* get_address_of_collisionHappened_10() { return &___collisionHappened_10; }
	inline void set_collisionHappened_10(bool value)
	{
		___collisionHappened_10 = value;
	}

	inline static int32_t get_offset_of_timeLimitToDePool_11() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___timeLimitToDePool_11)); }
	inline float get_timeLimitToDePool_11() const { return ___timeLimitToDePool_11; }
	inline float* get_address_of_timeLimitToDePool_11() { return &___timeLimitToDePool_11; }
	inline void set_timeLimitToDePool_11(float value)
	{
		___timeLimitToDePool_11 = value;
	}

	inline static int32_t get_offset_of_effectOfVelocityOnParticleEmission_12() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___effectOfVelocityOnParticleEmission_12)); }
	inline float get_effectOfVelocityOnParticleEmission_12() const { return ___effectOfVelocityOnParticleEmission_12; }
	inline float* get_address_of_effectOfVelocityOnParticleEmission_12() { return &___effectOfVelocityOnParticleEmission_12; }
	inline void set_effectOfVelocityOnParticleEmission_12(float value)
	{
		___effectOfVelocityOnParticleEmission_12 = value;
	}

	inline static int32_t get_offset_of_projectileHitParticleFlesh_13() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___projectileHitParticleFlesh_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectileHitParticleFlesh_13() const { return ___projectileHitParticleFlesh_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectileHitParticleFlesh_13() { return &___projectileHitParticleFlesh_13; }
	inline void set_projectileHitParticleFlesh_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectileHitParticleFlesh_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectileHitParticleFlesh_13), (void*)value);
	}

	inline static int32_t get_offset_of_projectileHitParticleWood_14() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___projectileHitParticleWood_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectileHitParticleWood_14() const { return ___projectileHitParticleWood_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectileHitParticleWood_14() { return &___projectileHitParticleWood_14; }
	inline void set_projectileHitParticleWood_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectileHitParticleWood_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectileHitParticleWood_14), (void*)value);
	}

	inline static int32_t get_offset_of_projectileHitParticleStone_15() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___projectileHitParticleStone_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectileHitParticleStone_15() const { return ___projectileHitParticleStone_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectileHitParticleStone_15() { return &___projectileHitParticleStone_15; }
	inline void set_projectileHitParticleStone_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectileHitParticleStone_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectileHitParticleStone_15), (void*)value);
	}

	inline static int32_t get_offset_of_projectileHitParticleMetal_16() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___projectileHitParticleMetal_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectileHitParticleMetal_16() const { return ___projectileHitParticleMetal_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectileHitParticleMetal_16() { return &___projectileHitParticleMetal_16; }
	inline void set_projectileHitParticleMetal_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectileHitParticleMetal_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectileHitParticleMetal_16), (void*)value);
	}

	inline static int32_t get_offset_of_hitSoundFlesh_17() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___hitSoundFlesh_17)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_hitSoundFlesh_17() const { return ___hitSoundFlesh_17; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_hitSoundFlesh_17() { return &___hitSoundFlesh_17; }
	inline void set_hitSoundFlesh_17(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___hitSoundFlesh_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitSoundFlesh_17), (void*)value);
	}

	inline static int32_t get_offset_of_hitSoundWood_18() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___hitSoundWood_18)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_hitSoundWood_18() const { return ___hitSoundWood_18; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_hitSoundWood_18() { return &___hitSoundWood_18; }
	inline void set_hitSoundWood_18(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___hitSoundWood_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitSoundWood_18), (void*)value);
	}

	inline static int32_t get_offset_of_hitSoundStone_19() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___hitSoundStone_19)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_hitSoundStone_19() const { return ___hitSoundStone_19; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_hitSoundStone_19() { return &___hitSoundStone_19; }
	inline void set_hitSoundStone_19(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___hitSoundStone_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitSoundStone_19), (void*)value);
	}

	inline static int32_t get_offset_of_hitSoundMetal_20() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___hitSoundMetal_20)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_hitSoundMetal_20() const { return ___hitSoundMetal_20; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_hitSoundMetal_20() { return &___hitSoundMetal_20; }
	inline void set_hitSoundMetal_20(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___hitSoundMetal_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitSoundMetal_20), (void*)value);
	}

	inline static int32_t get_offset_of_fleshDetectionTagToHash_21() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___fleshDetectionTagToHash_21)); }
	inline String_t* get_fleshDetectionTagToHash_21() const { return ___fleshDetectionTagToHash_21; }
	inline String_t** get_address_of_fleshDetectionTagToHash_21() { return &___fleshDetectionTagToHash_21; }
	inline void set_fleshDetectionTagToHash_21(String_t* value)
	{
		___fleshDetectionTagToHash_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fleshDetectionTagToHash_21), (void*)value);
	}

	inline static int32_t get_offset_of_woodDetectionTagToHash_22() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___woodDetectionTagToHash_22)); }
	inline String_t* get_woodDetectionTagToHash_22() const { return ___woodDetectionTagToHash_22; }
	inline String_t** get_address_of_woodDetectionTagToHash_22() { return &___woodDetectionTagToHash_22; }
	inline void set_woodDetectionTagToHash_22(String_t* value)
	{
		___woodDetectionTagToHash_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___woodDetectionTagToHash_22), (void*)value);
	}

	inline static int32_t get_offset_of_stoneDetectionTagToHash_23() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___stoneDetectionTagToHash_23)); }
	inline String_t* get_stoneDetectionTagToHash_23() const { return ___stoneDetectionTagToHash_23; }
	inline String_t** get_address_of_stoneDetectionTagToHash_23() { return &___stoneDetectionTagToHash_23; }
	inline void set_stoneDetectionTagToHash_23(String_t* value)
	{
		___stoneDetectionTagToHash_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stoneDetectionTagToHash_23), (void*)value);
	}

	inline static int32_t get_offset_of_metalDetectionTagToHash_24() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___metalDetectionTagToHash_24)); }
	inline String_t* get_metalDetectionTagToHash_24() const { return ___metalDetectionTagToHash_24; }
	inline String_t** get_address_of_metalDetectionTagToHash_24() { return &___metalDetectionTagToHash_24; }
	inline void set_metalDetectionTagToHash_24(String_t* value)
	{
		___metalDetectionTagToHash_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___metalDetectionTagToHash_24), (void*)value);
	}

	inline static int32_t get_offset_of_fleshHitPS_31() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___fleshHitPS_31)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_fleshHitPS_31() const { return ___fleshHitPS_31; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_fleshHitPS_31() { return &___fleshHitPS_31; }
	inline void set_fleshHitPS_31(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___fleshHitPS_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fleshHitPS_31), (void*)value);
	}

	inline static int32_t get_offset_of_woodHitPS_32() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___woodHitPS_32)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_woodHitPS_32() const { return ___woodHitPS_32; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_woodHitPS_32() { return &___woodHitPS_32; }
	inline void set_woodHitPS_32(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___woodHitPS_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___woodHitPS_32), (void*)value);
	}

	inline static int32_t get_offset_of_stoneHitPS_33() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___stoneHitPS_33)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_stoneHitPS_33() const { return ___stoneHitPS_33; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_stoneHitPS_33() { return &___stoneHitPS_33; }
	inline void set_stoneHitPS_33(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___stoneHitPS_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stoneHitPS_33), (void*)value);
	}

	inline static int32_t get_offset_of_metalHitPS_34() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___metalHitPS_34)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_metalHitPS_34() const { return ___metalHitPS_34; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_metalHitPS_34() { return &___metalHitPS_34; }
	inline void set_metalHitPS_34(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___metalHitPS_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___metalHitPS_34), (void*)value);
	}

	inline static int32_t get_offset_of_fleshParticleInstance_35() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___fleshParticleInstance_35)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_fleshParticleInstance_35() const { return ___fleshParticleInstance_35; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_fleshParticleInstance_35() { return &___fleshParticleInstance_35; }
	inline void set_fleshParticleInstance_35(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___fleshParticleInstance_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fleshParticleInstance_35), (void*)value);
	}

	inline static int32_t get_offset_of_woodParticleInstance_36() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___woodParticleInstance_36)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_woodParticleInstance_36() const { return ___woodParticleInstance_36; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_woodParticleInstance_36() { return &___woodParticleInstance_36; }
	inline void set_woodParticleInstance_36(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___woodParticleInstance_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___woodParticleInstance_36), (void*)value);
	}

	inline static int32_t get_offset_of_stoneParticleInstance_37() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___stoneParticleInstance_37)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stoneParticleInstance_37() const { return ___stoneParticleInstance_37; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stoneParticleInstance_37() { return &___stoneParticleInstance_37; }
	inline void set_stoneParticleInstance_37(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stoneParticleInstance_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stoneParticleInstance_37), (void*)value);
	}

	inline static int32_t get_offset_of_metalParticleInstance_38() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___metalParticleInstance_38)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_metalParticleInstance_38() const { return ___metalParticleInstance_38; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_metalParticleInstance_38() { return &___metalParticleInstance_38; }
	inline void set_metalParticleInstance_38(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___metalParticleInstance_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___metalParticleInstance_38), (void*)value);
	}

	inline static int32_t get_offset_of_currentParticleInstance_39() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___currentParticleInstance_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_currentParticleInstance_39() const { return ___currentParticleInstance_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_currentParticleInstance_39() { return &___currentParticleInstance_39; }
	inline void set_currentParticleInstance_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___currentParticleInstance_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentParticleInstance_39), (void*)value);
	}

	inline static int32_t get_offset_of_fleshParticleTransform_40() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___fleshParticleTransform_40)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_fleshParticleTransform_40() const { return ___fleshParticleTransform_40; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_fleshParticleTransform_40() { return &___fleshParticleTransform_40; }
	inline void set_fleshParticleTransform_40(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___fleshParticleTransform_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fleshParticleTransform_40), (void*)value);
	}

	inline static int32_t get_offset_of_woodParticleTransform_41() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___woodParticleTransform_41)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_woodParticleTransform_41() const { return ___woodParticleTransform_41; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_woodParticleTransform_41() { return &___woodParticleTransform_41; }
	inline void set_woodParticleTransform_41(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___woodParticleTransform_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___woodParticleTransform_41), (void*)value);
	}

	inline static int32_t get_offset_of_stoneParticleTransform_42() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___stoneParticleTransform_42)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_stoneParticleTransform_42() const { return ___stoneParticleTransform_42; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_stoneParticleTransform_42() { return &___stoneParticleTransform_42; }
	inline void set_stoneParticleTransform_42(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___stoneParticleTransform_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stoneParticleTransform_42), (void*)value);
	}

	inline static int32_t get_offset_of_metalParticleTransform_43() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___metalParticleTransform_43)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_metalParticleTransform_43() const { return ___metalParticleTransform_43; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_metalParticleTransform_43() { return &___metalParticleTransform_43; }
	inline void set_metalParticleTransform_43(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___metalParticleTransform_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___metalParticleTransform_43), (void*)value);
	}

	inline static int32_t get_offset_of_burstControl_44() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6, ___burstControl_44)); }
	inline Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB  get_burstControl_44() const { return ___burstControl_44; }
	inline Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB * get_address_of_burstControl_44() { return &___burstControl_44; }
	inline void set_burstControl_44(Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB  value)
	{
		___burstControl_44 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___burstControl_44))->___m_Count_1))->___m_CurveMin_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___burstControl_44))->___m_Count_1))->___m_CurveMax_3), (void*)NULL);
		#endif
	}
};

struct StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields
{
public:
	// System.Int32 StandardizedProjectileForHand::fleshHash
	int32_t ___fleshHash_25;
	// System.Int32 StandardizedProjectileForHand::woodHash
	int32_t ___woodHash_26;
	// System.Int32 StandardizedProjectileForHand::stoneHash
	int32_t ___stoneHash_27;
	// System.Int32 StandardizedProjectileForHand::metalHash
	int32_t ___metalHash_28;
	// System.Int32 StandardizedProjectileForHand::contactHash
	int32_t ___contactHash_29;
	// System.Int32 StandardizedProjectileForHand::projectileHash
	int32_t ___projectileHash_30;

public:
	inline static int32_t get_offset_of_fleshHash_25() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields, ___fleshHash_25)); }
	inline int32_t get_fleshHash_25() const { return ___fleshHash_25; }
	inline int32_t* get_address_of_fleshHash_25() { return &___fleshHash_25; }
	inline void set_fleshHash_25(int32_t value)
	{
		___fleshHash_25 = value;
	}

	inline static int32_t get_offset_of_woodHash_26() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields, ___woodHash_26)); }
	inline int32_t get_woodHash_26() const { return ___woodHash_26; }
	inline int32_t* get_address_of_woodHash_26() { return &___woodHash_26; }
	inline void set_woodHash_26(int32_t value)
	{
		___woodHash_26 = value;
	}

	inline static int32_t get_offset_of_stoneHash_27() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields, ___stoneHash_27)); }
	inline int32_t get_stoneHash_27() const { return ___stoneHash_27; }
	inline int32_t* get_address_of_stoneHash_27() { return &___stoneHash_27; }
	inline void set_stoneHash_27(int32_t value)
	{
		___stoneHash_27 = value;
	}

	inline static int32_t get_offset_of_metalHash_28() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields, ___metalHash_28)); }
	inline int32_t get_metalHash_28() const { return ___metalHash_28; }
	inline int32_t* get_address_of_metalHash_28() { return &___metalHash_28; }
	inline void set_metalHash_28(int32_t value)
	{
		___metalHash_28 = value;
	}

	inline static int32_t get_offset_of_contactHash_29() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields, ___contactHash_29)); }
	inline int32_t get_contactHash_29() const { return ___contactHash_29; }
	inline int32_t* get_address_of_contactHash_29() { return &___contactHash_29; }
	inline void set_contactHash_29(int32_t value)
	{
		___contactHash_29 = value;
	}

	inline static int32_t get_offset_of_projectileHash_30() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields, ___projectileHash_30)); }
	inline int32_t get_projectileHash_30() const { return ___projectileHash_30; }
	inline int32_t* get_address_of_projectileHash_30() { return &___projectileHash_30; }
	inline void set_projectileHash_30(int32_t value)
	{
		___projectileHash_30 = value;
	}
};


// StandardizedProjectileForHandBool
struct  StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Rigidbody StandardizedProjectileForHandBool::rigid
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___rigid_4;
	// StandardizedBowForHandBool StandardizedProjectileForHandBool::bowScript
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB * ___bowScript_5;
	// UnityEngine.Transform StandardizedProjectileForHandBool::quiver
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___quiver_6;
	// UnityEngine.BoxCollider StandardizedProjectileForHandBool::boxCollider
	BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * ___boxCollider_7;
	// UnityEngine.AudioSource StandardizedProjectileForHandBool::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_8;
	// System.Single StandardizedProjectileForHandBool::currentTime
	float ___currentTime_9;
	// System.Boolean StandardizedProjectileForHandBool::collisionHappened
	bool ___collisionHappened_10;
	// System.Single StandardizedProjectileForHandBool::timeLimitToDePool
	float ___timeLimitToDePool_11;
	// System.Single StandardizedProjectileForHandBool::effectOfVelocityOnParticleEmission
	float ___effectOfVelocityOnParticleEmission_12;
	// UnityEngine.GameObject StandardizedProjectileForHandBool::projectileHitParticleFlesh
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectileHitParticleFlesh_13;
	// UnityEngine.GameObject StandardizedProjectileForHandBool::projectileHitParticleWood
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectileHitParticleWood_14;
	// UnityEngine.GameObject StandardizedProjectileForHandBool::projectileHitParticleStone
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectileHitParticleStone_15;
	// UnityEngine.GameObject StandardizedProjectileForHandBool::projectileHitParticleMetal
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectileHitParticleMetal_16;
	// UnityEngine.AudioClip StandardizedProjectileForHandBool::hitSoundFlesh
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___hitSoundFlesh_17;
	// UnityEngine.AudioClip StandardizedProjectileForHandBool::hitSoundWood
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___hitSoundWood_18;
	// UnityEngine.AudioClip StandardizedProjectileForHandBool::hitSoundStone
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___hitSoundStone_19;
	// UnityEngine.AudioClip StandardizedProjectileForHandBool::hitSoundMetal
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___hitSoundMetal_20;
	// System.String StandardizedProjectileForHandBool::fleshDetectionTagToHash
	String_t* ___fleshDetectionTagToHash_21;
	// System.String StandardizedProjectileForHandBool::woodDetectionTagToHash
	String_t* ___woodDetectionTagToHash_22;
	// System.String StandardizedProjectileForHandBool::stoneDetectionTagToHash
	String_t* ___stoneDetectionTagToHash_23;
	// System.String StandardizedProjectileForHandBool::metalDetectionTagToHash
	String_t* ___metalDetectionTagToHash_24;
	// UnityEngine.ParticleSystem StandardizedProjectileForHandBool::fleshHitPS
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___fleshHitPS_31;
	// UnityEngine.ParticleSystem StandardizedProjectileForHandBool::woodHitPS
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___woodHitPS_32;
	// UnityEngine.ParticleSystem StandardizedProjectileForHandBool::stoneHitPS
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___stoneHitPS_33;
	// UnityEngine.ParticleSystem StandardizedProjectileForHandBool::metalHitPS
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___metalHitPS_34;
	// UnityEngine.GameObject StandardizedProjectileForHandBool::fleshParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___fleshParticleInstance_35;
	// UnityEngine.GameObject StandardizedProjectileForHandBool::woodParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___woodParticleInstance_36;
	// UnityEngine.GameObject StandardizedProjectileForHandBool::stoneParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stoneParticleInstance_37;
	// UnityEngine.GameObject StandardizedProjectileForHandBool::metalParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___metalParticleInstance_38;
	// UnityEngine.GameObject StandardizedProjectileForHandBool::currentParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___currentParticleInstance_39;
	// UnityEngine.Transform StandardizedProjectileForHandBool::fleshParticleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___fleshParticleTransform_40;
	// UnityEngine.Transform StandardizedProjectileForHandBool::woodParticleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___woodParticleTransform_41;
	// UnityEngine.Transform StandardizedProjectileForHandBool::stoneParticleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___stoneParticleTransform_42;
	// UnityEngine.Transform StandardizedProjectileForHandBool::metalParticleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___metalParticleTransform_43;
	// UnityEngine.ParticleSystem_Burst StandardizedProjectileForHandBool::burstControl
	Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB  ___burstControl_44;

public:
	inline static int32_t get_offset_of_rigid_4() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___rigid_4)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_rigid_4() const { return ___rigid_4; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_rigid_4() { return &___rigid_4; }
	inline void set_rigid_4(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___rigid_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rigid_4), (void*)value);
	}

	inline static int32_t get_offset_of_bowScript_5() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___bowScript_5)); }
	inline StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB * get_bowScript_5() const { return ___bowScript_5; }
	inline StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB ** get_address_of_bowScript_5() { return &___bowScript_5; }
	inline void set_bowScript_5(StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB * value)
	{
		___bowScript_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowScript_5), (void*)value);
	}

	inline static int32_t get_offset_of_quiver_6() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___quiver_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_quiver_6() const { return ___quiver_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_quiver_6() { return &___quiver_6; }
	inline void set_quiver_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___quiver_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___quiver_6), (void*)value);
	}

	inline static int32_t get_offset_of_boxCollider_7() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___boxCollider_7)); }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * get_boxCollider_7() const { return ___boxCollider_7; }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA ** get_address_of_boxCollider_7() { return &___boxCollider_7; }
	inline void set_boxCollider_7(BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * value)
	{
		___boxCollider_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___boxCollider_7), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_8() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___audioSource_8)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_8() const { return ___audioSource_8; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_8() { return &___audioSource_8; }
	inline void set_audioSource_8(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_8), (void*)value);
	}

	inline static int32_t get_offset_of_currentTime_9() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___currentTime_9)); }
	inline float get_currentTime_9() const { return ___currentTime_9; }
	inline float* get_address_of_currentTime_9() { return &___currentTime_9; }
	inline void set_currentTime_9(float value)
	{
		___currentTime_9 = value;
	}

	inline static int32_t get_offset_of_collisionHappened_10() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___collisionHappened_10)); }
	inline bool get_collisionHappened_10() const { return ___collisionHappened_10; }
	inline bool* get_address_of_collisionHappened_10() { return &___collisionHappened_10; }
	inline void set_collisionHappened_10(bool value)
	{
		___collisionHappened_10 = value;
	}

	inline static int32_t get_offset_of_timeLimitToDePool_11() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___timeLimitToDePool_11)); }
	inline float get_timeLimitToDePool_11() const { return ___timeLimitToDePool_11; }
	inline float* get_address_of_timeLimitToDePool_11() { return &___timeLimitToDePool_11; }
	inline void set_timeLimitToDePool_11(float value)
	{
		___timeLimitToDePool_11 = value;
	}

	inline static int32_t get_offset_of_effectOfVelocityOnParticleEmission_12() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___effectOfVelocityOnParticleEmission_12)); }
	inline float get_effectOfVelocityOnParticleEmission_12() const { return ___effectOfVelocityOnParticleEmission_12; }
	inline float* get_address_of_effectOfVelocityOnParticleEmission_12() { return &___effectOfVelocityOnParticleEmission_12; }
	inline void set_effectOfVelocityOnParticleEmission_12(float value)
	{
		___effectOfVelocityOnParticleEmission_12 = value;
	}

	inline static int32_t get_offset_of_projectileHitParticleFlesh_13() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___projectileHitParticleFlesh_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectileHitParticleFlesh_13() const { return ___projectileHitParticleFlesh_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectileHitParticleFlesh_13() { return &___projectileHitParticleFlesh_13; }
	inline void set_projectileHitParticleFlesh_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectileHitParticleFlesh_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectileHitParticleFlesh_13), (void*)value);
	}

	inline static int32_t get_offset_of_projectileHitParticleWood_14() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___projectileHitParticleWood_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectileHitParticleWood_14() const { return ___projectileHitParticleWood_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectileHitParticleWood_14() { return &___projectileHitParticleWood_14; }
	inline void set_projectileHitParticleWood_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectileHitParticleWood_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectileHitParticleWood_14), (void*)value);
	}

	inline static int32_t get_offset_of_projectileHitParticleStone_15() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___projectileHitParticleStone_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectileHitParticleStone_15() const { return ___projectileHitParticleStone_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectileHitParticleStone_15() { return &___projectileHitParticleStone_15; }
	inline void set_projectileHitParticleStone_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectileHitParticleStone_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectileHitParticleStone_15), (void*)value);
	}

	inline static int32_t get_offset_of_projectileHitParticleMetal_16() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___projectileHitParticleMetal_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectileHitParticleMetal_16() const { return ___projectileHitParticleMetal_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectileHitParticleMetal_16() { return &___projectileHitParticleMetal_16; }
	inline void set_projectileHitParticleMetal_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectileHitParticleMetal_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectileHitParticleMetal_16), (void*)value);
	}

	inline static int32_t get_offset_of_hitSoundFlesh_17() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___hitSoundFlesh_17)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_hitSoundFlesh_17() const { return ___hitSoundFlesh_17; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_hitSoundFlesh_17() { return &___hitSoundFlesh_17; }
	inline void set_hitSoundFlesh_17(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___hitSoundFlesh_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitSoundFlesh_17), (void*)value);
	}

	inline static int32_t get_offset_of_hitSoundWood_18() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___hitSoundWood_18)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_hitSoundWood_18() const { return ___hitSoundWood_18; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_hitSoundWood_18() { return &___hitSoundWood_18; }
	inline void set_hitSoundWood_18(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___hitSoundWood_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitSoundWood_18), (void*)value);
	}

	inline static int32_t get_offset_of_hitSoundStone_19() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___hitSoundStone_19)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_hitSoundStone_19() const { return ___hitSoundStone_19; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_hitSoundStone_19() { return &___hitSoundStone_19; }
	inline void set_hitSoundStone_19(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___hitSoundStone_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitSoundStone_19), (void*)value);
	}

	inline static int32_t get_offset_of_hitSoundMetal_20() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___hitSoundMetal_20)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_hitSoundMetal_20() const { return ___hitSoundMetal_20; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_hitSoundMetal_20() { return &___hitSoundMetal_20; }
	inline void set_hitSoundMetal_20(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___hitSoundMetal_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitSoundMetal_20), (void*)value);
	}

	inline static int32_t get_offset_of_fleshDetectionTagToHash_21() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___fleshDetectionTagToHash_21)); }
	inline String_t* get_fleshDetectionTagToHash_21() const { return ___fleshDetectionTagToHash_21; }
	inline String_t** get_address_of_fleshDetectionTagToHash_21() { return &___fleshDetectionTagToHash_21; }
	inline void set_fleshDetectionTagToHash_21(String_t* value)
	{
		___fleshDetectionTagToHash_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fleshDetectionTagToHash_21), (void*)value);
	}

	inline static int32_t get_offset_of_woodDetectionTagToHash_22() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___woodDetectionTagToHash_22)); }
	inline String_t* get_woodDetectionTagToHash_22() const { return ___woodDetectionTagToHash_22; }
	inline String_t** get_address_of_woodDetectionTagToHash_22() { return &___woodDetectionTagToHash_22; }
	inline void set_woodDetectionTagToHash_22(String_t* value)
	{
		___woodDetectionTagToHash_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___woodDetectionTagToHash_22), (void*)value);
	}

	inline static int32_t get_offset_of_stoneDetectionTagToHash_23() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___stoneDetectionTagToHash_23)); }
	inline String_t* get_stoneDetectionTagToHash_23() const { return ___stoneDetectionTagToHash_23; }
	inline String_t** get_address_of_stoneDetectionTagToHash_23() { return &___stoneDetectionTagToHash_23; }
	inline void set_stoneDetectionTagToHash_23(String_t* value)
	{
		___stoneDetectionTagToHash_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stoneDetectionTagToHash_23), (void*)value);
	}

	inline static int32_t get_offset_of_metalDetectionTagToHash_24() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___metalDetectionTagToHash_24)); }
	inline String_t* get_metalDetectionTagToHash_24() const { return ___metalDetectionTagToHash_24; }
	inline String_t** get_address_of_metalDetectionTagToHash_24() { return &___metalDetectionTagToHash_24; }
	inline void set_metalDetectionTagToHash_24(String_t* value)
	{
		___metalDetectionTagToHash_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___metalDetectionTagToHash_24), (void*)value);
	}

	inline static int32_t get_offset_of_fleshHitPS_31() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___fleshHitPS_31)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_fleshHitPS_31() const { return ___fleshHitPS_31; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_fleshHitPS_31() { return &___fleshHitPS_31; }
	inline void set_fleshHitPS_31(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___fleshHitPS_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fleshHitPS_31), (void*)value);
	}

	inline static int32_t get_offset_of_woodHitPS_32() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___woodHitPS_32)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_woodHitPS_32() const { return ___woodHitPS_32; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_woodHitPS_32() { return &___woodHitPS_32; }
	inline void set_woodHitPS_32(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___woodHitPS_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___woodHitPS_32), (void*)value);
	}

	inline static int32_t get_offset_of_stoneHitPS_33() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___stoneHitPS_33)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_stoneHitPS_33() const { return ___stoneHitPS_33; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_stoneHitPS_33() { return &___stoneHitPS_33; }
	inline void set_stoneHitPS_33(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___stoneHitPS_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stoneHitPS_33), (void*)value);
	}

	inline static int32_t get_offset_of_metalHitPS_34() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___metalHitPS_34)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_metalHitPS_34() const { return ___metalHitPS_34; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_metalHitPS_34() { return &___metalHitPS_34; }
	inline void set_metalHitPS_34(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___metalHitPS_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___metalHitPS_34), (void*)value);
	}

	inline static int32_t get_offset_of_fleshParticleInstance_35() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___fleshParticleInstance_35)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_fleshParticleInstance_35() const { return ___fleshParticleInstance_35; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_fleshParticleInstance_35() { return &___fleshParticleInstance_35; }
	inline void set_fleshParticleInstance_35(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___fleshParticleInstance_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fleshParticleInstance_35), (void*)value);
	}

	inline static int32_t get_offset_of_woodParticleInstance_36() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___woodParticleInstance_36)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_woodParticleInstance_36() const { return ___woodParticleInstance_36; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_woodParticleInstance_36() { return &___woodParticleInstance_36; }
	inline void set_woodParticleInstance_36(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___woodParticleInstance_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___woodParticleInstance_36), (void*)value);
	}

	inline static int32_t get_offset_of_stoneParticleInstance_37() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___stoneParticleInstance_37)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stoneParticleInstance_37() const { return ___stoneParticleInstance_37; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stoneParticleInstance_37() { return &___stoneParticleInstance_37; }
	inline void set_stoneParticleInstance_37(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stoneParticleInstance_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stoneParticleInstance_37), (void*)value);
	}

	inline static int32_t get_offset_of_metalParticleInstance_38() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___metalParticleInstance_38)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_metalParticleInstance_38() const { return ___metalParticleInstance_38; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_metalParticleInstance_38() { return &___metalParticleInstance_38; }
	inline void set_metalParticleInstance_38(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___metalParticleInstance_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___metalParticleInstance_38), (void*)value);
	}

	inline static int32_t get_offset_of_currentParticleInstance_39() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___currentParticleInstance_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_currentParticleInstance_39() const { return ___currentParticleInstance_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_currentParticleInstance_39() { return &___currentParticleInstance_39; }
	inline void set_currentParticleInstance_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___currentParticleInstance_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentParticleInstance_39), (void*)value);
	}

	inline static int32_t get_offset_of_fleshParticleTransform_40() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___fleshParticleTransform_40)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_fleshParticleTransform_40() const { return ___fleshParticleTransform_40; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_fleshParticleTransform_40() { return &___fleshParticleTransform_40; }
	inline void set_fleshParticleTransform_40(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___fleshParticleTransform_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fleshParticleTransform_40), (void*)value);
	}

	inline static int32_t get_offset_of_woodParticleTransform_41() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___woodParticleTransform_41)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_woodParticleTransform_41() const { return ___woodParticleTransform_41; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_woodParticleTransform_41() { return &___woodParticleTransform_41; }
	inline void set_woodParticleTransform_41(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___woodParticleTransform_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___woodParticleTransform_41), (void*)value);
	}

	inline static int32_t get_offset_of_stoneParticleTransform_42() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___stoneParticleTransform_42)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_stoneParticleTransform_42() const { return ___stoneParticleTransform_42; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_stoneParticleTransform_42() { return &___stoneParticleTransform_42; }
	inline void set_stoneParticleTransform_42(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___stoneParticleTransform_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stoneParticleTransform_42), (void*)value);
	}

	inline static int32_t get_offset_of_metalParticleTransform_43() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___metalParticleTransform_43)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_metalParticleTransform_43() const { return ___metalParticleTransform_43; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_metalParticleTransform_43() { return &___metalParticleTransform_43; }
	inline void set_metalParticleTransform_43(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___metalParticleTransform_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___metalParticleTransform_43), (void*)value);
	}

	inline static int32_t get_offset_of_burstControl_44() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F, ___burstControl_44)); }
	inline Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB  get_burstControl_44() const { return ___burstControl_44; }
	inline Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB * get_address_of_burstControl_44() { return &___burstControl_44; }
	inline void set_burstControl_44(Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB  value)
	{
		___burstControl_44 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___burstControl_44))->___m_Count_1))->___m_CurveMin_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___burstControl_44))->___m_Count_1))->___m_CurveMax_3), (void*)NULL);
		#endif
	}
};

struct StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields
{
public:
	// System.Int32 StandardizedProjectileForHandBool::fleshHash
	int32_t ___fleshHash_25;
	// System.Int32 StandardizedProjectileForHandBool::woodHash
	int32_t ___woodHash_26;
	// System.Int32 StandardizedProjectileForHandBool::stoneHash
	int32_t ___stoneHash_27;
	// System.Int32 StandardizedProjectileForHandBool::metalHash
	int32_t ___metalHash_28;
	// System.Int32 StandardizedProjectileForHandBool::contactHash
	int32_t ___contactHash_29;
	// System.Int32 StandardizedProjectileForHandBool::projectileHash
	int32_t ___projectileHash_30;

public:
	inline static int32_t get_offset_of_fleshHash_25() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields, ___fleshHash_25)); }
	inline int32_t get_fleshHash_25() const { return ___fleshHash_25; }
	inline int32_t* get_address_of_fleshHash_25() { return &___fleshHash_25; }
	inline void set_fleshHash_25(int32_t value)
	{
		___fleshHash_25 = value;
	}

	inline static int32_t get_offset_of_woodHash_26() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields, ___woodHash_26)); }
	inline int32_t get_woodHash_26() const { return ___woodHash_26; }
	inline int32_t* get_address_of_woodHash_26() { return &___woodHash_26; }
	inline void set_woodHash_26(int32_t value)
	{
		___woodHash_26 = value;
	}

	inline static int32_t get_offset_of_stoneHash_27() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields, ___stoneHash_27)); }
	inline int32_t get_stoneHash_27() const { return ___stoneHash_27; }
	inline int32_t* get_address_of_stoneHash_27() { return &___stoneHash_27; }
	inline void set_stoneHash_27(int32_t value)
	{
		___stoneHash_27 = value;
	}

	inline static int32_t get_offset_of_metalHash_28() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields, ___metalHash_28)); }
	inline int32_t get_metalHash_28() const { return ___metalHash_28; }
	inline int32_t* get_address_of_metalHash_28() { return &___metalHash_28; }
	inline void set_metalHash_28(int32_t value)
	{
		___metalHash_28 = value;
	}

	inline static int32_t get_offset_of_contactHash_29() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields, ___contactHash_29)); }
	inline int32_t get_contactHash_29() const { return ___contactHash_29; }
	inline int32_t* get_address_of_contactHash_29() { return &___contactHash_29; }
	inline void set_contactHash_29(int32_t value)
	{
		___contactHash_29 = value;
	}

	inline static int32_t get_offset_of_projectileHash_30() { return static_cast<int32_t>(offsetof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields, ___projectileHash_30)); }
	inline int32_t get_projectileHash_30() const { return ___projectileHash_30; }
	inline int32_t* get_address_of_projectileHash_30() { return &___projectileHash_30; }
	inline void set_projectileHash_30(int32_t value)
	{
		___projectileHash_30 = value;
	}
};


// StaticScript
struct  StaticScript_tB75409BF11FBBCFA339D6EFF6BDFF38EF5C5A3A2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Photon.Pun.PhotonView StaticScript::StaticPV
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___StaticPV_5;
	// UnityEngine.UI.Text StaticScript::Txtshow
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___Txtshow_6;
	// System.Int32 StaticScript::NextScene
	int32_t ___NextScene_7;

public:
	inline static int32_t get_offset_of_StaticPV_5() { return static_cast<int32_t>(offsetof(StaticScript_tB75409BF11FBBCFA339D6EFF6BDFF38EF5C5A3A2, ___StaticPV_5)); }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * get_StaticPV_5() const { return ___StaticPV_5; }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** get_address_of_StaticPV_5() { return &___StaticPV_5; }
	inline void set_StaticPV_5(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		___StaticPV_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StaticPV_5), (void*)value);
	}

	inline static int32_t get_offset_of_Txtshow_6() { return static_cast<int32_t>(offsetof(StaticScript_tB75409BF11FBBCFA339D6EFF6BDFF38EF5C5A3A2, ___Txtshow_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_Txtshow_6() const { return ___Txtshow_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_Txtshow_6() { return &___Txtshow_6; }
	inline void set_Txtshow_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___Txtshow_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Txtshow_6), (void*)value);
	}

	inline static int32_t get_offset_of_NextScene_7() { return static_cast<int32_t>(offsetof(StaticScript_tB75409BF11FBBCFA339D6EFF6BDFF38EF5C5A3A2, ___NextScene_7)); }
	inline int32_t get_NextScene_7() const { return ___NextScene_7; }
	inline int32_t* get_address_of_NextScene_7() { return &___NextScene_7; }
	inline void set_NextScene_7(int32_t value)
	{
		___NextScene_7 = value;
	}
};

struct StaticScript_tB75409BF11FBBCFA339D6EFF6BDFF38EF5C5A3A2_StaticFields
{
public:
	// System.String StaticScript::lastSceneTxt
	String_t* ___lastSceneTxt_4;

public:
	inline static int32_t get_offset_of_lastSceneTxt_4() { return static_cast<int32_t>(offsetof(StaticScript_tB75409BF11FBBCFA339D6EFF6BDFF38EF5C5A3A2_StaticFields, ___lastSceneTxt_4)); }
	inline String_t* get_lastSceneTxt_4() const { return ___lastSceneTxt_4; }
	inline String_t** get_address_of_lastSceneTxt_4() { return &___lastSceneTxt_4; }
	inline void set_lastSceneTxt_4(String_t* value)
	{
		___lastSceneTxt_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastSceneTxt_4), (void*)value);
	}
};


// HealthBarScript
struct  HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173  : public MonoBehaviourPun_t41335C3B2B6804088B520155103E3A0EEA4F814A
{
public:
	// UnityEngine.UI.Slider HealthBarScript::slider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___slider_5;
	// UnityEngine.GameObject HealthBarScript::question
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___question_6;
	// UnityEngine.GameObject HealthBarScript::mssIn
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mssIn_7;
	// UnityEngine.GameObject HealthBarScript::waiting
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___waiting_8;
	// UnityEngine.Gradient HealthBarScript::gradientColor
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * ___gradientColor_9;
	// UnityEngine.UI.Image HealthBarScript::fill
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___fill_10;
	// MotionChanger HealthBarScript::health
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317 * ___health_11;
	// System.Boolean HealthBarScript::appear
	bool ___appear_12;
	// System.Boolean HealthBarScript::change
	bool ___change_13;
	// Photon.Pun.PhotonView HealthBarScript::PV
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___PV_14;

public:
	inline static int32_t get_offset_of_slider_5() { return static_cast<int32_t>(offsetof(HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173, ___slider_5)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_slider_5() const { return ___slider_5; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_slider_5() { return &___slider_5; }
	inline void set_slider_5(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___slider_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___slider_5), (void*)value);
	}

	inline static int32_t get_offset_of_question_6() { return static_cast<int32_t>(offsetof(HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173, ___question_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_question_6() const { return ___question_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_question_6() { return &___question_6; }
	inline void set_question_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___question_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___question_6), (void*)value);
	}

	inline static int32_t get_offset_of_mssIn_7() { return static_cast<int32_t>(offsetof(HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173, ___mssIn_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mssIn_7() const { return ___mssIn_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mssIn_7() { return &___mssIn_7; }
	inline void set_mssIn_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mssIn_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mssIn_7), (void*)value);
	}

	inline static int32_t get_offset_of_waiting_8() { return static_cast<int32_t>(offsetof(HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173, ___waiting_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_waiting_8() const { return ___waiting_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_waiting_8() { return &___waiting_8; }
	inline void set_waiting_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___waiting_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___waiting_8), (void*)value);
	}

	inline static int32_t get_offset_of_gradientColor_9() { return static_cast<int32_t>(offsetof(HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173, ___gradientColor_9)); }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * get_gradientColor_9() const { return ___gradientColor_9; }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A ** get_address_of_gradientColor_9() { return &___gradientColor_9; }
	inline void set_gradientColor_9(Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * value)
	{
		___gradientColor_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gradientColor_9), (void*)value);
	}

	inline static int32_t get_offset_of_fill_10() { return static_cast<int32_t>(offsetof(HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173, ___fill_10)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_fill_10() const { return ___fill_10; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_fill_10() { return &___fill_10; }
	inline void set_fill_10(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___fill_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fill_10), (void*)value);
	}

	inline static int32_t get_offset_of_health_11() { return static_cast<int32_t>(offsetof(HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173, ___health_11)); }
	inline MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317 * get_health_11() const { return ___health_11; }
	inline MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317 ** get_address_of_health_11() { return &___health_11; }
	inline void set_health_11(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317 * value)
	{
		___health_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___health_11), (void*)value);
	}

	inline static int32_t get_offset_of_appear_12() { return static_cast<int32_t>(offsetof(HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173, ___appear_12)); }
	inline bool get_appear_12() const { return ___appear_12; }
	inline bool* get_address_of_appear_12() { return &___appear_12; }
	inline void set_appear_12(bool value)
	{
		___appear_12 = value;
	}

	inline static int32_t get_offset_of_change_13() { return static_cast<int32_t>(offsetof(HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173, ___change_13)); }
	inline bool get_change_13() const { return ___change_13; }
	inline bool* get_address_of_change_13() { return &___change_13; }
	inline void set_change_13(bool value)
	{
		___change_13 = value;
	}

	inline static int32_t get_offset_of_PV_14() { return static_cast<int32_t>(offsetof(HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173, ___PV_14)); }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * get_PV_14() const { return ___PV_14; }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** get_address_of_PV_14() { return &___PV_14; }
	inline void set_PV_14(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		___PV_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PV_14), (void*)value);
	}
};


// MotionChanger
struct  MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317  : public MonoBehaviourPun_t41335C3B2B6804088B520155103E3A0EEA4F814A
{
public:
	// UnityEngine.Animator MotionChanger::m_Animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_Animator_5;
	// System.Int32 MotionChanger::m_AnimationIndex
	int32_t ___m_AnimationIndex_6;
	// System.Int32 MotionChanger::m_AnimationMax
	int32_t ___m_AnimationMax_7;
	// UnityEngine.AnimatorStateInfo MotionChanger::m_PrevState
	AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  ___m_PrevState_8;
	// System.Boolean MotionChanger::m_ChangingMotion
	bool ___m_ChangingMotion_9;
	// System.Int32 MotionChanger::maxHealth
	int32_t ___maxHealth_10;
	// System.Int32 MotionChanger::currenthealth
	int32_t ___currenthealth_11;
	// HealthBarScript MotionChanger::healthbar
	HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173 * ___healthbar_12;
	// UnityEngine.GameObject MotionChanger::chat
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___chat_13;
	// UnityEngine.UI.Text MotionChanger::chatTxt
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___chatTxt_14;
	// UnityEngine.AudioSource MotionChanger::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_15;
	// UnityEngine.AudioClip MotionChanger::hurt
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___hurt_16;
	// UnityEngine.AudioClip MotionChanger::walk
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___walk_17;
	// Photon.Pun.PhotonView MotionChanger::motionPV
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___motionPV_18;

public:
	inline static int32_t get_offset_of_m_Animator_5() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___m_Animator_5)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_Animator_5() const { return ___m_Animator_5; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_Animator_5() { return &___m_Animator_5; }
	inline void set_m_Animator_5(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_Animator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Animator_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_AnimationIndex_6() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___m_AnimationIndex_6)); }
	inline int32_t get_m_AnimationIndex_6() const { return ___m_AnimationIndex_6; }
	inline int32_t* get_address_of_m_AnimationIndex_6() { return &___m_AnimationIndex_6; }
	inline void set_m_AnimationIndex_6(int32_t value)
	{
		___m_AnimationIndex_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationMax_7() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___m_AnimationMax_7)); }
	inline int32_t get_m_AnimationMax_7() const { return ___m_AnimationMax_7; }
	inline int32_t* get_address_of_m_AnimationMax_7() { return &___m_AnimationMax_7; }
	inline void set_m_AnimationMax_7(int32_t value)
	{
		___m_AnimationMax_7 = value;
	}

	inline static int32_t get_offset_of_m_PrevState_8() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___m_PrevState_8)); }
	inline AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  get_m_PrevState_8() const { return ___m_PrevState_8; }
	inline AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 * get_address_of_m_PrevState_8() { return &___m_PrevState_8; }
	inline void set_m_PrevState_8(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  value)
	{
		___m_PrevState_8 = value;
	}

	inline static int32_t get_offset_of_m_ChangingMotion_9() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___m_ChangingMotion_9)); }
	inline bool get_m_ChangingMotion_9() const { return ___m_ChangingMotion_9; }
	inline bool* get_address_of_m_ChangingMotion_9() { return &___m_ChangingMotion_9; }
	inline void set_m_ChangingMotion_9(bool value)
	{
		___m_ChangingMotion_9 = value;
	}

	inline static int32_t get_offset_of_maxHealth_10() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___maxHealth_10)); }
	inline int32_t get_maxHealth_10() const { return ___maxHealth_10; }
	inline int32_t* get_address_of_maxHealth_10() { return &___maxHealth_10; }
	inline void set_maxHealth_10(int32_t value)
	{
		___maxHealth_10 = value;
	}

	inline static int32_t get_offset_of_currenthealth_11() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___currenthealth_11)); }
	inline int32_t get_currenthealth_11() const { return ___currenthealth_11; }
	inline int32_t* get_address_of_currenthealth_11() { return &___currenthealth_11; }
	inline void set_currenthealth_11(int32_t value)
	{
		___currenthealth_11 = value;
	}

	inline static int32_t get_offset_of_healthbar_12() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___healthbar_12)); }
	inline HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173 * get_healthbar_12() const { return ___healthbar_12; }
	inline HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173 ** get_address_of_healthbar_12() { return &___healthbar_12; }
	inline void set_healthbar_12(HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173 * value)
	{
		___healthbar_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___healthbar_12), (void*)value);
	}

	inline static int32_t get_offset_of_chat_13() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___chat_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_chat_13() const { return ___chat_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_chat_13() { return &___chat_13; }
	inline void set_chat_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___chat_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chat_13), (void*)value);
	}

	inline static int32_t get_offset_of_chatTxt_14() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___chatTxt_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_chatTxt_14() const { return ___chatTxt_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_chatTxt_14() { return &___chatTxt_14; }
	inline void set_chatTxt_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___chatTxt_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chatTxt_14), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_15() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___audioSource_15)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_15() const { return ___audioSource_15; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_15() { return &___audioSource_15; }
	inline void set_audioSource_15(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_15), (void*)value);
	}

	inline static int32_t get_offset_of_hurt_16() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___hurt_16)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_hurt_16() const { return ___hurt_16; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_hurt_16() { return &___hurt_16; }
	inline void set_hurt_16(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___hurt_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hurt_16), (void*)value);
	}

	inline static int32_t get_offset_of_walk_17() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___walk_17)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_walk_17() const { return ___walk_17; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_walk_17() { return &___walk_17; }
	inline void set_walk_17(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___walk_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___walk_17), (void*)value);
	}

	inline static int32_t get_offset_of_motionPV_18() { return static_cast<int32_t>(offsetof(MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317, ___motionPV_18)); }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * get_motionPV_18() const { return ___motionPV_18; }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** get_address_of_motionPV_18() { return &___motionPV_18; }
	inline void set_motionPV_18(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		___motionPV_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___motionPV_18), (void*)value);
	}
};


// Photon.Pun.Demo.Cockpit.ServerProperty
struct  ServerProperty_tF396F8221CADA4E0341D954C80E2E98CA87370F6  : public PropertyListenerBase_t05B9504A6B7DFE170062320525A7A5F60D48E3CC
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Cockpit.ServerProperty::Text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___Text_7;
	// Photon.Realtime.ServerConnection Photon.Pun.Demo.Cockpit.ServerProperty::_cache
	int32_t ____cache_8;

public:
	inline static int32_t get_offset_of_Text_7() { return static_cast<int32_t>(offsetof(ServerProperty_tF396F8221CADA4E0341D954C80E2E98CA87370F6, ___Text_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_Text_7() const { return ___Text_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_Text_7() { return &___Text_7; }
	inline void set_Text_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___Text_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text_7), (void*)value);
	}

	inline static int32_t get_offset_of__cache_8() { return static_cast<int32_t>(offsetof(ServerProperty_tF396F8221CADA4E0341D954C80E2E98CA87370F6, ____cache_8)); }
	inline int32_t get__cache_8() const { return ____cache_8; }
	inline int32_t* get_address_of__cache_8() { return &____cache_8; }
	inline void set__cache_8(int32_t value)
	{
		____cache_8 = value;
	}
};


// Photon.Pun.Demo.PunBasics.PlayerAnimatorManager
struct  PlayerAnimatorManager_t359E814EB3F102A750B3504A3378A5C09D618336  : public MonoBehaviourPun_t41335C3B2B6804088B520155103E3A0EEA4F814A
{
public:
	// System.Single Photon.Pun.Demo.PunBasics.PlayerAnimatorManager::directionDampTime
	float ___directionDampTime_5;
	// UnityEngine.Animator Photon.Pun.Demo.PunBasics.PlayerAnimatorManager::animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___animator_6;

public:
	inline static int32_t get_offset_of_directionDampTime_5() { return static_cast<int32_t>(offsetof(PlayerAnimatorManager_t359E814EB3F102A750B3504A3378A5C09D618336, ___directionDampTime_5)); }
	inline float get_directionDampTime_5() const { return ___directionDampTime_5; }
	inline float* get_address_of_directionDampTime_5() { return &___directionDampTime_5; }
	inline void set_directionDampTime_5(float value)
	{
		___directionDampTime_5 = value;
	}

	inline static int32_t get_offset_of_animator_6() { return static_cast<int32_t>(offsetof(PlayerAnimatorManager_t359E814EB3F102A750B3504A3378A5C09D618336, ___animator_6)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_animator_6() const { return ___animator_6; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_animator_6() { return &___animator_6; }
	inline void set_animator_6(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___animator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animator_6), (void*)value);
	}
};


// Photon.Pun.Demo.SlotRacer.PlayerControl
struct  PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7  : public MonoBehaviourPun_t41335C3B2B6804088B520155103E3A0EEA4F814A
{
public:
	// UnityEngine.GameObject[] Photon.Pun.Demo.SlotRacer.PlayerControl::CarPrefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___CarPrefabs_5;
	// System.Single Photon.Pun.Demo.SlotRacer.PlayerControl::MaximumSpeed
	float ___MaximumSpeed_6;
	// System.Single Photon.Pun.Demo.SlotRacer.PlayerControl::Drag
	float ___Drag_7;
	// System.Single Photon.Pun.Demo.SlotRacer.PlayerControl::CurrentSpeed
	float ___CurrentSpeed_8;
	// System.Single Photon.Pun.Demo.SlotRacer.PlayerControl::CurrentDistance
	float ___CurrentDistance_9;
	// UnityEngine.GameObject Photon.Pun.Demo.SlotRacer.PlayerControl::CarInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___CarInstance_10;
	// Photon.Pun.Demo.SlotRacer.Utils.SplineWalker Photon.Pun.Demo.SlotRacer.PlayerControl::SplineWalker
	SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F * ___SplineWalker_11;
	// System.Boolean Photon.Pun.Demo.SlotRacer.PlayerControl::m_firstTake
	bool ___m_firstTake_12;
	// System.Single Photon.Pun.Demo.SlotRacer.PlayerControl::m_input
	float ___m_input_13;

public:
	inline static int32_t get_offset_of_CarPrefabs_5() { return static_cast<int32_t>(offsetof(PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7, ___CarPrefabs_5)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_CarPrefabs_5() const { return ___CarPrefabs_5; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_CarPrefabs_5() { return &___CarPrefabs_5; }
	inline void set_CarPrefabs_5(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___CarPrefabs_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CarPrefabs_5), (void*)value);
	}

	inline static int32_t get_offset_of_MaximumSpeed_6() { return static_cast<int32_t>(offsetof(PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7, ___MaximumSpeed_6)); }
	inline float get_MaximumSpeed_6() const { return ___MaximumSpeed_6; }
	inline float* get_address_of_MaximumSpeed_6() { return &___MaximumSpeed_6; }
	inline void set_MaximumSpeed_6(float value)
	{
		___MaximumSpeed_6 = value;
	}

	inline static int32_t get_offset_of_Drag_7() { return static_cast<int32_t>(offsetof(PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7, ___Drag_7)); }
	inline float get_Drag_7() const { return ___Drag_7; }
	inline float* get_address_of_Drag_7() { return &___Drag_7; }
	inline void set_Drag_7(float value)
	{
		___Drag_7 = value;
	}

	inline static int32_t get_offset_of_CurrentSpeed_8() { return static_cast<int32_t>(offsetof(PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7, ___CurrentSpeed_8)); }
	inline float get_CurrentSpeed_8() const { return ___CurrentSpeed_8; }
	inline float* get_address_of_CurrentSpeed_8() { return &___CurrentSpeed_8; }
	inline void set_CurrentSpeed_8(float value)
	{
		___CurrentSpeed_8 = value;
	}

	inline static int32_t get_offset_of_CurrentDistance_9() { return static_cast<int32_t>(offsetof(PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7, ___CurrentDistance_9)); }
	inline float get_CurrentDistance_9() const { return ___CurrentDistance_9; }
	inline float* get_address_of_CurrentDistance_9() { return &___CurrentDistance_9; }
	inline void set_CurrentDistance_9(float value)
	{
		___CurrentDistance_9 = value;
	}

	inline static int32_t get_offset_of_CarInstance_10() { return static_cast<int32_t>(offsetof(PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7, ___CarInstance_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_CarInstance_10() const { return ___CarInstance_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_CarInstance_10() { return &___CarInstance_10; }
	inline void set_CarInstance_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___CarInstance_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CarInstance_10), (void*)value);
	}

	inline static int32_t get_offset_of_SplineWalker_11() { return static_cast<int32_t>(offsetof(PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7, ___SplineWalker_11)); }
	inline SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F * get_SplineWalker_11() const { return ___SplineWalker_11; }
	inline SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F ** get_address_of_SplineWalker_11() { return &___SplineWalker_11; }
	inline void set_SplineWalker_11(SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F * value)
	{
		___SplineWalker_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SplineWalker_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_firstTake_12() { return static_cast<int32_t>(offsetof(PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7, ___m_firstTake_12)); }
	inline bool get_m_firstTake_12() const { return ___m_firstTake_12; }
	inline bool* get_address_of_m_firstTake_12() { return &___m_firstTake_12; }
	inline void set_m_firstTake_12(bool value)
	{
		___m_firstTake_12 = value;
	}

	inline static int32_t get_offset_of_m_input_13() { return static_cast<int32_t>(offsetof(PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7, ___m_input_13)); }
	inline float get_m_input_13() const { return ___m_input_13; }
	inline float* get_address_of_m_input_13() { return &___m_input_13; }
	inline void set_m_input_13(float value)
	{
		___m_input_13 = value;
	}
};


// Photon.Pun.MonoBehaviourPunCallbacks
struct  MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D  : public MonoBehaviourPun_t41335C3B2B6804088B520155103E3A0EEA4F814A
{
public:

public:
};


// StandardizedBow
struct  StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA  : public MonoBehaviourPun_t41335C3B2B6804088B520155103E3A0EEA4F814A
{
public:
	// System.Single StandardizedBow::currentTime
	float ___currentTime_5;
	// System.Single StandardizedBow::lerpPercentage
	float ___lerpPercentage_6;
	// System.Single StandardizedBow::startingAngleDown
	float ___startingAngleDown_7;
	// System.Single StandardizedBow::startingAngleUp
	float ___startingAngleUp_8;
	// System.Single StandardizedBow::currentAngleDown
	float ___currentAngleDown_9;
	// System.Single StandardizedBow::currentAngleUp
	float ___currentAngleUp_10;
	// UnityEngine.Vector3 StandardizedBow::stringStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___stringStartPos_11;
	// UnityEngine.Vector3 StandardizedBow::stringEndPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___stringEndPos_12;
	// UnityEngine.Vector3 StandardizedBow::stringLastPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___stringLastPos_13;
	// UnityEngine.Vector3 StandardizedBow::bowDirPull
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bowDirPull_14;
	// UnityEngine.Vector3 StandardizedBow::bowDirRetract
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bowDirRetract_15;
	// UnityEngine.Vector3 StandardizedBow::firstStartUpJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartUpJointRot1_16;
	// UnityEngine.Vector3 StandardizedBow::firstStartUpJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartUpJointRot2_17;
	// UnityEngine.Vector3 StandardizedBow::firstStartUpJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartUpJointRot3_18;
	// UnityEngine.Vector3 StandardizedBow::firstStartDownJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartDownJointRot1_19;
	// UnityEngine.Vector3 StandardizedBow::firstStartDownJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartDownJointRot2_20;
	// UnityEngine.Vector3 StandardizedBow::firstStartDownJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstStartDownJointRot3_21;
	// UnityEngine.Vector3 StandardizedBow::firstEndUpJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndUpJointRot1_22;
	// UnityEngine.Vector3 StandardizedBow::firstEndUpJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndUpJointRot2_23;
	// UnityEngine.Vector3 StandardizedBow::firstEndUpJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndUpJointRot3_24;
	// UnityEngine.Vector3 StandardizedBow::firstEndDownJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndDownJointRot1_25;
	// UnityEngine.Vector3 StandardizedBow::firstEndDownJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndDownJointRot2_26;
	// UnityEngine.Vector3 StandardizedBow::firstEndDownJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstEndDownJointRot3_27;
	// UnityEngine.Vector3 StandardizedBow::firstLastUpJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastUpJointRot1_28;
	// UnityEngine.Vector3 StandardizedBow::firstLastUpJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastUpJointRot2_29;
	// UnityEngine.Vector3 StandardizedBow::firstLastUpJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastUpJointRot3_30;
	// UnityEngine.Vector3 StandardizedBow::firstLastDownJointRot1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastDownJointRot1_31;
	// UnityEngine.Vector3 StandardizedBow::firstLastDownJointRot2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastDownJointRot2_32;
	// UnityEngine.Vector3 StandardizedBow::firstLastDownJointRot3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___firstLastDownJointRot3_33;
	// UnityEngine.GameObject StandardizedBow::lastProjectile
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lastProjectile_34;
	// UnityEngine.GameObject StandardizedBow::stringStartObj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stringStartObj_35;
	// UnityEngine.GameObject StandardizedBow::stringEndObj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stringEndObj_36;
	// UnityEngine.GameObject StandardizedBow::poolHolder
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___poolHolder_37;
	// UnityEngine.Transform StandardizedBow::stringEndObjTrans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___stringEndObjTrans_38;
	// UnityEngine.Transform StandardizedBow::stringStartObjTrans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___stringStartObjTrans_39;
	// UnityEngine.Transform StandardizedBow::poolHolderTrans
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___poolHolderTrans_40;
	// StandardizedProjectile StandardizedBow::lastProjectileScript
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439 * ___lastProjectileScript_41;
	// System.Collections.Generic.Queue`1<UnityEngine.GameObject> StandardizedBow::projectilePool
	Queue_1_t0069840E94B59EC80C3FA49EF6ECA3A067555290 * ___projectilePool_42;
	// UnityEngine.Transform StandardizedBow::lastProjectileTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___lastProjectileTransform_43;
	// UnityEngine.Rigidbody StandardizedBow::lastProjectileRigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___lastProjectileRigidbody_44;
	// UnityEngine.AudioSource StandardizedBow::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_45;
	// System.Single StandardizedBow::currentStressOnString
	float ___currentStressOnString_46;
	// System.Boolean StandardizedBow::justLeftString
	bool ___justLeftString_47;
	// System.Boolean StandardizedBow::justPulledString
	bool ___justPulledString_48;
	// UnityEngine.Transform StandardizedBow::bowUpJoint1
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowUpJoint1_51;
	// UnityEngine.Transform StandardizedBow::bowUpJoint2
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowUpJoint2_52;
	// UnityEngine.Transform StandardizedBow::bowUpJoint3
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowUpJoint3_53;
	// UnityEngine.Transform StandardizedBow::bowDownJoint1
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowDownJoint1_54;
	// UnityEngine.Transform StandardizedBow::bowDownJoint2
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowDownJoint2_55;
	// UnityEngine.Transform StandardizedBow::bowDownJoint3
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowDownJoint3_56;
	// UnityEngine.Transform StandardizedBow::bowStringPoint
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___bowStringPoint_57;
	// StandardizedBow_jointsDirection StandardizedBow::joint1RotateDirectionAxis
	int32_t ___joint1RotateDirectionAxis_58;
	// StandardizedBow_jointsDirection StandardizedBow::joint2RotateDirectionAxis
	int32_t ___joint2RotateDirectionAxis_59;
	// StandardizedBow_jointsDirection StandardizedBow::joint3RotateDirectionAxis
	int32_t ___joint3RotateDirectionAxis_60;
	// System.Single StandardizedBow::bendAngleLimitFirstJoints
	float ___bendAngleLimitFirstJoints_61;
	// System.Single StandardizedBow::bendAngleLimitSecondJoints
	float ___bendAngleLimitSecondJoints_62;
	// System.Single StandardizedBow::bendAngleLimitThirdJoints
	float ___bendAngleLimitThirdJoints_63;
	// System.Boolean StandardizedBow::inverseBend
	bool ___inverseBend_64;
	// StandardizedBow_axisDirection StandardizedBow::stringMoveDirectionAxis
	int32_t ___stringMoveDirectionAxis_65;
	// System.Single StandardizedBow::stringMoveAmount
	float ___stringMoveAmount_66;
	// System.Single StandardizedBow::stringMoveTime
	float ___stringMoveTime_67;
	// System.Single StandardizedBow::stringRetractTime
	float ___stringRetractTime_68;
	// System.Boolean StandardizedBow::inversePull
	bool ___inversePull_69;
	// StandardizedBow_stringStressEaseType StandardizedBow::stringMovementChoice
	int32_t ___stringMovementChoice_70;
	// StandardizedBow_stringRetractEaseType StandardizedBow::stringRetractChoice
	int32_t ___stringRetractChoice_71;
	// System.Single StandardizedBow::maxStringStrength
	float ___maxStringStrength_72;
	// StandardizedBow_accuracyProjectile StandardizedBow::projectileAccuracy
	int32_t ___projectileAccuracy_73;
	// UnityEngine.GameObject StandardizedBow::projectile
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectile_74;
	// System.Int32 StandardizedBow::projectilePoolSize
	int32_t ___projectilePoolSize_75;
	// UnityEngine.Vector3 StandardizedBow::projectileHoldPosOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___projectileHoldPosOffset_76;
	// System.Boolean StandardizedBow::isPosOffsetLocal
	bool ___isPosOffsetLocal_77;
	// UnityEngine.Vector3 StandardizedBow::projectileHoldRotOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___projectileHoldRotOffset_78;
	// StandardizedBow_axisDirection StandardizedBow::projectileForwardAxis
	int32_t ___projectileForwardAxis_79;
	// System.Single StandardizedBow::soundVolume
	float ___soundVolume_80;
	// UnityEngine.AudioClip StandardizedBow::pullSound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___pullSound_81;
	// UnityEngine.AudioClip StandardizedBow::retractSound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___retractSound_82;
	// System.Boolean StandardizedBow::stressEffectOnSound
	bool ___stressEffectOnSound_83;
	// System.Single StandardizedBow::x
	float ___x_84;
	// System.Single StandardizedBow::y
	float ___y_85;

public:
	inline static int32_t get_offset_of_currentTime_5() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___currentTime_5)); }
	inline float get_currentTime_5() const { return ___currentTime_5; }
	inline float* get_address_of_currentTime_5() { return &___currentTime_5; }
	inline void set_currentTime_5(float value)
	{
		___currentTime_5 = value;
	}

	inline static int32_t get_offset_of_lerpPercentage_6() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___lerpPercentage_6)); }
	inline float get_lerpPercentage_6() const { return ___lerpPercentage_6; }
	inline float* get_address_of_lerpPercentage_6() { return &___lerpPercentage_6; }
	inline void set_lerpPercentage_6(float value)
	{
		___lerpPercentage_6 = value;
	}

	inline static int32_t get_offset_of_startingAngleDown_7() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___startingAngleDown_7)); }
	inline float get_startingAngleDown_7() const { return ___startingAngleDown_7; }
	inline float* get_address_of_startingAngleDown_7() { return &___startingAngleDown_7; }
	inline void set_startingAngleDown_7(float value)
	{
		___startingAngleDown_7 = value;
	}

	inline static int32_t get_offset_of_startingAngleUp_8() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___startingAngleUp_8)); }
	inline float get_startingAngleUp_8() const { return ___startingAngleUp_8; }
	inline float* get_address_of_startingAngleUp_8() { return &___startingAngleUp_8; }
	inline void set_startingAngleUp_8(float value)
	{
		___startingAngleUp_8 = value;
	}

	inline static int32_t get_offset_of_currentAngleDown_9() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___currentAngleDown_9)); }
	inline float get_currentAngleDown_9() const { return ___currentAngleDown_9; }
	inline float* get_address_of_currentAngleDown_9() { return &___currentAngleDown_9; }
	inline void set_currentAngleDown_9(float value)
	{
		___currentAngleDown_9 = value;
	}

	inline static int32_t get_offset_of_currentAngleUp_10() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___currentAngleUp_10)); }
	inline float get_currentAngleUp_10() const { return ___currentAngleUp_10; }
	inline float* get_address_of_currentAngleUp_10() { return &___currentAngleUp_10; }
	inline void set_currentAngleUp_10(float value)
	{
		___currentAngleUp_10 = value;
	}

	inline static int32_t get_offset_of_stringStartPos_11() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stringStartPos_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_stringStartPos_11() const { return ___stringStartPos_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_stringStartPos_11() { return &___stringStartPos_11; }
	inline void set_stringStartPos_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___stringStartPos_11 = value;
	}

	inline static int32_t get_offset_of_stringEndPos_12() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stringEndPos_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_stringEndPos_12() const { return ___stringEndPos_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_stringEndPos_12() { return &___stringEndPos_12; }
	inline void set_stringEndPos_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___stringEndPos_12 = value;
	}

	inline static int32_t get_offset_of_stringLastPos_13() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stringLastPos_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_stringLastPos_13() const { return ___stringLastPos_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_stringLastPos_13() { return &___stringLastPos_13; }
	inline void set_stringLastPos_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___stringLastPos_13 = value;
	}

	inline static int32_t get_offset_of_bowDirPull_14() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___bowDirPull_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bowDirPull_14() const { return ___bowDirPull_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bowDirPull_14() { return &___bowDirPull_14; }
	inline void set_bowDirPull_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bowDirPull_14 = value;
	}

	inline static int32_t get_offset_of_bowDirRetract_15() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___bowDirRetract_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bowDirRetract_15() const { return ___bowDirRetract_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bowDirRetract_15() { return &___bowDirRetract_15; }
	inline void set_bowDirRetract_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bowDirRetract_15 = value;
	}

	inline static int32_t get_offset_of_firstStartUpJointRot1_16() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstStartUpJointRot1_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartUpJointRot1_16() const { return ___firstStartUpJointRot1_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartUpJointRot1_16() { return &___firstStartUpJointRot1_16; }
	inline void set_firstStartUpJointRot1_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartUpJointRot1_16 = value;
	}

	inline static int32_t get_offset_of_firstStartUpJointRot2_17() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstStartUpJointRot2_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartUpJointRot2_17() const { return ___firstStartUpJointRot2_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartUpJointRot2_17() { return &___firstStartUpJointRot2_17; }
	inline void set_firstStartUpJointRot2_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartUpJointRot2_17 = value;
	}

	inline static int32_t get_offset_of_firstStartUpJointRot3_18() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstStartUpJointRot3_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartUpJointRot3_18() const { return ___firstStartUpJointRot3_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartUpJointRot3_18() { return &___firstStartUpJointRot3_18; }
	inline void set_firstStartUpJointRot3_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartUpJointRot3_18 = value;
	}

	inline static int32_t get_offset_of_firstStartDownJointRot1_19() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstStartDownJointRot1_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartDownJointRot1_19() const { return ___firstStartDownJointRot1_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartDownJointRot1_19() { return &___firstStartDownJointRot1_19; }
	inline void set_firstStartDownJointRot1_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartDownJointRot1_19 = value;
	}

	inline static int32_t get_offset_of_firstStartDownJointRot2_20() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstStartDownJointRot2_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartDownJointRot2_20() const { return ___firstStartDownJointRot2_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartDownJointRot2_20() { return &___firstStartDownJointRot2_20; }
	inline void set_firstStartDownJointRot2_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartDownJointRot2_20 = value;
	}

	inline static int32_t get_offset_of_firstStartDownJointRot3_21() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstStartDownJointRot3_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstStartDownJointRot3_21() const { return ___firstStartDownJointRot3_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstStartDownJointRot3_21() { return &___firstStartDownJointRot3_21; }
	inline void set_firstStartDownJointRot3_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstStartDownJointRot3_21 = value;
	}

	inline static int32_t get_offset_of_firstEndUpJointRot1_22() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstEndUpJointRot1_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndUpJointRot1_22() const { return ___firstEndUpJointRot1_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndUpJointRot1_22() { return &___firstEndUpJointRot1_22; }
	inline void set_firstEndUpJointRot1_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndUpJointRot1_22 = value;
	}

	inline static int32_t get_offset_of_firstEndUpJointRot2_23() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstEndUpJointRot2_23)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndUpJointRot2_23() const { return ___firstEndUpJointRot2_23; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndUpJointRot2_23() { return &___firstEndUpJointRot2_23; }
	inline void set_firstEndUpJointRot2_23(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndUpJointRot2_23 = value;
	}

	inline static int32_t get_offset_of_firstEndUpJointRot3_24() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstEndUpJointRot3_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndUpJointRot3_24() const { return ___firstEndUpJointRot3_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndUpJointRot3_24() { return &___firstEndUpJointRot3_24; }
	inline void set_firstEndUpJointRot3_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndUpJointRot3_24 = value;
	}

	inline static int32_t get_offset_of_firstEndDownJointRot1_25() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstEndDownJointRot1_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndDownJointRot1_25() const { return ___firstEndDownJointRot1_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndDownJointRot1_25() { return &___firstEndDownJointRot1_25; }
	inline void set_firstEndDownJointRot1_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndDownJointRot1_25 = value;
	}

	inline static int32_t get_offset_of_firstEndDownJointRot2_26() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstEndDownJointRot2_26)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndDownJointRot2_26() const { return ___firstEndDownJointRot2_26; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndDownJointRot2_26() { return &___firstEndDownJointRot2_26; }
	inline void set_firstEndDownJointRot2_26(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndDownJointRot2_26 = value;
	}

	inline static int32_t get_offset_of_firstEndDownJointRot3_27() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstEndDownJointRot3_27)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstEndDownJointRot3_27() const { return ___firstEndDownJointRot3_27; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstEndDownJointRot3_27() { return &___firstEndDownJointRot3_27; }
	inline void set_firstEndDownJointRot3_27(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstEndDownJointRot3_27 = value;
	}

	inline static int32_t get_offset_of_firstLastUpJointRot1_28() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstLastUpJointRot1_28)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastUpJointRot1_28() const { return ___firstLastUpJointRot1_28; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastUpJointRot1_28() { return &___firstLastUpJointRot1_28; }
	inline void set_firstLastUpJointRot1_28(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastUpJointRot1_28 = value;
	}

	inline static int32_t get_offset_of_firstLastUpJointRot2_29() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstLastUpJointRot2_29)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastUpJointRot2_29() const { return ___firstLastUpJointRot2_29; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastUpJointRot2_29() { return &___firstLastUpJointRot2_29; }
	inline void set_firstLastUpJointRot2_29(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastUpJointRot2_29 = value;
	}

	inline static int32_t get_offset_of_firstLastUpJointRot3_30() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstLastUpJointRot3_30)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastUpJointRot3_30() const { return ___firstLastUpJointRot3_30; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastUpJointRot3_30() { return &___firstLastUpJointRot3_30; }
	inline void set_firstLastUpJointRot3_30(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastUpJointRot3_30 = value;
	}

	inline static int32_t get_offset_of_firstLastDownJointRot1_31() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstLastDownJointRot1_31)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastDownJointRot1_31() const { return ___firstLastDownJointRot1_31; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastDownJointRot1_31() { return &___firstLastDownJointRot1_31; }
	inline void set_firstLastDownJointRot1_31(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastDownJointRot1_31 = value;
	}

	inline static int32_t get_offset_of_firstLastDownJointRot2_32() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstLastDownJointRot2_32)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastDownJointRot2_32() const { return ___firstLastDownJointRot2_32; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastDownJointRot2_32() { return &___firstLastDownJointRot2_32; }
	inline void set_firstLastDownJointRot2_32(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastDownJointRot2_32 = value;
	}

	inline static int32_t get_offset_of_firstLastDownJointRot3_33() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___firstLastDownJointRot3_33)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_firstLastDownJointRot3_33() const { return ___firstLastDownJointRot3_33; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_firstLastDownJointRot3_33() { return &___firstLastDownJointRot3_33; }
	inline void set_firstLastDownJointRot3_33(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___firstLastDownJointRot3_33 = value;
	}

	inline static int32_t get_offset_of_lastProjectile_34() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___lastProjectile_34)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_lastProjectile_34() const { return ___lastProjectile_34; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_lastProjectile_34() { return &___lastProjectile_34; }
	inline void set_lastProjectile_34(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___lastProjectile_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastProjectile_34), (void*)value);
	}

	inline static int32_t get_offset_of_stringStartObj_35() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stringStartObj_35)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stringStartObj_35() const { return ___stringStartObj_35; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stringStartObj_35() { return &___stringStartObj_35; }
	inline void set_stringStartObj_35(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stringStartObj_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringStartObj_35), (void*)value);
	}

	inline static int32_t get_offset_of_stringEndObj_36() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stringEndObj_36)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stringEndObj_36() const { return ___stringEndObj_36; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stringEndObj_36() { return &___stringEndObj_36; }
	inline void set_stringEndObj_36(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stringEndObj_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringEndObj_36), (void*)value);
	}

	inline static int32_t get_offset_of_poolHolder_37() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___poolHolder_37)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_poolHolder_37() const { return ___poolHolder_37; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_poolHolder_37() { return &___poolHolder_37; }
	inline void set_poolHolder_37(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___poolHolder_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___poolHolder_37), (void*)value);
	}

	inline static int32_t get_offset_of_stringEndObjTrans_38() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stringEndObjTrans_38)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_stringEndObjTrans_38() const { return ___stringEndObjTrans_38; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_stringEndObjTrans_38() { return &___stringEndObjTrans_38; }
	inline void set_stringEndObjTrans_38(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___stringEndObjTrans_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringEndObjTrans_38), (void*)value);
	}

	inline static int32_t get_offset_of_stringStartObjTrans_39() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stringStartObjTrans_39)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_stringStartObjTrans_39() const { return ___stringStartObjTrans_39; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_stringStartObjTrans_39() { return &___stringStartObjTrans_39; }
	inline void set_stringStartObjTrans_39(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___stringStartObjTrans_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringStartObjTrans_39), (void*)value);
	}

	inline static int32_t get_offset_of_poolHolderTrans_40() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___poolHolderTrans_40)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_poolHolderTrans_40() const { return ___poolHolderTrans_40; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_poolHolderTrans_40() { return &___poolHolderTrans_40; }
	inline void set_poolHolderTrans_40(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___poolHolderTrans_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___poolHolderTrans_40), (void*)value);
	}

	inline static int32_t get_offset_of_lastProjectileScript_41() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___lastProjectileScript_41)); }
	inline StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439 * get_lastProjectileScript_41() const { return ___lastProjectileScript_41; }
	inline StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439 ** get_address_of_lastProjectileScript_41() { return &___lastProjectileScript_41; }
	inline void set_lastProjectileScript_41(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439 * value)
	{
		___lastProjectileScript_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastProjectileScript_41), (void*)value);
	}

	inline static int32_t get_offset_of_projectilePool_42() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___projectilePool_42)); }
	inline Queue_1_t0069840E94B59EC80C3FA49EF6ECA3A067555290 * get_projectilePool_42() const { return ___projectilePool_42; }
	inline Queue_1_t0069840E94B59EC80C3FA49EF6ECA3A067555290 ** get_address_of_projectilePool_42() { return &___projectilePool_42; }
	inline void set_projectilePool_42(Queue_1_t0069840E94B59EC80C3FA49EF6ECA3A067555290 * value)
	{
		___projectilePool_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectilePool_42), (void*)value);
	}

	inline static int32_t get_offset_of_lastProjectileTransform_43() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___lastProjectileTransform_43)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_lastProjectileTransform_43() const { return ___lastProjectileTransform_43; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_lastProjectileTransform_43() { return &___lastProjectileTransform_43; }
	inline void set_lastProjectileTransform_43(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___lastProjectileTransform_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastProjectileTransform_43), (void*)value);
	}

	inline static int32_t get_offset_of_lastProjectileRigidbody_44() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___lastProjectileRigidbody_44)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_lastProjectileRigidbody_44() const { return ___lastProjectileRigidbody_44; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_lastProjectileRigidbody_44() { return &___lastProjectileRigidbody_44; }
	inline void set_lastProjectileRigidbody_44(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___lastProjectileRigidbody_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastProjectileRigidbody_44), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_45() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___audioSource_45)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_45() const { return ___audioSource_45; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_45() { return &___audioSource_45; }
	inline void set_audioSource_45(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_45), (void*)value);
	}

	inline static int32_t get_offset_of_currentStressOnString_46() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___currentStressOnString_46)); }
	inline float get_currentStressOnString_46() const { return ___currentStressOnString_46; }
	inline float* get_address_of_currentStressOnString_46() { return &___currentStressOnString_46; }
	inline void set_currentStressOnString_46(float value)
	{
		___currentStressOnString_46 = value;
	}

	inline static int32_t get_offset_of_justLeftString_47() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___justLeftString_47)); }
	inline bool get_justLeftString_47() const { return ___justLeftString_47; }
	inline bool* get_address_of_justLeftString_47() { return &___justLeftString_47; }
	inline void set_justLeftString_47(bool value)
	{
		___justLeftString_47 = value;
	}

	inline static int32_t get_offset_of_justPulledString_48() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___justPulledString_48)); }
	inline bool get_justPulledString_48() const { return ___justPulledString_48; }
	inline bool* get_address_of_justPulledString_48() { return &___justPulledString_48; }
	inline void set_justPulledString_48(bool value)
	{
		___justPulledString_48 = value;
	}

	inline static int32_t get_offset_of_bowUpJoint1_51() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___bowUpJoint1_51)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowUpJoint1_51() const { return ___bowUpJoint1_51; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowUpJoint1_51() { return &___bowUpJoint1_51; }
	inline void set_bowUpJoint1_51(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowUpJoint1_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowUpJoint1_51), (void*)value);
	}

	inline static int32_t get_offset_of_bowUpJoint2_52() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___bowUpJoint2_52)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowUpJoint2_52() const { return ___bowUpJoint2_52; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowUpJoint2_52() { return &___bowUpJoint2_52; }
	inline void set_bowUpJoint2_52(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowUpJoint2_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowUpJoint2_52), (void*)value);
	}

	inline static int32_t get_offset_of_bowUpJoint3_53() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___bowUpJoint3_53)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowUpJoint3_53() const { return ___bowUpJoint3_53; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowUpJoint3_53() { return &___bowUpJoint3_53; }
	inline void set_bowUpJoint3_53(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowUpJoint3_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowUpJoint3_53), (void*)value);
	}

	inline static int32_t get_offset_of_bowDownJoint1_54() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___bowDownJoint1_54)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowDownJoint1_54() const { return ___bowDownJoint1_54; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowDownJoint1_54() { return &___bowDownJoint1_54; }
	inline void set_bowDownJoint1_54(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowDownJoint1_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowDownJoint1_54), (void*)value);
	}

	inline static int32_t get_offset_of_bowDownJoint2_55() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___bowDownJoint2_55)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowDownJoint2_55() const { return ___bowDownJoint2_55; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowDownJoint2_55() { return &___bowDownJoint2_55; }
	inline void set_bowDownJoint2_55(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowDownJoint2_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowDownJoint2_55), (void*)value);
	}

	inline static int32_t get_offset_of_bowDownJoint3_56() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___bowDownJoint3_56)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowDownJoint3_56() const { return ___bowDownJoint3_56; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowDownJoint3_56() { return &___bowDownJoint3_56; }
	inline void set_bowDownJoint3_56(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowDownJoint3_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowDownJoint3_56), (void*)value);
	}

	inline static int32_t get_offset_of_bowStringPoint_57() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___bowStringPoint_57)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_bowStringPoint_57() const { return ___bowStringPoint_57; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_bowStringPoint_57() { return &___bowStringPoint_57; }
	inline void set_bowStringPoint_57(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___bowStringPoint_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowStringPoint_57), (void*)value);
	}

	inline static int32_t get_offset_of_joint1RotateDirectionAxis_58() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___joint1RotateDirectionAxis_58)); }
	inline int32_t get_joint1RotateDirectionAxis_58() const { return ___joint1RotateDirectionAxis_58; }
	inline int32_t* get_address_of_joint1RotateDirectionAxis_58() { return &___joint1RotateDirectionAxis_58; }
	inline void set_joint1RotateDirectionAxis_58(int32_t value)
	{
		___joint1RotateDirectionAxis_58 = value;
	}

	inline static int32_t get_offset_of_joint2RotateDirectionAxis_59() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___joint2RotateDirectionAxis_59)); }
	inline int32_t get_joint2RotateDirectionAxis_59() const { return ___joint2RotateDirectionAxis_59; }
	inline int32_t* get_address_of_joint2RotateDirectionAxis_59() { return &___joint2RotateDirectionAxis_59; }
	inline void set_joint2RotateDirectionAxis_59(int32_t value)
	{
		___joint2RotateDirectionAxis_59 = value;
	}

	inline static int32_t get_offset_of_joint3RotateDirectionAxis_60() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___joint3RotateDirectionAxis_60)); }
	inline int32_t get_joint3RotateDirectionAxis_60() const { return ___joint3RotateDirectionAxis_60; }
	inline int32_t* get_address_of_joint3RotateDirectionAxis_60() { return &___joint3RotateDirectionAxis_60; }
	inline void set_joint3RotateDirectionAxis_60(int32_t value)
	{
		___joint3RotateDirectionAxis_60 = value;
	}

	inline static int32_t get_offset_of_bendAngleLimitFirstJoints_61() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___bendAngleLimitFirstJoints_61)); }
	inline float get_bendAngleLimitFirstJoints_61() const { return ___bendAngleLimitFirstJoints_61; }
	inline float* get_address_of_bendAngleLimitFirstJoints_61() { return &___bendAngleLimitFirstJoints_61; }
	inline void set_bendAngleLimitFirstJoints_61(float value)
	{
		___bendAngleLimitFirstJoints_61 = value;
	}

	inline static int32_t get_offset_of_bendAngleLimitSecondJoints_62() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___bendAngleLimitSecondJoints_62)); }
	inline float get_bendAngleLimitSecondJoints_62() const { return ___bendAngleLimitSecondJoints_62; }
	inline float* get_address_of_bendAngleLimitSecondJoints_62() { return &___bendAngleLimitSecondJoints_62; }
	inline void set_bendAngleLimitSecondJoints_62(float value)
	{
		___bendAngleLimitSecondJoints_62 = value;
	}

	inline static int32_t get_offset_of_bendAngleLimitThirdJoints_63() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___bendAngleLimitThirdJoints_63)); }
	inline float get_bendAngleLimitThirdJoints_63() const { return ___bendAngleLimitThirdJoints_63; }
	inline float* get_address_of_bendAngleLimitThirdJoints_63() { return &___bendAngleLimitThirdJoints_63; }
	inline void set_bendAngleLimitThirdJoints_63(float value)
	{
		___bendAngleLimitThirdJoints_63 = value;
	}

	inline static int32_t get_offset_of_inverseBend_64() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___inverseBend_64)); }
	inline bool get_inverseBend_64() const { return ___inverseBend_64; }
	inline bool* get_address_of_inverseBend_64() { return &___inverseBend_64; }
	inline void set_inverseBend_64(bool value)
	{
		___inverseBend_64 = value;
	}

	inline static int32_t get_offset_of_stringMoveDirectionAxis_65() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stringMoveDirectionAxis_65)); }
	inline int32_t get_stringMoveDirectionAxis_65() const { return ___stringMoveDirectionAxis_65; }
	inline int32_t* get_address_of_stringMoveDirectionAxis_65() { return &___stringMoveDirectionAxis_65; }
	inline void set_stringMoveDirectionAxis_65(int32_t value)
	{
		___stringMoveDirectionAxis_65 = value;
	}

	inline static int32_t get_offset_of_stringMoveAmount_66() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stringMoveAmount_66)); }
	inline float get_stringMoveAmount_66() const { return ___stringMoveAmount_66; }
	inline float* get_address_of_stringMoveAmount_66() { return &___stringMoveAmount_66; }
	inline void set_stringMoveAmount_66(float value)
	{
		___stringMoveAmount_66 = value;
	}

	inline static int32_t get_offset_of_stringMoveTime_67() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stringMoveTime_67)); }
	inline float get_stringMoveTime_67() const { return ___stringMoveTime_67; }
	inline float* get_address_of_stringMoveTime_67() { return &___stringMoveTime_67; }
	inline void set_stringMoveTime_67(float value)
	{
		___stringMoveTime_67 = value;
	}

	inline static int32_t get_offset_of_stringRetractTime_68() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stringRetractTime_68)); }
	inline float get_stringRetractTime_68() const { return ___stringRetractTime_68; }
	inline float* get_address_of_stringRetractTime_68() { return &___stringRetractTime_68; }
	inline void set_stringRetractTime_68(float value)
	{
		___stringRetractTime_68 = value;
	}

	inline static int32_t get_offset_of_inversePull_69() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___inversePull_69)); }
	inline bool get_inversePull_69() const { return ___inversePull_69; }
	inline bool* get_address_of_inversePull_69() { return &___inversePull_69; }
	inline void set_inversePull_69(bool value)
	{
		___inversePull_69 = value;
	}

	inline static int32_t get_offset_of_stringMovementChoice_70() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stringMovementChoice_70)); }
	inline int32_t get_stringMovementChoice_70() const { return ___stringMovementChoice_70; }
	inline int32_t* get_address_of_stringMovementChoice_70() { return &___stringMovementChoice_70; }
	inline void set_stringMovementChoice_70(int32_t value)
	{
		___stringMovementChoice_70 = value;
	}

	inline static int32_t get_offset_of_stringRetractChoice_71() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stringRetractChoice_71)); }
	inline int32_t get_stringRetractChoice_71() const { return ___stringRetractChoice_71; }
	inline int32_t* get_address_of_stringRetractChoice_71() { return &___stringRetractChoice_71; }
	inline void set_stringRetractChoice_71(int32_t value)
	{
		___stringRetractChoice_71 = value;
	}

	inline static int32_t get_offset_of_maxStringStrength_72() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___maxStringStrength_72)); }
	inline float get_maxStringStrength_72() const { return ___maxStringStrength_72; }
	inline float* get_address_of_maxStringStrength_72() { return &___maxStringStrength_72; }
	inline void set_maxStringStrength_72(float value)
	{
		___maxStringStrength_72 = value;
	}

	inline static int32_t get_offset_of_projectileAccuracy_73() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___projectileAccuracy_73)); }
	inline int32_t get_projectileAccuracy_73() const { return ___projectileAccuracy_73; }
	inline int32_t* get_address_of_projectileAccuracy_73() { return &___projectileAccuracy_73; }
	inline void set_projectileAccuracy_73(int32_t value)
	{
		___projectileAccuracy_73 = value;
	}

	inline static int32_t get_offset_of_projectile_74() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___projectile_74)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectile_74() const { return ___projectile_74; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectile_74() { return &___projectile_74; }
	inline void set_projectile_74(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectile_74 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectile_74), (void*)value);
	}

	inline static int32_t get_offset_of_projectilePoolSize_75() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___projectilePoolSize_75)); }
	inline int32_t get_projectilePoolSize_75() const { return ___projectilePoolSize_75; }
	inline int32_t* get_address_of_projectilePoolSize_75() { return &___projectilePoolSize_75; }
	inline void set_projectilePoolSize_75(int32_t value)
	{
		___projectilePoolSize_75 = value;
	}

	inline static int32_t get_offset_of_projectileHoldPosOffset_76() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___projectileHoldPosOffset_76)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_projectileHoldPosOffset_76() const { return ___projectileHoldPosOffset_76; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_projectileHoldPosOffset_76() { return &___projectileHoldPosOffset_76; }
	inline void set_projectileHoldPosOffset_76(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___projectileHoldPosOffset_76 = value;
	}

	inline static int32_t get_offset_of_isPosOffsetLocal_77() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___isPosOffsetLocal_77)); }
	inline bool get_isPosOffsetLocal_77() const { return ___isPosOffsetLocal_77; }
	inline bool* get_address_of_isPosOffsetLocal_77() { return &___isPosOffsetLocal_77; }
	inline void set_isPosOffsetLocal_77(bool value)
	{
		___isPosOffsetLocal_77 = value;
	}

	inline static int32_t get_offset_of_projectileHoldRotOffset_78() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___projectileHoldRotOffset_78)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_projectileHoldRotOffset_78() const { return ___projectileHoldRotOffset_78; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_projectileHoldRotOffset_78() { return &___projectileHoldRotOffset_78; }
	inline void set_projectileHoldRotOffset_78(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___projectileHoldRotOffset_78 = value;
	}

	inline static int32_t get_offset_of_projectileForwardAxis_79() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___projectileForwardAxis_79)); }
	inline int32_t get_projectileForwardAxis_79() const { return ___projectileForwardAxis_79; }
	inline int32_t* get_address_of_projectileForwardAxis_79() { return &___projectileForwardAxis_79; }
	inline void set_projectileForwardAxis_79(int32_t value)
	{
		___projectileForwardAxis_79 = value;
	}

	inline static int32_t get_offset_of_soundVolume_80() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___soundVolume_80)); }
	inline float get_soundVolume_80() const { return ___soundVolume_80; }
	inline float* get_address_of_soundVolume_80() { return &___soundVolume_80; }
	inline void set_soundVolume_80(float value)
	{
		___soundVolume_80 = value;
	}

	inline static int32_t get_offset_of_pullSound_81() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___pullSound_81)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_pullSound_81() const { return ___pullSound_81; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_pullSound_81() { return &___pullSound_81; }
	inline void set_pullSound_81(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___pullSound_81 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pullSound_81), (void*)value);
	}

	inline static int32_t get_offset_of_retractSound_82() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___retractSound_82)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_retractSound_82() const { return ___retractSound_82; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_retractSound_82() { return &___retractSound_82; }
	inline void set_retractSound_82(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___retractSound_82 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___retractSound_82), (void*)value);
	}

	inline static int32_t get_offset_of_stressEffectOnSound_83() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___stressEffectOnSound_83)); }
	inline bool get_stressEffectOnSound_83() const { return ___stressEffectOnSound_83; }
	inline bool* get_address_of_stressEffectOnSound_83() { return &___stressEffectOnSound_83; }
	inline void set_stressEffectOnSound_83(bool value)
	{
		___stressEffectOnSound_83 = value;
	}

	inline static int32_t get_offset_of_x_84() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___x_84)); }
	inline float get_x_84() const { return ___x_84; }
	inline float* get_address_of_x_84() { return &___x_84; }
	inline void set_x_84(float value)
	{
		___x_84 = value;
	}

	inline static int32_t get_offset_of_y_85() { return static_cast<int32_t>(offsetof(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA, ___y_85)); }
	inline float get_y_85() const { return ___y_85; }
	inline float* get_address_of_y_85() { return &___y_85; }
	inline void set_y_85(float value)
	{
		___y_85 = value;
	}
};


// StandardizedProjectile
struct  StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439  : public MonoBehaviourPun_t41335C3B2B6804088B520155103E3A0EEA4F814A
{
public:
	// UnityEngine.Rigidbody StandardizedProjectile::rigid
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___rigid_5;
	// StandardizedBow StandardizedProjectile::bowScript
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA * ___bowScript_6;
	// UnityEngine.Transform StandardizedProjectile::quiver
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___quiver_7;
	// UnityEngine.BoxCollider StandardizedProjectile::boxCollider
	BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * ___boxCollider_8;
	// UnityEngine.AudioSource StandardizedProjectile::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_9;
	// System.Single StandardizedProjectile::currentTime
	float ___currentTime_10;
	// System.Boolean StandardizedProjectile::collisionHappened
	bool ___collisionHappened_11;
	// System.Single StandardizedProjectile::timeLimitToDePool
	float ___timeLimitToDePool_12;
	// System.Single StandardizedProjectile::effectOfVelocityOnParticleEmission
	float ___effectOfVelocityOnParticleEmission_13;
	// UnityEngine.GameObject StandardizedProjectile::projectileHitParticleFlesh
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectileHitParticleFlesh_14;
	// UnityEngine.GameObject StandardizedProjectile::projectileHitParticleWood
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectileHitParticleWood_15;
	// UnityEngine.GameObject StandardizedProjectile::projectileHitParticleStone
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectileHitParticleStone_16;
	// UnityEngine.GameObject StandardizedProjectile::projectileHitParticleMetal
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectileHitParticleMetal_17;
	// UnityEngine.AudioClip StandardizedProjectile::hitSoundFlesh
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___hitSoundFlesh_18;
	// UnityEngine.AudioClip StandardizedProjectile::hitSoundWood
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___hitSoundWood_19;
	// UnityEngine.AudioClip StandardizedProjectile::hitSoundStone
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___hitSoundStone_20;
	// UnityEngine.AudioClip StandardizedProjectile::hitSoundMetal
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___hitSoundMetal_21;
	// System.String StandardizedProjectile::fleshDetectionTagToHash
	String_t* ___fleshDetectionTagToHash_22;
	// System.String StandardizedProjectile::woodDetectionTagToHash
	String_t* ___woodDetectionTagToHash_23;
	// System.String StandardizedProjectile::stoneDetectionTagToHash
	String_t* ___stoneDetectionTagToHash_24;
	// System.String StandardizedProjectile::metalDetectionTagToHash
	String_t* ___metalDetectionTagToHash_25;
	// UnityEngine.ParticleSystem StandardizedProjectile::fleshHitPS
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___fleshHitPS_32;
	// UnityEngine.ParticleSystem StandardizedProjectile::woodHitPS
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___woodHitPS_33;
	// UnityEngine.ParticleSystem StandardizedProjectile::stoneHitPS
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___stoneHitPS_34;
	// UnityEngine.ParticleSystem StandardizedProjectile::metalHitPS
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___metalHitPS_35;
	// UnityEngine.GameObject StandardizedProjectile::fleshParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___fleshParticleInstance_36;
	// UnityEngine.GameObject StandardizedProjectile::woodParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___woodParticleInstance_37;
	// UnityEngine.GameObject StandardizedProjectile::stoneParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stoneParticleInstance_38;
	// UnityEngine.GameObject StandardizedProjectile::metalParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___metalParticleInstance_39;
	// UnityEngine.GameObject StandardizedProjectile::currentParticleInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___currentParticleInstance_40;
	// UnityEngine.Transform StandardizedProjectile::fleshParticleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___fleshParticleTransform_41;
	// UnityEngine.Transform StandardizedProjectile::woodParticleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___woodParticleTransform_42;
	// UnityEngine.Transform StandardizedProjectile::stoneParticleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___stoneParticleTransform_43;
	// UnityEngine.Transform StandardizedProjectile::metalParticleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___metalParticleTransform_44;
	// UnityEngine.ParticleSystem_Burst StandardizedProjectile::burstControl
	Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB  ___burstControl_45;

public:
	inline static int32_t get_offset_of_rigid_5() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___rigid_5)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_rigid_5() const { return ___rigid_5; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_rigid_5() { return &___rigid_5; }
	inline void set_rigid_5(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___rigid_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rigid_5), (void*)value);
	}

	inline static int32_t get_offset_of_bowScript_6() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___bowScript_6)); }
	inline StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA * get_bowScript_6() const { return ___bowScript_6; }
	inline StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA ** get_address_of_bowScript_6() { return &___bowScript_6; }
	inline void set_bowScript_6(StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA * value)
	{
		___bowScript_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bowScript_6), (void*)value);
	}

	inline static int32_t get_offset_of_quiver_7() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___quiver_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_quiver_7() const { return ___quiver_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_quiver_7() { return &___quiver_7; }
	inline void set_quiver_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___quiver_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___quiver_7), (void*)value);
	}

	inline static int32_t get_offset_of_boxCollider_8() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___boxCollider_8)); }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * get_boxCollider_8() const { return ___boxCollider_8; }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA ** get_address_of_boxCollider_8() { return &___boxCollider_8; }
	inline void set_boxCollider_8(BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * value)
	{
		___boxCollider_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___boxCollider_8), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_9() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___audioSource_9)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_9() const { return ___audioSource_9; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_9() { return &___audioSource_9; }
	inline void set_audioSource_9(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_9), (void*)value);
	}

	inline static int32_t get_offset_of_currentTime_10() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___currentTime_10)); }
	inline float get_currentTime_10() const { return ___currentTime_10; }
	inline float* get_address_of_currentTime_10() { return &___currentTime_10; }
	inline void set_currentTime_10(float value)
	{
		___currentTime_10 = value;
	}

	inline static int32_t get_offset_of_collisionHappened_11() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___collisionHappened_11)); }
	inline bool get_collisionHappened_11() const { return ___collisionHappened_11; }
	inline bool* get_address_of_collisionHappened_11() { return &___collisionHappened_11; }
	inline void set_collisionHappened_11(bool value)
	{
		___collisionHappened_11 = value;
	}

	inline static int32_t get_offset_of_timeLimitToDePool_12() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___timeLimitToDePool_12)); }
	inline float get_timeLimitToDePool_12() const { return ___timeLimitToDePool_12; }
	inline float* get_address_of_timeLimitToDePool_12() { return &___timeLimitToDePool_12; }
	inline void set_timeLimitToDePool_12(float value)
	{
		___timeLimitToDePool_12 = value;
	}

	inline static int32_t get_offset_of_effectOfVelocityOnParticleEmission_13() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___effectOfVelocityOnParticleEmission_13)); }
	inline float get_effectOfVelocityOnParticleEmission_13() const { return ___effectOfVelocityOnParticleEmission_13; }
	inline float* get_address_of_effectOfVelocityOnParticleEmission_13() { return &___effectOfVelocityOnParticleEmission_13; }
	inline void set_effectOfVelocityOnParticleEmission_13(float value)
	{
		___effectOfVelocityOnParticleEmission_13 = value;
	}

	inline static int32_t get_offset_of_projectileHitParticleFlesh_14() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___projectileHitParticleFlesh_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectileHitParticleFlesh_14() const { return ___projectileHitParticleFlesh_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectileHitParticleFlesh_14() { return &___projectileHitParticleFlesh_14; }
	inline void set_projectileHitParticleFlesh_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectileHitParticleFlesh_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectileHitParticleFlesh_14), (void*)value);
	}

	inline static int32_t get_offset_of_projectileHitParticleWood_15() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___projectileHitParticleWood_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectileHitParticleWood_15() const { return ___projectileHitParticleWood_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectileHitParticleWood_15() { return &___projectileHitParticleWood_15; }
	inline void set_projectileHitParticleWood_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectileHitParticleWood_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectileHitParticleWood_15), (void*)value);
	}

	inline static int32_t get_offset_of_projectileHitParticleStone_16() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___projectileHitParticleStone_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectileHitParticleStone_16() const { return ___projectileHitParticleStone_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectileHitParticleStone_16() { return &___projectileHitParticleStone_16; }
	inline void set_projectileHitParticleStone_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectileHitParticleStone_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectileHitParticleStone_16), (void*)value);
	}

	inline static int32_t get_offset_of_projectileHitParticleMetal_17() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___projectileHitParticleMetal_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectileHitParticleMetal_17() const { return ___projectileHitParticleMetal_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectileHitParticleMetal_17() { return &___projectileHitParticleMetal_17; }
	inline void set_projectileHitParticleMetal_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectileHitParticleMetal_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___projectileHitParticleMetal_17), (void*)value);
	}

	inline static int32_t get_offset_of_hitSoundFlesh_18() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___hitSoundFlesh_18)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_hitSoundFlesh_18() const { return ___hitSoundFlesh_18; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_hitSoundFlesh_18() { return &___hitSoundFlesh_18; }
	inline void set_hitSoundFlesh_18(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___hitSoundFlesh_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitSoundFlesh_18), (void*)value);
	}

	inline static int32_t get_offset_of_hitSoundWood_19() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___hitSoundWood_19)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_hitSoundWood_19() const { return ___hitSoundWood_19; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_hitSoundWood_19() { return &___hitSoundWood_19; }
	inline void set_hitSoundWood_19(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___hitSoundWood_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitSoundWood_19), (void*)value);
	}

	inline static int32_t get_offset_of_hitSoundStone_20() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___hitSoundStone_20)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_hitSoundStone_20() const { return ___hitSoundStone_20; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_hitSoundStone_20() { return &___hitSoundStone_20; }
	inline void set_hitSoundStone_20(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___hitSoundStone_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitSoundStone_20), (void*)value);
	}

	inline static int32_t get_offset_of_hitSoundMetal_21() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___hitSoundMetal_21)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_hitSoundMetal_21() const { return ___hitSoundMetal_21; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_hitSoundMetal_21() { return &___hitSoundMetal_21; }
	inline void set_hitSoundMetal_21(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___hitSoundMetal_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitSoundMetal_21), (void*)value);
	}

	inline static int32_t get_offset_of_fleshDetectionTagToHash_22() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___fleshDetectionTagToHash_22)); }
	inline String_t* get_fleshDetectionTagToHash_22() const { return ___fleshDetectionTagToHash_22; }
	inline String_t** get_address_of_fleshDetectionTagToHash_22() { return &___fleshDetectionTagToHash_22; }
	inline void set_fleshDetectionTagToHash_22(String_t* value)
	{
		___fleshDetectionTagToHash_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fleshDetectionTagToHash_22), (void*)value);
	}

	inline static int32_t get_offset_of_woodDetectionTagToHash_23() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___woodDetectionTagToHash_23)); }
	inline String_t* get_woodDetectionTagToHash_23() const { return ___woodDetectionTagToHash_23; }
	inline String_t** get_address_of_woodDetectionTagToHash_23() { return &___woodDetectionTagToHash_23; }
	inline void set_woodDetectionTagToHash_23(String_t* value)
	{
		___woodDetectionTagToHash_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___woodDetectionTagToHash_23), (void*)value);
	}

	inline static int32_t get_offset_of_stoneDetectionTagToHash_24() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___stoneDetectionTagToHash_24)); }
	inline String_t* get_stoneDetectionTagToHash_24() const { return ___stoneDetectionTagToHash_24; }
	inline String_t** get_address_of_stoneDetectionTagToHash_24() { return &___stoneDetectionTagToHash_24; }
	inline void set_stoneDetectionTagToHash_24(String_t* value)
	{
		___stoneDetectionTagToHash_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stoneDetectionTagToHash_24), (void*)value);
	}

	inline static int32_t get_offset_of_metalDetectionTagToHash_25() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___metalDetectionTagToHash_25)); }
	inline String_t* get_metalDetectionTagToHash_25() const { return ___metalDetectionTagToHash_25; }
	inline String_t** get_address_of_metalDetectionTagToHash_25() { return &___metalDetectionTagToHash_25; }
	inline void set_metalDetectionTagToHash_25(String_t* value)
	{
		___metalDetectionTagToHash_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___metalDetectionTagToHash_25), (void*)value);
	}

	inline static int32_t get_offset_of_fleshHitPS_32() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___fleshHitPS_32)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_fleshHitPS_32() const { return ___fleshHitPS_32; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_fleshHitPS_32() { return &___fleshHitPS_32; }
	inline void set_fleshHitPS_32(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___fleshHitPS_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fleshHitPS_32), (void*)value);
	}

	inline static int32_t get_offset_of_woodHitPS_33() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___woodHitPS_33)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_woodHitPS_33() const { return ___woodHitPS_33; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_woodHitPS_33() { return &___woodHitPS_33; }
	inline void set_woodHitPS_33(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___woodHitPS_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___woodHitPS_33), (void*)value);
	}

	inline static int32_t get_offset_of_stoneHitPS_34() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___stoneHitPS_34)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_stoneHitPS_34() const { return ___stoneHitPS_34; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_stoneHitPS_34() { return &___stoneHitPS_34; }
	inline void set_stoneHitPS_34(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___stoneHitPS_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stoneHitPS_34), (void*)value);
	}

	inline static int32_t get_offset_of_metalHitPS_35() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___metalHitPS_35)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_metalHitPS_35() const { return ___metalHitPS_35; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_metalHitPS_35() { return &___metalHitPS_35; }
	inline void set_metalHitPS_35(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___metalHitPS_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___metalHitPS_35), (void*)value);
	}

	inline static int32_t get_offset_of_fleshParticleInstance_36() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___fleshParticleInstance_36)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_fleshParticleInstance_36() const { return ___fleshParticleInstance_36; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_fleshParticleInstance_36() { return &___fleshParticleInstance_36; }
	inline void set_fleshParticleInstance_36(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___fleshParticleInstance_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fleshParticleInstance_36), (void*)value);
	}

	inline static int32_t get_offset_of_woodParticleInstance_37() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___woodParticleInstance_37)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_woodParticleInstance_37() const { return ___woodParticleInstance_37; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_woodParticleInstance_37() { return &___woodParticleInstance_37; }
	inline void set_woodParticleInstance_37(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___woodParticleInstance_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___woodParticleInstance_37), (void*)value);
	}

	inline static int32_t get_offset_of_stoneParticleInstance_38() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___stoneParticleInstance_38)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stoneParticleInstance_38() const { return ___stoneParticleInstance_38; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stoneParticleInstance_38() { return &___stoneParticleInstance_38; }
	inline void set_stoneParticleInstance_38(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stoneParticleInstance_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stoneParticleInstance_38), (void*)value);
	}

	inline static int32_t get_offset_of_metalParticleInstance_39() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___metalParticleInstance_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_metalParticleInstance_39() const { return ___metalParticleInstance_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_metalParticleInstance_39() { return &___metalParticleInstance_39; }
	inline void set_metalParticleInstance_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___metalParticleInstance_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___metalParticleInstance_39), (void*)value);
	}

	inline static int32_t get_offset_of_currentParticleInstance_40() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___currentParticleInstance_40)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_currentParticleInstance_40() const { return ___currentParticleInstance_40; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_currentParticleInstance_40() { return &___currentParticleInstance_40; }
	inline void set_currentParticleInstance_40(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___currentParticleInstance_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentParticleInstance_40), (void*)value);
	}

	inline static int32_t get_offset_of_fleshParticleTransform_41() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___fleshParticleTransform_41)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_fleshParticleTransform_41() const { return ___fleshParticleTransform_41; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_fleshParticleTransform_41() { return &___fleshParticleTransform_41; }
	inline void set_fleshParticleTransform_41(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___fleshParticleTransform_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fleshParticleTransform_41), (void*)value);
	}

	inline static int32_t get_offset_of_woodParticleTransform_42() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___woodParticleTransform_42)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_woodParticleTransform_42() const { return ___woodParticleTransform_42; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_woodParticleTransform_42() { return &___woodParticleTransform_42; }
	inline void set_woodParticleTransform_42(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___woodParticleTransform_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___woodParticleTransform_42), (void*)value);
	}

	inline static int32_t get_offset_of_stoneParticleTransform_43() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___stoneParticleTransform_43)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_stoneParticleTransform_43() const { return ___stoneParticleTransform_43; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_stoneParticleTransform_43() { return &___stoneParticleTransform_43; }
	inline void set_stoneParticleTransform_43(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___stoneParticleTransform_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stoneParticleTransform_43), (void*)value);
	}

	inline static int32_t get_offset_of_metalParticleTransform_44() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___metalParticleTransform_44)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_metalParticleTransform_44() const { return ___metalParticleTransform_44; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_metalParticleTransform_44() { return &___metalParticleTransform_44; }
	inline void set_metalParticleTransform_44(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___metalParticleTransform_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___metalParticleTransform_44), (void*)value);
	}

	inline static int32_t get_offset_of_burstControl_45() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439, ___burstControl_45)); }
	inline Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB  get_burstControl_45() const { return ___burstControl_45; }
	inline Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB * get_address_of_burstControl_45() { return &___burstControl_45; }
	inline void set_burstControl_45(Burst_t5005E443623235F9ECEAD9568FF0D17A7860F9EB  value)
	{
		___burstControl_45 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___burstControl_45))->___m_Count_1))->___m_CurveMin_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___burstControl_45))->___m_Count_1))->___m_CurveMax_3), (void*)NULL);
		#endif
	}
};

struct StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields
{
public:
	// System.Int32 StandardizedProjectile::fleshHash
	int32_t ___fleshHash_26;
	// System.Int32 StandardizedProjectile::woodHash
	int32_t ___woodHash_27;
	// System.Int32 StandardizedProjectile::stoneHash
	int32_t ___stoneHash_28;
	// System.Int32 StandardizedProjectile::metalHash
	int32_t ___metalHash_29;
	// System.Int32 StandardizedProjectile::contactHash
	int32_t ___contactHash_30;
	// System.Int32 StandardizedProjectile::projectileHash
	int32_t ___projectileHash_31;

public:
	inline static int32_t get_offset_of_fleshHash_26() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields, ___fleshHash_26)); }
	inline int32_t get_fleshHash_26() const { return ___fleshHash_26; }
	inline int32_t* get_address_of_fleshHash_26() { return &___fleshHash_26; }
	inline void set_fleshHash_26(int32_t value)
	{
		___fleshHash_26 = value;
	}

	inline static int32_t get_offset_of_woodHash_27() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields, ___woodHash_27)); }
	inline int32_t get_woodHash_27() const { return ___woodHash_27; }
	inline int32_t* get_address_of_woodHash_27() { return &___woodHash_27; }
	inline void set_woodHash_27(int32_t value)
	{
		___woodHash_27 = value;
	}

	inline static int32_t get_offset_of_stoneHash_28() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields, ___stoneHash_28)); }
	inline int32_t get_stoneHash_28() const { return ___stoneHash_28; }
	inline int32_t* get_address_of_stoneHash_28() { return &___stoneHash_28; }
	inline void set_stoneHash_28(int32_t value)
	{
		___stoneHash_28 = value;
	}

	inline static int32_t get_offset_of_metalHash_29() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields, ___metalHash_29)); }
	inline int32_t get_metalHash_29() const { return ___metalHash_29; }
	inline int32_t* get_address_of_metalHash_29() { return &___metalHash_29; }
	inline void set_metalHash_29(int32_t value)
	{
		___metalHash_29 = value;
	}

	inline static int32_t get_offset_of_contactHash_30() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields, ___contactHash_30)); }
	inline int32_t get_contactHash_30() const { return ___contactHash_30; }
	inline int32_t* get_address_of_contactHash_30() { return &___contactHash_30; }
	inline void set_contactHash_30(int32_t value)
	{
		___contactHash_30 = value;
	}

	inline static int32_t get_offset_of_projectileHash_31() { return static_cast<int32_t>(offsetof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields, ___projectileHash_31)); }
	inline int32_t get_projectileHash_31() const { return ___projectileHash_31; }
	inline int32_t* get_address_of_projectileHash_31() { return &___projectileHash_31; }
	inline void set_projectileHash_31(int32_t value)
	{
		___projectileHash_31 = value;
	}
};


// UsernameIn
struct  UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE  : public MonoBehaviourPun_t41335C3B2B6804088B520155103E3A0EEA4F814A
{
public:
	// UnityEngine.UI.Text UsernameIn::NameIn
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___NameIn_5;
	// UnityEngine.GameObject[] UsernameIn::nameObj
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___nameObj_6;
	// System.String UsernameIn::nameStored
	String_t* ___nameStored_8;
	// System.Int32 UsernameIn::countName
	int32_t ___countName_9;
	// UnityEngine.GameObject UsernameIn::statusPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___statusPrefab_10;
	// UnityEngine.GameObject UsernameIn::chatContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___chatContainer_11;
	// Photon.Pun.PhotonView UsernameIn::InputPV
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___InputPV_12;

public:
	inline static int32_t get_offset_of_NameIn_5() { return static_cast<int32_t>(offsetof(UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE, ___NameIn_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_NameIn_5() const { return ___NameIn_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_NameIn_5() { return &___NameIn_5; }
	inline void set_NameIn_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___NameIn_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NameIn_5), (void*)value);
	}

	inline static int32_t get_offset_of_nameObj_6() { return static_cast<int32_t>(offsetof(UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE, ___nameObj_6)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_nameObj_6() const { return ___nameObj_6; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_nameObj_6() { return &___nameObj_6; }
	inline void set_nameObj_6(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___nameObj_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameObj_6), (void*)value);
	}

	inline static int32_t get_offset_of_nameStored_8() { return static_cast<int32_t>(offsetof(UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE, ___nameStored_8)); }
	inline String_t* get_nameStored_8() const { return ___nameStored_8; }
	inline String_t** get_address_of_nameStored_8() { return &___nameStored_8; }
	inline void set_nameStored_8(String_t* value)
	{
		___nameStored_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameStored_8), (void*)value);
	}

	inline static int32_t get_offset_of_countName_9() { return static_cast<int32_t>(offsetof(UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE, ___countName_9)); }
	inline int32_t get_countName_9() const { return ___countName_9; }
	inline int32_t* get_address_of_countName_9() { return &___countName_9; }
	inline void set_countName_9(int32_t value)
	{
		___countName_9 = value;
	}

	inline static int32_t get_offset_of_statusPrefab_10() { return static_cast<int32_t>(offsetof(UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE, ___statusPrefab_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_statusPrefab_10() const { return ___statusPrefab_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_statusPrefab_10() { return &___statusPrefab_10; }
	inline void set_statusPrefab_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___statusPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___statusPrefab_10), (void*)value);
	}

	inline static int32_t get_offset_of_chatContainer_11() { return static_cast<int32_t>(offsetof(UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE, ___chatContainer_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_chatContainer_11() const { return ___chatContainer_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_chatContainer_11() { return &___chatContainer_11; }
	inline void set_chatContainer_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___chatContainer_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chatContainer_11), (void*)value);
	}

	inline static int32_t get_offset_of_InputPV_12() { return static_cast<int32_t>(offsetof(UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE, ___InputPV_12)); }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * get_InputPV_12() const { return ___InputPV_12; }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** get_address_of_InputPV_12() { return &___InputPV_12; }
	inline void set_InputPV_12(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		___InputPV_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InputPV_12), (void*)value);
	}
};

struct UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE_StaticFields
{
public:
	// System.String UsernameIn::username
	String_t* ___username_7;

public:
	inline static int32_t get_offset_of_username_7() { return static_cast<int32_t>(offsetof(UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE_StaticFields, ___username_7)); }
	inline String_t* get_username_7() const { return ___username_7; }
	inline String_t** get_address_of_username_7() { return &___username_7; }
	inline void set_username_7(String_t* value)
	{
		___username_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___username_7), (void*)value);
	}
};


// DelayStartLobbyController
struct  DelayStartLobbyController_t95D7C470AFA59436452FD2A8EFC5548DA42A446B  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// UnityEngine.GameObject DelayStartLobbyController::delayStartButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___delayStartButton_5;
	// UnityEngine.GameObject DelayStartLobbyController::delayCancelButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___delayCancelButton_6;
	// System.Int32 DelayStartLobbyController::roomSize
	int32_t ___roomSize_7;

public:
	inline static int32_t get_offset_of_delayStartButton_5() { return static_cast<int32_t>(offsetof(DelayStartLobbyController_t95D7C470AFA59436452FD2A8EFC5548DA42A446B, ___delayStartButton_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_delayStartButton_5() const { return ___delayStartButton_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_delayStartButton_5() { return &___delayStartButton_5; }
	inline void set_delayStartButton_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___delayStartButton_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delayStartButton_5), (void*)value);
	}

	inline static int32_t get_offset_of_delayCancelButton_6() { return static_cast<int32_t>(offsetof(DelayStartLobbyController_t95D7C470AFA59436452FD2A8EFC5548DA42A446B, ___delayCancelButton_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_delayCancelButton_6() const { return ___delayCancelButton_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_delayCancelButton_6() { return &___delayCancelButton_6; }
	inline void set_delayCancelButton_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___delayCancelButton_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delayCancelButton_6), (void*)value);
	}

	inline static int32_t get_offset_of_roomSize_7() { return static_cast<int32_t>(offsetof(DelayStartLobbyController_t95D7C470AFA59436452FD2A8EFC5548DA42A446B, ___roomSize_7)); }
	inline int32_t get_roomSize_7() const { return ___roomSize_7; }
	inline int32_t* get_address_of_roomSize_7() { return &___roomSize_7; }
	inline void set_roomSize_7(int32_t value)
	{
		___roomSize_7 = value;
	}
};


// DelayStartRoomCOntroller
struct  DelayStartRoomCOntroller_t1FFB79C0F66865A41A3C461E8834718D8944D538  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// System.Int32 DelayStartRoomCOntroller::waitingRoomSceneIndex
	int32_t ___waitingRoomSceneIndex_5;

public:
	inline static int32_t get_offset_of_waitingRoomSceneIndex_5() { return static_cast<int32_t>(offsetof(DelayStartRoomCOntroller_t1FFB79C0F66865A41A3C461E8834718D8944D538, ___waitingRoomSceneIndex_5)); }
	inline int32_t get_waitingRoomSceneIndex_5() const { return ___waitingRoomSceneIndex_5; }
	inline int32_t* get_address_of_waitingRoomSceneIndex_5() { return &___waitingRoomSceneIndex_5; }
	inline void set_waitingRoomSceneIndex_5(int32_t value)
	{
		___waitingRoomSceneIndex_5 = value;
	}
};


// DelayStartWaitingRoomController
struct  DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// Photon.Pun.PhotonView DelayStartWaitingRoomController::myPhotonView
	PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * ___myPhotonView_5;
	// System.Int32 DelayStartWaitingRoomController::multiplayerSceneIndex
	int32_t ___multiplayerSceneIndex_6;
	// System.Int32 DelayStartWaitingRoomController::menuSceneIndex
	int32_t ___menuSceneIndex_7;
	// System.Int32 DelayStartWaitingRoomController::playerCount
	int32_t ___playerCount_8;
	// System.Int32 DelayStartWaitingRoomController::roomSize
	int32_t ___roomSize_9;
	// System.Int32 DelayStartWaitingRoomController::maxPlayersToStart
	int32_t ___maxPlayersToStart_10;
	// System.Boolean DelayStartWaitingRoomController::startingGame
	bool ___startingGame_11;
	// UnityEngine.UI.Text DelayStartWaitingRoomController::playerCountDisplay
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___playerCountDisplay_12;
	// UnityEngine.UI.Text DelayStartWaitingRoomController::timerToStartDisplay
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___timerToStartDisplay_13;
	// System.Boolean DelayStartWaitingRoomController::readyToCount
	bool ___readyToCount_14;
	// System.Boolean DelayStartWaitingRoomController::readyToStart
	bool ___readyToStart_15;
	// System.Single DelayStartWaitingRoomController::timerToStartGame
	float ___timerToStartGame_16;
	// System.Single DelayStartWaitingRoomController::notFullGameTimer
	float ___notFullGameTimer_17;
	// System.Single DelayStartWaitingRoomController::fullGameTimer
	float ___fullGameTimer_18;
	// System.Single DelayStartWaitingRoomController::maxWaitTime
	float ___maxWaitTime_19;
	// System.Single DelayStartWaitingRoomController::maxFullGameWaitTime
	float ___maxFullGameWaitTime_20;
	// UsernameIn DelayStartWaitingRoomController::nameIn
	UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE * ___nameIn_21;
	// System.Collections.ArrayList DelayStartWaitingRoomController::nameList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___nameList_22;

public:
	inline static int32_t get_offset_of_myPhotonView_5() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___myPhotonView_5)); }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * get_myPhotonView_5() const { return ___myPhotonView_5; }
	inline PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B ** get_address_of_myPhotonView_5() { return &___myPhotonView_5; }
	inline void set_myPhotonView_5(PhotonView_t9781C6CA59BA006553D186D4FC011AECA4C9BE0B * value)
	{
		___myPhotonView_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myPhotonView_5), (void*)value);
	}

	inline static int32_t get_offset_of_multiplayerSceneIndex_6() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___multiplayerSceneIndex_6)); }
	inline int32_t get_multiplayerSceneIndex_6() const { return ___multiplayerSceneIndex_6; }
	inline int32_t* get_address_of_multiplayerSceneIndex_6() { return &___multiplayerSceneIndex_6; }
	inline void set_multiplayerSceneIndex_6(int32_t value)
	{
		___multiplayerSceneIndex_6 = value;
	}

	inline static int32_t get_offset_of_menuSceneIndex_7() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___menuSceneIndex_7)); }
	inline int32_t get_menuSceneIndex_7() const { return ___menuSceneIndex_7; }
	inline int32_t* get_address_of_menuSceneIndex_7() { return &___menuSceneIndex_7; }
	inline void set_menuSceneIndex_7(int32_t value)
	{
		___menuSceneIndex_7 = value;
	}

	inline static int32_t get_offset_of_playerCount_8() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___playerCount_8)); }
	inline int32_t get_playerCount_8() const { return ___playerCount_8; }
	inline int32_t* get_address_of_playerCount_8() { return &___playerCount_8; }
	inline void set_playerCount_8(int32_t value)
	{
		___playerCount_8 = value;
	}

	inline static int32_t get_offset_of_roomSize_9() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___roomSize_9)); }
	inline int32_t get_roomSize_9() const { return ___roomSize_9; }
	inline int32_t* get_address_of_roomSize_9() { return &___roomSize_9; }
	inline void set_roomSize_9(int32_t value)
	{
		___roomSize_9 = value;
	}

	inline static int32_t get_offset_of_maxPlayersToStart_10() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___maxPlayersToStart_10)); }
	inline int32_t get_maxPlayersToStart_10() const { return ___maxPlayersToStart_10; }
	inline int32_t* get_address_of_maxPlayersToStart_10() { return &___maxPlayersToStart_10; }
	inline void set_maxPlayersToStart_10(int32_t value)
	{
		___maxPlayersToStart_10 = value;
	}

	inline static int32_t get_offset_of_startingGame_11() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___startingGame_11)); }
	inline bool get_startingGame_11() const { return ___startingGame_11; }
	inline bool* get_address_of_startingGame_11() { return &___startingGame_11; }
	inline void set_startingGame_11(bool value)
	{
		___startingGame_11 = value;
	}

	inline static int32_t get_offset_of_playerCountDisplay_12() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___playerCountDisplay_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_playerCountDisplay_12() const { return ___playerCountDisplay_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_playerCountDisplay_12() { return &___playerCountDisplay_12; }
	inline void set_playerCountDisplay_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___playerCountDisplay_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerCountDisplay_12), (void*)value);
	}

	inline static int32_t get_offset_of_timerToStartDisplay_13() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___timerToStartDisplay_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_timerToStartDisplay_13() const { return ___timerToStartDisplay_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_timerToStartDisplay_13() { return &___timerToStartDisplay_13; }
	inline void set_timerToStartDisplay_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___timerToStartDisplay_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timerToStartDisplay_13), (void*)value);
	}

	inline static int32_t get_offset_of_readyToCount_14() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___readyToCount_14)); }
	inline bool get_readyToCount_14() const { return ___readyToCount_14; }
	inline bool* get_address_of_readyToCount_14() { return &___readyToCount_14; }
	inline void set_readyToCount_14(bool value)
	{
		___readyToCount_14 = value;
	}

	inline static int32_t get_offset_of_readyToStart_15() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___readyToStart_15)); }
	inline bool get_readyToStart_15() const { return ___readyToStart_15; }
	inline bool* get_address_of_readyToStart_15() { return &___readyToStart_15; }
	inline void set_readyToStart_15(bool value)
	{
		___readyToStart_15 = value;
	}

	inline static int32_t get_offset_of_timerToStartGame_16() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___timerToStartGame_16)); }
	inline float get_timerToStartGame_16() const { return ___timerToStartGame_16; }
	inline float* get_address_of_timerToStartGame_16() { return &___timerToStartGame_16; }
	inline void set_timerToStartGame_16(float value)
	{
		___timerToStartGame_16 = value;
	}

	inline static int32_t get_offset_of_notFullGameTimer_17() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___notFullGameTimer_17)); }
	inline float get_notFullGameTimer_17() const { return ___notFullGameTimer_17; }
	inline float* get_address_of_notFullGameTimer_17() { return &___notFullGameTimer_17; }
	inline void set_notFullGameTimer_17(float value)
	{
		___notFullGameTimer_17 = value;
	}

	inline static int32_t get_offset_of_fullGameTimer_18() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___fullGameTimer_18)); }
	inline float get_fullGameTimer_18() const { return ___fullGameTimer_18; }
	inline float* get_address_of_fullGameTimer_18() { return &___fullGameTimer_18; }
	inline void set_fullGameTimer_18(float value)
	{
		___fullGameTimer_18 = value;
	}

	inline static int32_t get_offset_of_maxWaitTime_19() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___maxWaitTime_19)); }
	inline float get_maxWaitTime_19() const { return ___maxWaitTime_19; }
	inline float* get_address_of_maxWaitTime_19() { return &___maxWaitTime_19; }
	inline void set_maxWaitTime_19(float value)
	{
		___maxWaitTime_19 = value;
	}

	inline static int32_t get_offset_of_maxFullGameWaitTime_20() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___maxFullGameWaitTime_20)); }
	inline float get_maxFullGameWaitTime_20() const { return ___maxFullGameWaitTime_20; }
	inline float* get_address_of_maxFullGameWaitTime_20() { return &___maxFullGameWaitTime_20; }
	inline void set_maxFullGameWaitTime_20(float value)
	{
		___maxFullGameWaitTime_20 = value;
	}

	inline static int32_t get_offset_of_nameIn_21() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___nameIn_21)); }
	inline UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE * get_nameIn_21() const { return ___nameIn_21; }
	inline UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE ** get_address_of_nameIn_21() { return &___nameIn_21; }
	inline void set_nameIn_21(UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE * value)
	{
		___nameIn_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameIn_21), (void*)value);
	}

	inline static int32_t get_offset_of_nameList_22() { return static_cast<int32_t>(offsetof(DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF, ___nameList_22)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_nameList_22() const { return ___nameList_22; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_nameList_22() { return &___nameList_22; }
	inline void set_nameList_22(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___nameList_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameList_22), (void*)value);
	}
};


// NetworkController
struct  NetworkController_t963EBD615A1A309D71650B017C05456D1AF2B1ED  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:

public:
};


// Photon.Pun.Demo.Asteroids.AsteroidsGameManager
struct  AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// UnityEngine.UI.Text Photon.Pun.Demo.Asteroids.AsteroidsGameManager::InfoText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___InfoText_6;
	// UnityEngine.GameObject[] Photon.Pun.Demo.Asteroids.AsteroidsGameManager::AsteroidPrefabs
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___AsteroidPrefabs_7;

public:
	inline static int32_t get_offset_of_InfoText_6() { return static_cast<int32_t>(offsetof(AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4, ___InfoText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_InfoText_6() const { return ___InfoText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_InfoText_6() { return &___InfoText_6; }
	inline void set_InfoText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___InfoText_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InfoText_6), (void*)value);
	}

	inline static int32_t get_offset_of_AsteroidPrefabs_7() { return static_cast<int32_t>(offsetof(AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4, ___AsteroidPrefabs_7)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_AsteroidPrefabs_7() const { return ___AsteroidPrefabs_7; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_AsteroidPrefabs_7() { return &___AsteroidPrefabs_7; }
	inline void set_AsteroidPrefabs_7(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___AsteroidPrefabs_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AsteroidPrefabs_7), (void*)value);
	}
};

struct AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4_StaticFields
{
public:
	// Photon.Pun.Demo.Asteroids.AsteroidsGameManager Photon.Pun.Demo.Asteroids.AsteroidsGameManager::Instance
	AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4 * ___Instance_5;

public:
	inline static int32_t get_offset_of_Instance_5() { return static_cast<int32_t>(offsetof(AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4_StaticFields, ___Instance_5)); }
	inline AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4 * get_Instance_5() const { return ___Instance_5; }
	inline AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4 ** get_address_of_Instance_5() { return &___Instance_5; }
	inline void set_Instance_5(AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4 * value)
	{
		___Instance_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Instance_5), (void*)value);
	}
};


// Photon.Pun.Demo.Asteroids.LobbyMainPanel
struct  LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// UnityEngine.GameObject Photon.Pun.Demo.Asteroids.LobbyMainPanel::LoginPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___LoginPanel_5;
	// UnityEngine.UI.InputField Photon.Pun.Demo.Asteroids.LobbyMainPanel::PlayerNameInput
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___PlayerNameInput_6;
	// UnityEngine.GameObject Photon.Pun.Demo.Asteroids.LobbyMainPanel::SelectionPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___SelectionPanel_7;
	// UnityEngine.GameObject Photon.Pun.Demo.Asteroids.LobbyMainPanel::CreateRoomPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___CreateRoomPanel_8;
	// UnityEngine.UI.InputField Photon.Pun.Demo.Asteroids.LobbyMainPanel::RoomNameInputField
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___RoomNameInputField_9;
	// UnityEngine.UI.InputField Photon.Pun.Demo.Asteroids.LobbyMainPanel::MaxPlayersInputField
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___MaxPlayersInputField_10;
	// UnityEngine.GameObject Photon.Pun.Demo.Asteroids.LobbyMainPanel::JoinRandomRoomPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___JoinRandomRoomPanel_11;
	// UnityEngine.GameObject Photon.Pun.Demo.Asteroids.LobbyMainPanel::RoomListPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___RoomListPanel_12;
	// UnityEngine.GameObject Photon.Pun.Demo.Asteroids.LobbyMainPanel::RoomListContent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___RoomListContent_13;
	// UnityEngine.GameObject Photon.Pun.Demo.Asteroids.LobbyMainPanel::RoomListEntryPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___RoomListEntryPrefab_14;
	// UnityEngine.GameObject Photon.Pun.Demo.Asteroids.LobbyMainPanel::InsideRoomPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___InsideRoomPanel_15;
	// UnityEngine.UI.Button Photon.Pun.Demo.Asteroids.LobbyMainPanel::StartGameButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___StartGameButton_16;
	// UnityEngine.GameObject Photon.Pun.Demo.Asteroids.LobbyMainPanel::PlayerListEntryPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___PlayerListEntryPrefab_17;
	// System.Collections.Generic.Dictionary`2<System.String,Photon.Realtime.RoomInfo> Photon.Pun.Demo.Asteroids.LobbyMainPanel::cachedRoomList
	Dictionary_2_tFB2AFFFE62832819FFF0D2F94E3B61356799D6D0 * ___cachedRoomList_18;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> Photon.Pun.Demo.Asteroids.LobbyMainPanel::roomListEntries
	Dictionary_2_t32E4906D51ACD553523F806F7DF01E16E0EB56CC * ___roomListEntries_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> Photon.Pun.Demo.Asteroids.LobbyMainPanel::playerListEntries
	Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * ___playerListEntries_20;

public:
	inline static int32_t get_offset_of_LoginPanel_5() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___LoginPanel_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_LoginPanel_5() const { return ___LoginPanel_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_LoginPanel_5() { return &___LoginPanel_5; }
	inline void set_LoginPanel_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___LoginPanel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LoginPanel_5), (void*)value);
	}

	inline static int32_t get_offset_of_PlayerNameInput_6() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___PlayerNameInput_6)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_PlayerNameInput_6() const { return ___PlayerNameInput_6; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_PlayerNameInput_6() { return &___PlayerNameInput_6; }
	inline void set_PlayerNameInput_6(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___PlayerNameInput_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerNameInput_6), (void*)value);
	}

	inline static int32_t get_offset_of_SelectionPanel_7() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___SelectionPanel_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_SelectionPanel_7() const { return ___SelectionPanel_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_SelectionPanel_7() { return &___SelectionPanel_7; }
	inline void set_SelectionPanel_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___SelectionPanel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SelectionPanel_7), (void*)value);
	}

	inline static int32_t get_offset_of_CreateRoomPanel_8() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___CreateRoomPanel_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_CreateRoomPanel_8() const { return ___CreateRoomPanel_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_CreateRoomPanel_8() { return &___CreateRoomPanel_8; }
	inline void set_CreateRoomPanel_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___CreateRoomPanel_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CreateRoomPanel_8), (void*)value);
	}

	inline static int32_t get_offset_of_RoomNameInputField_9() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___RoomNameInputField_9)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_RoomNameInputField_9() const { return ___RoomNameInputField_9; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_RoomNameInputField_9() { return &___RoomNameInputField_9; }
	inline void set_RoomNameInputField_9(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___RoomNameInputField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RoomNameInputField_9), (void*)value);
	}

	inline static int32_t get_offset_of_MaxPlayersInputField_10() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___MaxPlayersInputField_10)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_MaxPlayersInputField_10() const { return ___MaxPlayersInputField_10; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_MaxPlayersInputField_10() { return &___MaxPlayersInputField_10; }
	inline void set_MaxPlayersInputField_10(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___MaxPlayersInputField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MaxPlayersInputField_10), (void*)value);
	}

	inline static int32_t get_offset_of_JoinRandomRoomPanel_11() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___JoinRandomRoomPanel_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_JoinRandomRoomPanel_11() const { return ___JoinRandomRoomPanel_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_JoinRandomRoomPanel_11() { return &___JoinRandomRoomPanel_11; }
	inline void set_JoinRandomRoomPanel_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___JoinRandomRoomPanel_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JoinRandomRoomPanel_11), (void*)value);
	}

	inline static int32_t get_offset_of_RoomListPanel_12() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___RoomListPanel_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_RoomListPanel_12() const { return ___RoomListPanel_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_RoomListPanel_12() { return &___RoomListPanel_12; }
	inline void set_RoomListPanel_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___RoomListPanel_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RoomListPanel_12), (void*)value);
	}

	inline static int32_t get_offset_of_RoomListContent_13() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___RoomListContent_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_RoomListContent_13() const { return ___RoomListContent_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_RoomListContent_13() { return &___RoomListContent_13; }
	inline void set_RoomListContent_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___RoomListContent_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RoomListContent_13), (void*)value);
	}

	inline static int32_t get_offset_of_RoomListEntryPrefab_14() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___RoomListEntryPrefab_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_RoomListEntryPrefab_14() const { return ___RoomListEntryPrefab_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_RoomListEntryPrefab_14() { return &___RoomListEntryPrefab_14; }
	inline void set_RoomListEntryPrefab_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___RoomListEntryPrefab_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RoomListEntryPrefab_14), (void*)value);
	}

	inline static int32_t get_offset_of_InsideRoomPanel_15() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___InsideRoomPanel_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_InsideRoomPanel_15() const { return ___InsideRoomPanel_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_InsideRoomPanel_15() { return &___InsideRoomPanel_15; }
	inline void set_InsideRoomPanel_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___InsideRoomPanel_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InsideRoomPanel_15), (void*)value);
	}

	inline static int32_t get_offset_of_StartGameButton_16() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___StartGameButton_16)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_StartGameButton_16() const { return ___StartGameButton_16; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_StartGameButton_16() { return &___StartGameButton_16; }
	inline void set_StartGameButton_16(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___StartGameButton_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StartGameButton_16), (void*)value);
	}

	inline static int32_t get_offset_of_PlayerListEntryPrefab_17() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___PlayerListEntryPrefab_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_PlayerListEntryPrefab_17() const { return ___PlayerListEntryPrefab_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_PlayerListEntryPrefab_17() { return &___PlayerListEntryPrefab_17; }
	inline void set_PlayerListEntryPrefab_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___PlayerListEntryPrefab_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerListEntryPrefab_17), (void*)value);
	}

	inline static int32_t get_offset_of_cachedRoomList_18() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___cachedRoomList_18)); }
	inline Dictionary_2_tFB2AFFFE62832819FFF0D2F94E3B61356799D6D0 * get_cachedRoomList_18() const { return ___cachedRoomList_18; }
	inline Dictionary_2_tFB2AFFFE62832819FFF0D2F94E3B61356799D6D0 ** get_address_of_cachedRoomList_18() { return &___cachedRoomList_18; }
	inline void set_cachedRoomList_18(Dictionary_2_tFB2AFFFE62832819FFF0D2F94E3B61356799D6D0 * value)
	{
		___cachedRoomList_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cachedRoomList_18), (void*)value);
	}

	inline static int32_t get_offset_of_roomListEntries_19() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___roomListEntries_19)); }
	inline Dictionary_2_t32E4906D51ACD553523F806F7DF01E16E0EB56CC * get_roomListEntries_19() const { return ___roomListEntries_19; }
	inline Dictionary_2_t32E4906D51ACD553523F806F7DF01E16E0EB56CC ** get_address_of_roomListEntries_19() { return &___roomListEntries_19; }
	inline void set_roomListEntries_19(Dictionary_2_t32E4906D51ACD553523F806F7DF01E16E0EB56CC * value)
	{
		___roomListEntries_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___roomListEntries_19), (void*)value);
	}

	inline static int32_t get_offset_of_playerListEntries_20() { return static_cast<int32_t>(offsetof(LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28, ___playerListEntries_20)); }
	inline Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * get_playerListEntries_20() const { return ___playerListEntries_20; }
	inline Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 ** get_address_of_playerListEntries_20() { return &___playerListEntries_20; }
	inline void set_playerListEntries_20(Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * value)
	{
		___playerListEntries_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerListEntries_20), (void*)value);
	}
};


// Photon.Pun.Demo.Asteroids.PlayerOverviewPanel
struct  PlayerOverviewPanel_t902FCFF3AC84E448B20B350A5FC7E0F14413DF59  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// UnityEngine.GameObject Photon.Pun.Demo.Asteroids.PlayerOverviewPanel::PlayerOverviewEntryPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___PlayerOverviewEntryPrefab_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> Photon.Pun.Demo.Asteroids.PlayerOverviewPanel::playerListEntries
	Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * ___playerListEntries_6;

public:
	inline static int32_t get_offset_of_PlayerOverviewEntryPrefab_5() { return static_cast<int32_t>(offsetof(PlayerOverviewPanel_t902FCFF3AC84E448B20B350A5FC7E0F14413DF59, ___PlayerOverviewEntryPrefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_PlayerOverviewEntryPrefab_5() const { return ___PlayerOverviewEntryPrefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_PlayerOverviewEntryPrefab_5() { return &___PlayerOverviewEntryPrefab_5; }
	inline void set_PlayerOverviewEntryPrefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___PlayerOverviewEntryPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerOverviewEntryPrefab_5), (void*)value);
	}

	inline static int32_t get_offset_of_playerListEntries_6() { return static_cast<int32_t>(offsetof(PlayerOverviewPanel_t902FCFF3AC84E448B20B350A5FC7E0F14413DF59, ___playerListEntries_6)); }
	inline Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * get_playerListEntries_6() const { return ___playerListEntries_6; }
	inline Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 ** get_address_of_playerListEntries_6() { return &___playerListEntries_6; }
	inline void set_playerListEntries_6(Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * value)
	{
		___playerListEntries_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerListEntries_6), (void*)value);
	}
};


// Photon.Pun.Demo.Cockpit.PunCockpitEmbed
struct  PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// System.String Photon.Pun.Demo.Cockpit.PunCockpitEmbed::PunCockpit_scene
	String_t* ___PunCockpit_scene_5;
	// System.Boolean Photon.Pun.Demo.Cockpit.PunCockpitEmbed::EmbeddCockpit
	bool ___EmbeddCockpit_6;
	// System.String Photon.Pun.Demo.Cockpit.PunCockpitEmbed::CockpitGameTitle
	String_t* ___CockpitGameTitle_7;
	// UnityEngine.GameObject Photon.Pun.Demo.Cockpit.PunCockpitEmbed::LoadingIndicator
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___LoadingIndicator_8;
	// Photon.Pun.UtilityScripts.ConnectAndJoinRandom Photon.Pun.Demo.Cockpit.PunCockpitEmbed::AutoConnect
	ConnectAndJoinRandom_t0EDE1854CB4711DAAF466201EEDFEA7011DF3A51 * ___AutoConnect_9;

public:
	inline static int32_t get_offset_of_PunCockpit_scene_5() { return static_cast<int32_t>(offsetof(PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802, ___PunCockpit_scene_5)); }
	inline String_t* get_PunCockpit_scene_5() const { return ___PunCockpit_scene_5; }
	inline String_t** get_address_of_PunCockpit_scene_5() { return &___PunCockpit_scene_5; }
	inline void set_PunCockpit_scene_5(String_t* value)
	{
		___PunCockpit_scene_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PunCockpit_scene_5), (void*)value);
	}

	inline static int32_t get_offset_of_EmbeddCockpit_6() { return static_cast<int32_t>(offsetof(PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802, ___EmbeddCockpit_6)); }
	inline bool get_EmbeddCockpit_6() const { return ___EmbeddCockpit_6; }
	inline bool* get_address_of_EmbeddCockpit_6() { return &___EmbeddCockpit_6; }
	inline void set_EmbeddCockpit_6(bool value)
	{
		___EmbeddCockpit_6 = value;
	}

	inline static int32_t get_offset_of_CockpitGameTitle_7() { return static_cast<int32_t>(offsetof(PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802, ___CockpitGameTitle_7)); }
	inline String_t* get_CockpitGameTitle_7() const { return ___CockpitGameTitle_7; }
	inline String_t** get_address_of_CockpitGameTitle_7() { return &___CockpitGameTitle_7; }
	inline void set_CockpitGameTitle_7(String_t* value)
	{
		___CockpitGameTitle_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CockpitGameTitle_7), (void*)value);
	}

	inline static int32_t get_offset_of_LoadingIndicator_8() { return static_cast<int32_t>(offsetof(PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802, ___LoadingIndicator_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_LoadingIndicator_8() const { return ___LoadingIndicator_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_LoadingIndicator_8() { return &___LoadingIndicator_8; }
	inline void set_LoadingIndicator_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___LoadingIndicator_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LoadingIndicator_8), (void*)value);
	}

	inline static int32_t get_offset_of_AutoConnect_9() { return static_cast<int32_t>(offsetof(PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802, ___AutoConnect_9)); }
	inline ConnectAndJoinRandom_t0EDE1854CB4711DAAF466201EEDFEA7011DF3A51 * get_AutoConnect_9() const { return ___AutoConnect_9; }
	inline ConnectAndJoinRandom_t0EDE1854CB4711DAAF466201EEDFEA7011DF3A51 ** get_address_of_AutoConnect_9() { return &___AutoConnect_9; }
	inline void set_AutoConnect_9(ConnectAndJoinRandom_t0EDE1854CB4711DAAF466201EEDFEA7011DF3A51 * value)
	{
		___AutoConnect_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AutoConnect_9), (void*)value);
	}
};


// Photon.Pun.Demo.Procedural.Cluster
struct  Cluster_tC00E1FD121201E0D140C1912D466166D15962344  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// System.String Photon.Pun.Demo.Procedural.Cluster::propertiesKey
	String_t* ___propertiesKey_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Single> Photon.Pun.Demo.Procedural.Cluster::propertiesValue
	Dictionary_2_tFE4C6D62D20EAD754114F260D3F311D06EFBFDEF * ___propertiesValue_6;
	// System.Int32 Photon.Pun.Demo.Procedural.Cluster::<ClusterId>k__BackingField
	int32_t ___U3CClusterIdU3Ek__BackingField_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> Photon.Pun.Demo.Procedural.Cluster::<Blocks>k__BackingField
	Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * ___U3CBlocksU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_propertiesKey_5() { return static_cast<int32_t>(offsetof(Cluster_tC00E1FD121201E0D140C1912D466166D15962344, ___propertiesKey_5)); }
	inline String_t* get_propertiesKey_5() const { return ___propertiesKey_5; }
	inline String_t** get_address_of_propertiesKey_5() { return &___propertiesKey_5; }
	inline void set_propertiesKey_5(String_t* value)
	{
		___propertiesKey_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___propertiesKey_5), (void*)value);
	}

	inline static int32_t get_offset_of_propertiesValue_6() { return static_cast<int32_t>(offsetof(Cluster_tC00E1FD121201E0D140C1912D466166D15962344, ___propertiesValue_6)); }
	inline Dictionary_2_tFE4C6D62D20EAD754114F260D3F311D06EFBFDEF * get_propertiesValue_6() const { return ___propertiesValue_6; }
	inline Dictionary_2_tFE4C6D62D20EAD754114F260D3F311D06EFBFDEF ** get_address_of_propertiesValue_6() { return &___propertiesValue_6; }
	inline void set_propertiesValue_6(Dictionary_2_tFE4C6D62D20EAD754114F260D3F311D06EFBFDEF * value)
	{
		___propertiesValue_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___propertiesValue_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CClusterIdU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Cluster_tC00E1FD121201E0D140C1912D466166D15962344, ___U3CClusterIdU3Ek__BackingField_7)); }
	inline int32_t get_U3CClusterIdU3Ek__BackingField_7() const { return ___U3CClusterIdU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CClusterIdU3Ek__BackingField_7() { return &___U3CClusterIdU3Ek__BackingField_7; }
	inline void set_U3CClusterIdU3Ek__BackingField_7(int32_t value)
	{
		___U3CClusterIdU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CBlocksU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Cluster_tC00E1FD121201E0D140C1912D466166D15962344, ___U3CBlocksU3Ek__BackingField_8)); }
	inline Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * get_U3CBlocksU3Ek__BackingField_8() const { return ___U3CBlocksU3Ek__BackingField_8; }
	inline Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 ** get_address_of_U3CBlocksU3Ek__BackingField_8() { return &___U3CBlocksU3Ek__BackingField_8; }
	inline void set_U3CBlocksU3Ek__BackingField_8(Dictionary_2_tB199C44A22760CF3D6D07041DFAD8659E0CE26E2 * value)
	{
		___U3CBlocksU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBlocksU3Ek__BackingField_8), (void*)value);
	}
};


// Photon.Pun.Demo.Procedural.IngameControlPanel
struct  IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// System.Boolean Photon.Pun.Demo.Procedural.IngameControlPanel::isSeedValid
	bool ___isSeedValid_5;
	// UnityEngine.UI.InputField Photon.Pun.Demo.Procedural.IngameControlPanel::seedInputField
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___seedInputField_6;
	// UnityEngine.UI.Dropdown Photon.Pun.Demo.Procedural.IngameControlPanel::worldSizeDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___worldSizeDropdown_7;
	// UnityEngine.UI.Dropdown Photon.Pun.Demo.Procedural.IngameControlPanel::clusterSizeDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___clusterSizeDropdown_8;
	// UnityEngine.UI.Dropdown Photon.Pun.Demo.Procedural.IngameControlPanel::worldTypeDropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___worldTypeDropdown_9;
	// UnityEngine.UI.Button Photon.Pun.Demo.Procedural.IngameControlPanel::generateWorldButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___generateWorldButton_10;

public:
	inline static int32_t get_offset_of_isSeedValid_5() { return static_cast<int32_t>(offsetof(IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688, ___isSeedValid_5)); }
	inline bool get_isSeedValid_5() const { return ___isSeedValid_5; }
	inline bool* get_address_of_isSeedValid_5() { return &___isSeedValid_5; }
	inline void set_isSeedValid_5(bool value)
	{
		___isSeedValid_5 = value;
	}

	inline static int32_t get_offset_of_seedInputField_6() { return static_cast<int32_t>(offsetof(IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688, ___seedInputField_6)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_seedInputField_6() const { return ___seedInputField_6; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_seedInputField_6() { return &___seedInputField_6; }
	inline void set_seedInputField_6(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___seedInputField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___seedInputField_6), (void*)value);
	}

	inline static int32_t get_offset_of_worldSizeDropdown_7() { return static_cast<int32_t>(offsetof(IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688, ___worldSizeDropdown_7)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_worldSizeDropdown_7() const { return ___worldSizeDropdown_7; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_worldSizeDropdown_7() { return &___worldSizeDropdown_7; }
	inline void set_worldSizeDropdown_7(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___worldSizeDropdown_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___worldSizeDropdown_7), (void*)value);
	}

	inline static int32_t get_offset_of_clusterSizeDropdown_8() { return static_cast<int32_t>(offsetof(IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688, ___clusterSizeDropdown_8)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_clusterSizeDropdown_8() const { return ___clusterSizeDropdown_8; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_clusterSizeDropdown_8() { return &___clusterSizeDropdown_8; }
	inline void set_clusterSizeDropdown_8(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___clusterSizeDropdown_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clusterSizeDropdown_8), (void*)value);
	}

	inline static int32_t get_offset_of_worldTypeDropdown_9() { return static_cast<int32_t>(offsetof(IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688, ___worldTypeDropdown_9)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_worldTypeDropdown_9() const { return ___worldTypeDropdown_9; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_worldTypeDropdown_9() { return &___worldTypeDropdown_9; }
	inline void set_worldTypeDropdown_9(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___worldTypeDropdown_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___worldTypeDropdown_9), (void*)value);
	}

	inline static int32_t get_offset_of_generateWorldButton_10() { return static_cast<int32_t>(offsetof(IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688, ___generateWorldButton_10)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_generateWorldButton_10() const { return ___generateWorldButton_10; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_generateWorldButton_10() { return &___generateWorldButton_10; }
	inline void set_generateWorldButton_10(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___generateWorldButton_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___generateWorldButton_10), (void*)value);
	}
};


// Photon.Pun.Demo.PunBasics.GameManager
struct  GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// UnityEngine.GameObject Photon.Pun.Demo.PunBasics.GameManager::instance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___instance_6;
	// UnityEngine.GameObject Photon.Pun.Demo.PunBasics.GameManager::playerPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___playerPrefab_7;

public:
	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811, ___instance_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_instance_6() const { return ___instance_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_6), (void*)value);
	}

	inline static int32_t get_offset_of_playerPrefab_7() { return static_cast<int32_t>(offsetof(GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811, ___playerPrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_playerPrefab_7() const { return ___playerPrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_playerPrefab_7() { return &___playerPrefab_7; }
	inline void set_playerPrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___playerPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerPrefab_7), (void*)value);
	}
};

struct GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811_StaticFields
{
public:
	// Photon.Pun.Demo.PunBasics.GameManager Photon.Pun.Demo.PunBasics.GameManager::Instance
	GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811 * ___Instance_5;

public:
	inline static int32_t get_offset_of_Instance_5() { return static_cast<int32_t>(offsetof(GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811_StaticFields, ___Instance_5)); }
	inline GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811 * get_Instance_5() const { return ___Instance_5; }
	inline GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811 ** get_address_of_Instance_5() { return &___Instance_5; }
	inline void set_Instance_5(GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811 * value)
	{
		___Instance_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Instance_5), (void*)value);
	}
};


// Photon.Pun.Demo.PunBasics.Launcher
struct  Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// UnityEngine.GameObject Photon.Pun.Demo.PunBasics.Launcher::controlPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___controlPanel_5;
	// UnityEngine.UI.Text Photon.Pun.Demo.PunBasics.Launcher::feedbackText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___feedbackText_6;
	// System.Byte Photon.Pun.Demo.PunBasics.Launcher::maxPlayersPerRoom
	uint8_t ___maxPlayersPerRoom_7;
	// Photon.Pun.Demo.PunBasics.LoaderAnime Photon.Pun.Demo.PunBasics.Launcher::loaderAnime
	LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484 * ___loaderAnime_8;
	// System.Boolean Photon.Pun.Demo.PunBasics.Launcher::isConnecting
	bool ___isConnecting_9;
	// System.String Photon.Pun.Demo.PunBasics.Launcher::gameVersion
	String_t* ___gameVersion_10;

public:
	inline static int32_t get_offset_of_controlPanel_5() { return static_cast<int32_t>(offsetof(Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E, ___controlPanel_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_controlPanel_5() const { return ___controlPanel_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_controlPanel_5() { return &___controlPanel_5; }
	inline void set_controlPanel_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___controlPanel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controlPanel_5), (void*)value);
	}

	inline static int32_t get_offset_of_feedbackText_6() { return static_cast<int32_t>(offsetof(Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E, ___feedbackText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_feedbackText_6() const { return ___feedbackText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_feedbackText_6() { return &___feedbackText_6; }
	inline void set_feedbackText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___feedbackText_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___feedbackText_6), (void*)value);
	}

	inline static int32_t get_offset_of_maxPlayersPerRoom_7() { return static_cast<int32_t>(offsetof(Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E, ___maxPlayersPerRoom_7)); }
	inline uint8_t get_maxPlayersPerRoom_7() const { return ___maxPlayersPerRoom_7; }
	inline uint8_t* get_address_of_maxPlayersPerRoom_7() { return &___maxPlayersPerRoom_7; }
	inline void set_maxPlayersPerRoom_7(uint8_t value)
	{
		___maxPlayersPerRoom_7 = value;
	}

	inline static int32_t get_offset_of_loaderAnime_8() { return static_cast<int32_t>(offsetof(Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E, ___loaderAnime_8)); }
	inline LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484 * get_loaderAnime_8() const { return ___loaderAnime_8; }
	inline LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484 ** get_address_of_loaderAnime_8() { return &___loaderAnime_8; }
	inline void set_loaderAnime_8(LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484 * value)
	{
		___loaderAnime_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loaderAnime_8), (void*)value);
	}

	inline static int32_t get_offset_of_isConnecting_9() { return static_cast<int32_t>(offsetof(Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E, ___isConnecting_9)); }
	inline bool get_isConnecting_9() const { return ___isConnecting_9; }
	inline bool* get_address_of_isConnecting_9() { return &___isConnecting_9; }
	inline void set_isConnecting_9(bool value)
	{
		___isConnecting_9 = value;
	}

	inline static int32_t get_offset_of_gameVersion_10() { return static_cast<int32_t>(offsetof(Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E, ___gameVersion_10)); }
	inline String_t* get_gameVersion_10() const { return ___gameVersion_10; }
	inline String_t** get_address_of_gameVersion_10() { return &___gameVersion_10; }
	inline void set_gameVersion_10(String_t* value)
	{
		___gameVersion_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameVersion_10), (void*)value);
	}
};


// Photon.Pun.Demo.PunBasics.PlayerManager
struct  PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// System.Single Photon.Pun.Demo.PunBasics.PlayerManager::Health
	float ___Health_5;
	// UnityEngine.GameObject Photon.Pun.Demo.PunBasics.PlayerManager::playerUiPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___playerUiPrefab_7;
	// UnityEngine.GameObject Photon.Pun.Demo.PunBasics.PlayerManager::beams
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___beams_8;
	// System.Boolean Photon.Pun.Demo.PunBasics.PlayerManager::IsFiring
	bool ___IsFiring_9;

public:
	inline static int32_t get_offset_of_Health_5() { return static_cast<int32_t>(offsetof(PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144, ___Health_5)); }
	inline float get_Health_5() const { return ___Health_5; }
	inline float* get_address_of_Health_5() { return &___Health_5; }
	inline void set_Health_5(float value)
	{
		___Health_5 = value;
	}

	inline static int32_t get_offset_of_playerUiPrefab_7() { return static_cast<int32_t>(offsetof(PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144, ___playerUiPrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_playerUiPrefab_7() const { return ___playerUiPrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_playerUiPrefab_7() { return &___playerUiPrefab_7; }
	inline void set_playerUiPrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___playerUiPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerUiPrefab_7), (void*)value);
	}

	inline static int32_t get_offset_of_beams_8() { return static_cast<int32_t>(offsetof(PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144, ___beams_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_beams_8() const { return ___beams_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_beams_8() { return &___beams_8; }
	inline void set_beams_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___beams_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___beams_8), (void*)value);
	}

	inline static int32_t get_offset_of_IsFiring_9() { return static_cast<int32_t>(offsetof(PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144, ___IsFiring_9)); }
	inline bool get_IsFiring_9() const { return ___IsFiring_9; }
	inline bool* get_address_of_IsFiring_9() { return &___IsFiring_9; }
	inline void set_IsFiring_9(bool value)
	{
		___IsFiring_9 = value;
	}
};

struct PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144_StaticFields
{
public:
	// UnityEngine.GameObject Photon.Pun.Demo.PunBasics.PlayerManager::LocalPlayerInstance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___LocalPlayerInstance_6;

public:
	inline static int32_t get_offset_of_LocalPlayerInstance_6() { return static_cast<int32_t>(offsetof(PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144_StaticFields, ___LocalPlayerInstance_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_LocalPlayerInstance_6() const { return ___LocalPlayerInstance_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_LocalPlayerInstance_6() { return &___LocalPlayerInstance_6; }
	inline void set_LocalPlayerInstance_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___LocalPlayerInstance_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LocalPlayerInstance_6), (void*)value);
	}
};


// QuickStartLobbyController
struct  QuickStartLobbyController_t537B876962888C9023F8EDD7F7765F2396D1A6EE  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// UnityEngine.GameObject QuickStartLobbyController::quickStartButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___quickStartButton_5;
	// UnityEngine.GameObject QuickStartLobbyController::quickCancelButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___quickCancelButton_6;
	// System.Int32 QuickStartLobbyController::RoomSize
	int32_t ___RoomSize_7;

public:
	inline static int32_t get_offset_of_quickStartButton_5() { return static_cast<int32_t>(offsetof(QuickStartLobbyController_t537B876962888C9023F8EDD7F7765F2396D1A6EE, ___quickStartButton_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_quickStartButton_5() const { return ___quickStartButton_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_quickStartButton_5() { return &___quickStartButton_5; }
	inline void set_quickStartButton_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___quickStartButton_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___quickStartButton_5), (void*)value);
	}

	inline static int32_t get_offset_of_quickCancelButton_6() { return static_cast<int32_t>(offsetof(QuickStartLobbyController_t537B876962888C9023F8EDD7F7765F2396D1A6EE, ___quickCancelButton_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_quickCancelButton_6() const { return ___quickCancelButton_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_quickCancelButton_6() { return &___quickCancelButton_6; }
	inline void set_quickCancelButton_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___quickCancelButton_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___quickCancelButton_6), (void*)value);
	}

	inline static int32_t get_offset_of_RoomSize_7() { return static_cast<int32_t>(offsetof(QuickStartLobbyController_t537B876962888C9023F8EDD7F7765F2396D1A6EE, ___RoomSize_7)); }
	inline int32_t get_RoomSize_7() const { return ___RoomSize_7; }
	inline int32_t* get_address_of_RoomSize_7() { return &___RoomSize_7; }
	inline void set_RoomSize_7(int32_t value)
	{
		___RoomSize_7 = value;
	}
};


// QuickStartRoomController
struct  QuickStartRoomController_t864FBE654742E09C4052F23F8499FE2A8B32CAF9  : public MonoBehaviourPunCallbacks_t1C6D230D24896A20359CB7016C7AD6E4654B885D
{
public:
	// System.Int32 QuickStartRoomController::waitingRoomSceneIndex
	int32_t ___waitingRoomSceneIndex_5;

public:
	inline static int32_t get_offset_of_waitingRoomSceneIndex_5() { return static_cast<int32_t>(offsetof(QuickStartRoomController_t864FBE654742E09C4052F23F8499FE2A8B32CAF9, ___waitingRoomSceneIndex_5)); }
	inline int32_t get_waitingRoomSceneIndex_5() const { return ___waitingRoomSceneIndex_5; }
	inline int32_t* get_address_of_waitingRoomSceneIndex_5() { return &___waitingRoomSceneIndex_5; }
	inline void set_waitingRoomSceneIndex_5(int32_t value)
	{
		___waitingRoomSceneIndex_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3952[6] = 
{
	U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC::get_offset_of_U3CU3E1__state_0(),
	U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC::get_offset_of_U3CU3E2__current_1(),
	U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC::get_offset_of_image_2(),
	U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC::get_offset_of_U3CU3E4__this_3(),
	U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC::get_offset_of_U3CelapsedTimeU3E5__2_4(),
	U3CFadeOutU3Ed__4_tC624AD7D0DF582FFD54C7BFDACEBF0288A2AA7CC::get_offset_of_U3CcU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { sizeof (ServerAddressProperty_tE9830998791F788D9B8DC7FD4508EB9782143270), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3953[2] = 
{
	ServerAddressProperty_tE9830998791F788D9B8DC7FD4508EB9782143270::get_offset_of_Text_4(),
	ServerAddressProperty_tE9830998791F788D9B8DC7FD4508EB9782143270::get_offset_of__cache_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { sizeof (ServerProperty_tF396F8221CADA4E0341D954C80E2E98CA87370F6), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3954[2] = 
{
	ServerProperty_tF396F8221CADA4E0341D954C80E2E98CA87370F6::get_offset_of_Text_7(),
	ServerProperty_tF396F8221CADA4E0341D954C80E2E98CA87370F6::get_offset_of__cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { sizeof (PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3955[5] = 
{
	PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802::get_offset_of_PunCockpit_scene_5(),
	PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802::get_offset_of_EmbeddCockpit_6(),
	PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802::get_offset_of_CockpitGameTitle_7(),
	PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802::get_offset_of_LoadingIndicator_8(),
	PunCockpitEmbed_t718EA03BCBF94C1B4CFF2ADB2D4A59A664CB4802::get_offset_of_AutoConnect_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { sizeof (U3CStartU3Ed__6_tE78D5702A9E0B439BA3EA81BC2C0ABBDE4960ABA), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3956[3] = 
{
	U3CStartU3Ed__6_tE78D5702A9E0B439BA3EA81BC2C0ABBDE4960ABA::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__6_tE78D5702A9E0B439BA3EA81BC2C0ABBDE4960ABA::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__6_tE78D5702A9E0B439BA3EA81BC2C0ABBDE4960ABA::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { sizeof (ConnectToRegionUIForm_t51B8AC0A75675EBE77175D754677010516216319), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3957[3] = 
{
	ConnectToRegionUIForm_t51B8AC0A75675EBE77175D754677010516216319::get_offset_of_RegionInput_4(),
	ConnectToRegionUIForm_t51B8AC0A75675EBE77175D754677010516216319::get_offset_of_RegionListInput_5(),
	ConnectToRegionUIForm_t51B8AC0A75675EBE77175D754677010516216319::get_offset_of_OnSubmit_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (OnSubmitEvent_t50178258F7E1A79F7B032E67B3C7662EC9B8BCD1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { sizeof (CreateRoomUiForm_tB18CFC75CB3E9710756EE340DC4BA39AB5E1CFD5), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3959[5] = 
{
	CreateRoomUiForm_tB18CFC75CB3E9710756EE340DC4BA39AB5E1CFD5::get_offset_of_RoomNameInput_4(),
	CreateRoomUiForm_tB18CFC75CB3E9710756EE340DC4BA39AB5E1CFD5::get_offset_of_LobbyNameInput_5(),
	CreateRoomUiForm_tB18CFC75CB3E9710756EE340DC4BA39AB5E1CFD5::get_offset_of_ExpectedUsersInput_6(),
	CreateRoomUiForm_tB18CFC75CB3E9710756EE340DC4BA39AB5E1CFD5::get_offset_of_LobbyTypeInput_7(),
	CreateRoomUiForm_tB18CFC75CB3E9710756EE340DC4BA39AB5E1CFD5::get_offset_of_OnSubmit_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { sizeof (OnSubmitEvent_tEAF5D32F65BE88AAD89EDE90BC06E2E0AB6B59B4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { sizeof (U3CU3Ec_t4040D60D562F65BFD9DAF6076260B1106996B994), -1, sizeof(U3CU3Ec_t4040D60D562F65BFD9DAF6076260B1106996B994_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3961[2] = 
{
	U3CU3Ec_t4040D60D562F65BFD9DAF6076260B1106996B994_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4040D60D562F65BFD9DAF6076260B1106996B994_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { sizeof (LoadLevelUIForm_t3C504A2C791E924CCE39CC93F1A351BFAB14C166), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3962[2] = 
{
	LoadLevelUIForm_t3C504A2C791E924CCE39CC93F1A351BFAB14C166::get_offset_of_PropertyValueInput_4(),
	LoadLevelUIForm_t3C504A2C791E924CCE39CC93F1A351BFAB14C166::get_offset_of_OnSubmit_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { sizeof (OnSubmitEvent_t48121DCBCBFA2A2D4D2051513ACC05438A437ED7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { sizeof (SetRoomCustomPropertyUIForm_tBD7E4023932AA3B4F89E5D8FF367044298CFEE0F), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3964[2] = 
{
	SetRoomCustomPropertyUIForm_tBD7E4023932AA3B4F89E5D8FF367044298CFEE0F::get_offset_of_PropertyValueInput_4(),
	SetRoomCustomPropertyUIForm_tBD7E4023932AA3B4F89E5D8FF367044298CFEE0F::get_offset_of_OnSubmit_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { sizeof (OnSubmitEvent_tCCDDD3E5DB3876A2D7BF8D930959AB47D6B785BC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { sizeof (UserIdUiForm_tFCE24B50C3A6BCBB56E6C78268FB541B811771E7), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3966[3] = 
{
	0,
	UserIdUiForm_tFCE24B50C3A6BCBB56E6C78268FB541B811771E7::get_offset_of_idInput_5(),
	UserIdUiForm_tFCE24B50C3A6BCBB56E6C78268FB541B811771E7::get_offset_of_OnSubmit_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { sizeof (OnSubmitEvent_t49EB5BBF0581C3E26C318B266AB48B6E05E68AC3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { sizeof (CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3968[9] = 
{
	CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F::get_offset_of_distance_4(),
	CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F::get_offset_of_height_5(),
	CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F::get_offset_of_heightSmoothLag_6(),
	CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F::get_offset_of_centerOffset_7(),
	CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F::get_offset_of_followOnStart_8(),
	CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F::get_offset_of_cameraTransform_9(),
	CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F::get_offset_of_isFollowing_10(),
	CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F::get_offset_of_heightVelocity_11(),
	CameraWork_t6A5EFE356B25D5F7EB91E73ED6B797471459BF7F::get_offset_of_targetHeight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { sizeof (GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811), -1, sizeof(GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3969[3] = 
{
	GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811_StaticFields::get_offset_of_Instance_5(),
	GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811::get_offset_of_instance_6(),
	GameManager_tDCB40C1B9C2FEF68E742B7ECA6BB79F32C93F811::get_offset_of_playerPrefab_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { sizeof (Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3970[6] = 
{
	Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E::get_offset_of_controlPanel_5(),
	Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E::get_offset_of_feedbackText_6(),
	Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E::get_offset_of_maxPlayersPerRoom_7(),
	Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E::get_offset_of_loaderAnime_8(),
	Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E::get_offset_of_isConnecting_9(),
	Launcher_t7326E573E89083D61CC214C144E8F44DCD303D1E::get_offset_of_gameVersion_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { sizeof (LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3971[7] = 
{
	LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484::get_offset_of_speed_4(),
	LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484::get_offset_of_radius_5(),
	LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484::get_offset_of_particles_6(),
	LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484::get_offset_of__offset_7(),
	LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484::get_offset_of__transform_8(),
	LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484::get_offset_of__particleTransform_9(),
	LoaderAnime_t57640CFC7D03AFB2FE543F9BFB7B001CC7411484::get_offset_of__isAnimating_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { sizeof (PlayerAnimatorManager_t359E814EB3F102A750B3504A3378A5C09D618336), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3972[2] = 
{
	PlayerAnimatorManager_t359E814EB3F102A750B3504A3378A5C09D618336::get_offset_of_directionDampTime_5(),
	PlayerAnimatorManager_t359E814EB3F102A750B3504A3378A5C09D618336::get_offset_of_animator_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { sizeof (PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144), -1, sizeof(PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3973[5] = 
{
	PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144::get_offset_of_Health_5(),
	PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144_StaticFields::get_offset_of_LocalPlayerInstance_6(),
	PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144::get_offset_of_playerUiPrefab_7(),
	PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144::get_offset_of_beams_8(),
	PlayerManager_tF057BAC34104D386C5F951E3BF07FDD59B277144::get_offset_of_IsFiring_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { sizeof (PlayerNameInputField_t1E8DFFB41F20135465C6CC7A6379387865955010), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3974[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { sizeof (PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3975[9] = 
{
	PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735::get_offset_of_screenOffset_4(),
	PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735::get_offset_of_playerNameText_5(),
	PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735::get_offset_of_playerHealthSlider_6(),
	PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735::get_offset_of_target_7(),
	PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735::get_offset_of_characterControllerHeight_8(),
	PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735::get_offset_of_targetTransform_9(),
	PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735::get_offset_of_targetRenderer_10(),
	PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735::get_offset_of__canvasGroup_11(),
	PlayerUI_t40D914361B558A8EF571A46BD90C2153B0BEA735::get_offset_of_targetPosition_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { sizeof (PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3976[9] = 
{
	PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7::get_offset_of_CarPrefabs_5(),
	PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7::get_offset_of_MaximumSpeed_6(),
	PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7::get_offset_of_Drag_7(),
	PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7::get_offset_of_CurrentSpeed_8(),
	PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7::get_offset_of_CurrentDistance_9(),
	PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7::get_offset_of_CarInstance_10(),
	PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7::get_offset_of_SplineWalker_11(),
	PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7::get_offset_of_m_firstTake_12(),
	PlayerControl_t8565EF1D0487DB640991BAD93157D105F826B5D7::get_offset_of_m_input_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { sizeof (U3CStartU3Ed__12_tDFCE7CD2DA1B2E6DFA1A68E4D9E7440F320E25C4), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3977[3] = 
{
	U3CStartU3Ed__12_tDFCE7CD2DA1B2E6DFA1A68E4D9E7440F320E25C4::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__12_tDFCE7CD2DA1B2E6DFA1A68E4D9E7440F320E25C4::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__12_tDFCE7CD2DA1B2E6DFA1A68E4D9E7440F320E25C4::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { sizeof (SlotLanes_t94D5B08A4B819627731EE5C5AC3FE4860198CD5B), -1, sizeof(SlotLanes_t94D5B08A4B819627731EE5C5AC3FE4860198CD5B_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3978[2] = 
{
	SlotLanes_t94D5B08A4B819627731EE5C5AC3FE4860198CD5B_StaticFields::get_offset_of_Instance_4(),
	SlotLanes_t94D5B08A4B819627731EE5C5AC3FE4860198CD5B::get_offset_of_GridPositions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { sizeof (SlotRacerSplashScreen_t9653B897FC883CCE01E40D345A69D973CFBC3BC9), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3979[3] = 
{
	SlotRacerSplashScreen_t9653B897FC883CCE01E40D345A69D973CFBC3BC9::get_offset_of_PunCockpit_scene_4(),
	SlotRacerSplashScreen_t9653B897FC883CCE01E40D345A69D973CFBC3BC9::get_offset_of_WarningText_5(),
	SlotRacerSplashScreen_t9653B897FC883CCE01E40D345A69D973CFBC3BC9::get_offset_of_SplashScreen_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (Bezier_t8F0CCEB73F1B9673BFC3D9808D50F8F336C148D8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { sizeof (BezierControlPointMode_t2B23887B50F4C7B9D8E539B09CA6F520D2123F4F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3981[4] = 
{
	BezierControlPointMode_t2B23887B50F4C7B9D8E539B09CA6F520D2123F4F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { sizeof (BezierCurve_tB4DF224E68E6BDD10E36E963033E370DA8A62714), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3982[1] = 
{
	BezierCurve_tB4DF224E68E6BDD10E36E963033E370DA8A62714::get_offset_of_points_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { sizeof (BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3983[6] = 
{
	BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944::get_offset_of_points_4(),
	BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944::get_offset_of_lengths_5(),
	BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944::get_offset_of_lengthsTime_6(),
	BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944::get_offset_of_TotalLength_7(),
	BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944::get_offset_of_modes_8(),
	BezierSpline_t2F9CB519B30FBD859E1C08337330871EE9091944::get_offset_of_loop_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (Line_t4FA3BA75F52436878AC0ED91D0AE1FF9CF112DEC), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3984[2] = 
{
	Line_t4FA3BA75F52436878AC0ED91D0AE1FF9CF112DEC::get_offset_of_p0_4(),
	Line_t4FA3BA75F52436878AC0ED91D0AE1FF9CF112DEC::get_offset_of_p1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985 = { sizeof (SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3985[6] = 
{
	SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392::get_offset_of_Spline_4(),
	SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392::get_offset_of_reverse_5(),
	SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392::get_offset_of_lookForward_6(),
	SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392::get_offset_of_currentDistance_7(),
	SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392::get_offset_of_currentClampedDistance_8(),
	SplinePosition_t9C88C01E54B456E87984E295B731DD6BB34C3392::get_offset_of_LastDistance_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986 = { sizeof (SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3986[7] = 
{
	SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F::get_offset_of_spline_4(),
	SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F::get_offset_of_Speed_5(),
	SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F::get_offset_of_lookForward_6(),
	SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F::get_offset_of_reverse_7(),
	SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F::get_offset_of_progress_8(),
	SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F::get_offset_of_currentDistance_9(),
	SplineWalker_t12339C2D26B24B2A47C4B84AE63164FD6895FD5F::get_offset_of_currentClampedDistance_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987 = { sizeof (Block_tCB9DFFA320A03A833F82D902FDF4F905FC1BE616), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3987[2] = 
{
	Block_tCB9DFFA320A03A833F82D902FDF4F905FC1BE616::get_offset_of_U3CBlockIdU3Ek__BackingField_4(),
	Block_tCB9DFFA320A03A833F82D902FDF4F905FC1BE616::get_offset_of_U3CClusterIdU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3988;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3988 = { sizeof (Cluster_tC00E1FD121201E0D140C1912D466166D15962344), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3988[4] = 
{
	Cluster_tC00E1FD121201E0D140C1912D466166D15962344::get_offset_of_propertiesKey_5(),
	Cluster_tC00E1FD121201E0D140C1912D466166D15962344::get_offset_of_propertiesValue_6(),
	Cluster_tC00E1FD121201E0D140C1912D466166D15962344::get_offset_of_U3CClusterIdU3Ek__BackingField_7(),
	Cluster_tC00E1FD121201E0D140C1912D466166D15962344::get_offset_of_U3CBlocksU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3989;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3989 = { sizeof (IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3989[6] = 
{
	IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688::get_offset_of_isSeedValid_5(),
	IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688::get_offset_of_seedInputField_6(),
	IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688::get_offset_of_worldSizeDropdown_7(),
	IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688::get_offset_of_clusterSizeDropdown_8(),
	IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688::get_offset_of_worldTypeDropdown_9(),
	IngameControlPanel_t0C30BC85B5A44991FF0952A5F24782B8CD509688::get_offset_of_generateWorldButton_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3990;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3990 = { sizeof (U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB), -1, sizeof(U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3990[4] = 
{
	U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB_StaticFields::get_offset_of_U3CU3E9__6_1_1(),
	U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB_StaticFields::get_offset_of_U3CU3E9__6_2_2(),
	U3CU3Ec_t26175D47616162EF397061A694DB9885698517FB_StaticFields::get_offset_of_U3CU3E9__6_3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3991;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3991 = { sizeof (ProceduralController_t53B3332F30DCA1222A797F949190B6E81D76425C), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3991[1] = 
{
	ProceduralController_t53B3332F30DCA1222A797F949190B6E81D76425C::get_offset_of_cam_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3992;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3992 = { sizeof (WorldSize_t3928CD2BF566A27AE96516C01333861B0427E6AE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3992[5] = 
{
	WorldSize_t3928CD2BF566A27AE96516C01333861B0427E6AE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3993;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3993 = { sizeof (WorldType_tC2ACAD0C1E2C30988845C480CEBF831D7A25D2C1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3993[4] = 
{
	WorldType_tC2ACAD0C1E2C30988845C480CEBF831D7A25D2C1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3994;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3994 = { sizeof (ClusterSize_t4368167F200E0194C0CE779F113528A4AAA3FCDB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3994[4] = 
{
	ClusterSize_t4368167F200E0194C0CE779F113528A4AAA3FCDB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3995;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3995 = { sizeof (WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5), -1, sizeof(WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3995[11] = 
{
	WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5::get_offset_of_SeedPropertiesKey_4(),
	WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5::get_offset_of_WorldSizePropertiesKey_5(),
	WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5::get_offset_of_ClusterSizePropertiesKey_6(),
	WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5::get_offset_of_WorldTypePropertiesKey_7(),
	WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5_StaticFields::get_offset_of_instance_8(),
	WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5::get_offset_of_U3CSeedU3Ek__BackingField_9(),
	WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5::get_offset_of_U3CWorldSizeU3Ek__BackingField_10(),
	WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5::get_offset_of_U3CClusterSizeU3Ek__BackingField_11(),
	WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5::get_offset_of_U3CWorldTypeU3Ek__BackingField_12(),
	WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5::get_offset_of_clusterList_13(),
	WorldGenerator_tDBB20C85BA5A60681BD04A8A05686E5E8ECBFCD5::get_offset_of_WorldMaterials_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3996;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3996 = { sizeof (U3CGenerateWorldU3Ed__31_t820A15AAC146A12428D1F4559D0391DFE2947A9D), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3996[4] = 
{
	U3CGenerateWorldU3Ed__31_t820A15AAC146A12428D1F4559D0391DFE2947A9D::get_offset_of_U3CU3E1__state_0(),
	U3CGenerateWorldU3Ed__31_t820A15AAC146A12428D1F4559D0391DFE2947A9D::get_offset_of_U3CU3E2__current_1(),
	U3CGenerateWorldU3Ed__31_t820A15AAC146A12428D1F4559D0391DFE2947A9D::get_offset_of_U3CU3E4__this_2(),
	U3CGenerateWorldU3Ed__31_t820A15AAC146A12428D1F4559D0391DFE2947A9D::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3997;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3997 = { sizeof (DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3997[8] = 
{
	DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906::get_offset_of_TitleText_4(),
	DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906::get_offset_of_DescriptionText_5(),
	DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906::get_offset_of_OpenSceneButton_6(),
	DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906::get_offset_of_OpenTutorialLinkButton_7(),
	DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906::get_offset_of_OpenDocLinkButton_8(),
	DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906::get_offset_of_MainDemoWebLink_9(),
	DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906::get_offset_of__data_10(),
	DemoHubManager_tADD7ADF8D504F6416F06A277C177A12D491DD906::get_offset_of_currentSelection_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3998;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3998 = { sizeof (DemoData_t459727E1190C57859B1586322996ECC573894F80)+ sizeof (RuntimeObject), sizeof(DemoData_t459727E1190C57859B1586322996ECC573894F80_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3998[5] = 
{
	DemoData_t459727E1190C57859B1586322996ECC573894F80::get_offset_of_Title_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DemoData_t459727E1190C57859B1586322996ECC573894F80::get_offset_of_Description_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DemoData_t459727E1190C57859B1586322996ECC573894F80::get_offset_of_Scene_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DemoData_t459727E1190C57859B1586322996ECC573894F80::get_offset_of_TutorialLink_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DemoData_t459727E1190C57859B1586322996ECC573894F80::get_offset_of_DocLink_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3999;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3999 = { sizeof (ToDemoHubButton_t5E0B3194BFEBB5D1C511FEB5653DFEBA161E2ABE), -1, sizeof(ToDemoHubButton_t5E0B3194BFEBB5D1C511FEB5653DFEBA161E2ABE_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3999[2] = 
{
	ToDemoHubButton_t5E0B3194BFEBB5D1C511FEB5653DFEBA161E2ABE_StaticFields::get_offset_of_instance_4(),
	ToDemoHubButton_t5E0B3194BFEBB5D1C511FEB5653DFEBA161E2ABE::get_offset_of__canvasGroup_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4000;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4000 = { sizeof (AsteroidsGame_t6530B69A465669603E35076B157B2B3DB43B0B04), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4000[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4001;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4001 = { sizeof (Asteroid_tAF18F8357F6F3A67E52FA518AD1B85DE84579424), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4001[4] = 
{
	Asteroid_tAF18F8357F6F3A67E52FA518AD1B85DE84579424::get_offset_of_isLargeAsteroid_4(),
	Asteroid_tAF18F8357F6F3A67E52FA518AD1B85DE84579424::get_offset_of_isDestroyed_5(),
	Asteroid_tAF18F8357F6F3A67E52FA518AD1B85DE84579424::get_offset_of_photonView_6(),
	Asteroid_tAF18F8357F6F3A67E52FA518AD1B85DE84579424::get_offset_of_rigidbody_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4002;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4002 = { sizeof (AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4), -1, sizeof(AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4002[3] = 
{
	AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4_StaticFields::get_offset_of_Instance_5(),
	AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4::get_offset_of_InfoText_6(),
	AsteroidsGameManager_tAAB97F6AE572EE267C507B1DFA19DA2472EB51D4::get_offset_of_AsteroidPrefabs_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4003;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4003 = { sizeof (U3CSpawnAsteroidU3Ed__7_t065C186F41B6C91B1AC4D85D2AF96A225092CB1A), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4003[2] = 
{
	U3CSpawnAsteroidU3Ed__7_t065C186F41B6C91B1AC4D85D2AF96A225092CB1A::get_offset_of_U3CU3E1__state_0(),
	U3CSpawnAsteroidU3Ed__7_t065C186F41B6C91B1AC4D85D2AF96A225092CB1A::get_offset_of_U3CU3E2__current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4004;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4004 = { sizeof (U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4004[6] = 
{
	U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3::get_offset_of_U3CU3E1__state_0(),
	U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3::get_offset_of_U3CU3E2__current_1(),
	U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3::get_offset_of_U3CU3E4__this_2(),
	U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3::get_offset_of_winner_3(),
	U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3::get_offset_of_score_4(),
	U3CEndOfGameU3Ed__8_t3E9B3D0A6C1C02D15526AC7F22E876B496DCCFC3::get_offset_of_U3CtimerU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4005;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4005 = { sizeof (Bullet_tBE11F9632642885D1ADCB036EE0E74CC13D9706A), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4005[1] = 
{
	Bullet_tBE11F9632642885D1ADCB036EE0E74CC13D9706A::get_offset_of_U3COwnerU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4006;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4006 = { sizeof (PlayerOverviewPanel_t902FCFF3AC84E448B20B350A5FC7E0F14413DF59), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4006[2] = 
{
	PlayerOverviewPanel_t902FCFF3AC84E448B20B350A5FC7E0F14413DF59::get_offset_of_PlayerOverviewEntryPrefab_5(),
	PlayerOverviewPanel_t902FCFF3AC84E448B20B350A5FC7E0F14413DF59::get_offset_of_playerListEntries_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4007;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4007 = { sizeof (Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4007[14] = 
{
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_RotationSpeed_4(),
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_MovementSpeed_5(),
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_MaxSpeed_6(),
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_Destruction_7(),
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_EngineTrail_8(),
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_BulletPrefab_9(),
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_photonView_10(),
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_rigidbody_11(),
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_collider_12(),
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_renderer_13(),
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_rotation_14(),
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_acceleration_15(),
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_shootingTimer_16(),
	Spaceship_t9CD4047310CE3D7EA15A5B350D9614BA50C0631B::get_offset_of_controllable_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4008;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4008 = { sizeof (U3CWaitForRespawnU3Ed__18_t2334CAF9C93C7E99B1990B947CBE4343351FC67F), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4008[3] = 
{
	U3CWaitForRespawnU3Ed__18_t2334CAF9C93C7E99B1990B947CBE4343351FC67F::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForRespawnU3Ed__18_t2334CAF9C93C7E99B1990B947CBE4343351FC67F::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForRespawnU3Ed__18_t2334CAF9C93C7E99B1990B947CBE4343351FC67F::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4009;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4009 = { sizeof (LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4009[16] = 
{
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_LoginPanel_5(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_PlayerNameInput_6(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_SelectionPanel_7(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_CreateRoomPanel_8(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_RoomNameInputField_9(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_MaxPlayersInputField_10(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_JoinRandomRoomPanel_11(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_RoomListPanel_12(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_RoomListContent_13(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_RoomListEntryPrefab_14(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_InsideRoomPanel_15(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_StartGameButton_16(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_PlayerListEntryPrefab_17(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_cachedRoomList_18(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_roomListEntries_19(),
	LobbyMainPanel_t25A10F7DD94A8144FB482DBA0000E8B468785B28::get_offset_of_playerListEntries_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4010;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4010 = { sizeof (LobbyTopPanel_t78575A2615C6AEEE06DD68E2095C8CF9C8B21A5F), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4010[2] = 
{
	LobbyTopPanel_t78575A2615C6AEEE06DD68E2095C8CF9C8B21A5F::get_offset_of_connectionStatusMessage_4(),
	LobbyTopPanel_t78575A2615C6AEEE06DD68E2095C8CF9C8B21A5F::get_offset_of_ConnectionStatusText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4011;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4011 = { sizeof (PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4011[6] = 
{
	PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8::get_offset_of_PlayerNameText_4(),
	PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8::get_offset_of_PlayerColorImage_5(),
	PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8::get_offset_of_PlayerReadyButton_6(),
	PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8::get_offset_of_PlayerReadyImage_7(),
	PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8::get_offset_of_ownerId_8(),
	PlayerListEntry_t6F0C9F068A3E9DB03F08DE07860990CA8A658BD8::get_offset_of_isPlayerReady_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4012;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4012 = { sizeof (RoomListEntry_t70DF026FFAFD48301B1496F29C7024F34C5D11FA), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4012[4] = 
{
	RoomListEntry_t70DF026FFAFD48301B1496F29C7024F34C5D11FA::get_offset_of_RoomNameText_4(),
	RoomListEntry_t70DF026FFAFD48301B1496F29C7024F34C5D11FA::get_offset_of_RoomPlayersText_5(),
	RoomListEntry_t70DF026FFAFD48301B1496F29C7024F34C5D11FA::get_offset_of_JoinRoomButton_6(),
	RoomListEntry_t70DF026FFAFD48301B1496F29C7024F34C5D11FA::get_offset_of_roomName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4013;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4013 = { sizeof (U3CPrivateImplementationDetailsU3E_t1DF3A1BCDFE891F17702C1ED79E71D854AEBD2AD), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1DF3A1BCDFE891F17702C1ED79E71D854AEBD2AD_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4013[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1DF3A1BCDFE891F17702C1ED79E71D854AEBD2AD_StaticFields::get_offset_of_E74588860CA220F5327520E16546CBB6016903F4_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4014;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4014 = { sizeof (__StaticArrayInitTypeSizeU3D512_t9F653113C990E176B6E55C042E7786EDD02BD76E)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D512_t9F653113C990E176B6E55C042E7786EDD02BD76E ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4015;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4015 = { sizeof (U3CModuleU3E_t2D35D876FED2BDD175BE36DB8283DBD59BA7B06E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4016;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4016 = { sizeof (U3CModuleU3E_t8276593D2182AD0E8D12AF6BAFC4DCCD5C1DB6C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4017;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4017 = { sizeof (Api_t4000D9F2E2A2012E34CA6CA8B3EA3E0BF565182C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4018;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4018 = { sizeof (ARKitCameraSubsystem_t60568A6D41C4421316052C06185DC95BA291F614), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4019;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4019 = { sizeof (Provider_t10F5BFB8A0883F5ABFC689F55DC4B6DFA0E9535E), -1, sizeof(Provider_t10F5BFB8A0883F5ABFC689F55DC4B6DFA0E9535E_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4019[2] = 
{
	Provider_t10F5BFB8A0883F5ABFC689F55DC4B6DFA0E9535E_StaticFields::get_offset_of_k_TextureYPropertyNameId_0(),
	Provider_t10F5BFB8A0883F5ABFC689F55DC4B6DFA0E9535E_StaticFields::get_offset_of_k_TextureCbCrPropertyNameId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4020;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4020 = { sizeof (NativeApi_tAC74EBF9B7EAB0504916300254C661F63CF9173A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4021;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4021 = { sizeof (ARKitRaycastSubsystem_t603C87A6FBC8139D35116A61251EB289E21ABC09), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4022;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4022 = { sizeof (Provider_tC61D3306173539ECD1E3F0E7184408EA855AA0B3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4023;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4023 = { sizeof (NativeApi_tB7917295BEDEB8E60FC3C8181CFDEB126B4DDB2A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4024;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4024 = { sizeof (ARKitReferencePointSubsystem_t7883B8562F4226A121B744F99A8CE262F2D0E017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4025;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4025 = { sizeof (Provider_t509F1B75CECA94E702BEAA9F7C67EA2313C58986), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4026;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4026 = { sizeof (ARKitSessionSubsystem_tD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A), -1, sizeof(ARKitSessionSubsystem_tD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4026[1] = 
{
	ARKitSessionSubsystem_tD9267F6454E65E2C795C8CAF65C9CCB0BBE1636A_StaticFields::get_offset_of_s_OnAsyncWorldMapCompleted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4027;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4027 = { sizeof (Provider_t475F303CC6F0955D8B266D4CD5B7022F3658389B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4028;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4028 = { sizeof (NativeApi_t164DECAC3F6004936824870871CC817A16AC9050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4029;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4029 = { sizeof (Availability_tFD76A2177DDFE5A5A8CDB75EB9CF784CDD9A7487)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4029[3] = 
{
	Availability_tFD76A2177DDFE5A5A8CDB75EB9CF784CDD9A7487::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4030;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4030 = { sizeof (OnAsyncConversionCompleteDelegate_t841FB5BE19010FE3AFBEDEA37C52A468755B19FF), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4031;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4031 = { sizeof (ARWorldMap_t8BAE5D083A023D7DD23C29E4082B6BBD329010DE)+ sizeof (RuntimeObject), sizeof(ARWorldMap_t8BAE5D083A023D7DD23C29E4082B6BBD329010DE ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4031[1] = 
{
	ARWorldMap_t8BAE5D083A023D7DD23C29E4082B6BBD329010DE::get_offset_of_U3CnativeHandleU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4032;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4032 = { sizeof (ARWorldMapRequestStatus_tF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4032[8] = 
{
	ARWorldMapRequestStatus_tF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4033;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4033 = { sizeof (ARWorldMapRequestStatusExtensions_t8FC86F2BC224C9CCC808FF3B4610B22BE5F8051F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4034;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4034 = { sizeof (ARKitXRDepthSubsystem_tA5A5BCEDB5F2217FEE76B4751167757193534501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4035;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4035 = { sizeof (Provider_t38A44526F6D5F1EBFDA048E10377C8F3FA82EF70), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4036;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4036 = { sizeof (TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4036[2] = 
{
	TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283::get_offset_of_positionsIn_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformPositionsJob_t4BCA4844CF5EFB6C0A19B9E5059390B2E499E283::get_offset_of_positionsOut_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4037;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4037 = { sizeof (ARKitXRPlaneSubsystem_tBCCDC8EA086FD3B3AD556F50AECA1BBFA9A8272A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4038;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4038 = { sizeof (Provider_tE828C43D91B7E57F44E04A10F068C304DBAE5A6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4039;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4039 = { sizeof (FlipBoundaryWindingJob_tAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4039[1] = 
{
	FlipBoundaryWindingJob_tAB484E8E0F98EEA68CD01FECAEBD5BFA7C75B312::get_offset_of_positions_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4040;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4040 = { sizeof (TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4040[2] = 
{
	TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A::get_offset_of_positionsIn_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformBoundaryPositionsJob_t42DE86BF3E6AB9CAC98E5C1772288A28226EC59A::get_offset_of_positionsOut_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4041;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4041 = { sizeof (U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4042;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4042 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4043;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4043 = { sizeof (AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4043[10] = 
{
	AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794::get_offset_of_m_WalkSpeed_4(),
	AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794::get_offset_of_m_RunSpeed_5(),
	AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794::get_offset_of_m_RotateSpeed_6(),
	AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794::get_offset_of_m_JumpForce_7(),
	AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794::get_offset_of_m_RunningStart_8(),
	AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794::get_offset_of_m_RigidBody_9(),
	AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794::get_offset_of_m_Animator_10(),
	AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794::get_offset_of_m_MoveTime_11(),
	AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794::get_offset_of_m_MoveSpeed_12(),
	AcquireChanController_tC2CFC3B96ED6F630CA60506700FC39132E743794::get_offset_of_m_IsGround_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4044;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4044 = { sizeof (MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4044[14] = 
{
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_m_Animator_5(),
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_m_AnimationIndex_6(),
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_m_AnimationMax_7(),
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_m_PrevState_8(),
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_m_ChangingMotion_9(),
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_maxHealth_10(),
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_currenthealth_11(),
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_healthbar_12(),
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_chat_13(),
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_chatTxt_14(),
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_audioSource_15(),
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_hurt_16(),
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_walk_17(),
	MotionChanger_t7B0A4EA235FEC22DD284CF6A55386A201F88A317::get_offset_of_motionPV_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4045;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4045 = { sizeof (U3CwaitSecU3Ed__21_tEC60F5EB702D7207105E2754AEBE6D2A50A720DD), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4045[3] = 
{
	U3CwaitSecU3Ed__21_tEC60F5EB702D7207105E2754AEBE6D2A50A720DD::get_offset_of_U3CU3E1__state_0(),
	U3CwaitSecU3Ed__21_tEC60F5EB702D7207105E2754AEBE6D2A50A720DD::get_offset_of_U3CU3E2__current_1(),
	U3CwaitSecU3Ed__21_tEC60F5EB702D7207105E2754AEBE6D2A50A720DD::get_offset_of_arrow_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4046;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4046 = { sizeof (U3CWaitDelayU3Ed__24_t31D6FD36E48161D4BBB5E499900605DF27437E35), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4046[4] = 
{
	U3CWaitDelayU3Ed__24_t31D6FD36E48161D4BBB5E499900605DF27437E35::get_offset_of_U3CU3E1__state_0(),
	U3CWaitDelayU3Ed__24_t31D6FD36E48161D4BBB5E499900605DF27437E35::get_offset_of_U3CU3E2__current_1(),
	U3CWaitDelayU3Ed__24_t31D6FD36E48161D4BBB5E499900605DF27437E35::get_offset_of_U3CU3E4__this_2(),
	U3CWaitDelayU3Ed__24_t31D6FD36E48161D4BBB5E499900605DF27437E35::get_offset_of_txt_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4047;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4047 = { sizeof (SimpleFollowCam_t07E02D8F29399188F5ACC766708003FCACE71A5E), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4047[2] = 
{
	SimpleFollowCam_t07E02D8F29399188F5ACC766708003FCACE71A5E::get_offset_of_m_Target_4(),
	SimpleFollowCam_t07E02D8F29399188F5ACC766708003FCACE71A5E::get_offset_of_m_FollowSpeed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4048;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4048 = { sizeof (ChannelSelector_tB0A23474F199F2A0EB95C2154188D7F84AD5DD01), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4048[1] = 
{
	ChannelSelector_tB0A23474F199F2A0EB95C2154188D7F84AD5DD01::get_offset_of_Channel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4049;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4049 = { sizeof (ChatAppIdCheckerUI_t1340C90B22E44EECD1D00DC60E9446F875931038), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4049[1] = 
{
	ChatAppIdCheckerUI_t1340C90B22E44EECD1D00DC60E9446F875931038::get_offset_of_Description_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4050;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4050 = { sizeof (ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE), -1, sizeof(ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4050[24] = 
{
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_ChannelsToJoinOnConnect_4(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_FriendsList_5(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_HistoryLengthToFetch_6(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_U3CUserNameU3Ek__BackingField_7(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_selectedChannelName_8(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_chatClient_9(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_chatAppSettings_10(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_missingAppIdErrorPanel_11(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_ConnectingLabel_12(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_ChatPanel_13(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_UserIdFormPanel_14(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_InputFieldChat_15(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_CurrentChannelText_16(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_ChannelToggleToInstantiate_17(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_FriendListUiItemtoInstantiate_18(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_channelToggles_19(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_friendListItemLUT_20(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_ShowState_21(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_Title_22(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_StateText_23(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_UserIdText_24(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE_StaticFields::get_offset_of_HelpText_25(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_TestLength_26(),
	ChatGui_t1D879E32F6DC7B32CB3C7841735024A21FC4D7BE::get_offset_of_testBytes_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4051;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4051 = { sizeof (FriendItem_tE22AE289C68E0B00B4E35C59CC745C6FBDE9B972), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4051[3] = 
{
	FriendItem_tE22AE289C68E0B00B4E35C59CC745C6FBDE9B972::get_offset_of_NameLabel_4(),
	FriendItem_tE22AE289C68E0B00B4E35C59CC745C6FBDE9B972::get_offset_of_StatusLabel_5(),
	FriendItem_tE22AE289C68E0B00B4E35C59CC745C6FBDE9B972::get_offset_of_Health_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4052;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4052 = { sizeof (IgnoreUiRaycastWhenInactive_tA341D2C70F8A63B7CFBC412B2B41A1A04060A8E6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4053;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4053 = { sizeof (NamePickGui_tC365A70D500B67AF8B9DD2520A525CA1AA135AAD), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4053[3] = 
{
	0,
	NamePickGui_tC365A70D500B67AF8B9DD2520A525CA1AA135AAD::get_offset_of_chatNewComponent_5(),
	NamePickGui_tC365A70D500B67AF8B9DD2520A525CA1AA135AAD::get_offset_of_idInput_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4054;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4054 = { sizeof (DelayStartLobbyController_t95D7C470AFA59436452FD2A8EFC5548DA42A446B), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4054[3] = 
{
	DelayStartLobbyController_t95D7C470AFA59436452FD2A8EFC5548DA42A446B::get_offset_of_delayStartButton_5(),
	DelayStartLobbyController_t95D7C470AFA59436452FD2A8EFC5548DA42A446B::get_offset_of_delayCancelButton_6(),
	DelayStartLobbyController_t95D7C470AFA59436452FD2A8EFC5548DA42A446B::get_offset_of_roomSize_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4055;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4055 = { sizeof (DelayStartRoomCOntroller_t1FFB79C0F66865A41A3C461E8834718D8944D538), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4055[1] = 
{
	DelayStartRoomCOntroller_t1FFB79C0F66865A41A3C461E8834718D8944D538::get_offset_of_waitingRoomSceneIndex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4056;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4056 = { sizeof (DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4056[18] = 
{
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_myPhotonView_5(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_multiplayerSceneIndex_6(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_menuSceneIndex_7(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_playerCount_8(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_roomSize_9(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_maxPlayersToStart_10(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_startingGame_11(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_playerCountDisplay_12(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_timerToStartDisplay_13(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_readyToCount_14(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_readyToStart_15(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_timerToStartGame_16(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_notFullGameTimer_17(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_fullGameTimer_18(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_maxWaitTime_19(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_maxFullGameWaitTime_20(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_nameIn_21(),
	DelayStartWaitingRoomController_tBD3C683283C0FE354C8D8F9E921C277F42C68ADF::get_offset_of_nameList_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4057;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4057 = { sizeof (DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857), -1, sizeof(DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4057[10] = 
{
	DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857::get_offset_of_arcamera_4(),
	DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857::get_offset_of_arRayMng_5(),
	DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857::get_offset_of_onTouchHold_6(),
	DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857::get_offset_of_touchPosition_7(),
	DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857::get_offset_of_Bow_8(),
	DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857::get_offset_of_Txt_9(),
	DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857::get_offset_of_bowaudio_10(),
	DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857::get_offset_of_bowFx_11(),
	DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857::get_offset_of_DragPv_12(),
	DragObject_t048CC36F18C2B9B5E385B31D92CA7197C36C6857_StaticFields::get_offset_of_hits_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4058;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4058 = { sizeof (EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4058[6] = 
{
	EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D::get_offset_of_DisButton_4(),
	EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D::get_offset_of_menuScene_5(),
	EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D::get_offset_of_audioPlay_6(),
	EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D::get_offset_of_greeting_7(),
	EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D::get_offset_of_crying_8(),
	EndCredit_tE6F7E7F4D702607B8EF6C5CDF6DA81DEDF55372D::get_offset_of_checkTxt_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4059;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4059 = { sizeof (GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4059[19] = 
{
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_delayCancelButton_4(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_menuSceneIndex_5(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_animationTxt_6(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_holdTxt_7(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_arCamera_8(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_model_9(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_arRayMng_10(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_camOffset_11(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_question_12(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_mssInput_13(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_waiting_14(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_swap_15(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_myPV_16(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_mssChatTxt_17(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_chatTxt_18(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_forgiveQuestion_19(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_audiosource_20(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_bgSound_21(),
	GameSetuoController_t09B53B393155954EABF729ABB34D53D28DA11BFA::get_offset_of_BgAudio_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4060;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4060 = { sizeof (HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4060[10] = 
{
	HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173::get_offset_of_slider_5(),
	HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173::get_offset_of_question_6(),
	HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173::get_offset_of_mssIn_7(),
	HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173::get_offset_of_waiting_8(),
	HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173::get_offset_of_gradientColor_9(),
	HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173::get_offset_of_fill_10(),
	HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173::get_offset_of_health_11(),
	HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173::get_offset_of_appear_12(),
	HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173::get_offset_of_change_13(),
	HealthBarScript_t33E337C94F4A90B02E1D75B249720A51147E8173::get_offset_of_PV_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4061;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4061 = { sizeof (NetworkController_t963EBD615A1A309D71650B017C05456D1AF2B1ED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4062;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4062 = { sizeof (QuickStartLobbyController_t537B876962888C9023F8EDD7F7765F2396D1A6EE), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4062[3] = 
{
	QuickStartLobbyController_t537B876962888C9023F8EDD7F7765F2396D1A6EE::get_offset_of_quickStartButton_5(),
	QuickStartLobbyController_t537B876962888C9023F8EDD7F7765F2396D1A6EE::get_offset_of_quickCancelButton_6(),
	QuickStartLobbyController_t537B876962888C9023F8EDD7F7765F2396D1A6EE::get_offset_of_RoomSize_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4063;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4063 = { sizeof (QuickStartRoomController_t864FBE654742E09C4052F23F8499FE2A8B32CAF9), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4063[1] = 
{
	QuickStartRoomController_t864FBE654742E09C4052F23F8499FE2A8B32CAF9::get_offset_of_waitingRoomSceneIndex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4064;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4064 = { sizeof (StaticScript_tB75409BF11FBBCFA339D6EFF6BDFF38EF5C5A3A2), -1, sizeof(StaticScript_tB75409BF11FBBCFA339D6EFF6BDFF38EF5C5A3A2_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4064[4] = 
{
	StaticScript_tB75409BF11FBBCFA339D6EFF6BDFF38EF5C5A3A2_StaticFields::get_offset_of_lastSceneTxt_4(),
	StaticScript_tB75409BF11FBBCFA339D6EFF6BDFF38EF5C5A3A2::get_offset_of_StaticPV_5(),
	StaticScript_tB75409BF11FBBCFA339D6EFF6BDFF38EF5C5A3A2::get_offset_of_Txtshow_6(),
	StaticScript_tB75409BF11FBBCFA339D6EFF6BDFF38EF5C5A3A2::get_offset_of_NextScene_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4065;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4065 = { sizeof (UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE), -1, sizeof(UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4065[8] = 
{
	UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE::get_offset_of_NameIn_5(),
	UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE::get_offset_of_nameObj_6(),
	UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE_StaticFields::get_offset_of_username_7(),
	UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE::get_offset_of_nameStored_8(),
	UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE::get_offset_of_countName_9(),
	UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE::get_offset_of_statusPrefab_10(),
	UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE::get_offset_of_chatContainer_11(),
	UsernameIn_tE219AE4249B2F07EE5E49023F31BF861B61E5CCE::get_offset_of_InputPV_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4066;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4066 = { sizeof (StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4066[91] = 
{
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_currentTime_4(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_lerpPercentage_5(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_startingAngleDown_6(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_startingAngleUp_7(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_currentAngleDown_8(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_currentAngleUp_9(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stringStartPos_10(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stringEndPos_11(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stringLastPos_12(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bowDirPull_13(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bowDirRetract_14(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstStartUpJointRot1_15(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstStartUpJointRot2_16(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstStartUpJointRot3_17(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstStartDownJointRot1_18(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstStartDownJointRot2_19(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstStartDownJointRot3_20(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstEndUpJointRot1_21(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstEndUpJointRot2_22(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstEndUpJointRot3_23(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstEndDownJointRot1_24(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstEndDownJointRot2_25(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstEndDownJointRot3_26(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstLastUpJointRot1_27(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstLastUpJointRot2_28(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstLastUpJointRot3_29(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstLastDownJointRot1_30(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstLastDownJointRot2_31(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_firstLastDownJointRot3_32(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_lastProjectile_33(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stringStartObj_34(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stringEndObj_35(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_poolHolder_36(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stringEndObjTrans_37(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stringStartObjTrans_38(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_poolHolderTrans_39(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_lastProjectileScript_40(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_projectilePool_41(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_lastProjectileTransform_42(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_lastProjectileRigidbody_43(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_audioSource_44(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_currentStressOnString_45(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_pullParamHash_46(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_projectileDelayTimer_47(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_pullDelayTimer_48(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_justLeftString_49(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_justPulledString_50(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_justStoppedPulling_51(),
	0,
	0,
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bowUserAnim_54(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bowUserPullHand_55(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_pullParamName_56(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_pullStateDelay_57(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_projectileHoldDelay_58(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_projectileOnHand_59(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bowUpJoint1_60(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bowUpJoint2_61(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bowUpJoint3_62(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bowDownJoint1_63(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bowDownJoint2_64(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bowDownJoint3_65(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bowStringPoint_66(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_joint1RotateDirectionAxis_67(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_joint2RotateDirectionAxis_68(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_joint3RotateDirectionAxis_69(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bendAngleLimitFirstJoints_70(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bendAngleLimitSecondJoints_71(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_bendAngleLimitThirdJoints_72(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_inverseBend_73(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stringMoveDirectionAxis_74(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stringMoveAmount_75(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stringMoveTime_76(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stringRetractTime_77(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_inversePull_78(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stringMovementChoice_79(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stringRetractChoice_80(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_maxStringStrength_81(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_projectileAccuracy_82(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_projectile_83(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_projectilePoolSize_84(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_projectileHoldPosOffset_85(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_isPosOffsetLocal_86(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_projectileHoldRotOffset_87(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_projectileForwardAxis_88(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_soundVolume_89(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_pullSound_90(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_retractSound_91(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_stressEffectOnSound_92(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_x_93(),
	StandardizedBowForHandBool_t6FBA29DCAC71234FD7CE475763410037093777EB::get_offset_of_y_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4067;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4067 = { sizeof (jointsDirection_t56AC6A4C0E2038F79D026727E27F4CF5782E6DCA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4067[4] = 
{
	jointsDirection_t56AC6A4C0E2038F79D026727E27F4CF5782E6DCA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4068;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4068 = { sizeof (axisDirection_t57C8521B5BC07DA14A6C0C308B9B8CDFA63E459A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4068[4] = 
{
	axisDirection_t57C8521B5BC07DA14A6C0C308B9B8CDFA63E459A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4069;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4069 = { sizeof (stringStressEaseType_tD8A79F112448CEC235337553E7A06DD640914834)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4069[9] = 
{
	stringStressEaseType_tD8A79F112448CEC235337553E7A06DD640914834::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4070;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4070 = { sizeof (stringRetractEaseType_t2570D2A62B7FFEA234510AE7E0512CE64D663FD1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4070[3] = 
{
	stringRetractEaseType_t2570D2A62B7FFEA234510AE7E0512CE64D663FD1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4071;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4071 = { sizeof (accuracyProjectile_t3E5E3F52F548E246160ECA18C99777CB3731579C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4071[5] = 
{
	accuracyProjectile_t3E5E3F52F548E246160ECA18C99777CB3731579C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4072;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4072 = { sizeof (StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F), -1, sizeof(StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4072[41] = 
{
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_rigid_4(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_bowScript_5(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_quiver_6(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_boxCollider_7(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_audioSource_8(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_currentTime_9(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_collisionHappened_10(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_timeLimitToDePool_11(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_effectOfVelocityOnParticleEmission_12(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_projectileHitParticleFlesh_13(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_projectileHitParticleWood_14(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_projectileHitParticleStone_15(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_projectileHitParticleMetal_16(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_hitSoundFlesh_17(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_hitSoundWood_18(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_hitSoundStone_19(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_hitSoundMetal_20(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_fleshDetectionTagToHash_21(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_woodDetectionTagToHash_22(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_stoneDetectionTagToHash_23(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_metalDetectionTagToHash_24(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields::get_offset_of_fleshHash_25(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields::get_offset_of_woodHash_26(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields::get_offset_of_stoneHash_27(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields::get_offset_of_metalHash_28(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields::get_offset_of_contactHash_29(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F_StaticFields::get_offset_of_projectileHash_30(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_fleshHitPS_31(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_woodHitPS_32(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_stoneHitPS_33(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_metalHitPS_34(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_fleshParticleInstance_35(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_woodParticleInstance_36(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_stoneParticleInstance_37(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_metalParticleInstance_38(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_currentParticleInstance_39(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_fleshParticleTransform_40(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_woodParticleTransform_41(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_stoneParticleTransform_42(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_metalParticleTransform_43(),
	StandardizedProjectileForHandBool_t0FF5003F5F3CD99643A50CEAAC6453B8F6C6BD7F::get_offset_of_burstControl_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4073;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4073 = { sizeof (StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4073[95] = 
{
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_currentTime_4(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_lerpPercentage_5(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_startingAngleDown_6(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_startingAngleUp_7(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_currentAngleDown_8(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_currentAngleUp_9(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stringStartPos_10(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stringEndPos_11(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stringLastPos_12(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bowDirPull_13(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bowDirRetract_14(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstStartUpJointRot1_15(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstStartUpJointRot2_16(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstStartUpJointRot3_17(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstStartDownJointRot1_18(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstStartDownJointRot2_19(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstStartDownJointRot3_20(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstEndUpJointRot1_21(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstEndUpJointRot2_22(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstEndUpJointRot3_23(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstEndDownJointRot1_24(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstEndDownJointRot2_25(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstEndDownJointRot3_26(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstLastUpJointRot1_27(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstLastUpJointRot2_28(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstLastUpJointRot3_29(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstLastDownJointRot1_30(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstLastDownJointRot2_31(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_firstLastDownJointRot3_32(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_lastProjectile_33(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stringStartObj_34(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stringEndObj_35(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_poolHolder_36(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stringEndObjTrans_37(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stringStartObjTrans_38(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_poolHolderTrans_39(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_lastProjectileScript_40(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_projectilePool_41(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_lastProjectileTransform_42(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_lastProjectileRigidbody_43(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_audioSource_44(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_currentStressOnString_45(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_pullStateHash_46(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_pullStateHash2_47(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_currentStateInfo_48(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_projectileDelayTimer_49(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_pullDelayTimer_50(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_justLeftString_51(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_justPulledString_52(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_justStoppedPulling_53(),
	0,
	0,
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bowUserAnim_56(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bowUserPullHand_57(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_pullStateLayerIndex_58(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_pullStateName_59(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_pullStateName2_60(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_pullStateDelay_61(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_projectileHoldDelay_62(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_projectileOnHand_63(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bowUpJoint1_64(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bowUpJoint2_65(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bowUpJoint3_66(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bowDownJoint1_67(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bowDownJoint2_68(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bowDownJoint3_69(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bowStringPoint_70(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_joint1RotateDirectionAxis_71(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_joint2RotateDirectionAxis_72(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_joint3RotateDirectionAxis_73(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bendAngleLimitFirstJoints_74(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bendAngleLimitSecondJoints_75(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_bendAngleLimitThirdJoints_76(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_inverseBend_77(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stringMoveDirectionAxis_78(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stringMoveAmount_79(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stringMoveTime_80(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stringRetractTime_81(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_inversePull_82(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stringMovementChoice_83(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stringRetractChoice_84(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_maxStringStrength_85(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_projectileAccuracy_86(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_projectile_87(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_projectilePoolSize_88(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_projectileHoldPosOffset_89(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_isPosOffsetLocal_90(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_projectileHoldRotOffset_91(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_projectileForwardAxis_92(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_soundVolume_93(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_pullSound_94(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_retractSound_95(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_stressEffectOnSound_96(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_x_97(),
	StandardizedBowForHand_t6F6B9B49F281D1F7D88C42E732688246CD310E79::get_offset_of_y_98(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4074;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4074 = { sizeof (jointsDirection_tC2928F58D46D2029DF98D1911627E5A2D887FBC2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4074[4] = 
{
	jointsDirection_tC2928F58D46D2029DF98D1911627E5A2D887FBC2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4075;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4075 = { sizeof (axisDirection_t51F56CA449D7BD849335C8993D885CA9B9D836F1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4075[4] = 
{
	axisDirection_t51F56CA449D7BD849335C8993D885CA9B9D836F1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4076;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4076 = { sizeof (stringStressEaseType_tB6F2A4811C3F5783C33E0191200245FCCC7FE001)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4076[9] = 
{
	stringStressEaseType_tB6F2A4811C3F5783C33E0191200245FCCC7FE001::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4077;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4077 = { sizeof (stringRetractEaseType_tF81AB2B08105AA25B00E671BA1873DDD36A89FBE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4077[3] = 
{
	stringRetractEaseType_tF81AB2B08105AA25B00E671BA1873DDD36A89FBE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4078;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4078 = { sizeof (accuracyProjectile_t415634A6C9063DB7D30413FA9D59FA35F6761BD9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4078[5] = 
{
	accuracyProjectile_t415634A6C9063DB7D30413FA9D59FA35F6761BD9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4079;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4079 = { sizeof (StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6), -1, sizeof(StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4079[41] = 
{
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_rigid_4(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_bowScript_5(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_quiver_6(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_boxCollider_7(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_audioSource_8(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_currentTime_9(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_collisionHappened_10(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_timeLimitToDePool_11(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_effectOfVelocityOnParticleEmission_12(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_projectileHitParticleFlesh_13(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_projectileHitParticleWood_14(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_projectileHitParticleStone_15(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_projectileHitParticleMetal_16(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_hitSoundFlesh_17(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_hitSoundWood_18(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_hitSoundStone_19(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_hitSoundMetal_20(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_fleshDetectionTagToHash_21(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_woodDetectionTagToHash_22(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_stoneDetectionTagToHash_23(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_metalDetectionTagToHash_24(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields::get_offset_of_fleshHash_25(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields::get_offset_of_woodHash_26(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields::get_offset_of_stoneHash_27(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields::get_offset_of_metalHash_28(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields::get_offset_of_contactHash_29(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6_StaticFields::get_offset_of_projectileHash_30(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_fleshHitPS_31(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_woodHitPS_32(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_stoneHitPS_33(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_metalHitPS_34(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_fleshParticleInstance_35(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_woodParticleInstance_36(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_stoneParticleInstance_37(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_metalParticleInstance_38(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_currentParticleInstance_39(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_fleshParticleTransform_40(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_woodParticleTransform_41(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_stoneParticleTransform_42(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_metalParticleTransform_43(),
	StandardizedProjectileForHand_t65CA80DE5996F815D945338A473E2EDC31CA75A6::get_offset_of_burstControl_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4080;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4080 = { sizeof (StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4080[81] = 
{
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_currentTime_5(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_lerpPercentage_6(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_startingAngleDown_7(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_startingAngleUp_8(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_currentAngleDown_9(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_currentAngleUp_10(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stringStartPos_11(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stringEndPos_12(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stringLastPos_13(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_bowDirPull_14(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_bowDirRetract_15(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstStartUpJointRot1_16(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstStartUpJointRot2_17(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstStartUpJointRot3_18(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstStartDownJointRot1_19(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstStartDownJointRot2_20(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstStartDownJointRot3_21(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstEndUpJointRot1_22(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstEndUpJointRot2_23(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstEndUpJointRot3_24(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstEndDownJointRot1_25(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstEndDownJointRot2_26(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstEndDownJointRot3_27(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstLastUpJointRot1_28(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstLastUpJointRot2_29(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstLastUpJointRot3_30(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstLastDownJointRot1_31(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstLastDownJointRot2_32(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_firstLastDownJointRot3_33(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_lastProjectile_34(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stringStartObj_35(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stringEndObj_36(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_poolHolder_37(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stringEndObjTrans_38(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stringStartObjTrans_39(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_poolHolderTrans_40(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_lastProjectileScript_41(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_projectilePool_42(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_lastProjectileTransform_43(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_lastProjectileRigidbody_44(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_audioSource_45(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_currentStressOnString_46(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_justLeftString_47(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_justPulledString_48(),
	0,
	0,
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_bowUpJoint1_51(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_bowUpJoint2_52(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_bowUpJoint3_53(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_bowDownJoint1_54(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_bowDownJoint2_55(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_bowDownJoint3_56(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_bowStringPoint_57(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_joint1RotateDirectionAxis_58(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_joint2RotateDirectionAxis_59(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_joint3RotateDirectionAxis_60(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_bendAngleLimitFirstJoints_61(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_bendAngleLimitSecondJoints_62(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_bendAngleLimitThirdJoints_63(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_inverseBend_64(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stringMoveDirectionAxis_65(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stringMoveAmount_66(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stringMoveTime_67(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stringRetractTime_68(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_inversePull_69(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stringMovementChoice_70(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stringRetractChoice_71(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_maxStringStrength_72(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_projectileAccuracy_73(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_projectile_74(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_projectilePoolSize_75(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_projectileHoldPosOffset_76(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_isPosOffsetLocal_77(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_projectileHoldRotOffset_78(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_projectileForwardAxis_79(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_soundVolume_80(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_pullSound_81(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_retractSound_82(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_stressEffectOnSound_83(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_x_84(),
	StandardizedBow_t68D3B177174BB48D9ECC1D7A174389CFA92411DA::get_offset_of_y_85(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4081;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4081 = { sizeof (jointsDirection_tE9CD4E8C72ACA5A602B6FEFC349CC2CB4FACBAE6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4081[4] = 
{
	jointsDirection_tE9CD4E8C72ACA5A602B6FEFC349CC2CB4FACBAE6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4082;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4082 = { sizeof (axisDirection_t5F663D90C6A0E0F8EC09255603A7877311A1736A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4082[4] = 
{
	axisDirection_t5F663D90C6A0E0F8EC09255603A7877311A1736A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4083;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4083 = { sizeof (stringStressEaseType_tB513B5B994C13396BC40062E3AD544AA28436994)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4083[9] = 
{
	stringStressEaseType_tB513B5B994C13396BC40062E3AD544AA28436994::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4084;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4084 = { sizeof (stringRetractEaseType_t21F517458624D647C12D86FDD981C03AFFC42CFA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4084[3] = 
{
	stringRetractEaseType_t21F517458624D647C12D86FDD981C03AFFC42CFA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4085;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4085 = { sizeof (accuracyProjectile_tC198D8C60616B536266A9F076E20E0EE45D61063)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4085[5] = 
{
	accuracyProjectile_tC198D8C60616B536266A9F076E20E0EE45D61063::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4086;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4086 = { sizeof (StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439), -1, sizeof(StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4086[41] = 
{
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_rigid_5(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_bowScript_6(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_quiver_7(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_boxCollider_8(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_audioSource_9(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_currentTime_10(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_collisionHappened_11(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_timeLimitToDePool_12(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_effectOfVelocityOnParticleEmission_13(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_projectileHitParticleFlesh_14(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_projectileHitParticleWood_15(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_projectileHitParticleStone_16(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_projectileHitParticleMetal_17(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_hitSoundFlesh_18(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_hitSoundWood_19(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_hitSoundStone_20(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_hitSoundMetal_21(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_fleshDetectionTagToHash_22(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_woodDetectionTagToHash_23(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_stoneDetectionTagToHash_24(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_metalDetectionTagToHash_25(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields::get_offset_of_fleshHash_26(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields::get_offset_of_woodHash_27(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields::get_offset_of_stoneHash_28(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields::get_offset_of_metalHash_29(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields::get_offset_of_contactHash_30(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439_StaticFields::get_offset_of_projectileHash_31(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_fleshHitPS_32(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_woodHitPS_33(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_stoneHitPS_34(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_metalHitPS_35(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_fleshParticleInstance_36(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_woodParticleInstance_37(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_stoneParticleInstance_38(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_metalParticleInstance_39(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_currentParticleInstance_40(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_fleshParticleTransform_41(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_woodParticleTransform_42(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_stoneParticleTransform_43(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_metalParticleTransform_44(),
	StandardizedProjectile_t7B2B213FA59FD22B677DE232676EF84AE3490439::get_offset_of_burstControl_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4087;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4087 = { sizeof (ExampleBowUserControl_tCC12B82A59C26FDF6E7E5B690B3D1F8D83BDFDC1), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4087[2] = 
{
	ExampleBowUserControl_tCC12B82A59C26FDF6E7E5B690B3D1F8D83BDFDC1::get_offset_of_animator_4(),
	ExampleBowUserControl_tCC12B82A59C26FDF6E7E5B690B3D1F8D83BDFDC1::get_offset_of_paramHash_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4088;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4088 = { sizeof (MoveAndShoot_t30B182CC8E3D84C59BAE0D52BF2B0483191CFC94), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4088[2] = 
{
	MoveAndShoot_t30B182CC8E3D84C59BAE0D52BF2B0483191CFC94::get_offset_of_speed_4(),
	MoveAndShoot_t30B182CC8E3D84C59BAE0D52BF2B0483191CFC94::get_offset_of_speedRot_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4089;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4089 = { sizeof (SimpleRotate_t9FC225F770B17BDD805878D0B8945D58CB80AB35), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4090;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4090 = { sizeof (ConnectAndJoinRandomLb_t5FC18B8B9775DA842D8BA0629CA52F5348FDC462), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4090[4] = 
{
	ConnectAndJoinRandomLb_t5FC18B8B9775DA842D8BA0629CA52F5348FDC462::get_offset_of_AppId_4(),
	ConnectAndJoinRandomLb_t5FC18B8B9775DA842D8BA0629CA52F5348FDC462::get_offset_of_lbc_5(),
	ConnectAndJoinRandomLb_t5FC18B8B9775DA842D8BA0629CA52F5348FDC462::get_offset_of_ch_6(),
	ConnectAndJoinRandomLb_t5FC18B8B9775DA842D8BA0629CA52F5348FDC462::get_offset_of_StateUiText_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4091;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4091 = { sizeof (EventSystemSpawner_tBD63CD17625F56A1597D4C873FFD361924446474), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4092;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4092 = { sizeof (OnStartDelete_t89163FA953176DC83BBD1291768C4FB034ED9692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4093;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4093 = { sizeof (TextButtonTransition_t427318D1060DFF7706A60FB1475FEEBFB52941DB), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4093[4] = 
{
	TextButtonTransition_t427318D1060DFF7706A60FB1475FEEBFB52941DB::get_offset_of__text_4(),
	TextButtonTransition_t427318D1060DFF7706A60FB1475FEEBFB52941DB::get_offset_of_Selectable_5(),
	TextButtonTransition_t427318D1060DFF7706A60FB1475FEEBFB52941DB::get_offset_of_NormalColor_6(),
	TextButtonTransition_t427318D1060DFF7706A60FB1475FEEBFB52941DB::get_offset_of_HoverColor_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4094;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4094 = { sizeof (TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable4094[7] = 
{
	TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA::get_offset_of_toggle_4(),
	TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA::get_offset_of__text_5(),
	TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA::get_offset_of_NormalOnColor_6(),
	TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA::get_offset_of_NormalOffColor_7(),
	TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA::get_offset_of_HoverOnColor_8(),
	TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA::get_offset_of_HoverOffColor_9(),
	TextToggleIsOnTransition_t75673CDD01639519B2B50CC882838CBBAF34F5CA::get_offset_of_isHover_10(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
