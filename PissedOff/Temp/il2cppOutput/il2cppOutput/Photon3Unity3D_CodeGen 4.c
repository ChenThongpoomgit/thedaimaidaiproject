﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Photon.SocketServer.Security.OakleyGroups::.cctor()
extern void OakleyGroups__cctor_m6DDB6F8F3F62E0D0C0C6B01247463FBE8669B4AC ();
// 0x00000002 System.Byte[] Photon.SocketServer.Security.ICryptoProvider::get_PublicKey()
// 0x00000003 System.Void Photon.SocketServer.Security.ICryptoProvider::DeriveSharedKey(System.Byte[])
// 0x00000004 System.Byte[] Photon.SocketServer.Security.ICryptoProvider::Encrypt(System.Byte[],System.Int32,System.Int32)
// 0x00000005 System.Byte[] Photon.SocketServer.Security.ICryptoProvider::Decrypt(System.Byte[],System.Int32,System.Int32)
// 0x00000006 System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::.ctor()
extern void DiffieHellmanCryptoProvider__ctor_m79DEA3B0297FB0DB21D1A1EA16D6BF14EB472703 ();
// 0x00000007 System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::.ctor(System.Byte[])
extern void DiffieHellmanCryptoProvider__ctor_m46EB7E11B85E1D31FD493D5F3C2F86C67642E434 ();
// 0x00000008 System.Byte[] Photon.SocketServer.Security.DiffieHellmanCryptoProvider::get_PublicKey()
extern void DiffieHellmanCryptoProvider_get_PublicKey_m8E7276BE42A886E230A2102ADF13EA29752173D8 ();
// 0x00000009 System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::DeriveSharedKey(System.Byte[])
extern void DiffieHellmanCryptoProvider_DeriveSharedKey_m2031692DFBB7F2119F974FE4BA88E38A63C47B4A ();
// 0x0000000A System.Byte[] Photon.SocketServer.Security.DiffieHellmanCryptoProvider::Encrypt(System.Byte[],System.Int32,System.Int32)
extern void DiffieHellmanCryptoProvider_Encrypt_m5E52663C39B82965DFC05AF0FD1178F2631EA879 ();
// 0x0000000B System.Byte[] Photon.SocketServer.Security.DiffieHellmanCryptoProvider::Decrypt(System.Byte[],System.Int32,System.Int32)
extern void DiffieHellmanCryptoProvider_Decrypt_m0E801317D4E936DD7C0AD02E83EDD78699C9CCA5 ();
// 0x0000000C System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::Dispose()
extern void DiffieHellmanCryptoProvider_Dispose_m1D49D0A52C2E0758D7BF801BA798ED40BB7DCFFA ();
// 0x0000000D System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::Dispose(System.Boolean)
extern void DiffieHellmanCryptoProvider_Dispose_mC3B9950CECA79C1292681C1D10430C515CF4E451 ();
// 0x0000000E Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::CalculatePublicKey()
extern void DiffieHellmanCryptoProvider_CalculatePublicKey_mFD9D62DDC1BCF6F9C17C82B0EC75A68C9E450A76 ();
// 0x0000000F Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::CalculateSharedKey(Photon.SocketServer.Numeric.BigInteger)
extern void DiffieHellmanCryptoProvider_CalculateSharedKey_mA7119E92EE90CC0C4A398C9EAD064D06FF86681E ();
// 0x00000010 Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Security.DiffieHellmanCryptoProvider::GenerateRandomSecret(System.Int32)
extern void DiffieHellmanCryptoProvider_GenerateRandomSecret_m4D455EDCFA647BBE315C940F1018C43FB67C2827 ();
// 0x00000011 System.Void Photon.SocketServer.Security.DiffieHellmanCryptoProvider::.cctor()
extern void DiffieHellmanCryptoProvider__cctor_m8FA2325AB3859E7C9CC2C4C0AE3111031F5AA1FD ();
// 0x00000012 System.Void Photon.SocketServer.Numeric.BigInteger::.ctor()
extern void BigInteger__ctor_m1874B51838EC4F8D067FEA413B7F7F1DA8130B0A ();
// 0x00000013 System.Void Photon.SocketServer.Numeric.BigInteger::.ctor(System.Int64)
extern void BigInteger__ctor_m243E7CF41BF85F94A83650D3549E5B0FD57D17A6 ();
// 0x00000014 System.Void Photon.SocketServer.Numeric.BigInteger::.ctor(Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger__ctor_m576241D78F756CF92A6B8F36D49131905132FB30 ();
// 0x00000015 System.Void Photon.SocketServer.Numeric.BigInteger::.ctor(System.Byte[])
extern void BigInteger__ctor_m5762E042F9203046C63BF713C5ABC31435288287 ();
// 0x00000016 System.Void Photon.SocketServer.Numeric.BigInteger::.ctor(System.UInt32[])
extern void BigInteger__ctor_m036EE068346EBE52AE4875CC651C16A0B758087F ();
// 0x00000017 Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Implicit(System.Int64)
extern void BigInteger_op_Implicit_m564A43BF8BA574B94C12CB08E7D3935234F1B340 ();
// 0x00000018 Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Implicit(System.Int32)
extern void BigInteger_op_Implicit_m955F82B86090B094E03C8F65FDB5BD25E3BCD474 ();
// 0x00000019 Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Addition(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_Addition_m7FE8E41C912CF901484A25B61DA96FFF62FA72D9 ();
// 0x0000001A Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Subtraction(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_Subtraction_m2D0D848548637F085163E674918CD89F3A489E7E ();
// 0x0000001B Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Multiply(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_Multiply_mC4CCB2CA2EF1A303C21DE4574F8692C459AFA2AE ();
// 0x0000001C Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_LeftShift(Photon.SocketServer.Numeric.BigInteger,System.Int32)
extern void BigInteger_op_LeftShift_mA686E81F08F5670489FD740A4EA834B1D4881545 ();
// 0x0000001D System.Int32 Photon.SocketServer.Numeric.BigInteger::shiftLeft(System.UInt32[],System.Int32)
extern void BigInteger_shiftLeft_m161B412AFEA159F6F484108D6F863213EFEF1036 ();
// 0x0000001E System.Int32 Photon.SocketServer.Numeric.BigInteger::shiftRight(System.UInt32[],System.Int32)
extern void BigInteger_shiftRight_m37AFC8BCEAAA5F2BE3AEA7F15148107964B7A17B ();
// 0x0000001F Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_UnaryNegation(Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_UnaryNegation_mB68C807CD19497DC1CF710E872C5E3BD33E22E80 ();
// 0x00000020 System.Boolean Photon.SocketServer.Numeric.BigInteger::op_Equality(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_Equality_m7A14B9D0B86AF4EE72F306B6F9E76286BFC3CF11 ();
// 0x00000021 System.Boolean Photon.SocketServer.Numeric.BigInteger::Equals(System.Object)
extern void BigInteger_Equals_m86A81AF95E6303444DFF3366FAA4EACA1B23B12B ();
// 0x00000022 System.Int32 Photon.SocketServer.Numeric.BigInteger::GetHashCode()
extern void BigInteger_GetHashCode_mE55145DCF56C5AA6974EFB3562A3C184CA7FC06B ();
// 0x00000023 System.Boolean Photon.SocketServer.Numeric.BigInteger::op_GreaterThan(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_GreaterThan_m2A5106342053197A5390A39024D34FB3502EADA7 ();
// 0x00000024 System.Boolean Photon.SocketServer.Numeric.BigInteger::op_LessThan(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_LessThan_m993DD48692FFC429A570745FE4C9B22AC7A28D8B ();
// 0x00000025 System.Boolean Photon.SocketServer.Numeric.BigInteger::op_GreaterThanOrEqual(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_GreaterThanOrEqual_m3E6B736FE08BACC445EC227FABCCFDF64686EB44 ();
// 0x00000026 System.Void Photon.SocketServer.Numeric.BigInteger::multiByteDivide(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_multiByteDivide_mF7FACC442AFB7BB6DEDB9EE404CF2548BB1A76FE ();
// 0x00000027 System.Void Photon.SocketServer.Numeric.BigInteger::singleByteDivide(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_singleByteDivide_m7FBE625577D8FE564CACA2FB5AE4F3A5FABC40BB ();
// 0x00000028 Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Division(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_Division_m768C174CCBBE2F82B71FD6BB08E83F2300D61B41 ();
// 0x00000029 Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::op_Modulus(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_op_Modulus_mD5ACC815712A15417EEFC142B590F9FE17014EEE ();
// 0x0000002A System.String Photon.SocketServer.Numeric.BigInteger::ToString()
extern void BigInteger_ToString_m5CA343F2EEDC85396F38C1B6EF5EA9C1E9A3DC6B ();
// 0x0000002B System.String Photon.SocketServer.Numeric.BigInteger::ToString(System.Int32)
extern void BigInteger_ToString_mAE2F840EA532BDEA84496BCFD54CD07E461A75E3 ();
// 0x0000002C Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::ModPow(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_ModPow_m601C7A7757CFB06C7A53A5917F72D77F407CD679 ();
// 0x0000002D Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::BarrettReduction(Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger,Photon.SocketServer.Numeric.BigInteger)
extern void BigInteger_BarrettReduction_m53E7C8DE9491A545786AF1AB24817E89869CFE1E ();
// 0x0000002E Photon.SocketServer.Numeric.BigInteger Photon.SocketServer.Numeric.BigInteger::GenerateRandom(System.Int32)
extern void BigInteger_GenerateRandom_m0848E8DE2C5BBEB276D3776905731DD5EDA0BBBC ();
// 0x0000002F System.Void Photon.SocketServer.Numeric.BigInteger::genRandomBits(System.Int32,System.Random)
extern void BigInteger_genRandomBits_mE93D15A5E6FA31A997431B11B2F7C15C5F674812 ();
// 0x00000030 System.Int32 Photon.SocketServer.Numeric.BigInteger::bitCount()
extern void BigInteger_bitCount_m5DB8FFF1FFBE4EA9C7F6C9F2F14A5526B313731F ();
// 0x00000031 System.Byte[] Photon.SocketServer.Numeric.BigInteger::GetBytes()
extern void BigInteger_GetBytes_m4257C5540FA8974B19667A2F6CD2605E2AAE8862 ();
// 0x00000032 System.Void Photon.SocketServer.Numeric.BigInteger::.cctor()
extern void BigInteger__cctor_mD67596AD7B0D05C834E7503DAA44DFC0885D988C ();
// 0x00000033 System.Void ExitGames.Client.Photon.Version::.cctor()
extern void Version__cctor_m14FA82A4734CCE23AF054CC896685FDDE296B572 ();
// 0x00000034 System.Void ExitGames.Client.Photon.Hashtable::.ctor()
extern void Hashtable__ctor_m128B1C4B6AC5E9A88A74CE1582751C1E733DB223 ();
// 0x00000035 System.Void ExitGames.Client.Photon.Hashtable::.ctor(System.Int32)
extern void Hashtable__ctor_m137EE3DF5F91DB16F2D2D3BD45CF196B6306687E ();
// 0x00000036 System.Object ExitGames.Client.Photon.Hashtable::get_Item(System.Object)
extern void Hashtable_get_Item_m5B65D1232714A2D0A39445BBE953D0E4397DA16D ();
// 0x00000037 System.Void ExitGames.Client.Photon.Hashtable::set_Item(System.Object,System.Object)
extern void Hashtable_set_Item_m4929053EB49FCF167F875347908C3DAB626294F1 ();
// 0x00000038 System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry> ExitGames.Client.Photon.Hashtable::GetEnumerator()
extern void Hashtable_GetEnumerator_mD62D5FC1FCC3DA732D47FFF231D643898EA041D4 ();
// 0x00000039 System.String ExitGames.Client.Photon.Hashtable::ToString()
extern void Hashtable_ToString_mBDDA2B0E463275F55D9C89A5092C66681273265C ();
// 0x0000003A System.Void ExitGames.Client.Photon.DictionaryEntryEnumerator::.ctor(System.Collections.IDictionaryEnumerator)
extern void DictionaryEntryEnumerator__ctor_m647C4D53CFE27CB1EACBD36C4DDA876075EC197A ();
// 0x0000003B System.Boolean ExitGames.Client.Photon.DictionaryEntryEnumerator::MoveNext()
extern void DictionaryEntryEnumerator_MoveNext_m413A422B68D5BB659B81774B9A9F717CFC39615C ();
// 0x0000003C System.Void ExitGames.Client.Photon.DictionaryEntryEnumerator::Reset()
extern void DictionaryEntryEnumerator_Reset_mE411A09A768941623E04AF230AFE5505691A3B41 ();
// 0x0000003D System.Object ExitGames.Client.Photon.DictionaryEntryEnumerator::System.Collections.IEnumerator.get_Current()
extern void DictionaryEntryEnumerator_System_Collections_IEnumerator_get_Current_mB64C01E27CBCDE369F6DAB0E546007FDC295A69D ();
// 0x0000003E System.Collections.DictionaryEntry ExitGames.Client.Photon.DictionaryEntryEnumerator::get_Current()
extern void DictionaryEntryEnumerator_get_Current_m05B264BAB3FAE6577A37DBDD335043A946A2A988 ();
// 0x0000003F System.Void ExitGames.Client.Photon.DictionaryEntryEnumerator::Dispose()
extern void DictionaryEntryEnumerator_Dispose_mFA68B5A6501D365DF03EE49FCE651D5D4BE1FDFF ();
// 0x00000040 System.Collections.Generic.List`1<System.Reflection.MethodInfo> ExitGames.Client.Photon.SupportClass::GetMethods(System.Type,System.Type)
extern void SupportClass_GetMethods_mD990ECE70FA9D65EBF231C3806798916394FC522 ();
// 0x00000041 System.Int32 ExitGames.Client.Photon.SupportClass::GetTickCount()
extern void SupportClass_GetTickCount_m7E3DE0D42353490BEEF7B0BDE433DB9524273B10 ();
// 0x00000042 System.Byte ExitGames.Client.Photon.SupportClass::CallInBackground(System.Func`1<System.Boolean>,System.Int32,System.String)
extern void SupportClass_CallInBackground_m950A69DD6E5EDD16405DF162FF00FE3A289FC21B ();
// 0x00000043 System.Byte ExitGames.Client.Photon.SupportClass::StartBackgroundCalls(System.Func`1<System.Boolean>,System.Int32,System.String)
extern void SupportClass_StartBackgroundCalls_m89F559E64E8C705FF3348421844768D6BBF3C016 ();
// 0x00000044 System.Boolean ExitGames.Client.Photon.SupportClass::StopBackgroundCalls(System.Byte)
extern void SupportClass_StopBackgroundCalls_mDCA3A47BA88DCE54D20CAF422821B5D1C8B4DD5D ();
// 0x00000045 System.Boolean ExitGames.Client.Photon.SupportClass::StopAllBackgroundCalls()
extern void SupportClass_StopAllBackgroundCalls_m4665A9E71A707DAAB95DB42C8E2398A481564DDC ();
// 0x00000046 System.Void ExitGames.Client.Photon.SupportClass::WriteStackTrace(System.Exception,System.IO.TextWriter)
extern void SupportClass_WriteStackTrace_m821DDA155CC35B368A43294E76493B041183C866 ();
// 0x00000047 System.Void ExitGames.Client.Photon.SupportClass::WriteStackTrace(System.Exception)
extern void SupportClass_WriteStackTrace_m6B991ED5A73DAB803F2F0EA4A1C8875E6B8F0A19 ();
// 0x00000048 System.String ExitGames.Client.Photon.SupportClass::DictionaryToString(System.Collections.IDictionary)
extern void SupportClass_DictionaryToString_m484EDDCC419255410C2D6B751BA82459EF457AC4 ();
// 0x00000049 System.String ExitGames.Client.Photon.SupportClass::DictionaryToString(System.Collections.IDictionary,System.Boolean)
extern void SupportClass_DictionaryToString_m5D7D958F5EABA733371535051AD73CFF9745A0D0 ();
// 0x0000004A System.String ExitGames.Client.Photon.SupportClass::ByteArrayToString(System.Byte[])
extern void SupportClass_ByteArrayToString_m0C96B9733F80956D39083D0C0E1B2FD23D2BAB6D ();
// 0x0000004B System.UInt32[] ExitGames.Client.Photon.SupportClass::InitializeTable(System.UInt32)
extern void SupportClass_InitializeTable_mC85E2A519668DE8C524DF621390DD70477B5074D ();
// 0x0000004C System.UInt32 ExitGames.Client.Photon.SupportClass::CalculateCrc(System.Byte[],System.Int32)
extern void SupportClass_CalculateCrc_m7C0815AFDDCCE3BE687E3B86224711663D504565 ();
// 0x0000004D System.Void ExitGames.Client.Photon.SupportClass::.cctor()
extern void SupportClass__cctor_mA3FC2DEFE008984D0E2F256C3E8A750CEF164E97 ();
// 0x0000004E System.Void ExitGames.Client.Photon.SupportClass_IntegerMillisecondsDelegate::.ctor(System.Object,System.IntPtr)
extern void IntegerMillisecondsDelegate__ctor_m7C66C161724794B4B20AC908A5A393792D9B6079 ();
// 0x0000004F System.Int32 ExitGames.Client.Photon.SupportClass_IntegerMillisecondsDelegate::Invoke()
extern void IntegerMillisecondsDelegate_Invoke_m2AFEAD5E437F6A0F71F57B789216568D8C20CC62 ();
// 0x00000050 System.IAsyncResult ExitGames.Client.Photon.SupportClass_IntegerMillisecondsDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void IntegerMillisecondsDelegate_BeginInvoke_m57183F46B7EEB4F0DFAC4E60E2B3024CEFECBE0B ();
// 0x00000051 System.Int32 ExitGames.Client.Photon.SupportClass_IntegerMillisecondsDelegate::EndInvoke(System.IAsyncResult)
extern void IntegerMillisecondsDelegate_EndInvoke_m38F6FA9AE47810170226C5C93B7744BB9F372DEE ();
// 0x00000052 System.Int32 ExitGames.Client.Photon.SupportClass_ThreadSafeRandom::Next()
extern void ThreadSafeRandom_Next_mCEA60A24441B4AEEDBF3071F4749C22D9629A61F ();
// 0x00000053 System.Void ExitGames.Client.Photon.SupportClass_ThreadSafeRandom::.cctor()
extern void ThreadSafeRandom__cctor_m78A792257C9FAA7B9F3BB4736229A98384702F67 ();
// 0x00000054 System.Void ExitGames.Client.Photon.SupportClass_<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m2501B940AE7227E3BB857B8B603D93880B2EC2E3 ();
// 0x00000055 System.Void ExitGames.Client.Photon.SupportClass_<>c__DisplayClass6_0::<StartBackgroundCalls>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CStartBackgroundCallsU3Eb__0_mCA3AC632216510DBC1F89391354EA454A6D97BB8 ();
// 0x00000056 System.Void ExitGames.Client.Photon.SupportClass_<>c::.cctor()
extern void U3CU3Ec__cctor_mBD253ED84DA8C4C05CC48B9EF51D2357FEBB80D2 ();
// 0x00000057 System.Void ExitGames.Client.Photon.SupportClass_<>c::.ctor()
extern void U3CU3Ec__ctor_m69D8A16EAB5497C0AC656D8608CBE3A6DC5EF081 ();
// 0x00000058 System.Int32 ExitGames.Client.Photon.SupportClass_<>c::<.cctor>b__20_0()
extern void U3CU3Ec_U3C_cctorU3Eb__20_0_m65EC7EE943DCF46EC6901D31E9574A450DB0B073 ();
// 0x00000059 System.Void ExitGames.Client.Photon.IPhotonPeerListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
// 0x0000005A System.Void ExitGames.Client.Photon.IPhotonPeerListener::OnOperationResponse(ExitGames.Client.Photon.OperationResponse)
// 0x0000005B System.Void ExitGames.Client.Photon.IPhotonPeerListener::OnStatusChanged(ExitGames.Client.Photon.StatusCode)
// 0x0000005C System.Void ExitGames.Client.Photon.IPhotonPeerListener::OnEvent(ExitGames.Client.Photon.EventData)
// 0x0000005D System.Void ExitGames.Client.Photon.StreamBuffer::.ctor(System.Int32)
extern void StreamBuffer__ctor_m5FB420807193C10A5CDB1D516F43F80B2E56E7E6 ();
// 0x0000005E System.Void ExitGames.Client.Photon.StreamBuffer::.ctor(System.Byte[])
extern void StreamBuffer__ctor_m7270D47D34ED9C0C5F3E3B90030D575DF24A5589 ();
// 0x0000005F System.Byte[] ExitGames.Client.Photon.StreamBuffer::ToArray()
extern void StreamBuffer_ToArray_m4C35C9D4D60956CE142D34B6DCB09F110F1374E5 ();
// 0x00000060 System.Byte[] ExitGames.Client.Photon.StreamBuffer::ToArrayFromPos()
extern void StreamBuffer_ToArrayFromPos_m989A8E239A9560E5C9236E8F7969BE67DDFD71A1 ();
// 0x00000061 System.Void ExitGames.Client.Photon.StreamBuffer::Compact()
extern void StreamBuffer_Compact_mA1AC3BE238DA60448CBD2F08FC68D31611A99BA5 ();
// 0x00000062 System.Byte[] ExitGames.Client.Photon.StreamBuffer::GetBuffer()
extern void StreamBuffer_GetBuffer_m0F64B92A3E3EDD5B26D118612B34B4294BC8804F ();
// 0x00000063 System.Byte[] ExitGames.Client.Photon.StreamBuffer::GetBufferAndAdvance(System.Int32,System.Int32&)
extern void StreamBuffer_GetBufferAndAdvance_mDDE205140EB252E6F5D303F42663C88C4D729719 ();
// 0x00000064 System.Int32 ExitGames.Client.Photon.StreamBuffer::get_Length()
extern void StreamBuffer_get_Length_mC0126534A1A0530A79F0D52BDE13EF70BFDA8824 ();
// 0x00000065 System.Int32 ExitGames.Client.Photon.StreamBuffer::get_Position()
extern void StreamBuffer_get_Position_mE759C9306FA73B90D1F65C833E7F799E636E4197 ();
// 0x00000066 System.Void ExitGames.Client.Photon.StreamBuffer::set_Position(System.Int32)
extern void StreamBuffer_set_Position_mF3C5FF0F28FCA6C3536B6CF5340BD449CFA484B9 ();
// 0x00000067 System.Int64 ExitGames.Client.Photon.StreamBuffer::Seek(System.Int64,System.IO.SeekOrigin)
extern void StreamBuffer_Seek_m1CD57505422C50EA75A7223AF06760929D71956E ();
// 0x00000068 System.Void ExitGames.Client.Photon.StreamBuffer::SetLength(System.Int64)
extern void StreamBuffer_SetLength_mFCF6494BC42E63E94686D60A2CA4AEBB5DB1B442 ();
// 0x00000069 System.Void ExitGames.Client.Photon.StreamBuffer::SetCapacityMinimum(System.Int32)
extern void StreamBuffer_SetCapacityMinimum_m9B922E08CAB2F56155A7694648A3E32A4C4D1326 ();
// 0x0000006A System.Int32 ExitGames.Client.Photon.StreamBuffer::Read(System.Byte[],System.Int32,System.Int32)
extern void StreamBuffer_Read_m854D8D5F6959D01220745557E9E118FCE7751820 ();
// 0x0000006B System.Void ExitGames.Client.Photon.StreamBuffer::Write(System.Byte[],System.Int32,System.Int32)
extern void StreamBuffer_Write_m2FF513BEA385876C43D2F6D5D5561423565529A6 ();
// 0x0000006C System.Byte ExitGames.Client.Photon.StreamBuffer::ReadByte()
extern void StreamBuffer_ReadByte_m0EEE833E3B4FD50CFCF76D399DF0DE433718BCB1 ();
// 0x0000006D System.Void ExitGames.Client.Photon.StreamBuffer::WriteByte(System.Byte)
extern void StreamBuffer_WriteByte_m7F8CFF3A3CAE908F77DD821EEAE7665C9BF7BCC2 ();
// 0x0000006E System.Void ExitGames.Client.Photon.StreamBuffer::WriteBytes(System.Byte,System.Byte)
extern void StreamBuffer_WriteBytes_mDC81D1BFC462AF5E4287D529A6D6E460C8DF52EA ();
// 0x0000006F System.Boolean ExitGames.Client.Photon.StreamBuffer::CheckSize(System.Int32)
extern void StreamBuffer_CheckSize_mC501A6A098B1575B6C1E4061D30A4DE63619B062 ();
// 0x00000070 System.Byte ExitGames.Client.Photon.PhotonPeer::get_ClientSdkIdShifted()
extern void PhotonPeer_get_ClientSdkIdShifted_m5D93452847DB469CB09535F3DC318EE75D663A3A ();
// 0x00000071 System.String ExitGames.Client.Photon.PhotonPeer::get_ClientVersion()
extern void PhotonPeer_get_ClientVersion_mF570F1DCD857456B9AB9FB144EB0FE1DC7E47253 ();
// 0x00000072 ExitGames.Client.Photon.SerializationProtocol ExitGames.Client.Photon.PhotonPeer::get_SerializationProtocolType()
extern void PhotonPeer_get_SerializationProtocolType_mC987375B239ACAC881AD4368EAE036FDCBF97373 ();
// 0x00000073 System.Void ExitGames.Client.Photon.PhotonPeer::set_SerializationProtocolType(ExitGames.Client.Photon.SerializationProtocol)
extern void PhotonPeer_set_SerializationProtocolType_m3E1BEB7DF7817F2CCBC28B0E29C9CF2BC51AFAB2 ();
// 0x00000074 System.Type ExitGames.Client.Photon.PhotonPeer::get_SocketImplementation()
extern void PhotonPeer_get_SocketImplementation_mCDFACD136189970F8BC3FBAF03DF1AB4AF5DAE62 ();
// 0x00000075 System.Void ExitGames.Client.Photon.PhotonPeer::set_SocketImplementation(System.Type)
extern void PhotonPeer_set_SocketImplementation_mC5DD60961EDFCB1FFAA6607AEF8D20393A783DC2 ();
// 0x00000076 ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PhotonPeer::get_Listener()
extern void PhotonPeer_get_Listener_mB19B6E7B6D70021472A50C6EC6E6623FFF0FFDA0 ();
// 0x00000077 System.Void ExitGames.Client.Photon.PhotonPeer::set_Listener(ExitGames.Client.Photon.IPhotonPeerListener)
extern void PhotonPeer_set_Listener_mA2C71ECEDB0A8CB522E11D196729E0FEB518EFC8 ();
// 0x00000078 System.Boolean ExitGames.Client.Photon.PhotonPeer::get_ReuseEventInstance()
extern void PhotonPeer_get_ReuseEventInstance_m6171C587D78F312F3764457B259397D0E09DF4D6 ();
// 0x00000079 System.Boolean ExitGames.Client.Photon.PhotonPeer::get_EnableServerTracing()
extern void PhotonPeer_get_EnableServerTracing_m862AF0F0031BE368ECF34FFDEBD7114C26BB4D29 ();
// 0x0000007A System.Byte ExitGames.Client.Photon.PhotonPeer::get_QuickResendAttempts()
extern void PhotonPeer_get_QuickResendAttempts_m70A49737144C02FC712FED99515BDBA8F11FC5D7 ();
// 0x0000007B System.Void ExitGames.Client.Photon.PhotonPeer::set_QuickResendAttempts(System.Byte)
extern void PhotonPeer_set_QuickResendAttempts_m11BBF88009532655638855E3BAFAAA50FD71B3CE ();
// 0x0000007C ExitGames.Client.Photon.PeerStateValue ExitGames.Client.Photon.PhotonPeer::get_PeerState()
extern void PhotonPeer_get_PeerState_m8A6337B9E169A236F13735775CF8D054C32C5027 ();
// 0x0000007D System.String ExitGames.Client.Photon.PhotonPeer::get_PeerID()
extern void PhotonPeer_get_PeerID_mB7CF922C20ECF23B57674C033511109D9EA395EA ();
// 0x0000007E System.Boolean ExitGames.Client.Photon.PhotonPeer::get_CrcEnabled()
extern void PhotonPeer_get_CrcEnabled_mB2F03ED7315D9E8161896307A51107EFF29B0FF5 ();
// 0x0000007F System.Void ExitGames.Client.Photon.PhotonPeer::set_CrcEnabled(System.Boolean)
extern void PhotonPeer_set_CrcEnabled_m1D5B1C8DAD50CD1B13E18AC4C5560EDB819FDA14 ();
// 0x00000080 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_PacketLossByCrc()
extern void PhotonPeer_get_PacketLossByCrc_m688F3F46480519F55E313CB723A8A24CBFF963E8 ();
// 0x00000081 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_ResentReliableCommands()
extern void PhotonPeer_get_ResentReliableCommands_m578321511F0070E81EDBDC5C4EBC662AA99BC664 ();
// 0x00000082 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_ServerTimeInMilliSeconds()
extern void PhotonPeer_get_ServerTimeInMilliSeconds_m36973D3BF6C500BDED6A55B53519487E9B85DE6C ();
// 0x00000083 System.Void ExitGames.Client.Photon.PhotonPeer::set_LocalMsTimestampDelegate(ExitGames.Client.Photon.SupportClass_IntegerMillisecondsDelegate)
extern void PhotonPeer_set_LocalMsTimestampDelegate_mBBEF4344E0C026810E8E5C24FE11F01AA39245FC ();
// 0x00000084 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_ConnectionTime()
extern void PhotonPeer_get_ConnectionTime_mB9175384D70F0C54BE6F3B4CF75EA5DEC4A418AC ();
// 0x00000085 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_LastSendOutgoingTime()
extern void PhotonPeer_get_LastSendOutgoingTime_mFB343AB718D829AE3AB1739418DD805D7CF7D654 ();
// 0x00000086 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_LongestSentCall()
extern void PhotonPeer_get_LongestSentCall_m12BD70259C53AC28BED5A0DFC7F84ABF49C28385 ();
// 0x00000087 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_RoundTripTime()
extern void PhotonPeer_get_RoundTripTime_mD1220510B856E469C55C835D64AEA9FB1410191A ();
// 0x00000088 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_RoundTripTimeVariance()
extern void PhotonPeer_get_RoundTripTimeVariance_m832ACFBCDA38299618294E356BA616A3C1422020 ();
// 0x00000089 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_TimestampOfLastSocketReceive()
extern void PhotonPeer_get_TimestampOfLastSocketReceive_m0C63BF8985D3779F7DB337E2ABB998959B93367A ();
// 0x0000008A System.String ExitGames.Client.Photon.PhotonPeer::get_ServerAddress()
extern void PhotonPeer_get_ServerAddress_m8CEDA789F9F433B2D3C77E373CC3765A9C2837F6 ();
// 0x0000008B System.String ExitGames.Client.Photon.PhotonPeer::get_ServerIpAddress()
extern void PhotonPeer_get_ServerIpAddress_mE22CFE0115DF072ABECCE60A7DBC946A29DA60B1 ();
// 0x0000008C ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PhotonPeer::get_UsedProtocol()
extern void PhotonPeer_get_UsedProtocol_m4A96B5CEBC315986C2D680B6E7E0861E50BE03C4 ();
// 0x0000008D ExitGames.Client.Photon.ConnectionProtocol ExitGames.Client.Photon.PhotonPeer::get_TransportProtocol()
extern void PhotonPeer_get_TransportProtocol_m1DA2EDF0354DACEA41770B1EAF2453A9DD97408D ();
// 0x0000008E System.Void ExitGames.Client.Photon.PhotonPeer::set_TransportProtocol(ExitGames.Client.Photon.ConnectionProtocol)
extern void PhotonPeer_set_TransportProtocol_m088ED6A378CC5664E86A19ACC7DDA0927211EC83 ();
// 0x0000008F System.Boolean ExitGames.Client.Photon.PhotonPeer::get_IsSimulationEnabled()
extern void PhotonPeer_get_IsSimulationEnabled_m29B9D53C229FA00ECABDBB03880F2F3B5291476D ();
// 0x00000090 System.Void ExitGames.Client.Photon.PhotonPeer::set_IsSimulationEnabled(System.Boolean)
extern void PhotonPeer_set_IsSimulationEnabled_m9CBB4D26099F774F660DBAA3B388D7F9C28B29D4 ();
// 0x00000091 ExitGames.Client.Photon.NetworkSimulationSet ExitGames.Client.Photon.PhotonPeer::get_NetworkSimulationSettings()
extern void PhotonPeer_get_NetworkSimulationSettings_m225E1C716C04B6E5202F7B383AF93B862FDA6A36 ();
// 0x00000092 System.Int32 ExitGames.Client.Photon.PhotonPeer::get_MaximumTransferUnit()
extern void PhotonPeer_get_MaximumTransferUnit_m4246C48422C652164BE88DD80CC477D11022A0D7 ();
// 0x00000093 System.Boolean ExitGames.Client.Photon.PhotonPeer::get_IsEncryptionAvailable()
extern void PhotonPeer_get_IsEncryptionAvailable_mF6B6222BDB4F3D70E4483FBF2EBA8A82BBC3EA08 ();
// 0x00000094 System.Boolean ExitGames.Client.Photon.PhotonPeer::get_IsSendingOnlyAcks()
extern void PhotonPeer_get_IsSendingOnlyAcks_mBB3CB8CDBA42DED733E803AC751D44ABDE230FC2 ();
// 0x00000095 System.Void ExitGames.Client.Photon.PhotonPeer::set_IsSendingOnlyAcks(System.Boolean)
extern void PhotonPeer_set_IsSendingOnlyAcks_m35710A5C13BE678D49B093838529C898E9E086C7 ();
// 0x00000096 ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsIncoming()
extern void PhotonPeer_get_TrafficStatsIncoming_m21C22D464F47A4D62B25DC16D1F396A6BEBCFBE0 ();
// 0x00000097 System.Void ExitGames.Client.Photon.PhotonPeer::set_TrafficStatsIncoming(ExitGames.Client.Photon.TrafficStats)
extern void PhotonPeer_set_TrafficStatsIncoming_m1D9291DA3EC33E78FDE7AA5E6107C1791792DCD5 ();
// 0x00000098 ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsOutgoing()
extern void PhotonPeer_get_TrafficStatsOutgoing_m49B5F77DDF3F87292D4C712A2CA6927C8F19888A ();
// 0x00000099 System.Void ExitGames.Client.Photon.PhotonPeer::set_TrafficStatsOutgoing(ExitGames.Client.Photon.TrafficStats)
extern void PhotonPeer_set_TrafficStatsOutgoing_m70CBCFC0BA4C8B659253DBD680B155A2E6BA4B64 ();
// 0x0000009A ExitGames.Client.Photon.TrafficStatsGameLevel ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsGameLevel()
extern void PhotonPeer_get_TrafficStatsGameLevel_m062C229DDE638D61C7BF7699FC7CC8616D8D8C5B ();
// 0x0000009B System.Void ExitGames.Client.Photon.PhotonPeer::set_TrafficStatsGameLevel(ExitGames.Client.Photon.TrafficStatsGameLevel)
extern void PhotonPeer_set_TrafficStatsGameLevel_mCC6D59F425055DFAFFCBD214D3CDBF70C24EEA78 ();
// 0x0000009C System.Int64 ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsElapsedMs()
extern void PhotonPeer_get_TrafficStatsElapsedMs_m2C1A5CA757684F4C18A172B18462923626A295B9 ();
// 0x0000009D System.Boolean ExitGames.Client.Photon.PhotonPeer::get_TrafficStatsEnabled()
extern void PhotonPeer_get_TrafficStatsEnabled_m3E2F3A7DB1C87731322D75C06FAA25E35D2FB550 ();
// 0x0000009E System.Void ExitGames.Client.Photon.PhotonPeer::set_TrafficStatsEnabled(System.Boolean)
extern void PhotonPeer_set_TrafficStatsEnabled_mE552AE4D2E8909CEE98793E017EB09FC3340C24D ();
// 0x0000009F System.Void ExitGames.Client.Photon.PhotonPeer::TrafficStatsReset()
extern void PhotonPeer_TrafficStatsReset_m8033C02817FA105EA4F9673294500C783C6E97BA ();
// 0x000000A0 System.Void ExitGames.Client.Photon.PhotonPeer::InitializeTrafficStats()
extern void PhotonPeer_InitializeTrafficStats_mB03A2F99E1DDFD9BCEFE3EA11D5D66264368469D ();
// 0x000000A1 System.String ExitGames.Client.Photon.PhotonPeer::VitalStatsToString(System.Boolean)
extern void PhotonPeer_VitalStatsToString_mC72BA74C40278E9FE53750D097D9430213141EAB ();
// 0x000000A2 System.Type ExitGames.Client.Photon.PhotonPeer::get_EncryptorType()
extern void PhotonPeer_get_EncryptorType_mC29B978D94BFC34D29DDF1A0FF190BDCFD49A788 ();
// 0x000000A3 System.Void ExitGames.Client.Photon.PhotonPeer::.ctor(ExitGames.Client.Photon.ConnectionProtocol)
extern void PhotonPeer__ctor_mE85E3E0315790B34E49EA83FA9689F96BDAAEEC2 ();
// 0x000000A4 System.Void ExitGames.Client.Photon.PhotonPeer::.ctor(ExitGames.Client.Photon.IPhotonPeerListener,ExitGames.Client.Photon.ConnectionProtocol)
extern void PhotonPeer__ctor_m11329795A816E741CB74149995B87117E9C4A57C ();
// 0x000000A5 System.Boolean ExitGames.Client.Photon.PhotonPeer::Connect(System.String,System.String)
extern void PhotonPeer_Connect_m78CB5B88FE197BA1CD178DA4C2A2312949F65E6C ();
// 0x000000A6 System.Boolean ExitGames.Client.Photon.PhotonPeer::Connect(System.String,System.String,System.Object)
extern void PhotonPeer_Connect_m642A0FE519BEFF9C6B0CF36060D622D199013228 ();
// 0x000000A7 System.Void ExitGames.Client.Photon.PhotonPeer::CreatePeerBase()
extern void PhotonPeer_CreatePeerBase_m2BE91CCD43077D50C75C33C2EC4546ED84D2E9B2 ();
// 0x000000A8 System.Void ExitGames.Client.Photon.PhotonPeer::Disconnect()
extern void PhotonPeer_Disconnect_mF94675A2925B67E38F9A47385A7D379735E6592F ();
// 0x000000A9 System.Void ExitGames.Client.Photon.PhotonPeer::StopThread()
extern void PhotonPeer_StopThread_mFE00C5E48A115427ABF1972C8E7EC14F7290E3BF ();
// 0x000000AA System.Void ExitGames.Client.Photon.PhotonPeer::FetchServerTimestamp()
extern void PhotonPeer_FetchServerTimestamp_m6EAF351BBA3C543276DE99CECD470EE04D2F78AB ();
// 0x000000AB System.Boolean ExitGames.Client.Photon.PhotonPeer::EstablishEncryption()
extern void PhotonPeer_EstablishEncryption_m699B37EA6CBD6BA786AB5FA115024F98932CA872 ();
// 0x000000AC System.Boolean ExitGames.Client.Photon.PhotonPeer::InitDatagramEncryption(System.Byte[],System.Byte[],System.Boolean)
extern void PhotonPeer_InitDatagramEncryption_m45623D0F081AF6AAE585DAD2AEBC78B5095DE163 ();
// 0x000000AD System.Void ExitGames.Client.Photon.PhotonPeer::InitPayloadEncryption(System.Byte[])
extern void PhotonPeer_InitPayloadEncryption_mB3AE1B057676502EBCC75BF2748B9E44ACE44138 ();
// 0x000000AE System.Void ExitGames.Client.Photon.PhotonPeer::Service()
extern void PhotonPeer_Service_m3BDE5A265B81BF990B571E7A6ED99E0AFB0D5BF4 ();
// 0x000000AF System.Boolean ExitGames.Client.Photon.PhotonPeer::SendOutgoingCommands()
extern void PhotonPeer_SendOutgoingCommands_m3DE10C529921BF182A1D82A03D51C0C4F245DE75 ();
// 0x000000B0 System.Boolean ExitGames.Client.Photon.PhotonPeer::SendAcksOnly()
extern void PhotonPeer_SendAcksOnly_m932A7C8E7B4750203E81B91F088659192BD47757 ();
// 0x000000B1 System.Boolean ExitGames.Client.Photon.PhotonPeer::DispatchIncomingCommands()
extern void PhotonPeer_DispatchIncomingCommands_m2262FE3FFD98F647C5E851FE70099214BB4D8862 ();
// 0x000000B2 System.Boolean ExitGames.Client.Photon.PhotonPeer::SendOperation(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,ExitGames.Client.Photon.SendOptions)
extern void PhotonPeer_SendOperation_mF27C3AA82ACFC14A7A1473072A35865A574BDBFE ();
// 0x000000B3 System.Boolean ExitGames.Client.Photon.PhotonPeer::RegisterType(System.Type,System.Byte,ExitGames.Client.Photon.SerializeStreamMethod,ExitGames.Client.Photon.DeserializeStreamMethod)
extern void PhotonPeer_RegisterType_m140D65F38FFEF6EC73AFB8D18EF92B0B4480F6E8 ();
// 0x000000B4 System.Void ExitGames.Client.Photon.PhotonPeer::.cctor()
extern void PhotonPeer__cctor_m4343627EE95DECA021D29643E2FF09317FE06B93 ();
// 0x000000B5 System.Boolean ExitGames.Client.Photon.PhotonPeer::<EstablishEncryption>b__179_0()
extern void PhotonPeer_U3CEstablishEncryptionU3Eb__179_0_mE0B331AE68A5D86013E24F5763CC8DA8737CB4BA ();
// 0x000000B6 System.Void ExitGames.Client.Photon.PhotonCodes::.cctor()
extern void PhotonCodes__cctor_mD37D66D134FBFCFC7DD571B559EC2F07CA6B4553 ();
// 0x000000B7 System.Type ExitGames.Client.Photon.PeerBase::get_SocketImplementation()
extern void PeerBase_get_SocketImplementation_m4647CB9ED146B091C943D5ADBC797AAB2D6F9875 ();
// 0x000000B8 System.String ExitGames.Client.Photon.PeerBase::get_ServerAddress()
extern void PeerBase_get_ServerAddress_m28CA06E3042D0ED845707E78E090F5CE93BA0057 ();
// 0x000000B9 System.Void ExitGames.Client.Photon.PeerBase::set_ServerAddress(System.String)
extern void PeerBase_set_ServerAddress_m9CE07E488636AFD51D91D3F225512C094CC0B9E7 ();
// 0x000000BA ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.PeerBase::get_Listener()
extern void PeerBase_get_Listener_mFF6D9E8A312608BDBF68C66667099AF35D8BCE04 ();
// 0x000000BB ExitGames.Client.Photon.DebugLevel ExitGames.Client.Photon.PeerBase::get_debugOut()
extern void PeerBase_get_debugOut_mBF471576063280A694729303723E9344397DA5C4 ();
// 0x000000BC System.Int32 ExitGames.Client.Photon.PeerBase::get_DisconnectTimeout()
extern void PeerBase_get_DisconnectTimeout_m90AB0B41BDD39F8EFD25E9AAA73427082719AC13 ();
// 0x000000BD System.Int32 ExitGames.Client.Photon.PeerBase::get_timePingInterval()
extern void PeerBase_get_timePingInterval_m98540AC460200073C31FDFA84E7B16A53265BF98 ();
// 0x000000BE System.Byte ExitGames.Client.Photon.PeerBase::get_ChannelCount()
extern void PeerBase_get_ChannelCount_m20BE2DEB563763DA31A8D9226E05D5F9E2D320BD ();
// 0x000000BF System.String ExitGames.Client.Photon.PeerBase::get_PeerID()
extern void PeerBase_get_PeerID_mFA80C2EE1F5994DE9E7702C57200E2E9CA2CCAEB ();
// 0x000000C0 System.Int32 ExitGames.Client.Photon.PeerBase::get_timeInt()
extern void PeerBase_get_timeInt_m28DAFD454927A2CBFCEE09FC6AE23259A2E7287A ();
// 0x000000C1 System.Boolean ExitGames.Client.Photon.PeerBase::get_IsSendingOnlyAcks()
extern void PeerBase_get_IsSendingOnlyAcks_mE068550D91457552C8C1B0DE32876B4AB4520E6A ();
// 0x000000C2 System.Int32 ExitGames.Client.Photon.PeerBase::get_mtu()
extern void PeerBase_get_mtu_mF93805524D1839EF2EFBC403079C1DA604FBCC77 ();
// 0x000000C3 System.Boolean ExitGames.Client.Photon.PeerBase::get_IsIpv6()
extern void PeerBase_get_IsIpv6_m6E1045BE36303BEDDD3E8AF61D6F1900658B06AF ();
// 0x000000C4 System.Void ExitGames.Client.Photon.PeerBase::.ctor()
extern void PeerBase__ctor_m7D86C5ADE5CBD10BBA9921BCDF5F5ADBDD730CF2 ();
// 0x000000C5 ExitGames.Client.Photon.StreamBuffer ExitGames.Client.Photon.PeerBase::MessageBufferPoolGet()
extern void PeerBase_MessageBufferPoolGet_mBB8003365B4191B751D1A5272CF8862419298D5B ();
// 0x000000C6 System.Void ExitGames.Client.Photon.PeerBase::MessageBufferPoolPut(ExitGames.Client.Photon.StreamBuffer)
extern void PeerBase_MessageBufferPoolPut_mC0468FEFBE6D6145D96D06C9E9B8BC14008E0F53 ();
// 0x000000C7 System.Void ExitGames.Client.Photon.PeerBase::InitPeerBase()
extern void PeerBase_InitPeerBase_m5BEB668B5B9EA1507B8C36FD03AD0113222946D4 ();
// 0x000000C8 System.Boolean ExitGames.Client.Photon.PeerBase::Connect(System.String,System.String,System.Object)
// 0x000000C9 System.String ExitGames.Client.Photon.PeerBase::GetHttpKeyValueString(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void PeerBase_GetHttpKeyValueString_mEE271AA6E1515967DA5A9CB40C85C2F0606EF071 ();
// 0x000000CA System.Byte[] ExitGames.Client.Photon.PeerBase::PrepareConnectData(System.String,System.String,System.Object)
extern void PeerBase_PrepareConnectData_mC866ED530BC8132628DD0F485CA23E311FAB5EB7 ();
// 0x000000CB System.String ExitGames.Client.Photon.PeerBase::PepareWebSocketUrl(System.String,System.String,System.Object)
extern void PeerBase_PepareWebSocketUrl_m243DF4DAD2D05083C94CBB262F115B304577EEB8 ();
// 0x000000CC System.Void ExitGames.Client.Photon.PeerBase::OnConnect()
// 0x000000CD System.Void ExitGames.Client.Photon.PeerBase::InitCallback()
extern void PeerBase_InitCallback_mDB98B0F7ECC86896F2359E7841694399B731AB77 ();
// 0x000000CE System.Void ExitGames.Client.Photon.PeerBase::Disconnect()
// 0x000000CF System.Void ExitGames.Client.Photon.PeerBase::StopConnection()
// 0x000000D0 System.Void ExitGames.Client.Photon.PeerBase::FetchServerTimestamp()
// 0x000000D1 System.Boolean ExitGames.Client.Photon.PeerBase::EnqueueOperation(System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Byte,ExitGames.Client.Photon.SendOptions,ExitGames.Client.Photon.EgMessageType)
// 0x000000D2 ExitGames.Client.Photon.StreamBuffer ExitGames.Client.Photon.PeerBase::SerializeOperationToMessage(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,ExitGames.Client.Photon.EgMessageType,System.Boolean)
// 0x000000D3 System.Boolean ExitGames.Client.Photon.PeerBase::SendOutgoingCommands()
// 0x000000D4 System.Boolean ExitGames.Client.Photon.PeerBase::SendAcksOnly()
extern void PeerBase_SendAcksOnly_m277C420F7AC57D351F1DFB0C29FA61C15917FAD0 ();
// 0x000000D5 System.Void ExitGames.Client.Photon.PeerBase::ReceiveIncomingCommands(System.Byte[],System.Int32)
// 0x000000D6 System.Boolean ExitGames.Client.Photon.PeerBase::DispatchIncomingCommands()
// 0x000000D7 System.Boolean ExitGames.Client.Photon.PeerBase::DeserializeMessageAndCallback(ExitGames.Client.Photon.StreamBuffer)
extern void PeerBase_DeserializeMessageAndCallback_mA6864B242B23087A4E00FD5F288A23F524E4B250 ();
// 0x000000D8 System.Void ExitGames.Client.Photon.PeerBase::UpdateRoundTripTimeAndVariance(System.Int32)
extern void PeerBase_UpdateRoundTripTimeAndVariance_m01078129E510E9D76C5B931C2D6F55E99D605D3B ();
// 0x000000D9 System.Boolean ExitGames.Client.Photon.PeerBase::ExchangeKeysForEncryption(System.Object)
extern void PeerBase_ExchangeKeysForEncryption_m0A913DDBC8AC8386F0BDB8AD9DE756721294B1CC ();
// 0x000000DA System.Void ExitGames.Client.Photon.PeerBase::DeriveSharedKey(ExitGames.Client.Photon.OperationResponse)
extern void PeerBase_DeriveSharedKey_m0B310EC273143701E3E4F4B822226C5F84CDEB44 ();
// 0x000000DB System.Void ExitGames.Client.Photon.PeerBase::InitEncryption(System.Byte[])
extern void PeerBase_InitEncryption_m88CB0C7CB0363615A733CD8CA865C3E8EEE9B0B2 ();
// 0x000000DC System.Void ExitGames.Client.Photon.PeerBase::EnqueueActionForDispatch(ExitGames.Client.Photon.PeerBase_MyAction)
extern void PeerBase_EnqueueActionForDispatch_mA1330C6E75D7C2E58DD59EB2466BF53CAC044C9E ();
// 0x000000DD System.Void ExitGames.Client.Photon.PeerBase::EnqueueDebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern void PeerBase_EnqueueDebugReturn_m2319EB790FF6ED98AEC67E26F910CCDE5845C6D9 ();
// 0x000000DE System.Void ExitGames.Client.Photon.PeerBase::EnqueueStatusCallback(ExitGames.Client.Photon.StatusCode)
extern void PeerBase_EnqueueStatusCallback_m5D276AF8B7A79BE931A02825CC4230426021C6F3 ();
// 0x000000DF ExitGames.Client.Photon.NetworkSimulationSet ExitGames.Client.Photon.PeerBase::get_NetworkSimulationSettings()
extern void PeerBase_get_NetworkSimulationSettings_mD0E02A66EFD537B5B1B3A455E2100134C8E27F08 ();
// 0x000000E0 System.Void ExitGames.Client.Photon.PeerBase::SendNetworkSimulated(System.Byte[])
extern void PeerBase_SendNetworkSimulated_m7ACD4176F1C7F784E938828F890F3DC8CB2632F8 ();
// 0x000000E1 System.Void ExitGames.Client.Photon.PeerBase::ReceiveNetworkSimulated(System.Byte[])
extern void PeerBase_ReceiveNetworkSimulated_m5328F976625000D0026C83E9A9F34D5B378280EB ();
// 0x000000E2 System.Void ExitGames.Client.Photon.PeerBase::NetworkSimRun()
extern void PeerBase_NetworkSimRun_m1E53765FB212A50487DFCAE4B1C8BF1118B669A0 ();
// 0x000000E3 System.Boolean ExitGames.Client.Photon.PeerBase::get_TrafficStatsEnabled()
extern void PeerBase_get_TrafficStatsEnabled_m24D65CD7EDD321B84495CFFCA08732D07FCA9365 ();
// 0x000000E4 ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PeerBase::get_TrafficStatsIncoming()
extern void PeerBase_get_TrafficStatsIncoming_m43F4F47DEC3C382DF37D5B75D017395213DEC28F ();
// 0x000000E5 ExitGames.Client.Photon.TrafficStats ExitGames.Client.Photon.PeerBase::get_TrafficStatsOutgoing()
extern void PeerBase_get_TrafficStatsOutgoing_m6EA662978E51E03E5F363ACAE2B2F1B8737E0493 ();
// 0x000000E6 ExitGames.Client.Photon.TrafficStatsGameLevel ExitGames.Client.Photon.PeerBase::get_TrafficStatsGameLevel()
extern void PeerBase_get_TrafficStatsGameLevel_m6DED18D8C309FFA7AB3F5423482A2385620FFC3F ();
// 0x000000E7 System.Int32 ExitGames.Client.Photon.PeerBase::get_CommandLogSize()
extern void PeerBase_get_CommandLogSize_m743F6D9E1A1C426555BB9F96E7633FAA7DC776B4 ();
// 0x000000E8 System.Void ExitGames.Client.Photon.PeerBase::CommandLogResize()
extern void PeerBase_CommandLogResize_m06318B6094B33D22EB65594A28C792FD22F360EE ();
// 0x000000E9 System.Void ExitGames.Client.Photon.PeerBase::CommandLogInit()
extern void PeerBase_CommandLogInit_mA3DC117204D1EFF34C793B81DBB2676282CA15E5 ();
// 0x000000EA System.Void ExitGames.Client.Photon.PeerBase::.cctor()
extern void PeerBase__cctor_mD60C91CD707F8E4BD9ED454D2AD76E7D33D4C9D0 ();
// 0x000000EB System.Void ExitGames.Client.Photon.PeerBase_MyAction::.ctor(System.Object,System.IntPtr)
extern void MyAction__ctor_m23B43F51DFF614D2A1C30B4844562512468B1206 ();
// 0x000000EC System.Void ExitGames.Client.Photon.PeerBase_MyAction::Invoke()
extern void MyAction_Invoke_m302257D976009F277BCA041C5E96362E83F64F16 ();
// 0x000000ED System.IAsyncResult ExitGames.Client.Photon.PeerBase_MyAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void MyAction_BeginInvoke_m0FB7DB17CA9539AA30959C05F3B5C1AF7D430EF6 ();
// 0x000000EE System.Void ExitGames.Client.Photon.PeerBase_MyAction::EndInvoke(System.IAsyncResult)
extern void MyAction_EndInvoke_m257CEC57AEE975A5540D66FC581693DD65E04C7D ();
// 0x000000EF System.Void ExitGames.Client.Photon.PeerBase_<>c__DisplayClass104_0::.ctor()
extern void U3CU3Ec__DisplayClass104_0__ctor_mBC4D46D56EA3594FB1897D17F7F58DA61CE620C4 ();
// 0x000000F0 System.Void ExitGames.Client.Photon.PeerBase_<>c__DisplayClass104_0::<EnqueueDebugReturn>b__0()
extern void U3CU3Ec__DisplayClass104_0_U3CEnqueueDebugReturnU3Eb__0_mCE0232F5F58178738301BB7AC583D2EE2EEB7128 ();
// 0x000000F1 System.Void ExitGames.Client.Photon.PeerBase_<>c__DisplayClass105_0::.ctor()
extern void U3CU3Ec__DisplayClass105_0__ctor_m0AAE731F9C372DACBC9769F1D16C964CBE59D147 ();
// 0x000000F2 System.Void ExitGames.Client.Photon.PeerBase_<>c__DisplayClass105_0::<EnqueueStatusCallback>b__0()
extern void U3CU3Ec__DisplayClass105_0_U3CEnqueueStatusCallbackU3Eb__0_m2886FC67F93137EC2CCD634F67FCFF9B0652C2D7 ();
// 0x000000F3 System.Void ExitGames.Client.Photon.CmdLogItem::.ctor()
extern void CmdLogItem__ctor_mCA536F132FE73C844CE874683CA6B340FD724463 ();
// 0x000000F4 System.Void ExitGames.Client.Photon.CmdLogItem::.ctor(ExitGames.Client.Photon.NCommand,System.Int32,System.Int32,System.Int32)
extern void CmdLogItem__ctor_m632D35EDB2311E94674FB67D9E310610B174D9F1 ();
// 0x000000F5 System.String ExitGames.Client.Photon.CmdLogItem::ToString()
extern void CmdLogItem_ToString_mEECEBFC61EBC2134E10E2AA0394962D3A8CD892C ();
// 0x000000F6 System.Void ExitGames.Client.Photon.CmdLogReceivedReliable::.ctor(ExitGames.Client.Photon.NCommand,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void CmdLogReceivedReliable__ctor_m69DF1708D65680A1F5E00C41984F7A393F7EE79F ();
// 0x000000F7 System.String ExitGames.Client.Photon.CmdLogReceivedReliable::ToString()
extern void CmdLogReceivedReliable_ToString_m0DAE2973BD044EAA8A14EF0089001271FA869F04 ();
// 0x000000F8 System.Void ExitGames.Client.Photon.CmdLogReceivedAck::.ctor(ExitGames.Client.Photon.NCommand,System.Int32,System.Int32,System.Int32)
extern void CmdLogReceivedAck__ctor_m650A47F63C299A251CF5DC28543596432515A0E9 ();
// 0x000000F9 System.String ExitGames.Client.Photon.CmdLogReceivedAck::ToString()
extern void CmdLogReceivedAck_ToString_mC0433A2B4C3F124AF9698784F50E0FF18BC77037 ();
// 0x000000FA System.Void ExitGames.Client.Photon.CmdLogSentReliable::.ctor(ExitGames.Client.Photon.NCommand,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void CmdLogSentReliable__ctor_mD7B3951F8FD50AE82769A8CDDB5066A97FF27018 ();
// 0x000000FB System.String ExitGames.Client.Photon.CmdLogSentReliable::ToString()
extern void CmdLogSentReliable_ToString_mF2EE54EF09496849C74039D409AC8DD4C3FDB5BB ();
// 0x000000FC System.Void ExitGames.Client.Photon.SendOptions::set_Reliability(System.Boolean)
extern void SendOptions_set_Reliability_mE2EC7041FF3CAC3F5A4D63C15D1E8ECC6703903C_AdjustorThunk ();
// 0x000000FD System.Void ExitGames.Client.Photon.SendOptions::.cctor()
extern void SendOptions__cctor_m75F23CE5062907B0715D78321E8DD4051C7860D8 ();
// 0x000000FE System.Void ExitGames.Client.Photon.EnetPeer::.ctor()
extern void EnetPeer__ctor_mF4E82D0EFF65EACEAE255E8F1DF553E0EEC8D4CA ();
// 0x000000FF System.Void ExitGames.Client.Photon.EnetPeer::InitPeerBase()
extern void EnetPeer_InitPeerBase_m7C5A2483E2BB5680BEF2715D1FBEDB7C6409F4E4 ();
// 0x00000100 System.Void ExitGames.Client.Photon.EnetPeer::ApplyRandomizedSequenceNumbers()
extern void EnetPeer_ApplyRandomizedSequenceNumbers_m10AEA46A8042D0845FC57FA3C6EEC7E8A41596E4 ();
// 0x00000101 System.Boolean ExitGames.Client.Photon.EnetPeer::Connect(System.String,System.String,System.Object)
extern void EnetPeer_Connect_m84BF77CFE0958B1D610C14F361117ED9A454A526 ();
// 0x00000102 System.Void ExitGames.Client.Photon.EnetPeer::OnConnect()
extern void EnetPeer_OnConnect_mA6EC2D4F2C34D712E671BD9149E4AF83C52DA502 ();
// 0x00000103 System.Void ExitGames.Client.Photon.EnetPeer::Disconnect()
extern void EnetPeer_Disconnect_mD657EA685669D998F3A2A879D63F1EDEA3C6157F ();
// 0x00000104 System.Void ExitGames.Client.Photon.EnetPeer::StopConnection()
extern void EnetPeer_StopConnection_m424ECE77ED61CA5AF9AD54C2FE3267E37088BBD6 ();
// 0x00000105 System.Void ExitGames.Client.Photon.EnetPeer::FetchServerTimestamp()
extern void EnetPeer_FetchServerTimestamp_mCFDB72D8C4169733EB75D9D24A7AE94F7CBC2D1A ();
// 0x00000106 System.Boolean ExitGames.Client.Photon.EnetPeer::DispatchIncomingCommands()
extern void EnetPeer_DispatchIncomingCommands_mFEDB0149FB9D93C374093600A2E0C58D877373D8 ();
// 0x00000107 System.Int32 ExitGames.Client.Photon.EnetPeer::GetFragmentLength()
extern void EnetPeer_GetFragmentLength_m46BE3096F819766D7F985DD3D35E4CE0F0FA85EE ();
// 0x00000108 System.Int32 ExitGames.Client.Photon.EnetPeer::CalculateBufferLen()
extern void EnetPeer_CalculateBufferLen_m4F93B333E6568D2D941FE8DCF51A704ACE5C71C0 ();
// 0x00000109 System.Int32 ExitGames.Client.Photon.EnetPeer::CalculateInitialOffset()
extern void EnetPeer_CalculateInitialOffset_m38B118FBE2F90CCAEF28304DCAFCEFF7E66A31EA ();
// 0x0000010A System.Boolean ExitGames.Client.Photon.EnetPeer::SendAcksOnly()
extern void EnetPeer_SendAcksOnly_mAE2ABF55D7622D273B000EDAD4E2BF820FBEB6DF ();
// 0x0000010B System.Boolean ExitGames.Client.Photon.EnetPeer::SendOutgoingCommands()
extern void EnetPeer_SendOutgoingCommands_m1D90EE9F92064E29730AD8683F07EF7EC0361F64 ();
// 0x0000010C System.Boolean ExitGames.Client.Photon.EnetPeer::AreReliableCommandsInTransit()
extern void EnetPeer_AreReliableCommandsInTransit_mC52FAE7C6E74D11923EA9A157DD14D4BA2408C66 ();
// 0x0000010D System.Boolean ExitGames.Client.Photon.EnetPeer::EnqueueOperation(System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Byte,ExitGames.Client.Photon.SendOptions,ExitGames.Client.Photon.EgMessageType)
extern void EnetPeer_EnqueueOperation_mD3A706F1C279F85D1B2DE2C62661595F1FF35B43 ();
// 0x0000010E ExitGames.Client.Photon.EnetChannel ExitGames.Client.Photon.EnetPeer::GetChannel(System.Byte)
extern void EnetPeer_GetChannel_mDB97980F8E29D5B7975C7FA57EB07145B4B15527 ();
// 0x0000010F System.Boolean ExitGames.Client.Photon.EnetPeer::CreateAndEnqueueCommand(System.Byte,ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void EnetPeer_CreateAndEnqueueCommand_m24C26381CF79AAC5F78969586F5BE4FDA939D217 ();
// 0x00000110 ExitGames.Client.Photon.StreamBuffer ExitGames.Client.Photon.EnetPeer::SerializeOperationToMessage(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,ExitGames.Client.Photon.EgMessageType,System.Boolean)
extern void EnetPeer_SerializeOperationToMessage_mABA2E0A5D6A3351B10AC2774F3F9ED7DBE66D3B2 ();
// 0x00000111 System.Int32 ExitGames.Client.Photon.EnetPeer::SerializeAckToBuffer()
extern void EnetPeer_SerializeAckToBuffer_m2C2FACDD3EF859EBB2CE44307DCD9E46F37291F1 ();
// 0x00000112 System.Int32 ExitGames.Client.Photon.EnetPeer::SerializeToBuffer(System.Collections.Generic.Queue`1<ExitGames.Client.Photon.NCommand>)
extern void EnetPeer_SerializeToBuffer_mD7B07335AD2323BB8B3E9FAFD84B0142EC019C77 ();
// 0x00000113 System.Void ExitGames.Client.Photon.EnetPeer::SendData(System.Byte[],System.Int32)
extern void EnetPeer_SendData_mDE8A8A05365FEBFD025B0C5281FF6281E474FC39 ();
// 0x00000114 System.Void ExitGames.Client.Photon.EnetPeer::SendToSocket(System.Byte[],System.Int32)
extern void EnetPeer_SendToSocket_mCE1A8171F5D5EB322E9C041FA57CFBC89EDE2B66 ();
// 0x00000115 System.Void ExitGames.Client.Photon.EnetPeer::SendDataEncrypted(System.Byte[],System.Int32)
extern void EnetPeer_SendDataEncrypted_mA33A067C949754D31A149E532EF15C23250D9F18 ();
// 0x00000116 System.Void ExitGames.Client.Photon.EnetPeer::QueueSentCommand(ExitGames.Client.Photon.NCommand)
extern void EnetPeer_QueueSentCommand_mC93E9F41EE58832860DC309380EA1D8BE7F5F8E4 ();
// 0x00000117 System.Void ExitGames.Client.Photon.EnetPeer::QueueOutgoingReliableCommand(ExitGames.Client.Photon.NCommand)
extern void EnetPeer_QueueOutgoingReliableCommand_mD406D1F04F18EBCC40C22D4D5CDAAC1247C1FD00 ();
// 0x00000118 System.Void ExitGames.Client.Photon.EnetPeer::QueueOutgoingUnreliableCommand(ExitGames.Client.Photon.NCommand)
extern void EnetPeer_QueueOutgoingUnreliableCommand_m474B26B6976278F8563DA33EF5F83081DF93BC3F ();
// 0x00000119 System.Void ExitGames.Client.Photon.EnetPeer::QueueOutgoingAcknowledgement(ExitGames.Client.Photon.NCommand,System.Int32)
extern void EnetPeer_QueueOutgoingAcknowledgement_mFB629A0EFFE9F0DA90C0D48E747ACA9D98197416 ();
// 0x0000011A System.Void ExitGames.Client.Photon.EnetPeer::ReceiveIncomingCommands(System.Byte[],System.Int32)
extern void EnetPeer_ReceiveIncomingCommands_mD89F882DFEE9B00D8255FCAFD688ABA8EF216067 ();
// 0x0000011B System.Boolean ExitGames.Client.Photon.EnetPeer::ExecuteCommand(ExitGames.Client.Photon.NCommand)
extern void EnetPeer_ExecuteCommand_mF19A2B9B0FC53644D850610FE4AFC3DAF4D3E352 ();
// 0x0000011C System.Boolean ExitGames.Client.Photon.EnetPeer::QueueIncomingCommand(ExitGames.Client.Photon.NCommand)
extern void EnetPeer_QueueIncomingCommand_m5D64C855E3F9CB02EDFE7BF017AD8EA478FAEEEC ();
// 0x0000011D ExitGames.Client.Photon.NCommand ExitGames.Client.Photon.EnetPeer::RemoveSentReliableCommand(System.Int32,System.Int32,System.Boolean)
extern void EnetPeer_RemoveSentReliableCommand_mF75DBC5760D184F9BEE6B004EED8087E97913696 ();
// 0x0000011E System.Void ExitGames.Client.Photon.EnetPeer::.cctor()
extern void EnetPeer__cctor_m79DF68D6A8AD8704C58922F524E758B274FADDAF ();
// 0x0000011F System.Void ExitGames.Client.Photon.EnetPeer::<ExecuteCommand>b__71_0()
extern void EnetPeer_U3CExecuteCommandU3Eb__71_0_mF27C21CA35A2A52AFF5B94974A8EE2BC90FE3B2B ();
// 0x00000120 System.Void ExitGames.Client.Photon.EnetChannel::.ctor(System.Byte,System.Int32)
extern void EnetChannel__ctor_m9809886C8DC822F3C7EEC4996AC816414371C71B ();
// 0x00000121 System.Boolean ExitGames.Client.Photon.EnetChannel::ContainsUnreliableSequenceNumber(System.Int32)
extern void EnetChannel_ContainsUnreliableSequenceNumber_mA159FFC30FCA2FB55F40BF17A1647F929E221838 ();
// 0x00000122 System.Boolean ExitGames.Client.Photon.EnetChannel::ContainsReliableSequenceNumber(System.Int32)
extern void EnetChannel_ContainsReliableSequenceNumber_m7A44850F1DB28C723A20E95D0C9AC74B225B8124 ();
// 0x00000123 ExitGames.Client.Photon.NCommand ExitGames.Client.Photon.EnetChannel::FetchReliableSequenceNumber(System.Int32)
extern void EnetChannel_FetchReliableSequenceNumber_mDB2427D6F91A8C73F121918EFCF32DDA21496BA3 ();
// 0x00000124 System.Boolean ExitGames.Client.Photon.EnetChannel::TryGetFragment(System.Int32,System.Boolean,ExitGames.Client.Photon.NCommand&)
extern void EnetChannel_TryGetFragment_m2438ADDC5D5095540ADD7351B8741DDA160FA2DD ();
// 0x00000125 System.Void ExitGames.Client.Photon.EnetChannel::RemoveFragment(System.Int32,System.Boolean)
extern void EnetChannel_RemoveFragment_m2869A04E6476AEBC683E131C857119298310447A ();
// 0x00000126 System.Void ExitGames.Client.Photon.EnetChannel::clearAll()
extern void EnetChannel_clearAll_m1DC1086F63C3C8D3DF1358D650C6D20667E364EC ();
// 0x00000127 System.Boolean ExitGames.Client.Photon.EnetChannel::QueueIncomingReliableUnsequenced(ExitGames.Client.Photon.NCommand)
extern void EnetChannel_QueueIncomingReliableUnsequenced_mE54FB77CA623CE443EF26FC2EAB0CA4619B821FB ();
// 0x00000128 System.Int32 ExitGames.Client.Photon.NCommand::get_SizeOfPayload()
extern void NCommand_get_SizeOfPayload_m5D453FED7B59577C1C947F7B52EE0BB2483B00B5 ();
// 0x00000129 System.Boolean ExitGames.Client.Photon.NCommand::get_IsFlaggedUnsequenced()
extern void NCommand_get_IsFlaggedUnsequenced_m42113687B85BEA75053F662EC7996EFA85C6D043 ();
// 0x0000012A System.Boolean ExitGames.Client.Photon.NCommand::get_IsFlaggedReliable()
extern void NCommand_get_IsFlaggedReliable_mE079B022D8FCEC2E41FBF55327C535696B78201B ();
// 0x0000012B System.Void ExitGames.Client.Photon.NCommand::.ctor(ExitGames.Client.Photon.EnetPeer,System.Byte,ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void NCommand__ctor_m5D475113D2712F02C458550C88FEEA3EC3FC9623 ();
// 0x0000012C System.Void ExitGames.Client.Photon.NCommand::CreateAck(System.Byte[],System.Int32,ExitGames.Client.Photon.NCommand,System.Int32)
extern void NCommand_CreateAck_m8CEEC27DE1D315696E00F47D28CB5A7EB6B29204 ();
// 0x0000012D System.Void ExitGames.Client.Photon.NCommand::.ctor(ExitGames.Client.Photon.EnetPeer,System.Byte[],System.Int32&)
extern void NCommand__ctor_m9B54AE53932683F81A579FF01438D14E20456FA2 ();
// 0x0000012E System.Void ExitGames.Client.Photon.NCommand::SerializeHeader(System.Byte[],System.Int32&)
extern void NCommand_SerializeHeader_m682386C18CFE0BD32AB3F432D84FE6554CDCC78B ();
// 0x0000012F System.Byte[] ExitGames.Client.Photon.NCommand::Serialize()
extern void NCommand_Serialize_m8EEB3FC26E807DA04649171A1C27F6D4EE900754 ();
// 0x00000130 System.Void ExitGames.Client.Photon.NCommand::FreePayload()
extern void NCommand_FreePayload_mCDF9213F68CD0CEB4B707FDBA18FB104CE53E48D ();
// 0x00000131 System.Int32 ExitGames.Client.Photon.NCommand::CompareTo(ExitGames.Client.Photon.NCommand)
extern void NCommand_CompareTo_mF886FF1046C4323DC7E78BFB0FF0DAB946386A47 ();
// 0x00000132 System.String ExitGames.Client.Photon.NCommand::ToString()
extern void NCommand_ToString_m089E773F2F24190D8625E6869139B0979CEAF2DC ();
// 0x00000133 System.Void ExitGames.Client.Photon.TPeer::.ctor()
extern void TPeer__ctor_mC978FB8C22DA016B53F0B6866C8B1694B2D8EB7D ();
// 0x00000134 System.Void ExitGames.Client.Photon.TPeer::InitPeerBase()
extern void TPeer_InitPeerBase_m155C5D48B50B6328AA9CBEE96247C6C41AF3B1F0 ();
// 0x00000135 System.Boolean ExitGames.Client.Photon.TPeer::Connect(System.String,System.String,System.Object)
extern void TPeer_Connect_m1E6B60187F8FF172B4B828E773066E0C841B9E97 ();
// 0x00000136 System.Void ExitGames.Client.Photon.TPeer::OnConnect()
extern void TPeer_OnConnect_m7878A5993B2BDEA0BFDC103000672F3DA73589AF ();
// 0x00000137 System.Void ExitGames.Client.Photon.TPeer::Disconnect()
extern void TPeer_Disconnect_m2F59E0592F77D0B0E3549A20DFC78D05F73589DB ();
// 0x00000138 System.Void ExitGames.Client.Photon.TPeer::StopConnection()
extern void TPeer_StopConnection_m6FC45F4BADAB138CF0682FD1A2A0E3F6741DBF7B ();
// 0x00000139 System.Void ExitGames.Client.Photon.TPeer::FetchServerTimestamp()
extern void TPeer_FetchServerTimestamp_m53E65FABF237D447916293ADE3AEAB4813090F27 ();
// 0x0000013A System.Void ExitGames.Client.Photon.TPeer::EnqueueInit(System.Byte[])
extern void TPeer_EnqueueInit_mF10041F585220020438730E6788B6DCDB55C7941 ();
// 0x0000013B System.Boolean ExitGames.Client.Photon.TPeer::DispatchIncomingCommands()
extern void TPeer_DispatchIncomingCommands_m92BF484E80C1E529802CDB85F9EE71D49514E06A ();
// 0x0000013C System.Boolean ExitGames.Client.Photon.TPeer::SendOutgoingCommands()
extern void TPeer_SendOutgoingCommands_mFDD5AC837638CEC82EF87402F66EE6BAFF32D06E ();
// 0x0000013D System.Boolean ExitGames.Client.Photon.TPeer::SendAcksOnly()
extern void TPeer_SendAcksOnly_mD0A7DC8CAA7C0B8AAB4A519175B870EABDFA09A3 ();
// 0x0000013E System.Boolean ExitGames.Client.Photon.TPeer::EnqueueOperation(System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Byte,ExitGames.Client.Photon.SendOptions,ExitGames.Client.Photon.EgMessageType)
extern void TPeer_EnqueueOperation_m3464B044EF3995C436B175E28BC85F5C27AACEDC ();
// 0x0000013F ExitGames.Client.Photon.StreamBuffer ExitGames.Client.Photon.TPeer::SerializeOperationToMessage(System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,ExitGames.Client.Photon.EgMessageType,System.Boolean)
extern void TPeer_SerializeOperationToMessage_mD8378F051AABADE1C9E29312A20EF60EDB2F2AB0 ();
// 0x00000140 System.Boolean ExitGames.Client.Photon.TPeer::EnqueueMessageAsPayload(ExitGames.Client.Photon.DeliveryMode,ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void TPeer_EnqueueMessageAsPayload_mE7A99ED180941FE5B2081A0092E6057611605E8D ();
// 0x00000141 System.Void ExitGames.Client.Photon.TPeer::SendPing()
extern void TPeer_SendPing_m0F73ABE4C4E09F96D49D99D32B0AE959539C4C83 ();
// 0x00000142 System.Void ExitGames.Client.Photon.TPeer::SendData(System.Byte[],System.Int32)
extern void TPeer_SendData_m00C955E5184E0B8BFBD38E83F9934A5F5CE861FA ();
// 0x00000143 System.Void ExitGames.Client.Photon.TPeer::ReceiveIncomingCommands(System.Byte[],System.Int32)
extern void TPeer_ReceiveIncomingCommands_m74AD3B904C13D114D40FF2521D7DE53860CCDE26 ();
// 0x00000144 System.Void ExitGames.Client.Photon.TPeer::ReadPingResult(System.Byte[])
extern void TPeer_ReadPingResult_mFA183D9868E97D1F32D506AFADBAAE9656280A97 ();
// 0x00000145 System.Void ExitGames.Client.Photon.TPeer::ReadPingResult(ExitGames.Client.Photon.OperationResponse)
extern void TPeer_ReadPingResult_m4A32154C796613F24A06E6FD36661F98B17E8EB5 ();
// 0x00000146 System.Void ExitGames.Client.Photon.TPeer::.cctor()
extern void TPeer__cctor_mAE2AEB3DD0194417AD845595E60210C302F27118 ();
// 0x00000147 ExitGames.Client.Photon.IProtocol ExitGames.Client.Photon.SerializationProtocolFactory::Create(ExitGames.Client.Photon.SerializationProtocol)
extern void SerializationProtocolFactory_Create_m09C1332144808DDC2EF6D846AE0D5DCF1A3B7E32 ();
// 0x00000148 System.String ExitGames.Client.Photon.IProtocol::get_ProtocolType()
// 0x00000149 System.Byte[] ExitGames.Client.Photon.IProtocol::get_VersionBytes()
// 0x0000014A System.Void ExitGames.Client.Photon.IProtocol::Serialize(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
// 0x0000014B System.Void ExitGames.Client.Photon.IProtocol::SerializeShort(ExitGames.Client.Photon.StreamBuffer,System.Int16,System.Boolean)
// 0x0000014C System.Void ExitGames.Client.Photon.IProtocol::SerializeString(ExitGames.Client.Photon.StreamBuffer,System.String,System.Boolean)
// 0x0000014D System.Void ExitGames.Client.Photon.IProtocol::SerializeEventData(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.EventData,System.Boolean)
// 0x0000014E System.Void ExitGames.Client.Photon.IProtocol::SerializeOperationRequest(ExitGames.Client.Photon.StreamBuffer,System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean)
// 0x0000014F System.Void ExitGames.Client.Photon.IProtocol::SerializeOperationResponse(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.OperationResponse,System.Boolean)
// 0x00000150 System.Object ExitGames.Client.Photon.IProtocol::Deserialize(ExitGames.Client.Photon.StreamBuffer,System.Byte)
// 0x00000151 System.Int16 ExitGames.Client.Photon.IProtocol::DeserializeShort(ExitGames.Client.Photon.StreamBuffer)
// 0x00000152 System.Byte ExitGames.Client.Photon.IProtocol::DeserializeByte(ExitGames.Client.Photon.StreamBuffer)
// 0x00000153 ExitGames.Client.Photon.EventData ExitGames.Client.Photon.IProtocol::DeserializeEventData(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.EventData)
// 0x00000154 ExitGames.Client.Photon.OperationRequest ExitGames.Client.Photon.IProtocol::DeserializeOperationRequest(ExitGames.Client.Photon.StreamBuffer)
// 0x00000155 ExitGames.Client.Photon.OperationResponse ExitGames.Client.Photon.IProtocol::DeserializeOperationResponse(ExitGames.Client.Photon.StreamBuffer)
// 0x00000156 System.Byte[] ExitGames.Client.Photon.IProtocol::Serialize(System.Object)
extern void IProtocol_Serialize_m032815F3087BEC52DAF2FADA759EB9FC02E86D65 ();
// 0x00000157 System.Object ExitGames.Client.Photon.IProtocol::DeserializeMessage(ExitGames.Client.Photon.StreamBuffer)
extern void IProtocol_DeserializeMessage_m9E8B1893F2542A56F8BAC2E2E43B1E71A4D87632 ();
// 0x00000158 System.Void ExitGames.Client.Photon.IProtocol::.ctor()
extern void IProtocol__ctor_mBF064D333DD172B2AD7D9BB684BB10D34CA1AA8C ();
// 0x00000159 System.Void ExitGames.Client.Photon.OperationRequest::.ctor()
extern void OperationRequest__ctor_m2CCE6648C58100E08C7E664C90A41C99EBAB62D6 ();
// 0x0000015A System.Object ExitGames.Client.Photon.OperationResponse::get_Item(System.Byte)
extern void OperationResponse_get_Item_m3C3F6BDDB750CF51D887F5D29FDA402DABFA6E38 ();
// 0x0000015B System.String ExitGames.Client.Photon.OperationResponse::ToString()
extern void OperationResponse_ToString_mB9335FAE8C266361E8FE02FC6051374B7599E724 ();
// 0x0000015C System.String ExitGames.Client.Photon.OperationResponse::ToStringFull()
extern void OperationResponse_ToStringFull_mB998F0712031113D2239A21059C59567B12F463C ();
// 0x0000015D System.Void ExitGames.Client.Photon.OperationResponse::.ctor()
extern void OperationResponse__ctor_mB702440274BFBCE5293A3B25BD46550C990EACF6 ();
// 0x0000015E System.Object ExitGames.Client.Photon.EventData::get_Item(System.Byte)
extern void EventData_get_Item_m8DABB0E7B8B21377AA5842DAF82E1828F2B2F728 ();
// 0x0000015F System.Int32 ExitGames.Client.Photon.EventData::get_Sender()
extern void EventData_get_Sender_mD3182863B51AA76D316626308334753F90025255 ();
// 0x00000160 System.Object ExitGames.Client.Photon.EventData::get_CustomData()
extern void EventData_get_CustomData_mB502B0552037F132F4CF131850141F835826060C ();
// 0x00000161 System.Void ExitGames.Client.Photon.EventData::Reset()
extern void EventData_Reset_m399519D3A2528CA5FDAB55616F1D568A631B4EDE ();
// 0x00000162 System.String ExitGames.Client.Photon.EventData::ToString()
extern void EventData_ToString_mC92D919944D5593674A9DA3FDE2C2DF8175F4F64 ();
// 0x00000163 System.Void ExitGames.Client.Photon.EventData::.ctor()
extern void EventData__ctor_mDEC4EFF4DA13212388C984C8D794E5E83E3E826B ();
// 0x00000164 System.Void ExitGames.Client.Photon.SerializeMethod::.ctor(System.Object,System.IntPtr)
extern void SerializeMethod__ctor_m31A10C573F1E426CB601759D689FE1769D0170CA ();
// 0x00000165 System.Byte[] ExitGames.Client.Photon.SerializeMethod::Invoke(System.Object)
extern void SerializeMethod_Invoke_mFC77F6F80C0F4919655054760BBD667C13B69E62 ();
// 0x00000166 System.IAsyncResult ExitGames.Client.Photon.SerializeMethod::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern void SerializeMethod_BeginInvoke_mB8E3AC52E7CB89709059AE76730AD5A818B7EC45 ();
// 0x00000167 System.Byte[] ExitGames.Client.Photon.SerializeMethod::EndInvoke(System.IAsyncResult)
extern void SerializeMethod_EndInvoke_m58345D95D3CEF61D75453C5F4D32B04851EEEB69 ();
// 0x00000168 System.Void ExitGames.Client.Photon.SerializeStreamMethod::.ctor(System.Object,System.IntPtr)
extern void SerializeStreamMethod__ctor_m62DDB8F9059743ADF1212DE8107F40ECC077D773 ();
// 0x00000169 System.Int16 ExitGames.Client.Photon.SerializeStreamMethod::Invoke(ExitGames.Client.Photon.StreamBuffer,System.Object)
extern void SerializeStreamMethod_Invoke_mDDF000B9E8494155E55E59B288438CC496B6566C ();
// 0x0000016A System.IAsyncResult ExitGames.Client.Photon.SerializeStreamMethod::BeginInvoke(ExitGames.Client.Photon.StreamBuffer,System.Object,System.AsyncCallback,System.Object)
extern void SerializeStreamMethod_BeginInvoke_m3111B38CB027B8CE7643ECA8A00AD47C8CBF44A1 ();
// 0x0000016B System.Int16 ExitGames.Client.Photon.SerializeStreamMethod::EndInvoke(System.IAsyncResult)
extern void SerializeStreamMethod_EndInvoke_mDEA54F874FC3C3909EE142FCFD38AE1EE0399EDB ();
// 0x0000016C System.Void ExitGames.Client.Photon.DeserializeMethod::.ctor(System.Object,System.IntPtr)
extern void DeserializeMethod__ctor_m0AD9010015F9B1482A572176647C9C74AC87B6DF ();
// 0x0000016D System.Object ExitGames.Client.Photon.DeserializeMethod::Invoke(System.Byte[])
extern void DeserializeMethod_Invoke_m80C94E8D9D4EE94D70254B175121D7D3DC7086DD ();
// 0x0000016E System.IAsyncResult ExitGames.Client.Photon.DeserializeMethod::BeginInvoke(System.Byte[],System.AsyncCallback,System.Object)
extern void DeserializeMethod_BeginInvoke_m615EDEC58CE77DD95D795828F42D18869BD98B7D ();
// 0x0000016F System.Object ExitGames.Client.Photon.DeserializeMethod::EndInvoke(System.IAsyncResult)
extern void DeserializeMethod_EndInvoke_m1B493BBB64859172D5F492ED208BADBCCA472975 ();
// 0x00000170 System.Void ExitGames.Client.Photon.DeserializeStreamMethod::.ctor(System.Object,System.IntPtr)
extern void DeserializeStreamMethod__ctor_m7217FF56F768B5C35297BEE05F684DECE7FC5153 ();
// 0x00000171 System.Object ExitGames.Client.Photon.DeserializeStreamMethod::Invoke(ExitGames.Client.Photon.StreamBuffer,System.Int16)
extern void DeserializeStreamMethod_Invoke_m0722D9CFBA30747E6D395AD40FCEE38B5DDA27BD ();
// 0x00000172 System.IAsyncResult ExitGames.Client.Photon.DeserializeStreamMethod::BeginInvoke(ExitGames.Client.Photon.StreamBuffer,System.Int16,System.AsyncCallback,System.Object)
extern void DeserializeStreamMethod_BeginInvoke_m86C41EF7632009A58738AFF8838D4EB68150E484 ();
// 0x00000173 System.Object ExitGames.Client.Photon.DeserializeStreamMethod::EndInvoke(System.IAsyncResult)
extern void DeserializeStreamMethod_EndInvoke_m47CE8A6D087D697C82F8D627E8AC166C29F94B86 ();
// 0x00000174 System.Void ExitGames.Client.Photon.CustomType::.ctor(System.Type,System.Byte,ExitGames.Client.Photon.SerializeStreamMethod,ExitGames.Client.Photon.DeserializeStreamMethod)
extern void CustomType__ctor_m050B94D1713B52A53721079532311A0190889881 ();
// 0x00000175 System.Boolean ExitGames.Client.Photon.Protocol::TryRegisterType(System.Type,System.Byte,ExitGames.Client.Photon.SerializeStreamMethod,ExitGames.Client.Photon.DeserializeStreamMethod)
extern void Protocol_TryRegisterType_m5D502BBDFA7C1FE7AF01804710164DBF3222733B ();
// 0x00000176 System.Void ExitGames.Client.Photon.Protocol::Serialize(System.Int16,System.Byte[],System.Int32&)
extern void Protocol_Serialize_mBAFDF59241DBC2D5AF21FC4BE5A40E2520897EC8 ();
// 0x00000177 System.Void ExitGames.Client.Photon.Protocol::Serialize(System.Int32,System.Byte[],System.Int32&)
extern void Protocol_Serialize_mA541EF2B245999813353D445FD9A666FB2B16EA4 ();
// 0x00000178 System.Void ExitGames.Client.Photon.Protocol::Serialize(System.Single,System.Byte[],System.Int32&)
extern void Protocol_Serialize_m03A37FAA6695AB344985B02DFD6010DBF0FC9107 ();
// 0x00000179 System.Void ExitGames.Client.Photon.Protocol::Deserialize(System.Int32&,System.Byte[],System.Int32&)
extern void Protocol_Deserialize_mC3BEF234327D479EE08903C244DB09F1049267D3 ();
// 0x0000017A System.Void ExitGames.Client.Photon.Protocol::Deserialize(System.Int16&,System.Byte[],System.Int32&)
extern void Protocol_Deserialize_mBE2E567D607AAA500EC4B74D52981AD474CA2BDA ();
// 0x0000017B System.Void ExitGames.Client.Photon.Protocol::Deserialize(System.Single&,System.Byte[],System.Int32&)
extern void Protocol_Deserialize_mEC010A79640C90AD545EFA1338A8BEF895FD2EA0 ();
// 0x0000017C System.Void ExitGames.Client.Photon.Protocol::.cctor()
extern void Protocol__cctor_m64884E2DA6FB8A64181CD0409B9593634D1E18AE ();
// 0x0000017D System.String ExitGames.Client.Photon.Protocol16::get_ProtocolType()
extern void Protocol16_get_ProtocolType_mEF3335B4B343053A2193DAE59F8AD21F8FB16537 ();
// 0x0000017E System.Byte[] ExitGames.Client.Photon.Protocol16::get_VersionBytes()
extern void Protocol16_get_VersionBytes_mDCD6CC44785D2317A4C75F33849EA1BACE5625AE ();
// 0x0000017F System.Boolean ExitGames.Client.Photon.Protocol16::SerializeCustom(ExitGames.Client.Photon.StreamBuffer,System.Object)
extern void Protocol16_SerializeCustom_m03F4278E496D356865CAC1321A555488A9A4FE66 ();
// 0x00000180 System.Object ExitGames.Client.Photon.Protocol16::DeserializeCustom(ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void Protocol16_DeserializeCustom_mAC30492952B0F44FB89EA161921CF17BD343AECD ();
// 0x00000181 System.Type ExitGames.Client.Photon.Protocol16::GetTypeOfCode(System.Byte)
extern void Protocol16_GetTypeOfCode_m9D37CBD9D4A1EFB8AE06C3B3D9EAF0D4BB55F5D9 ();
// 0x00000182 ExitGames.Client.Photon.Protocol16_GpType ExitGames.Client.Photon.Protocol16::GetCodeOfType(System.Type)
extern void Protocol16_GetCodeOfType_m29ABC36AA1207F5DF9B8311984A6234F592C4F52 ();
// 0x00000183 System.Array ExitGames.Client.Photon.Protocol16::CreateArrayByType(System.Byte,System.Int16)
extern void Protocol16_CreateArrayByType_m8B3FB1BE7CC0C2EE962ECDBEF5AAC2EDCA8AFFA1 ();
// 0x00000184 System.Void ExitGames.Client.Photon.Protocol16::SerializeOperationRequest(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.OperationRequest,System.Boolean)
extern void Protocol16_SerializeOperationRequest_m81757C144B536C26FCC01DECBEBC03DB6E98C355 ();
// 0x00000185 System.Void ExitGames.Client.Photon.Protocol16::SerializeOperationRequest(ExitGames.Client.Photon.StreamBuffer,System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean)
extern void Protocol16_SerializeOperationRequest_m4EBDF610C96D8587C68F4C714A6724E0FD78A0BF ();
// 0x00000186 ExitGames.Client.Photon.OperationRequest ExitGames.Client.Photon.Protocol16::DeserializeOperationRequest(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeOperationRequest_mEBD2710E5DC7703FCE45253AB1F2A05E0EFDE0D5 ();
// 0x00000187 System.Void ExitGames.Client.Photon.Protocol16::SerializeOperationResponse(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.OperationResponse,System.Boolean)
extern void Protocol16_SerializeOperationResponse_m36DCCD424ADECA4E02FFE2C1942A1B64753E90F4 ();
// 0x00000188 ExitGames.Client.Photon.OperationResponse ExitGames.Client.Photon.Protocol16::DeserializeOperationResponse(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeOperationResponse_mB41FFDC25306804310FE963ECBD9A5411C5FA06B ();
// 0x00000189 System.Void ExitGames.Client.Photon.Protocol16::SerializeEventData(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.EventData,System.Boolean)
extern void Protocol16_SerializeEventData_mC5F8C026E7421669C9E5F005C836B12E62935482 ();
// 0x0000018A ExitGames.Client.Photon.EventData ExitGames.Client.Photon.Protocol16::DeserializeEventData(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.EventData)
extern void Protocol16_DeserializeEventData_m0B1A645ACB7747B78982566C8457DAABF3816357 ();
// 0x0000018B System.Void ExitGames.Client.Photon.Protocol16::SerializeParameterTable(ExitGames.Client.Photon.StreamBuffer,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>)
extern void Protocol16_SerializeParameterTable_m5187051C8DEDE687D8CD5A7A48180DBF9A7BD557 ();
// 0x0000018C System.Collections.Generic.Dictionary`2<System.Byte,System.Object> ExitGames.Client.Photon.Protocol16::DeserializeParameterTable(ExitGames.Client.Photon.StreamBuffer,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>)
extern void Protocol16_DeserializeParameterTable_m345F15648330E20C16944D5021F4228359FF5118 ();
// 0x0000018D System.Void ExitGames.Client.Photon.Protocol16::Serialize(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol16_Serialize_m6D5D5506FC7019CCF8A5EAD3844FB2EFC818C04D ();
// 0x0000018E System.Void ExitGames.Client.Photon.Protocol16::SerializeByte(ExitGames.Client.Photon.StreamBuffer,System.Byte,System.Boolean)
extern void Protocol16_SerializeByte_m46FEB067F4509A57EF9A6461680888C3F3A8EA3A ();
// 0x0000018F System.Void ExitGames.Client.Photon.Protocol16::SerializeBoolean(ExitGames.Client.Photon.StreamBuffer,System.Boolean,System.Boolean)
extern void Protocol16_SerializeBoolean_mFFF768664B7F2885AD5025969B03E5841D5F220A ();
// 0x00000190 System.Void ExitGames.Client.Photon.Protocol16::SerializeShort(ExitGames.Client.Photon.StreamBuffer,System.Int16,System.Boolean)
extern void Protocol16_SerializeShort_m4D8A2CF9D94C2651CB465266603A2FC06AD09DAD ();
// 0x00000191 System.Void ExitGames.Client.Photon.Protocol16::SerializeInteger(ExitGames.Client.Photon.StreamBuffer,System.Int32,System.Boolean)
extern void Protocol16_SerializeInteger_mAF92DE73951AC804D903765A899FA7CF2EAD7988 ();
// 0x00000192 System.Void ExitGames.Client.Photon.Protocol16::SerializeLong(ExitGames.Client.Photon.StreamBuffer,System.Int64,System.Boolean)
extern void Protocol16_SerializeLong_m5B9E736D283B8A87D223E88229583F702C6A5778 ();
// 0x00000193 System.Void ExitGames.Client.Photon.Protocol16::SerializeFloat(ExitGames.Client.Photon.StreamBuffer,System.Single,System.Boolean)
extern void Protocol16_SerializeFloat_mC4BB247E04CF12C664F2010ECBF76C27C08C046C ();
// 0x00000194 System.Void ExitGames.Client.Photon.Protocol16::SerializeDouble(ExitGames.Client.Photon.StreamBuffer,System.Double,System.Boolean)
extern void Protocol16_SerializeDouble_mF13D6D27DB4F0CD564EBF9EAAE3DB4EF8FAD49CA ();
// 0x00000195 System.Void ExitGames.Client.Photon.Protocol16::SerializeString(ExitGames.Client.Photon.StreamBuffer,System.String,System.Boolean)
extern void Protocol16_SerializeString_mC78290BFDCE1ADB519A9ADD11045534C3095E197 ();
// 0x00000196 System.Void ExitGames.Client.Photon.Protocol16::SerializeArray(ExitGames.Client.Photon.StreamBuffer,System.Array,System.Boolean)
extern void Protocol16_SerializeArray_m16BD43873E4ED1208B367ABB69A38B25843E824D ();
// 0x00000197 System.Void ExitGames.Client.Photon.Protocol16::SerializeByteArray(ExitGames.Client.Photon.StreamBuffer,System.Byte[],System.Boolean)
extern void Protocol16_SerializeByteArray_mCFFF9506539F331AC7339C1BF8B14939CC38E9D8 ();
// 0x00000198 System.Void ExitGames.Client.Photon.Protocol16::SerializeByteArraySegment(ExitGames.Client.Photon.StreamBuffer,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void Protocol16_SerializeByteArraySegment_m5F70C7E987F02C742AA489957B7C9C0D9FFC23C6 ();
// 0x00000199 System.Void ExitGames.Client.Photon.Protocol16::SerializeIntArrayOptimized(ExitGames.Client.Photon.StreamBuffer,System.Int32[],System.Boolean)
extern void Protocol16_SerializeIntArrayOptimized_mBFB2D3E8243D449897207A33276C662D34F39E58 ();
// 0x0000019A System.Void ExitGames.Client.Photon.Protocol16::SerializeObjectArray(ExitGames.Client.Photon.StreamBuffer,System.Collections.IList,System.Boolean)
extern void Protocol16_SerializeObjectArray_mE7F958D827723400DBDB4823541FA4953266B6AE ();
// 0x0000019B System.Void ExitGames.Client.Photon.Protocol16::SerializeHashTable(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.Hashtable,System.Boolean)
extern void Protocol16_SerializeHashTable_m54301A664259057E0BCD7A5948B1D00251C05D34 ();
// 0x0000019C System.Void ExitGames.Client.Photon.Protocol16::SerializeDictionary(ExitGames.Client.Photon.StreamBuffer,System.Collections.IDictionary,System.Boolean)
extern void Protocol16_SerializeDictionary_mB856FF8F65235D7A0514098CDF121623ABDC6B2C ();
// 0x0000019D System.Void ExitGames.Client.Photon.Protocol16::SerializeDictionaryHeader(ExitGames.Client.Photon.StreamBuffer,System.Type)
extern void Protocol16_SerializeDictionaryHeader_m5803CE6DC0DD7459D30D28592802E889A8A0E278 ();
// 0x0000019E System.Void ExitGames.Client.Photon.Protocol16::SerializeDictionaryHeader(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean&,System.Boolean&)
extern void Protocol16_SerializeDictionaryHeader_mC4D9588211B9680862879BBABCFDD9A09D420862 ();
// 0x0000019F System.Void ExitGames.Client.Photon.Protocol16::SerializeDictionaryElements(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean,System.Boolean)
extern void Protocol16_SerializeDictionaryElements_mF75828C38FA06044C375190CBFAE570738C103DF ();
// 0x000001A0 System.Object ExitGames.Client.Photon.Protocol16::Deserialize(ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void Protocol16_Deserialize_m00281AFE308E39580DD669AFBA0F95B76B189896 ();
// 0x000001A1 System.Byte ExitGames.Client.Photon.Protocol16::DeserializeByte(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeByte_m99E00B3A651FD62DC6F1015EF95C6411D047CB8C ();
// 0x000001A2 System.Boolean ExitGames.Client.Photon.Protocol16::DeserializeBoolean(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeBoolean_m5B863A011742C3275F4FDED8960B1C2B5B094E18 ();
// 0x000001A3 System.Int16 ExitGames.Client.Photon.Protocol16::DeserializeShort(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeShort_mD420A1DC9319D892CA3BC2D8C842EC8B2F48A7F0 ();
// 0x000001A4 System.Int32 ExitGames.Client.Photon.Protocol16::DeserializeInteger(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeInteger_m6BB87CFBA3E9D35E783E92006C1D01B7D6B20002 ();
// 0x000001A5 System.Int64 ExitGames.Client.Photon.Protocol16::DeserializeLong(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeLong_m0A5A359E00C8D26A37809978E9DDDF156A5501A9 ();
// 0x000001A6 System.Single ExitGames.Client.Photon.Protocol16::DeserializeFloat(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeFloat_mCBEB4FE45A712381B6DC457D24676D526623E814 ();
// 0x000001A7 System.Double ExitGames.Client.Photon.Protocol16::DeserializeDouble(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeDouble_m78AAC151A5C89CB259ADD5E2BC502D0677604007 ();
// 0x000001A8 System.String ExitGames.Client.Photon.Protocol16::DeserializeString(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeString_m768E351F55F5039007B63DF791E80897A2CF9BE3 ();
// 0x000001A9 System.Array ExitGames.Client.Photon.Protocol16::DeserializeArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeArray_mC317F49B9C7FCDDA631B582732E4E7DCA78E1BFC ();
// 0x000001AA System.Byte[] ExitGames.Client.Photon.Protocol16::DeserializeByteArray(ExitGames.Client.Photon.StreamBuffer,System.Int32)
extern void Protocol16_DeserializeByteArray_m6CC3652D4E0A798C9488F6FF66A9AEEE7F2EC0D3 ();
// 0x000001AB System.Int32[] ExitGames.Client.Photon.Protocol16::DeserializeIntArray(ExitGames.Client.Photon.StreamBuffer,System.Int32)
extern void Protocol16_DeserializeIntArray_m689D43E9935374CDCAAFC2D6DA569F18A5BAD623 ();
// 0x000001AC System.String[] ExitGames.Client.Photon.Protocol16::DeserializeStringArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeStringArray_m69872A7A8E1D480976AF233E5CDECADDD95AEA89 ();
// 0x000001AD System.Object[] ExitGames.Client.Photon.Protocol16::DeserializeObjectArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeObjectArray_mC3069BFA6D60FB091E5F90F1153B999900DE7985 ();
// 0x000001AE ExitGames.Client.Photon.Hashtable ExitGames.Client.Photon.Protocol16::DeserializeHashTable(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeHashTable_mDBDD0124A8B59EE1AC01F6ADC2BD18123C05E9BA ();
// 0x000001AF System.Collections.IDictionary ExitGames.Client.Photon.Protocol16::DeserializeDictionary(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol16_DeserializeDictionary_mE43A156237FB40344DD7C55AC4441A9B115B64A9 ();
// 0x000001B0 System.Boolean ExitGames.Client.Photon.Protocol16::DeserializeDictionaryArray(ExitGames.Client.Photon.StreamBuffer,System.Int16,System.Array&)
extern void Protocol16_DeserializeDictionaryArray_m5E9B93F27597D641B9EAB3121988F8FDA12F8A76 ();
// 0x000001B1 System.Type ExitGames.Client.Photon.Protocol16::DeserializeDictionaryType(ExitGames.Client.Photon.StreamBuffer,System.Byte&,System.Byte&)
extern void Protocol16_DeserializeDictionaryType_m5E1E320B4281DA3C92D3AD0AFC479F504C7A8E01 ();
// 0x000001B2 System.Void ExitGames.Client.Photon.Protocol16::.ctor()
extern void Protocol16__ctor_m81031362D9ADAF37E84AF544BA223790BFDC4DB1 ();
// 0x000001B3 System.Void ExitGames.Client.Photon.Protocol16::.cctor()
extern void Protocol16__cctor_m2B1FE742BE2586A2DAAED91533AAF9314B020859 ();
// 0x000001B4 System.Void ExitGames.Client.Photon.InvalidDataException::.ctor(System.String)
extern void InvalidDataException__ctor_mE011B2C659108CE372955D10ECF410D3EAE32D4C ();
// 0x000001B5 System.String ExitGames.Client.Photon.Protocol18::get_ProtocolType()
extern void Protocol18_get_ProtocolType_mB9B5C84ED6340AD2BC40DDF6FEEA78A81E1D7DB7 ();
// 0x000001B6 System.Byte[] ExitGames.Client.Photon.Protocol18::get_VersionBytes()
extern void Protocol18_get_VersionBytes_m6799465BD76B6B0198C39AE3779ABA1E264728AC ();
// 0x000001B7 System.Void ExitGames.Client.Photon.Protocol18::Serialize(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_Serialize_mE131573B59A7FFC97F510494402D24135A86C6C1 ();
// 0x000001B8 System.Void ExitGames.Client.Photon.Protocol18::SerializeShort(ExitGames.Client.Photon.StreamBuffer,System.Int16,System.Boolean)
extern void Protocol18_SerializeShort_mA091B93B1732345BD8B67AC7EFF237D4711050CB ();
// 0x000001B9 System.Void ExitGames.Client.Photon.Protocol18::SerializeString(ExitGames.Client.Photon.StreamBuffer,System.String,System.Boolean)
extern void Protocol18_SerializeString_m28634BAB8108367A37094D6521FE9B3E40EA1877 ();
// 0x000001BA System.Object ExitGames.Client.Photon.Protocol18::Deserialize(ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void Protocol18_Deserialize_m215BE13B261AA318860C1C83CFDD28A244437E60 ();
// 0x000001BB System.Int16 ExitGames.Client.Photon.Protocol18::DeserializeShort(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_DeserializeShort_mF25F5E016FFB96FD17B2FD62F4B076DF9808BF1C ();
// 0x000001BC System.Byte ExitGames.Client.Photon.Protocol18::DeserializeByte(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_DeserializeByte_m307F7BF90033DFDE12FF73097CBBAA2AC9AC64D6 ();
// 0x000001BD System.Type ExitGames.Client.Photon.Protocol18::GetAllowedDictionaryKeyTypes(ExitGames.Client.Photon.Protocol18_GpType)
extern void Protocol18_GetAllowedDictionaryKeyTypes_m6BCFBB8366A1FF4D1247986670C2645C3108F10A ();
// 0x000001BE System.Type ExitGames.Client.Photon.Protocol18::GetClrArrayType(ExitGames.Client.Photon.Protocol18_GpType)
extern void Protocol18_GetClrArrayType_mA6A39B7F5737083092975EF356767153F7562434 ();
// 0x000001BF ExitGames.Client.Photon.Protocol18_GpType ExitGames.Client.Photon.Protocol18::GetCodeOfType(System.Type)
extern void Protocol18_GetCodeOfType_m8FB25C633F98D1F10A8C61EAA60AC3FA52BC6D5E ();
// 0x000001C0 ExitGames.Client.Photon.Protocol18_GpType ExitGames.Client.Photon.Protocol18::GetCodeOfTypeCode(System.TypeCode)
extern void Protocol18_GetCodeOfTypeCode_mEECEE3F5F09D03BD7A4A8CE2EE4FFEB026F9AFF8 ();
// 0x000001C1 System.Object ExitGames.Client.Photon.Protocol18::Read(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_Read_mF066333C7A1AED49DB6B39C0F210C1EA10619169 ();
// 0x000001C2 System.Object ExitGames.Client.Photon.Protocol18::Read(ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void Protocol18_Read_m970EDFA63A97B3C5708EAE3BC25CB880BE3F1E51 ();
// 0x000001C3 System.Boolean ExitGames.Client.Photon.Protocol18::ReadBoolean(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadBoolean_m890DFD9FB3AC915747833B3753BA6B9C17D93435 ();
// 0x000001C4 System.Byte ExitGames.Client.Photon.Protocol18::ReadByte(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadByte_mD38375BB3588EB63E66F7675963F492B9A033532 ();
// 0x000001C5 System.Int16 ExitGames.Client.Photon.Protocol18::ReadInt16(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadInt16_m002D54DDBA732168769C94DCC565DDC99F9E9070 ();
// 0x000001C6 System.UInt16 ExitGames.Client.Photon.Protocol18::ReadUShort(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadUShort_mA09B54D4B2951E19BDF6105FF0A5A72387D8E4C3 ();
// 0x000001C7 System.Single ExitGames.Client.Photon.Protocol18::ReadSingle(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadSingle_m00A801F067FB8AFCD19D0E34A4CC681AC6E80FAB ();
// 0x000001C8 System.Double ExitGames.Client.Photon.Protocol18::ReadDouble(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadDouble_mB53AB954E9F3E31D1FB2F73EE2130BF5CEE08FA7 ();
// 0x000001C9 System.Byte[] ExitGames.Client.Photon.Protocol18::ReadByteArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadByteArray_m5B28D578ED6374A0D2F91B12683AC4422300696C ();
// 0x000001CA System.Object ExitGames.Client.Photon.Protocol18::ReadCustomType(ExitGames.Client.Photon.StreamBuffer,System.Byte)
extern void Protocol18_ReadCustomType_m717E7EBAD098EFD0DF90FEC65A5B89E36DB3D341 ();
// 0x000001CB ExitGames.Client.Photon.EventData ExitGames.Client.Photon.Protocol18::DeserializeEventData(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.EventData)
extern void Protocol18_DeserializeEventData_m5E36311112CE1D161D2260D0D92A3435444D30E8 ();
// 0x000001CC System.Collections.Generic.Dictionary`2<System.Byte,System.Object> ExitGames.Client.Photon.Protocol18::ReadParameterTable(ExitGames.Client.Photon.StreamBuffer,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>)
extern void Protocol18_ReadParameterTable_mFE0ABB79843803ECD0CD84E2B312EC07F23B9CD2 ();
// 0x000001CD ExitGames.Client.Photon.Hashtable ExitGames.Client.Photon.Protocol18::ReadHashtable(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadHashtable_m270DDFF504559C4E08F8437AE91E8DB9EC2D8024 ();
// 0x000001CE ExitGames.Client.Photon.OperationRequest ExitGames.Client.Photon.Protocol18::DeserializeOperationRequest(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_DeserializeOperationRequest_mB3F5B0A8AA2DFFB35B9F4B9CAF4BB4A811831EF3 ();
// 0x000001CF ExitGames.Client.Photon.OperationResponse ExitGames.Client.Photon.Protocol18::DeserializeOperationResponse(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_DeserializeOperationResponse_mE21AA412C0CA74BD2728999EA822ED93832D9289 ();
// 0x000001D0 System.String ExitGames.Client.Photon.Protocol18::ReadString(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadString_mEF6EBD3AFC3098AA2974E1459880B4BB25FBF2DA ();
// 0x000001D1 System.Object ExitGames.Client.Photon.Protocol18::ReadCustomTypeArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCustomTypeArray_mC334FE546D75907F21037548263E86D62824A2A1 ();
// 0x000001D2 System.Type ExitGames.Client.Photon.Protocol18::ReadDictionaryType(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.Protocol18_GpType&,ExitGames.Client.Photon.Protocol18_GpType&)
extern void Protocol18_ReadDictionaryType_m7C7B19B42DB48595EEF3974053046CE8DA604130 ();
// 0x000001D3 System.Type ExitGames.Client.Photon.Protocol18::ReadDictionaryType(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadDictionaryType_m51FFA22A02EB1066D31E8D28EEFF95FC9F04DCA1 ();
// 0x000001D4 System.Type ExitGames.Client.Photon.Protocol18::GetDictArrayType(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_GetDictArrayType_m3A2B993C6419F73F254CC0F1A92A84499A759AAA ();
// 0x000001D5 System.Collections.IDictionary ExitGames.Client.Photon.Protocol18::ReadDictionary(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadDictionary_mAC05B9475802306F0989B69C341AF958CB76AFDC ();
// 0x000001D6 System.Boolean ExitGames.Client.Photon.Protocol18::ReadDictionaryElements(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.Protocol18_GpType,ExitGames.Client.Photon.Protocol18_GpType,System.Collections.IDictionary)
extern void Protocol18_ReadDictionaryElements_m5B67F3E726564069CCA051EECF2018D476896181 ();
// 0x000001D7 System.Object[] ExitGames.Client.Photon.Protocol18::ReadObjectArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadObjectArray_m0477F13A24A4FD18348D01A0C3302D2441335682 ();
// 0x000001D8 System.Boolean[] ExitGames.Client.Photon.Protocol18::ReadBooleanArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadBooleanArray_m7414A38305BB44BDDEA3F1B4E587BA5F80FDE3D6 ();
// 0x000001D9 System.Int16[] ExitGames.Client.Photon.Protocol18::ReadInt16Array(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadInt16Array_m91994BD3B863E4E400AB79ED4ACE7931EFC8AEF3 ();
// 0x000001DA System.Single[] ExitGames.Client.Photon.Protocol18::ReadSingleArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadSingleArray_mE214DDB9B5CCEC270F442032BA93858CD0F96943 ();
// 0x000001DB System.Double[] ExitGames.Client.Photon.Protocol18::ReadDoubleArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadDoubleArray_mA2017D334A3F7F1877FE3B5DFEC9F81DA612222F ();
// 0x000001DC System.String[] ExitGames.Client.Photon.Protocol18::ReadStringArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadStringArray_m7F0869411EBAD2330467E6018F9A951F1D32DAD3 ();
// 0x000001DD ExitGames.Client.Photon.Hashtable[] ExitGames.Client.Photon.Protocol18::ReadHashtableArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadHashtableArray_m9497F293B378E3F141116391FD1D84316B9A21F9 ();
// 0x000001DE System.Collections.IDictionary[] ExitGames.Client.Photon.Protocol18::ReadDictionaryArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadDictionaryArray_mD15335C559B535EAA923062D61AADD457FEE70D1 ();
// 0x000001DF System.Array ExitGames.Client.Photon.Protocol18::ReadArrayInArray(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadArrayInArray_m71ED366F6A1B65B704F75B9746C5F17309A7C85D ();
// 0x000001E0 System.Int32 ExitGames.Client.Photon.Protocol18::ReadInt1(ExitGames.Client.Photon.StreamBuffer,System.Boolean)
extern void Protocol18_ReadInt1_m323DC131E1491BB77010CB35175D9FC642F1548B ();
// 0x000001E1 System.Int32 ExitGames.Client.Photon.Protocol18::ReadInt2(ExitGames.Client.Photon.StreamBuffer,System.Boolean)
extern void Protocol18_ReadInt2_m97FED0CBB704EE232F00F181711F4FAD2A413CB2 ();
// 0x000001E2 System.Int32 ExitGames.Client.Photon.Protocol18::ReadCompressedInt32(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCompressedInt32_mEDAD5B393FC2EBD104EE119AFA9038CAFBB7007D ();
// 0x000001E3 System.UInt32 ExitGames.Client.Photon.Protocol18::ReadCompressedUInt32(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCompressedUInt32_m703B81CFF05540771F926D377D381DB04B1FBA4D ();
// 0x000001E4 System.Int64 ExitGames.Client.Photon.Protocol18::ReadCompressedInt64(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCompressedInt64_m950FC539CCB79F013A2C9032A363E96296D7477E ();
// 0x000001E5 System.UInt64 ExitGames.Client.Photon.Protocol18::ReadCompressedUInt64(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCompressedUInt64_m07B50FA75A9E83203E40818BB1D25958C3C7CD2A ();
// 0x000001E6 System.Int32[] ExitGames.Client.Photon.Protocol18::ReadCompressedInt32Array(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCompressedInt32Array_mA61503C39F20DAE88DE8FD10416B74241AD88FF0 ();
// 0x000001E7 System.Int64[] ExitGames.Client.Photon.Protocol18::ReadCompressedInt64Array(ExitGames.Client.Photon.StreamBuffer)
extern void Protocol18_ReadCompressedInt64Array_m7A08A585BBE5446D45859E5310B92F5EF3AD3F41 ();
// 0x000001E8 System.Int32 ExitGames.Client.Photon.Protocol18::DecodeZigZag32(System.UInt32)
extern void Protocol18_DecodeZigZag32_m6AEFB426AF36C3CC946F440EE161639FC2D8DF3F ();
// 0x000001E9 System.Int64 ExitGames.Client.Photon.Protocol18::DecodeZigZag64(System.UInt64)
extern void Protocol18_DecodeZigZag64_mDE468DA862ABDF892E3752526D689CDC509B9022 ();
// 0x000001EA System.Void ExitGames.Client.Photon.Protocol18::Write(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_Write_m28CA5AC55C1BC975A925B143E4D59CB48B825677 ();
// 0x000001EB System.Void ExitGames.Client.Photon.Protocol18::Write(ExitGames.Client.Photon.StreamBuffer,System.Object,ExitGames.Client.Photon.Protocol18_GpType,System.Boolean)
extern void Protocol18_Write_mB1273F9ADBE7C1F6420BB5E78FE1576932FE0873 ();
// 0x000001EC System.Void ExitGames.Client.Photon.Protocol18::SerializeEventData(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.EventData,System.Boolean)
extern void Protocol18_SerializeEventData_mCD276A62F7A539B1076420125CE44D09798D4161 ();
// 0x000001ED System.Void ExitGames.Client.Photon.Protocol18::WriteParameterTable(ExitGames.Client.Photon.StreamBuffer,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>)
extern void Protocol18_WriteParameterTable_m15339891E0318BA7FFD85BDE67378DBE2A934473 ();
// 0x000001EE System.Void ExitGames.Client.Photon.Protocol18::SerializeOperationRequest(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.OperationRequest,System.Boolean)
extern void Protocol18_SerializeOperationRequest_m6BB4581B489DE8F61EAD5DE8B0CF09CAF89CECCB ();
// 0x000001EF System.Void ExitGames.Client.Photon.Protocol18::SerializeOperationRequest(ExitGames.Client.Photon.StreamBuffer,System.Byte,System.Collections.Generic.Dictionary`2<System.Byte,System.Object>,System.Boolean)
extern void Protocol18_SerializeOperationRequest_m70A5C045585022B1DCD7D64BE27C92637418A3A4 ();
// 0x000001F0 System.Void ExitGames.Client.Photon.Protocol18::SerializeOperationResponse(ExitGames.Client.Photon.StreamBuffer,ExitGames.Client.Photon.OperationResponse,System.Boolean)
extern void Protocol18_SerializeOperationResponse_m5598289DDB276D5A96E8304C6B0390B151B62013 ();
// 0x000001F1 System.Void ExitGames.Client.Photon.Protocol18::WriteByte(ExitGames.Client.Photon.StreamBuffer,System.Byte,System.Boolean)
extern void Protocol18_WriteByte_mFE8AA8996495ECA9DC2740387D949450F436A1A4 ();
// 0x000001F2 System.Void ExitGames.Client.Photon.Protocol18::WriteBoolean(ExitGames.Client.Photon.StreamBuffer,System.Boolean,System.Boolean)
extern void Protocol18_WriteBoolean_m1042C171D0BD9EFD0D6127B111FCD03E9851726A ();
// 0x000001F3 System.Void ExitGames.Client.Photon.Protocol18::WriteUShort(ExitGames.Client.Photon.StreamBuffer,System.UInt16)
extern void Protocol18_WriteUShort_mE75524C0EC98D116092D1F2A685ABBF87AB48AA8 ();
// 0x000001F4 System.Void ExitGames.Client.Photon.Protocol18::WriteInt16(ExitGames.Client.Photon.StreamBuffer,System.Int16,System.Boolean)
extern void Protocol18_WriteInt16_m5DA8138FD04DC30AB88B07F02D3ED1AA84898AF0 ();
// 0x000001F5 System.Void ExitGames.Client.Photon.Protocol18::WriteDouble(ExitGames.Client.Photon.StreamBuffer,System.Double,System.Boolean)
extern void Protocol18_WriteDouble_m6C01426AF813AB32C24FF8F443EBFAF9682D2229 ();
// 0x000001F6 System.Void ExitGames.Client.Photon.Protocol18::WriteSingle(ExitGames.Client.Photon.StreamBuffer,System.Single,System.Boolean)
extern void Protocol18_WriteSingle_mC0A903FD8125905B37D8F845DF792760AC4524DF ();
// 0x000001F7 System.Void ExitGames.Client.Photon.Protocol18::WriteString(ExitGames.Client.Photon.StreamBuffer,System.String,System.Boolean)
extern void Protocol18_WriteString_m1B5FB5527A6827BD43C9F62B9BA8AABB47B8E913 ();
// 0x000001F8 System.Void ExitGames.Client.Photon.Protocol18::WriteHashtable(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteHashtable_m252B4756606686476FC8C60A3B1A83C9E5B44123 ();
// 0x000001F9 System.Void ExitGames.Client.Photon.Protocol18::WriteByteArray(ExitGames.Client.Photon.StreamBuffer,System.Byte[],System.Boolean)
extern void Protocol18_WriteByteArray_m92D081FF5DD5BA7CF115AA1368A07D5B9F477A7F ();
// 0x000001FA System.Void ExitGames.Client.Photon.Protocol18::WriteByteArraySegment(ExitGames.Client.Photon.StreamBuffer,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void Protocol18_WriteByteArraySegment_mAAC60895EFB733F94C6011A1CDEE28D702F2829B ();
// 0x000001FB System.Void ExitGames.Client.Photon.Protocol18::WriteInt32ArrayCompressed(ExitGames.Client.Photon.StreamBuffer,System.Int32[],System.Boolean)
extern void Protocol18_WriteInt32ArrayCompressed_m4B56B8D16B02BA551A5BD7D5B4774AF72D8D021E ();
// 0x000001FC System.Void ExitGames.Client.Photon.Protocol18::WriteInt64ArrayCompressed(ExitGames.Client.Photon.StreamBuffer,System.Int64[],System.Boolean)
extern void Protocol18_WriteInt64ArrayCompressed_m771B7DECF04B3752318FC0C59D03C302850608CA ();
// 0x000001FD System.Void ExitGames.Client.Photon.Protocol18::WriteBoolArray(ExitGames.Client.Photon.StreamBuffer,System.Boolean[],System.Boolean)
extern void Protocol18_WriteBoolArray_m4FD033ED63211050C39D6E0E5B11F784AD5CA08F ();
// 0x000001FE System.Void ExitGames.Client.Photon.Protocol18::WriteInt16Array(ExitGames.Client.Photon.StreamBuffer,System.Int16[],System.Boolean)
extern void Protocol18_WriteInt16Array_mAAFFC1BE1CD51E0B826BD925C22EE73F45DE85DC ();
// 0x000001FF System.Void ExitGames.Client.Photon.Protocol18::WriteSingleArray(ExitGames.Client.Photon.StreamBuffer,System.Single[],System.Boolean)
extern void Protocol18_WriteSingleArray_m9A1E93C1AE31A7CFB7C3C6F64F882C58CF82780C ();
// 0x00000200 System.Void ExitGames.Client.Photon.Protocol18::WriteDoubleArray(ExitGames.Client.Photon.StreamBuffer,System.Double[],System.Boolean)
extern void Protocol18_WriteDoubleArray_mF24BFD14E535751A76DBB750F302F503E6D2A7C9 ();
// 0x00000201 System.Void ExitGames.Client.Photon.Protocol18::WriteStringArray(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteStringArray_m6B2601D69A9E75F79B4E008DB13B29B4EB398C06 ();
// 0x00000202 System.Void ExitGames.Client.Photon.Protocol18::WriteObjectArray(ExitGames.Client.Photon.StreamBuffer,System.Collections.IList,System.Boolean)
extern void Protocol18_WriteObjectArray_m87F0813C8F18506083F9529BD7803AFFC672D369 ();
// 0x00000203 System.Void ExitGames.Client.Photon.Protocol18::WriteArrayInArray(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteArrayInArray_m0832BF6B5DE9D17BC1E579E9DA42518235B26DE4 ();
// 0x00000204 System.Void ExitGames.Client.Photon.Protocol18::WriteCustomTypeBody(ExitGames.Client.Photon.CustomType,ExitGames.Client.Photon.StreamBuffer,System.Object)
extern void Protocol18_WriteCustomTypeBody_m77F0901379D2EE2DBDF248C06A1D41242A48CA1D ();
// 0x00000205 System.Void ExitGames.Client.Photon.Protocol18::WriteCustomType(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteCustomType_mD85A387626FF480228157C9C8F2C64E7F1D92B6E ();
// 0x00000206 System.Void ExitGames.Client.Photon.Protocol18::WriteCustomTypeArray(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteCustomTypeArray_m676F40E94CBA53075184ED7CB970031697662113 ();
// 0x00000207 System.Boolean ExitGames.Client.Photon.Protocol18::WriteArrayHeader(ExitGames.Client.Photon.StreamBuffer,System.Type)
extern void Protocol18_WriteArrayHeader_m776FC13CD7C7BCD2D15060D650F1617F69255C20 ();
// 0x00000208 System.Void ExitGames.Client.Photon.Protocol18::WriteDictionaryElements(ExitGames.Client.Photon.StreamBuffer,System.Collections.IDictionary,ExitGames.Client.Photon.Protocol18_GpType,ExitGames.Client.Photon.Protocol18_GpType)
extern void Protocol18_WriteDictionaryElements_mBBE12B0A16E2524850B58CCC13AE3584B48E1FE0 ();
// 0x00000209 System.Void ExitGames.Client.Photon.Protocol18::WriteDictionary(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteDictionary_mD5C1BCF2C77A8BD1F45C7C97188A9E767C87D87F ();
// 0x0000020A System.Void ExitGames.Client.Photon.Protocol18::WriteDictionaryHeader(ExitGames.Client.Photon.StreamBuffer,System.Type,ExitGames.Client.Photon.Protocol18_GpType&,ExitGames.Client.Photon.Protocol18_GpType&)
extern void Protocol18_WriteDictionaryHeader_m3AFD460007174903762B278D74DEA2E4783D1AD8 ();
// 0x0000020B System.Boolean ExitGames.Client.Photon.Protocol18::WriteArrayType(ExitGames.Client.Photon.StreamBuffer,System.Type,ExitGames.Client.Photon.Protocol18_GpType&)
extern void Protocol18_WriteArrayType_mCB7280CA0FD70D50C86F411A751A3B1CE12118F8 ();
// 0x0000020C System.Void ExitGames.Client.Photon.Protocol18::WriteHashtableArray(ExitGames.Client.Photon.StreamBuffer,System.Object,System.Boolean)
extern void Protocol18_WriteHashtableArray_mBB95B7F30F51C7F22555C94E84E6CDE0CDC0C3C1 ();
// 0x0000020D System.Void ExitGames.Client.Photon.Protocol18::WriteDictionaryArray(ExitGames.Client.Photon.StreamBuffer,System.Collections.IDictionary[],System.Boolean)
extern void Protocol18_WriteDictionaryArray_mD5E129A632A5B746D655EB4F4AB7EE0FB47F7099 ();
// 0x0000020E System.Void ExitGames.Client.Photon.Protocol18::WriteIntLength(ExitGames.Client.Photon.StreamBuffer,System.Int32)
extern void Protocol18_WriteIntLength_m7DC4CE5B1D2D6615562981C5E97DDE3FCB8717F9 ();
// 0x0000020F System.Void ExitGames.Client.Photon.Protocol18::WriteCompressedInt32(ExitGames.Client.Photon.StreamBuffer,System.Int32,System.Boolean)
extern void Protocol18_WriteCompressedInt32_mD1D3355F57AD9654E6F42810A4F70448F870F547 ();
// 0x00000210 System.Void ExitGames.Client.Photon.Protocol18::WriteCompressedInt64(ExitGames.Client.Photon.StreamBuffer,System.Int64,System.Boolean)
extern void Protocol18_WriteCompressedInt64_m05B8623CED35DC0113CB18E5ED7841B024E44E20 ();
// 0x00000211 System.Void ExitGames.Client.Photon.Protocol18::WriteCompressedUInt32(ExitGames.Client.Photon.StreamBuffer,System.UInt32)
extern void Protocol18_WriteCompressedUInt32_m1C3460A5FF6664BD7F575B72E7E9AED19EEB2753 ();
// 0x00000212 System.Int32 ExitGames.Client.Photon.Protocol18::WriteCompressedUInt32(System.Byte[],System.UInt32)
extern void Protocol18_WriteCompressedUInt32_mCC3BE83B036C07D21704EEE50DFA8F25FA51E59F ();
// 0x00000213 System.Void ExitGames.Client.Photon.Protocol18::WriteCompressedUInt64(ExitGames.Client.Photon.StreamBuffer,System.UInt64)
extern void Protocol18_WriteCompressedUInt64_mE794942333102405F34828C1AE8E7E786FC3034F ();
// 0x00000214 System.UInt32 ExitGames.Client.Photon.Protocol18::EncodeZigZag32(System.Int32)
extern void Protocol18_EncodeZigZag32_mCA5D8EB726897EC5575ADA856F8F0397CF2E1D4C ();
// 0x00000215 System.UInt64 ExitGames.Client.Photon.Protocol18::EncodeZigZag64(System.Int64)
extern void Protocol18_EncodeZigZag64_m5E852A7CE5B18E969CE4D1B795431F62DBC6FA57 ();
// 0x00000216 System.Void ExitGames.Client.Photon.Protocol18::.ctor()
extern void Protocol18__ctor_m3890B59E63DA91683F3DD21EF9A00F1A8EA8F51A ();
// 0x00000217 System.Void ExitGames.Client.Photon.Protocol18::.cctor()
extern void Protocol18__cctor_mBCCEF9F2E0BE8A0FACFA4585F4FD17A55521075E ();
// 0x00000218 ExitGames.Client.Photon.IPhotonPeerListener ExitGames.Client.Photon.IPhotonSocket::get_Listener()
extern void IPhotonSocket_get_Listener_m81FD492F65B80FED784D951C9D17A809C55B1C10 ();
// 0x00000219 System.Int32 ExitGames.Client.Photon.IPhotonSocket::get_MTU()
extern void IPhotonSocket_get_MTU_mE95246456D43CF26141C19252FEE5624505FFC9F ();
// 0x0000021A ExitGames.Client.Photon.PhotonSocketState ExitGames.Client.Photon.IPhotonSocket::get_State()
extern void IPhotonSocket_get_State_mFBEC420EEA20C844A7435873FD0D6FDD6553D9DA ();
// 0x0000021B System.Void ExitGames.Client.Photon.IPhotonSocket::set_State(ExitGames.Client.Photon.PhotonSocketState)
extern void IPhotonSocket_set_State_m3A80CC93E8DEC71D42E5C9CFE9129C673D4B3158 ();
// 0x0000021C System.Boolean ExitGames.Client.Photon.IPhotonSocket::get_Connected()
extern void IPhotonSocket_get_Connected_mE05FEE2E88634230D2BF632BC19A71B2F8A0B11D ();
// 0x0000021D System.String ExitGames.Client.Photon.IPhotonSocket::get_ServerAddress()
extern void IPhotonSocket_get_ServerAddress_m38E4E2DCF7BE2EA7EB346D6D02803A251449BBE6 ();
// 0x0000021E System.Void ExitGames.Client.Photon.IPhotonSocket::set_ServerAddress(System.String)
extern void IPhotonSocket_set_ServerAddress_mDAE37C57D47165D8E7CA44D1AECD22DAD2E80EDF ();
// 0x0000021F System.String ExitGames.Client.Photon.IPhotonSocket::get_ServerIpAddress()
extern void IPhotonSocket_get_ServerIpAddress_mDA6B85C3AE9384AB2421FD980C6B9E71233505D9 ();
// 0x00000220 System.Void ExitGames.Client.Photon.IPhotonSocket::set_ServerIpAddress(System.String)
extern void IPhotonSocket_set_ServerIpAddress_mE7539725BD69FC0C9277073472941F8D99FFC20B ();
// 0x00000221 System.Int32 ExitGames.Client.Photon.IPhotonSocket::get_ServerPort()
extern void IPhotonSocket_get_ServerPort_m9F0013DBF7F5D00A8111D9382EE9A7CC83949AF4 ();
// 0x00000222 System.Void ExitGames.Client.Photon.IPhotonSocket::set_ServerPort(System.Int32)
extern void IPhotonSocket_set_ServerPort_mF3E5C7C9FCC51AB8E59653A5FEE400C59D9286F0 ();
// 0x00000223 System.Boolean ExitGames.Client.Photon.IPhotonSocket::get_AddressResolvedAsIpv6()
extern void IPhotonSocket_get_AddressResolvedAsIpv6_m836466CFDC7E21C38D7F275EE372B3475356BF6F ();
// 0x00000224 System.Void ExitGames.Client.Photon.IPhotonSocket::set_AddressResolvedAsIpv6(System.Boolean)
extern void IPhotonSocket_set_AddressResolvedAsIpv6_mCD5DE9956B3C74C3832F3D078969FA99E7263587 ();
// 0x00000225 System.Void ExitGames.Client.Photon.IPhotonSocket::set_UrlProtocol(System.String)
extern void IPhotonSocket_set_UrlProtocol_mD95FF7BEC530105FECD81272480D5D5316F713BF ();
// 0x00000226 System.Void ExitGames.Client.Photon.IPhotonSocket::set_UrlPath(System.String)
extern void IPhotonSocket_set_UrlPath_mDB678DCCAA3CF16723A3FAF74D6BEB8668597EF3 ();
// 0x00000227 System.Void ExitGames.Client.Photon.IPhotonSocket::.ctor(ExitGames.Client.Photon.PeerBase)
extern void IPhotonSocket__ctor_m5DF31D1BB5054387EEBA22001CFC5001AA28E05A ();
// 0x00000228 System.Boolean ExitGames.Client.Photon.IPhotonSocket::Connect()
extern void IPhotonSocket_Connect_mB8A977E839E01FECA1A136F9626A872D35557F16 ();
// 0x00000229 System.Boolean ExitGames.Client.Photon.IPhotonSocket::Disconnect()
// 0x0000022A ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.IPhotonSocket::Send(System.Byte[],System.Int32)
// 0x0000022B System.Void ExitGames.Client.Photon.IPhotonSocket::HandleReceivedDatagram(System.Byte[],System.Int32,System.Boolean)
extern void IPhotonSocket_HandleReceivedDatagram_m04401580F4FDA75D811B4F57AFAFC73EE0C5D438 ();
// 0x0000022C System.Boolean ExitGames.Client.Photon.IPhotonSocket::ReportDebugOfLevel(ExitGames.Client.Photon.DebugLevel)
extern void IPhotonSocket_ReportDebugOfLevel_mB847CD73CDF1609E9C31ACFFD474F67B634E594B ();
// 0x0000022D System.Void ExitGames.Client.Photon.IPhotonSocket::EnqueueDebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern void IPhotonSocket_EnqueueDebugReturn_m438F9AA21D09AFE51B22A8655DB36CBE0C3B076E ();
// 0x0000022E System.Void ExitGames.Client.Photon.IPhotonSocket::HandleException(ExitGames.Client.Photon.StatusCode)
extern void IPhotonSocket_HandleException_m2D54CFC4B60DE4C819EE8885E9009F066839C820 ();
// 0x0000022F System.Boolean ExitGames.Client.Photon.IPhotonSocket::TryParseAddress(System.String,System.String&,System.UInt16&,System.String&,System.String&)
extern void IPhotonSocket_TryParseAddress_m13AF27BE73438878EE02F240AE274CC5B36F91BD ();
// 0x00000230 System.Net.IPAddress[] ExitGames.Client.Photon.IPhotonSocket::GetIpAddresses(System.String)
extern void IPhotonSocket_GetIpAddresses_mA02D979F86382FD71052436C48CAAAF192311061 ();
// 0x00000231 System.Int32 ExitGames.Client.Photon.IPhotonSocket::AddressSortComparer(System.Net.IPAddress,System.Net.IPAddress)
extern void IPhotonSocket_AddressSortComparer_mC4206E1F50E250C44CE2FB4F8C7C1BF209DDD2EB ();
// 0x00000232 System.Void ExitGames.Client.Photon.IPhotonSocket::<HandleException>b__47_0()
extern void IPhotonSocket_U3CHandleExceptionU3Eb__47_0_mFF37A3133D6DEDB980308A3721E0756907DEC7FF ();
// 0x00000233 System.Void ExitGames.Client.Photon.IPhotonSocket_<>c::.cctor()
extern void U3CU3Ec__cctor_mE2568C71F20DE95122EF4EE39FE35EF89B790E58 ();
// 0x00000234 System.Void ExitGames.Client.Photon.IPhotonSocket_<>c::.ctor()
extern void U3CU3Ec__ctor_m84C52ED064666E73701081A639C32BC74032A759 ();
// 0x00000235 System.String ExitGames.Client.Photon.IPhotonSocket_<>c::<GetIpAddresses>b__50_0(System.Net.IPAddress)
extern void U3CU3Ec_U3CGetIpAddressesU3Eb__50_0_m1C6766A50B28FA17C5779178D0D933D23B24110E ();
// 0x00000236 System.Void ExitGames.Client.Photon.SocketUdp::.ctor(ExitGames.Client.Photon.PeerBase)
extern void SocketUdp__ctor_m6E26694A7CED098C2F314825803A06C270A68EB3 ();
// 0x00000237 System.Void ExitGames.Client.Photon.SocketUdp::Finalize()
extern void SocketUdp_Finalize_mE15AD4AC372E99DF61B75F39FA02581659E2EDE1 ();
// 0x00000238 System.Void ExitGames.Client.Photon.SocketUdp::Dispose()
extern void SocketUdp_Dispose_m9A801E4C0FE2FC7193E49E4A1CF00C5CBA2BFF5C ();
// 0x00000239 System.Boolean ExitGames.Client.Photon.SocketUdp::Connect()
extern void SocketUdp_Connect_mB5CAB1D5221E5931185EB4A11686D9DCF467CA7A ();
// 0x0000023A System.Boolean ExitGames.Client.Photon.SocketUdp::Disconnect()
extern void SocketUdp_Disconnect_m5F2B92C707159B0CD124E5F697BFC6EFBEE2FDBA ();
// 0x0000023B ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.SocketUdp::Send(System.Byte[],System.Int32)
extern void SocketUdp_Send_m145DD7477DCD3B862C8EB6053914A5D7F5B2134A ();
// 0x0000023C System.Void ExitGames.Client.Photon.SocketUdp::DnsAndConnect()
extern void SocketUdp_DnsAndConnect_m4D109A93C1EA9B29D15BAC57379F0A68AEA3D2F9 ();
// 0x0000023D System.Void ExitGames.Client.Photon.SocketUdp::ReceiveLoop()
extern void SocketUdp_ReceiveLoop_m49A47A4C41D60201CE348266789F7D0DB6CE8B87 ();
// 0x0000023E System.Void ExitGames.Client.Photon.SocketTcp::.ctor(ExitGames.Client.Photon.PeerBase)
extern void SocketTcp__ctor_mEBFBD0E3A3C2EF85D78F4D7359070DF6E0DB99A9 ();
// 0x0000023F System.Void ExitGames.Client.Photon.SocketTcp::Finalize()
extern void SocketTcp_Finalize_mE8C1DA41258E85FD2657524114BE42A0523568BB ();
// 0x00000240 System.Void ExitGames.Client.Photon.SocketTcp::Dispose()
extern void SocketTcp_Dispose_m2BE7A5F8EFE3EE224BFA23A97ECF8B54B101A290 ();
// 0x00000241 System.Boolean ExitGames.Client.Photon.SocketTcp::Connect()
extern void SocketTcp_Connect_mEC635608EB8F9C3B208B0EE5D37DCB5FD814A5D3 ();
// 0x00000242 System.Boolean ExitGames.Client.Photon.SocketTcp::Disconnect()
extern void SocketTcp_Disconnect_m8923AF84BA302BA0F1BB17F1B0C6C2C2073D854D ();
// 0x00000243 ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.SocketTcp::Send(System.Byte[],System.Int32)
extern void SocketTcp_Send_mBEE6F86AC36C82C7AE9DB2C4D2DAD7B288671012 ();
// 0x00000244 System.Void ExitGames.Client.Photon.SocketTcp::DnsAndConnect()
extern void SocketTcp_DnsAndConnect_m34281428071478E2B890AAA23AF84087617B8A4D ();
// 0x00000245 System.Void ExitGames.Client.Photon.SocketTcp::ReceiveLoop()
extern void SocketTcp_ReceiveLoop_m94F3091FC347E263BED7D381AD5727E510D406C2 ();
// 0x00000246 System.Void ExitGames.Client.Photon.SimulationItem::.ctor()
extern void SimulationItem__ctor_m6B0ACA82BA28D399FC5493B70241F2CA5AC26611 ();
// 0x00000247 System.Int32 ExitGames.Client.Photon.SimulationItem::get_Delay()
extern void SimulationItem_get_Delay_mCA068BE15D1A7D336BC197F53A64A68FA09CFE8B ();
// 0x00000248 System.Void ExitGames.Client.Photon.SimulationItem::set_Delay(System.Int32)
extern void SimulationItem_set_Delay_m310EF147636DC2628AC6B59391BA2210A69B93E0 ();
// 0x00000249 System.Boolean ExitGames.Client.Photon.NetworkSimulationSet::get_IsSimulationEnabled()
extern void NetworkSimulationSet_get_IsSimulationEnabled_m69E140B0EE45A07AE9A783649D7FFF58B67EB699 ();
// 0x0000024A System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_IsSimulationEnabled(System.Boolean)
extern void NetworkSimulationSet_set_IsSimulationEnabled_mFA99DF613ED88D96301CC8C9B841383B321C406F ();
// 0x0000024B System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_OutgoingLag()
extern void NetworkSimulationSet_get_OutgoingLag_mF94EBE63A5371EF88EDD33CACC423CF3F3D7ED14 ();
// 0x0000024C System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_OutgoingLag(System.Int32)
extern void NetworkSimulationSet_set_OutgoingLag_mBB0B22C2AD7C94D31F63D44C0BEF1B4F9200CEB3 ();
// 0x0000024D System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_OutgoingJitter()
extern void NetworkSimulationSet_get_OutgoingJitter_m1DBD690367CE7E155DDE3FC9AD41D36CE6FFC48E ();
// 0x0000024E System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_OutgoingJitter(System.Int32)
extern void NetworkSimulationSet_set_OutgoingJitter_mACF982FFF9F1EB83F2D9B867127977C6F1C7E18B ();
// 0x0000024F System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_OutgoingLossPercentage()
extern void NetworkSimulationSet_get_OutgoingLossPercentage_m421F0F46CD8F43DD019575DEBBAF94E91A146EA2 ();
// 0x00000250 System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_OutgoingLossPercentage(System.Int32)
extern void NetworkSimulationSet_set_OutgoingLossPercentage_mC8F6990B43B189A5C5101C5FB85B5854306F67E5 ();
// 0x00000251 System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_IncomingLag()
extern void NetworkSimulationSet_get_IncomingLag_mBE5DF4388742F8E62CFAD334EB045DB66D9C2ED4 ();
// 0x00000252 System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_IncomingLag(System.Int32)
extern void NetworkSimulationSet_set_IncomingLag_m308F42FBFAAE01ACE8B538E834BD496C8DBE1378 ();
// 0x00000253 System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_IncomingJitter()
extern void NetworkSimulationSet_get_IncomingJitter_mD36DE5AF19056CA9E580C2E13548CD51555F2A85 ();
// 0x00000254 System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_IncomingJitter(System.Int32)
extern void NetworkSimulationSet_set_IncomingJitter_m9ACDEE491D5534944BFF1DD05ED93B82DAE633AF ();
// 0x00000255 System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_IncomingLossPercentage()
extern void NetworkSimulationSet_get_IncomingLossPercentage_mAA923D5758E1058C7FE786F6C8C6E20647570EA7 ();
// 0x00000256 System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_IncomingLossPercentage(System.Int32)
extern void NetworkSimulationSet_set_IncomingLossPercentage_m77013FCEBFB6641E63A02E1E3E8644BC8D11D9C6 ();
// 0x00000257 System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_LostPackagesOut()
extern void NetworkSimulationSet_get_LostPackagesOut_m98C39271612038E1DDDCEA0B4B80FE045C87FCF4 ();
// 0x00000258 System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_LostPackagesOut(System.Int32)
extern void NetworkSimulationSet_set_LostPackagesOut_m50A7475AF1C054FE66F8E01688175EE150A82040 ();
// 0x00000259 System.Int32 ExitGames.Client.Photon.NetworkSimulationSet::get_LostPackagesIn()
extern void NetworkSimulationSet_get_LostPackagesIn_m2F58DBFC7E1008386EA01F847381D91ADB5C3E0A ();
// 0x0000025A System.Void ExitGames.Client.Photon.NetworkSimulationSet::set_LostPackagesIn(System.Int32)
extern void NetworkSimulationSet_set_LostPackagesIn_m062238219CE1A63FC33A228FA800E41B21AFDC8A ();
// 0x0000025B System.String ExitGames.Client.Photon.NetworkSimulationSet::ToString()
extern void NetworkSimulationSet_ToString_m0C7D3E809E45807A59D324FCFA0C9899E550737A ();
// 0x0000025C System.Void ExitGames.Client.Photon.NetworkSimulationSet::.ctor()
extern void NetworkSimulationSet__ctor_m29E97BA7CAC45A955FE4D0EC2A17B2B005325788 ();
// 0x0000025D System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_OperationByteCount()
extern void TrafficStatsGameLevel_get_OperationByteCount_m4AF83FBACDE96B7B1364C033FCB3D4E494AE0FC7 ();
// 0x0000025E System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_OperationByteCount(System.Int32)
extern void TrafficStatsGameLevel_set_OperationByteCount_m927C68A447313CE13AA5D84987CA2287F142CB6F ();
// 0x0000025F System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_OperationCount()
extern void TrafficStatsGameLevel_get_OperationCount_m99206E1FFB8CF775C04FD197C113301FF4F32D2B ();
// 0x00000260 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_OperationCount(System.Int32)
extern void TrafficStatsGameLevel_set_OperationCount_m36A4955600270A48DB877E0F80C410808A127DC8 ();
// 0x00000261 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_ResultByteCount()
extern void TrafficStatsGameLevel_get_ResultByteCount_m1B12324F6F9B7E06FC33BAD7CF2BD459E52FD8DB ();
// 0x00000262 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_ResultByteCount(System.Int32)
extern void TrafficStatsGameLevel_set_ResultByteCount_mAC03A57F3D1FD4B59FFF9E3D3ADC0E24944FE200 ();
// 0x00000263 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_ResultCount()
extern void TrafficStatsGameLevel_get_ResultCount_m1F09B89A40625665CD25BA3C69E82B3A075327F6 ();
// 0x00000264 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_ResultCount(System.Int32)
extern void TrafficStatsGameLevel_set_ResultCount_mE00C2311493E0E2E16081FE1D77BB5A229F4F29B ();
// 0x00000265 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_EventByteCount()
extern void TrafficStatsGameLevel_get_EventByteCount_m54BD41A76DFA33BFCAA77015ED7C0A6119B526A5 ();
// 0x00000266 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_EventByteCount(System.Int32)
extern void TrafficStatsGameLevel_set_EventByteCount_m6498350536AF9E5789F199A7F7A4CB67F5A9C1B5 ();
// 0x00000267 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_EventCount()
extern void TrafficStatsGameLevel_get_EventCount_mA5E527E99CD36B7AFDC7D85A7767998D0FCA67D3 ();
// 0x00000268 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_EventCount(System.Int32)
extern void TrafficStatsGameLevel_set_EventCount_mC05D97475B40C59ADC0CF8FDC52106477E661D91 ();
// 0x00000269 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestOpResponseCallback()
extern void TrafficStatsGameLevel_get_LongestOpResponseCallback_m734C3233D3CD5C9566AFEC2E40DAC45B6A2BC44C ();
// 0x0000026A System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestOpResponseCallback(System.Int32)
extern void TrafficStatsGameLevel_set_LongestOpResponseCallback_m9B64B8C65D333FE61983C8E62A6124353A53A666 ();
// 0x0000026B System.Byte ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestOpResponseCallbackOpCode()
extern void TrafficStatsGameLevel_get_LongestOpResponseCallbackOpCode_mBDEF5EAD7204F1C01ED5A48D17CE5A78CABB9B71 ();
// 0x0000026C System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestOpResponseCallbackOpCode(System.Byte)
extern void TrafficStatsGameLevel_set_LongestOpResponseCallbackOpCode_m01581F59B4C807C6DC2D339184447C583622899E ();
// 0x0000026D System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestEventCallback()
extern void TrafficStatsGameLevel_get_LongestEventCallback_m1A548D3427C18526B150D2863B997E5947C269F3 ();
// 0x0000026E System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestEventCallback(System.Int32)
extern void TrafficStatsGameLevel_set_LongestEventCallback_m06F742549E1093BE269291F9889E077F39A887EC ();
// 0x0000026F System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestMessageCallback()
extern void TrafficStatsGameLevel_get_LongestMessageCallback_m1DA736B0F2CC25CFE3EEA389558AEF127BE209D5 ();
// 0x00000270 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestMessageCallback(System.Int32)
extern void TrafficStatsGameLevel_set_LongestMessageCallback_mD119881F98C7F196ABB5983E09D47C6416DCC67B ();
// 0x00000271 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestRawMessageCallback()
extern void TrafficStatsGameLevel_get_LongestRawMessageCallback_m68892F488A616E129D7CD00AE7283A05581BBF2D ();
// 0x00000272 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestRawMessageCallback(System.Int32)
extern void TrafficStatsGameLevel_set_LongestRawMessageCallback_m77E9CFED473CC323B09EE0C139C4E988EAAFE9B9 ();
// 0x00000273 System.Byte ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestEventCallbackCode()
extern void TrafficStatsGameLevel_get_LongestEventCallbackCode_mDFA1AB8C9D5DAEC0D538A9A15AFC3163CB6E1C8A ();
// 0x00000274 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestEventCallbackCode(System.Byte)
extern void TrafficStatsGameLevel_set_LongestEventCallbackCode_m199E1B2B952DEBE8781382732B4332460AE42BC8 ();
// 0x00000275 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestDeltaBetweenDispatching()
extern void TrafficStatsGameLevel_get_LongestDeltaBetweenDispatching_m7D96F3912C4B5C5A0E9F15C87D52DD962885066A ();
// 0x00000276 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestDeltaBetweenDispatching(System.Int32)
extern void TrafficStatsGameLevel_set_LongestDeltaBetweenDispatching_mCAECF6500C7F42CC508185710BC1B79C65A105CC ();
// 0x00000277 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_LongestDeltaBetweenSending()
extern void TrafficStatsGameLevel_get_LongestDeltaBetweenSending_m652A61B15DFB86B0C32971F3D01795F7C9BF052E ();
// 0x00000278 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_LongestDeltaBetweenSending(System.Int32)
extern void TrafficStatsGameLevel_set_LongestDeltaBetweenSending_mA760542D3F7A40970DEE16FA2170140C8569452C ();
// 0x00000279 System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_DispatchIncomingCommandsCalls()
extern void TrafficStatsGameLevel_get_DispatchIncomingCommandsCalls_m548C7E96AD1AE4EF80CF5788EBDC808C908189CF ();
// 0x0000027A System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_DispatchIncomingCommandsCalls(System.Int32)
extern void TrafficStatsGameLevel_set_DispatchIncomingCommandsCalls_m2F5C9394445C125DCAAC17CF9F09820CD73963CA ();
// 0x0000027B System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_SendOutgoingCommandsCalls()
extern void TrafficStatsGameLevel_get_SendOutgoingCommandsCalls_m8B1D331C1819EEB5859F895F48F3A1CD873C0CF1 ();
// 0x0000027C System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::set_SendOutgoingCommandsCalls(System.Int32)
extern void TrafficStatsGameLevel_set_SendOutgoingCommandsCalls_mEDC64FA66CB1746E28ACEE399B3BE4366FEC3977 ();
// 0x0000027D System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_TotalMessageCount()
extern void TrafficStatsGameLevel_get_TotalMessageCount_m1215B8640C7534BE6E9BE5ABF8A8D1612AEE69DD ();
// 0x0000027E System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_TotalIncomingMessageCount()
extern void TrafficStatsGameLevel_get_TotalIncomingMessageCount_mCECA7894C34796872D9F33D79EBEA64B6D1EADC2 ();
// 0x0000027F System.Int32 ExitGames.Client.Photon.TrafficStatsGameLevel::get_TotalOutgoingMessageCount()
extern void TrafficStatsGameLevel_get_TotalOutgoingMessageCount_mBB5803D9FFAF7B71EE49BBDEAEC3CB038EAB2121 ();
// 0x00000280 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::CountOperation(System.Int32)
extern void TrafficStatsGameLevel_CountOperation_mB9917247563AB3F6707131C35C4FCD63D0DD18D2 ();
// 0x00000281 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::CountResult(System.Int32)
extern void TrafficStatsGameLevel_CountResult_m21B61E41067A4078662B77B268A6B7B08DA179E4 ();
// 0x00000282 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::CountEvent(System.Int32)
extern void TrafficStatsGameLevel_CountEvent_m3A9C6A3A2BD4203FE3287D56563338F55A3F5D7C ();
// 0x00000283 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::TimeForResponseCallback(System.Byte,System.Int32)
extern void TrafficStatsGameLevel_TimeForResponseCallback_m1422AE3342D5ADA5E73B8D5317F4BD12F433AA07 ();
// 0x00000284 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::TimeForEventCallback(System.Byte,System.Int32)
extern void TrafficStatsGameLevel_TimeForEventCallback_m5BE93E4C166364DDDCA5CDEC2FC7FACA4A46B35C ();
// 0x00000285 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::TimeForMessageCallback(System.Int32)
extern void TrafficStatsGameLevel_TimeForMessageCallback_mAC6AEF4E9491312C0F3EDDAD6F813DAF19BD6AC2 ();
// 0x00000286 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::TimeForRawMessageCallback(System.Int32)
extern void TrafficStatsGameLevel_TimeForRawMessageCallback_mD3401787693A72A683E98589CBBB2FFB6DE0938F ();
// 0x00000287 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::DispatchIncomingCommandsCalled()
extern void TrafficStatsGameLevel_DispatchIncomingCommandsCalled_m28A3F7E4AEAB6B9016D047F760333EADF84E7CF9 ();
// 0x00000288 System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::SendOutgoingCommandsCalled()
extern void TrafficStatsGameLevel_SendOutgoingCommandsCalled_mF60ABB96AAEE5DDA0CD28B2A5C9F4B6A7F97491F ();
// 0x00000289 System.String ExitGames.Client.Photon.TrafficStatsGameLevel::ToString()
extern void TrafficStatsGameLevel_ToString_m8B74070B65BDD3A0E8CBB7FA468F32D47406B7FF ();
// 0x0000028A System.String ExitGames.Client.Photon.TrafficStatsGameLevel::ToStringVitalStats()
extern void TrafficStatsGameLevel_ToStringVitalStats_mD5F8AECA8127EC570B86E05F8B17DF61D5E75E15 ();
// 0x0000028B System.Void ExitGames.Client.Photon.TrafficStatsGameLevel::.ctor()
extern void TrafficStatsGameLevel__ctor_mAF40606532076BFD8202354DF47AC23DEE132E4B ();
// 0x0000028C System.Int32 ExitGames.Client.Photon.TrafficStats::get_PackageHeaderSize()
extern void TrafficStats_get_PackageHeaderSize_m7AF4D435AD51ECDF03344BEC194E554C8F81A3B9 ();
// 0x0000028D System.Void ExitGames.Client.Photon.TrafficStats::set_PackageHeaderSize(System.Int32)
extern void TrafficStats_set_PackageHeaderSize_m4A2C48E99681143A74B27213C8E82EEE98BC9879 ();
// 0x0000028E System.Int32 ExitGames.Client.Photon.TrafficStats::get_ReliableCommandCount()
extern void TrafficStats_get_ReliableCommandCount_m495D39C6CBC2F167A80380FBB83CFEB9E07F6677 ();
// 0x0000028F System.Void ExitGames.Client.Photon.TrafficStats::set_ReliableCommandCount(System.Int32)
extern void TrafficStats_set_ReliableCommandCount_m36D3639D4E3226D5734E4D0F1144497448303404 ();
// 0x00000290 System.Int32 ExitGames.Client.Photon.TrafficStats::get_UnreliableCommandCount()
extern void TrafficStats_get_UnreliableCommandCount_m8860C8A12C8057813B8973D6B7862F1BA26DD1CB ();
// 0x00000291 System.Void ExitGames.Client.Photon.TrafficStats::set_UnreliableCommandCount(System.Int32)
extern void TrafficStats_set_UnreliableCommandCount_mC13497B207168DE840AE71851993032B9A90ACA6 ();
// 0x00000292 System.Int32 ExitGames.Client.Photon.TrafficStats::get_FragmentCommandCount()
extern void TrafficStats_get_FragmentCommandCount_m694D6BC4CFCE1DE403229697E21102A3359D1A3F ();
// 0x00000293 System.Void ExitGames.Client.Photon.TrafficStats::set_FragmentCommandCount(System.Int32)
extern void TrafficStats_set_FragmentCommandCount_m28BDC126F7A3A4A4D258D6B84EDDF6BEC364796A ();
// 0x00000294 System.Int32 ExitGames.Client.Photon.TrafficStats::get_ControlCommandCount()
extern void TrafficStats_get_ControlCommandCount_m04178269182F266E2AA23C9953DC58B5445C90FF ();
// 0x00000295 System.Void ExitGames.Client.Photon.TrafficStats::set_ControlCommandCount(System.Int32)
extern void TrafficStats_set_ControlCommandCount_mCC328A8D5867D3F1F6BB8DBA516396155246E40A ();
// 0x00000296 System.Int32 ExitGames.Client.Photon.TrafficStats::get_TotalPacketCount()
extern void TrafficStats_get_TotalPacketCount_m7C5BDC0900E1F834C72F2CEA84E7D98B3C5BC518 ();
// 0x00000297 System.Void ExitGames.Client.Photon.TrafficStats::set_TotalPacketCount(System.Int32)
extern void TrafficStats_set_TotalPacketCount_m6BAD3048EB978145025F6715C95065427025544C ();
// 0x00000298 System.Int32 ExitGames.Client.Photon.TrafficStats::get_TotalCommandsInPackets()
extern void TrafficStats_get_TotalCommandsInPackets_mF9798CC0CD31D46A4459A32422A374CEFAB4D13E ();
// 0x00000299 System.Void ExitGames.Client.Photon.TrafficStats::set_TotalCommandsInPackets(System.Int32)
extern void TrafficStats_set_TotalCommandsInPackets_mF95F8732EFD8E04223F10D8D8FA61734177BF810 ();
// 0x0000029A System.Int32 ExitGames.Client.Photon.TrafficStats::get_ReliableCommandBytes()
extern void TrafficStats_get_ReliableCommandBytes_m147AC0C2244013C73A1453DF491DBD8C11D81B12 ();
// 0x0000029B System.Void ExitGames.Client.Photon.TrafficStats::set_ReliableCommandBytes(System.Int32)
extern void TrafficStats_set_ReliableCommandBytes_mA3EA4B791C27A642B30D951DF5428D4CC42DC745 ();
// 0x0000029C System.Int32 ExitGames.Client.Photon.TrafficStats::get_UnreliableCommandBytes()
extern void TrafficStats_get_UnreliableCommandBytes_mCE8460DBA1FBEF7EC79308384E4CB04EE0B7537D ();
// 0x0000029D System.Void ExitGames.Client.Photon.TrafficStats::set_UnreliableCommandBytes(System.Int32)
extern void TrafficStats_set_UnreliableCommandBytes_m63A84DFF298BB4187C77E67908DBD15C329177E5 ();
// 0x0000029E System.Int32 ExitGames.Client.Photon.TrafficStats::get_FragmentCommandBytes()
extern void TrafficStats_get_FragmentCommandBytes_mD9136721B1962E97701D2212D6D506D76A236441 ();
// 0x0000029F System.Void ExitGames.Client.Photon.TrafficStats::set_FragmentCommandBytes(System.Int32)
extern void TrafficStats_set_FragmentCommandBytes_mBBA2D4E9D0EE48C56A249E27928D353983D91CCB ();
// 0x000002A0 System.Int32 ExitGames.Client.Photon.TrafficStats::get_ControlCommandBytes()
extern void TrafficStats_get_ControlCommandBytes_m5EBBA04F12C94A6DBE6D0B72017BF1869EC39D41 ();
// 0x000002A1 System.Void ExitGames.Client.Photon.TrafficStats::set_ControlCommandBytes(System.Int32)
extern void TrafficStats_set_ControlCommandBytes_m356CF57C9033CC747D1D11720EB4FAB20BA604F1 ();
// 0x000002A2 System.Void ExitGames.Client.Photon.TrafficStats::.ctor(System.Int32)
extern void TrafficStats__ctor_m18E0B90734A90DE5D0DB5A59789CD2CD1C22C711 ();
// 0x000002A3 System.Int32 ExitGames.Client.Photon.TrafficStats::get_TotalCommandBytes()
extern void TrafficStats_get_TotalCommandBytes_m70C8948B443AAA86475BE67453F39EAD8D0695A8 ();
// 0x000002A4 System.Int32 ExitGames.Client.Photon.TrafficStats::get_TotalPacketBytes()
extern void TrafficStats_get_TotalPacketBytes_m9EEFBCF2112A49697A369CBFB405C046FBCF2ADA ();
// 0x000002A5 System.Void ExitGames.Client.Photon.TrafficStats::set_TimestampOfLastAck(System.Int32)
extern void TrafficStats_set_TimestampOfLastAck_m574CA4916E730E3605FAD329118F287E31BDB2A2 ();
// 0x000002A6 System.Void ExitGames.Client.Photon.TrafficStats::set_TimestampOfLastReliableCommand(System.Int32)
extern void TrafficStats_set_TimestampOfLastReliableCommand_mEBA5FB895C9F33C8152BA19279BDF72943A457EA ();
// 0x000002A7 System.Void ExitGames.Client.Photon.TrafficStats::CountControlCommand(System.Int32)
extern void TrafficStats_CountControlCommand_mC8EEA269EF85F415A2D0A2CC202CC3A27E77F8A4 ();
// 0x000002A8 System.Void ExitGames.Client.Photon.TrafficStats::CountReliableOpCommand(System.Int32)
extern void TrafficStats_CountReliableOpCommand_mA355A2EBC1EF95EDC839ABF0A24E4318434BFD7F ();
// 0x000002A9 System.Void ExitGames.Client.Photon.TrafficStats::CountUnreliableOpCommand(System.Int32)
extern void TrafficStats_CountUnreliableOpCommand_mD0F52CB2992BAC2F1D4A793DAA4D18D211328190 ();
// 0x000002AA System.Void ExitGames.Client.Photon.TrafficStats::CountFragmentOpCommand(System.Int32)
extern void TrafficStats_CountFragmentOpCommand_m381C1C468A75C32220D02FB93D032E73C4EA9F98 ();
// 0x000002AB System.String ExitGames.Client.Photon.TrafficStats::ToString()
extern void TrafficStats_ToString_m6031F87D452D9566B4F4D6B564C624CCB6F618A5 ();
// 0x000002AC System.Void ExitGames.Client.Photon.Encryption.IPhotonEncryptor::Init(System.Byte[],System.Byte[],System.Byte[])
// 0x000002AD System.Void ExitGames.Client.Photon.Encryption.IPhotonEncryptor::Encrypt(System.Byte[],System.Int32,System.Byte[],System.Int32&,System.Boolean)
// 0x000002AE System.Byte[] ExitGames.Client.Photon.Encryption.IPhotonEncryptor::Decrypt(System.Byte[],System.Int32,System.Int32,System.Int32&,System.Boolean)
// 0x000002AF System.Byte[] ExitGames.Client.Photon.Encryption.IPhotonEncryptor::CreateHMAC(System.Byte[],System.Int32,System.Int32)
// 0x000002B0 System.Boolean ExitGames.Client.Photon.Encryption.IPhotonEncryptor::CheckHMAC(System.Byte[],System.Int32)
// 0x000002B1 System.Void ExitGames.Client.Photon.Encryption.EncryptorNet::Init(System.Byte[],System.Byte[],System.Byte[])
extern void EncryptorNet_Init_m3C295187A64A0EEB253A690F63A205348597E627 ();
// 0x000002B2 System.Void ExitGames.Client.Photon.Encryption.EncryptorNet::Encrypt(System.Byte[],System.Int32,System.Byte[],System.Int32&,System.Boolean)
extern void EncryptorNet_Encrypt_m6E9513964EF53DAD3ED2DF23BEBB6589D308512B ();
// 0x000002B3 System.Byte[] ExitGames.Client.Photon.Encryption.EncryptorNet::Decrypt(System.Byte[],System.Int32,System.Int32,System.Int32&,System.Boolean)
extern void EncryptorNet_Decrypt_m98021DC7ECBA10A8E67E3489491C3378EB88A5F1 ();
// 0x000002B4 System.Byte[] ExitGames.Client.Photon.Encryption.EncryptorNet::CreateHMAC(System.Byte[],System.Int32,System.Int32)
extern void EncryptorNet_CreateHMAC_m591927E086329C737EAC035BE538D0991E8B2013 ();
// 0x000002B5 System.Boolean ExitGames.Client.Photon.Encryption.EncryptorNet::CheckHMAC(System.Byte[],System.Int32)
extern void EncryptorNet_CheckHMAC_m7E55816A36F39E3FD355279B2063C488ACB58C74 ();
// 0x000002B6 System.Void ExitGames.Client.Photon.Encryption.EncryptorNet::.ctor()
extern void EncryptorNet__ctor_m5758AC49519B24D1F87B5A300182EC8B4B091B30 ();
// 0x000002B7 System.IntPtr ExitGames.Client.Photon.Encryption.EncryptorNative::egconstructEncryptor(System.Byte[],System.Byte[])
extern void EncryptorNative_egconstructEncryptor_mAB53050C9FFFD131D63178C1554C172B12F76AE4 ();
// 0x000002B8 System.Void ExitGames.Client.Photon.Encryption.EncryptorNative::egdestructEncryptor(System.IntPtr)
extern void EncryptorNative_egdestructEncryptor_m2A7B84C7B83103F3B441C615824E7D74C9F22452 ();
// 0x000002B9 System.Void ExitGames.Client.Photon.Encryption.EncryptorNative::egencrypt(System.IntPtr,System.Byte[],System.Int32,System.Byte[],System.Int32&,System.Int32&)
extern void EncryptorNative_egencrypt_mFC097F5932C9089F0CF3C9BD59D87B2FB33C55E9 ();
// 0x000002BA System.Void ExitGames.Client.Photon.Encryption.EncryptorNative::egdecrypt(System.IntPtr,System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32&)
extern void EncryptorNative_egdecrypt_m83F62EB73ED50254D89A75A2B1F38466118326EB ();
// 0x000002BB System.Void ExitGames.Client.Photon.Encryption.EncryptorNative::egHMAC(System.IntPtr,System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32&)
extern void EncryptorNative_egHMAC_mD44DBC015296FA4DF079D61C6DBBAF9DB9DF307E ();
// 0x000002BC System.Int32 ExitGames.Client.Photon.Encryption.EncryptorNative::eggetBlockSize()
extern void EncryptorNative_eggetBlockSize_mFE29064BB8B7098AF924832CFF5784A30125C6EC ();
// 0x000002BD System.Int32 ExitGames.Client.Photon.Encryption.EncryptorNative::eggetIVSize()
extern void EncryptorNative_eggetIVSize_mF8818B86EC45EC6D5AD745387004EDDF5B4B49B4 ();
// 0x000002BE System.Int32 ExitGames.Client.Photon.Encryption.EncryptorNative::eggetHMACSize()
extern void EncryptorNative_eggetHMACSize_m81EFFD4D5943FC74DECA40489793FFFE6A4080AF ();
// 0x000002BF System.Void ExitGames.Client.Photon.Encryption.EncryptorNative::egsetEncryptorLoggingCallback(System.IntPtr,ExitGames.Client.Photon.Encryption.EncryptorNative_LogCallbackDelegate)
extern void EncryptorNative_egsetEncryptorLoggingCallback_mD38FA4A0B05D6A42D02E1BACA2893D8E1FAE6447 ();
// 0x000002C0 System.Boolean ExitGames.Client.Photon.Encryption.EncryptorNative::egsetEncryptorLoggingLevel(System.Int32)
extern void EncryptorNative_egsetEncryptorLoggingLevel_m8AAE0E53235B5188B96B59416F838824F7FB5899 ();
// 0x000002C1 System.Void ExitGames.Client.Photon.Encryption.EncryptorNative::Finalize()
extern void EncryptorNative_Finalize_m303F3461EB9A0F2E7730326C8251D53003AB626D ();
// 0x000002C2 System.Void ExitGames.Client.Photon.Encryption.EncryptorNative::OnNativeLog(System.IntPtr,System.Int32,System.String)
extern void EncryptorNative_OnNativeLog_m023F639F0E7BA64C870B8AE3A107F3CCB6872293 ();
// 0x000002C3 System.Void ExitGames.Client.Photon.Encryption.EncryptorNative::Init(System.Byte[],System.Byte[],System.Byte[])
extern void EncryptorNative_Init_mB39AEFC6E6DD5A7B6F1EF9BFE16C103ABA3EE94A ();
// 0x000002C4 System.Void ExitGames.Client.Photon.Encryption.EncryptorNative::Dispose(System.Boolean)
extern void EncryptorNative_Dispose_mC9AEC7139694C9F5AC301A7E52171E719C7B3369 ();
// 0x000002C5 System.Void ExitGames.Client.Photon.Encryption.EncryptorNative::Encrypt(System.Byte[],System.Int32,System.Byte[],System.Int32&,System.Boolean)
extern void EncryptorNative_Encrypt_mCED29DF26F5FD39D5B55DD75183387F7557A4C40 ();
// 0x000002C6 System.Byte[] ExitGames.Client.Photon.Encryption.EncryptorNative::CreateHMAC(System.Byte[],System.Int32,System.Int32)
extern void EncryptorNative_CreateHMAC_m2B35323E98B3AC5158623705F9FEED9975137DC9 ();
// 0x000002C7 System.Byte[] ExitGames.Client.Photon.Encryption.EncryptorNative::Decrypt(System.Byte[],System.Int32,System.Int32,System.Int32&,System.Boolean)
extern void EncryptorNative_Decrypt_m865197312E5C5F5ACB2F592C704E8E0529D23025 ();
// 0x000002C8 System.Boolean ExitGames.Client.Photon.Encryption.EncryptorNative::CheckHMAC(System.Byte[],System.Int32)
extern void EncryptorNative_CheckHMAC_mA79C7F22D0D84939D9B1F984EAE5563F3DA2D79A ();
// 0x000002C9 System.Void ExitGames.Client.Photon.Encryption.EncryptorNative::.ctor()
extern void EncryptorNative__ctor_m47A1CC6BFCDC8AEB3D74A1DEE80C8BFAB52C30BE ();
// 0x000002CA System.Void ExitGames.Client.Photon.Encryption.EncryptorNative::.cctor()
extern void EncryptorNative__cctor_m38BF852AD4B6D9592D7DCCAE7819517911E83E81 ();
// 0x000002CB System.Void ExitGames.Client.Photon.Encryption.EncryptorNative_LogCallbackDelegate::.ctor(System.Object,System.IntPtr)
extern void LogCallbackDelegate__ctor_mDB4F8F225AC10C75C2F7AF70EE155D7AB2BBDCBC ();
// 0x000002CC System.Void ExitGames.Client.Photon.Encryption.EncryptorNative_LogCallbackDelegate::Invoke(System.IntPtr,System.Int32,System.String)
extern void LogCallbackDelegate_Invoke_mC2D5A5E34B1C37937D43A79765D9DA03224867AB ();
// 0x000002CD System.IAsyncResult ExitGames.Client.Photon.Encryption.EncryptorNative_LogCallbackDelegate::BeginInvoke(System.IntPtr,System.Int32,System.String,System.AsyncCallback,System.Object)
extern void LogCallbackDelegate_BeginInvoke_mE8E333AA921C84442715FE745CA7E869E13BD2E1 ();
// 0x000002CE System.Void ExitGames.Client.Photon.Encryption.EncryptorNative_LogCallbackDelegate::EndInvoke(System.IAsyncResult)
extern void LogCallbackDelegate_EndInvoke_m27133481EF73D81AF6AA3A51EACDAB9B6FDEAA56 ();
static Il2CppMethodPointer s_methodPointers[718] = 
{
	OakleyGroups__cctor_m6DDB6F8F3F62E0D0C0C6B01247463FBE8669B4AC,
	NULL,
	NULL,
	NULL,
	NULL,
	DiffieHellmanCryptoProvider__ctor_m79DEA3B0297FB0DB21D1A1EA16D6BF14EB472703,
	DiffieHellmanCryptoProvider__ctor_m46EB7E11B85E1D31FD493D5F3C2F86C67642E434,
	DiffieHellmanCryptoProvider_get_PublicKey_m8E7276BE42A886E230A2102ADF13EA29752173D8,
	DiffieHellmanCryptoProvider_DeriveSharedKey_m2031692DFBB7F2119F974FE4BA88E38A63C47B4A,
	DiffieHellmanCryptoProvider_Encrypt_m5E52663C39B82965DFC05AF0FD1178F2631EA879,
	DiffieHellmanCryptoProvider_Decrypt_m0E801317D4E936DD7C0AD02E83EDD78699C9CCA5,
	DiffieHellmanCryptoProvider_Dispose_m1D49D0A52C2E0758D7BF801BA798ED40BB7DCFFA,
	DiffieHellmanCryptoProvider_Dispose_mC3B9950CECA79C1292681C1D10430C515CF4E451,
	DiffieHellmanCryptoProvider_CalculatePublicKey_mFD9D62DDC1BCF6F9C17C82B0EC75A68C9E450A76,
	DiffieHellmanCryptoProvider_CalculateSharedKey_mA7119E92EE90CC0C4A398C9EAD064D06FF86681E,
	DiffieHellmanCryptoProvider_GenerateRandomSecret_m4D455EDCFA647BBE315C940F1018C43FB67C2827,
	DiffieHellmanCryptoProvider__cctor_m8FA2325AB3859E7C9CC2C4C0AE3111031F5AA1FD,
	BigInteger__ctor_m1874B51838EC4F8D067FEA413B7F7F1DA8130B0A,
	BigInteger__ctor_m243E7CF41BF85F94A83650D3549E5B0FD57D17A6,
	BigInteger__ctor_m576241D78F756CF92A6B8F36D49131905132FB30,
	BigInteger__ctor_m5762E042F9203046C63BF713C5ABC31435288287,
	BigInteger__ctor_m036EE068346EBE52AE4875CC651C16A0B758087F,
	BigInteger_op_Implicit_m564A43BF8BA574B94C12CB08E7D3935234F1B340,
	BigInteger_op_Implicit_m955F82B86090B094E03C8F65FDB5BD25E3BCD474,
	BigInteger_op_Addition_m7FE8E41C912CF901484A25B61DA96FFF62FA72D9,
	BigInteger_op_Subtraction_m2D0D848548637F085163E674918CD89F3A489E7E,
	BigInteger_op_Multiply_mC4CCB2CA2EF1A303C21DE4574F8692C459AFA2AE,
	BigInteger_op_LeftShift_mA686E81F08F5670489FD740A4EA834B1D4881545,
	BigInteger_shiftLeft_m161B412AFEA159F6F484108D6F863213EFEF1036,
	BigInteger_shiftRight_m37AFC8BCEAAA5F2BE3AEA7F15148107964B7A17B,
	BigInteger_op_UnaryNegation_mB68C807CD19497DC1CF710E872C5E3BD33E22E80,
	BigInteger_op_Equality_m7A14B9D0B86AF4EE72F306B6F9E76286BFC3CF11,
	BigInteger_Equals_m86A81AF95E6303444DFF3366FAA4EACA1B23B12B,
	BigInteger_GetHashCode_mE55145DCF56C5AA6974EFB3562A3C184CA7FC06B,
	BigInteger_op_GreaterThan_m2A5106342053197A5390A39024D34FB3502EADA7,
	BigInteger_op_LessThan_m993DD48692FFC429A570745FE4C9B22AC7A28D8B,
	BigInteger_op_GreaterThanOrEqual_m3E6B736FE08BACC445EC227FABCCFDF64686EB44,
	BigInteger_multiByteDivide_mF7FACC442AFB7BB6DEDB9EE404CF2548BB1A76FE,
	BigInteger_singleByteDivide_m7FBE625577D8FE564CACA2FB5AE4F3A5FABC40BB,
	BigInteger_op_Division_m768C174CCBBE2F82B71FD6BB08E83F2300D61B41,
	BigInteger_op_Modulus_mD5ACC815712A15417EEFC142B590F9FE17014EEE,
	BigInteger_ToString_m5CA343F2EEDC85396F38C1B6EF5EA9C1E9A3DC6B,
	BigInteger_ToString_mAE2F840EA532BDEA84496BCFD54CD07E461A75E3,
	BigInteger_ModPow_m601C7A7757CFB06C7A53A5917F72D77F407CD679,
	BigInteger_BarrettReduction_m53E7C8DE9491A545786AF1AB24817E89869CFE1E,
	BigInteger_GenerateRandom_m0848E8DE2C5BBEB276D3776905731DD5EDA0BBBC,
	BigInteger_genRandomBits_mE93D15A5E6FA31A997431B11B2F7C15C5F674812,
	BigInteger_bitCount_m5DB8FFF1FFBE4EA9C7F6C9F2F14A5526B313731F,
	BigInteger_GetBytes_m4257C5540FA8974B19667A2F6CD2605E2AAE8862,
	BigInteger__cctor_mD67596AD7B0D05C834E7503DAA44DFC0885D988C,
	Version__cctor_m14FA82A4734CCE23AF054CC896685FDDE296B572,
	Hashtable__ctor_m128B1C4B6AC5E9A88A74CE1582751C1E733DB223,
	Hashtable__ctor_m137EE3DF5F91DB16F2D2D3BD45CF196B6306687E,
	Hashtable_get_Item_m5B65D1232714A2D0A39445BBE953D0E4397DA16D,
	Hashtable_set_Item_m4929053EB49FCF167F875347908C3DAB626294F1,
	Hashtable_GetEnumerator_mD62D5FC1FCC3DA732D47FFF231D643898EA041D4,
	Hashtable_ToString_mBDDA2B0E463275F55D9C89A5092C66681273265C,
	DictionaryEntryEnumerator__ctor_m647C4D53CFE27CB1EACBD36C4DDA876075EC197A,
	DictionaryEntryEnumerator_MoveNext_m413A422B68D5BB659B81774B9A9F717CFC39615C,
	DictionaryEntryEnumerator_Reset_mE411A09A768941623E04AF230AFE5505691A3B41,
	DictionaryEntryEnumerator_System_Collections_IEnumerator_get_Current_mB64C01E27CBCDE369F6DAB0E546007FDC295A69D,
	DictionaryEntryEnumerator_get_Current_m05B264BAB3FAE6577A37DBDD335043A946A2A988,
	DictionaryEntryEnumerator_Dispose_mFA68B5A6501D365DF03EE49FCE651D5D4BE1FDFF,
	SupportClass_GetMethods_mD990ECE70FA9D65EBF231C3806798916394FC522,
	SupportClass_GetTickCount_m7E3DE0D42353490BEEF7B0BDE433DB9524273B10,
	SupportClass_CallInBackground_m950A69DD6E5EDD16405DF162FF00FE3A289FC21B,
	SupportClass_StartBackgroundCalls_m89F559E64E8C705FF3348421844768D6BBF3C016,
	SupportClass_StopBackgroundCalls_mDCA3A47BA88DCE54D20CAF422821B5D1C8B4DD5D,
	SupportClass_StopAllBackgroundCalls_m4665A9E71A707DAAB95DB42C8E2398A481564DDC,
	SupportClass_WriteStackTrace_m821DDA155CC35B368A43294E76493B041183C866,
	SupportClass_WriteStackTrace_m6B991ED5A73DAB803F2F0EA4A1C8875E6B8F0A19,
	SupportClass_DictionaryToString_m484EDDCC419255410C2D6B751BA82459EF457AC4,
	SupportClass_DictionaryToString_m5D7D958F5EABA733371535051AD73CFF9745A0D0,
	SupportClass_ByteArrayToString_m0C96B9733F80956D39083D0C0E1B2FD23D2BAB6D,
	SupportClass_InitializeTable_mC85E2A519668DE8C524DF621390DD70477B5074D,
	SupportClass_CalculateCrc_m7C0815AFDDCCE3BE687E3B86224711663D504565,
	SupportClass__cctor_mA3FC2DEFE008984D0E2F256C3E8A750CEF164E97,
	IntegerMillisecondsDelegate__ctor_m7C66C161724794B4B20AC908A5A393792D9B6079,
	IntegerMillisecondsDelegate_Invoke_m2AFEAD5E437F6A0F71F57B789216568D8C20CC62,
	IntegerMillisecondsDelegate_BeginInvoke_m57183F46B7EEB4F0DFAC4E60E2B3024CEFECBE0B,
	IntegerMillisecondsDelegate_EndInvoke_m38F6FA9AE47810170226C5C93B7744BB9F372DEE,
	ThreadSafeRandom_Next_mCEA60A24441B4AEEDBF3071F4749C22D9629A61F,
	ThreadSafeRandom__cctor_m78A792257C9FAA7B9F3BB4736229A98384702F67,
	U3CU3Ec__DisplayClass6_0__ctor_m2501B940AE7227E3BB857B8B603D93880B2EC2E3,
	U3CU3Ec__DisplayClass6_0_U3CStartBackgroundCallsU3Eb__0_mCA3AC632216510DBC1F89391354EA454A6D97BB8,
	U3CU3Ec__cctor_mBD253ED84DA8C4C05CC48B9EF51D2357FEBB80D2,
	U3CU3Ec__ctor_m69D8A16EAB5497C0AC656D8608CBE3A6DC5EF081,
	U3CU3Ec_U3C_cctorU3Eb__20_0_m65EC7EE943DCF46EC6901D31E9574A450DB0B073,
	NULL,
	NULL,
	NULL,
	NULL,
	StreamBuffer__ctor_m5FB420807193C10A5CDB1D516F43F80B2E56E7E6,
	StreamBuffer__ctor_m7270D47D34ED9C0C5F3E3B90030D575DF24A5589,
	StreamBuffer_ToArray_m4C35C9D4D60956CE142D34B6DCB09F110F1374E5,
	StreamBuffer_ToArrayFromPos_m989A8E239A9560E5C9236E8F7969BE67DDFD71A1,
	StreamBuffer_Compact_mA1AC3BE238DA60448CBD2F08FC68D31611A99BA5,
	StreamBuffer_GetBuffer_m0F64B92A3E3EDD5B26D118612B34B4294BC8804F,
	StreamBuffer_GetBufferAndAdvance_mDDE205140EB252E6F5D303F42663C88C4D729719,
	StreamBuffer_get_Length_mC0126534A1A0530A79F0D52BDE13EF70BFDA8824,
	StreamBuffer_get_Position_mE759C9306FA73B90D1F65C833E7F799E636E4197,
	StreamBuffer_set_Position_mF3C5FF0F28FCA6C3536B6CF5340BD449CFA484B9,
	StreamBuffer_Seek_m1CD57505422C50EA75A7223AF06760929D71956E,
	StreamBuffer_SetLength_mFCF6494BC42E63E94686D60A2CA4AEBB5DB1B442,
	StreamBuffer_SetCapacityMinimum_m9B922E08CAB2F56155A7694648A3E32A4C4D1326,
	StreamBuffer_Read_m854D8D5F6959D01220745557E9E118FCE7751820,
	StreamBuffer_Write_m2FF513BEA385876C43D2F6D5D5561423565529A6,
	StreamBuffer_ReadByte_m0EEE833E3B4FD50CFCF76D399DF0DE433718BCB1,
	StreamBuffer_WriteByte_m7F8CFF3A3CAE908F77DD821EEAE7665C9BF7BCC2,
	StreamBuffer_WriteBytes_mDC81D1BFC462AF5E4287D529A6D6E460C8DF52EA,
	StreamBuffer_CheckSize_mC501A6A098B1575B6C1E4061D30A4DE63619B062,
	PhotonPeer_get_ClientSdkIdShifted_m5D93452847DB469CB09535F3DC318EE75D663A3A,
	PhotonPeer_get_ClientVersion_mF570F1DCD857456B9AB9FB144EB0FE1DC7E47253,
	PhotonPeer_get_SerializationProtocolType_mC987375B239ACAC881AD4368EAE036FDCBF97373,
	PhotonPeer_set_SerializationProtocolType_m3E1BEB7DF7817F2CCBC28B0E29C9CF2BC51AFAB2,
	PhotonPeer_get_SocketImplementation_mCDFACD136189970F8BC3FBAF03DF1AB4AF5DAE62,
	PhotonPeer_set_SocketImplementation_mC5DD60961EDFCB1FFAA6607AEF8D20393A783DC2,
	PhotonPeer_get_Listener_mB19B6E7B6D70021472A50C6EC6E6623FFF0FFDA0,
	PhotonPeer_set_Listener_mA2C71ECEDB0A8CB522E11D196729E0FEB518EFC8,
	PhotonPeer_get_ReuseEventInstance_m6171C587D78F312F3764457B259397D0E09DF4D6,
	PhotonPeer_get_EnableServerTracing_m862AF0F0031BE368ECF34FFDEBD7114C26BB4D29,
	PhotonPeer_get_QuickResendAttempts_m70A49737144C02FC712FED99515BDBA8F11FC5D7,
	PhotonPeer_set_QuickResendAttempts_m11BBF88009532655638855E3BAFAAA50FD71B3CE,
	PhotonPeer_get_PeerState_m8A6337B9E169A236F13735775CF8D054C32C5027,
	PhotonPeer_get_PeerID_mB7CF922C20ECF23B57674C033511109D9EA395EA,
	PhotonPeer_get_CrcEnabled_mB2F03ED7315D9E8161896307A51107EFF29B0FF5,
	PhotonPeer_set_CrcEnabled_m1D5B1C8DAD50CD1B13E18AC4C5560EDB819FDA14,
	PhotonPeer_get_PacketLossByCrc_m688F3F46480519F55E313CB723A8A24CBFF963E8,
	PhotonPeer_get_ResentReliableCommands_m578321511F0070E81EDBDC5C4EBC662AA99BC664,
	PhotonPeer_get_ServerTimeInMilliSeconds_m36973D3BF6C500BDED6A55B53519487E9B85DE6C,
	PhotonPeer_set_LocalMsTimestampDelegate_mBBEF4344E0C026810E8E5C24FE11F01AA39245FC,
	PhotonPeer_get_ConnectionTime_mB9175384D70F0C54BE6F3B4CF75EA5DEC4A418AC,
	PhotonPeer_get_LastSendOutgoingTime_mFB343AB718D829AE3AB1739418DD805D7CF7D654,
	PhotonPeer_get_LongestSentCall_m12BD70259C53AC28BED5A0DFC7F84ABF49C28385,
	PhotonPeer_get_RoundTripTime_mD1220510B856E469C55C835D64AEA9FB1410191A,
	PhotonPeer_get_RoundTripTimeVariance_m832ACFBCDA38299618294E356BA616A3C1422020,
	PhotonPeer_get_TimestampOfLastSocketReceive_m0C63BF8985D3779F7DB337E2ABB998959B93367A,
	PhotonPeer_get_ServerAddress_m8CEDA789F9F433B2D3C77E373CC3765A9C2837F6,
	PhotonPeer_get_ServerIpAddress_mE22CFE0115DF072ABECCE60A7DBC946A29DA60B1,
	PhotonPeer_get_UsedProtocol_m4A96B5CEBC315986C2D680B6E7E0861E50BE03C4,
	PhotonPeer_get_TransportProtocol_m1DA2EDF0354DACEA41770B1EAF2453A9DD97408D,
	PhotonPeer_set_TransportProtocol_m088ED6A378CC5664E86A19ACC7DDA0927211EC83,
	PhotonPeer_get_IsSimulationEnabled_m29B9D53C229FA00ECABDBB03880F2F3B5291476D,
	PhotonPeer_set_IsSimulationEnabled_m9CBB4D26099F774F660DBAA3B388D7F9C28B29D4,
	PhotonPeer_get_NetworkSimulationSettings_m225E1C716C04B6E5202F7B383AF93B862FDA6A36,
	PhotonPeer_get_MaximumTransferUnit_m4246C48422C652164BE88DD80CC477D11022A0D7,
	PhotonPeer_get_IsEncryptionAvailable_mF6B6222BDB4F3D70E4483FBF2EBA8A82BBC3EA08,
	PhotonPeer_get_IsSendingOnlyAcks_mBB3CB8CDBA42DED733E803AC751D44ABDE230FC2,
	PhotonPeer_set_IsSendingOnlyAcks_m35710A5C13BE678D49B093838529C898E9E086C7,
	PhotonPeer_get_TrafficStatsIncoming_m21C22D464F47A4D62B25DC16D1F396A6BEBCFBE0,
	PhotonPeer_set_TrafficStatsIncoming_m1D9291DA3EC33E78FDE7AA5E6107C1791792DCD5,
	PhotonPeer_get_TrafficStatsOutgoing_m49B5F77DDF3F87292D4C712A2CA6927C8F19888A,
	PhotonPeer_set_TrafficStatsOutgoing_m70CBCFC0BA4C8B659253DBD680B155A2E6BA4B64,
	PhotonPeer_get_TrafficStatsGameLevel_m062C229DDE638D61C7BF7699FC7CC8616D8D8C5B,
	PhotonPeer_set_TrafficStatsGameLevel_mCC6D59F425055DFAFFCBD214D3CDBF70C24EEA78,
	PhotonPeer_get_TrafficStatsElapsedMs_m2C1A5CA757684F4C18A172B18462923626A295B9,
	PhotonPeer_get_TrafficStatsEnabled_m3E2F3A7DB1C87731322D75C06FAA25E35D2FB550,
	PhotonPeer_set_TrafficStatsEnabled_mE552AE4D2E8909CEE98793E017EB09FC3340C24D,
	PhotonPeer_TrafficStatsReset_m8033C02817FA105EA4F9673294500C783C6E97BA,
	PhotonPeer_InitializeTrafficStats_mB03A2F99E1DDFD9BCEFE3EA11D5D66264368469D,
	PhotonPeer_VitalStatsToString_mC72BA74C40278E9FE53750D097D9430213141EAB,
	PhotonPeer_get_EncryptorType_mC29B978D94BFC34D29DDF1A0FF190BDCFD49A788,
	PhotonPeer__ctor_mE85E3E0315790B34E49EA83FA9689F96BDAAEEC2,
	PhotonPeer__ctor_m11329795A816E741CB74149995B87117E9C4A57C,
	PhotonPeer_Connect_m78CB5B88FE197BA1CD178DA4C2A2312949F65E6C,
	PhotonPeer_Connect_m642A0FE519BEFF9C6B0CF36060D622D199013228,
	PhotonPeer_CreatePeerBase_m2BE91CCD43077D50C75C33C2EC4546ED84D2E9B2,
	PhotonPeer_Disconnect_mF94675A2925B67E38F9A47385A7D379735E6592F,
	PhotonPeer_StopThread_mFE00C5E48A115427ABF1972C8E7EC14F7290E3BF,
	PhotonPeer_FetchServerTimestamp_m6EAF351BBA3C543276DE99CECD470EE04D2F78AB,
	PhotonPeer_EstablishEncryption_m699B37EA6CBD6BA786AB5FA115024F98932CA872,
	PhotonPeer_InitDatagramEncryption_m45623D0F081AF6AAE585DAD2AEBC78B5095DE163,
	PhotonPeer_InitPayloadEncryption_mB3AE1B057676502EBCC75BF2748B9E44ACE44138,
	PhotonPeer_Service_m3BDE5A265B81BF990B571E7A6ED99E0AFB0D5BF4,
	PhotonPeer_SendOutgoingCommands_m3DE10C529921BF182A1D82A03D51C0C4F245DE75,
	PhotonPeer_SendAcksOnly_m932A7C8E7B4750203E81B91F088659192BD47757,
	PhotonPeer_DispatchIncomingCommands_m2262FE3FFD98F647C5E851FE70099214BB4D8862,
	PhotonPeer_SendOperation_mF27C3AA82ACFC14A7A1473072A35865A574BDBFE,
	PhotonPeer_RegisterType_m140D65F38FFEF6EC73AFB8D18EF92B0B4480F6E8,
	PhotonPeer__cctor_m4343627EE95DECA021D29643E2FF09317FE06B93,
	PhotonPeer_U3CEstablishEncryptionU3Eb__179_0_mE0B331AE68A5D86013E24F5763CC8DA8737CB4BA,
	PhotonCodes__cctor_mD37D66D134FBFCFC7DD571B559EC2F07CA6B4553,
	PeerBase_get_SocketImplementation_m4647CB9ED146B091C943D5ADBC797AAB2D6F9875,
	PeerBase_get_ServerAddress_m28CA06E3042D0ED845707E78E090F5CE93BA0057,
	PeerBase_set_ServerAddress_m9CE07E488636AFD51D91D3F225512C094CC0B9E7,
	PeerBase_get_Listener_mFF6D9E8A312608BDBF68C66667099AF35D8BCE04,
	PeerBase_get_debugOut_mBF471576063280A694729303723E9344397DA5C4,
	PeerBase_get_DisconnectTimeout_m90AB0B41BDD39F8EFD25E9AAA73427082719AC13,
	PeerBase_get_timePingInterval_m98540AC460200073C31FDFA84E7B16A53265BF98,
	PeerBase_get_ChannelCount_m20BE2DEB563763DA31A8D9226E05D5F9E2D320BD,
	PeerBase_get_PeerID_mFA80C2EE1F5994DE9E7702C57200E2E9CA2CCAEB,
	PeerBase_get_timeInt_m28DAFD454927A2CBFCEE09FC6AE23259A2E7287A,
	PeerBase_get_IsSendingOnlyAcks_mE068550D91457552C8C1B0DE32876B4AB4520E6A,
	PeerBase_get_mtu_mF93805524D1839EF2EFBC403079C1DA604FBCC77,
	PeerBase_get_IsIpv6_m6E1045BE36303BEDDD3E8AF61D6F1900658B06AF,
	PeerBase__ctor_m7D86C5ADE5CBD10BBA9921BCDF5F5ADBDD730CF2,
	PeerBase_MessageBufferPoolGet_mBB8003365B4191B751D1A5272CF8862419298D5B,
	PeerBase_MessageBufferPoolPut_mC0468FEFBE6D6145D96D06C9E9B8BC14008E0F53,
	PeerBase_InitPeerBase_m5BEB668B5B9EA1507B8C36FD03AD0113222946D4,
	NULL,
	PeerBase_GetHttpKeyValueString_mEE271AA6E1515967DA5A9CB40C85C2F0606EF071,
	PeerBase_PrepareConnectData_mC866ED530BC8132628DD0F485CA23E311FAB5EB7,
	PeerBase_PepareWebSocketUrl_m243DF4DAD2D05083C94CBB262F115B304577EEB8,
	NULL,
	PeerBase_InitCallback_mDB98B0F7ECC86896F2359E7841694399B731AB77,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PeerBase_SendAcksOnly_m277C420F7AC57D351F1DFB0C29FA61C15917FAD0,
	NULL,
	NULL,
	PeerBase_DeserializeMessageAndCallback_mA6864B242B23087A4E00FD5F288A23F524E4B250,
	PeerBase_UpdateRoundTripTimeAndVariance_m01078129E510E9D76C5B931C2D6F55E99D605D3B,
	PeerBase_ExchangeKeysForEncryption_m0A913DDBC8AC8386F0BDB8AD9DE756721294B1CC,
	PeerBase_DeriveSharedKey_m0B310EC273143701E3E4F4B822226C5F84CDEB44,
	PeerBase_InitEncryption_m88CB0C7CB0363615A733CD8CA865C3E8EEE9B0B2,
	PeerBase_EnqueueActionForDispatch_mA1330C6E75D7C2E58DD59EB2466BF53CAC044C9E,
	PeerBase_EnqueueDebugReturn_m2319EB790FF6ED98AEC67E26F910CCDE5845C6D9,
	PeerBase_EnqueueStatusCallback_m5D276AF8B7A79BE931A02825CC4230426021C6F3,
	PeerBase_get_NetworkSimulationSettings_mD0E02A66EFD537B5B1B3A455E2100134C8E27F08,
	PeerBase_SendNetworkSimulated_m7ACD4176F1C7F784E938828F890F3DC8CB2632F8,
	PeerBase_ReceiveNetworkSimulated_m5328F976625000D0026C83E9A9F34D5B378280EB,
	PeerBase_NetworkSimRun_m1E53765FB212A50487DFCAE4B1C8BF1118B669A0,
	PeerBase_get_TrafficStatsEnabled_m24D65CD7EDD321B84495CFFCA08732D07FCA9365,
	PeerBase_get_TrafficStatsIncoming_m43F4F47DEC3C382DF37D5B75D017395213DEC28F,
	PeerBase_get_TrafficStatsOutgoing_m6EA662978E51E03E5F363ACAE2B2F1B8737E0493,
	PeerBase_get_TrafficStatsGameLevel_m6DED18D8C309FFA7AB3F5423482A2385620FFC3F,
	PeerBase_get_CommandLogSize_m743F6D9E1A1C426555BB9F96E7633FAA7DC776B4,
	PeerBase_CommandLogResize_m06318B6094B33D22EB65594A28C792FD22F360EE,
	PeerBase_CommandLogInit_mA3DC117204D1EFF34C793B81DBB2676282CA15E5,
	PeerBase__cctor_mD60C91CD707F8E4BD9ED454D2AD76E7D33D4C9D0,
	MyAction__ctor_m23B43F51DFF614D2A1C30B4844562512468B1206,
	MyAction_Invoke_m302257D976009F277BCA041C5E96362E83F64F16,
	MyAction_BeginInvoke_m0FB7DB17CA9539AA30959C05F3B5C1AF7D430EF6,
	MyAction_EndInvoke_m257CEC57AEE975A5540D66FC581693DD65E04C7D,
	U3CU3Ec__DisplayClass104_0__ctor_mBC4D46D56EA3594FB1897D17F7F58DA61CE620C4,
	U3CU3Ec__DisplayClass104_0_U3CEnqueueDebugReturnU3Eb__0_mCE0232F5F58178738301BB7AC583D2EE2EEB7128,
	U3CU3Ec__DisplayClass105_0__ctor_m0AAE731F9C372DACBC9769F1D16C964CBE59D147,
	U3CU3Ec__DisplayClass105_0_U3CEnqueueStatusCallbackU3Eb__0_m2886FC67F93137EC2CCD634F67FCFF9B0652C2D7,
	CmdLogItem__ctor_mCA536F132FE73C844CE874683CA6B340FD724463,
	CmdLogItem__ctor_m632D35EDB2311E94674FB67D9E310610B174D9F1,
	CmdLogItem_ToString_mEECEBFC61EBC2134E10E2AA0394962D3A8CD892C,
	CmdLogReceivedReliable__ctor_m69DF1708D65680A1F5E00C41984F7A393F7EE79F,
	CmdLogReceivedReliable_ToString_m0DAE2973BD044EAA8A14EF0089001271FA869F04,
	CmdLogReceivedAck__ctor_m650A47F63C299A251CF5DC28543596432515A0E9,
	CmdLogReceivedAck_ToString_mC0433A2B4C3F124AF9698784F50E0FF18BC77037,
	CmdLogSentReliable__ctor_mD7B3951F8FD50AE82769A8CDDB5066A97FF27018,
	CmdLogSentReliable_ToString_mF2EE54EF09496849C74039D409AC8DD4C3FDB5BB,
	SendOptions_set_Reliability_mE2EC7041FF3CAC3F5A4D63C15D1E8ECC6703903C_AdjustorThunk,
	SendOptions__cctor_m75F23CE5062907B0715D78321E8DD4051C7860D8,
	EnetPeer__ctor_mF4E82D0EFF65EACEAE255E8F1DF553E0EEC8D4CA,
	EnetPeer_InitPeerBase_m7C5A2483E2BB5680BEF2715D1FBEDB7C6409F4E4,
	EnetPeer_ApplyRandomizedSequenceNumbers_m10AEA46A8042D0845FC57FA3C6EEC7E8A41596E4,
	EnetPeer_Connect_m84BF77CFE0958B1D610C14F361117ED9A454A526,
	EnetPeer_OnConnect_mA6EC2D4F2C34D712E671BD9149E4AF83C52DA502,
	EnetPeer_Disconnect_mD657EA685669D998F3A2A879D63F1EDEA3C6157F,
	EnetPeer_StopConnection_m424ECE77ED61CA5AF9AD54C2FE3267E37088BBD6,
	EnetPeer_FetchServerTimestamp_mCFDB72D8C4169733EB75D9D24A7AE94F7CBC2D1A,
	EnetPeer_DispatchIncomingCommands_mFEDB0149FB9D93C374093600A2E0C58D877373D8,
	EnetPeer_GetFragmentLength_m46BE3096F819766D7F985DD3D35E4CE0F0FA85EE,
	EnetPeer_CalculateBufferLen_m4F93B333E6568D2D941FE8DCF51A704ACE5C71C0,
	EnetPeer_CalculateInitialOffset_m38B118FBE2F90CCAEF28304DCAFCEFF7E66A31EA,
	EnetPeer_SendAcksOnly_mAE2ABF55D7622D273B000EDAD4E2BF820FBEB6DF,
	EnetPeer_SendOutgoingCommands_m1D90EE9F92064E29730AD8683F07EF7EC0361F64,
	EnetPeer_AreReliableCommandsInTransit_mC52FAE7C6E74D11923EA9A157DD14D4BA2408C66,
	EnetPeer_EnqueueOperation_mD3A706F1C279F85D1B2DE2C62661595F1FF35B43,
	EnetPeer_GetChannel_mDB97980F8E29D5B7975C7FA57EB07145B4B15527,
	EnetPeer_CreateAndEnqueueCommand_m24C26381CF79AAC5F78969586F5BE4FDA939D217,
	EnetPeer_SerializeOperationToMessage_mABA2E0A5D6A3351B10AC2774F3F9ED7DBE66D3B2,
	EnetPeer_SerializeAckToBuffer_m2C2FACDD3EF859EBB2CE44307DCD9E46F37291F1,
	EnetPeer_SerializeToBuffer_mD7B07335AD2323BB8B3E9FAFD84B0142EC019C77,
	EnetPeer_SendData_mDE8A8A05365FEBFD025B0C5281FF6281E474FC39,
	EnetPeer_SendToSocket_mCE1A8171F5D5EB322E9C041FA57CFBC89EDE2B66,
	EnetPeer_SendDataEncrypted_mA33A067C949754D31A149E532EF15C23250D9F18,
	EnetPeer_QueueSentCommand_mC93E9F41EE58832860DC309380EA1D8BE7F5F8E4,
	EnetPeer_QueueOutgoingReliableCommand_mD406D1F04F18EBCC40C22D4D5CDAAC1247C1FD00,
	EnetPeer_QueueOutgoingUnreliableCommand_m474B26B6976278F8563DA33EF5F83081DF93BC3F,
	EnetPeer_QueueOutgoingAcknowledgement_mFB629A0EFFE9F0DA90C0D48E747ACA9D98197416,
	EnetPeer_ReceiveIncomingCommands_mD89F882DFEE9B00D8255FCAFD688ABA8EF216067,
	EnetPeer_ExecuteCommand_mF19A2B9B0FC53644D850610FE4AFC3DAF4D3E352,
	EnetPeer_QueueIncomingCommand_m5D64C855E3F9CB02EDFE7BF017AD8EA478FAEEEC,
	EnetPeer_RemoveSentReliableCommand_mF75DBC5760D184F9BEE6B004EED8087E97913696,
	EnetPeer__cctor_m79DF68D6A8AD8704C58922F524E758B274FADDAF,
	EnetPeer_U3CExecuteCommandU3Eb__71_0_mF27C21CA35A2A52AFF5B94974A8EE2BC90FE3B2B,
	EnetChannel__ctor_m9809886C8DC822F3C7EEC4996AC816414371C71B,
	EnetChannel_ContainsUnreliableSequenceNumber_mA159FFC30FCA2FB55F40BF17A1647F929E221838,
	EnetChannel_ContainsReliableSequenceNumber_m7A44850F1DB28C723A20E95D0C9AC74B225B8124,
	EnetChannel_FetchReliableSequenceNumber_mDB2427D6F91A8C73F121918EFCF32DDA21496BA3,
	EnetChannel_TryGetFragment_m2438ADDC5D5095540ADD7351B8741DDA160FA2DD,
	EnetChannel_RemoveFragment_m2869A04E6476AEBC683E131C857119298310447A,
	EnetChannel_clearAll_m1DC1086F63C3C8D3DF1358D650C6D20667E364EC,
	EnetChannel_QueueIncomingReliableUnsequenced_mE54FB77CA623CE443EF26FC2EAB0CA4619B821FB,
	NCommand_get_SizeOfPayload_m5D453FED7B59577C1C947F7B52EE0BB2483B00B5,
	NCommand_get_IsFlaggedUnsequenced_m42113687B85BEA75053F662EC7996EFA85C6D043,
	NCommand_get_IsFlaggedReliable_mE079B022D8FCEC2E41FBF55327C535696B78201B,
	NCommand__ctor_m5D475113D2712F02C458550C88FEEA3EC3FC9623,
	NCommand_CreateAck_m8CEEC27DE1D315696E00F47D28CB5A7EB6B29204,
	NCommand__ctor_m9B54AE53932683F81A579FF01438D14E20456FA2,
	NCommand_SerializeHeader_m682386C18CFE0BD32AB3F432D84FE6554CDCC78B,
	NCommand_Serialize_m8EEB3FC26E807DA04649171A1C27F6D4EE900754,
	NCommand_FreePayload_mCDF9213F68CD0CEB4B707FDBA18FB104CE53E48D,
	NCommand_CompareTo_mF886FF1046C4323DC7E78BFB0FF0DAB946386A47,
	NCommand_ToString_m089E773F2F24190D8625E6869139B0979CEAF2DC,
	TPeer__ctor_mC978FB8C22DA016B53F0B6866C8B1694B2D8EB7D,
	TPeer_InitPeerBase_m155C5D48B50B6328AA9CBEE96247C6C41AF3B1F0,
	TPeer_Connect_m1E6B60187F8FF172B4B828E773066E0C841B9E97,
	TPeer_OnConnect_m7878A5993B2BDEA0BFDC103000672F3DA73589AF,
	TPeer_Disconnect_m2F59E0592F77D0B0E3549A20DFC78D05F73589DB,
	TPeer_StopConnection_m6FC45F4BADAB138CF0682FD1A2A0E3F6741DBF7B,
	TPeer_FetchServerTimestamp_m53E65FABF237D447916293ADE3AEAB4813090F27,
	TPeer_EnqueueInit_mF10041F585220020438730E6788B6DCDB55C7941,
	TPeer_DispatchIncomingCommands_m92BF484E80C1E529802CDB85F9EE71D49514E06A,
	TPeer_SendOutgoingCommands_mFDD5AC837638CEC82EF87402F66EE6BAFF32D06E,
	TPeer_SendAcksOnly_mD0A7DC8CAA7C0B8AAB4A519175B870EABDFA09A3,
	TPeer_EnqueueOperation_m3464B044EF3995C436B175E28BC85F5C27AACEDC,
	TPeer_SerializeOperationToMessage_mD8378F051AABADE1C9E29312A20EF60EDB2F2AB0,
	TPeer_EnqueueMessageAsPayload_mE7A99ED180941FE5B2081A0092E6057611605E8D,
	TPeer_SendPing_m0F73ABE4C4E09F96D49D99D32B0AE959539C4C83,
	TPeer_SendData_m00C955E5184E0B8BFBD38E83F9934A5F5CE861FA,
	TPeer_ReceiveIncomingCommands_m74AD3B904C13D114D40FF2521D7DE53860CCDE26,
	TPeer_ReadPingResult_mFA183D9868E97D1F32D506AFADBAAE9656280A97,
	TPeer_ReadPingResult_m4A32154C796613F24A06E6FD36661F98B17E8EB5,
	TPeer__cctor_mAE2AEB3DD0194417AD845595E60210C302F27118,
	SerializationProtocolFactory_Create_m09C1332144808DDC2EF6D846AE0D5DCF1A3B7E32,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IProtocol_Serialize_m032815F3087BEC52DAF2FADA759EB9FC02E86D65,
	IProtocol_DeserializeMessage_m9E8B1893F2542A56F8BAC2E2E43B1E71A4D87632,
	IProtocol__ctor_mBF064D333DD172B2AD7D9BB684BB10D34CA1AA8C,
	OperationRequest__ctor_m2CCE6648C58100E08C7E664C90A41C99EBAB62D6,
	OperationResponse_get_Item_m3C3F6BDDB750CF51D887F5D29FDA402DABFA6E38,
	OperationResponse_ToString_mB9335FAE8C266361E8FE02FC6051374B7599E724,
	OperationResponse_ToStringFull_mB998F0712031113D2239A21059C59567B12F463C,
	OperationResponse__ctor_mB702440274BFBCE5293A3B25BD46550C990EACF6,
	EventData_get_Item_m8DABB0E7B8B21377AA5842DAF82E1828F2B2F728,
	EventData_get_Sender_mD3182863B51AA76D316626308334753F90025255,
	EventData_get_CustomData_mB502B0552037F132F4CF131850141F835826060C,
	EventData_Reset_m399519D3A2528CA5FDAB55616F1D568A631B4EDE,
	EventData_ToString_mC92D919944D5593674A9DA3FDE2C2DF8175F4F64,
	EventData__ctor_mDEC4EFF4DA13212388C984C8D794E5E83E3E826B,
	SerializeMethod__ctor_m31A10C573F1E426CB601759D689FE1769D0170CA,
	SerializeMethod_Invoke_mFC77F6F80C0F4919655054760BBD667C13B69E62,
	SerializeMethod_BeginInvoke_mB8E3AC52E7CB89709059AE76730AD5A818B7EC45,
	SerializeMethod_EndInvoke_m58345D95D3CEF61D75453C5F4D32B04851EEEB69,
	SerializeStreamMethod__ctor_m62DDB8F9059743ADF1212DE8107F40ECC077D773,
	SerializeStreamMethod_Invoke_mDDF000B9E8494155E55E59B288438CC496B6566C,
	SerializeStreamMethod_BeginInvoke_m3111B38CB027B8CE7643ECA8A00AD47C8CBF44A1,
	SerializeStreamMethod_EndInvoke_mDEA54F874FC3C3909EE142FCFD38AE1EE0399EDB,
	DeserializeMethod__ctor_m0AD9010015F9B1482A572176647C9C74AC87B6DF,
	DeserializeMethod_Invoke_m80C94E8D9D4EE94D70254B175121D7D3DC7086DD,
	DeserializeMethod_BeginInvoke_m615EDEC58CE77DD95D795828F42D18869BD98B7D,
	DeserializeMethod_EndInvoke_m1B493BBB64859172D5F492ED208BADBCCA472975,
	DeserializeStreamMethod__ctor_m7217FF56F768B5C35297BEE05F684DECE7FC5153,
	DeserializeStreamMethod_Invoke_m0722D9CFBA30747E6D395AD40FCEE38B5DDA27BD,
	DeserializeStreamMethod_BeginInvoke_m86C41EF7632009A58738AFF8838D4EB68150E484,
	DeserializeStreamMethod_EndInvoke_m47CE8A6D087D697C82F8D627E8AC166C29F94B86,
	CustomType__ctor_m050B94D1713B52A53721079532311A0190889881,
	Protocol_TryRegisterType_m5D502BBDFA7C1FE7AF01804710164DBF3222733B,
	Protocol_Serialize_mBAFDF59241DBC2D5AF21FC4BE5A40E2520897EC8,
	Protocol_Serialize_mA541EF2B245999813353D445FD9A666FB2B16EA4,
	Protocol_Serialize_m03A37FAA6695AB344985B02DFD6010DBF0FC9107,
	Protocol_Deserialize_mC3BEF234327D479EE08903C244DB09F1049267D3,
	Protocol_Deserialize_mBE2E567D607AAA500EC4B74D52981AD474CA2BDA,
	Protocol_Deserialize_mEC010A79640C90AD545EFA1338A8BEF895FD2EA0,
	Protocol__cctor_m64884E2DA6FB8A64181CD0409B9593634D1E18AE,
	Protocol16_get_ProtocolType_mEF3335B4B343053A2193DAE59F8AD21F8FB16537,
	Protocol16_get_VersionBytes_mDCD6CC44785D2317A4C75F33849EA1BACE5625AE,
	Protocol16_SerializeCustom_m03F4278E496D356865CAC1321A555488A9A4FE66,
	Protocol16_DeserializeCustom_mAC30492952B0F44FB89EA161921CF17BD343AECD,
	Protocol16_GetTypeOfCode_m9D37CBD9D4A1EFB8AE06C3B3D9EAF0D4BB55F5D9,
	Protocol16_GetCodeOfType_m29ABC36AA1207F5DF9B8311984A6234F592C4F52,
	Protocol16_CreateArrayByType_m8B3FB1BE7CC0C2EE962ECDBEF5AAC2EDCA8AFFA1,
	Protocol16_SerializeOperationRequest_m81757C144B536C26FCC01DECBEBC03DB6E98C355,
	Protocol16_SerializeOperationRequest_m4EBDF610C96D8587C68F4C714A6724E0FD78A0BF,
	Protocol16_DeserializeOperationRequest_mEBD2710E5DC7703FCE45253AB1F2A05E0EFDE0D5,
	Protocol16_SerializeOperationResponse_m36DCCD424ADECA4E02FFE2C1942A1B64753E90F4,
	Protocol16_DeserializeOperationResponse_mB41FFDC25306804310FE963ECBD9A5411C5FA06B,
	Protocol16_SerializeEventData_mC5F8C026E7421669C9E5F005C836B12E62935482,
	Protocol16_DeserializeEventData_m0B1A645ACB7747B78982566C8457DAABF3816357,
	Protocol16_SerializeParameterTable_m5187051C8DEDE687D8CD5A7A48180DBF9A7BD557,
	Protocol16_DeserializeParameterTable_m345F15648330E20C16944D5021F4228359FF5118,
	Protocol16_Serialize_m6D5D5506FC7019CCF8A5EAD3844FB2EFC818C04D,
	Protocol16_SerializeByte_m46FEB067F4509A57EF9A6461680888C3F3A8EA3A,
	Protocol16_SerializeBoolean_mFFF768664B7F2885AD5025969B03E5841D5F220A,
	Protocol16_SerializeShort_m4D8A2CF9D94C2651CB465266603A2FC06AD09DAD,
	Protocol16_SerializeInteger_mAF92DE73951AC804D903765A899FA7CF2EAD7988,
	Protocol16_SerializeLong_m5B9E736D283B8A87D223E88229583F702C6A5778,
	Protocol16_SerializeFloat_mC4BB247E04CF12C664F2010ECBF76C27C08C046C,
	Protocol16_SerializeDouble_mF13D6D27DB4F0CD564EBF9EAAE3DB4EF8FAD49CA,
	Protocol16_SerializeString_mC78290BFDCE1ADB519A9ADD11045534C3095E197,
	Protocol16_SerializeArray_m16BD43873E4ED1208B367ABB69A38B25843E824D,
	Protocol16_SerializeByteArray_mCFFF9506539F331AC7339C1BF8B14939CC38E9D8,
	Protocol16_SerializeByteArraySegment_m5F70C7E987F02C742AA489957B7C9C0D9FFC23C6,
	Protocol16_SerializeIntArrayOptimized_mBFB2D3E8243D449897207A33276C662D34F39E58,
	Protocol16_SerializeObjectArray_mE7F958D827723400DBDB4823541FA4953266B6AE,
	Protocol16_SerializeHashTable_m54301A664259057E0BCD7A5948B1D00251C05D34,
	Protocol16_SerializeDictionary_mB856FF8F65235D7A0514098CDF121623ABDC6B2C,
	Protocol16_SerializeDictionaryHeader_m5803CE6DC0DD7459D30D28592802E889A8A0E278,
	Protocol16_SerializeDictionaryHeader_mC4D9588211B9680862879BBABCFDD9A09D420862,
	Protocol16_SerializeDictionaryElements_mF75828C38FA06044C375190CBFAE570738C103DF,
	Protocol16_Deserialize_m00281AFE308E39580DD669AFBA0F95B76B189896,
	Protocol16_DeserializeByte_m99E00B3A651FD62DC6F1015EF95C6411D047CB8C,
	Protocol16_DeserializeBoolean_m5B863A011742C3275F4FDED8960B1C2B5B094E18,
	Protocol16_DeserializeShort_mD420A1DC9319D892CA3BC2D8C842EC8B2F48A7F0,
	Protocol16_DeserializeInteger_m6BB87CFBA3E9D35E783E92006C1D01B7D6B20002,
	Protocol16_DeserializeLong_m0A5A359E00C8D26A37809978E9DDDF156A5501A9,
	Protocol16_DeserializeFloat_mCBEB4FE45A712381B6DC457D24676D526623E814,
	Protocol16_DeserializeDouble_m78AAC151A5C89CB259ADD5E2BC502D0677604007,
	Protocol16_DeserializeString_m768E351F55F5039007B63DF791E80897A2CF9BE3,
	Protocol16_DeserializeArray_mC317F49B9C7FCDDA631B582732E4E7DCA78E1BFC,
	Protocol16_DeserializeByteArray_m6CC3652D4E0A798C9488F6FF66A9AEEE7F2EC0D3,
	Protocol16_DeserializeIntArray_m689D43E9935374CDCAAFC2D6DA569F18A5BAD623,
	Protocol16_DeserializeStringArray_m69872A7A8E1D480976AF233E5CDECADDD95AEA89,
	Protocol16_DeserializeObjectArray_mC3069BFA6D60FB091E5F90F1153B999900DE7985,
	Protocol16_DeserializeHashTable_mDBDD0124A8B59EE1AC01F6ADC2BD18123C05E9BA,
	Protocol16_DeserializeDictionary_mE43A156237FB40344DD7C55AC4441A9B115B64A9,
	Protocol16_DeserializeDictionaryArray_m5E9B93F27597D641B9EAB3121988F8FDA12F8A76,
	Protocol16_DeserializeDictionaryType_m5E1E320B4281DA3C92D3AD0AFC479F504C7A8E01,
	Protocol16__ctor_m81031362D9ADAF37E84AF544BA223790BFDC4DB1,
	Protocol16__cctor_m2B1FE742BE2586A2DAAED91533AAF9314B020859,
	InvalidDataException__ctor_mE011B2C659108CE372955D10ECF410D3EAE32D4C,
	Protocol18_get_ProtocolType_mB9B5C84ED6340AD2BC40DDF6FEEA78A81E1D7DB7,
	Protocol18_get_VersionBytes_m6799465BD76B6B0198C39AE3779ABA1E264728AC,
	Protocol18_Serialize_mE131573B59A7FFC97F510494402D24135A86C6C1,
	Protocol18_SerializeShort_mA091B93B1732345BD8B67AC7EFF237D4711050CB,
	Protocol18_SerializeString_m28634BAB8108367A37094D6521FE9B3E40EA1877,
	Protocol18_Deserialize_m215BE13B261AA318860C1C83CFDD28A244437E60,
	Protocol18_DeserializeShort_mF25F5E016FFB96FD17B2FD62F4B076DF9808BF1C,
	Protocol18_DeserializeByte_m307F7BF90033DFDE12FF73097CBBAA2AC9AC64D6,
	Protocol18_GetAllowedDictionaryKeyTypes_m6BCFBB8366A1FF4D1247986670C2645C3108F10A,
	Protocol18_GetClrArrayType_mA6A39B7F5737083092975EF356767153F7562434,
	Protocol18_GetCodeOfType_m8FB25C633F98D1F10A8C61EAA60AC3FA52BC6D5E,
	Protocol18_GetCodeOfTypeCode_mEECEE3F5F09D03BD7A4A8CE2EE4FFEB026F9AFF8,
	Protocol18_Read_mF066333C7A1AED49DB6B39C0F210C1EA10619169,
	Protocol18_Read_m970EDFA63A97B3C5708EAE3BC25CB880BE3F1E51,
	Protocol18_ReadBoolean_m890DFD9FB3AC915747833B3753BA6B9C17D93435,
	Protocol18_ReadByte_mD38375BB3588EB63E66F7675963F492B9A033532,
	Protocol18_ReadInt16_m002D54DDBA732168769C94DCC565DDC99F9E9070,
	Protocol18_ReadUShort_mA09B54D4B2951E19BDF6105FF0A5A72387D8E4C3,
	Protocol18_ReadSingle_m00A801F067FB8AFCD19D0E34A4CC681AC6E80FAB,
	Protocol18_ReadDouble_mB53AB954E9F3E31D1FB2F73EE2130BF5CEE08FA7,
	Protocol18_ReadByteArray_m5B28D578ED6374A0D2F91B12683AC4422300696C,
	Protocol18_ReadCustomType_m717E7EBAD098EFD0DF90FEC65A5B89E36DB3D341,
	Protocol18_DeserializeEventData_m5E36311112CE1D161D2260D0D92A3435444D30E8,
	Protocol18_ReadParameterTable_mFE0ABB79843803ECD0CD84E2B312EC07F23B9CD2,
	Protocol18_ReadHashtable_m270DDFF504559C4E08F8437AE91E8DB9EC2D8024,
	Protocol18_DeserializeOperationRequest_mB3F5B0A8AA2DFFB35B9F4B9CAF4BB4A811831EF3,
	Protocol18_DeserializeOperationResponse_mE21AA412C0CA74BD2728999EA822ED93832D9289,
	Protocol18_ReadString_mEF6EBD3AFC3098AA2974E1459880B4BB25FBF2DA,
	Protocol18_ReadCustomTypeArray_mC334FE546D75907F21037548263E86D62824A2A1,
	Protocol18_ReadDictionaryType_m7C7B19B42DB48595EEF3974053046CE8DA604130,
	Protocol18_ReadDictionaryType_m51FFA22A02EB1066D31E8D28EEFF95FC9F04DCA1,
	Protocol18_GetDictArrayType_m3A2B993C6419F73F254CC0F1A92A84499A759AAA,
	Protocol18_ReadDictionary_mAC05B9475802306F0989B69C341AF958CB76AFDC,
	Protocol18_ReadDictionaryElements_m5B67F3E726564069CCA051EECF2018D476896181,
	Protocol18_ReadObjectArray_m0477F13A24A4FD18348D01A0C3302D2441335682,
	Protocol18_ReadBooleanArray_m7414A38305BB44BDDEA3F1B4E587BA5F80FDE3D6,
	Protocol18_ReadInt16Array_m91994BD3B863E4E400AB79ED4ACE7931EFC8AEF3,
	Protocol18_ReadSingleArray_mE214DDB9B5CCEC270F442032BA93858CD0F96943,
	Protocol18_ReadDoubleArray_mA2017D334A3F7F1877FE3B5DFEC9F81DA612222F,
	Protocol18_ReadStringArray_m7F0869411EBAD2330467E6018F9A951F1D32DAD3,
	Protocol18_ReadHashtableArray_m9497F293B378E3F141116391FD1D84316B9A21F9,
	Protocol18_ReadDictionaryArray_mD15335C559B535EAA923062D61AADD457FEE70D1,
	Protocol18_ReadArrayInArray_m71ED366F6A1B65B704F75B9746C5F17309A7C85D,
	Protocol18_ReadInt1_m323DC131E1491BB77010CB35175D9FC642F1548B,
	Protocol18_ReadInt2_m97FED0CBB704EE232F00F181711F4FAD2A413CB2,
	Protocol18_ReadCompressedInt32_mEDAD5B393FC2EBD104EE119AFA9038CAFBB7007D,
	Protocol18_ReadCompressedUInt32_m703B81CFF05540771F926D377D381DB04B1FBA4D,
	Protocol18_ReadCompressedInt64_m950FC539CCB79F013A2C9032A363E96296D7477E,
	Protocol18_ReadCompressedUInt64_m07B50FA75A9E83203E40818BB1D25958C3C7CD2A,
	Protocol18_ReadCompressedInt32Array_mA61503C39F20DAE88DE8FD10416B74241AD88FF0,
	Protocol18_ReadCompressedInt64Array_m7A08A585BBE5446D45859E5310B92F5EF3AD3F41,
	Protocol18_DecodeZigZag32_m6AEFB426AF36C3CC946F440EE161639FC2D8DF3F,
	Protocol18_DecodeZigZag64_mDE468DA862ABDF892E3752526D689CDC509B9022,
	Protocol18_Write_m28CA5AC55C1BC975A925B143E4D59CB48B825677,
	Protocol18_Write_mB1273F9ADBE7C1F6420BB5E78FE1576932FE0873,
	Protocol18_SerializeEventData_mCD276A62F7A539B1076420125CE44D09798D4161,
	Protocol18_WriteParameterTable_m15339891E0318BA7FFD85BDE67378DBE2A934473,
	Protocol18_SerializeOperationRequest_m6BB4581B489DE8F61EAD5DE8B0CF09CAF89CECCB,
	Protocol18_SerializeOperationRequest_m70A5C045585022B1DCD7D64BE27C92637418A3A4,
	Protocol18_SerializeOperationResponse_m5598289DDB276D5A96E8304C6B0390B151B62013,
	Protocol18_WriteByte_mFE8AA8996495ECA9DC2740387D949450F436A1A4,
	Protocol18_WriteBoolean_m1042C171D0BD9EFD0D6127B111FCD03E9851726A,
	Protocol18_WriteUShort_mE75524C0EC98D116092D1F2A685ABBF87AB48AA8,
	Protocol18_WriteInt16_m5DA8138FD04DC30AB88B07F02D3ED1AA84898AF0,
	Protocol18_WriteDouble_m6C01426AF813AB32C24FF8F443EBFAF9682D2229,
	Protocol18_WriteSingle_mC0A903FD8125905B37D8F845DF792760AC4524DF,
	Protocol18_WriteString_m1B5FB5527A6827BD43C9F62B9BA8AABB47B8E913,
	Protocol18_WriteHashtable_m252B4756606686476FC8C60A3B1A83C9E5B44123,
	Protocol18_WriteByteArray_m92D081FF5DD5BA7CF115AA1368A07D5B9F477A7F,
	Protocol18_WriteByteArraySegment_mAAC60895EFB733F94C6011A1CDEE28D702F2829B,
	Protocol18_WriteInt32ArrayCompressed_m4B56B8D16B02BA551A5BD7D5B4774AF72D8D021E,
	Protocol18_WriteInt64ArrayCompressed_m771B7DECF04B3752318FC0C59D03C302850608CA,
	Protocol18_WriteBoolArray_m4FD033ED63211050C39D6E0E5B11F784AD5CA08F,
	Protocol18_WriteInt16Array_mAAFFC1BE1CD51E0B826BD925C22EE73F45DE85DC,
	Protocol18_WriteSingleArray_m9A1E93C1AE31A7CFB7C3C6F64F882C58CF82780C,
	Protocol18_WriteDoubleArray_mF24BFD14E535751A76DBB750F302F503E6D2A7C9,
	Protocol18_WriteStringArray_m6B2601D69A9E75F79B4E008DB13B29B4EB398C06,
	Protocol18_WriteObjectArray_m87F0813C8F18506083F9529BD7803AFFC672D369,
	Protocol18_WriteArrayInArray_m0832BF6B5DE9D17BC1E579E9DA42518235B26DE4,
	Protocol18_WriteCustomTypeBody_m77F0901379D2EE2DBDF248C06A1D41242A48CA1D,
	Protocol18_WriteCustomType_mD85A387626FF480228157C9C8F2C64E7F1D92B6E,
	Protocol18_WriteCustomTypeArray_m676F40E94CBA53075184ED7CB970031697662113,
	Protocol18_WriteArrayHeader_m776FC13CD7C7BCD2D15060D650F1617F69255C20,
	Protocol18_WriteDictionaryElements_mBBE12B0A16E2524850B58CCC13AE3584B48E1FE0,
	Protocol18_WriteDictionary_mD5C1BCF2C77A8BD1F45C7C97188A9E767C87D87F,
	Protocol18_WriteDictionaryHeader_m3AFD460007174903762B278D74DEA2E4783D1AD8,
	Protocol18_WriteArrayType_mCB7280CA0FD70D50C86F411A751A3B1CE12118F8,
	Protocol18_WriteHashtableArray_mBB95B7F30F51C7F22555C94E84E6CDE0CDC0C3C1,
	Protocol18_WriteDictionaryArray_mD5E129A632A5B746D655EB4F4AB7EE0FB47F7099,
	Protocol18_WriteIntLength_m7DC4CE5B1D2D6615562981C5E97DDE3FCB8717F9,
	Protocol18_WriteCompressedInt32_mD1D3355F57AD9654E6F42810A4F70448F870F547,
	Protocol18_WriteCompressedInt64_m05B8623CED35DC0113CB18E5ED7841B024E44E20,
	Protocol18_WriteCompressedUInt32_m1C3460A5FF6664BD7F575B72E7E9AED19EEB2753,
	Protocol18_WriteCompressedUInt32_mCC3BE83B036C07D21704EEE50DFA8F25FA51E59F,
	Protocol18_WriteCompressedUInt64_mE794942333102405F34828C1AE8E7E786FC3034F,
	Protocol18_EncodeZigZag32_mCA5D8EB726897EC5575ADA856F8F0397CF2E1D4C,
	Protocol18_EncodeZigZag64_m5E852A7CE5B18E969CE4D1B795431F62DBC6FA57,
	Protocol18__ctor_m3890B59E63DA91683F3DD21EF9A00F1A8EA8F51A,
	Protocol18__cctor_mBCCEF9F2E0BE8A0FACFA4585F4FD17A55521075E,
	IPhotonSocket_get_Listener_m81FD492F65B80FED784D951C9D17A809C55B1C10,
	IPhotonSocket_get_MTU_mE95246456D43CF26141C19252FEE5624505FFC9F,
	IPhotonSocket_get_State_mFBEC420EEA20C844A7435873FD0D6FDD6553D9DA,
	IPhotonSocket_set_State_m3A80CC93E8DEC71D42E5C9CFE9129C673D4B3158,
	IPhotonSocket_get_Connected_mE05FEE2E88634230D2BF632BC19A71B2F8A0B11D,
	IPhotonSocket_get_ServerAddress_m38E4E2DCF7BE2EA7EB346D6D02803A251449BBE6,
	IPhotonSocket_set_ServerAddress_mDAE37C57D47165D8E7CA44D1AECD22DAD2E80EDF,
	IPhotonSocket_get_ServerIpAddress_mDA6B85C3AE9384AB2421FD980C6B9E71233505D9,
	IPhotonSocket_set_ServerIpAddress_mE7539725BD69FC0C9277073472941F8D99FFC20B,
	IPhotonSocket_get_ServerPort_m9F0013DBF7F5D00A8111D9382EE9A7CC83949AF4,
	IPhotonSocket_set_ServerPort_mF3E5C7C9FCC51AB8E59653A5FEE400C59D9286F0,
	IPhotonSocket_get_AddressResolvedAsIpv6_m836466CFDC7E21C38D7F275EE372B3475356BF6F,
	IPhotonSocket_set_AddressResolvedAsIpv6_mCD5DE9956B3C74C3832F3D078969FA99E7263587,
	IPhotonSocket_set_UrlProtocol_mD95FF7BEC530105FECD81272480D5D5316F713BF,
	IPhotonSocket_set_UrlPath_mDB678DCCAA3CF16723A3FAF74D6BEB8668597EF3,
	IPhotonSocket__ctor_m5DF31D1BB5054387EEBA22001CFC5001AA28E05A,
	IPhotonSocket_Connect_mB8A977E839E01FECA1A136F9626A872D35557F16,
	NULL,
	NULL,
	IPhotonSocket_HandleReceivedDatagram_m04401580F4FDA75D811B4F57AFAFC73EE0C5D438,
	IPhotonSocket_ReportDebugOfLevel_mB847CD73CDF1609E9C31ACFFD474F67B634E594B,
	IPhotonSocket_EnqueueDebugReturn_m438F9AA21D09AFE51B22A8655DB36CBE0C3B076E,
	IPhotonSocket_HandleException_m2D54CFC4B60DE4C819EE8885E9009F066839C820,
	IPhotonSocket_TryParseAddress_m13AF27BE73438878EE02F240AE274CC5B36F91BD,
	IPhotonSocket_GetIpAddresses_mA02D979F86382FD71052436C48CAAAF192311061,
	IPhotonSocket_AddressSortComparer_mC4206E1F50E250C44CE2FB4F8C7C1BF209DDD2EB,
	IPhotonSocket_U3CHandleExceptionU3Eb__47_0_mFF37A3133D6DEDB980308A3721E0756907DEC7FF,
	U3CU3Ec__cctor_mE2568C71F20DE95122EF4EE39FE35EF89B790E58,
	U3CU3Ec__ctor_m84C52ED064666E73701081A639C32BC74032A759,
	U3CU3Ec_U3CGetIpAddressesU3Eb__50_0_m1C6766A50B28FA17C5779178D0D933D23B24110E,
	SocketUdp__ctor_m6E26694A7CED098C2F314825803A06C270A68EB3,
	SocketUdp_Finalize_mE15AD4AC372E99DF61B75F39FA02581659E2EDE1,
	SocketUdp_Dispose_m9A801E4C0FE2FC7193E49E4A1CF00C5CBA2BFF5C,
	SocketUdp_Connect_mB5CAB1D5221E5931185EB4A11686D9DCF467CA7A,
	SocketUdp_Disconnect_m5F2B92C707159B0CD124E5F697BFC6EFBEE2FDBA,
	SocketUdp_Send_m145DD7477DCD3B862C8EB6053914A5D7F5B2134A,
	SocketUdp_DnsAndConnect_m4D109A93C1EA9B29D15BAC57379F0A68AEA3D2F9,
	SocketUdp_ReceiveLoop_m49A47A4C41D60201CE348266789F7D0DB6CE8B87,
	SocketTcp__ctor_mEBFBD0E3A3C2EF85D78F4D7359070DF6E0DB99A9,
	SocketTcp_Finalize_mE8C1DA41258E85FD2657524114BE42A0523568BB,
	SocketTcp_Dispose_m2BE7A5F8EFE3EE224BFA23A97ECF8B54B101A290,
	SocketTcp_Connect_mEC635608EB8F9C3B208B0EE5D37DCB5FD814A5D3,
	SocketTcp_Disconnect_m8923AF84BA302BA0F1BB17F1B0C6C2C2073D854D,
	SocketTcp_Send_mBEE6F86AC36C82C7AE9DB2C4D2DAD7B288671012,
	SocketTcp_DnsAndConnect_m34281428071478E2B890AAA23AF84087617B8A4D,
	SocketTcp_ReceiveLoop_m94F3091FC347E263BED7D381AD5727E510D406C2,
	SimulationItem__ctor_m6B0ACA82BA28D399FC5493B70241F2CA5AC26611,
	SimulationItem_get_Delay_mCA068BE15D1A7D336BC197F53A64A68FA09CFE8B,
	SimulationItem_set_Delay_m310EF147636DC2628AC6B59391BA2210A69B93E0,
	NetworkSimulationSet_get_IsSimulationEnabled_m69E140B0EE45A07AE9A783649D7FFF58B67EB699,
	NetworkSimulationSet_set_IsSimulationEnabled_mFA99DF613ED88D96301CC8C9B841383B321C406F,
	NetworkSimulationSet_get_OutgoingLag_mF94EBE63A5371EF88EDD33CACC423CF3F3D7ED14,
	NetworkSimulationSet_set_OutgoingLag_mBB0B22C2AD7C94D31F63D44C0BEF1B4F9200CEB3,
	NetworkSimulationSet_get_OutgoingJitter_m1DBD690367CE7E155DDE3FC9AD41D36CE6FFC48E,
	NetworkSimulationSet_set_OutgoingJitter_mACF982FFF9F1EB83F2D9B867127977C6F1C7E18B,
	NetworkSimulationSet_get_OutgoingLossPercentage_m421F0F46CD8F43DD019575DEBBAF94E91A146EA2,
	NetworkSimulationSet_set_OutgoingLossPercentage_mC8F6990B43B189A5C5101C5FB85B5854306F67E5,
	NetworkSimulationSet_get_IncomingLag_mBE5DF4388742F8E62CFAD334EB045DB66D9C2ED4,
	NetworkSimulationSet_set_IncomingLag_m308F42FBFAAE01ACE8B538E834BD496C8DBE1378,
	NetworkSimulationSet_get_IncomingJitter_mD36DE5AF19056CA9E580C2E13548CD51555F2A85,
	NetworkSimulationSet_set_IncomingJitter_m9ACDEE491D5534944BFF1DD05ED93B82DAE633AF,
	NetworkSimulationSet_get_IncomingLossPercentage_mAA923D5758E1058C7FE786F6C8C6E20647570EA7,
	NetworkSimulationSet_set_IncomingLossPercentage_m77013FCEBFB6641E63A02E1E3E8644BC8D11D9C6,
	NetworkSimulationSet_get_LostPackagesOut_m98C39271612038E1DDDCEA0B4B80FE045C87FCF4,
	NetworkSimulationSet_set_LostPackagesOut_m50A7475AF1C054FE66F8E01688175EE150A82040,
	NetworkSimulationSet_get_LostPackagesIn_m2F58DBFC7E1008386EA01F847381D91ADB5C3E0A,
	NetworkSimulationSet_set_LostPackagesIn_m062238219CE1A63FC33A228FA800E41B21AFDC8A,
	NetworkSimulationSet_ToString_m0C7D3E809E45807A59D324FCFA0C9899E550737A,
	NetworkSimulationSet__ctor_m29E97BA7CAC45A955FE4D0EC2A17B2B005325788,
	TrafficStatsGameLevel_get_OperationByteCount_m4AF83FBACDE96B7B1364C033FCB3D4E494AE0FC7,
	TrafficStatsGameLevel_set_OperationByteCount_m927C68A447313CE13AA5D84987CA2287F142CB6F,
	TrafficStatsGameLevel_get_OperationCount_m99206E1FFB8CF775C04FD197C113301FF4F32D2B,
	TrafficStatsGameLevel_set_OperationCount_m36A4955600270A48DB877E0F80C410808A127DC8,
	TrafficStatsGameLevel_get_ResultByteCount_m1B12324F6F9B7E06FC33BAD7CF2BD459E52FD8DB,
	TrafficStatsGameLevel_set_ResultByteCount_mAC03A57F3D1FD4B59FFF9E3D3ADC0E24944FE200,
	TrafficStatsGameLevel_get_ResultCount_m1F09B89A40625665CD25BA3C69E82B3A075327F6,
	TrafficStatsGameLevel_set_ResultCount_mE00C2311493E0E2E16081FE1D77BB5A229F4F29B,
	TrafficStatsGameLevel_get_EventByteCount_m54BD41A76DFA33BFCAA77015ED7C0A6119B526A5,
	TrafficStatsGameLevel_set_EventByteCount_m6498350536AF9E5789F199A7F7A4CB67F5A9C1B5,
	TrafficStatsGameLevel_get_EventCount_mA5E527E99CD36B7AFDC7D85A7767998D0FCA67D3,
	TrafficStatsGameLevel_set_EventCount_mC05D97475B40C59ADC0CF8FDC52106477E661D91,
	TrafficStatsGameLevel_get_LongestOpResponseCallback_m734C3233D3CD5C9566AFEC2E40DAC45B6A2BC44C,
	TrafficStatsGameLevel_set_LongestOpResponseCallback_m9B64B8C65D333FE61983C8E62A6124353A53A666,
	TrafficStatsGameLevel_get_LongestOpResponseCallbackOpCode_mBDEF5EAD7204F1C01ED5A48D17CE5A78CABB9B71,
	TrafficStatsGameLevel_set_LongestOpResponseCallbackOpCode_m01581F59B4C807C6DC2D339184447C583622899E,
	TrafficStatsGameLevel_get_LongestEventCallback_m1A548D3427C18526B150D2863B997E5947C269F3,
	TrafficStatsGameLevel_set_LongestEventCallback_m06F742549E1093BE269291F9889E077F39A887EC,
	TrafficStatsGameLevel_get_LongestMessageCallback_m1DA736B0F2CC25CFE3EEA389558AEF127BE209D5,
	TrafficStatsGameLevel_set_LongestMessageCallback_mD119881F98C7F196ABB5983E09D47C6416DCC67B,
	TrafficStatsGameLevel_get_LongestRawMessageCallback_m68892F488A616E129D7CD00AE7283A05581BBF2D,
	TrafficStatsGameLevel_set_LongestRawMessageCallback_m77E9CFED473CC323B09EE0C139C4E988EAAFE9B9,
	TrafficStatsGameLevel_get_LongestEventCallbackCode_mDFA1AB8C9D5DAEC0D538A9A15AFC3163CB6E1C8A,
	TrafficStatsGameLevel_set_LongestEventCallbackCode_m199E1B2B952DEBE8781382732B4332460AE42BC8,
	TrafficStatsGameLevel_get_LongestDeltaBetweenDispatching_m7D96F3912C4B5C5A0E9F15C87D52DD962885066A,
	TrafficStatsGameLevel_set_LongestDeltaBetweenDispatching_mCAECF6500C7F42CC508185710BC1B79C65A105CC,
	TrafficStatsGameLevel_get_LongestDeltaBetweenSending_m652A61B15DFB86B0C32971F3D01795F7C9BF052E,
	TrafficStatsGameLevel_set_LongestDeltaBetweenSending_mA760542D3F7A40970DEE16FA2170140C8569452C,
	TrafficStatsGameLevel_get_DispatchIncomingCommandsCalls_m548C7E96AD1AE4EF80CF5788EBDC808C908189CF,
	TrafficStatsGameLevel_set_DispatchIncomingCommandsCalls_m2F5C9394445C125DCAAC17CF9F09820CD73963CA,
	TrafficStatsGameLevel_get_SendOutgoingCommandsCalls_m8B1D331C1819EEB5859F895F48F3A1CD873C0CF1,
	TrafficStatsGameLevel_set_SendOutgoingCommandsCalls_mEDC64FA66CB1746E28ACEE399B3BE4366FEC3977,
	TrafficStatsGameLevel_get_TotalMessageCount_m1215B8640C7534BE6E9BE5ABF8A8D1612AEE69DD,
	TrafficStatsGameLevel_get_TotalIncomingMessageCount_mCECA7894C34796872D9F33D79EBEA64B6D1EADC2,
	TrafficStatsGameLevel_get_TotalOutgoingMessageCount_mBB5803D9FFAF7B71EE49BBDEAEC3CB038EAB2121,
	TrafficStatsGameLevel_CountOperation_mB9917247563AB3F6707131C35C4FCD63D0DD18D2,
	TrafficStatsGameLevel_CountResult_m21B61E41067A4078662B77B268A6B7B08DA179E4,
	TrafficStatsGameLevel_CountEvent_m3A9C6A3A2BD4203FE3287D56563338F55A3F5D7C,
	TrafficStatsGameLevel_TimeForResponseCallback_m1422AE3342D5ADA5E73B8D5317F4BD12F433AA07,
	TrafficStatsGameLevel_TimeForEventCallback_m5BE93E4C166364DDDCA5CDEC2FC7FACA4A46B35C,
	TrafficStatsGameLevel_TimeForMessageCallback_mAC6AEF4E9491312C0F3EDDAD6F813DAF19BD6AC2,
	TrafficStatsGameLevel_TimeForRawMessageCallback_mD3401787693A72A683E98589CBBB2FFB6DE0938F,
	TrafficStatsGameLevel_DispatchIncomingCommandsCalled_m28A3F7E4AEAB6B9016D047F760333EADF84E7CF9,
	TrafficStatsGameLevel_SendOutgoingCommandsCalled_mF60ABB96AAEE5DDA0CD28B2A5C9F4B6A7F97491F,
	TrafficStatsGameLevel_ToString_m8B74070B65BDD3A0E8CBB7FA468F32D47406B7FF,
	TrafficStatsGameLevel_ToStringVitalStats_mD5F8AECA8127EC570B86E05F8B17DF61D5E75E15,
	TrafficStatsGameLevel__ctor_mAF40606532076BFD8202354DF47AC23DEE132E4B,
	TrafficStats_get_PackageHeaderSize_m7AF4D435AD51ECDF03344BEC194E554C8F81A3B9,
	TrafficStats_set_PackageHeaderSize_m4A2C48E99681143A74B27213C8E82EEE98BC9879,
	TrafficStats_get_ReliableCommandCount_m495D39C6CBC2F167A80380FBB83CFEB9E07F6677,
	TrafficStats_set_ReliableCommandCount_m36D3639D4E3226D5734E4D0F1144497448303404,
	TrafficStats_get_UnreliableCommandCount_m8860C8A12C8057813B8973D6B7862F1BA26DD1CB,
	TrafficStats_set_UnreliableCommandCount_mC13497B207168DE840AE71851993032B9A90ACA6,
	TrafficStats_get_FragmentCommandCount_m694D6BC4CFCE1DE403229697E21102A3359D1A3F,
	TrafficStats_set_FragmentCommandCount_m28BDC126F7A3A4A4D258D6B84EDDF6BEC364796A,
	TrafficStats_get_ControlCommandCount_m04178269182F266E2AA23C9953DC58B5445C90FF,
	TrafficStats_set_ControlCommandCount_mCC328A8D5867D3F1F6BB8DBA516396155246E40A,
	TrafficStats_get_TotalPacketCount_m7C5BDC0900E1F834C72F2CEA84E7D98B3C5BC518,
	TrafficStats_set_TotalPacketCount_m6BAD3048EB978145025F6715C95065427025544C,
	TrafficStats_get_TotalCommandsInPackets_mF9798CC0CD31D46A4459A32422A374CEFAB4D13E,
	TrafficStats_set_TotalCommandsInPackets_mF95F8732EFD8E04223F10D8D8FA61734177BF810,
	TrafficStats_get_ReliableCommandBytes_m147AC0C2244013C73A1453DF491DBD8C11D81B12,
	TrafficStats_set_ReliableCommandBytes_mA3EA4B791C27A642B30D951DF5428D4CC42DC745,
	TrafficStats_get_UnreliableCommandBytes_mCE8460DBA1FBEF7EC79308384E4CB04EE0B7537D,
	TrafficStats_set_UnreliableCommandBytes_m63A84DFF298BB4187C77E67908DBD15C329177E5,
	TrafficStats_get_FragmentCommandBytes_mD9136721B1962E97701D2212D6D506D76A236441,
	TrafficStats_set_FragmentCommandBytes_mBBA2D4E9D0EE48C56A249E27928D353983D91CCB,
	TrafficStats_get_ControlCommandBytes_m5EBBA04F12C94A6DBE6D0B72017BF1869EC39D41,
	TrafficStats_set_ControlCommandBytes_m356CF57C9033CC747D1D11720EB4FAB20BA604F1,
	TrafficStats__ctor_m18E0B90734A90DE5D0DB5A59789CD2CD1C22C711,
	TrafficStats_get_TotalCommandBytes_m70C8948B443AAA86475BE67453F39EAD8D0695A8,
	TrafficStats_get_TotalPacketBytes_m9EEFBCF2112A49697A369CBFB405C046FBCF2ADA,
	TrafficStats_set_TimestampOfLastAck_m574CA4916E730E3605FAD329118F287E31BDB2A2,
	TrafficStats_set_TimestampOfLastReliableCommand_mEBA5FB895C9F33C8152BA19279BDF72943A457EA,
	TrafficStats_CountControlCommand_mC8EEA269EF85F415A2D0A2CC202CC3A27E77F8A4,
	TrafficStats_CountReliableOpCommand_mA355A2EBC1EF95EDC839ABF0A24E4318434BFD7F,
	TrafficStats_CountUnreliableOpCommand_mD0F52CB2992BAC2F1D4A793DAA4D18D211328190,
	TrafficStats_CountFragmentOpCommand_m381C1C468A75C32220D02FB93D032E73C4EA9F98,
	TrafficStats_ToString_m6031F87D452D9566B4F4D6B564C624CCB6F618A5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EncryptorNet_Init_m3C295187A64A0EEB253A690F63A205348597E627,
	EncryptorNet_Encrypt_m6E9513964EF53DAD3ED2DF23BEBB6589D308512B,
	EncryptorNet_Decrypt_m98021DC7ECBA10A8E67E3489491C3378EB88A5F1,
	EncryptorNet_CreateHMAC_m591927E086329C737EAC035BE538D0991E8B2013,
	EncryptorNet_CheckHMAC_m7E55816A36F39E3FD355279B2063C488ACB58C74,
	EncryptorNet__ctor_m5758AC49519B24D1F87B5A300182EC8B4B091B30,
	EncryptorNative_egconstructEncryptor_mAB53050C9FFFD131D63178C1554C172B12F76AE4,
	EncryptorNative_egdestructEncryptor_m2A7B84C7B83103F3B441C615824E7D74C9F22452,
	EncryptorNative_egencrypt_mFC097F5932C9089F0CF3C9BD59D87B2FB33C55E9,
	EncryptorNative_egdecrypt_m83F62EB73ED50254D89A75A2B1F38466118326EB,
	EncryptorNative_egHMAC_mD44DBC015296FA4DF079D61C6DBBAF9DB9DF307E,
	EncryptorNative_eggetBlockSize_mFE29064BB8B7098AF924832CFF5784A30125C6EC,
	EncryptorNative_eggetIVSize_mF8818B86EC45EC6D5AD745387004EDDF5B4B49B4,
	EncryptorNative_eggetHMACSize_m81EFFD4D5943FC74DECA40489793FFFE6A4080AF,
	EncryptorNative_egsetEncryptorLoggingCallback_mD38FA4A0B05D6A42D02E1BACA2893D8E1FAE6447,
	EncryptorNative_egsetEncryptorLoggingLevel_m8AAE0E53235B5188B96B59416F838824F7FB5899,
	EncryptorNative_Finalize_m303F3461EB9A0F2E7730326C8251D53003AB626D,
	EncryptorNative_OnNativeLog_m023F639F0E7BA64C870B8AE3A107F3CCB6872293,
	EncryptorNative_Init_mB39AEFC6E6DD5A7B6F1EF9BFE16C103ABA3EE94A,
	EncryptorNative_Dispose_mC9AEC7139694C9F5AC301A7E52171E719C7B3369,
	EncryptorNative_Encrypt_mCED29DF26F5FD39D5B55DD75183387F7557A4C40,
	EncryptorNative_CreateHMAC_m2B35323E98B3AC5158623705F9FEED9975137DC9,
	EncryptorNative_Decrypt_m865197312E5C5F5ACB2F592C704E8E0529D23025,
	EncryptorNative_CheckHMAC_mA79C7F22D0D84939D9B1F984EAE5563F3DA2D79A,
	EncryptorNative__ctor_m47A1CC6BFCDC8AEB3D74A1DEE80C8BFAB52C30BE,
	EncryptorNative__cctor_m38BF852AD4B6D9592D7DCCAE7819517911E83E81,
	LogCallbackDelegate__ctor_mDB4F8F225AC10C75C2F7AF70EE155D7AB2BBDCBC,
	LogCallbackDelegate_Invoke_mC2D5A5E34B1C37937D43A79765D9DA03224867AB,
	LogCallbackDelegate_BeginInvoke_mE8E333AA921C84442715FE745CA7E869E13BD2E1,
	LogCallbackDelegate_EndInvoke_m27133481EF73D81AF6AA3A51EACDAB9B6FDEAA56,
};
static const int32_t s_InvokerIndices[718] = 
{
	3,
	14,
	26,
	54,
	54,
	23,
	26,
	14,
	26,
	54,
	54,
	23,
	31,
	14,
	28,
	34,
	3,
	23,
	213,
	26,
	26,
	26,
	946,
	43,
	1,
	1,
	1,
	115,
	114,
	114,
	0,
	121,
	9,
	10,
	121,
	121,
	121,
	1677,
	1677,
	1,
	1,
	14,
	34,
	105,
	218,
	43,
	62,
	10,
	14,
	3,
	3,
	23,
	32,
	28,
	27,
	14,
	14,
	26,
	89,
	23,
	14,
	695,
	23,
	1,
	106,
	241,
	241,
	5,
	49,
	142,
	168,
	0,
	167,
	0,
	43,
	114,
	3,
	131,
	10,
	105,
	112,
	106,
	3,
	23,
	23,
	3,
	23,
	10,
	88,
	26,
	32,
	26,
	32,
	26,
	14,
	14,
	23,
	14,
	694,
	10,
	10,
	32,
	739,
	213,
	32,
	513,
	35,
	89,
	31,
	42,
	30,
	89,
	14,
	10,
	32,
	14,
	26,
	14,
	26,
	89,
	89,
	89,
	31,
	89,
	14,
	89,
	31,
	10,
	10,
	10,
	26,
	10,
	10,
	10,
	10,
	10,
	10,
	14,
	14,
	89,
	89,
	31,
	89,
	31,
	14,
	10,
	89,
	89,
	31,
	14,
	26,
	14,
	26,
	14,
	26,
	186,
	89,
	31,
	23,
	23,
	130,
	14,
	31,
	461,
	90,
	938,
	23,
	23,
	23,
	23,
	89,
	1678,
	26,
	23,
	89,
	89,
	89,
	1679,
	1680,
	3,
	89,
	3,
	14,
	14,
	26,
	14,
	89,
	10,
	10,
	89,
	14,
	10,
	89,
	10,
	89,
	23,
	4,
	168,
	23,
	938,
	28,
	218,
	218,
	23,
	23,
	23,
	23,
	23,
	1681,
	1682,
	89,
	89,
	137,
	89,
	9,
	32,
	9,
	26,
	26,
	26,
	88,
	32,
	14,
	26,
	26,
	23,
	89,
	14,
	14,
	14,
	10,
	23,
	23,
	3,
	131,
	23,
	105,
	26,
	23,
	23,
	23,
	23,
	23,
	206,
	14,
	756,
	14,
	206,
	14,
	1683,
	14,
	31,
	3,
	23,
	23,
	23,
	938,
	23,
	23,
	23,
	23,
	89,
	10,
	10,
	10,
	89,
	89,
	89,
	1681,
	130,
	1684,
	1682,
	10,
	112,
	137,
	137,
	137,
	26,
	26,
	26,
	137,
	137,
	9,
	9,
	785,
	3,
	23,
	833,
	30,
	30,
	34,
	1685,
	139,
	23,
	9,
	10,
	89,
	89,
	1686,
	1687,
	624,
	639,
	14,
	23,
	112,
	14,
	23,
	23,
	938,
	23,
	23,
	23,
	23,
	26,
	89,
	89,
	89,
	1681,
	1682,
	1688,
	23,
	137,
	137,
	26,
	26,
	3,
	43,
	14,
	14,
	161,
	1305,
	161,
	161,
	1686,
	161,
	162,
	230,
	9,
	105,
	28,
	28,
	28,
	28,
	23,
	23,
	130,
	14,
	14,
	23,
	130,
	10,
	14,
	23,
	14,
	23,
	131,
	28,
	218,
	28,
	131,
	1689,
	132,
	230,
	131,
	28,
	218,
	28,
	131,
	1690,
	1691,
	28,
	947,
	1680,
	1250,
	1692,
	1693,
	1131,
	1131,
	1131,
	3,
	14,
	14,
	90,
	162,
	130,
	9,
	1694,
	161,
	1686,
	28,
	161,
	28,
	161,
	105,
	27,
	105,
	161,
	817,
	817,
	1305,
	365,
	1695,
	1696,
	1697,
	161,
	161,
	161,
	1698,
	161,
	161,
	161,
	161,
	27,
	589,
	1699,
	162,
	9,
	9,
	230,
	112,
	231,
	232,
	233,
	28,
	28,
	58,
	58,
	28,
	28,
	28,
	28,
	1700,
	1355,
	23,
	3,
	26,
	14,
	14,
	161,
	1305,
	161,
	162,
	230,
	9,
	828,
	828,
	9,
	30,
	28,
	162,
	9,
	9,
	230,
	230,
	232,
	233,
	28,
	162,
	105,
	105,
	28,
	28,
	28,
	28,
	28,
	1355,
	28,
	28,
	28,
	1701,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	1343,
	1343,
	112,
	112,
	231,
	231,
	28,
	28,
	37,
	996,
	161,
	1699,
	161,
	27,
	161,
	1686,
	161,
	817,
	817,
	962,
	1305,
	1697,
	1696,
	161,
	161,
	161,
	1698,
	161,
	161,
	161,
	161,
	161,
	161,
	161,
	161,
	161,
	211,
	161,
	161,
	90,
	1699,
	161,
	589,
	1298,
	161,
	161,
	137,
	365,
	1695,
	137,
	512,
	185,
	37,
	996,
	23,
	3,
	14,
	10,
	10,
	32,
	89,
	14,
	26,
	4,
	168,
	10,
	32,
	89,
	31,
	26,
	26,
	26,
	89,
	89,
	512,
	365,
	227,
	88,
	32,
	1702,
	28,
	41,
	23,
	3,
	23,
	28,
	26,
	23,
	23,
	89,
	89,
	512,
	23,
	23,
	26,
	23,
	23,
	89,
	89,
	512,
	23,
	23,
	23,
	10,
	32,
	89,
	31,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	89,
	31,
	10,
	32,
	10,
	32,
	10,
	32,
	89,
	31,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	10,
	10,
	32,
	32,
	32,
	833,
	833,
	32,
	32,
	23,
	23,
	14,
	14,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	32,
	10,
	10,
	32,
	32,
	32,
	32,
	32,
	32,
	14,
	211,
	1703,
	1704,
	54,
	146,
	211,
	1703,
	1704,
	54,
	146,
	23,
	1705,
	25,
	1706,
	1707,
	1707,
	106,
	106,
	106,
	1266,
	46,
	23,
	1708,
	211,
	31,
	1703,
	54,
	1704,
	146,
	23,
	3,
	131,
	1709,
	1710,
	26,
};
extern const Il2CppCodeGenModule g_Photon3Unity3DCodeGenModule;
const Il2CppCodeGenModule g_Photon3Unity3DCodeGenModule = 
{
	"Photon3Unity3D.dll",
	718,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
