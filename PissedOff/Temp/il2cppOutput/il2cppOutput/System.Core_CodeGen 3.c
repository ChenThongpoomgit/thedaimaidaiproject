﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B ();
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 ();
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 ();
// 0x00000004 System.Void System.Security.Cryptography.AesManaged::set_FeedbackSize(System.Int32)
extern void AesManaged_set_FeedbackSize_m881146DEC23D6F9FA3480D2ABDA7CBAFBD71C7C9 ();
// 0x00000005 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E ();
// 0x00000006 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 ();
// 0x00000007 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 ();
// 0x00000008 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC ();
// 0x00000009 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 ();
// 0x0000000A System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 ();
// 0x0000000B System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA ();
// 0x0000000C System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 ();
// 0x0000000D System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 ();
// 0x0000000E System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 ();
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD ();
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 ();
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 ();
// 0x00000012 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 ();
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED ();
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC ();
// 0x00000015 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 ();
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E ();
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC ();
// 0x00000018 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 ();
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 ();
// 0x0000001A System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 ();
// 0x0000001B System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F ();
// 0x0000001C System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C ();
// 0x0000001D System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A ();
// 0x0000001E System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 ();
// 0x0000001F System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA ();
// 0x00000020 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B ();
// 0x00000021 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F ();
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_FeedbackSize(System.Int32)
extern void AesCryptoServiceProvider_set_FeedbackSize_m409990EF50B03E207F0BAAE9BE19C23A77EA9C27 ();
// 0x00000023 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F ();
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA ();
// 0x00000025 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 ();
// 0x00000026 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 ();
// 0x00000027 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 ();
// 0x00000028 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA ();
// 0x00000029 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F ();
// 0x0000002A System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD ();
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA ();
// 0x0000002C System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 ();
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA ();
// 0x0000002E System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 ();
// 0x0000002F System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 ();
// 0x00000030 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000031 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000032 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000033 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000034 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000035 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000036 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000037 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000038 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000039 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003A System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003B TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003C TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000003D System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003E System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000003F System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000040 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000041 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000042 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000043 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000044 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000045 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000046 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000047 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000048 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000049 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000004A System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000004B System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000004C System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000004E System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000004F System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000050 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000051 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000052 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000053 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000054 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000055 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000056 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000057 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000058 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000059 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000005A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000005C System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000005D System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000005E System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000005F System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000060 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000061 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000062 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000063 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000064 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000065 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000066 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000067 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000068 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000069 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x0000006A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000006B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000006C System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000006D System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000006E System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000006F TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000070 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000071 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000072 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000073 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000074 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000075 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000076 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x00000077 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000078 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000079 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x0000007A System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000007B System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000007C System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x0000007D System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x0000007E System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000007F System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000080 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000081 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000082 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000083 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000084 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000085 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000086 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000087 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000088 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000089 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000008A System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000008B System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000008C System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000008D System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000008E System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000008F System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000090 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000091 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000092 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000093 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000094 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000095 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000096 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000097 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000098 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000099 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000009A System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000009B System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000009C System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000009D System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000009E System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x0000009F System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000A0 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000A1 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000A2 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[162] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_set_FeedbackSize_m881146DEC23D6F9FA3480D2ABDA7CBAFBD71C7C9,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_set_FeedbackSize_m409990EF50B03E207F0BAAE9BE19C23A77EA9C27,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[162] = 
{
	0,
	23,
	10,
	32,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	105,
	14,
	105,
	31,
	23,
	23,
	23,
	23,
	23,
	105,
	105,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	947,
	27,
	37,
	211,
	211,
	3,
	0,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[38] = 
{
	{ 0x02000008, { 61, 4 } },
	{ 0x02000009, { 65, 9 } },
	{ 0x0200000A, { 76, 7 } },
	{ 0x0200000B, { 85, 10 } },
	{ 0x0200000C, { 97, 11 } },
	{ 0x0200000D, { 111, 9 } },
	{ 0x0200000E, { 123, 12 } },
	{ 0x0200000F, { 138, 1 } },
	{ 0x02000010, { 139, 2 } },
	{ 0x02000012, { 141, 3 } },
	{ 0x02000013, { 144, 5 } },
	{ 0x02000014, { 149, 7 } },
	{ 0x02000015, { 156, 3 } },
	{ 0x02000016, { 159, 7 } },
	{ 0x02000017, { 166, 4 } },
	{ 0x02000018, { 170, 21 } },
	{ 0x0200001A, { 191, 2 } },
	{ 0x06000032, { 0, 10 } },
	{ 0x06000033, { 10, 10 } },
	{ 0x06000034, { 20, 5 } },
	{ 0x06000035, { 25, 5 } },
	{ 0x06000036, { 30, 2 } },
	{ 0x06000037, { 32, 1 } },
	{ 0x06000038, { 33, 5 } },
	{ 0x06000039, { 38, 3 } },
	{ 0x0600003A, { 41, 2 } },
	{ 0x0600003B, { 43, 4 } },
	{ 0x0600003C, { 47, 3 } },
	{ 0x0600003D, { 50, 1 } },
	{ 0x0600003E, { 51, 3 } },
	{ 0x0600003F, { 54, 2 } },
	{ 0x06000040, { 56, 5 } },
	{ 0x06000050, { 74, 2 } },
	{ 0x06000055, { 83, 2 } },
	{ 0x0600005A, { 95, 2 } },
	{ 0x06000060, { 108, 3 } },
	{ 0x06000065, { 120, 3 } },
	{ 0x0600006A, { 135, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[193] = 
{
	{ (Il2CppRGCTXDataType)2, 22294 },
	{ (Il2CppRGCTXDataType)3, 15289 },
	{ (Il2CppRGCTXDataType)2, 22295 },
	{ (Il2CppRGCTXDataType)2, 22296 },
	{ (Il2CppRGCTXDataType)3, 15290 },
	{ (Il2CppRGCTXDataType)2, 22297 },
	{ (Il2CppRGCTXDataType)2, 22298 },
	{ (Il2CppRGCTXDataType)3, 15291 },
	{ (Il2CppRGCTXDataType)2, 22299 },
	{ (Il2CppRGCTXDataType)3, 15292 },
	{ (Il2CppRGCTXDataType)2, 22300 },
	{ (Il2CppRGCTXDataType)3, 15293 },
	{ (Il2CppRGCTXDataType)2, 22301 },
	{ (Il2CppRGCTXDataType)2, 22302 },
	{ (Il2CppRGCTXDataType)3, 15294 },
	{ (Il2CppRGCTXDataType)2, 22303 },
	{ (Il2CppRGCTXDataType)2, 22304 },
	{ (Il2CppRGCTXDataType)3, 15295 },
	{ (Il2CppRGCTXDataType)2, 22305 },
	{ (Il2CppRGCTXDataType)3, 15296 },
	{ (Il2CppRGCTXDataType)2, 22306 },
	{ (Il2CppRGCTXDataType)3, 15297 },
	{ (Il2CppRGCTXDataType)3, 15298 },
	{ (Il2CppRGCTXDataType)2, 17459 },
	{ (Il2CppRGCTXDataType)3, 15299 },
	{ (Il2CppRGCTXDataType)2, 22307 },
	{ (Il2CppRGCTXDataType)3, 15300 },
	{ (Il2CppRGCTXDataType)3, 15301 },
	{ (Il2CppRGCTXDataType)2, 17466 },
	{ (Il2CppRGCTXDataType)3, 15302 },
	{ (Il2CppRGCTXDataType)2, 22308 },
	{ (Il2CppRGCTXDataType)3, 15303 },
	{ (Il2CppRGCTXDataType)3, 15304 },
	{ (Il2CppRGCTXDataType)3, 15305 },
	{ (Il2CppRGCTXDataType)2, 22309 },
	{ (Il2CppRGCTXDataType)2, 17474 },
	{ (Il2CppRGCTXDataType)2, 22310 },
	{ (Il2CppRGCTXDataType)2, 17476 },
	{ (Il2CppRGCTXDataType)2, 22311 },
	{ (Il2CppRGCTXDataType)3, 15306 },
	{ (Il2CppRGCTXDataType)3, 15307 },
	{ (Il2CppRGCTXDataType)2, 17482 },
	{ (Il2CppRGCTXDataType)3, 15308 },
	{ (Il2CppRGCTXDataType)2, 22312 },
	{ (Il2CppRGCTXDataType)2, 22313 },
	{ (Il2CppRGCTXDataType)2, 17483 },
	{ (Il2CppRGCTXDataType)2, 22314 },
	{ (Il2CppRGCTXDataType)2, 17485 },
	{ (Il2CppRGCTXDataType)2, 22315 },
	{ (Il2CppRGCTXDataType)3, 15309 },
	{ (Il2CppRGCTXDataType)2, 17488 },
	{ (Il2CppRGCTXDataType)2, 17490 },
	{ (Il2CppRGCTXDataType)2, 22316 },
	{ (Il2CppRGCTXDataType)3, 15310 },
	{ (Il2CppRGCTXDataType)2, 22317 },
	{ (Il2CppRGCTXDataType)3, 15311 },
	{ (Il2CppRGCTXDataType)3, 15312 },
	{ (Il2CppRGCTXDataType)2, 22318 },
	{ (Il2CppRGCTXDataType)2, 17495 },
	{ (Il2CppRGCTXDataType)2, 22319 },
	{ (Il2CppRGCTXDataType)2, 17497 },
	{ (Il2CppRGCTXDataType)3, 15313 },
	{ (Il2CppRGCTXDataType)3, 15314 },
	{ (Il2CppRGCTXDataType)2, 17500 },
	{ (Il2CppRGCTXDataType)3, 15315 },
	{ (Il2CppRGCTXDataType)3, 15316 },
	{ (Il2CppRGCTXDataType)2, 17512 },
	{ (Il2CppRGCTXDataType)2, 22320 },
	{ (Il2CppRGCTXDataType)3, 15317 },
	{ (Il2CppRGCTXDataType)3, 15318 },
	{ (Il2CppRGCTXDataType)2, 17514 },
	{ (Il2CppRGCTXDataType)2, 22172 },
	{ (Il2CppRGCTXDataType)3, 15319 },
	{ (Il2CppRGCTXDataType)3, 15320 },
	{ (Il2CppRGCTXDataType)2, 22321 },
	{ (Il2CppRGCTXDataType)3, 15321 },
	{ (Il2CppRGCTXDataType)3, 15322 },
	{ (Il2CppRGCTXDataType)2, 17524 },
	{ (Il2CppRGCTXDataType)2, 22322 },
	{ (Il2CppRGCTXDataType)3, 15323 },
	{ (Il2CppRGCTXDataType)3, 15324 },
	{ (Il2CppRGCTXDataType)3, 14557 },
	{ (Il2CppRGCTXDataType)3, 15325 },
	{ (Il2CppRGCTXDataType)2, 22323 },
	{ (Il2CppRGCTXDataType)3, 15326 },
	{ (Il2CppRGCTXDataType)3, 15327 },
	{ (Il2CppRGCTXDataType)2, 17536 },
	{ (Il2CppRGCTXDataType)2, 22324 },
	{ (Il2CppRGCTXDataType)3, 15328 },
	{ (Il2CppRGCTXDataType)3, 15329 },
	{ (Il2CppRGCTXDataType)3, 15330 },
	{ (Il2CppRGCTXDataType)3, 15331 },
	{ (Il2CppRGCTXDataType)3, 15332 },
	{ (Il2CppRGCTXDataType)3, 14563 },
	{ (Il2CppRGCTXDataType)3, 15333 },
	{ (Il2CppRGCTXDataType)2, 22325 },
	{ (Il2CppRGCTXDataType)3, 15334 },
	{ (Il2CppRGCTXDataType)3, 15335 },
	{ (Il2CppRGCTXDataType)2, 17549 },
	{ (Il2CppRGCTXDataType)2, 22326 },
	{ (Il2CppRGCTXDataType)3, 15336 },
	{ (Il2CppRGCTXDataType)3, 15337 },
	{ (Il2CppRGCTXDataType)2, 17551 },
	{ (Il2CppRGCTXDataType)2, 22327 },
	{ (Il2CppRGCTXDataType)3, 15338 },
	{ (Il2CppRGCTXDataType)3, 15339 },
	{ (Il2CppRGCTXDataType)2, 22328 },
	{ (Il2CppRGCTXDataType)3, 15340 },
	{ (Il2CppRGCTXDataType)3, 15341 },
	{ (Il2CppRGCTXDataType)2, 22329 },
	{ (Il2CppRGCTXDataType)3, 15342 },
	{ (Il2CppRGCTXDataType)3, 15343 },
	{ (Il2CppRGCTXDataType)2, 17566 },
	{ (Il2CppRGCTXDataType)2, 22330 },
	{ (Il2CppRGCTXDataType)3, 15344 },
	{ (Il2CppRGCTXDataType)3, 15345 },
	{ (Il2CppRGCTXDataType)3, 15346 },
	{ (Il2CppRGCTXDataType)3, 14574 },
	{ (Il2CppRGCTXDataType)2, 22331 },
	{ (Il2CppRGCTXDataType)3, 15347 },
	{ (Il2CppRGCTXDataType)3, 15348 },
	{ (Il2CppRGCTXDataType)2, 22332 },
	{ (Il2CppRGCTXDataType)3, 15349 },
	{ (Il2CppRGCTXDataType)3, 15350 },
	{ (Il2CppRGCTXDataType)2, 17582 },
	{ (Il2CppRGCTXDataType)2, 22333 },
	{ (Il2CppRGCTXDataType)3, 15351 },
	{ (Il2CppRGCTXDataType)3, 15352 },
	{ (Il2CppRGCTXDataType)3, 15353 },
	{ (Il2CppRGCTXDataType)3, 15354 },
	{ (Il2CppRGCTXDataType)3, 15355 },
	{ (Il2CppRGCTXDataType)3, 15356 },
	{ (Il2CppRGCTXDataType)3, 14580 },
	{ (Il2CppRGCTXDataType)2, 22334 },
	{ (Il2CppRGCTXDataType)3, 15357 },
	{ (Il2CppRGCTXDataType)3, 15358 },
	{ (Il2CppRGCTXDataType)2, 22335 },
	{ (Il2CppRGCTXDataType)3, 15359 },
	{ (Il2CppRGCTXDataType)3, 15360 },
	{ (Il2CppRGCTXDataType)3, 15361 },
	{ (Il2CppRGCTXDataType)3, 15362 },
	{ (Il2CppRGCTXDataType)2, 22336 },
	{ (Il2CppRGCTXDataType)3, 15363 },
	{ (Il2CppRGCTXDataType)3, 15364 },
	{ (Il2CppRGCTXDataType)2, 22337 },
	{ (Il2CppRGCTXDataType)3, 15365 },
	{ (Il2CppRGCTXDataType)3, 15366 },
	{ (Il2CppRGCTXDataType)3, 15367 },
	{ (Il2CppRGCTXDataType)2, 17620 },
	{ (Il2CppRGCTXDataType)3, 15368 },
	{ (Il2CppRGCTXDataType)2, 17629 },
	{ (Il2CppRGCTXDataType)3, 15369 },
	{ (Il2CppRGCTXDataType)2, 22338 },
	{ (Il2CppRGCTXDataType)2, 22339 },
	{ (Il2CppRGCTXDataType)3, 15370 },
	{ (Il2CppRGCTXDataType)3, 15371 },
	{ (Il2CppRGCTXDataType)3, 15372 },
	{ (Il2CppRGCTXDataType)3, 15373 },
	{ (Il2CppRGCTXDataType)3, 15374 },
	{ (Il2CppRGCTXDataType)3, 15375 },
	{ (Il2CppRGCTXDataType)2, 17645 },
	{ (Il2CppRGCTXDataType)2, 22340 },
	{ (Il2CppRGCTXDataType)3, 15376 },
	{ (Il2CppRGCTXDataType)3, 15377 },
	{ (Il2CppRGCTXDataType)2, 17649 },
	{ (Il2CppRGCTXDataType)3, 15378 },
	{ (Il2CppRGCTXDataType)2, 22341 },
	{ (Il2CppRGCTXDataType)2, 17659 },
	{ (Il2CppRGCTXDataType)2, 17657 },
	{ (Il2CppRGCTXDataType)2, 22342 },
	{ (Il2CppRGCTXDataType)3, 15379 },
	{ (Il2CppRGCTXDataType)2, 22343 },
	{ (Il2CppRGCTXDataType)3, 15380 },
	{ (Il2CppRGCTXDataType)3, 15381 },
	{ (Il2CppRGCTXDataType)3, 15382 },
	{ (Il2CppRGCTXDataType)2, 17663 },
	{ (Il2CppRGCTXDataType)3, 15383 },
	{ (Il2CppRGCTXDataType)3, 15384 },
	{ (Il2CppRGCTXDataType)2, 17666 },
	{ (Il2CppRGCTXDataType)3, 15385 },
	{ (Il2CppRGCTXDataType)1, 22344 },
	{ (Il2CppRGCTXDataType)2, 17665 },
	{ (Il2CppRGCTXDataType)3, 15386 },
	{ (Il2CppRGCTXDataType)1, 17665 },
	{ (Il2CppRGCTXDataType)1, 17663 },
	{ (Il2CppRGCTXDataType)2, 22345 },
	{ (Il2CppRGCTXDataType)2, 17665 },
	{ (Il2CppRGCTXDataType)3, 15387 },
	{ (Il2CppRGCTXDataType)3, 15388 },
	{ (Il2CppRGCTXDataType)3, 15389 },
	{ (Il2CppRGCTXDataType)2, 17664 },
	{ (Il2CppRGCTXDataType)3, 15390 },
	{ (Il2CppRGCTXDataType)2, 17677 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	162,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	38,
	s_rgctxIndices,
	193,
	s_rgctxValues,
	NULL,
};
