﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void AcquireChanController::Awake()
extern void AcquireChanController_Awake_mC2CFB4AD467DFEF49079BD2E81145B9A85057A96 ();
// 0x00000002 System.Void AcquireChanController::Update()
extern void AcquireChanController_Update_m274A4CD4C3653DB63418CFD9E38E2C93B4394815 ();
// 0x00000003 System.Void AcquireChanController::.ctor()
extern void AcquireChanController__ctor_m87D8CAD8778B02874F5C1A604B8992E6A0D29BB3 ();
// 0x00000004 System.Void MotionChanger::Start()
extern void MotionChanger_Start_m236F623AD6DB7802B2222EB3C1DD4195DDA992A8 ();
// 0x00000005 System.Void MotionChanger::Update()
extern void MotionChanger_Update_m2E62977E5D90378AA7FE538888F2897F56B4CA18 ();
// 0x00000006 System.Void MotionChanger::NextAnimation()
extern void MotionChanger_NextAnimation_m5B375F97BB6C33F79EF27FE647265DBB2D150553 ();
// 0x00000007 System.Void MotionChanger::PrevAnimation()
extern void MotionChanger_PrevAnimation_m7CEA9488097CAE17F5F400E62095C34C860B2811 ();
// 0x00000008 System.Void MotionChanger::OnGUI()
extern void MotionChanger_OnGUI_m73EA75D5B0245C91239C07521F211D03A0D4D223 ();
// 0x00000009 System.Void MotionChanger::OnTriggerEnter(UnityEngine.Collider)
extern void MotionChanger_OnTriggerEnter_mD3FDA1C97D397E7802914132FDC199966F60B62D ();
// 0x0000000A System.Void MotionChanger::TakeDamage(System.Int32)
extern void MotionChanger_TakeDamage_mB8E8D1E8B4D979484B31135FD177695BF2D23714 ();
// 0x0000000B System.Collections.IEnumerator MotionChanger::waitSec(UnityEngine.GameObject)
extern void MotionChanger_waitSec_mD2873F5D9DF2108D71C84C9580776D2816672E75 ();
// 0x0000000C System.Void MotionChanger::regenerate()
extern void MotionChanger_regenerate_m6E0FFBED4C8EE1E4326D645378986C615820C557 ();
// 0x0000000D System.Void MotionChanger::UpdateChat(System.String)
extern void MotionChanger_UpdateChat_m707796E1AA7279C8AF8630C52EA65AA0BEC01255 ();
// 0x0000000E System.Collections.IEnumerator MotionChanger::WaitDelay(System.String)
extern void MotionChanger_WaitDelay_mC56B2F40698751795346DEB88242135E2A7026AA ();
// 0x0000000F System.Void MotionChanger::.ctor()
extern void MotionChanger__ctor_m41C5694AAD9DCC8E3E636152A7BF9BFC045678CA ();
// 0x00000010 System.Void SimpleFollowCam::FixedUpdate()
extern void SimpleFollowCam_FixedUpdate_m89F6F4B0134F40CE83C66FD40BF0EA729A802383 ();
// 0x00000011 System.Void SimpleFollowCam::.ctor()
extern void SimpleFollowCam__ctor_mEBB42530BFD0D019F1DAA8974235FC32FF4CFF9C ();
// 0x00000012 System.Void ChannelSelector::SetChannel(System.String)
extern void ChannelSelector_SetChannel_m42445342022E7FFA42EC3F5A15D2E5F1230F20EF ();
// 0x00000013 System.Void ChannelSelector::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void ChannelSelector_OnPointerClick_mEC70BF8011630441106C9D276F68567679D08F2B ();
// 0x00000014 System.Void ChannelSelector::.ctor()
extern void ChannelSelector__ctor_m1A9CA3EDE510F2C0936E460355944CF963C53AF8 ();
// 0x00000015 System.Void ChatAppIdCheckerUI::Update()
extern void ChatAppIdCheckerUI_Update_m1AC6AE7BE4D369EE3FA8B071E97CDD8978434351 ();
// 0x00000016 System.Void ChatAppIdCheckerUI::.ctor()
extern void ChatAppIdCheckerUI__ctor_m42CD82562A49DD287217CE2FDEB1FFCA8DCCD976 ();
// 0x00000017 System.String ChatGui::get_UserName()
extern void ChatGui_get_UserName_m2D7189147C1B72BD1BF3B240234E3C8FADA4CEBE ();
// 0x00000018 System.Void ChatGui::set_UserName(System.String)
extern void ChatGui_set_UserName_mB775E61D7B0AE9547A9FC4986DF5C0829051BD66 ();
// 0x00000019 System.Void ChatGui::Start()
extern void ChatGui_Start_m9E8D8C2BC4462E72E991CD45E8E3CC8A2B13DA11 ();
// 0x0000001A System.Void ChatGui::Connect()
extern void ChatGui_Connect_m0F2777BC430257B61AEC2E0195EB8833B2A823A9 ();
// 0x0000001B System.Void ChatGui::OnDestroy()
extern void ChatGui_OnDestroy_mA878BA277C56EFD51ED99E0BA7E3C27E4217A599 ();
// 0x0000001C System.Void ChatGui::OnApplicationQuit()
extern void ChatGui_OnApplicationQuit_m2E53294EA4F5AFAEC09012D90D35C9669869CB69 ();
// 0x0000001D System.Void ChatGui::Update()
extern void ChatGui_Update_m117EE1AE4617CB186701801E27A81C4F19C66A0C ();
// 0x0000001E System.Void ChatGui::OnEnterSend()
extern void ChatGui_OnEnterSend_m0203888ED9A4F79D7D4BE0C65FCEBFD856D02E1A ();
// 0x0000001F System.Void ChatGui::OnClickSend()
extern void ChatGui_OnClickSend_mCA632A41AAB1AC6138100466C4BA6D21DB79A3BD ();
// 0x00000020 System.Void ChatGui::SendChatMessage(System.String)
extern void ChatGui_SendChatMessage_m2935E03C2E3E895FF39B820DBF9EBB68ED82109C ();
// 0x00000021 System.Void ChatGui::PostHelpToCurrentChannel()
extern void ChatGui_PostHelpToCurrentChannel_mFC53973D9F29FF1EF925EC2688AE4480C15BCBAB ();
// 0x00000022 System.Void ChatGui::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern void ChatGui_DebugReturn_m2EB4F0C1B1259132A79AB1588A448C6E4A23646A ();
// 0x00000023 System.Void ChatGui::OnConnected()
extern void ChatGui_OnConnected_mC0826EAB343F4A3DD575EBF26B53B085457316DF ();
// 0x00000024 System.Void ChatGui::OnDisconnected()
extern void ChatGui_OnDisconnected_m76182F0ED767653F7FFAD61B815C632A78D0F0AC ();
// 0x00000025 System.Void ChatGui::OnChatStateChange(Photon.Chat.ChatState)
extern void ChatGui_OnChatStateChange_m83E3976649121248276C423B82287AF3393E1FA0 ();
// 0x00000026 System.Void ChatGui::OnSubscribed(System.String[],System.Boolean[])
extern void ChatGui_OnSubscribed_mC4CE0E33989BAD899756434A8D9674542EEE7787 ();
// 0x00000027 System.Void ChatGui::InstantiateChannelButton(System.String)
extern void ChatGui_InstantiateChannelButton_mAC43B7D5ABBAC119A7B0E79DB7D327EAC0EB61DA ();
// 0x00000028 System.Void ChatGui::InstantiateFriendButton(System.String)
extern void ChatGui_InstantiateFriendButton_m770E010399E1B8C7143BA4E9DD209D64E48958DA ();
// 0x00000029 System.Void ChatGui::OnUnsubscribed(System.String[])
extern void ChatGui_OnUnsubscribed_m7B92447BA0144BA8BAD69F372CCA49FCD7DDBE24 ();
// 0x0000002A System.Void ChatGui::OnGetMessages(System.String,System.String[],System.Object[])
extern void ChatGui_OnGetMessages_m41520F9090CAC47E8D28FBF082E4092B2EFE9371 ();
// 0x0000002B System.Void ChatGui::OnPrivateMessage(System.String,System.Object,System.String)
extern void ChatGui_OnPrivateMessage_m020F729CA33DFE2F4CBD08B3A693FA61D080573A ();
// 0x0000002C System.Void ChatGui::OnStatusUpdate(System.String,System.Int32,System.Boolean,System.Object)
extern void ChatGui_OnStatusUpdate_m0318643027388B3911C050A32E3910B1C2784D83 ();
// 0x0000002D System.Void ChatGui::OnUserSubscribed(System.String,System.String)
extern void ChatGui_OnUserSubscribed_m429025D0F91C78B629AB5CC8F40C86FF80E9BAB0 ();
// 0x0000002E System.Void ChatGui::OnUserUnsubscribed(System.String,System.String)
extern void ChatGui_OnUserUnsubscribed_mF3C325C964655102B5B3D3FC4ABFB54569B889B7 ();
// 0x0000002F System.Void ChatGui::AddMessageToSelectedChannel(System.String)
extern void ChatGui_AddMessageToSelectedChannel_mB25598D1E6E34A2C82ABF808FD2C19D41CC88131 ();
// 0x00000030 System.Void ChatGui::ShowChannel(System.String)
extern void ChatGui_ShowChannel_mA1621F270A0B212DF44223B183E9695FA5FD69B0 ();
// 0x00000031 System.Void ChatGui::OpenDashboard()
extern void ChatGui_OpenDashboard_m8FB1CA73CF3C535A743F1C1523DFD1484E819D0B ();
// 0x00000032 System.Void ChatGui::.ctor()
extern void ChatGui__ctor_mA4A951576B88ECBDB754514AC4114E60386608B5 ();
// 0x00000033 System.Void ChatGui::.cctor()
extern void ChatGui__cctor_m72B9DCD148155721FBF7C8FBE0F21AEDEA0CD15C ();
// 0x00000034 System.Void FriendItem::set_FriendId(System.String)
extern void FriendItem_set_FriendId_mCAE0C9115FBDA3AD25FDC4279D674B1498ECBCA2 ();
// 0x00000035 System.String FriendItem::get_FriendId()
extern void FriendItem_get_FriendId_m3FB64D35B15F1E85065C0AB278543C2F0913F1F1 ();
// 0x00000036 System.Void FriendItem::Awake()
extern void FriendItem_Awake_mDA0F0972D787D79FE657F35F829796442DF5FE2E ();
// 0x00000037 System.Void FriendItem::OnFriendStatusUpdate(System.Int32,System.Boolean,System.Object)
extern void FriendItem_OnFriendStatusUpdate_m92B9750B90F833B15663B4127531C1D2EE5252AD ();
// 0x00000038 System.Void FriendItem::.ctor()
extern void FriendItem__ctor_m77402589F7CB63B107066CD7B2F2BC650ABAEF95 ();
// 0x00000039 System.Boolean IgnoreUiRaycastWhenInactive::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern void IgnoreUiRaycastWhenInactive_IsRaycastLocationValid_mB04A97B0C6B2F741AA1775807054F3F147BDAA31 ();
// 0x0000003A System.Void IgnoreUiRaycastWhenInactive::.ctor()
extern void IgnoreUiRaycastWhenInactive__ctor_m573C1522D6C18004507A53DB8DADE79F19E3BB9F ();
// 0x0000003B System.Void NamePickGui::Start()
extern void NamePickGui_Start_mB0BBD21B5440F12C73AB5B7250640910DD92B25B ();
// 0x0000003C System.Void NamePickGui::EndEditOnEnter()
extern void NamePickGui_EndEditOnEnter_m4D90D94D1B3BB2085E74AB280CE788148694BB5E ();
// 0x0000003D System.Void NamePickGui::StartChat()
extern void NamePickGui_StartChat_m0F97F13D467453D7F2667E889F64136E5FA101DC ();
// 0x0000003E System.Void NamePickGui::.ctor()
extern void NamePickGui__ctor_mE766F508304EC60C4BE09549C401A0F050C5C3D8 ();
// 0x0000003F System.Void DelayStartLobbyController::OnConnectedToMaster()
extern void DelayStartLobbyController_OnConnectedToMaster_m067E38EDD94DA292E40CAFEF6D7733851A05309A ();
// 0x00000040 System.Void DelayStartLobbyController::DelayStart()
extern void DelayStartLobbyController_DelayStart_m4A9F853EE29241EF04B8005C726E4967AF0B624F ();
// 0x00000041 System.Void DelayStartLobbyController::OnJoinRandomFailed(System.Int16,System.String)
extern void DelayStartLobbyController_OnJoinRandomFailed_m15FD332E50F8FA939A8ED18C60370AAE49B3FF36 ();
// 0x00000042 System.Void DelayStartLobbyController::CreateRoom()
extern void DelayStartLobbyController_CreateRoom_m5C3AB00138E2ECE922BBF35271651F4C8D84E2A3 ();
// 0x00000043 System.Void DelayStartLobbyController::OnCreateRoomFailed(System.Int16,System.String)
extern void DelayStartLobbyController_OnCreateRoomFailed_m31E0373A8E11957AF61DD935BC94C1D5E4F26F53 ();
// 0x00000044 System.Void DelayStartLobbyController::DelayCancel()
extern void DelayStartLobbyController_DelayCancel_m23313E4BC00FC534732AB4D02524A7CA43664200 ();
// 0x00000045 System.Void DelayStartLobbyController::.ctor()
extern void DelayStartLobbyController__ctor_m2FC5A5E9DD2FD84A6B2D2724BE9BCC764BE04E16 ();
// 0x00000046 System.Void DelayStartRoomCOntroller::OnEnable()
extern void DelayStartRoomCOntroller_OnEnable_m4A3B04BFEC1A1044781AA6370ED261EF35F5DB92 ();
// 0x00000047 System.Void DelayStartRoomCOntroller::OnDisable()
extern void DelayStartRoomCOntroller_OnDisable_m32AE09C36DAEB8D7028BDF94164B05C384B3C642 ();
// 0x00000048 System.Void DelayStartRoomCOntroller::OnJoinedRoom()
extern void DelayStartRoomCOntroller_OnJoinedRoom_m7BCFBB1BE560F2FD8C4F006C58CA0D1A4B862277 ();
// 0x00000049 System.Void DelayStartRoomCOntroller::.ctor()
extern void DelayStartRoomCOntroller__ctor_m57B2535BA034DB930EAD2503E64367B1BDD7BF46 ();
// 0x0000004A System.Void DelayStartWaitingRoomController::Start()
extern void DelayStartWaitingRoomController_Start_mAC221BFE52BE2A6A2106AD402A6E9F7131437B36 ();
// 0x0000004B System.Void DelayStartWaitingRoomController::PlayerCountUpdate()
extern void DelayStartWaitingRoomController_PlayerCountUpdate_m81E7DC8387A23E0751ECECB45FC61610EBAD1B31 ();
// 0x0000004C System.Void DelayStartWaitingRoomController::OnPlayerEnteredRoom(Photon.Realtime.Player)
extern void DelayStartWaitingRoomController_OnPlayerEnteredRoom_mF55B786CF5987FE7B5DC9053D0F14E02B4ED4348 ();
// 0x0000004D System.Void DelayStartWaitingRoomController::RPC_SendTimer(System.Single)
extern void DelayStartWaitingRoomController_RPC_SendTimer_m4A6F84E41DD8854FDEFA3AF8FA593ADEE088ACB0 ();
// 0x0000004E System.Void DelayStartWaitingRoomController::OnPlayerLeftRoom(Photon.Realtime.Player)
extern void DelayStartWaitingRoomController_OnPlayerLeftRoom_m32F63AE7E1D8AB0BE6EAB25E87228C1B04CC5C0E ();
// 0x0000004F System.Void DelayStartWaitingRoomController::Update()
extern void DelayStartWaitingRoomController_Update_mD41BB300CB2F90257C70F95E1FDA69609D67D7E8 ();
// 0x00000050 System.Void DelayStartWaitingRoomController::WaitingForMorePlayers()
extern void DelayStartWaitingRoomController_WaitingForMorePlayers_m25C50BC93E4CCEFEB6B50A2977CA9C9D1963DB8A ();
// 0x00000051 System.Void DelayStartWaitingRoomController::ResetTimer()
extern void DelayStartWaitingRoomController_ResetTimer_m92BFD9C6D296510DEBF2ACDA8F1055871044533C ();
// 0x00000052 System.Void DelayStartWaitingRoomController::StartGame()
extern void DelayStartWaitingRoomController_StartGame_m4C432EF7640DFCE60417798D1C94F8ED5414041A ();
// 0x00000053 System.Void DelayStartWaitingRoomController::DelayCancel()
extern void DelayStartWaitingRoomController_DelayCancel_mF8088A87F228A8E0995557D02AB13A2FDFC27C0E ();
// 0x00000054 System.Void DelayStartWaitingRoomController::.ctor()
extern void DelayStartWaitingRoomController__ctor_m7B60FE787283D2B5220626B13DDED5CDC18A3373 ();
// 0x00000055 System.Void DragObject::Start()
extern void DragObject_Start_m5B9472E00DBF533D3239A993875573F7CAA9C971 ();
// 0x00000056 System.Void DragObject::Update()
extern void DragObject_Update_m263AACA77705AD5168741959E62244730AFF5B7A ();
// 0x00000057 System.Void DragObject::RPC_BowSoundFx()
extern void DragObject_RPC_BowSoundFx_mFA594202A846893AF553F465D17E77917450E192 ();
// 0x00000058 System.Void DragObject::CreateWeapon()
extern void DragObject_CreateWeapon_m3EB7E7D803538BF06F47DCCF5F65C05FC05424B9 ();
// 0x00000059 System.Void DragObject::.ctor()
extern void DragObject__ctor_m94824E37CDB4EE819AF3A08B806148A398E6B00C ();
// 0x0000005A System.Void DragObject::.cctor()
extern void DragObject__cctor_m552BF31E4C4112EB31FD7DB1C7AD09667664A60D ();
// 0x0000005B System.Void EndCredit::Start()
extern void EndCredit_Start_m3E309548E0D94ED0FDB80C0B63E39FD171F14CBB ();
// 0x0000005C System.Void EndCredit::OnclickDis()
extern void EndCredit_OnclickDis_m1B6F59BF3F85279F5DB564C8982B7E2D76975883 ();
// 0x0000005D System.Void EndCredit::.ctor()
extern void EndCredit__ctor_m2D96ECCD4EC18344CBF66D0427F1F44DB0117E5C ();
// 0x0000005E System.Void GameSetuoController::Start()
extern void GameSetuoController_Start_m65C1D5E6CDC4A82C14CC1CAD95439C305BB7DC14 ();
// 0x0000005F System.Void GameSetuoController::Update()
extern void GameSetuoController_Update_mA5F3E85078182FF7DF75B4B7F23F8D790B2ACCEE ();
// 0x00000060 System.Void GameSetuoController::OnclickFinish()
extern void GameSetuoController_OnclickFinish_mEF2DC18C593EA4E975A709521CD640BF3B390BB2 ();
// 0x00000061 System.Void GameSetuoController::RPC_FinishTrans()
extern void GameSetuoController_RPC_FinishTrans_m07E5FBF6425E1A8183D634F5E4F26636A1DC4FF7 ();
// 0x00000062 System.Void GameSetuoController::OnclickEnter()
extern void GameSetuoController_OnclickEnter_m031563E7F63D201CCD062D976576D49D74750250 ();
// 0x00000063 System.Void GameSetuoController::RPC_TextSend(System.String)
extern void GameSetuoController_RPC_TextSend_m3CD7A6B18F45BF4C1B9C161582FCE68463818636 ();
// 0x00000064 System.Void GameSetuoController::OnClickY()
extern void GameSetuoController_OnClickY_m67A4EED342C5FBE3C0D987AD8756DC114168A923 ();
// 0x00000065 System.Void GameSetuoController::RPC_changeSwapBool(System.Boolean)
extern void GameSetuoController_RPC_changeSwapBool_mBC6F670A82DBEA12753B03A47EF7AE053E52A200 ();
// 0x00000066 System.Void GameSetuoController::OnCLickN()
extern void GameSetuoController_OnCLickN_mBE7C4D4CDC514F01FEC8AAF71C928CB3326D7BDC ();
// 0x00000067 System.Void GameSetuoController::CreatePlayer()
extern void GameSetuoController_CreatePlayer_m7E706D60863D282B3DBE789B328B61E18DF0EDC1 ();
// 0x00000068 System.Void GameSetuoController::DelayCancel()
extern void GameSetuoController_DelayCancel_m53AF2DB5C28EFE554B4BCA31236B64FBF28166EA ();
// 0x00000069 System.Void GameSetuoController::OnPlayerLeftRoom(Photon.Realtime.Player)
extern void GameSetuoController_OnPlayerLeftRoom_m22E6674F31885AB5421533DCA00AD16981F9F929 ();
// 0x0000006A System.Void GameSetuoController::.ctor()
extern void GameSetuoController__ctor_mB20E7C21F7989D91134B4A053BBBA19A9B8C69A0 ();
// 0x0000006B System.Void HealthBarScript::Start()
extern void HealthBarScript_Start_mA183CAE2B3085B6C2C39AA2FD9A70BF37351A596 ();
// 0x0000006C System.Void HealthBarScript::Update()
extern void HealthBarScript_Update_m1524536B326E03B471293D7EF4685AB704221244 ();
// 0x0000006D System.Void HealthBarScript::onClickNo()
extern void HealthBarScript_onClickNo_mD9D1F5268C0720EE5FFCDA6C75F5813950436ACE ();
// 0x0000006E System.Void HealthBarScript::RPC_BoolChange(System.Boolean,System.Boolean)
extern void HealthBarScript_RPC_BoolChange_mEDE3E957C8EB81367BD62592D3570469E6CE2A4B ();
// 0x0000006F System.Void HealthBarScript::SetMaxHealth(System.Int32)
extern void HealthBarScript_SetMaxHealth_m06F39C8B4C503FCA287F4567AF13D2ECD394CF90 ();
// 0x00000070 System.Void HealthBarScript::SetHealth(System.Int32)
extern void HealthBarScript_SetHealth_mDDC56D712E1117F74805CE92AA841EECDBD493ED ();
// 0x00000071 System.Void HealthBarScript::.ctor()
extern void HealthBarScript__ctor_mD770D587B266B45E2E56DF55A408768FAA957320 ();
// 0x00000072 System.Void NetworkController::Start()
extern void NetworkController_Start_m73AF63BC4320765D76E3375991AE68764286679F ();
// 0x00000073 System.Void NetworkController::OnConnectedToMaster()
extern void NetworkController_OnConnectedToMaster_m57B532303B340E7D6EFAC331F870E80BD8669EBC ();
// 0x00000074 System.Void NetworkController::Update()
extern void NetworkController_Update_m58C647D9C642F290A2F4FFC2000BE8A9A961D8DC ();
// 0x00000075 System.Void NetworkController::.ctor()
extern void NetworkController__ctor_m13E3F9EE9B091AD93FFC952C2288ADE2865F7DD2 ();
// 0x00000076 System.Void QuickStartLobbyController::OnConnectedToMaster()
extern void QuickStartLobbyController_OnConnectedToMaster_m1B0BF26C9E0EC3C26CD0DB658110BD4C98792760 ();
// 0x00000077 System.Void QuickStartLobbyController::QuickStart()
extern void QuickStartLobbyController_QuickStart_m0FEB009688B76FCD3A8D3E3781EE22CD98989EE4 ();
// 0x00000078 System.Void QuickStartLobbyController::OnJoinRandomFailed(System.Int16,System.String)
extern void QuickStartLobbyController_OnJoinRandomFailed_m7068C79A57342402FC1C0D35FF75E283113EEC91 ();
// 0x00000079 System.Void QuickStartLobbyController::CreateRoom()
extern void QuickStartLobbyController_CreateRoom_m32AD7F71AAD4D7382BC50B86C99F456ABE4786B2 ();
// 0x0000007A System.Void QuickStartLobbyController::OnCreateRoomFailed(System.Int16,System.String)
extern void QuickStartLobbyController_OnCreateRoomFailed_m5AD53E4EDD3E966B24497FC341E10FC62071C6F8 ();
// 0x0000007B System.Void QuickStartLobbyController::QuickCancel()
extern void QuickStartLobbyController_QuickCancel_m20FC56447A0F1B83762DA04FEBE9D6F06E5386E5 ();
// 0x0000007C System.Void QuickStartLobbyController::.ctor()
extern void QuickStartLobbyController__ctor_m667BDF3C8BC4E4EF1FCACF4A1FE125A53E2F93DC ();
// 0x0000007D System.Void QuickStartRoomController::OnEnable()
extern void QuickStartRoomController_OnEnable_mB29CDBC118DC1407FA8D80B166CD76B1F661457E ();
// 0x0000007E System.Void QuickStartRoomController::OnDisable()
extern void QuickStartRoomController_OnDisable_m769EAEAE86356D5A96DE706EE3D6E7DF8AAB60F6 ();
// 0x0000007F System.Void QuickStartRoomController::OnJoinedRoom()
extern void QuickStartRoomController_OnJoinedRoom_m47210F1A592D21A6C904E0EA1E387429AEE1C274 ();
// 0x00000080 System.Void QuickStartRoomController::.ctor()
extern void QuickStartRoomController__ctor_m62CF3932DE3ABE9F91BC1896544F4DEE0090451E ();
// 0x00000081 System.Void StaticScript::showTxt()
extern void StaticScript_showTxt_m25D3D0B578535A3E8C0F7814CEFAB8B6EDEDF486 ();
// 0x00000082 System.Void StaticScript::OnClickForgiveY()
extern void StaticScript_OnClickForgiveY_mA33842EA33636DFC741DCAFFD40FE72483203F61 ();
// 0x00000083 System.Void StaticScript::OnClickForgiveN()
extern void StaticScript_OnClickForgiveN_mAF5419A013270D0166B898FC088F0EDA41C6F53B ();
// 0x00000084 System.Void StaticScript::RPC_toLastScene(System.String)
extern void StaticScript_RPC_toLastScene_m4C5D0B6AA94C2863AA976240A72A0E83DB854279 ();
// 0x00000085 System.Void StaticScript::.ctor()
extern void StaticScript__ctor_m7E5B0FB704AC298E2DF1464559D1BF0C0BF69D9E ();
// 0x00000086 System.Void UsernameIn::UserNameInput()
extern void UsernameIn_UserNameInput_m48038286CA811F099AC323809C6B37DC39C38B25 ();
// 0x00000087 System.String UsernameIn::NameRe()
extern void UsernameIn_NameRe_m29AC9B36E0817B587B2148A8C7AF66A47AD9C5FD ();
// 0x00000088 System.Void UsernameIn::changeStatusName()
extern void UsernameIn_changeStatusName_mBD00D486491507472225889854DE8784038A0273 ();
// 0x00000089 System.Void UsernameIn::.ctor()
extern void UsernameIn__ctor_m94B46200DFF5F299FA12F16194E5D2F2DC3CC928 ();
// 0x0000008A System.Void UsernameIn::.cctor()
extern void UsernameIn__cctor_m618F1C3EC933D325F51B0D9894C026375DE776FF ();
// 0x0000008B System.Void StandardizedBowForHandBool::Start()
extern void StandardizedBowForHandBool_Start_m6A12954A48EFF4AF7B6324C9DDF11FA838AE717A ();
// 0x0000008C System.Void StandardizedBowForHandBool::Update()
extern void StandardizedBowForHandBool_Update_mEF1614B8EAA5AC6AD7E6F66BEFABABA697EA0161 ();
// 0x0000008D System.Void StandardizedBowForHandBool::StringPull()
extern void StandardizedBowForHandBool_StringPull_m80BFCCEFE949C8032B3E2B57A2787865EAD990BD ();
// 0x0000008E System.Single StandardizedBowForHandBool::InterpolateStringStress(System.Single)
extern void StandardizedBowForHandBool_InterpolateStringStress_m6C95241E5885CFCC6A92FEDF3D0DDF05079D65D3 ();
// 0x0000008F System.Void StandardizedBowForHandBool::RetractString()
extern void StandardizedBowForHandBool_RetractString_m9F416CE5DA92DFDA14336109BA7B15EE772D2C33 ();
// 0x00000090 System.Single StandardizedBowForHandBool::InterpolateStringRetract(System.Single)
extern void StandardizedBowForHandBool_InterpolateStringRetract_m78C41445FFCCE2221333CF13A5341D03F1028360 ();
// 0x00000091 System.Void StandardizedBowForHandBool::RotateUpperJoints()
extern void StandardizedBowForHandBool_RotateUpperJoints_mD9ACA3C22B49D48DA73B2566D6536B0FD25F1780 ();
// 0x00000092 System.Void StandardizedBowForHandBool::RotateDownJoints()
extern void StandardizedBowForHandBool_RotateDownJoints_m8AE932DA3BBF9A45DD1E6B23507EB0AF12F3DD8F ();
// 0x00000093 System.Void StandardizedBowForHandBool::RotateBackJoints()
extern void StandardizedBowForHandBool_RotateBackJoints_m27AF8940B9DFD2D1C44AD52E13EC89BA1759A11E ();
// 0x00000094 System.Void StandardizedBowForHandBool::ProjectileFollowString()
extern void StandardizedBowForHandBool_ProjectileFollowString_mB176A33D5AEE08429960AC3EFB3C79820D87FAE1 ();
// 0x00000095 System.Void StandardizedBowForHandBool::ShootProjectile(System.Single)
extern void StandardizedBowForHandBool_ShootProjectile_m0F4DF800E4C52F091C078BF548AD6DAA604E0667 ();
// 0x00000096 System.Single StandardizedBowForHandBool::CubicEaseIn(System.Single)
extern void StandardizedBowForHandBool_CubicEaseIn_m8DA5A1FBD2CB505ED71370D9BECA648CA6DA9F90 ();
// 0x00000097 System.Single StandardizedBowForHandBool::QuadraticEaseIn(System.Single)
extern void StandardizedBowForHandBool_QuadraticEaseIn_m3B532E18ACDBF129980A91B8F97E20EAC85A0324 ();
// 0x00000098 System.Single StandardizedBowForHandBool::SineEaseIn(System.Single)
extern void StandardizedBowForHandBool_SineEaseIn_mDC91EF2115F161CD9ADB6450A34A3C3EA5ED8817 ();
// 0x00000099 System.Single StandardizedBowForHandBool::QuadraticEaseOut(System.Single)
extern void StandardizedBowForHandBool_QuadraticEaseOut_m47053BA6790D569900311C6952935656CFB51FC6 ();
// 0x0000009A System.Single StandardizedBowForHandBool::CubicEaseOut(System.Single)
extern void StandardizedBowForHandBool_CubicEaseOut_mCFFE9B790388864B4F7BACCD333C0301B2EF8E09 ();
// 0x0000009B System.Single StandardizedBowForHandBool::CircularEaseInOut(System.Single)
extern void StandardizedBowForHandBool_CircularEaseInOut_m7EA37AC6681820FACC08C0C5EAFA361C01DD2B46 ();
// 0x0000009C System.Single StandardizedBowForHandBool::BounceEaseOut(System.Single)
extern void StandardizedBowForHandBool_BounceEaseOut_m13439E2B5B81FA3ED9B730E3C19899A1FB93BF81 ();
// 0x0000009D System.Single StandardizedBowForHandBool::BounceEaseIn(System.Single)
extern void StandardizedBowForHandBool_BounceEaseIn_m715DC83ABE3952FC01D21B0B37CF1E26DB272CF5 ();
// 0x0000009E System.Single StandardizedBowForHandBool::BounceEaseInOut(System.Single)
extern void StandardizedBowForHandBool_BounceEaseInOut_m6BBEFC5399E1D9092154F8E23B460B78DA633973 ();
// 0x0000009F System.Single StandardizedBowForHandBool::BackEaseOut(System.Single)
extern void StandardizedBowForHandBool_BackEaseOut_mF852AA72699784F59833BD2D01539112409CCC04 ();
// 0x000000A0 System.Single StandardizedBowForHandBool::ElasticEaseOut(System.Single)
extern void StandardizedBowForHandBool_ElasticEaseOut_mF12CBA23C2C2A20DE451BEC32E99FB52E79F5C4F ();
// 0x000000A1 System.Void StandardizedBowForHandBool::ProjectileObjectPool()
extern void StandardizedBowForHandBool_ProjectileObjectPool_mFC79FB047343FB0AF606E604A2BFC4F31715EB59 ();
// 0x000000A2 System.Void StandardizedBowForHandBool::AddExtraProjectileToPool()
extern void StandardizedBowForHandBool_AddExtraProjectileToPool_m6D2CB7E3946B2333B73129C2C75C347A08857421 ();
// 0x000000A3 System.Void StandardizedBowForHandBool::FindBoneRigs()
extern void StandardizedBowForHandBool_FindBoneRigs_m19D099D16413DC9313FCCC691D4D1323303969A2 ();
// 0x000000A4 System.Void StandardizedBowForHandBool::.ctor()
extern void StandardizedBowForHandBool__ctor_m736FAC14558DD4AD89A1D98C6CCA7260DF9E7F82 ();
// 0x000000A5 System.Void StandardizedProjectileForHandBool::Start()
extern void StandardizedProjectileForHandBool_Start_m63D41E029B05AEF6D9A066FDD793473DF1CCDF06 ();
// 0x000000A6 System.Void StandardizedProjectileForHandBool::Update()
extern void StandardizedProjectileForHandBool_Update_m154734E3221627B41F4540374992AA9D2FBDF92A ();
// 0x000000A7 System.Void StandardizedProjectileForHandBool::OnTriggerEnter(UnityEngine.Collider)
extern void StandardizedProjectileForHandBool_OnTriggerEnter_m85FD545FE0FE851C8044CFA5B459893FF450AA5C ();
// 0x000000A8 System.Void StandardizedProjectileForHandBool::PoolTheParticles()
extern void StandardizedProjectileForHandBool_PoolTheParticles_m002657C446A53C5C7ACE81B1BF288809DCF916BD ();
// 0x000000A9 System.Void StandardizedProjectileForHandBool::.ctor()
extern void StandardizedProjectileForHandBool__ctor_m0522ADDF483421E36B17117C481DE500E298C57C ();
// 0x000000AA System.Void StandardizedBowForHand::Start()
extern void StandardizedBowForHand_Start_m1773C8DBDB5BAD76D293A48E8113404CB9DEA6D2 ();
// 0x000000AB System.Void StandardizedBowForHand::Update()
extern void StandardizedBowForHand_Update_m030A939AAB7CC55222CDE83E334B9798C9452B89 ();
// 0x000000AC System.Void StandardizedBowForHand::StringPull()
extern void StandardizedBowForHand_StringPull_mE52CAF652458736CCFCA28D9B1E77A020AAD3791 ();
// 0x000000AD System.Single StandardizedBowForHand::InterpolateStringStress(System.Single)
extern void StandardizedBowForHand_InterpolateStringStress_mA7757489997EE63261A910D3181F2C16574C8157 ();
// 0x000000AE System.Void StandardizedBowForHand::RetractString()
extern void StandardizedBowForHand_RetractString_m8087E78F7C64F0C58AE613287482F31C337D99BD ();
// 0x000000AF System.Single StandardizedBowForHand::InterpolateStringRetract(System.Single)
extern void StandardizedBowForHand_InterpolateStringRetract_m1B7B2B2552FAA20C6A54917B7BE77E7D98882659 ();
// 0x000000B0 System.Void StandardizedBowForHand::RotateUpperJoints()
extern void StandardizedBowForHand_RotateUpperJoints_m70420F7740CC8C37BB18346237CB15594871B001 ();
// 0x000000B1 System.Void StandardizedBowForHand::RotateDownJoints()
extern void StandardizedBowForHand_RotateDownJoints_mC55FCD32D74D3B4D7C24F6A428ACC532041D1DC2 ();
// 0x000000B2 System.Void StandardizedBowForHand::RotateBackJoints()
extern void StandardizedBowForHand_RotateBackJoints_m65A46E3E8B84E35A376073ED17F9597C8E906BCD ();
// 0x000000B3 System.Void StandardizedBowForHand::ProjectileFollowString()
extern void StandardizedBowForHand_ProjectileFollowString_m6858F1C8E66DD83DA388A75B8CB8E3B2C14ED8B6 ();
// 0x000000B4 System.Void StandardizedBowForHand::ShootProjectile(System.Single)
extern void StandardizedBowForHand_ShootProjectile_mB3CF31F9DA1EF02CDAFB666EB7D7689D8B7A9842 ();
// 0x000000B5 System.Single StandardizedBowForHand::CubicEaseIn(System.Single)
extern void StandardizedBowForHand_CubicEaseIn_m268C9C97D981EF53AAB062B764D9228A22691359 ();
// 0x000000B6 System.Single StandardizedBowForHand::QuadraticEaseIn(System.Single)
extern void StandardizedBowForHand_QuadraticEaseIn_m5EE167F81D6ACC3DA44B144025176E2EA4B9E6DF ();
// 0x000000B7 System.Single StandardizedBowForHand::SineEaseIn(System.Single)
extern void StandardizedBowForHand_SineEaseIn_m172204E326B3C3BBCB03C88E199A99E68ADDB5F4 ();
// 0x000000B8 System.Single StandardizedBowForHand::QuadraticEaseOut(System.Single)
extern void StandardizedBowForHand_QuadraticEaseOut_m57B1CBB4DEE6352ADCA885E9A8F538B4C6AB3D7F ();
// 0x000000B9 System.Single StandardizedBowForHand::CubicEaseOut(System.Single)
extern void StandardizedBowForHand_CubicEaseOut_mF5BE560206B7D6AE3F84BCFEF8794FD9E03ED96A ();
// 0x000000BA System.Single StandardizedBowForHand::CircularEaseInOut(System.Single)
extern void StandardizedBowForHand_CircularEaseInOut_mD9D0B2F3BBB2F0417616CCABEF4AABB16005BFE3 ();
// 0x000000BB System.Single StandardizedBowForHand::BounceEaseOut(System.Single)
extern void StandardizedBowForHand_BounceEaseOut_mEBDF335F7F958BB4726E0FFE20FE6C16A928FF92 ();
// 0x000000BC System.Single StandardizedBowForHand::BounceEaseIn(System.Single)
extern void StandardizedBowForHand_BounceEaseIn_m7F48CECCDE30B1584FC0DE6602B325BD92B8FAC3 ();
// 0x000000BD System.Single StandardizedBowForHand::BounceEaseInOut(System.Single)
extern void StandardizedBowForHand_BounceEaseInOut_m978C8B0E451CB6F85C4371BCF10132E0878A4828 ();
// 0x000000BE System.Single StandardizedBowForHand::BackEaseOut(System.Single)
extern void StandardizedBowForHand_BackEaseOut_mAB8E7A898F7B175BBF9E4E9E9D87DA68DDDE099F ();
// 0x000000BF System.Single StandardizedBowForHand::ElasticEaseOut(System.Single)
extern void StandardizedBowForHand_ElasticEaseOut_m1E7F3191C120BBA8C12C52DD8BF171E50D2DF11D ();
// 0x000000C0 System.Void StandardizedBowForHand::ProjectileObjectPool()
extern void StandardizedBowForHand_ProjectileObjectPool_m17289603CEBA38D6EE5E8B9A1FF6F69359F825CD ();
// 0x000000C1 System.Void StandardizedBowForHand::AddExtraProjectileToPool()
extern void StandardizedBowForHand_AddExtraProjectileToPool_m1C924F56C9908B385DF867AA23E4D74B2012381B ();
// 0x000000C2 System.Void StandardizedBowForHand::FindBoneRigs()
extern void StandardizedBowForHand_FindBoneRigs_m0642E368599687B91CDCCD3D04B5B6ACB3597EBE ();
// 0x000000C3 System.Void StandardizedBowForHand::.ctor()
extern void StandardizedBowForHand__ctor_m43F1F6888EAD437B9BCF62C12853399EFA58C810 ();
// 0x000000C4 System.Void StandardizedProjectileForHand::Start()
extern void StandardizedProjectileForHand_Start_m4F5E0C126521FE1361FD88B8F97D2DF9783E080D ();
// 0x000000C5 System.Void StandardizedProjectileForHand::Update()
extern void StandardizedProjectileForHand_Update_m2AF87C7DA9CAB6B462B6357DC59E279C34771452 ();
// 0x000000C6 System.Void StandardizedProjectileForHand::OnTriggerEnter(UnityEngine.Collider)
extern void StandardizedProjectileForHand_OnTriggerEnter_m2E6946B686477D1F83D5908CA953CE0F9CBA988C ();
// 0x000000C7 System.Void StandardizedProjectileForHand::PoolTheParticles()
extern void StandardizedProjectileForHand_PoolTheParticles_m77E21683FD266CA090D013C5881F59FBC0E3B428 ();
// 0x000000C8 System.Void StandardizedProjectileForHand::.ctor()
extern void StandardizedProjectileForHand__ctor_m3CAAA27DDD535AA80181B7E6A6A0D6C1174224F3 ();
// 0x000000C9 System.Void StandardizedBow::Start()
extern void StandardizedBow_Start_m648DD7E65EFAE3DDEE022E3095678BB8E65DB90C ();
// 0x000000CA System.Void StandardizedBow::Update()
extern void StandardizedBow_Update_m3036207749578A755856BF1C9AD6AFF5543BD525 ();
// 0x000000CB System.Void StandardizedBow::StringPull()
extern void StandardizedBow_StringPull_mF66133AEB3C6BCBF8C75D2F88E5EC6CA881FA8DE ();
// 0x000000CC System.Single StandardizedBow::InterpolateStringStress(System.Single)
extern void StandardizedBow_InterpolateStringStress_mE0D211A61061E51D403E4675AF7CF9DA84FCC3EA ();
// 0x000000CD System.Void StandardizedBow::RetractString()
extern void StandardizedBow_RetractString_m4ED95F9584E2412BC3778DA346FC7AC8B6012DA3 ();
// 0x000000CE System.Single StandardizedBow::InterpolateStringRetract(System.Single)
extern void StandardizedBow_InterpolateStringRetract_m9851F9A93E3E1F149C01EC20C49A495F4EF77D73 ();
// 0x000000CF System.Void StandardizedBow::RotateUpperJoints()
extern void StandardizedBow_RotateUpperJoints_m178DE5C84C9B7AE9D5822AFC8B85BF59922A50D7 ();
// 0x000000D0 System.Void StandardizedBow::RotateDownJoints()
extern void StandardizedBow_RotateDownJoints_mF62F68F6C073AE7B601D5C4979BA2D397CFAB119 ();
// 0x000000D1 System.Void StandardizedBow::RotateBackJoints()
extern void StandardizedBow_RotateBackJoints_m06482830AB6FE456AACB2DD97759D382496F4945 ();
// 0x000000D2 System.Void StandardizedBow::ProjectileFollowString()
extern void StandardizedBow_ProjectileFollowString_mA6261900F68DC5F7DB69D66ABC7F867A96EAA07E ();
// 0x000000D3 System.Void StandardizedBow::ShootProjectile(System.Single)
extern void StandardizedBow_ShootProjectile_m2A09FAE694F253317A21299B67611D21B2EA18A0 ();
// 0x000000D4 System.Single StandardizedBow::CubicEaseIn(System.Single)
extern void StandardizedBow_CubicEaseIn_mA7D0F0BDE76AF9A809AA1EE6830F0B9ECFAEC2A9 ();
// 0x000000D5 System.Single StandardizedBow::QuadraticEaseIn(System.Single)
extern void StandardizedBow_QuadraticEaseIn_m2EF203B3F001507DACABDC0C8F1A5DE7B84322B5 ();
// 0x000000D6 System.Single StandardizedBow::SineEaseIn(System.Single)
extern void StandardizedBow_SineEaseIn_m80DF6F04326C88828469FE408DF612D03D8D8C4E ();
// 0x000000D7 System.Single StandardizedBow::QuadraticEaseOut(System.Single)
extern void StandardizedBow_QuadraticEaseOut_m9E759B6E2C790A0BDEDAABC1C8FF2B74697DFF52 ();
// 0x000000D8 System.Single StandardizedBow::CubicEaseOut(System.Single)
extern void StandardizedBow_CubicEaseOut_m057DA64588D4B1CDB77DF500CAB299174D29D426 ();
// 0x000000D9 System.Single StandardizedBow::CircularEaseInOut(System.Single)
extern void StandardizedBow_CircularEaseInOut_mE6412B213BC1EE510EE37AAF3E8B7BADA09958E1 ();
// 0x000000DA System.Single StandardizedBow::BounceEaseOut(System.Single)
extern void StandardizedBow_BounceEaseOut_m5D99AA2795DDF8D959E2FBAE655E0AF672615B20 ();
// 0x000000DB System.Single StandardizedBow::BounceEaseIn(System.Single)
extern void StandardizedBow_BounceEaseIn_m58FB9B6BA6147BB860DC06C90700653D12F6FDFF ();
// 0x000000DC System.Single StandardizedBow::BounceEaseInOut(System.Single)
extern void StandardizedBow_BounceEaseInOut_m1946A2B6F8EDA4A9B2C325FD8EBFD2136E376C08 ();
// 0x000000DD System.Single StandardizedBow::BackEaseOut(System.Single)
extern void StandardizedBow_BackEaseOut_mC9BCB4A15C4B775C88C2C6E6EF76C91226AD77CA ();
// 0x000000DE System.Single StandardizedBow::ElasticEaseOut(System.Single)
extern void StandardizedBow_ElasticEaseOut_mF92A6770A15B8316E6FC762297E50BB486400011 ();
// 0x000000DF System.Void StandardizedBow::ProjectileObjectPool()
extern void StandardizedBow_ProjectileObjectPool_mFD18DF17335215B37A7C7CE8F3594CC589BD10AE ();
// 0x000000E0 System.Void StandardizedBow::AddExtraProjectileToPool()
extern void StandardizedBow_AddExtraProjectileToPool_mE434AD89512E25101556F08B0E834739BA3C600C ();
// 0x000000E1 System.Void StandardizedBow::FindBoneRigs()
extern void StandardizedBow_FindBoneRigs_m808189CBF5F159096AF702FE19AF9F0A255E61B9 ();
// 0x000000E2 System.Void StandardizedBow::.ctor()
extern void StandardizedBow__ctor_mB5AFA9B20DD45F344C6F6DAAD63970ADABB14D45 ();
// 0x000000E3 System.Void StandardizedProjectile::Start()
extern void StandardizedProjectile_Start_m4649C596C0577DD1343B04D999C76A03BC293B5B ();
// 0x000000E4 System.Void StandardizedProjectile::Update()
extern void StandardizedProjectile_Update_m8DCA9DA73B25A2F594EE622AA7515711D68ABF4B ();
// 0x000000E5 System.Void StandardizedProjectile::OnTriggerEnter(UnityEngine.Collider)
extern void StandardizedProjectile_OnTriggerEnter_m2587094830A1B3F53D2D2C2DC84A201D7A75140B ();
// 0x000000E6 System.Void StandardizedProjectile::PoolTheParticles()
extern void StandardizedProjectile_PoolTheParticles_mD2D7A70B4254A4CFB7E9E09C1F9F80D0599BE862 ();
// 0x000000E7 System.Void StandardizedProjectile::.ctor()
extern void StandardizedProjectile__ctor_mCE6A40F6AFAB7912C82137A1B9679BCCC66CD067 ();
// 0x000000E8 System.Void ExampleBowUserControl::Start()
extern void ExampleBowUserControl_Start_m3C5E450513EBC1C086FA46DB65A8453F6B39F65D ();
// 0x000000E9 System.Void ExampleBowUserControl::Update()
extern void ExampleBowUserControl_Update_mB22F4B2D09CEA79C850187E8BD148FDE31ECC8AA ();
// 0x000000EA System.Void ExampleBowUserControl::.ctor()
extern void ExampleBowUserControl__ctor_m8FD93112E86F00957DCA18B6E0CB7CB2DDCA3829 ();
// 0x000000EB System.Void MoveAndShoot::Update()
extern void MoveAndShoot_Update_m2A7834E87AB930CF70EC55449FC04EB1399DA91D ();
// 0x000000EC System.Void MoveAndShoot::FixedUpdate()
extern void MoveAndShoot_FixedUpdate_m2D3DB043FED7AAEF3ABF17A48C0C70838A067985 ();
// 0x000000ED System.Void MoveAndShoot::.ctor()
extern void MoveAndShoot__ctor_mDEB05EC082E287073535E3C0472774FDFDE9560D ();
// 0x000000EE System.Void SimpleRotate::Update()
extern void SimpleRotate_Update_m316B3C8D3E409563D766EFA1BF502B48B1179B24 ();
// 0x000000EF System.Void SimpleRotate::.ctor()
extern void SimpleRotate__ctor_mF6DDE2955D721AB862A1CDC494B96C006B9B29E2 ();
// 0x000000F0 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::Start()
extern void ConnectAndJoinRandomLb_Start_mA98A97B7BD8DD57E3543DE0B1E5FC6CC0B4A3231 ();
// 0x000000F1 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::Update()
extern void ConnectAndJoinRandomLb_Update_m266BDF947F50E3AF2ECCB687D647ECCD232949ED ();
// 0x000000F2 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnConnected()
extern void ConnectAndJoinRandomLb_OnConnected_mB1D665FBBE726B592E1E7D389E1C94AED2DAF0A9 ();
// 0x000000F3 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnConnectedToMaster()
extern void ConnectAndJoinRandomLb_OnConnectedToMaster_m080AC337618BC9FE2909797E6556166750675049 ();
// 0x000000F4 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnDisconnected(Photon.Realtime.DisconnectCause)
extern void ConnectAndJoinRandomLb_OnDisconnected_m6A28E69A6A6817AAB303D8B64816AD774CBF9EBF ();
// 0x000000F5 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCustomAuthenticationResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void ConnectAndJoinRandomLb_OnCustomAuthenticationResponse_m4F7287B88E340E32D9DFB3D35C2AA6F53109B9C6 ();
// 0x000000F6 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCustomAuthenticationFailed(System.String)
extern void ConnectAndJoinRandomLb_OnCustomAuthenticationFailed_m87ADCA9E7B0A6B63C2CA770F02B9A22B355C4D19 ();
// 0x000000F7 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnRegionListReceived(Photon.Realtime.RegionHandler)
extern void ConnectAndJoinRandomLb_OnRegionListReceived_mB545EF0E3524D98C44DD46775D086CF8E7B4B951 ();
// 0x000000F8 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnRoomListUpdate(System.Collections.Generic.List`1<Photon.Realtime.RoomInfo>)
extern void ConnectAndJoinRandomLb_OnRoomListUpdate_m2178B3CC6E0FA2B1B0339F755CBF4298FA9937F2 ();
// 0x000000F9 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnLobbyStatisticsUpdate(System.Collections.Generic.List`1<Photon.Realtime.TypedLobbyInfo>)
extern void ConnectAndJoinRandomLb_OnLobbyStatisticsUpdate_mF3C89CDF388A2244288F1D51B5A7CBE7ED7B9A48 ();
// 0x000000FA System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinedLobby()
extern void ConnectAndJoinRandomLb_OnJoinedLobby_mA45DDA20E17E4D123F0F0978CF7CED3F11950FB2 ();
// 0x000000FB System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnLeftLobby()
extern void ConnectAndJoinRandomLb_OnLeftLobby_mD7BE56F629C45BC0815BB543A61E7966D6DE87CF ();
// 0x000000FC System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnFriendListUpdate(System.Collections.Generic.List`1<Photon.Realtime.FriendInfo>)
extern void ConnectAndJoinRandomLb_OnFriendListUpdate_m941C6BCBEFC2DF2739FE244605ED6A61B321EEF1 ();
// 0x000000FD System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCreatedRoom()
extern void ConnectAndJoinRandomLb_OnCreatedRoom_mFBB1A2705AD2064AC3F04BEF02541D7593688998 ();
// 0x000000FE System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCreateRoomFailed(System.Int16,System.String)
extern void ConnectAndJoinRandomLb_OnCreateRoomFailed_m72BB64E1373E5927FF3F6B2948D6ADCCAC3CEE6F ();
// 0x000000FF System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinedRoom()
extern void ConnectAndJoinRandomLb_OnJoinedRoom_mDC96413952B57AFFD5267C8429B4DFCB10D59290 ();
// 0x00000100 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinRoomFailed(System.Int16,System.String)
extern void ConnectAndJoinRandomLb_OnJoinRoomFailed_m02ED1C40A8C92DC5D0DBF45261931F53037FD322 ();
// 0x00000101 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinRandomFailed(System.Int16,System.String)
extern void ConnectAndJoinRandomLb_OnJoinRandomFailed_m432338C1FFCE8F62BC5385CBC2566FBFD50521E2 ();
// 0x00000102 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnLeftRoom()
extern void ConnectAndJoinRandomLb_OnLeftRoom_m00009A5D05AD22EB29246B62EBF7A1AE65DF3C92 ();
// 0x00000103 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnRegionPingCompleted(Photon.Realtime.RegionHandler)
extern void ConnectAndJoinRandomLb_OnRegionPingCompleted_mFE73067D0C0448A40B492514540E5C916170CE7C ();
// 0x00000104 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::.ctor()
extern void ConnectAndJoinRandomLb__ctor_m3FBD3F0B0AA670F431894ADFE9C35E5232026C0C ();
// 0x00000105 System.Void Photon.Chat.UtilityScripts.EventSystemSpawner::Start()
extern void EventSystemSpawner_Start_mB5A22F53FB658BA934ECA5D46167BCBCCC8DB2A9 ();
// 0x00000106 System.Void Photon.Chat.UtilityScripts.EventSystemSpawner::.ctor()
extern void EventSystemSpawner__ctor_m302D63C2836DB052BDDF330E478C8022992E8BC3 ();
// 0x00000107 System.Void Photon.Chat.UtilityScripts.OnStartDelete::Start()
extern void OnStartDelete_Start_m017B9D75D0DFC1FE2B3A67F68D5D000E6F817ADE ();
// 0x00000108 System.Void Photon.Chat.UtilityScripts.OnStartDelete::.ctor()
extern void OnStartDelete__ctor_mD04CC77CF82AE59A6DF89B396F88852F076E3C45 ();
// 0x00000109 System.Void Photon.Chat.UtilityScripts.TextButtonTransition::Awake()
extern void TextButtonTransition_Awake_m8FC85294A7516E31D457D158D04D14E04F98F66B ();
// 0x0000010A System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnEnable()
extern void TextButtonTransition_OnEnable_mA206918D53BC706F59029070A6E5E5A1CC0947B0 ();
// 0x0000010B System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnDisable()
extern void TextButtonTransition_OnDisable_m1322A238564EED4F8B2B016DE67F95C565D43FA4 ();
// 0x0000010C System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TextButtonTransition_OnPointerEnter_m62778690725C0EA9138BF7B0177EF4E460C3E4D5 ();
// 0x0000010D System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TextButtonTransition_OnPointerExit_mE8B5369BCE44F6D9E5BB74F83F09D6589DEF946B ();
// 0x0000010E System.Void Photon.Chat.UtilityScripts.TextButtonTransition::.ctor()
extern void TextButtonTransition__ctor_m005B5F3004290289FEE5EE9B30A68E817EBBFA9C ();
// 0x0000010F System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnEnable()
extern void TextToggleIsOnTransition_OnEnable_m74B3195664BC5AF4E72DFA10E7CC70FCED0E2447 ();
// 0x00000110 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnDisable()
extern void TextToggleIsOnTransition_OnDisable_mC438163F7CE03F6A43FE007BA3AE47BF914B40E3 ();
// 0x00000111 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnValueChanged(System.Boolean)
extern void TextToggleIsOnTransition_OnValueChanged_m09A3FC53039687AFAB04F581C4F013A90B4819FE ();
// 0x00000112 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TextToggleIsOnTransition_OnPointerEnter_mCA942A650C14DBE57E606613DC2B9E8137A093A9 ();
// 0x00000113 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TextToggleIsOnTransition_OnPointerExit_mA13B9D4663513F1BE4B349BE6E58A358507DBE27 ();
// 0x00000114 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::.ctor()
extern void TextToggleIsOnTransition__ctor_m311DFBCFFD1EBB8D8B7003028C967805482E0D2E ();
// 0x00000115 System.Void MotionChanger_<waitSec>d__21::.ctor(System.Int32)
extern void U3CwaitSecU3Ed__21__ctor_m1495E8674C4D2D5690C67862C9C3168775F37795 ();
// 0x00000116 System.Void MotionChanger_<waitSec>d__21::System.IDisposable.Dispose()
extern void U3CwaitSecU3Ed__21_System_IDisposable_Dispose_mCAC1A01CA2775D993604E3C2C78EB0FC9EE4C5A0 ();
// 0x00000117 System.Boolean MotionChanger_<waitSec>d__21::MoveNext()
extern void U3CwaitSecU3Ed__21_MoveNext_m3B0C02BB6C53549ED02DA7551FAFC71E654D1B0E ();
// 0x00000118 System.Object MotionChanger_<waitSec>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CwaitSecU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58834DA4451B2E6F256BFEA82E8C8EF53D47D1B5 ();
// 0x00000119 System.Void MotionChanger_<waitSec>d__21::System.Collections.IEnumerator.Reset()
extern void U3CwaitSecU3Ed__21_System_Collections_IEnumerator_Reset_mCB063CBF74E29536DFB8BF94AE6646E1853DBAA9 ();
// 0x0000011A System.Object MotionChanger_<waitSec>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CwaitSecU3Ed__21_System_Collections_IEnumerator_get_Current_m902F6DAC03ACE0388BEF5237E6C03D314164F6FB ();
// 0x0000011B System.Void MotionChanger_<WaitDelay>d__24::.ctor(System.Int32)
extern void U3CWaitDelayU3Ed__24__ctor_m7AA1FDCE081E6E64809DB4791480E11A5DF9C102 ();
// 0x0000011C System.Void MotionChanger_<WaitDelay>d__24::System.IDisposable.Dispose()
extern void U3CWaitDelayU3Ed__24_System_IDisposable_Dispose_mEFB1C119EEADE65BBCAEBFF8E7100DBE02D41D38 ();
// 0x0000011D System.Boolean MotionChanger_<WaitDelay>d__24::MoveNext()
extern void U3CWaitDelayU3Ed__24_MoveNext_mBB45A3EADF066F3EA6E48CBC579C14D434B9E931 ();
// 0x0000011E System.Object MotionChanger_<WaitDelay>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitDelayU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF1E0E69CE5A3B295F75393088175A3B0A0EF0E82 ();
// 0x0000011F System.Void MotionChanger_<WaitDelay>d__24::System.Collections.IEnumerator.Reset()
extern void U3CWaitDelayU3Ed__24_System_Collections_IEnumerator_Reset_m2E203E3CE8E33FAF49881B10949249B006685CA8 ();
// 0x00000120 System.Object MotionChanger_<WaitDelay>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CWaitDelayU3Ed__24_System_Collections_IEnumerator_get_Current_mFF3B1B7F01AFC05FFF7F42E5FD4281F93EAB09F0 ();
static Il2CppMethodPointer s_methodPointers[288] = 
{
	AcquireChanController_Awake_mC2CFB4AD467DFEF49079BD2E81145B9A85057A96,
	AcquireChanController_Update_m274A4CD4C3653DB63418CFD9E38E2C93B4394815,
	AcquireChanController__ctor_m87D8CAD8778B02874F5C1A604B8992E6A0D29BB3,
	MotionChanger_Start_m236F623AD6DB7802B2222EB3C1DD4195DDA992A8,
	MotionChanger_Update_m2E62977E5D90378AA7FE538888F2897F56B4CA18,
	MotionChanger_NextAnimation_m5B375F97BB6C33F79EF27FE647265DBB2D150553,
	MotionChanger_PrevAnimation_m7CEA9488097CAE17F5F400E62095C34C860B2811,
	MotionChanger_OnGUI_m73EA75D5B0245C91239C07521F211D03A0D4D223,
	MotionChanger_OnTriggerEnter_mD3FDA1C97D397E7802914132FDC199966F60B62D,
	MotionChanger_TakeDamage_mB8E8D1E8B4D979484B31135FD177695BF2D23714,
	MotionChanger_waitSec_mD2873F5D9DF2108D71C84C9580776D2816672E75,
	MotionChanger_regenerate_m6E0FFBED4C8EE1E4326D645378986C615820C557,
	MotionChanger_UpdateChat_m707796E1AA7279C8AF8630C52EA65AA0BEC01255,
	MotionChanger_WaitDelay_mC56B2F40698751795346DEB88242135E2A7026AA,
	MotionChanger__ctor_m41C5694AAD9DCC8E3E636152A7BF9BFC045678CA,
	SimpleFollowCam_FixedUpdate_m89F6F4B0134F40CE83C66FD40BF0EA729A802383,
	SimpleFollowCam__ctor_mEBB42530BFD0D019F1DAA8974235FC32FF4CFF9C,
	ChannelSelector_SetChannel_m42445342022E7FFA42EC3F5A15D2E5F1230F20EF,
	ChannelSelector_OnPointerClick_mEC70BF8011630441106C9D276F68567679D08F2B,
	ChannelSelector__ctor_m1A9CA3EDE510F2C0936E460355944CF963C53AF8,
	ChatAppIdCheckerUI_Update_m1AC6AE7BE4D369EE3FA8B071E97CDD8978434351,
	ChatAppIdCheckerUI__ctor_m42CD82562A49DD287217CE2FDEB1FFCA8DCCD976,
	ChatGui_get_UserName_m2D7189147C1B72BD1BF3B240234E3C8FADA4CEBE,
	ChatGui_set_UserName_mB775E61D7B0AE9547A9FC4986DF5C0829051BD66,
	ChatGui_Start_m9E8D8C2BC4462E72E991CD45E8E3CC8A2B13DA11,
	ChatGui_Connect_m0F2777BC430257B61AEC2E0195EB8833B2A823A9,
	ChatGui_OnDestroy_mA878BA277C56EFD51ED99E0BA7E3C27E4217A599,
	ChatGui_OnApplicationQuit_m2E53294EA4F5AFAEC09012D90D35C9669869CB69,
	ChatGui_Update_m117EE1AE4617CB186701801E27A81C4F19C66A0C,
	ChatGui_OnEnterSend_m0203888ED9A4F79D7D4BE0C65FCEBFD856D02E1A,
	ChatGui_OnClickSend_mCA632A41AAB1AC6138100466C4BA6D21DB79A3BD,
	ChatGui_SendChatMessage_m2935E03C2E3E895FF39B820DBF9EBB68ED82109C,
	ChatGui_PostHelpToCurrentChannel_mFC53973D9F29FF1EF925EC2688AE4480C15BCBAB,
	ChatGui_DebugReturn_m2EB4F0C1B1259132A79AB1588A448C6E4A23646A,
	ChatGui_OnConnected_mC0826EAB343F4A3DD575EBF26B53B085457316DF,
	ChatGui_OnDisconnected_m76182F0ED767653F7FFAD61B815C632A78D0F0AC,
	ChatGui_OnChatStateChange_m83E3976649121248276C423B82287AF3393E1FA0,
	ChatGui_OnSubscribed_mC4CE0E33989BAD899756434A8D9674542EEE7787,
	ChatGui_InstantiateChannelButton_mAC43B7D5ABBAC119A7B0E79DB7D327EAC0EB61DA,
	ChatGui_InstantiateFriendButton_m770E010399E1B8C7143BA4E9DD209D64E48958DA,
	ChatGui_OnUnsubscribed_m7B92447BA0144BA8BAD69F372CCA49FCD7DDBE24,
	ChatGui_OnGetMessages_m41520F9090CAC47E8D28FBF082E4092B2EFE9371,
	ChatGui_OnPrivateMessage_m020F729CA33DFE2F4CBD08B3A693FA61D080573A,
	ChatGui_OnStatusUpdate_m0318643027388B3911C050A32E3910B1C2784D83,
	ChatGui_OnUserSubscribed_m429025D0F91C78B629AB5CC8F40C86FF80E9BAB0,
	ChatGui_OnUserUnsubscribed_mF3C325C964655102B5B3D3FC4ABFB54569B889B7,
	ChatGui_AddMessageToSelectedChannel_mB25598D1E6E34A2C82ABF808FD2C19D41CC88131,
	ChatGui_ShowChannel_mA1621F270A0B212DF44223B183E9695FA5FD69B0,
	ChatGui_OpenDashboard_m8FB1CA73CF3C535A743F1C1523DFD1484E819D0B,
	ChatGui__ctor_mA4A951576B88ECBDB754514AC4114E60386608B5,
	ChatGui__cctor_m72B9DCD148155721FBF7C8FBE0F21AEDEA0CD15C,
	FriendItem_set_FriendId_mCAE0C9115FBDA3AD25FDC4279D674B1498ECBCA2,
	FriendItem_get_FriendId_m3FB64D35B15F1E85065C0AB278543C2F0913F1F1,
	FriendItem_Awake_mDA0F0972D787D79FE657F35F829796442DF5FE2E,
	FriendItem_OnFriendStatusUpdate_m92B9750B90F833B15663B4127531C1D2EE5252AD,
	FriendItem__ctor_m77402589F7CB63B107066CD7B2F2BC650ABAEF95,
	IgnoreUiRaycastWhenInactive_IsRaycastLocationValid_mB04A97B0C6B2F741AA1775807054F3F147BDAA31,
	IgnoreUiRaycastWhenInactive__ctor_m573C1522D6C18004507A53DB8DADE79F19E3BB9F,
	NamePickGui_Start_mB0BBD21B5440F12C73AB5B7250640910DD92B25B,
	NamePickGui_EndEditOnEnter_m4D90D94D1B3BB2085E74AB280CE788148694BB5E,
	NamePickGui_StartChat_m0F97F13D467453D7F2667E889F64136E5FA101DC,
	NamePickGui__ctor_mE766F508304EC60C4BE09549C401A0F050C5C3D8,
	DelayStartLobbyController_OnConnectedToMaster_m067E38EDD94DA292E40CAFEF6D7733851A05309A,
	DelayStartLobbyController_DelayStart_m4A9F853EE29241EF04B8005C726E4967AF0B624F,
	DelayStartLobbyController_OnJoinRandomFailed_m15FD332E50F8FA939A8ED18C60370AAE49B3FF36,
	DelayStartLobbyController_CreateRoom_m5C3AB00138E2ECE922BBF35271651F4C8D84E2A3,
	DelayStartLobbyController_OnCreateRoomFailed_m31E0373A8E11957AF61DD935BC94C1D5E4F26F53,
	DelayStartLobbyController_DelayCancel_m23313E4BC00FC534732AB4D02524A7CA43664200,
	DelayStartLobbyController__ctor_m2FC5A5E9DD2FD84A6B2D2724BE9BCC764BE04E16,
	DelayStartRoomCOntroller_OnEnable_m4A3B04BFEC1A1044781AA6370ED261EF35F5DB92,
	DelayStartRoomCOntroller_OnDisable_m32AE09C36DAEB8D7028BDF94164B05C384B3C642,
	DelayStartRoomCOntroller_OnJoinedRoom_m7BCFBB1BE560F2FD8C4F006C58CA0D1A4B862277,
	DelayStartRoomCOntroller__ctor_m57B2535BA034DB930EAD2503E64367B1BDD7BF46,
	DelayStartWaitingRoomController_Start_mAC221BFE52BE2A6A2106AD402A6E9F7131437B36,
	DelayStartWaitingRoomController_PlayerCountUpdate_m81E7DC8387A23E0751ECECB45FC61610EBAD1B31,
	DelayStartWaitingRoomController_OnPlayerEnteredRoom_mF55B786CF5987FE7B5DC9053D0F14E02B4ED4348,
	DelayStartWaitingRoomController_RPC_SendTimer_m4A6F84E41DD8854FDEFA3AF8FA593ADEE088ACB0,
	DelayStartWaitingRoomController_OnPlayerLeftRoom_m32F63AE7E1D8AB0BE6EAB25E87228C1B04CC5C0E,
	DelayStartWaitingRoomController_Update_mD41BB300CB2F90257C70F95E1FDA69609D67D7E8,
	DelayStartWaitingRoomController_WaitingForMorePlayers_m25C50BC93E4CCEFEB6B50A2977CA9C9D1963DB8A,
	DelayStartWaitingRoomController_ResetTimer_m92BFD9C6D296510DEBF2ACDA8F1055871044533C,
	DelayStartWaitingRoomController_StartGame_m4C432EF7640DFCE60417798D1C94F8ED5414041A,
	DelayStartWaitingRoomController_DelayCancel_mF8088A87F228A8E0995557D02AB13A2FDFC27C0E,
	DelayStartWaitingRoomController__ctor_m7B60FE787283D2B5220626B13DDED5CDC18A3373,
	DragObject_Start_m5B9472E00DBF533D3239A993875573F7CAA9C971,
	DragObject_Update_m263AACA77705AD5168741959E62244730AFF5B7A,
	DragObject_RPC_BowSoundFx_mFA594202A846893AF553F465D17E77917450E192,
	DragObject_CreateWeapon_m3EB7E7D803538BF06F47DCCF5F65C05FC05424B9,
	DragObject__ctor_m94824E37CDB4EE819AF3A08B806148A398E6B00C,
	DragObject__cctor_m552BF31E4C4112EB31FD7DB1C7AD09667664A60D,
	EndCredit_Start_m3E309548E0D94ED0FDB80C0B63E39FD171F14CBB,
	EndCredit_OnclickDis_m1B6F59BF3F85279F5DB564C8982B7E2D76975883,
	EndCredit__ctor_m2D96ECCD4EC18344CBF66D0427F1F44DB0117E5C,
	GameSetuoController_Start_m65C1D5E6CDC4A82C14CC1CAD95439C305BB7DC14,
	GameSetuoController_Update_mA5F3E85078182FF7DF75B4B7F23F8D790B2ACCEE,
	GameSetuoController_OnclickFinish_mEF2DC18C593EA4E975A709521CD640BF3B390BB2,
	GameSetuoController_RPC_FinishTrans_m07E5FBF6425E1A8183D634F5E4F26636A1DC4FF7,
	GameSetuoController_OnclickEnter_m031563E7F63D201CCD062D976576D49D74750250,
	GameSetuoController_RPC_TextSend_m3CD7A6B18F45BF4C1B9C161582FCE68463818636,
	GameSetuoController_OnClickY_m67A4EED342C5FBE3C0D987AD8756DC114168A923,
	GameSetuoController_RPC_changeSwapBool_mBC6F670A82DBEA12753B03A47EF7AE053E52A200,
	GameSetuoController_OnCLickN_mBE7C4D4CDC514F01FEC8AAF71C928CB3326D7BDC,
	GameSetuoController_CreatePlayer_m7E706D60863D282B3DBE789B328B61E18DF0EDC1,
	GameSetuoController_DelayCancel_m53AF2DB5C28EFE554B4BCA31236B64FBF28166EA,
	GameSetuoController_OnPlayerLeftRoom_m22E6674F31885AB5421533DCA00AD16981F9F929,
	GameSetuoController__ctor_mB20E7C21F7989D91134B4A053BBBA19A9B8C69A0,
	HealthBarScript_Start_mA183CAE2B3085B6C2C39AA2FD9A70BF37351A596,
	HealthBarScript_Update_m1524536B326E03B471293D7EF4685AB704221244,
	HealthBarScript_onClickNo_mD9D1F5268C0720EE5FFCDA6C75F5813950436ACE,
	HealthBarScript_RPC_BoolChange_mEDE3E957C8EB81367BD62592D3570469E6CE2A4B,
	HealthBarScript_SetMaxHealth_m06F39C8B4C503FCA287F4567AF13D2ECD394CF90,
	HealthBarScript_SetHealth_mDDC56D712E1117F74805CE92AA841EECDBD493ED,
	HealthBarScript__ctor_mD770D587B266B45E2E56DF55A408768FAA957320,
	NetworkController_Start_m73AF63BC4320765D76E3375991AE68764286679F,
	NetworkController_OnConnectedToMaster_m57B532303B340E7D6EFAC331F870E80BD8669EBC,
	NetworkController_Update_m58C647D9C642F290A2F4FFC2000BE8A9A961D8DC,
	NetworkController__ctor_m13E3F9EE9B091AD93FFC952C2288ADE2865F7DD2,
	QuickStartLobbyController_OnConnectedToMaster_m1B0BF26C9E0EC3C26CD0DB658110BD4C98792760,
	QuickStartLobbyController_QuickStart_m0FEB009688B76FCD3A8D3E3781EE22CD98989EE4,
	QuickStartLobbyController_OnJoinRandomFailed_m7068C79A57342402FC1C0D35FF75E283113EEC91,
	QuickStartLobbyController_CreateRoom_m32AD7F71AAD4D7382BC50B86C99F456ABE4786B2,
	QuickStartLobbyController_OnCreateRoomFailed_m5AD53E4EDD3E966B24497FC341E10FC62071C6F8,
	QuickStartLobbyController_QuickCancel_m20FC56447A0F1B83762DA04FEBE9D6F06E5386E5,
	QuickStartLobbyController__ctor_m667BDF3C8BC4E4EF1FCACF4A1FE125A53E2F93DC,
	QuickStartRoomController_OnEnable_mB29CDBC118DC1407FA8D80B166CD76B1F661457E,
	QuickStartRoomController_OnDisable_m769EAEAE86356D5A96DE706EE3D6E7DF8AAB60F6,
	QuickStartRoomController_OnJoinedRoom_m47210F1A592D21A6C904E0EA1E387429AEE1C274,
	QuickStartRoomController__ctor_m62CF3932DE3ABE9F91BC1896544F4DEE0090451E,
	StaticScript_showTxt_m25D3D0B578535A3E8C0F7814CEFAB8B6EDEDF486,
	StaticScript_OnClickForgiveY_mA33842EA33636DFC741DCAFFD40FE72483203F61,
	StaticScript_OnClickForgiveN_mAF5419A013270D0166B898FC088F0EDA41C6F53B,
	StaticScript_RPC_toLastScene_m4C5D0B6AA94C2863AA976240A72A0E83DB854279,
	StaticScript__ctor_m7E5B0FB704AC298E2DF1464559D1BF0C0BF69D9E,
	UsernameIn_UserNameInput_m48038286CA811F099AC323809C6B37DC39C38B25,
	UsernameIn_NameRe_m29AC9B36E0817B587B2148A8C7AF66A47AD9C5FD,
	UsernameIn_changeStatusName_mBD00D486491507472225889854DE8784038A0273,
	UsernameIn__ctor_m94B46200DFF5F299FA12F16194E5D2F2DC3CC928,
	UsernameIn__cctor_m618F1C3EC933D325F51B0D9894C026375DE776FF,
	StandardizedBowForHandBool_Start_m6A12954A48EFF4AF7B6324C9DDF11FA838AE717A,
	StandardizedBowForHandBool_Update_mEF1614B8EAA5AC6AD7E6F66BEFABABA697EA0161,
	StandardizedBowForHandBool_StringPull_m80BFCCEFE949C8032B3E2B57A2787865EAD990BD,
	StandardizedBowForHandBool_InterpolateStringStress_m6C95241E5885CFCC6A92FEDF3D0DDF05079D65D3,
	StandardizedBowForHandBool_RetractString_m9F416CE5DA92DFDA14336109BA7B15EE772D2C33,
	StandardizedBowForHandBool_InterpolateStringRetract_m78C41445FFCCE2221333CF13A5341D03F1028360,
	StandardizedBowForHandBool_RotateUpperJoints_mD9ACA3C22B49D48DA73B2566D6536B0FD25F1780,
	StandardizedBowForHandBool_RotateDownJoints_m8AE932DA3BBF9A45DD1E6B23507EB0AF12F3DD8F,
	StandardizedBowForHandBool_RotateBackJoints_m27AF8940B9DFD2D1C44AD52E13EC89BA1759A11E,
	StandardizedBowForHandBool_ProjectileFollowString_mB176A33D5AEE08429960AC3EFB3C79820D87FAE1,
	StandardizedBowForHandBool_ShootProjectile_m0F4DF800E4C52F091C078BF548AD6DAA604E0667,
	StandardizedBowForHandBool_CubicEaseIn_m8DA5A1FBD2CB505ED71370D9BECA648CA6DA9F90,
	StandardizedBowForHandBool_QuadraticEaseIn_m3B532E18ACDBF129980A91B8F97E20EAC85A0324,
	StandardizedBowForHandBool_SineEaseIn_mDC91EF2115F161CD9ADB6450A34A3C3EA5ED8817,
	StandardizedBowForHandBool_QuadraticEaseOut_m47053BA6790D569900311C6952935656CFB51FC6,
	StandardizedBowForHandBool_CubicEaseOut_mCFFE9B790388864B4F7BACCD333C0301B2EF8E09,
	StandardizedBowForHandBool_CircularEaseInOut_m7EA37AC6681820FACC08C0C5EAFA361C01DD2B46,
	StandardizedBowForHandBool_BounceEaseOut_m13439E2B5B81FA3ED9B730E3C19899A1FB93BF81,
	StandardizedBowForHandBool_BounceEaseIn_m715DC83ABE3952FC01D21B0B37CF1E26DB272CF5,
	StandardizedBowForHandBool_BounceEaseInOut_m6BBEFC5399E1D9092154F8E23B460B78DA633973,
	StandardizedBowForHandBool_BackEaseOut_mF852AA72699784F59833BD2D01539112409CCC04,
	StandardizedBowForHandBool_ElasticEaseOut_mF12CBA23C2C2A20DE451BEC32E99FB52E79F5C4F,
	StandardizedBowForHandBool_ProjectileObjectPool_mFC79FB047343FB0AF606E604A2BFC4F31715EB59,
	StandardizedBowForHandBool_AddExtraProjectileToPool_m6D2CB7E3946B2333B73129C2C75C347A08857421,
	StandardizedBowForHandBool_FindBoneRigs_m19D099D16413DC9313FCCC691D4D1323303969A2,
	StandardizedBowForHandBool__ctor_m736FAC14558DD4AD89A1D98C6CCA7260DF9E7F82,
	StandardizedProjectileForHandBool_Start_m63D41E029B05AEF6D9A066FDD793473DF1CCDF06,
	StandardizedProjectileForHandBool_Update_m154734E3221627B41F4540374992AA9D2FBDF92A,
	StandardizedProjectileForHandBool_OnTriggerEnter_m85FD545FE0FE851C8044CFA5B459893FF450AA5C,
	StandardizedProjectileForHandBool_PoolTheParticles_m002657C446A53C5C7ACE81B1BF288809DCF916BD,
	StandardizedProjectileForHandBool__ctor_m0522ADDF483421E36B17117C481DE500E298C57C,
	StandardizedBowForHand_Start_m1773C8DBDB5BAD76D293A48E8113404CB9DEA6D2,
	StandardizedBowForHand_Update_m030A939AAB7CC55222CDE83E334B9798C9452B89,
	StandardizedBowForHand_StringPull_mE52CAF652458736CCFCA28D9B1E77A020AAD3791,
	StandardizedBowForHand_InterpolateStringStress_mA7757489997EE63261A910D3181F2C16574C8157,
	StandardizedBowForHand_RetractString_m8087E78F7C64F0C58AE613287482F31C337D99BD,
	StandardizedBowForHand_InterpolateStringRetract_m1B7B2B2552FAA20C6A54917B7BE77E7D98882659,
	StandardizedBowForHand_RotateUpperJoints_m70420F7740CC8C37BB18346237CB15594871B001,
	StandardizedBowForHand_RotateDownJoints_mC55FCD32D74D3B4D7C24F6A428ACC532041D1DC2,
	StandardizedBowForHand_RotateBackJoints_m65A46E3E8B84E35A376073ED17F9597C8E906BCD,
	StandardizedBowForHand_ProjectileFollowString_m6858F1C8E66DD83DA388A75B8CB8E3B2C14ED8B6,
	StandardizedBowForHand_ShootProjectile_mB3CF31F9DA1EF02CDAFB666EB7D7689D8B7A9842,
	StandardizedBowForHand_CubicEaseIn_m268C9C97D981EF53AAB062B764D9228A22691359,
	StandardizedBowForHand_QuadraticEaseIn_m5EE167F81D6ACC3DA44B144025176E2EA4B9E6DF,
	StandardizedBowForHand_SineEaseIn_m172204E326B3C3BBCB03C88E199A99E68ADDB5F4,
	StandardizedBowForHand_QuadraticEaseOut_m57B1CBB4DEE6352ADCA885E9A8F538B4C6AB3D7F,
	StandardizedBowForHand_CubicEaseOut_mF5BE560206B7D6AE3F84BCFEF8794FD9E03ED96A,
	StandardizedBowForHand_CircularEaseInOut_mD9D0B2F3BBB2F0417616CCABEF4AABB16005BFE3,
	StandardizedBowForHand_BounceEaseOut_mEBDF335F7F958BB4726E0FFE20FE6C16A928FF92,
	StandardizedBowForHand_BounceEaseIn_m7F48CECCDE30B1584FC0DE6602B325BD92B8FAC3,
	StandardizedBowForHand_BounceEaseInOut_m978C8B0E451CB6F85C4371BCF10132E0878A4828,
	StandardizedBowForHand_BackEaseOut_mAB8E7A898F7B175BBF9E4E9E9D87DA68DDDE099F,
	StandardizedBowForHand_ElasticEaseOut_m1E7F3191C120BBA8C12C52DD8BF171E50D2DF11D,
	StandardizedBowForHand_ProjectileObjectPool_m17289603CEBA38D6EE5E8B9A1FF6F69359F825CD,
	StandardizedBowForHand_AddExtraProjectileToPool_m1C924F56C9908B385DF867AA23E4D74B2012381B,
	StandardizedBowForHand_FindBoneRigs_m0642E368599687B91CDCCD3D04B5B6ACB3597EBE,
	StandardizedBowForHand__ctor_m43F1F6888EAD437B9BCF62C12853399EFA58C810,
	StandardizedProjectileForHand_Start_m4F5E0C126521FE1361FD88B8F97D2DF9783E080D,
	StandardizedProjectileForHand_Update_m2AF87C7DA9CAB6B462B6357DC59E279C34771452,
	StandardizedProjectileForHand_OnTriggerEnter_m2E6946B686477D1F83D5908CA953CE0F9CBA988C,
	StandardizedProjectileForHand_PoolTheParticles_m77E21683FD266CA090D013C5881F59FBC0E3B428,
	StandardizedProjectileForHand__ctor_m3CAAA27DDD535AA80181B7E6A6A0D6C1174224F3,
	StandardizedBow_Start_m648DD7E65EFAE3DDEE022E3095678BB8E65DB90C,
	StandardizedBow_Update_m3036207749578A755856BF1C9AD6AFF5543BD525,
	StandardizedBow_StringPull_mF66133AEB3C6BCBF8C75D2F88E5EC6CA881FA8DE,
	StandardizedBow_InterpolateStringStress_mE0D211A61061E51D403E4675AF7CF9DA84FCC3EA,
	StandardizedBow_RetractString_m4ED95F9584E2412BC3778DA346FC7AC8B6012DA3,
	StandardizedBow_InterpolateStringRetract_m9851F9A93E3E1F149C01EC20C49A495F4EF77D73,
	StandardizedBow_RotateUpperJoints_m178DE5C84C9B7AE9D5822AFC8B85BF59922A50D7,
	StandardizedBow_RotateDownJoints_mF62F68F6C073AE7B601D5C4979BA2D397CFAB119,
	StandardizedBow_RotateBackJoints_m06482830AB6FE456AACB2DD97759D382496F4945,
	StandardizedBow_ProjectileFollowString_mA6261900F68DC5F7DB69D66ABC7F867A96EAA07E,
	StandardizedBow_ShootProjectile_m2A09FAE694F253317A21299B67611D21B2EA18A0,
	StandardizedBow_CubicEaseIn_mA7D0F0BDE76AF9A809AA1EE6830F0B9ECFAEC2A9,
	StandardizedBow_QuadraticEaseIn_m2EF203B3F001507DACABDC0C8F1A5DE7B84322B5,
	StandardizedBow_SineEaseIn_m80DF6F04326C88828469FE408DF612D03D8D8C4E,
	StandardizedBow_QuadraticEaseOut_m9E759B6E2C790A0BDEDAABC1C8FF2B74697DFF52,
	StandardizedBow_CubicEaseOut_m057DA64588D4B1CDB77DF500CAB299174D29D426,
	StandardizedBow_CircularEaseInOut_mE6412B213BC1EE510EE37AAF3E8B7BADA09958E1,
	StandardizedBow_BounceEaseOut_m5D99AA2795DDF8D959E2FBAE655E0AF672615B20,
	StandardizedBow_BounceEaseIn_m58FB9B6BA6147BB860DC06C90700653D12F6FDFF,
	StandardizedBow_BounceEaseInOut_m1946A2B6F8EDA4A9B2C325FD8EBFD2136E376C08,
	StandardizedBow_BackEaseOut_mC9BCB4A15C4B775C88C2C6E6EF76C91226AD77CA,
	StandardizedBow_ElasticEaseOut_mF92A6770A15B8316E6FC762297E50BB486400011,
	StandardizedBow_ProjectileObjectPool_mFD18DF17335215B37A7C7CE8F3594CC589BD10AE,
	StandardizedBow_AddExtraProjectileToPool_mE434AD89512E25101556F08B0E834739BA3C600C,
	StandardizedBow_FindBoneRigs_m808189CBF5F159096AF702FE19AF9F0A255E61B9,
	StandardizedBow__ctor_mB5AFA9B20DD45F344C6F6DAAD63970ADABB14D45,
	StandardizedProjectile_Start_m4649C596C0577DD1343B04D999C76A03BC293B5B,
	StandardizedProjectile_Update_m8DCA9DA73B25A2F594EE622AA7515711D68ABF4B,
	StandardizedProjectile_OnTriggerEnter_m2587094830A1B3F53D2D2C2DC84A201D7A75140B,
	StandardizedProjectile_PoolTheParticles_mD2D7A70B4254A4CFB7E9E09C1F9F80D0599BE862,
	StandardizedProjectile__ctor_mCE6A40F6AFAB7912C82137A1B9679BCCC66CD067,
	ExampleBowUserControl_Start_m3C5E450513EBC1C086FA46DB65A8453F6B39F65D,
	ExampleBowUserControl_Update_mB22F4B2D09CEA79C850187E8BD148FDE31ECC8AA,
	ExampleBowUserControl__ctor_m8FD93112E86F00957DCA18B6E0CB7CB2DDCA3829,
	MoveAndShoot_Update_m2A7834E87AB930CF70EC55449FC04EB1399DA91D,
	MoveAndShoot_FixedUpdate_m2D3DB043FED7AAEF3ABF17A48C0C70838A067985,
	MoveAndShoot__ctor_mDEB05EC082E287073535E3C0472774FDFDE9560D,
	SimpleRotate_Update_m316B3C8D3E409563D766EFA1BF502B48B1179B24,
	SimpleRotate__ctor_mF6DDE2955D721AB862A1CDC494B96C006B9B29E2,
	ConnectAndJoinRandomLb_Start_mA98A97B7BD8DD57E3543DE0B1E5FC6CC0B4A3231,
	ConnectAndJoinRandomLb_Update_m266BDF947F50E3AF2ECCB687D647ECCD232949ED,
	ConnectAndJoinRandomLb_OnConnected_mB1D665FBBE726B592E1E7D389E1C94AED2DAF0A9,
	ConnectAndJoinRandomLb_OnConnectedToMaster_m080AC337618BC9FE2909797E6556166750675049,
	ConnectAndJoinRandomLb_OnDisconnected_m6A28E69A6A6817AAB303D8B64816AD774CBF9EBF,
	ConnectAndJoinRandomLb_OnCustomAuthenticationResponse_m4F7287B88E340E32D9DFB3D35C2AA6F53109B9C6,
	ConnectAndJoinRandomLb_OnCustomAuthenticationFailed_m87ADCA9E7B0A6B63C2CA770F02B9A22B355C4D19,
	ConnectAndJoinRandomLb_OnRegionListReceived_mB545EF0E3524D98C44DD46775D086CF8E7B4B951,
	ConnectAndJoinRandomLb_OnRoomListUpdate_m2178B3CC6E0FA2B1B0339F755CBF4298FA9937F2,
	ConnectAndJoinRandomLb_OnLobbyStatisticsUpdate_mF3C89CDF388A2244288F1D51B5A7CBE7ED7B9A48,
	ConnectAndJoinRandomLb_OnJoinedLobby_mA45DDA20E17E4D123F0F0978CF7CED3F11950FB2,
	ConnectAndJoinRandomLb_OnLeftLobby_mD7BE56F629C45BC0815BB543A61E7966D6DE87CF,
	ConnectAndJoinRandomLb_OnFriendListUpdate_m941C6BCBEFC2DF2739FE244605ED6A61B321EEF1,
	ConnectAndJoinRandomLb_OnCreatedRoom_mFBB1A2705AD2064AC3F04BEF02541D7593688998,
	ConnectAndJoinRandomLb_OnCreateRoomFailed_m72BB64E1373E5927FF3F6B2948D6ADCCAC3CEE6F,
	ConnectAndJoinRandomLb_OnJoinedRoom_mDC96413952B57AFFD5267C8429B4DFCB10D59290,
	ConnectAndJoinRandomLb_OnJoinRoomFailed_m02ED1C40A8C92DC5D0DBF45261931F53037FD322,
	ConnectAndJoinRandomLb_OnJoinRandomFailed_m432338C1FFCE8F62BC5385CBC2566FBFD50521E2,
	ConnectAndJoinRandomLb_OnLeftRoom_m00009A5D05AD22EB29246B62EBF7A1AE65DF3C92,
	ConnectAndJoinRandomLb_OnRegionPingCompleted_mFE73067D0C0448A40B492514540E5C916170CE7C,
	ConnectAndJoinRandomLb__ctor_m3FBD3F0B0AA670F431894ADFE9C35E5232026C0C,
	EventSystemSpawner_Start_mB5A22F53FB658BA934ECA5D46167BCBCCC8DB2A9,
	EventSystemSpawner__ctor_m302D63C2836DB052BDDF330E478C8022992E8BC3,
	OnStartDelete_Start_m017B9D75D0DFC1FE2B3A67F68D5D000E6F817ADE,
	OnStartDelete__ctor_mD04CC77CF82AE59A6DF89B396F88852F076E3C45,
	TextButtonTransition_Awake_m8FC85294A7516E31D457D158D04D14E04F98F66B,
	TextButtonTransition_OnEnable_mA206918D53BC706F59029070A6E5E5A1CC0947B0,
	TextButtonTransition_OnDisable_m1322A238564EED4F8B2B016DE67F95C565D43FA4,
	TextButtonTransition_OnPointerEnter_m62778690725C0EA9138BF7B0177EF4E460C3E4D5,
	TextButtonTransition_OnPointerExit_mE8B5369BCE44F6D9E5BB74F83F09D6589DEF946B,
	TextButtonTransition__ctor_m005B5F3004290289FEE5EE9B30A68E817EBBFA9C,
	TextToggleIsOnTransition_OnEnable_m74B3195664BC5AF4E72DFA10E7CC70FCED0E2447,
	TextToggleIsOnTransition_OnDisable_mC438163F7CE03F6A43FE007BA3AE47BF914B40E3,
	TextToggleIsOnTransition_OnValueChanged_m09A3FC53039687AFAB04F581C4F013A90B4819FE,
	TextToggleIsOnTransition_OnPointerEnter_mCA942A650C14DBE57E606613DC2B9E8137A093A9,
	TextToggleIsOnTransition_OnPointerExit_mA13B9D4663513F1BE4B349BE6E58A358507DBE27,
	TextToggleIsOnTransition__ctor_m311DFBCFFD1EBB8D8B7003028C967805482E0D2E,
	U3CwaitSecU3Ed__21__ctor_m1495E8674C4D2D5690C67862C9C3168775F37795,
	U3CwaitSecU3Ed__21_System_IDisposable_Dispose_mCAC1A01CA2775D993604E3C2C78EB0FC9EE4C5A0,
	U3CwaitSecU3Ed__21_MoveNext_m3B0C02BB6C53549ED02DA7551FAFC71E654D1B0E,
	U3CwaitSecU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58834DA4451B2E6F256BFEA82E8C8EF53D47D1B5,
	U3CwaitSecU3Ed__21_System_Collections_IEnumerator_Reset_mCB063CBF74E29536DFB8BF94AE6646E1853DBAA9,
	U3CwaitSecU3Ed__21_System_Collections_IEnumerator_get_Current_m902F6DAC03ACE0388BEF5237E6C03D314164F6FB,
	U3CWaitDelayU3Ed__24__ctor_m7AA1FDCE081E6E64809DB4791480E11A5DF9C102,
	U3CWaitDelayU3Ed__24_System_IDisposable_Dispose_mEFB1C119EEADE65BBCAEBFF8E7100DBE02D41D38,
	U3CWaitDelayU3Ed__24_MoveNext_mBB45A3EADF066F3EA6E48CBC579C14D434B9E931,
	U3CWaitDelayU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF1E0E69CE5A3B295F75393088175A3B0A0EF0E82,
	U3CWaitDelayU3Ed__24_System_Collections_IEnumerator_Reset_m2E203E3CE8E33FAF49881B10949249B006685CA8,
	U3CWaitDelayU3Ed__24_System_Collections_IEnumerator_get_Current_mFF3B1B7F01AFC05FFF7F42E5FD4281F93EAB09F0,
};
static const int32_t s_InvokerIndices[288] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	32,
	28,
	23,
	26,
	28,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	88,
	23,
	23,
	32,
	27,
	26,
	26,
	26,
	211,
	211,
	1748,
	27,
	27,
	26,
	26,
	23,
	23,
	3,
	26,
	14,
	23,
	642,
	23,
	1843,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1115,
	23,
	1115,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	346,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	31,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	42,
	32,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1115,
	23,
	1115,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	14,
	23,
	23,
	3,
	23,
	23,
	23,
	1810,
	23,
	1810,
	23,
	23,
	23,
	23,
	346,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	1810,
	23,
	1810,
	23,
	23,
	23,
	23,
	346,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	1810,
	23,
	1810,
	23,
	23,
	23,
	23,
	346,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	446,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	26,
	23,
	1115,
	23,
	1115,
	1115,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	31,
	26,
	26,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	288,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
