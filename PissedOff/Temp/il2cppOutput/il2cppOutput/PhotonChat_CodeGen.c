﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Boolean Photon.Chat.ChannelCreationOptions::get_PublishSubscribers()
extern void ChannelCreationOptions_get_PublishSubscribers_m95B809EAFBB363FA01A7132CD7FA48D2200D85AC ();
// 0x00000002 System.Void Photon.Chat.ChannelCreationOptions::set_PublishSubscribers(System.Boolean)
extern void ChannelCreationOptions_set_PublishSubscribers_m5A86AB97EE0743192DF387D96C8D954E8E872351 ();
// 0x00000003 System.Int32 Photon.Chat.ChannelCreationOptions::get_MaxSubscribers()
extern void ChannelCreationOptions_get_MaxSubscribers_m695771BA6E0451DEF5C00D270D95F218735DD6F0 ();
// 0x00000004 System.Void Photon.Chat.ChannelCreationOptions::set_MaxSubscribers(System.Int32)
extern void ChannelCreationOptions_set_MaxSubscribers_m93B6C3596A984DBD6FBB4EB89920062DC90FD0B8 ();
// 0x00000005 System.Void Photon.Chat.ChannelCreationOptions::.ctor()
extern void ChannelCreationOptions__ctor_m81C207BB78F06CA880DDBE76D5B747ACD145194E ();
// 0x00000006 System.Void Photon.Chat.ChannelCreationOptions::.cctor()
extern void ChannelCreationOptions__cctor_m0C802A2ED726B657BA62EAF7DD1717C77A42A6F4 ();
// 0x00000007 System.Void Photon.Chat.ChannelWellKnownProperties::.ctor()
extern void ChannelWellKnownProperties__ctor_mACCE1BE31AABEB657F01B5401D95FC370D674F80 ();
// 0x00000008 System.Boolean Photon.Chat.ChatChannel::get_IsPrivate()
extern void ChatChannel_get_IsPrivate_mF3161AC27D69E4594D632BE0C37D2010A5E3FA2D ();
// 0x00000009 System.Void Photon.Chat.ChatChannel::set_IsPrivate(System.Boolean)
extern void ChatChannel_set_IsPrivate_m1BA5274B9E487BB6284FDFBAFDF12C1074F86A28 ();
// 0x0000000A System.Int32 Photon.Chat.ChatChannel::get_MessageCount()
extern void ChatChannel_get_MessageCount_m55833104CB82ADCAE20517D318E130365D54C835 ();
// 0x0000000B System.Int32 Photon.Chat.ChatChannel::get_LastMsgId()
extern void ChatChannel_get_LastMsgId_mB439D932AC09AFD3712CFDCBF5B83585E7957892 ();
// 0x0000000C System.Void Photon.Chat.ChatChannel::set_LastMsgId(System.Int32)
extern void ChatChannel_set_LastMsgId_mD7539C79B24056E2718FDD4DA4DED06AE641863C ();
// 0x0000000D System.Boolean Photon.Chat.ChatChannel::get_PublishSubscribers()
extern void ChatChannel_get_PublishSubscribers_m95072AE75264032C94FBE71EA09F257C889B18EB ();
// 0x0000000E System.Void Photon.Chat.ChatChannel::set_PublishSubscribers(System.Boolean)
extern void ChatChannel_set_PublishSubscribers_m3162B1905CF3F87749D06FD6B865836C37B9F24C ();
// 0x0000000F System.Int32 Photon.Chat.ChatChannel::get_MaxSubscribers()
extern void ChatChannel_get_MaxSubscribers_m7564E3A11AF219E2DECD04BB4A27BD5B3D2C8915 ();
// 0x00000010 System.Void Photon.Chat.ChatChannel::set_MaxSubscribers(System.Int32)
extern void ChatChannel_set_MaxSubscribers_mA3EF3E020422FFE128ADDCC8C0657D77AF353D62 ();
// 0x00000011 System.Void Photon.Chat.ChatChannel::.ctor(System.String)
extern void ChatChannel__ctor_mA5DF33529980160873130ACB6B51CC8D3CF23C83 ();
// 0x00000012 System.Void Photon.Chat.ChatChannel::Add(System.String,System.Object,System.Int32)
extern void ChatChannel_Add_m27C5E5A559983353AB0D695A3A82056B23B6853E ();
// 0x00000013 System.Void Photon.Chat.ChatChannel::Add(System.String[],System.Object[],System.Int32)
extern void ChatChannel_Add_m40DB5A55A3C7DFD1195FBF16C50CBFBBECBA3DFA ();
// 0x00000014 System.Void Photon.Chat.ChatChannel::TruncateMessages()
extern void ChatChannel_TruncateMessages_m8BAAC468313ADAED4EC6C86B55EA068DE0A2268E ();
// 0x00000015 System.Void Photon.Chat.ChatChannel::ClearMessages()
extern void ChatChannel_ClearMessages_m9EF1467E2BD5CB8CAE4DC2FFFAEF551327661E55 ();
// 0x00000016 System.String Photon.Chat.ChatChannel::ToStringMessages()
extern void ChatChannel_ToStringMessages_m72F03F0CBA803F8A6BA13C02D981EB23506B06B1 ();
// 0x00000017 System.Void Photon.Chat.ChatChannel::ReadProperties(System.Collections.Generic.Dictionary`2<System.Object,System.Object>)
extern void ChatChannel_ReadProperties_m6633C7C905179618C610C9EB69610AEEE570A09D ();
// 0x00000018 System.Void Photon.Chat.ChatChannel::AddSubscribers(System.String[])
extern void ChatChannel_AddSubscribers_m67F53F0F32C4061FB3870EE624D7481338F950EB ();
// 0x00000019 System.Void Photon.Chat.ChatChannel::ClearProperties()
extern void ChatChannel_ClearProperties_m4A373CC7CFCB23C5472292D8ED0D4E5AC6D7A036 ();
// 0x0000001A System.String Photon.Chat.ChatClient::get_NameServerAddress()
extern void ChatClient_get_NameServerAddress_m27512FE5315C5872B2215EFFC468BF354EB6896D ();
// 0x0000001B System.Void Photon.Chat.ChatClient::set_NameServerAddress(System.String)
extern void ChatClient_set_NameServerAddress_m7A3F9E28BE94D5DA80D7559F6A265DD2C86510E6 ();
// 0x0000001C System.String Photon.Chat.ChatClient::get_FrontendAddress()
extern void ChatClient_get_FrontendAddress_m847BEC17A9DDF9204AB4A1C860392DDD6BCB7F83 ();
// 0x0000001D System.Void Photon.Chat.ChatClient::set_FrontendAddress(System.String)
extern void ChatClient_set_FrontendAddress_mF45D1E2D49E57DCCB99146E706C21245D972C4CC ();
// 0x0000001E System.String Photon.Chat.ChatClient::get_ChatRegion()
extern void ChatClient_get_ChatRegion_mBDD657C571E8583799BF73FA271B82BE6D06F2D9 ();
// 0x0000001F System.Void Photon.Chat.ChatClient::set_ChatRegion(System.String)
extern void ChatClient_set_ChatRegion_mDFE542A6A3CE4AB4173A7E16079D14E5ECECEFCB ();
// 0x00000020 Photon.Chat.ChatState Photon.Chat.ChatClient::get_State()
extern void ChatClient_get_State_m699EC4A67CAA0596492E6E32DA46654A3DBC9EA0 ();
// 0x00000021 System.Void Photon.Chat.ChatClient::set_State(Photon.Chat.ChatState)
extern void ChatClient_set_State_mB4F7447BD9819E2D8EE5E6F169A0AE7A0F49DC1E ();
// 0x00000022 Photon.Chat.ChatDisconnectCause Photon.Chat.ChatClient::get_DisconnectedCause()
extern void ChatClient_get_DisconnectedCause_m57D498FAFF15DE6D6ED299021BAD2EEE35B2EEFC ();
// 0x00000023 System.Void Photon.Chat.ChatClient::set_DisconnectedCause(Photon.Chat.ChatDisconnectCause)
extern void ChatClient_set_DisconnectedCause_m0726D082C1B2F8E366BAC950319DEA20CF09FFDB ();
// 0x00000024 System.Boolean Photon.Chat.ChatClient::get_CanChat()
extern void ChatClient_get_CanChat_m303DEA407E43F29A30A67CF23FA9BF1D8CDC28F7 ();
// 0x00000025 System.Boolean Photon.Chat.ChatClient::CanChatInChannel(System.String)
extern void ChatClient_CanChatInChannel_m056F9E3199627DC00B9D30327069029F5601CF1C ();
// 0x00000026 System.Boolean Photon.Chat.ChatClient::get_HasPeer()
extern void ChatClient_get_HasPeer_m0F8268A96258BC9856A223ACE894A1DBD2C2026B ();
// 0x00000027 System.String Photon.Chat.ChatClient::get_AppVersion()
extern void ChatClient_get_AppVersion_m325B5082717878BDB33E2AD0F338BA920984489B ();
// 0x00000028 System.Void Photon.Chat.ChatClient::set_AppVersion(System.String)
extern void ChatClient_set_AppVersion_m7852C67700E10420F7E14D3BA0C4DC5E5E9781C8 ();
// 0x00000029 System.String Photon.Chat.ChatClient::get_AppId()
extern void ChatClient_get_AppId_mA154B74C72A401D4A2569B9D1DBE815AC26AB8CC ();
// 0x0000002A System.Void Photon.Chat.ChatClient::set_AppId(System.String)
extern void ChatClient_set_AppId_m49B2AD5DFFCB3328EB95E1416DC4640BAC72847A ();
// 0x0000002B Photon.Chat.AuthenticationValues Photon.Chat.ChatClient::get_AuthValues()
extern void ChatClient_get_AuthValues_mF82FDE2770658A9C379F1CF74DF14A601F5D317E ();
// 0x0000002C System.Void Photon.Chat.ChatClient::set_AuthValues(Photon.Chat.AuthenticationValues)
extern void ChatClient_set_AuthValues_mA9F19EDA73501C75C59D69C949B66953CE0B8EFE ();
// 0x0000002D System.String Photon.Chat.ChatClient::get_UserId()
extern void ChatClient_get_UserId_m2572A1A6BB1413F845DED350093EAAFD6B518291 ();
// 0x0000002E System.Void Photon.Chat.ChatClient::set_UserId(System.String)
extern void ChatClient_set_UserId_m5499473A68F5A0ADD366380F3135D9FC32A9EC6D ();
// 0x0000002F System.Boolean Photon.Chat.ChatClient::get_UseBackgroundWorkerForSending()
extern void ChatClient_get_UseBackgroundWorkerForSending_mA5A8EE30F7BB6048082C233967E2DA5D2CEE77E5 ();
// 0x00000030 System.Void Photon.Chat.ChatClient::set_UseBackgroundWorkerForSending(System.Boolean)
extern void ChatClient_set_UseBackgroundWorkerForSending_mFC84C720BB9E2EFC7780214093D51CBB9765A53F ();
// 0x00000031 ExitGames.Client.Photon.ConnectionProtocol Photon.Chat.ChatClient::get_TransportProtocol()
extern void ChatClient_get_TransportProtocol_m25094E1A427A4A31BA5A4406B2B68A5AA6B3BEE3 ();
// 0x00000032 System.Void Photon.Chat.ChatClient::set_TransportProtocol(ExitGames.Client.Photon.ConnectionProtocol)
extern void ChatClient_set_TransportProtocol_m6286E7D74D21E1C6D7A6FF1230E5160A590A8238 ();
// 0x00000033 System.Collections.Generic.Dictionary`2<ExitGames.Client.Photon.ConnectionProtocol,System.Type> Photon.Chat.ChatClient::get_SocketImplementationConfig()
extern void ChatClient_get_SocketImplementationConfig_m273FA4B8A6B399A7246D4052DEAE505355649C2E ();
// 0x00000034 System.Void Photon.Chat.ChatClient::.ctor(Photon.Chat.IChatClientListener,ExitGames.Client.Photon.ConnectionProtocol)
extern void ChatClient__ctor_m519ED33F2BE9B3BA820520F051C3208EC059FC16 ();
// 0x00000035 System.Boolean Photon.Chat.ChatClient::Connect(System.String,System.String,Photon.Chat.AuthenticationValues)
extern void ChatClient_Connect_m6A720637091E96EB0559F3BEAF85B0415BB0D376 ();
// 0x00000036 System.Boolean Photon.Chat.ChatClient::ConnectAndSetStatus(System.String,System.String,Photon.Chat.AuthenticationValues,System.Int32,System.Object)
extern void ChatClient_ConnectAndSetStatus_m3111CC36FB9C7871CD736E0C376BF0868859CC87 ();
// 0x00000037 System.Void Photon.Chat.ChatClient::Service()
extern void ChatClient_Service_m2EF335812DD640B6CC84AACDDB02689B595D95AE ();
// 0x00000038 System.Boolean Photon.Chat.ChatClient::SendOutgoingInBackground()
extern void ChatClient_SendOutgoingInBackground_m6DACEF4746C9018F9300FC55DB31F72459728CE4 ();
// 0x00000039 System.Void Photon.Chat.ChatClient::SendAcksOnly()
extern void ChatClient_SendAcksOnly_m085143D7A29367B04DF0FF6C58ED4CF5894195F9 ();
// 0x0000003A System.Void Photon.Chat.ChatClient::Disconnect()
extern void ChatClient_Disconnect_m60913D750CE62764246A28A0024E536FCE8EDD90 ();
// 0x0000003B System.Void Photon.Chat.ChatClient::StopThread()
extern void ChatClient_StopThread_mA350213BEDE2B23419BF0DB5DCE01423AA4E51B8 ();
// 0x0000003C System.Boolean Photon.Chat.ChatClient::Subscribe(System.String[])
extern void ChatClient_Subscribe_m01D13AF1DF355F1AFEA2CA2F3184247CCF9EF2EB ();
// 0x0000003D System.Boolean Photon.Chat.ChatClient::Subscribe(System.String[],System.Int32[])
extern void ChatClient_Subscribe_m162186BD32689824C32D27765C4418229ADE67F1 ();
// 0x0000003E System.Boolean Photon.Chat.ChatClient::Subscribe(System.String[],System.Int32)
extern void ChatClient_Subscribe_m0391C33DF36612A52DE871DD0B1726EA62F99B2A ();
// 0x0000003F System.Boolean Photon.Chat.ChatClient::Unsubscribe(System.String[])
extern void ChatClient_Unsubscribe_mF3824B5D3C47882442414123A2C48B31C62532CC ();
// 0x00000040 System.Boolean Photon.Chat.ChatClient::PublishMessage(System.String,System.Object,System.Boolean)
extern void ChatClient_PublishMessage_mC4654F785E2073DF765DB653B9AC6B0275BFF082 ();
// 0x00000041 System.Boolean Photon.Chat.ChatClient::PublishMessageUnreliable(System.String,System.Object,System.Boolean)
extern void ChatClient_PublishMessageUnreliable_mE608B1F6C93BCCCC6D02A71C15717684AD0B8A43 ();
// 0x00000042 System.Boolean Photon.Chat.ChatClient::publishMessage(System.String,System.Object,System.Boolean,System.Boolean)
extern void ChatClient_publishMessage_m112332E73165EF56FF7ED01EEF1EED95C417077D ();
// 0x00000043 System.Boolean Photon.Chat.ChatClient::SendPrivateMessage(System.String,System.Object,System.Boolean)
extern void ChatClient_SendPrivateMessage_m5F692F987A6E6233681A597066279DD7BC2F41EB ();
// 0x00000044 System.Boolean Photon.Chat.ChatClient::SendPrivateMessage(System.String,System.Object,System.Boolean,System.Boolean)
extern void ChatClient_SendPrivateMessage_m901045581F7DE5BAC226C52C9369184523EC5118 ();
// 0x00000045 System.Boolean Photon.Chat.ChatClient::SendPrivateMessageUnreliable(System.String,System.Object,System.Boolean,System.Boolean)
extern void ChatClient_SendPrivateMessageUnreliable_m21DF7070ADC81ADC83BF32FA0536D1D80310C4A9 ();
// 0x00000046 System.Boolean Photon.Chat.ChatClient::sendPrivateMessage(System.String,System.Object,System.Boolean,System.Boolean,System.Boolean)
extern void ChatClient_sendPrivateMessage_mCF7B7BD77EB7D22DFD1ED058A4235578551C0C02 ();
// 0x00000047 System.Boolean Photon.Chat.ChatClient::SetOnlineStatus(System.Int32,System.Object,System.Boolean)
extern void ChatClient_SetOnlineStatus_mC9DBD5B31D8A48987B17A7E4C7A6D8CF6C299B85 ();
// 0x00000048 System.Boolean Photon.Chat.ChatClient::SetOnlineStatus(System.Int32)
extern void ChatClient_SetOnlineStatus_mEB17F03D8059E4C4A407087F4798D1BB83FD2849 ();
// 0x00000049 System.Boolean Photon.Chat.ChatClient::SetOnlineStatus(System.Int32,System.Object)
extern void ChatClient_SetOnlineStatus_mB88272E0588B53E913EC3CED58A35D185FE50B0D ();
// 0x0000004A System.Boolean Photon.Chat.ChatClient::AddFriends(System.String[])
extern void ChatClient_AddFriends_mCC05D291C9509A0F4D776428BC69AFF28DD25AB7 ();
// 0x0000004B System.Boolean Photon.Chat.ChatClient::RemoveFriends(System.String[])
extern void ChatClient_RemoveFriends_m55D5D9623A9B84C801016004A1E436F606417513 ();
// 0x0000004C System.String Photon.Chat.ChatClient::GetPrivateChannelNameByUser(System.String)
extern void ChatClient_GetPrivateChannelNameByUser_mF71560A0FA905208F34FCA6331BD19051A598F2E ();
// 0x0000004D System.Boolean Photon.Chat.ChatClient::TryGetChannel(System.String,System.Boolean,Photon.Chat.ChatChannel&)
extern void ChatClient_TryGetChannel_m1353AC1501810593E12AD94C6B40A9DCA4272E5D ();
// 0x0000004E System.Boolean Photon.Chat.ChatClient::TryGetChannel(System.String,Photon.Chat.ChatChannel&)
extern void ChatClient_TryGetChannel_m98591C6D7F4304FAC4F1FAE967DC8AD8B36C684B ();
// 0x0000004F System.Boolean Photon.Chat.ChatClient::TryGetPrivateChannelByUser(System.String,Photon.Chat.ChatChannel&)
extern void ChatClient_TryGetPrivateChannelByUser_mEC5655F7F36413A2C05FA54794DC7681F0E7EF6A ();
// 0x00000050 System.Void Photon.Chat.ChatClient::set_DebugOut(ExitGames.Client.Photon.DebugLevel)
extern void ChatClient_set_DebugOut_m743928BC0E48DDBFF6189961C7021981B81F5F14 ();
// 0x00000051 ExitGames.Client.Photon.DebugLevel Photon.Chat.ChatClient::get_DebugOut()
extern void ChatClient_get_DebugOut_m2CF0AB00C4149E1C87579AED3259E9C16FF5EE3E ();
// 0x00000052 System.Void Photon.Chat.ChatClient::ExitGames.Client.Photon.IPhotonPeerListener.DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_DebugReturn_m839DC9F90D7FAA6FB99ED2BD11F70DD011C24F3E ();
// 0x00000053 System.Void Photon.Chat.ChatClient::ExitGames.Client.Photon.IPhotonPeerListener.OnEvent(ExitGames.Client.Photon.EventData)
extern void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnEvent_mCB50D65A6476CA679FC0A53086670174BD9B6520 ();
// 0x00000054 System.Void Photon.Chat.ChatClient::ExitGames.Client.Photon.IPhotonPeerListener.OnOperationResponse(ExitGames.Client.Photon.OperationResponse)
extern void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnOperationResponse_mF3D11ED8D0695BC30EB93DB09EB5ADCDB5D757DD ();
// 0x00000055 System.Void Photon.Chat.ChatClient::ExitGames.Client.Photon.IPhotonPeerListener.OnStatusChanged(ExitGames.Client.Photon.StatusCode)
extern void ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnStatusChanged_m35846B72C3CD364C901F26FEDB3F0BCA98BCB8E0 ();
// 0x00000056 System.Boolean Photon.Chat.ChatClient::SendChannelOperation(System.String[],System.Byte,System.Int32)
extern void ChatClient_SendChannelOperation_m18D325052913A99B794CF2CC4B24D290344B97AF ();
// 0x00000057 System.Void Photon.Chat.ChatClient::HandlePrivateMessageEvent(ExitGames.Client.Photon.EventData)
extern void ChatClient_HandlePrivateMessageEvent_mDA58DBBF050FCDB626C81EFC54565ED921B3964D ();
// 0x00000058 System.Void Photon.Chat.ChatClient::HandleChatMessagesEvent(ExitGames.Client.Photon.EventData)
extern void ChatClient_HandleChatMessagesEvent_m396D3671FD7D82CDDAA061853140CC3509E0C08C ();
// 0x00000059 System.Void Photon.Chat.ChatClient::HandleSubscribeEvent(ExitGames.Client.Photon.EventData)
extern void ChatClient_HandleSubscribeEvent_mFB647ACE16281DFE79CDFBCB7BAC435C43099DA4 ();
// 0x0000005A System.Void Photon.Chat.ChatClient::HandleUnsubscribeEvent(ExitGames.Client.Photon.EventData)
extern void ChatClient_HandleUnsubscribeEvent_m9A4724792F7A5DA097C59201F4BE301CEE752D5B ();
// 0x0000005B System.Void Photon.Chat.ChatClient::HandleAuthResponse(ExitGames.Client.Photon.OperationResponse)
extern void ChatClient_HandleAuthResponse_m1B87CA382F32D97CC8FBE955D8D8A77D93E29C47 ();
// 0x0000005C System.Void Photon.Chat.ChatClient::HandleStatusUpdate(ExitGames.Client.Photon.EventData)
extern void ChatClient_HandleStatusUpdate_m6AAACBA3D1841A4B8E6FE0FC4347CE6AFD793DFA ();
// 0x0000005D System.Void Photon.Chat.ChatClient::ConnectToFrontEnd()
extern void ChatClient_ConnectToFrontEnd_m83830EDC762ADBB4EF3835E212BFE190F7AC100A ();
// 0x0000005E System.Boolean Photon.Chat.ChatClient::AuthenticateOnFrontEnd()
extern void ChatClient_AuthenticateOnFrontEnd_mFA3F028897624959C21DBBEAFABC611E7D40D178 ();
// 0x0000005F System.Void Photon.Chat.ChatClient::HandleUserUnsubscribedEvent(ExitGames.Client.Photon.EventData)
extern void ChatClient_HandleUserUnsubscribedEvent_m28F7860D105EF0348648A3002AEEB6DA37ED22D1 ();
// 0x00000060 System.Void Photon.Chat.ChatClient::HandleUserSubscribedEvent(ExitGames.Client.Photon.EventData)
extern void ChatClient_HandleUserSubscribedEvent_mE87D9FFBF7C3B8AE0BBA5F0448C9A0947A72E07B ();
// 0x00000061 System.Boolean Photon.Chat.ChatClient::Subscribe(System.String,System.Int32,System.Int32,Photon.Chat.ChannelCreationOptions)
extern void ChatClient_Subscribe_m0DB2A59D3FAE36EB09B8F34839AD18A95D0E6A98 ();
// 0x00000062 System.Void Photon.Chat.ChatEventCode::.ctor()
extern void ChatEventCode__ctor_m17DE89A722A955012AEC573B9C87560E318AD081 ();
// 0x00000063 System.Void Photon.Chat.ChatOperationCode::.ctor()
extern void ChatOperationCode__ctor_m481F24E834ADF462072735029D4886E36F0C149E ();
// 0x00000064 System.Void Photon.Chat.ChatParameterCode::.ctor()
extern void ChatParameterCode__ctor_mB5BE08E745BDC3571F9E24C104FA8AB379D20B3F ();
// 0x00000065 System.String Photon.Chat.ChatPeer::get_NameServerAddress()
extern void ChatPeer_get_NameServerAddress_mEC869AA34F11AD1F019EA0D6F7090A154DC0CAA7 ();
// 0x00000066 System.Boolean Photon.Chat.ChatPeer::get_IsProtocolSecure()
extern void ChatPeer_get_IsProtocolSecure_m5A49DEB1B8850A3F2F40527EF6151312D4776601 ();
// 0x00000067 System.Void Photon.Chat.ChatPeer::.ctor(ExitGames.Client.Photon.IPhotonPeerListener,ExitGames.Client.Photon.ConnectionProtocol)
extern void ChatPeer__ctor_mDAE5572BCAEA02789867C206B4E820CC30A18A44 ();
// 0x00000068 System.Void Photon.Chat.ChatPeer::ConfigUnitySockets()
extern void ChatPeer_ConfigUnitySockets_m3CA9CA2B2B41E6CC69775F1112F8BDF897F6C2B0 ();
// 0x00000069 System.String Photon.Chat.ChatPeer::GetNameServerAddress()
extern void ChatPeer_GetNameServerAddress_mEE174269D17B37F6B706229ADE9F5A070637A82A ();
// 0x0000006A System.Boolean Photon.Chat.ChatPeer::Connect()
extern void ChatPeer_Connect_m57D6143BBC1FFA33E40820A96611D098C8E2C9C2 ();
// 0x0000006B System.Boolean Photon.Chat.ChatPeer::AuthenticateOnNameServer(System.String,System.String,System.String,Photon.Chat.AuthenticationValues)
extern void ChatPeer_AuthenticateOnNameServer_mEE9E9AC65A21D029BB21F0D3BFDDAFE0D79316BB ();
// 0x0000006C System.Void Photon.Chat.ChatPeer::.cctor()
extern void ChatPeer__cctor_m22A7660C34602B8F951AC2CEBC56AA40DFEE7866 ();
// 0x0000006D Photon.Chat.CustomAuthenticationType Photon.Chat.AuthenticationValues::get_AuthType()
extern void AuthenticationValues_get_AuthType_mCBBF19A8A2D5B91B99EB4E1A529888E883EEB137 ();
// 0x0000006E System.Void Photon.Chat.AuthenticationValues::set_AuthType(Photon.Chat.CustomAuthenticationType)
extern void AuthenticationValues_set_AuthType_mFF8089E6A86CA5A54EB9AED446F55077422B6D80 ();
// 0x0000006F System.String Photon.Chat.AuthenticationValues::get_AuthGetParameters()
extern void AuthenticationValues_get_AuthGetParameters_m3E0298098E97E6C4EE98BEBD4C9180F4B2EFA2DC ();
// 0x00000070 System.Void Photon.Chat.AuthenticationValues::set_AuthGetParameters(System.String)
extern void AuthenticationValues_set_AuthGetParameters_mA4168C7B93997D0957D5373984D60D58C3D763F2 ();
// 0x00000071 System.Object Photon.Chat.AuthenticationValues::get_AuthPostData()
extern void AuthenticationValues_get_AuthPostData_mFF1BEF9E3C7A6F0C0AB2094957BE9657D8C9AC3F ();
// 0x00000072 System.Void Photon.Chat.AuthenticationValues::set_AuthPostData(System.Object)
extern void AuthenticationValues_set_AuthPostData_m4BB0A1F1BF32144A5AE42460E6DC676D40AD8061 ();
// 0x00000073 System.String Photon.Chat.AuthenticationValues::get_Token()
extern void AuthenticationValues_get_Token_mEB968C8B2E7A8D874E7DB46257D897A7DFEEDAA2 ();
// 0x00000074 System.Void Photon.Chat.AuthenticationValues::set_Token(System.String)
extern void AuthenticationValues_set_Token_m0377AF534F7FFAA94542BA9747D9FB79DF40C80F ();
// 0x00000075 System.String Photon.Chat.AuthenticationValues::get_UserId()
extern void AuthenticationValues_get_UserId_m240707AC145B84C517AA9251278E3A6AD3A3C9F6 ();
// 0x00000076 System.Void Photon.Chat.AuthenticationValues::set_UserId(System.String)
extern void AuthenticationValues_set_UserId_m3D9EE5F706F311197E729BD61E81A37CDC81DE8B ();
// 0x00000077 System.Void Photon.Chat.AuthenticationValues::.ctor()
extern void AuthenticationValues__ctor_m32B8E2CA72D7C3EFAADBF65AD18B40C98CAAAB42 ();
// 0x00000078 System.Void Photon.Chat.AuthenticationValues::.ctor(System.String)
extern void AuthenticationValues__ctor_m9FA4C1E00AE7E91FF6517E36A4BA4F7778E927F9 ();
// 0x00000079 System.Void Photon.Chat.AuthenticationValues::SetAuthPostData(System.String)
extern void AuthenticationValues_SetAuthPostData_mA60C9ECF23D9E3634A3A207FCA348C0FEC4D758C ();
// 0x0000007A System.Void Photon.Chat.AuthenticationValues::SetAuthPostData(System.Byte[])
extern void AuthenticationValues_SetAuthPostData_mC387DAA5F17257D62FFE26765EFD4AD8C88C988B ();
// 0x0000007B System.Void Photon.Chat.AuthenticationValues::AddAuthParameter(System.String,System.String)
extern void AuthenticationValues_AddAuthParameter_m5A2A8B1815CFD3CAE2A02CAC79DC38626DB0DA42 ();
// 0x0000007C System.String Photon.Chat.AuthenticationValues::ToString()
extern void AuthenticationValues_ToString_m61B34D3505FB9B8829ADCE870C0C56AA91919C87 ();
// 0x0000007D System.Void Photon.Chat.ParameterCode::.ctor()
extern void ParameterCode__ctor_m4A87B3E2E228B85CF730FBDDDBC4DD1DB54EFA6B ();
// 0x0000007E System.Void Photon.Chat.ErrorCode::.ctor()
extern void ErrorCode__ctor_m834E31BB4EA037772AC46662DA9EB1511287E8B5 ();
// 0x0000007F System.Void Photon.Chat.IChatClientListener::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
// 0x00000080 System.Void Photon.Chat.IChatClientListener::OnDisconnected()
// 0x00000081 System.Void Photon.Chat.IChatClientListener::OnConnected()
// 0x00000082 System.Void Photon.Chat.IChatClientListener::OnChatStateChange(Photon.Chat.ChatState)
// 0x00000083 System.Void Photon.Chat.IChatClientListener::OnGetMessages(System.String,System.String[],System.Object[])
// 0x00000084 System.Void Photon.Chat.IChatClientListener::OnPrivateMessage(System.String,System.Object,System.String)
// 0x00000085 System.Void Photon.Chat.IChatClientListener::OnSubscribed(System.String[],System.Boolean[])
// 0x00000086 System.Void Photon.Chat.IChatClientListener::OnUnsubscribed(System.String[])
// 0x00000087 System.Void Photon.Chat.IChatClientListener::OnStatusUpdate(System.String,System.Int32,System.Boolean,System.Object)
// 0x00000088 System.Void Photon.Chat.IChatClientListener::OnUserSubscribed(System.String,System.String)
// 0x00000089 System.Void Photon.Chat.IChatClientListener::OnUserUnsubscribed(System.String,System.String)
static Il2CppMethodPointer s_methodPointers[137] = 
{
	ChannelCreationOptions_get_PublishSubscribers_m95B809EAFBB363FA01A7132CD7FA48D2200D85AC,
	ChannelCreationOptions_set_PublishSubscribers_m5A86AB97EE0743192DF387D96C8D954E8E872351,
	ChannelCreationOptions_get_MaxSubscribers_m695771BA6E0451DEF5C00D270D95F218735DD6F0,
	ChannelCreationOptions_set_MaxSubscribers_m93B6C3596A984DBD6FBB4EB89920062DC90FD0B8,
	ChannelCreationOptions__ctor_m81C207BB78F06CA880DDBE76D5B747ACD145194E,
	ChannelCreationOptions__cctor_m0C802A2ED726B657BA62EAF7DD1717C77A42A6F4,
	ChannelWellKnownProperties__ctor_mACCE1BE31AABEB657F01B5401D95FC370D674F80,
	ChatChannel_get_IsPrivate_mF3161AC27D69E4594D632BE0C37D2010A5E3FA2D,
	ChatChannel_set_IsPrivate_m1BA5274B9E487BB6284FDFBAFDF12C1074F86A28,
	ChatChannel_get_MessageCount_m55833104CB82ADCAE20517D318E130365D54C835,
	ChatChannel_get_LastMsgId_mB439D932AC09AFD3712CFDCBF5B83585E7957892,
	ChatChannel_set_LastMsgId_mD7539C79B24056E2718FDD4DA4DED06AE641863C,
	ChatChannel_get_PublishSubscribers_m95072AE75264032C94FBE71EA09F257C889B18EB,
	ChatChannel_set_PublishSubscribers_m3162B1905CF3F87749D06FD6B865836C37B9F24C,
	ChatChannel_get_MaxSubscribers_m7564E3A11AF219E2DECD04BB4A27BD5B3D2C8915,
	ChatChannel_set_MaxSubscribers_mA3EF3E020422FFE128ADDCC8C0657D77AF353D62,
	ChatChannel__ctor_mA5DF33529980160873130ACB6B51CC8D3CF23C83,
	ChatChannel_Add_m27C5E5A559983353AB0D695A3A82056B23B6853E,
	ChatChannel_Add_m40DB5A55A3C7DFD1195FBF16C50CBFBBECBA3DFA,
	ChatChannel_TruncateMessages_m8BAAC468313ADAED4EC6C86B55EA068DE0A2268E,
	ChatChannel_ClearMessages_m9EF1467E2BD5CB8CAE4DC2FFFAEF551327661E55,
	ChatChannel_ToStringMessages_m72F03F0CBA803F8A6BA13C02D981EB23506B06B1,
	ChatChannel_ReadProperties_m6633C7C905179618C610C9EB69610AEEE570A09D,
	ChatChannel_AddSubscribers_m67F53F0F32C4061FB3870EE624D7481338F950EB,
	ChatChannel_ClearProperties_m4A373CC7CFCB23C5472292D8ED0D4E5AC6D7A036,
	ChatClient_get_NameServerAddress_m27512FE5315C5872B2215EFFC468BF354EB6896D,
	ChatClient_set_NameServerAddress_m7A3F9E28BE94D5DA80D7559F6A265DD2C86510E6,
	ChatClient_get_FrontendAddress_m847BEC17A9DDF9204AB4A1C860392DDD6BCB7F83,
	ChatClient_set_FrontendAddress_mF45D1E2D49E57DCCB99146E706C21245D972C4CC,
	ChatClient_get_ChatRegion_mBDD657C571E8583799BF73FA271B82BE6D06F2D9,
	ChatClient_set_ChatRegion_mDFE542A6A3CE4AB4173A7E16079D14E5ECECEFCB,
	ChatClient_get_State_m699EC4A67CAA0596492E6E32DA46654A3DBC9EA0,
	ChatClient_set_State_mB4F7447BD9819E2D8EE5E6F169A0AE7A0F49DC1E,
	ChatClient_get_DisconnectedCause_m57D498FAFF15DE6D6ED299021BAD2EEE35B2EEFC,
	ChatClient_set_DisconnectedCause_m0726D082C1B2F8E366BAC950319DEA20CF09FFDB,
	ChatClient_get_CanChat_m303DEA407E43F29A30A67CF23FA9BF1D8CDC28F7,
	ChatClient_CanChatInChannel_m056F9E3199627DC00B9D30327069029F5601CF1C,
	ChatClient_get_HasPeer_m0F8268A96258BC9856A223ACE894A1DBD2C2026B,
	ChatClient_get_AppVersion_m325B5082717878BDB33E2AD0F338BA920984489B,
	ChatClient_set_AppVersion_m7852C67700E10420F7E14D3BA0C4DC5E5E9781C8,
	ChatClient_get_AppId_mA154B74C72A401D4A2569B9D1DBE815AC26AB8CC,
	ChatClient_set_AppId_m49B2AD5DFFCB3328EB95E1416DC4640BAC72847A,
	ChatClient_get_AuthValues_mF82FDE2770658A9C379F1CF74DF14A601F5D317E,
	ChatClient_set_AuthValues_mA9F19EDA73501C75C59D69C949B66953CE0B8EFE,
	ChatClient_get_UserId_m2572A1A6BB1413F845DED350093EAAFD6B518291,
	ChatClient_set_UserId_m5499473A68F5A0ADD366380F3135D9FC32A9EC6D,
	ChatClient_get_UseBackgroundWorkerForSending_mA5A8EE30F7BB6048082C233967E2DA5D2CEE77E5,
	ChatClient_set_UseBackgroundWorkerForSending_mFC84C720BB9E2EFC7780214093D51CBB9765A53F,
	ChatClient_get_TransportProtocol_m25094E1A427A4A31BA5A4406B2B68A5AA6B3BEE3,
	ChatClient_set_TransportProtocol_m6286E7D74D21E1C6D7A6FF1230E5160A590A8238,
	ChatClient_get_SocketImplementationConfig_m273FA4B8A6B399A7246D4052DEAE505355649C2E,
	ChatClient__ctor_m519ED33F2BE9B3BA820520F051C3208EC059FC16,
	ChatClient_Connect_m6A720637091E96EB0559F3BEAF85B0415BB0D376,
	ChatClient_ConnectAndSetStatus_m3111CC36FB9C7871CD736E0C376BF0868859CC87,
	ChatClient_Service_m2EF335812DD640B6CC84AACDDB02689B595D95AE,
	ChatClient_SendOutgoingInBackground_m6DACEF4746C9018F9300FC55DB31F72459728CE4,
	ChatClient_SendAcksOnly_m085143D7A29367B04DF0FF6C58ED4CF5894195F9,
	ChatClient_Disconnect_m60913D750CE62764246A28A0024E536FCE8EDD90,
	ChatClient_StopThread_mA350213BEDE2B23419BF0DB5DCE01423AA4E51B8,
	ChatClient_Subscribe_m01D13AF1DF355F1AFEA2CA2F3184247CCF9EF2EB,
	ChatClient_Subscribe_m162186BD32689824C32D27765C4418229ADE67F1,
	ChatClient_Subscribe_m0391C33DF36612A52DE871DD0B1726EA62F99B2A,
	ChatClient_Unsubscribe_mF3824B5D3C47882442414123A2C48B31C62532CC,
	ChatClient_PublishMessage_mC4654F785E2073DF765DB653B9AC6B0275BFF082,
	ChatClient_PublishMessageUnreliable_mE608B1F6C93BCCCC6D02A71C15717684AD0B8A43,
	ChatClient_publishMessage_m112332E73165EF56FF7ED01EEF1EED95C417077D,
	ChatClient_SendPrivateMessage_m5F692F987A6E6233681A597066279DD7BC2F41EB,
	ChatClient_SendPrivateMessage_m901045581F7DE5BAC226C52C9369184523EC5118,
	ChatClient_SendPrivateMessageUnreliable_m21DF7070ADC81ADC83BF32FA0536D1D80310C4A9,
	ChatClient_sendPrivateMessage_mCF7B7BD77EB7D22DFD1ED058A4235578551C0C02,
	ChatClient_SetOnlineStatus_mC9DBD5B31D8A48987B17A7E4C7A6D8CF6C299B85,
	ChatClient_SetOnlineStatus_mEB17F03D8059E4C4A407087F4798D1BB83FD2849,
	ChatClient_SetOnlineStatus_mB88272E0588B53E913EC3CED58A35D185FE50B0D,
	ChatClient_AddFriends_mCC05D291C9509A0F4D776428BC69AFF28DD25AB7,
	ChatClient_RemoveFriends_m55D5D9623A9B84C801016004A1E436F606417513,
	ChatClient_GetPrivateChannelNameByUser_mF71560A0FA905208F34FCA6331BD19051A598F2E,
	ChatClient_TryGetChannel_m1353AC1501810593E12AD94C6B40A9DCA4272E5D,
	ChatClient_TryGetChannel_m98591C6D7F4304FAC4F1FAE967DC8AD8B36C684B,
	ChatClient_TryGetPrivateChannelByUser_mEC5655F7F36413A2C05FA54794DC7681F0E7EF6A,
	ChatClient_set_DebugOut_m743928BC0E48DDBFF6189961C7021981B81F5F14,
	ChatClient_get_DebugOut_m2CF0AB00C4149E1C87579AED3259E9C16FF5EE3E,
	ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_DebugReturn_m839DC9F90D7FAA6FB99ED2BD11F70DD011C24F3E,
	ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnEvent_mCB50D65A6476CA679FC0A53086670174BD9B6520,
	ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnOperationResponse_mF3D11ED8D0695BC30EB93DB09EB5ADCDB5D757DD,
	ChatClient_ExitGames_Client_Photon_IPhotonPeerListener_OnStatusChanged_m35846B72C3CD364C901F26FEDB3F0BCA98BCB8E0,
	ChatClient_SendChannelOperation_m18D325052913A99B794CF2CC4B24D290344B97AF,
	ChatClient_HandlePrivateMessageEvent_mDA58DBBF050FCDB626C81EFC54565ED921B3964D,
	ChatClient_HandleChatMessagesEvent_m396D3671FD7D82CDDAA061853140CC3509E0C08C,
	ChatClient_HandleSubscribeEvent_mFB647ACE16281DFE79CDFBCB7BAC435C43099DA4,
	ChatClient_HandleUnsubscribeEvent_m9A4724792F7A5DA097C59201F4BE301CEE752D5B,
	ChatClient_HandleAuthResponse_m1B87CA382F32D97CC8FBE955D8D8A77D93E29C47,
	ChatClient_HandleStatusUpdate_m6AAACBA3D1841A4B8E6FE0FC4347CE6AFD793DFA,
	ChatClient_ConnectToFrontEnd_m83830EDC762ADBB4EF3835E212BFE190F7AC100A,
	ChatClient_AuthenticateOnFrontEnd_mFA3F028897624959C21DBBEAFABC611E7D40D178,
	ChatClient_HandleUserUnsubscribedEvent_m28F7860D105EF0348648A3002AEEB6DA37ED22D1,
	ChatClient_HandleUserSubscribedEvent_mE87D9FFBF7C3B8AE0BBA5F0448C9A0947A72E07B,
	ChatClient_Subscribe_m0DB2A59D3FAE36EB09B8F34839AD18A95D0E6A98,
	ChatEventCode__ctor_m17DE89A722A955012AEC573B9C87560E318AD081,
	ChatOperationCode__ctor_m481F24E834ADF462072735029D4886E36F0C149E,
	ChatParameterCode__ctor_mB5BE08E745BDC3571F9E24C104FA8AB379D20B3F,
	ChatPeer_get_NameServerAddress_mEC869AA34F11AD1F019EA0D6F7090A154DC0CAA7,
	ChatPeer_get_IsProtocolSecure_m5A49DEB1B8850A3F2F40527EF6151312D4776601,
	ChatPeer__ctor_mDAE5572BCAEA02789867C206B4E820CC30A18A44,
	ChatPeer_ConfigUnitySockets_m3CA9CA2B2B41E6CC69775F1112F8BDF897F6C2B0,
	ChatPeer_GetNameServerAddress_mEE174269D17B37F6B706229ADE9F5A070637A82A,
	ChatPeer_Connect_m57D6143BBC1FFA33E40820A96611D098C8E2C9C2,
	ChatPeer_AuthenticateOnNameServer_mEE9E9AC65A21D029BB21F0D3BFDDAFE0D79316BB,
	ChatPeer__cctor_m22A7660C34602B8F951AC2CEBC56AA40DFEE7866,
	AuthenticationValues_get_AuthType_mCBBF19A8A2D5B91B99EB4E1A529888E883EEB137,
	AuthenticationValues_set_AuthType_mFF8089E6A86CA5A54EB9AED446F55077422B6D80,
	AuthenticationValues_get_AuthGetParameters_m3E0298098E97E6C4EE98BEBD4C9180F4B2EFA2DC,
	AuthenticationValues_set_AuthGetParameters_mA4168C7B93997D0957D5373984D60D58C3D763F2,
	AuthenticationValues_get_AuthPostData_mFF1BEF9E3C7A6F0C0AB2094957BE9657D8C9AC3F,
	AuthenticationValues_set_AuthPostData_m4BB0A1F1BF32144A5AE42460E6DC676D40AD8061,
	AuthenticationValues_get_Token_mEB968C8B2E7A8D874E7DB46257D897A7DFEEDAA2,
	AuthenticationValues_set_Token_m0377AF534F7FFAA94542BA9747D9FB79DF40C80F,
	AuthenticationValues_get_UserId_m240707AC145B84C517AA9251278E3A6AD3A3C9F6,
	AuthenticationValues_set_UserId_m3D9EE5F706F311197E729BD61E81A37CDC81DE8B,
	AuthenticationValues__ctor_m32B8E2CA72D7C3EFAADBF65AD18B40C98CAAAB42,
	AuthenticationValues__ctor_m9FA4C1E00AE7E91FF6517E36A4BA4F7778E927F9,
	AuthenticationValues_SetAuthPostData_mA60C9ECF23D9E3634A3A207FCA348C0FEC4D758C,
	AuthenticationValues_SetAuthPostData_mC387DAA5F17257D62FFE26765EFD4AD8C88C988B,
	AuthenticationValues_AddAuthParameter_m5A2A8B1815CFD3CAE2A02CAC79DC38626DB0DA42,
	AuthenticationValues_ToString_m61B34D3505FB9B8829ADCE870C0C56AA91919C87,
	ParameterCode__ctor_m4A87B3E2E228B85CF730FBDDDBC4DD1DB54EFA6B,
	ErrorCode__ctor_m834E31BB4EA037772AC46662DA9EB1511287E8B5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[137] = 
{
	89,
	31,
	10,
	32,
	23,
	3,
	23,
	89,
	31,
	10,
	10,
	32,
	89,
	31,
	10,
	32,
	26,
	120,
	120,
	23,
	23,
	14,
	26,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	89,
	9,
	89,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	14,
	461,
	938,
	2052,
	23,
	89,
	23,
	23,
	23,
	9,
	90,
	146,
	9,
	1678,
	1678,
	2053,
	1678,
	2053,
	2053,
	2054,
	1688,
	30,
	1745,
	9,
	9,
	28,
	421,
	806,
	806,
	31,
	89,
	88,
	26,
	26,
	32,
	2055,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	89,
	26,
	26,
	673,
	23,
	23,
	23,
	14,
	89,
	461,
	23,
	14,
	89,
	1004,
	3,
	89,
	31,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	26,
	26,
	26,
	27,
	14,
	23,
	23,
	88,
	23,
	23,
	32,
	211,
	211,
	27,
	26,
	1748,
	27,
	27,
};
extern const Il2CppCodeGenModule g_PhotonChatCodeGenModule;
const Il2CppCodeGenModule g_PhotonChatCodeGenModule = 
{
	"PhotonChat.dll",
	137,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
