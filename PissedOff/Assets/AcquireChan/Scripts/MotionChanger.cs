﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;


/*!
 *	----------------------------------------------------------------------
 *	@brief	モーション切り替えスクリプト
 *	
 *	@note	←→キーかOnGUIのボタンでモーションを切り替える
 *	
*/
public class MotionChanger : MonoBehaviourPun
{
	[SerializeField] private Animator	m_Animator;

	private int					m_AnimationIndex = 0;
	private int					m_AnimationMax = 0;
	private AnimatorStateInfo	m_PrevState;
	private bool				m_ChangingMotion = false;

	//healthbar
	
	public int maxHealth = 100;
	public int currenthealth;
    [SerializeField]
	private HealthBarScript healthbar;
	
	public GameObject chat;
	public Text chatTxt;
	//audio sound
	private AudioSource audioSource;
	public  AudioClip hurt;
	public AudioClip walk;
	[SerializeField]
	private PhotonView motionPV;
	
	



	/*!
	 *	----------------------------------------------------------------------
	 *	@brief	初期化
	*/
	private void Start()
	{
		currenthealth = maxHealth;
		healthbar.SetMaxHealth(maxHealth);
		chat.SetActive(false);

		AnimationClip[] AnimationClips = m_Animator.runtimeAnimatorController.animationClips;

		m_AnimationIndex = 0;
		m_AnimationMax = AnimationClips.Length;
		m_PrevState = m_Animator.GetCurrentAnimatorStateInfo(0);

		// index
		for( int i=0; i < m_AnimationMax; ++i )
		{
			if( m_PrevState.IsName( AnimationClips[i].name ) )
			{
				m_AnimationIndex = i;
				break;
			}
		}

		audioSource = GetComponent<AudioSource>();
	}

	/*!
	 *	----------------------------------------------------------------------
	 *	@brief	更新
	*/
	private void Update()
	{

		// モーション遷移中
		AnimatorStateInfo animState = m_Animator.GetCurrentAnimatorStateInfo(0);
		if( animState.fullPathHash != m_PrevState.fullPathHash )
		{
			// 遷移完了
			m_Animator.SetBool( "prev", false );
			m_Animator.SetBool( "next", false );

			m_ChangingMotion = false;
			m_PrevState = m_Animator.GetCurrentAnimatorStateInfo(0);
		}
		else
		{
			// モーション変更
            if (base.photonView.IsMine)
            {
				if (Input.GetKeyDown(KeyCode.LeftArrow))
				{
					PrevAnimation();
					
				}
				else if (Input.GetKeyDown(KeyCode.RightArrow))
				{
					NextAnimation();
					
				}
			}
           
			
		}

		// 終了
		if( Input.GetKeyDown( KeyCode.Escape ) ) Application.Quit();
	}


    
	/*!
	 *	----------------------------------------------------------------------
	 *	@brief	アニメーション次へ
	*/
	private void NextAnimation()
	{
		if( m_ChangingMotion ) return;
        
		m_AnimationIndex = ((m_AnimationIndex + 1) % m_AnimationMax);
		m_PrevState = m_Animator.GetCurrentAnimatorStateInfo(0);
		m_Animator.SetBool("next", true);
		m_ChangingMotion = true;
		Debug.Log("Success Changing Animation");
		
		
	}

	/*!
	 *	----------------------------------------------------------------------
	 *	@brief	アニメーション前へ
	*/
	private void PrevAnimation()
	{
		if( m_ChangingMotion ) return;
        
        
		m_AnimationIndex = ((m_AnimationIndex - 1 + m_AnimationMax) % m_AnimationMax);
		m_PrevState = m_Animator.GetCurrentAnimatorStateInfo(0);
		m_Animator.SetBool("prev", true);
		m_ChangingMotion = true;
		Debug.Log("Success Changing Animation");
		
		
	}






    /*!
	 *	----------------------------------------------------------------------
	 *	@brief	ボタン表示
	//*/
    private void OnGUI()
    {
        if (base.photonView.IsMine)
        {
            GUIStyle tempStyle = GUI.skin.box;
            tempStyle.fontSize = 50;

            Vector2 boxSize = new Vector2(600f, 200f);
            Vector2 pos = new Vector2(Screen.width-boxSize.x-250f, (Screen.height - boxSize.y-400f));

            // モーション名
            string animName = m_Animator.runtimeAnimatorController.animationClips[m_AnimationIndex].name;
            string text = string.Format("{0} [{1}/{2}]", animName, (m_AnimationIndex + 1), m_AnimationMax);
            GUI.Box(new Rect(pos, boxSize), text, tempStyle);

            //ボタン
            pos.x += 50f;
            pos.y += 70f;
            if (GUI.Button(new Rect(pos, new Vector2(250f, 100f)), "<<<", tempStyle))
            {
                PrevAnimation();
                FindObjectOfType<GameSetuoController>().animationTxt.SetActive(false);
            }
            pos.x += (250f);
            if (GUI.Button(new Rect(pos, new Vector2(250f, 100f)), ">>>", tempStyle))
            {
                NextAnimation();
                FindObjectOfType<GameSetuoController>().animationTxt.SetActive(false);
            }
        }

    }

    //GUI.Button(new Rect(pos, new Vector2(200f, 100f)), "<<", tempStyle)
    //GUI.Button(new Rect(pos, new Vector2(200f, 100f)), ">>", tempStyle)

    public void OnTriggerEnter(Collider other)
    {
		if (other.gameObject.tag == "Arrow")
		{
			audioSource.Stop();
			TakeDamage(10);
			audioSource.PlayOneShot(hurt);
			StartCoroutine(waitSec(other.gameObject));

		}
	}

    void TakeDamage(int damage)
    {
		currenthealth -= damage;
		healthbar.SetHealth(currenthealth);
		
    }

    IEnumerator waitSec (GameObject arrow)
    {
		yield return new WaitForSeconds(3f);

		Destroy(arrow);
    }

    public void regenerate()
    {
      
		currenthealth = maxHealth;
		healthbar.SetMaxHealth(maxHealth);

	}

    public void UpdateChat(string txt)
    {
		Debug.Log("update chat");
		StartCoroutine(WaitDelay(txt));
		
    }

    IEnumerator WaitDelay(string txt)
    {
        
		chat.SetActive(true);
		yield return new WaitForSeconds(3f);
		chatTxt.text = txt;
	}

}
