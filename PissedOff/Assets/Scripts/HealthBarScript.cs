﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarScript : MonoBehaviourPun
{

    public Slider slider;
    private GameObject question;
    private GameObject mssIn;
    private GameObject waiting;

    public Gradient gradientColor;
    public Image fill;
    [SerializeField]
    private MotionChanger health;
    public bool appear;

    private bool change;
    private PhotonView PV;




    private void Start()
    {
        question = FindObjectOfType<GameSetuoController>().question;
        mssIn = FindObjectOfType<GameSetuoController>().mssInput;
        waiting = FindObjectOfType<GameSetuoController>().waiting;
        appear = true;
        change = false;
        PV = GetComponent<PhotonView>();



    }


    void Update()
    {
        //check if the opposite user, which is not the master client, press No then set every UI in masterclient display to false. 
        if (PhotonNetwork.IsMasterClient)
        {
            Debug.Log(slider.value);
            if (health.currenthealth <= 0 && appear) // check if the hp is 0 then display UI on both client
            {
                Debug.Log(appear);
                // for master client display only waiting UI at first
                Debug.Log("waiting UI display");
                question.SetActive(false);
                mssIn.SetActive(false);
                waiting.SetActive(true);
                appear = false;

            }

            else if (change)
            {
                Debug.Log("UI off");

                question.SetActive(false);
                mssIn.SetActive(false);
                waiting.SetActive(false);
                change = false;

            }
        }
        else if (!PhotonNetwork.IsMasterClient)
        {
            if (slider.value <= 0 && appear)
            {
                // for other client display the question UI to choose Yes/No.
                question.SetActive(true);
                mssIn.SetActive(false);
                waiting.SetActive(false);
                appear = false;
            }

        }


    }


    public void onClickNo()
    {

        // if the user press No then reset the healthbar and set every UI to false.
        //SetMaxHealth(health.maxHealth);
        //health.currenthealth = health.maxHealth;
        
        question.SetActive(false);
        waiting.SetActive(false);
        mssIn.SetActive(false);
        change = true;
        appear = true;
        if (!PhotonNetwork.IsMasterClient)
        {
            PV.RPC("RPC_BoolChange", RpcTarget.AllBuffered, change, appear);
        }

    }

    [PunRPC]
    void RPC_BoolChange(bool changIn, bool appearIn)
    {
        change = changIn;
        appear = appearIn;
        health.regenerate();
        
        //slider.value = maxHPIn;
    }




    public void SetMaxHealth(int health)
    {
        slider.maxValue = health;
        slider.value = health;

        fill.color = gradientColor.Evaluate(1f);
    }


    public void SetHealth(int health)
    {
        slider.value = health;

        fill.color = gradientColor.Evaluate(slider.normalizedValue);

        //if (!PhotonNetwork.IsMasterClient)
        //{
        //    PV.RPC("RPC_hpchange", RpcTarget.AllBuffered, health);
        //}

    }

    //[PunRPC]
    //void RPC_hpchange(int healthIn)
    //{
    //    slider.value = healthIn;
    //}

}
