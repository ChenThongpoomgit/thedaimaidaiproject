﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DelayStartWaitingRoomController : MonoBehaviourPunCallbacks
{
    /*This object must be attached to an object
     * in the waiting room scene of your project.*/

    //photon view for sending rpc that updates the timer
    private PhotonView myPhotonView;

    // scene navigation indexes
    [SerializeField]
    private int multiplayerSceneIndex;
    [SerializeField]
    private int menuSceneIndex;
    //number of the players in the room out of the total room size
    private int playerCount;
    private int roomSize;
    [SerializeField]
    private int maxPlayersToStart;
    private bool startingGame;

    //text variables for holding the displays for the player count
    [SerializeField]
    private Text playerCountDisplay;
    [SerializeField]
    private Text timerToStartDisplay;

    //bool values for if the timer can count down
    private bool readyToCount;
    private bool readyToStart;

    //countdown timer variables
    private float timerToStartGame;
    private float notFullGameTimer;
    private float fullGameTimer;
    //countdown reset
    [SerializeField]
    private float maxWaitTime;
    [SerializeField]
    private float maxFullGameWaitTime;

    [SerializeField]
    private GameObject statusPrefab;
    public GameObject chatContainer;

    private GameObject MasterPlayer;
    private GameObject otherPlayers;

    private int First;
    private int Second;





    private void Start()
    {
        //initialize variables
        myPhotonView = GetComponent<PhotonView>();
        fullGameTimer = maxFullGameWaitTime;
        notFullGameTimer = maxWaitTime;
        timerToStartGame = maxWaitTime;

        PlayerCountUpdate();

        //instantiate the player status
        //FindObjectOfType<UsernameIn>().changeStatusName();
        if (PhotonNetwork.IsMasterClient)
        {
            First = Random.Range(1000, 9999);
            MasterPlayer = Instantiate(statusPrefab, chatContainer.transform);
            MasterPlayer.GetComponentInChildren<Text>().text = "User" + playerCount + "#" + First + " IS CONNECTED !!";

        }
        //else if (!PhotonNetwork.IsMasterClient)
        //{
        //    MasterPlayer = Instantiate(statusPrefab, chatContainer.transform);
        //    MasterPlayer.GetComponentInChildren<Text>().text = "User1" + "#" + First + " IS CONNECTED !!";
        //}
        


    }

    

    void PlayerCountUpdate()
    {
        //updates player count when players join the room
        //displays player count
        playerCount = PhotonNetwork.PlayerList.Length;
        roomSize = PhotonNetwork.CurrentRoom.MaxPlayers;
        playerCountDisplay.text = playerCount + " / " + roomSize;

        if (playerCount == roomSize)
        {
            readyToStart = true;
        }
        else if (playerCount >= maxPlayersToStart)
        {
            readyToCount = true;
        }
        else
        {
            readyToCount = false;
            readyToStart = false;
        }


    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        
        //called whenever a new player joins the room
        PlayerCountUpdate();
        Second = Random.Range(1000, 9999);
        // send master clients countdown timer to all other players in order to sync time
        if (PhotonNetwork.IsMasterClient)
        {
            myPhotonView.RPC("RPC_SendTimer", RpcTarget.Others, timerToStartGame,First,Second);
            otherPlayers = Instantiate(statusPrefab, chatContainer.transform);
            otherPlayers.GetComponentInChildren<Text>().text = "User" + playerCount + "#" + Second + " IS CONNECTED !!";
        }
       





    }

    [PunRPC]
    private void RPC_SendTimer(float timeIn, int firstId, int secondId)
    {
        MasterPlayer = Instantiate(statusPrefab, chatContainer.transform);
        MasterPlayer.GetComponentInChildren<Text>().text = "User1" + "#" + firstId + " IS CONNECTED !!";
        //GameObject go = Instantiate(statusPrefab, chatContainer.transform);
        //go.GetComponentInChildren<Text>().text = "User" + playerCount + "#" + Random.Range(1000, 9999) + " IS CONNECTED !!";
        otherPlayers = Instantiate(statusPrefab, chatContainer.transform);
        otherPlayers.GetComponentInChildren<Text>().text = "User" + playerCount + "#" + secondId + " IS CONNECTED !!";
        
        //RPC for syncing the countdown timer to those that join after it has started the countdown
        timerToStartGame = timeIn;
        notFullGameTimer = timeIn;
        if (timeIn < fullGameTimer)
        {
            fullGameTimer = timeIn;
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        //called whenever a player leaves the room.
        PlayerCountUpdate();
        if (otherPlayer.IsMasterClient)
        {
            myPhotonView.RPC("RPC_DestroyMaster", RpcTarget.AllBuffered,First,Second);
        }
        else if (!otherPlayer.IsMasterClient)
        {
            myPhotonView.RPC("RPC_DestroyOther", RpcTarget.AllBuffered);
        }
            
        
    }

    [PunRPC]
    void RPC_DestroyMaster(int firstId, int secondId)
    {
        
        Destroy(MasterPlayer);
        otherPlayers.GetComponentInChildren<Text>().text = "User" + playerCount + "#" + firstId + " IS CONNECTED !!";
        
        
    }

    [PunRPC]
    void RPC_DestroyOther()
    {
        Destroy(otherPlayers);
    }

    private void Update()
    {
        WaitingForMorePlayers();
    }

    void WaitingForMorePlayers()
    {
        // if there is only 1 player in the room the timer will reset
        if (playerCount <= 1)
        {
            ResetTimer();
        }

        //when there is enough players in the room the start timer will begin counting down
        if (readyToStart)
        {
            fullGameTimer -= Time.deltaTime;
            timerToStartGame = fullGameTimer;
        }
        else if (readyToCount)
        {
            notFullGameTimer -= Time.deltaTime;
            timerToStartGame = notFullGameTimer;
        }

        //format and display countdown timer
        string tempTimer = string.Format("{0:00}", timerToStartGame);
        timerToStartDisplay.text = tempTimer;
        //if the countdown timer reaches 0 the game will then start
        if (timerToStartGame <= 0f)
        {
            timerToStartDisplay.text = "00";
            if (startingGame)
            {
                return;
            }
            StartGame();


        }

    }

    


    void ResetTimer()
    {
        //resets the countdown timer
        timerToStartGame = maxWaitTime;
        notFullGameTimer = maxWaitTime;
        fullGameTimer = maxFullGameWaitTime;
    }

    public void StartGame()
    {
        //Multiplayer scene is loaded to start the game
        startingGame = true;
        if (!PhotonNetwork.IsMasterClient)
        {
            return;
        }
        PhotonNetwork.CurrentRoom.IsOpen = false;
        PhotonNetwork.LoadLevel(multiplayerSceneIndex);
    }

    public void DelayCancel()
    {
        //public function paired to cancel button on waiting room scene
        Debug.Log("quit");
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(menuSceneIndex);
    }
}
