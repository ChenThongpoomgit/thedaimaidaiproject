﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndCredit : MonoBehaviour
{
    [SerializeField]
    private GameObject DisButton;
    public int menuScene;
    public AudioSource audioPlay;
    public AudioClip greeting;
    public AudioClip crying;
    [SerializeField]
    private Text checkTxt;
    

    
    // Start is called before the first frame update
    void Start()
    {

        FindObjectOfType<StaticScript>().showTxt();
        DisButton.SetActive(true);
        if (checkTxt.text == "CONGRATULATION YOU ARE THE WINNER ! ! !")
        {
            audioPlay.PlayOneShot(greeting);
        }
        else if (checkTxt.text == "SORRY YOU ARE A LOSER ! ! !")
        {
            audioPlay.PlayOneShot(crying);
        }
    }

    public void OnclickDis()
    {
        Debug.Log("Player disconnected !!");
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(menuScene);
    }
}
