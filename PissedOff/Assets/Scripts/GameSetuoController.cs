﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using UnityEngine.SceneManagement;
using Photon.Realtime;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;


public class GameSetuoController : MonoBehaviour
{
    [SerializeField]
    private GameObject delayCancelButton; //button to exit the room
    [SerializeField]
    private int menuSceneIndex;

    //Button for choosing Animation.
    
    
    public GameObject animationTxt;
    public GameObject holdTxt;
    

    [SerializeField]
    public Camera arCamera;
    private GameObject model;
    public ARRaycastManager arRayMng;
    public Vector3 camOffset = new Vector3(0.06f, -0.05f, 0.20f);

    //UI display
    public GameObject question;
    public GameObject mssInput;
    public GameObject waiting;
    private bool swap;
    private PhotonView myPV;

    //UI Text
    public string mssChatTxt;
    public GameObject chatTxt;
    public GameObject forgiveQuestion;
    
    

    //audio sound
    private AudioSource audiosource;
    public AudioClip bgSound;
    public AudioSource BgAudio;

    
    
    
    
  

    void Start()
    {

        swap = false;
        audiosource = GetComponent<AudioSource>();
        myPV = GetComponent<PhotonView>();
        
        
        if (PhotonNetwork.IsMasterClient)
        {
            

            CreatePlayer();
            
            holdTxt.SetActive(false);
            animationTxt.SetActive(true);
            Debug.Log("animatxt");
            
        }
        else
        {
            
            animationTxt.SetActive(false);
            holdTxt.SetActive(true);
            
        }

        



    }

    private void Update()
    {
        //check if opposite user is press yes and this user will display input text box.
        if (swap && PhotonNetwork.IsMasterClient) 
        {
            //if other client choose Yes, then the master client display the input txt for sending the message.
            mssInput.SetActive(true);
            question.SetActive(false);
            waiting.SetActive(false);
            swap = false;

            
        }
       
        
    }

    public void OnclickFinish()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            myPV.RPC("RPC_FinishTrans", RpcTarget.AllBuffered);
        }
    }

    [PunRPC]
    void RPC_FinishTrans()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            forgiveQuestion.SetActive(true);
        }
        else if (PhotonNetwork.IsMasterClient)
        {
            waiting.SetActive(true);
            mssInput.SetActive(false);
        }
        
    }

    public void OnclickEnter()
    {
        
        if (PhotonNetwork.IsMasterClient)
        {
            
            myPV.RPC("RPC_TextSend", RpcTarget.AllBuffered, chatTxt.GetComponent<Text>().text);
        }
        
    }

    [PunRPC]
    void RPC_TextSend(string msstxt)
    {

        waiting.SetActive(false);
        chatTxt.GetComponent<Text>().text = "";
        FindObjectOfType<MotionChanger>().UpdateChat(msstxt);
        
    }

    public void OnClickY()
    {

        
        Debug.Log("waitingAct");
        // display waiting ui when the user press yes, and wait for opposite user to type in the txt.
        question.SetActive(false);
        waiting.SetActive(true);
        mssInput.SetActive(false);
        swap = true;
        if (!PhotonNetwork.IsMasterClient)
        {
            myPV.RPC("RPC_changeSwapBool", RpcTarget.AllBuffered, swap);
        }
        
    }

    [PunRPC]
    void RPC_changeSwapBool(bool swapIn)
    {
        BgAudio.Stop();
        swap = swapIn;
        audiosource.PlayOneShot(bgSound);
        audiosource.loop = true;
    }

    public void OnCLickN()
    {
        Debug.Log("onclickNo");
        // if the user press no, this will call to the HealthBarScript onClickNo() function.
        FindObjectOfType<HealthBarScript>().onClickNo();
        
    }


    private void CreatePlayer()
    {
        Debug.Log("Creating Player");
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Character"), new Vector3(0f, -3f, 4f), Quaternion.AngleAxis(180, Vector3.up));
        
        //model = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Healthbar"), new Vector3(0f, 1f, 4f), Quaternion.identity);
        //delayCancelButton.SetActive(true);
        
        //model.transform.parent = gameObject.transform;

    }

    public void DelayCancel() // paired to the cancel button, for disconnect the room
    {
        delayCancelButton.SetActive(false);
        Debug.Log("Player disconnected !!");
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(menuSceneIndex);
    }

    public void OnPlayerLeftRoom(Player otherPlayer)
    {
        // Destroy player object
        
        PhotonNetwork.DestroyPlayerObjects(otherPlayer);
    }
}
