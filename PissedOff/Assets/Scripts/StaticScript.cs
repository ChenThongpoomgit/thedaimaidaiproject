﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class StaticScript : MonoBehaviour
{

    static public string lastSceneTxt;
    public PhotonView StaticPV;
    [SerializeField]
    private Text Txtshow;
    //Load Next Scene
    [SerializeField]
    private int NextScene;

    public void showTxt()
    {
        Txtshow.text = lastSceneTxt;
    }

    public void OnClickForgiveY()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            lastSceneTxt = "CONGRATULATION YOU ARE THE WINNER ! ! !";
            StaticPV.RPC("RPC_toLastScene", RpcTarget.AllBuffered, lastSceneTxt);
        }
    }

    public void OnClickForgiveN()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            lastSceneTxt = "SORRY YOU ARE A LOSER ! ! !";
            StaticPV.RPC("RPC_toLastScene", RpcTarget.AllBuffered, lastSceneTxt);
        }
    }

    [PunRPC]
    void RPC_toLastScene(string lastTxt)
    {
        lastSceneTxt = lastTxt;
        SceneManager.LoadScene(NextScene);
    }


}
