﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using Photon.Realtime;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.XR.ARFoundation;

public class DragObject : MonoBehaviour
{

    [SerializeField]
    private Camera arcamera;

    public ARRaycastManager arRayMng;
    private bool onTouchHold = false;
    private Vector2 touchPosition = default;
    private GameObject Bow;
    public GameObject Txt;

    //bow sound fx
    [SerializeField]
    private AudioSource bowaudio;
    public AudioClip bowFx;
    [SerializeField]
    private PhotonView DragPv;

    private static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    void Start()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            CreateWeapon(); // create a network weapon
            Txt.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

        
        
        if (!PhotonNetwork.IsMasterClient)
        {

            

            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                touchPosition = touch.position;

                if (touch.phase == TouchPhase.Began)
                {
                    Txt.SetActive(false);
                    Ray ray = arcamera.ScreenPointToRay(touch.position);
                    RaycastHit hitObject;
                    
                    if (Physics.Raycast(ray, out hitObject))
                    {

                        if (hitObject.transform.name.Contains("bow(Clone)"))
                        {
                            //Debug.Log("hit");
                            onTouchHold = true;
                        }

                    }
                }
                
                if (touch.phase == TouchPhase.Moved)
                {
                    touchPosition = touch.position;
                    //Debug.Log(touchPosition);
                   
                }
                if (touch.phase == TouchPhase.Ended)
                {
                    onTouchHold = false;
                    DragPv.RPC("RPC_BowSoundFx", RpcTarget.AllBuffered);
                }

            }

            //Debug.Log(arRayMng.Raycast(touchPosition, hits, UnityEngine.XR.ARSubsystems.TrackableType.PlaneWithinPolygon));
            if (arRayMng.Raycast(touchPosition, hits, UnityEngine.XR.ARSubsystems.TrackableType.PlaneWithinPolygon))
            {
                Pose hitPose = hits[0].pose;
                //Debug.Log(hitPose);
                if (onTouchHold)
                {
                    //update the bow position according to your drag position
                    Bow.transform.SetPositionAndRotation(hitPose.position, Quaternion.AngleAxis(-90, Vector3.up));

                }
            }

        }
    }

    [PunRPC]
    void RPC_BowSoundFx()
    {
        bowaudio.PlayOneShot(bowFx);
    }

    private void CreateWeapon()
    {
        Debug.Log("Creating weapon");

        Bow = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "bow"), new Vector3(0.06f, -0.05f, 0.25f), Quaternion.AngleAxis(-100f, Vector3.up));

    }
}

